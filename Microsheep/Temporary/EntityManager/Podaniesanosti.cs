namespace BizAgi.EntityManager.Entities {

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Vision.Defs.OracleSpecific;
using BizAgi.PAL;
using BizAgi.PAL.Util;
using BizAgi.PAL.BeanUtils;
using BizAgi.EntityManager.Persistence;
using BizAgi.PAL.historylog;
using BizAgi.Resources;

public sealed class Podaniesanosti : BaseEntity {


	public Podaniesanosti() {
	}

	public override int getIdEnt() {
		return 10001;
	}

	public override String getEntityName() {
		return  "Podaniesanosti";
	}

   public override String getClassName() {
      return "BizAgi.EntityManager.Entities.Podaniesanosti";
   }

	String surrogateKey = "idPodaniesanosti";

	public override String getSurrogateKey() {
		return surrogateKey;
	}

	public override bool isVirtualEntity() {
		return false;
	}

	public override Dictionary<string, object> getVirtualBusinessKey() {
		return new Dictionary<string, object>() {  };
	}

	public override bool canCreateNewInstances() {
		return true;
	}

    static Dictionary<string, Func<Podaniesanosti, object>> gets = new Dictionary<string, Func<Podaniesanosti, object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Func<Podaniesanosti, object>((Podaniesanosti be) => be.getId() ) },
        { "Stiznost", new Func<Podaniesanosti, object>((Podaniesanosti be) => be.getStiznost() )         },
        { "Stav", new Func<Podaniesanosti, object>((Podaniesanosti be) => be.getStav() )         },
        { "Typ", new Func<Podaniesanosti, object>((Podaniesanosti be) => be.getTyp() )         },
        { "Datum", new Func<Podaniesanosti, object>((Podaniesanosti be) => be.getDatum() )         }    };

    static Dictionary<string, Action<Podaniesanosti,object>> sets = new Dictionary<string, Action<Podaniesanosti,object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Action<Podaniesanosti,object>((Podaniesanosti be, object newValue ) => be.setId( (int)newValue) ) },
        { "Stiznost", new Action<Podaniesanosti, object>((Podaniesanosti be, object newValue) => { if(newValue==null)be.setStiznost(null); else be.setStiznost((string) newValue ); })         },
        { "Stav", new Action<Podaniesanosti, object>((Podaniesanosti be, object newValue) => { if(newValue==null)be.setStav(null); else be.setStav(Convert.ToInt32(newValue)); })         },
        { "Typ", new Action<Podaniesanosti, object>((Podaniesanosti be, object newValue) => { if(newValue==null)be.setTyp(null); else be.setTyp(Convert.ToInt32(newValue)); })         },
        { "Datum", new Action<Podaniesanosti, object>((Podaniesanosti be, object newValue) => { if(newValue==null)be.setDatum(null); else be.setDatum(Convert.ToDateTime(newValue)); })         }    };

    static Dictionary<string, string> _factToParentAttribute = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)    {
    };
	public String getDisplayAttrib() {
		return null;
	}

	int? m_entityType = new int?(1);

	public override int? getEntityType()  {
		return m_entityType;
	}

	string m_stiznost ;

	public string getStiznost() {
		return m_stiznost;
	}

	public void setStiznost(string paramstiznost){
		this.m_stiznost = paramstiznost;
	}

	int? m_stav ;

	public int? getStav() {
		return m_stav;
	}

	public void setStav(int? paramstav){
		this.m_stav = paramstav;
	}

	int? m_Typ ;

	public int? getTyp() {
		return m_Typ;
	}

	public void setTyp(int? paramTyp){
		this.m_Typ = paramTyp;
	}

	DateTime? m_datum ;

	public DateTime? getDatum() {
		return m_datum;
	}

	public void setDatum(DateTime? paramdatum){
		this.m_datum = paramdatum;
	}

   public override object getXPath(PropertyTokenizer xPathTokenizer) {
       String completeXPath = xPathTokenizer.getCurrentPath();
               return BizAgi.PAL.XPath.XPathUtil.evaluateXPath(this, completeXPath);
   }

    public override IBaseEntity addRelationWithId(String path, int id, bool createBackRelationScopeAction, bool createHistoryLogActions, bool isAttach = true) {
        try {
        BaseEntity ret = null;
        String attrRelated = null;
        bool bIsMxMFact = false;


        if (ret == null) {
            throw new BABeanUtilsException("Path not found. Path:" + path);
        } else {
            ret.setHistoryLog(getHistoryLog());
            ret.setPersistenceManager(getPersistenceManager());
            if (!bIsMxMFact) {
				if (createBackRelationScopeAction) {
					ret.setXPath(attrRelated, this);
				} else {
					PropertyUtils.setProperty(ret, attrRelated, this);
				}
            }
            return ret;
        }
        } catch (Exception e) {
           throw new BABeanUtilsException(e);
        }
    }

	public override IBaseEntity addRelation(String path, int id) {
		BaseEntity ret = (BaseEntity)addRelationWithId(path, id, true, true);
       ScopeAction action;
       if (getEntityType().Value == 0) {// PV Entity
           action = ScopeActionFactory.getAddRelationAction(ret.getId(), path);
       } else {
           action = ScopeActionFactory.getAddRelationEntityAction(this.getClassName(), this.getId(), ret.getId(), path);
       }
       getHistoryLog().add(action);
       return ret;
	}

	public override String toXML()  {
		StringBuilder xml = new StringBuilder();
		return xml.ToString();
	}
	public override Object getLocalizedValue(String attributeName) {
		Object attributeValue = this.getXPath(attributeName);		
		return attributeValue;
	}
	public override bool containsAttribute(string propertyName) {
		return gets.ContainsKey(propertyName); 
	}
	protected override Object GetPropertyValueByName(string propertyName) {
		return gets[propertyName].Invoke(this); 
	}
	public override void SetPropertyValue(string propertyName, object newValue) {
	    if(newValue == DBNull.Value) 
	        newValue = null; 
	    try {
	        sets[propertyName].Invoke(this, newValue); 
	    } catch (Exception e) {
	        throw new Exception(string.Format("Error trying to set value for Attribute {0} in Entity {1}", propertyName, "Podaniesanosti"), e); 
	    } 
	}
	public override string GetFactOwnerAttribute(string factKey) {
		return _factToParentAttribute[factKey]; 
	}
	public override bool ExistsFactOwnerAttribute(string factKey) {
		return _factToParentAttribute.ContainsKey(factKey); 
	}
	protected override void  cloneValues(object target, Hashtable reentrance){

    Podaniesanosti tg = (Podaniesanosti) target; 
    cloneBaseValues(tg);
    tg.m_stiznost = this.m_stiznost; 
    tg.m_stav = this.m_stav; 
    tg.m_Typ = this.m_Typ; 
    tg.m_datum = this.m_datum; 

}

 
	}

}