namespace BizAgi.EntityManager.Entities {

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Vision.Defs.OracleSpecific;
using BizAgi.PAL;
using BizAgi.PAL.Util;
using BizAgi.PAL.BeanUtils;
using BizAgi.EntityManager.Persistence;
using BizAgi.PAL.historylog;
using BizAgi.Resources;

public sealed class App : BaseEntity {


	public App() {
	}

	public override int getIdEnt() {
		return 10000;
	}

	public override String getEntityName() {
		return  "App";
	}

   public override String getClassName() {
      return "BizAgi.EntityManager.Entities.App";
   }

	String surrogateKey = "idCase";

	public override String getSurrogateKey() {
		return surrogateKey;
	}

	public override bool isVirtualEntity() {
		return false;
	}

	public override Dictionary<string, object> getVirtualBusinessKey() {
		return new Dictionary<string, object>() {  };
	}

	public override bool canCreateNewInstances() {
		return true;
	}

    static Dictionary<string, Func<App, object>> gets = new Dictionary<string, Func<App, object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Func<App, object>((App be) => be.getId() ) }, { "idcase", new Func<App, object>((App be) => be.getIdCase() ) },
        { "Podaniesanosti", new Func<App, object>((App be) => be.getPodaniesanosti() )         },
        { "Podanistinosti", new Func<App, object>((App be) => be.getPodanistinosti() )         }    };

    static Dictionary<string, Action<App,object>> sets = new Dictionary<string, Action<App,object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Action<App,object>((App be, object newValue ) => be.setId( (int)newValue) ) }, { "idcase", new Action<App, object>((App be, object newValue) => be.setIdCase((int)newValue) ) },
        { "Podaniesanosti", new Action<App, object>((App be, object newValue) => { if(newValue==null)be.setPodaniesanosti(null); else be.setPodaniesanosti((Podaniesanosti) newValue ); })         },
        { "Podanistinosti", new Action<App, object>((App be, object newValue) => { if(newValue==null)be.setPodanistinosti(null); else be.setPodanistinosti((Podanistinosti) newValue ); })         }    };

    static Dictionary<string, string> _factToParentAttribute = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)    {
    };
	int? m_idCase ;

	public int? getIdCase() {
		return m_idCase;
	}

	public void setIdCase(int? paramidCase){
		this.m_idCase = paramidCase;
	}

	public String getDisplayAttrib() {
		return null;
	}

	int? m_entityType = new int?(0);

	public override int? getEntityType()  {
		return m_entityType;
	}

	Podaniesanosti m_Podaniesanosti ;

	public Podaniesanosti getPodaniesanosti() {
        if ((!this.isPersisting()) && (m_Podaniesanosti != null) && (m_Podaniesanosti.isLoaded() ) ) { 
            m_Podaniesanosti = (Podaniesanosti)getPersistenceManager().find(m_Podaniesanosti.getClassName(), m_Podaniesanosti.getId());
        } else if ((!this.isPersisting()) && (m_Podaniesanosti != null)) { 
            m_Podaniesanosti = (Podaniesanosti)ApplyScopeChangesToEntity(m_Podaniesanosti);
        }
		return m_Podaniesanosti;
	}

	public void setPodaniesanosti(Podaniesanosti paramPodaniesanosti){
		this.m_Podaniesanosti = paramPodaniesanosti;
	}

	Podanistinosti m_Podanistinosti ;

	public Podanistinosti getPodanistinosti() {
        if ((!this.isPersisting()) && (m_Podanistinosti != null) && (m_Podanistinosti.isLoaded() ) ) { 
            m_Podanistinosti = (Podanistinosti)getPersistenceManager().find(m_Podanistinosti.getClassName(), m_Podanistinosti.getId());
        } else if ((!this.isPersisting()) && (m_Podanistinosti != null)) { 
            m_Podanistinosti = (Podanistinosti)ApplyScopeChangesToEntity(m_Podanistinosti);
        }
		return m_Podanistinosti;
	}

	public void setPodanistinosti(Podanistinosti paramPodanistinosti){
		this.m_Podanistinosti = paramPodanistinosti;
	}

   public override object getXPath(PropertyTokenizer xPathTokenizer) {
       String completeXPath = xPathTokenizer.getCurrentPath();
               return BizAgi.PAL.XPath.XPathUtil.evaluateXPath(this, completeXPath);
   }

    public override IBaseEntity addRelationWithId(String path, int id, bool createBackRelationScopeAction, bool createHistoryLogActions, bool isAttach = true) {
        try {
        BaseEntity ret = null;
        String attrRelated = null;
        bool bIsMxMFact = false;


        if (ret == null) {
            throw new BABeanUtilsException("Path not found. Path:" + path);
        } else {
            ret.setHistoryLog(getHistoryLog());
            ret.setPersistenceManager(getPersistenceManager());
            if (!bIsMxMFact) {
				if (createBackRelationScopeAction) {
					ret.setXPath(attrRelated, this);
				} else {
					PropertyUtils.setProperty(ret, attrRelated, this);
				}
            }
            return ret;
        }
        } catch (Exception e) {
           throw new BABeanUtilsException(e);
        }
    }

	public override IBaseEntity addRelation(String path, int id) {
		BaseEntity ret = (BaseEntity)addRelationWithId(path, id, true, true);
       ScopeAction action;
       if (getEntityType().Value == 0) {// PV Entity
           action = ScopeActionFactory.getAddRelationAction(ret.getId(), path);
       } else {
           action = ScopeActionFactory.getAddRelationEntityAction(this.getClassName(), this.getId(), ret.getId(), path);
       }
       getHistoryLog().add(action);
       return ret;
	}

	public override String toXML()  {
		StringBuilder xml = new StringBuilder();
		return xml.ToString();
	}
	public override Object getLocalizedValue(String attributeName) {
		Object attributeValue = this.getXPath(attributeName);		
		return attributeValue;
	}
	public override bool containsAttribute(string propertyName) {
		return gets.ContainsKey(propertyName); 
	}
	protected override Object GetPropertyValueByName(string propertyName) {
		return gets[propertyName].Invoke(this); 
	}
	public override void SetPropertyValue(string propertyName, object newValue) {
	    if(newValue == DBNull.Value) 
	        newValue = null; 
	    try {
	        sets[propertyName].Invoke(this, newValue); 
	    } catch (Exception e) {
	        throw new Exception(string.Format("Error trying to set value for Attribute {0} in Entity {1}", propertyName, "App"), e); 
	    } 
	}
	public override string GetFactOwnerAttribute(string factKey) {
		return _factToParentAttribute[factKey]; 
	}
	public override bool ExistsFactOwnerAttribute(string factKey) {
		return _factToParentAttribute.ContainsKey(factKey); 
	}
	protected override void  cloneValues(object target, Hashtable reentrance){

    App tg = (App) target; 
    cloneBaseValues(tg);
    tg.m_Podaniesanosti = (Podaniesanosti)cloneRelation(this.m_Podaniesanosti); 
    tg.m_Podanistinosti = (Podanistinosti)cloneRelation(this.m_Podanistinosti); 

}

 
	}

}