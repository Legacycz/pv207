namespace BizAgi.EntityManager.Entities {

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Vision.Defs.OracleSpecific;
using BizAgi.PAL;
using BizAgi.PAL.Util;
using BizAgi.PAL.BeanUtils;
using BizAgi.EntityManager.Persistence;
using BizAgi.PAL.historylog;
using BizAgi.Resources;

public sealed class Podanistinosti : BaseEntity {


	public Podanistinosti() {
	}

	public override int getIdEnt() {
		return 10002;
	}

	public override String getEntityName() {
		return  "Podanistinosti";
	}

   public override String getClassName() {
      return "BizAgi.EntityManager.Entities.Podanistinosti";
   }

	String surrogateKey = "idPodanistinosti";

	public override String getSurrogateKey() {
		return surrogateKey;
	}

	public override bool isVirtualEntity() {
		return false;
	}

	public override Dictionary<string, object> getVirtualBusinessKey() {
		return new Dictionary<string, object>() {  };
	}

	public override bool canCreateNewInstances() {
		return true;
	}

    static Dictionary<string, Func<Podanistinosti, object>> gets = new Dictionary<string, Func<Podanistinosti, object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Func<Podanistinosti, object>((Podanistinosti be) => be.getId() ) },
        { "TypStiznosti", new Func<Podanistinosti, object>((Podanistinosti be) => be.getTypStiznosti() )         },
        { "DatumUdalosti", new Func<Podanistinosti, object>((Podanistinosti be) => be.getDatumUdalosti() )         },
        { "Stav", new Func<Podanistinosti, object>((Podanistinosti be) => be.getStav() )         },
        { "PopisStinosti", new Func<Podanistinosti, object>((Podanistinosti be) => be.getPopisStinosti() )         },
        { "PracovniZaleitost", new Func<Podanistinosti, object>((Podanistinosti be) => be.getPracovniZaleitost() )         },
        { "IdentifikovaniZodpovedni", new Func<Podanistinosti, object>((Podanistinosti be) => be.getIdentifikovaniZodpovedni() )         },
        { "OpakovanyProblem", new Func<Podanistinosti, object>((Podanistinosti be) => be.getOpakovanyProblem() )         },
        { "OpodstatnenyProblem", new Func<Podanistinosti, object>((Podanistinosti be) => be.getOpodstatnenyProblem() )         },
        { "VyhovujiciReseni", new Func<Podanistinosti, object>((Podanistinosti be) => be.getVyhovujiciReseni() )         }    };

    static Dictionary<string, Action<Podanistinosti,object>> sets = new Dictionary<string, Action<Podanistinosti,object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Action<Podanistinosti,object>((Podanistinosti be, object newValue ) => be.setId( (int)newValue) ) },
        { "TypStiznosti", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setTypStiznosti(null); else be.setTypStiznosti((TypStinosti) newValue ); })         },
        { "DatumUdalosti", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setDatumUdalosti(null); else be.setDatumUdalosti(Convert.ToDateTime(newValue)); })         },
        { "Stav", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setStav(null); else be.setStav((StavStinosti) newValue ); })         },
        { "PopisStinosti", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setPopisStinosti(null); else be.setPopisStinosti((string) newValue ); })         },
        { "PracovniZaleitost", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setPracovniZaleitost(null); else be.setPracovniZaleitost(Convert.ToBoolean(newValue)); })         },
        { "IdentifikovaniZodpovedni", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setIdentifikovaniZodpovedni(null); else be.setIdentifikovaniZodpovedni((string) newValue ); })         },
        { "OpakovanyProblem", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setOpakovanyProblem(null); else be.setOpakovanyProblem(Convert.ToBoolean(newValue)); })         },
        { "OpodstatnenyProblem", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setOpodstatnenyProblem(null); else be.setOpodstatnenyProblem(Convert.ToBoolean(newValue)); })         },
        { "VyhovujiciReseni", new Action<Podanistinosti, object>((Podanistinosti be, object newValue) => { if(newValue==null)be.setVyhovujiciReseni(null); else be.setVyhovujiciReseni(Convert.ToBoolean(newValue)); })         }    };

    static Dictionary<string, string> _factToParentAttribute = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)    {
    };
	public String getDisplayAttrib() {
		return null;
	}

	int? m_entityType = new int?(1);

	public override int? getEntityType()  {
		return m_entityType;
	}

	TypStinosti m_typStiznosti ;

	public TypStinosti getTypStiznosti() {
		if ((!this.isPersisting()) && (m_typStiznosti != null) ) { 
			m_typStiznosti = (TypStinosti)getPersistenceManager().find(m_typStiznosti.getClassName(), m_typStiznosti.getId());
		}
		return m_typStiznosti;
	}

	public void setTypStiznosti(TypStinosti paramtypStiznosti){
		this.m_typStiznosti = paramtypStiznosti;
	}

	DateTime? m_datumUdalosti ;

	public DateTime? getDatumUdalosti() {
		return m_datumUdalosti;
	}

	public void setDatumUdalosti(DateTime? paramdatumUdalosti){
		this.m_datumUdalosti = paramdatumUdalosti;
	}

	StavStinosti m_stav ;

	public StavStinosti getStav() {
		if ((!this.isPersisting()) && (m_stav != null) ) { 
			m_stav = (StavStinosti)getPersistenceManager().find(m_stav.getClassName(), m_stav.getId());
		}
		return m_stav;
	}

	public void setStav(StavStinosti paramstav){
		this.m_stav = paramstav;
	}

	string m_popisStinosti ;

	public string getPopisStinosti() {
		return m_popisStinosti;
	}

	public void setPopisStinosti(string parampopisStinosti){
		this.m_popisStinosti = parampopisStinosti;
	}

	bool? m_PracovniZaleitost ;

	public bool? getPracovniZaleitost() {
		return m_PracovniZaleitost;
	}

	public void setPracovniZaleitost(bool? paramPracovniZaleitost){
		this.m_PracovniZaleitost = paramPracovniZaleitost;
	}

	string m_IdentifikovaniZodpovedni ;

	public string getIdentifikovaniZodpovedni() {
		return m_IdentifikovaniZodpovedni;
	}

	public void setIdentifikovaniZodpovedni(string paramIdentifikovaniZodpovedni){
		this.m_IdentifikovaniZodpovedni = paramIdentifikovaniZodpovedni;
	}

	bool? m_OpakovanyProblem ;

	public bool? getOpakovanyProblem() {
		return m_OpakovanyProblem;
	}

	public void setOpakovanyProblem(bool? paramOpakovanyProblem){
		this.m_OpakovanyProblem = paramOpakovanyProblem;
	}

	bool? m_OpodstatnenyProblem ;

	public bool? getOpodstatnenyProblem() {
		return m_OpodstatnenyProblem;
	}

	public void setOpodstatnenyProblem(bool? paramOpodstatnenyProblem){
		this.m_OpodstatnenyProblem = paramOpodstatnenyProblem;
	}

	bool? m_VyhovujiciReseni ;

	public bool? getVyhovujiciReseni() {
		return m_VyhovujiciReseni;
	}

	public void setVyhovujiciReseni(bool? paramVyhovujiciReseni){
		this.m_VyhovujiciReseni = paramVyhovujiciReseni;
	}

   public override object getXPath(PropertyTokenizer xPathTokenizer) {
       String completeXPath = xPathTokenizer.getCurrentPath();
               return BizAgi.PAL.XPath.XPathUtil.evaluateXPath(this, completeXPath);
   }

    public override IBaseEntity addRelationWithId(String path, int id, bool createBackRelationScopeAction, bool createHistoryLogActions, bool isAttach = true) {
        try {
        BaseEntity ret = null;
        String attrRelated = null;
        bool bIsMxMFact = false;


        if (ret == null) {
            throw new BABeanUtilsException("Path not found. Path:" + path);
        } else {
            ret.setHistoryLog(getHistoryLog());
            ret.setPersistenceManager(getPersistenceManager());
            if (!bIsMxMFact) {
				if (createBackRelationScopeAction) {
					ret.setXPath(attrRelated, this);
				} else {
					PropertyUtils.setProperty(ret, attrRelated, this);
				}
            }
            return ret;
        }
        } catch (Exception e) {
           throw new BABeanUtilsException(e);
        }
    }

	public override IBaseEntity addRelation(String path, int id) {
		BaseEntity ret = (BaseEntity)addRelationWithId(path, id, true, true);
       ScopeAction action;
       if (getEntityType().Value == 0) {// PV Entity
           action = ScopeActionFactory.getAddRelationAction(ret.getId(), path);
       } else {
           action = ScopeActionFactory.getAddRelationEntityAction(this.getClassName(), this.getId(), ret.getId(), path);
       }
       getHistoryLog().add(action);
       return ret;
	}

	public override String toXML()  {
		StringBuilder xml = new StringBuilder();
		return xml.ToString();
	}
	public override Object getLocalizedValue(String attributeName) {
		Object attributeValue = this.getXPath(attributeName);		
		return attributeValue;
	}
	public override bool containsAttribute(string propertyName) {
		return gets.ContainsKey(propertyName); 
	}
	protected override Object GetPropertyValueByName(string propertyName) {
		return gets[propertyName].Invoke(this); 
	}
	public override void SetPropertyValue(string propertyName, object newValue) {
	    if(newValue == DBNull.Value) 
	        newValue = null; 
	    try {
	        sets[propertyName].Invoke(this, newValue); 
	    } catch (Exception e) {
	        throw new Exception(string.Format("Error trying to set value for Attribute {0} in Entity {1}", propertyName, "Podanistinosti"), e); 
	    } 
	}
	public override string GetFactOwnerAttribute(string factKey) {
		return _factToParentAttribute[factKey]; 
	}
	public override bool ExistsFactOwnerAttribute(string factKey) {
		return _factToParentAttribute.ContainsKey(factKey); 
	}
	protected override void  cloneValues(object target, Hashtable reentrance){

    Podanistinosti tg = (Podanistinosti) target; 
    cloneBaseValues(tg);
    tg.m_typStiznosti = (TypStinosti)cloneRelation(this.m_typStiznosti); 
    tg.m_datumUdalosti = this.m_datumUdalosti; 
    tg.m_stav = (StavStinosti)cloneRelation(this.m_stav); 
    tg.m_popisStinosti = this.m_popisStinosti; 
    tg.m_PracovniZaleitost = this.m_PracovniZaleitost; 
    tg.m_IdentifikovaniZodpovedni = this.m_IdentifikovaniZodpovedni; 
    tg.m_OpakovanyProblem = this.m_OpakovanyProblem; 
    tg.m_OpodstatnenyProblem = this.m_OpodstatnenyProblem; 
    tg.m_VyhovujiciReseni = this.m_VyhovujiciReseni; 

}

 
	}

}