namespace BizAgi.EntityManager.Entities {

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Vision.Defs.OracleSpecific;
using BizAgi.PAL;
using BizAgi.PAL.Util;
using BizAgi.PAL.BeanUtils;
using BizAgi.EntityManager.Persistence;
using BizAgi.PAL.historylog;
using BizAgi.Resources;

public sealed class Zamestnanec : BaseEntity {


	public Zamestnanec() {
	}

	public override int getIdEnt() {
		return 10005;
	}

	public override String getEntityName() {
		return  "Zamestnanec";
	}

   public override String getClassName() {
      return "BizAgi.EntityManager.Entities.Zamestnanec";
   }

	String surrogateKey = "idZamestnanec";

	public override String getSurrogateKey() {
		return surrogateKey;
	}

	public override bool isVirtualEntity() {
		return false;
	}

	public override Dictionary<string, object> getVirtualBusinessKey() {
		return new Dictionary<string, object>() {  };
	}

	public override bool canCreateNewInstances() {
		return true;
	}

    static Dictionary<string, Func<Zamestnanec, object>> gets = new Dictionary<string, Func<Zamestnanec, object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Func<Zamestnanec, object>((Zamestnanec be) => be.getId() ) },
        { "Uivatel", new Func<Zamestnanec, object>((Zamestnanec be) => be.getUivatel() )         }    };

    static Dictionary<string, Action<Zamestnanec,object>> sets = new Dictionary<string, Action<Zamestnanec,object>>(StringComparer.OrdinalIgnoreCase)    {{ "id", new Action<Zamestnanec,object>((Zamestnanec be, object newValue ) => be.setId( (int)newValue) ) },
        { "Uivatel", new Action<Zamestnanec, object>((Zamestnanec be, object newValue) => { if(newValue==null)be.setUivatel(null); else be.setUivatel((WFUSER) newValue ); })         }    };

    static Dictionary<string, string> _factToParentAttribute = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)    {
    };
	public String getDisplayAttrib() {
		return null;
	}

	int? m_entityType = new int?(1);

	public override int? getEntityType()  {
		return m_entityType;
	}

	WFUSER m_Uivatel ;

	public WFUSER getUivatel() {
		if ((!this.isPersisting()) && (m_Uivatel != null) ) { 
			m_Uivatel = (WFUSER)getPersistenceManager().find(m_Uivatel.getClassName(), m_Uivatel.getId());
		}
		return m_Uivatel;
	}

	public void setUivatel(WFUSER paramUivatel){
		this.m_Uivatel = paramUivatel;
	}

   public override object getXPath(PropertyTokenizer xPathTokenizer) {
       String completeXPath = xPathTokenizer.getCurrentPath();
               return BizAgi.PAL.XPath.XPathUtil.evaluateXPath(this, completeXPath);
   }

    public override IBaseEntity addRelationWithId(String path, int id, bool createBackRelationScopeAction, bool createHistoryLogActions, bool isAttach = true) {
        try {
        BaseEntity ret = null;
        String attrRelated = null;
        bool bIsMxMFact = false;


        if (ret == null) {
            throw new BABeanUtilsException("Path not found. Path:" + path);
        } else {
            ret.setHistoryLog(getHistoryLog());
            ret.setPersistenceManager(getPersistenceManager());
            if (!bIsMxMFact) {
				if (createBackRelationScopeAction) {
					ret.setXPath(attrRelated, this);
				} else {
					PropertyUtils.setProperty(ret, attrRelated, this);
				}
            }
            return ret;
        }
        } catch (Exception e) {
           throw new BABeanUtilsException(e);
        }
    }

	public override IBaseEntity addRelation(String path, int id) {
		BaseEntity ret = (BaseEntity)addRelationWithId(path, id, true, true);
       ScopeAction action;
       if (getEntityType().Value == 0) {// PV Entity
           action = ScopeActionFactory.getAddRelationAction(ret.getId(), path);
       } else {
           action = ScopeActionFactory.getAddRelationEntityAction(this.getClassName(), this.getId(), ret.getId(), path);
       }
       getHistoryLog().add(action);
       return ret;
	}

	public override String toXML()  {
		StringBuilder xml = new StringBuilder();
		return xml.ToString();
	}
	public override Object getLocalizedValue(String attributeName) {
		Object attributeValue = this.getXPath(attributeName);		
		return attributeValue;
	}
	public override bool containsAttribute(string propertyName) {
		return gets.ContainsKey(propertyName); 
	}
	protected override Object GetPropertyValueByName(string propertyName) {
		return gets[propertyName].Invoke(this); 
	}
	public override void SetPropertyValue(string propertyName, object newValue) {
	    if(newValue == DBNull.Value) 
	        newValue = null; 
	    try {
	        sets[propertyName].Invoke(this, newValue); 
	    } catch (Exception e) {
	        throw new Exception(string.Format("Error trying to set value for Attribute {0} in Entity {1}", propertyName, "Zamestnanec"), e); 
	    } 
	}
	public override string GetFactOwnerAttribute(string factKey) {
		return _factToParentAttribute[factKey]; 
	}
	public override bool ExistsFactOwnerAttribute(string factKey) {
		return _factToParentAttribute.ContainsKey(factKey); 
	}
	protected override void  cloneValues(object target, Hashtable reentrance){

    Zamestnanec tg = (Zamestnanec) target; 
    cloneBaseValues(tg);
    tg.m_Uivatel = (WFUSER)cloneRelation(this.m_Uivatel); 

}

 
	}

}