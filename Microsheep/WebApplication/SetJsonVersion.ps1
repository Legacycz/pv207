﻿param
(
    [parameter(Mandatory=$true)] [string]$sourceDir,
    [parameter(Mandatory=$true)] [string]$platform

)

$script:ErrorActionPreference = 'Stop';

$sourceFile = "$sourceDir\..\References\$platform\Vision.Defs.dll";
$targetFile = "$sourceDir\jquery\version.json.txt";

$fileVersion = [System.Diagnostics.FileVersionInfo]::GetVersionInfo($sourceFile).FileVersion;

$buildText = '"build": "' + $($fileVersion) + '"';

Set-ItemProperty $targetFile -name IsReadOnly -value $false
Set-Content -path $targetFile -value "{", $buildText, "}";
Set-ItemProperty $targetFile -name IsReadOnly -value $true