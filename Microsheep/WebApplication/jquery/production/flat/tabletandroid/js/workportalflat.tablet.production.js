/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\base\js\lib\pouchdb-nightly.js */ 
// pouchdb.nightly - 2013-11-15T17:22:49

(function() {
 // BEGIN Math.uuid.js

/*!
Math.uuid.js (v1.4)
http://www.broofa.com
mailto:robert@broofa.com

Copyright (c) 2010 Robert Kieffer
Dual licensed under the MIT and GPL licenses.
*/

/*
 * Generate a random uuid.
 *
 * USAGE: Math.uuid(length, radix)
 *   length - the desired number of characters
 *   radix  - the number of allowable values for each character.
 *
 * EXAMPLES:
 *   // No arguments  - returns RFC4122, version 4 ID
 *   >>> Math.uuid()
 *   "92329D39-6F5C-4520-ABFC-AAB64544E172"
 *
 *   // One argument - returns ID of the specified length
 *   >>> Math.uuid(15)     // 15 character ID (default base=62)
 *   "VcydxgltxrVZSTV"
 *
 *   // Two arguments - returns ID of the specified length, and radix. (Radix must be <= 62)
 *   >>> Math.uuid(8, 2)  // 8 character ID (base=2)
 *   "01001010"
 *   >>> Math.uuid(8, 10) // 8 character ID (base=10)
 *   "47473046"
 *   >>> Math.uuid(8, 16) // 8 character ID (base=16)
 *   "098F4D35"
 */
var uuid;

(function() {

  var CHARS = (
    '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
    'abcdefghijklmnopqrstuvwxyz'
    ).split('');

  uuid = function uuid_inner(len, radix) {
    var chars = CHARS;
    var uuidInner = [];
    var i;

    radix = radix || chars.length;

    if (len) {
      // Compact form
      for (i = 0; i < len; i++) uuidInner[i] = chars[0 | Math.random()*radix];
    } else {
      // rfc4122, version 4 form
      var r;

      // rfc4122 requires these characters
      uuidInner[8] = uuidInner[13] = uuidInner[18] = uuidInner[23] = '-';
      uuidInner[14] = '4';

      // Fill in random data.  At i==19 set the high bits of clock sequence as
      // per rfc4122, sec. 4.1.5
      for (i = 0; i < 36; i++) {
        if (!uuidInner[i]) {
          r = 0 | Math.random()*16;
          uuidInner[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }

    return uuidInner.join('');
  };

})();

if (typeof module !== 'undefined' && module.exports) {
  module.exports = uuid;
}

/**
*
*  MD5 (Message-Digest Algorithm)
*
*  For original source see http://www.webtoolkit.info/
*  Download: 15.02.2009 from http://www.webtoolkit.info/javascript-md5.html
*
*  Licensed under CC-BY 2.0 License
*  (http://creativecommons.org/licenses/by/2.0/uk/)
*
**/

var Crypto = {};

(function() {
  Crypto.MD5 = function(string) {

    function RotateLeft(lValue, iShiftBits) {
      return (lValue<<iShiftBits) | (lValue>>>(32-iShiftBits));
    }

    function AddUnsigned(lX,lY) {
      var lX4,lY4,lX8,lY8,lResult;
      lX8 = (lX & 0x80000000);
      lY8 = (lY & 0x80000000);
      lX4 = (lX & 0x40000000);
      lY4 = (lY & 0x40000000);
      lResult = (lX & 0x3FFFFFFF)+(lY & 0x3FFFFFFF);
      if (lX4 & lY4) {
        return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
      }
      if (lX4 | lY4) {
        if (lResult & 0x40000000) {
          return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
        } else {
          return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
        }
      } else {
        return (lResult ^ lX8 ^ lY8);
      }
    }

    function F(x,y,z) { return (x & y) | ((~x) & z); }
    function G(x,y,z) { return (x & z) | (y & (~z)); }
    function H(x,y,z) { return (x ^ y ^ z); }
    function I(x,y,z) { return (y ^ (x | (~z))); }

    function FF(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
    };

    function GG(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
    };

    function HH(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
    };

    function II(a,b,c,d,x,s,ac) {
      a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
      return AddUnsigned(RotateLeft(a, s), b);
    };

    function ConvertToWordArray(string) {
      var lWordCount;
      var lMessageLength = string.length;
      var lNumberOfWords_temp1=lMessageLength + 8;
      var lNumberOfWords_temp2=(lNumberOfWords_temp1-(lNumberOfWords_temp1 % 64))/64;
      var lNumberOfWords = (lNumberOfWords_temp2+1)*16;
      var lWordArray=Array(lNumberOfWords-1);
      var lBytePosition = 0;
      var lByteCount = 0;
      while ( lByteCount < lMessageLength ) {
        lWordCount = (lByteCount-(lByteCount % 4))/4;
        lBytePosition = (lByteCount % 4)*8;
        lWordArray[lWordCount] = (lWordArray[lWordCount] | (string.charCodeAt(lByteCount)<<lBytePosition));
        lByteCount++;
      }
      lWordCount = (lByteCount-(lByteCount % 4))/4;
      lBytePosition = (lByteCount % 4)*8;
      lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80<<lBytePosition);
      lWordArray[lNumberOfWords-2] = lMessageLength<<3;
      lWordArray[lNumberOfWords-1] = lMessageLength>>>29;
      return lWordArray;
    };

    function WordToHex(lValue) {
      var WordToHexValue="",WordToHexValue_temp="",lByte,lCount;
      for (lCount = 0;lCount<=3;lCount++) {
        lByte = (lValue>>>(lCount*8)) & 255;
        WordToHexValue_temp = "0" + lByte.toString(16);
        WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length-2,2);
      }
      return WordToHexValue;
    };

    //**	function Utf8Encode(string) removed. Aready defined in pidcrypt_utils.js

    var x=Array();
    var k,AA,BB,CC,DD,a,b,c,d;
    var S11=7, S12=12, S13=17, S14=22;
    var S21=5, S22=9 , S23=14, S24=20;
    var S31=4, S32=11, S33=16, S34=23;
    var S41=6, S42=10, S43=15, S44=21;

    //	string = Utf8Encode(string); #function call removed

    x = ConvertToWordArray(string);

    a = 0x67452301; b = 0xEFCDAB89; c = 0x98BADCFE; d = 0x10325476;

    for (k=0;k<x.length;k+=16) {
      AA=a; BB=b; CC=c; DD=d;
      a=FF(a,b,c,d,x[k+0], S11,0xD76AA478);
      d=FF(d,a,b,c,x[k+1], S12,0xE8C7B756);
      c=FF(c,d,a,b,x[k+2], S13,0x242070DB);
      b=FF(b,c,d,a,x[k+3], S14,0xC1BDCEEE);
      a=FF(a,b,c,d,x[k+4], S11,0xF57C0FAF);
      d=FF(d,a,b,c,x[k+5], S12,0x4787C62A);
      c=FF(c,d,a,b,x[k+6], S13,0xA8304613);
      b=FF(b,c,d,a,x[k+7], S14,0xFD469501);
      a=FF(a,b,c,d,x[k+8], S11,0x698098D8);
      d=FF(d,a,b,c,x[k+9], S12,0x8B44F7AF);
      c=FF(c,d,a,b,x[k+10],S13,0xFFFF5BB1);
      b=FF(b,c,d,a,x[k+11],S14,0x895CD7BE);
      a=FF(a,b,c,d,x[k+12],S11,0x6B901122);
      d=FF(d,a,b,c,x[k+13],S12,0xFD987193);
      c=FF(c,d,a,b,x[k+14],S13,0xA679438E);
      b=FF(b,c,d,a,x[k+15],S14,0x49B40821);
      a=GG(a,b,c,d,x[k+1], S21,0xF61E2562);
      d=GG(d,a,b,c,x[k+6], S22,0xC040B340);
      c=GG(c,d,a,b,x[k+11],S23,0x265E5A51);
      b=GG(b,c,d,a,x[k+0], S24,0xE9B6C7AA);
      a=GG(a,b,c,d,x[k+5], S21,0xD62F105D);
      d=GG(d,a,b,c,x[k+10],S22,0x2441453);
      c=GG(c,d,a,b,x[k+15],S23,0xD8A1E681);
      b=GG(b,c,d,a,x[k+4], S24,0xE7D3FBC8);
      a=GG(a,b,c,d,x[k+9], S21,0x21E1CDE6);
      d=GG(d,a,b,c,x[k+14],S22,0xC33707D6);
      c=GG(c,d,a,b,x[k+3], S23,0xF4D50D87);
      b=GG(b,c,d,a,x[k+8], S24,0x455A14ED);
      a=GG(a,b,c,d,x[k+13],S21,0xA9E3E905);
      d=GG(d,a,b,c,x[k+2], S22,0xFCEFA3F8);
      c=GG(c,d,a,b,x[k+7], S23,0x676F02D9);
      b=GG(b,c,d,a,x[k+12],S24,0x8D2A4C8A);
      a=HH(a,b,c,d,x[k+5], S31,0xFFFA3942);
      d=HH(d,a,b,c,x[k+8], S32,0x8771F681);
      c=HH(c,d,a,b,x[k+11],S33,0x6D9D6122);
      b=HH(b,c,d,a,x[k+14],S34,0xFDE5380C);
      a=HH(a,b,c,d,x[k+1], S31,0xA4BEEA44);
      d=HH(d,a,b,c,x[k+4], S32,0x4BDECFA9);
      c=HH(c,d,a,b,x[k+7], S33,0xF6BB4B60);
      b=HH(b,c,d,a,x[k+10],S34,0xBEBFBC70);
      a=HH(a,b,c,d,x[k+13],S31,0x289B7EC6);
      d=HH(d,a,b,c,x[k+0], S32,0xEAA127FA);
      c=HH(c,d,a,b,x[k+3], S33,0xD4EF3085);
      b=HH(b,c,d,a,x[k+6], S34,0x4881D05);
      a=HH(a,b,c,d,x[k+9], S31,0xD9D4D039);
      d=HH(d,a,b,c,x[k+12],S32,0xE6DB99E5);
      c=HH(c,d,a,b,x[k+15],S33,0x1FA27CF8);
      b=HH(b,c,d,a,x[k+2], S34,0xC4AC5665);
      a=II(a,b,c,d,x[k+0], S41,0xF4292244);
      d=II(d,a,b,c,x[k+7], S42,0x432AFF97);
      c=II(c,d,a,b,x[k+14],S43,0xAB9423A7);
      b=II(b,c,d,a,x[k+5], S44,0xFC93A039);
      a=II(a,b,c,d,x[k+12],S41,0x655B59C3);
      d=II(d,a,b,c,x[k+3], S42,0x8F0CCC92);
      c=II(c,d,a,b,x[k+10],S43,0xFFEFF47D);
      b=II(b,c,d,a,x[k+1], S44,0x85845DD1);
      a=II(a,b,c,d,x[k+8], S41,0x6FA87E4F);
      d=II(d,a,b,c,x[k+15],S42,0xFE2CE6E0);
      c=II(c,d,a,b,x[k+6], S43,0xA3014314);
      b=II(b,c,d,a,x[k+13],S44,0x4E0811A1);
      a=II(a,b,c,d,x[k+4], S41,0xF7537E82);
      d=II(d,a,b,c,x[k+11],S42,0xBD3AF235);
      c=II(c,d,a,b,x[k+2], S43,0x2AD7D2BB);
      b=II(b,c,d,a,x[k+9], S44,0xEB86D391);
      a=AddUnsigned(a,AA);
      b=AddUnsigned(b,BB);
      c=AddUnsigned(c,CC);
      d=AddUnsigned(d,DD);
    }
    var temp = WordToHex(a)+WordToHex(b)+WordToHex(c)+WordToHex(d);
    return temp.toLowerCase();
  }
})();

if (typeof module !== 'undefined' && module.exports) {
  module.exports = Crypto;
}
//Abstracts constructing a Blob object, so it also works in older
//browsers that don't support the native Blob constructor. (i.e.
//old QtWebKit versions, at least).
function createBlob(parts, properties) {
  parts = parts || [];
  properties = properties || {};
  try {
    return new Blob(parts, properties);
  } catch (e) {
    if (e.name !== "TypeError") {
      throw(e);
    }
    var BlobBuilder = window.BlobBuilder || window.MSBlobBuilder || window.MozBlobBuilder || window.WebKitBlobBuilder;
    var builder = new BlobBuilder();
    for (var i = 0; i < parts.length; i += 1) {
      builder.append(parts[i]);
    }
    return builder.getBlob(properties.type);
  }
};

if (typeof module !== 'undefined' && module.exports) {
  module.exports = createBlob;
}

//----------------------------------------------------------------------
//
// ECMAScript 5 Polyfills
//  from www.calocomrmen./polyfill/
//
//----------------------------------------------------------------------

//----------------------------------------------------------------------
// ES5 15.2 Object Objects
//----------------------------------------------------------------------



// ES 15.2.3.6 Object.defineProperty ( O, P, Attributes )
// Partial support for most common case - getters, setters, and values
(function() {
  if (!Object.defineProperty ||
      !(function () { try { Object.defineProperty({}, 'x', {}); return true; } catch (e) { return false; } } ())) {
    var orig = Object.defineProperty;
    Object.defineProperty = function (o, prop, desc) {
      "use strict";

      // In IE8 try built-in implementation for defining properties on DOM prototypes.
      if (orig) { try { return orig(o, prop, desc); } catch (e) {} }

      if (o !== Object(o)) { throw new TypeError("Object.defineProperty called on non-object"); }
      if (Object.prototype.__defineGetter__ && ('get' in desc)) {
        Object.prototype.__defineGetter__.call(o, prop, desc.get);
      }
      if (Object.prototype.__defineSetter__ && ('set' in desc)) {
        Object.prototype.__defineSetter__.call(o, prop, desc.set);
      }
      if ('value' in desc) {
        o[prop] = desc.value;
      }
      return o;
    };
  }
}());



// ES5 15.2.3.14 Object.keys ( O )
// https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Object/keys
if (!Object.keys) {
  Object.keys = function (o) {
    if (o !== Object(o)) { throw new TypeError('Object.keys called on non-object'); }
    var ret = [], p;
    for (p in o) {
      if (Object.prototype.hasOwnProperty.call(o, p)) {
        ret.push(p);
      }
    }
    return ret;
  };
}

//----------------------------------------------------------------------
// ES5 15.4 Array Objects
//----------------------------------------------------------------------



// ES5 15.4.4.18 Array.prototype.forEach ( callbackfn [ , thisArg ] )
// From https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/forEach
if (!Array.prototype.forEach) {
  Array.prototype.forEach = function (fun /*, thisp */) {
    "use strict";

    if (this === void 0 || this === null) { throw new TypeError(); }

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== "function") { throw new TypeError(); }

    var thisp = arguments[1], i;
    for (i = 0; i < len; i++) {
      if (i in t) {
        fun.call(thisp, t[i], i, t);
      }
    }
  };
}


// ES5 15.4.4.19 Array.prototype.map ( callbackfn [ , thisArg ] )
// From https://developer.mozilla.org/en/JavaScript/Reference/Global_Objects/Array/Map
if (!Array.prototype.map) {
  Array.prototype.map = function (fun /*, thisp */) {
    "use strict";

    if (this === void 0 || this === null) { throw new TypeError(); }

    var t = Object(this);
    var len = t.length >>> 0;
    if (typeof fun !== "function") { throw new TypeError(); }

    var res = []; res.length = len;
    var thisp = arguments[1], i;
    for (i = 0; i < len; i++) {
      if (i in t) {
        res[i] = fun.call(thisp, t[i], i, t);
      }
    }

    return res;
  };
}


// Extends method
// (taken from http://code.jquery.com/jquery-1.9.0.js)
// Populate the class2type map
var class2type = {};

var types = ["Boolean", "Number", "String", "Function", "Array", "Date", "RegExp", "Object", "Error"];
for (var i = 0; i < types.length; i++) {
  var typename = types[i];
  class2type[ "[object " + typename + "]" ] = typename.toLowerCase();
}

var core_toString = class2type.toString;
var core_hasOwn = class2type.hasOwnProperty;

var type = function(obj) {
  if (obj === null) {
    return String( obj );
  }
  return typeof obj === "object" || typeof obj === "function" ?
    class2type[core_toString.call(obj)] || "object" :
    typeof obj;
};

var isWindow = function(obj) {
  return obj !== null && obj === obj.window;
};

var isPlainObject = function( obj ) {
  // Must be an Object.
  // Because of IE, we also have to check the presence of the constructor property.
  // Make sure that DOM nodes and window objects don't pass through, as well
  if ( !obj || type(obj) !== "object" || obj.nodeType || isWindow( obj ) ) {
    return false;
  }

  try {
    // Not own constructor property must be Object
    if ( obj.constructor &&
      !core_hasOwn.call(obj, "constructor") &&
      !core_hasOwn.call(obj.constructor.prototype, "isPrototypeOf") ) {
      return false;
    }
  } catch ( e ) {
    // IE8,9 Will throw exceptions on certain host objects #9897
    return false;
  }

  // Own properties are enumerated firstly, so to speed up,
  // if last one is own, then all properties are own.

  var key;
  for ( key in obj ) {}

  return key === undefined || core_hasOwn.call( obj, key );
};

var isFunction = function(obj) {
  return type(obj) === "function";
};

var isArray = Array.isArray || function(obj) {
  return type(obj) === "array";
};

var extend = function() {
  var options, name, src, copy, copyIsArray, clone,
    target = arguments[0] || {},
    i = 1,
    length = arguments.length,
    deep = false;

  // Handle a deep copy situation
  if ( typeof target === "boolean" ) {
    deep = target;
    target = arguments[1] || {};
    // skip the boolean and the target
    i = 2;
  }

  // Handle case when target is a string or something (possible in deep copy)
  if ( typeof target !== "object" && !isFunction(target) ) {
    target = {};
  }

  // extend jQuery itself if only one argument is passed
  if ( length === i ) {
    target = this;
    --i;
  }

  for ( ; i < length; i++ ) {
    // Only deal with non-null/undefined values
    if ((options = arguments[ i ]) != null) {
      // Extend the base object
      for ( name in options ) {
        src = target[ name ];
        copy = options[ name ];

        // Prevent never-ending loop
        if ( target === copy ) {
          continue;
        }

        // Recurse if we're merging plain objects or arrays
        if ( deep && copy && ( isPlainObject(copy) || (copyIsArray = isArray(copy)) ) ) {
          if ( copyIsArray ) {
            copyIsArray = false;
            clone = src && isArray(src) ? src : [];

          } else {
            clone = src && isPlainObject(src) ? src : {};
          }

          // Never move original objects, clone them
          target[ name ] = extend( deep, clone, copy );

        // Don't bring in undefined values
        } else if ( copy !== undefined ) {
          if (!(isArray(options) && isFunction(copy))) {
            target[ name ] = copy;
          }
        }
      }
    }
  }

  // Return the modified object
  return target;
};

if (typeof module !== 'undefined' && module.exports) {
  module.exports = extend;
}

var request;
var extend;
var createBlob;

if (typeof module !== 'undefined' && module.exports) {
  request = require('request');
  extend = require('./extend.js');
  createBlob = require('./blob.js');
}

var ajax = function ajax(options, callback) {

  if (typeof options === "function") {
    callback = options;
    options = {};
  }

  var call = function(fun) {
    var args = Array.prototype.slice.call(arguments, 1);
    if (typeof fun === typeof Function) {
      fun.apply(this, args);
    }
  };

  var defaultOptions = {
    method : "GET",
    headers: {},
    json: true,
    processData: true,
    timeout: 10000
  };

  options = extend(true, defaultOptions, options);

  var onSuccess = function(obj, resp, cb){
    if (!options.binary && !options.json && options.processData &&
        typeof obj !== 'string') {
      obj = JSON.stringify(obj);
    } else if (!options.binary && options.json && typeof obj === 'string') {
      try {
        obj = JSON.parse(obj);
      } catch (e) {
        // Probably a malformed JSON from server
        call(cb, e);
        return;
      }
    }
    call(cb, null, obj, resp);
  };

  var onError = function(err, cb){
    var errParsed;
    var errObj = {status: err.status};
    try {
      errParsed = JSON.parse(err.responseText);
      //would prefer not to have a try/catch clause
      errObj = extend(true, {}, errObj, errParsed);
    } catch(e) {}
    call(cb, errObj);
  };

  if (typeof window !== 'undefined' && window.XMLHttpRequest) {
    var timer, timedout = false;
    var xhr = new XMLHttpRequest();

    xhr.open(options.method, options.url);
    xhr.withCredentials = true;

    if (options.json) {
      options.headers.Accept = 'application/json';
      options.headers['Content-Type'] = options.headers['Content-Type'] ||
        'application/json';
      if (options.body && options.processData && typeof options.body !== "string") {
        options.body = JSON.stringify(options.body);
      }
    }

    if (options.binary) {
      xhr.responseType = 'arraybuffer';
    }

    function createCookie(name,value,days) {
      if (days) {
	var date = new Date();
	date.setTime(date.getTime()+(days*24*60*60*1000));
	var expires = "; expires="+date.toGMTString();
      } else {
        var expires = "";
      }
      document.cookie = name+"="+value+expires+"; path=/";
    }

    for (var key in options.headers) {
      if (key === 'Cookie') {
        var cookie = options.headers[key].split('=');
        createCookie(cookie[0], cookie[1], 10);
      } else {
        xhr.setRequestHeader(key, options.headers[key]);
      }
    }

    if (!("body" in options)) {
      options.body = null;
    }

    var abortReq = function() {
      timedout=true;
      xhr.abort();
      call(onError, xhr, callback);
    };

    xhr.onreadystatechange = function() {
      if (xhr.readyState !== 4 || timedout) {
        return;
      }
      clearTimeout(timer);
      if (xhr.status >= 200 && xhr.status < 300) {
        var data;
        if (options.binary) {
          data = createBlob([xhr.response || ''], {
            type: xhr.getResponseHeader('Content-Type')
          });
        } else {
          data = xhr.responseText;
        }
        call(onSuccess, data, xhr, callback);
      } else {
         call(onError, xhr, callback);
      }
    };

    if (options.timeout > 0) {
      timer = setTimeout(abortReq, options.timeout);
    }
    xhr.send(options.body);
    return {abort:abortReq};

  } else {

    if (options.json) {
      if (!options.binary) {
        options.headers.Accept = 'application/json';
      }
      options.headers['Content-Type'] = options.headers['Content-Type'] ||
        'application/json';
    }

    if (options.binary) {
      options.encoding = null;
      options.json = false;
    }

    if (!options.processData) {
      options.json = false;
    }

    return request(options, function(err, response, body) {
      if (err) {
        err.status = response ? response.statusCode : 400;
        return call(onError, err, callback);
      }

      var content_type = response.headers['content-type'];
      var data = (body || '');

      // CouchDB doesn't always return the right content-type for JSON data, so
      // we check for ^{ and }$ (ignoring leading/trailing whitespace)
      if (!options.binary && (options.json || !options.processData) &&
          typeof data !== 'object' &&
          (/json/.test(content_type) ||
           (/^[\s]*\{/.test(data) && /\}[\s]*$/.test(data)))) {
        data = JSON.parse(data);
      }

      if (response.statusCode >= 200 && response.statusCode < 300) {
        call(onSuccess, data, response, callback);
      }
      else {
        if (options.binary) {
          data = JSON.parse(data.toString());
        }
        data.status = response.statusCode;
        call(callback, data);
      }
    });
  }
};

if (typeof module !== 'undefined' && module.exports) {
  module.exports = ajax;
}

/*globals PouchAdapter: true, PouchUtils: true */

"use strict";

var PouchUtils;

if (typeof module !== 'undefined' && module.exports) {
  PouchUtils = require('./pouch.utils.js');
}

var Pouch = function Pouch(name, opts, callback) {

  if (!(this instanceof Pouch)) {
    return new Pouch(name, opts, callback);
  }

  if (typeof opts === 'function' || typeof opts === 'undefined') {
    callback = opts;
    opts = {};
  }

  if (typeof name === 'object') {
    opts = name;
    name = undefined;
  }

  if (typeof callback === 'undefined') {
    callback = function() {};
  }

  var backend = Pouch.parseAdapter(opts.name || name);
  opts.originalName = name;
  opts.name = opts.name || backend.name;
  opts.adapter = opts.adapter || backend.adapter;

  if (!Pouch.adapters[opts.adapter]) {
    throw 'Adapter is missing';
  }

  if (!Pouch.adapters[opts.adapter].valid()) {
    throw 'Invalid Adapter';
  }

  var adapter = new PouchAdapter(opts, function(err, db) {
    if (err) {
      if (callback) {
        callback(err);
      }
      return;
    }

    for (var plugin in Pouch.plugins) {
      // In future these will likely need to be async to allow the plugin
      // to initialise
      var pluginObj = Pouch.plugins[plugin](db);
      for (var api in pluginObj) {
        // We let things like the http adapter use its own implementation
        // as it shares a lot of code
        if (!(api in db)) {
          db[api] = pluginObj[api];
        }
      }
    }
    db.taskqueue.ready(true);
    db.taskqueue.execute(db);
    callback(null, db);
  });
  for (var j in adapter) {
    this[j] = adapter[j];
  }
  for (var plugin in Pouch.plugins) {
    // In future these will likely need to be async to allow the plugin
    // to initialise
    var pluginObj = Pouch.plugins[plugin](this);
    for (var api in pluginObj) {
      // We let things like the http adapter use its own implementation
      // as it shares a lot of code
      if (!(api in this)) {
        this[api] = pluginObj[api];
      }
    }
  }
};

Pouch.DEBUG = false;
Pouch.openReqList = {};
Pouch.adapters = {};
Pouch.plugins = {};

Pouch.prefix = '_pouch_';

Pouch.parseAdapter = function(name) {
  var match = name.match(/([a-z\-]*):\/\/(.*)/);
  var adapter;
  if (match) {
    // the http adapter expects the fully qualified name
    name = /http(s?)/.test(match[1]) ? match[1] + '://' + match[2] : match[2];
    adapter = match[1];
    if (!Pouch.adapters[adapter].valid()) {
      throw 'Invalid adapter';
    }
    return {name: name, adapter: match[1]};
  }

  var preferredAdapters = ['idb', 'leveldb', 'websql'];
  for (var i = 0; i < preferredAdapters.length; ++i) {
    if (preferredAdapters[i] in Pouch.adapters) {
      adapter = Pouch.adapters[preferredAdapters[i]];
      var use_prefix = 'use_prefix' in adapter ? adapter.use_prefix : true;

      return {
        name: use_prefix ? Pouch.prefix + name : name,
        adapter: preferredAdapters[i]
      };
    }
  }

  throw 'No valid adapter found';
};

Pouch.destroy = function(name, opts, callback) {
  if (typeof opts === 'function' || typeof opts === 'undefined') {
    callback = opts;
    opts = {};
  }

  if (typeof name === 'object') {
    opts = name;
    name = undefined;
  }

  if (typeof callback === 'undefined') {
    callback = function() {};
  }
  var backend = Pouch.parseAdapter(opts.name || name);

  var cb = function(err, response) {
    if (err) {
      callback(err);
      return;
    }

    for (var plugin in Pouch.plugins) {
      Pouch.plugins[plugin]._delete(backend.name);
    }
    if (Pouch.DEBUG) {
      console.log(backend.name + ': Delete Database');
    }

    // call destroy method of the particular adaptor
    Pouch.adapters[backend.adapter].destroy(backend.name, opts, callback);
  };

  // remove Pouch from allDBs
  Pouch.removeFromAllDbs(backend, cb);
};

Pouch.removeFromAllDbs = function(opts, callback) {
  // Only execute function if flag is enabled
  if (!Pouch.enableAllDbs) {
    callback();
    return;
  }

  // skip http and https adaptors for allDbs
  var adapter = opts.adapter;
  if (adapter === "http" || adapter === "https") {
    callback();
    return;
  }

  // remove db from Pouch.ALL_DBS
  new Pouch(Pouch.allDBName(opts.adapter), function(err, db) {
    if (err) {
      // don't fail when allDbs fail
      console.error(err);
      callback();
      return;
    }
    // check if db has been registered in Pouch.ALL_DBS
    var dbname = Pouch.dbName(opts.adapter, opts.name);
    db.get(dbname, function(err, doc) {
      if (err) {
        callback();
      } else {
        db.remove(doc, function(err, response) {
          if (err) {
            console.error(err);
          }
          callback();
        });
      }
    });
  });

};

Pouch.adapter = function (id, obj) {
  if (obj.valid()) {
    Pouch.adapters[id] = obj;
  }
};

Pouch.plugin = function(id, obj) {
  Pouch.plugins[id] = obj;
};

// flag to toggle allDbs (off by default)
Pouch.enableAllDbs = false;

// name of database used to keep track of databases
Pouch.ALL_DBS = "_allDbs";
Pouch.dbName = function(adapter, name) {
  return [adapter, "-", name].join('');
};
Pouch.realDBName = function(adapter, name) {
  return [adapter, "://", name].join('');
};
Pouch.allDBName = function(adapter) {
  return [adapter, "://", Pouch.prefix + Pouch.ALL_DBS].join('');
};

Pouch.open = function(opts, callback) {
  // Only register pouch with allDbs if flag is enabled
  if (!Pouch.enableAllDbs) {
    callback();
    return;
  }

  var adapter = opts.adapter;
  // skip http and https adaptors for allDbs
  if (adapter === "http" || adapter === "https") {
    callback();
    return;
  }

  new Pouch(Pouch.allDBName(adapter), function(err, db) {
    if (err) {
      // don't fail when allDb registration fails
      console.error(err);
      callback();
      return;
    }

    // check if db has been registered in Pouch.ALL_DBS
    var dbname = Pouch.dbName(adapter, opts.name);
    db.get(dbname, function(err, response) {
      if (err && err.status === 404) {
        db.put({
          _id: dbname,
          dbname: opts.originalName
        }, function(err) {
            if (err) {
                console.error(err);
            }

            callback();
        });
      } else {
        callback();
      }
    });
  });
};

Pouch.allDbs = function(callback) {
  var accumulate = function(adapters, all_dbs) {
    if (adapters.length === 0) {
      // remove duplicates
      var result = [];
      all_dbs.forEach(function(doc) {
        var exists = result.some(function(db) {
          return db.id === doc.id;
        });

        if (!exists) {
          result.push(doc);
        }
      });

      // return an array of dbname
      callback(null, result.map(function(row) {
          return row.doc.dbname;
      }));
      return;
    }

    var adapter = adapters.shift();

    // skip http and https adaptors for allDbs
    if (adapter === "http" || adapter === "https") {
      accumulate(adapters, all_dbs);
      return;
    }

    new Pouch(Pouch.allDBName(adapter), function(err, db) {
      if (err) {
        callback(err);
        return;
      }
      db.allDocs({include_docs: true}, function(err, response) {
        if (err) {
          callback(err);
          return;
        }

        // append from current adapter rows
        all_dbs.unshift.apply(all_dbs, response.rows);

        // code to clear allDbs.
        // response.rows.forEach(function(row) {
        //   db.remove(row.doc, function() {
        //     console.log(arguments);
        //   });
        // });

        // recurse
        accumulate(adapters, all_dbs);
      });
    });
  };
  var adapters = Object.keys(Pouch.adapters);
  accumulate(adapters, []);
};

/*
  Examples:

  >>> Pouch.uuids()
  "92329D39-6F5C-4520-ABFC-AAB64544E172"]

  >>> Pouch.uuids(10, {length: 32, radix: 5})
  [ '04422200002240221333300140323100',
    '02304411022101001312440440020110',
    '41432430322114143303343433433030',
    '21234330022303431304443100330401',
    '23044133434242034101422131301213',
    '43142032223224403322031032232041',
    '41121132424023141101403324200330',
    '00341042023103204342124004122342',
    '01001141433040113422403034004214',
    '30221232324132303123433131020020' ]
 */
Pouch.uuids = function (count, options) {

  if (typeof(options) !== 'object') {
    options = {};
  }

  var length = options.length;
  var radix = options.radix;
  var uuids = [];

  while (uuids.push(PouchUtils.uuid(length, radix)) < count) { }

  return uuids;
};

// Give back one UUID
Pouch.uuid = function (options) {
  return Pouch.uuids(1, options)[0];
};

// Enumerate errors, add the status code so we can reflect the HTTP api
// in future
Pouch.Errors = {
  MISSING_BULK_DOCS: {
    status: 400,
    error: 'bad_request',
    reason: "Missing JSON list of 'docs'"
  },
  MISSING_DOC: {
    status: 404,
    error: 'not_found',
    reason: 'missing'
  },
  REV_CONFLICT: {
    status: 409,
    error: 'conflict',
    reason: 'Document update conflict'
  },
  INVALID_ID: {
    status: 400,
    error: 'invalid_id',
    reason: '_id field must contain a string'
  },
  MISSING_ID: {
    status: 412,
    error: 'missing_id',
    reason: '_id is required for puts'
  },
  RESERVED_ID: {
    status: 400,
    error: 'bad_request',
    reason: 'Only reserved document ids may start with underscore.'
  },
  NOT_OPEN: {
    status: 412,
    error: 'precondition_failed',
    reason: 'Database not open so cannot close'
  },
  UNKNOWN_ERROR: {
    status: 500,
    error: 'unknown_error',
    reason: 'Database encountered an unknown error'
  },
  BAD_ARG: {
    status: 500,
    error: 'badarg',
    reason: 'Some query argument is invalid'
  },
  INVALID_REQUEST: {
    status: 400,
    error: 'invalid_request',
    reason: 'Request was invalid'
  },
  QUERY_PARSE_ERROR: {
    status: 400,
    error: 'query_parse_error',
    reason: 'Some query parameter is invalid'
  },
  DOC_VALIDATION: {
    status: 500,
    error: 'doc_validation',
    reason: 'Bad special document member'
  },
  BAD_REQUEST: {
    status: 400,
    error: 'bad_request',
    reason: 'Something wrong with the request'
  },
  NOT_AN_OBJECT: {
    status: 400,
    error: 'bad_request',
    reason: 'Document must be a JSON object'
  },
  DB_MISSING: {
    status: 404,
    error: 'not_found',
    reason: 'Database not found'
  }
};

Pouch.error = function(error, reason) {
  return PouchUtils.extend({}, error, {reason: reason});
};

if (typeof module !== 'undefined' && module.exports) {
  global.Pouch = Pouch;
  global.PouchDB = Pouch;
  module.exports = Pouch;
  Pouch.replicate = require('./pouch.replicate.js').replicate;
  var PouchAdapter = require('./pouch.adapter.js');
  require('./adapters/pouch.http.js');
  require('./adapters/pouch.idb.js');
  require('./adapters/pouch.websql.js');
  require('./adapters/pouch.leveldb.js');
  require('./plugins/pouchdb.mapreduce.js');
} else {
  window.Pouch = Pouch;
  window.PouchDB = Pouch;
}

'use strict';

var pouchCollate = function(a, b) {
  var ai = collationIndex(a);
  var bi = collationIndex(b);
  if ((ai - bi) !== 0) {
    return ai - bi;
  }
  if (a === null) {
    return 0;
  }
  if (typeof a === 'number') {
    return a - b;
  }
  if (typeof a === 'boolean') {
    return a < b ? -1 : 1;
  }
  if (typeof a === 'string') {
    return stringCollate(a, b);
  }
  if (Array.isArray(a)) {
    return arrayCollate(a, b);
  }
  if (typeof a === 'object') {
    return objectCollate(a, b);
  }
};

var stringCollate = function(a, b) {
  // See: https://github.com/daleharvey/pouchdb/issues/40
  // This is incompatible with the CouchDB implementation, but its the
  // best we can do for now
  return (a === b) ? 0 : ((a > b) ? 1 : -1);
};

var objectCollate = function(a, b) {
  var ak = Object.keys(a), bk = Object.keys(b);
  var len = Math.min(ak.length, bk.length);
  for (var i = 0; i < len; i++) {
    // First sort the keys
    var sort = pouchCollate(ak[i], bk[i]);
    if (sort !== 0) {
      return sort;
    }
    // if the keys are equal sort the values
    sort = pouchCollate(a[ak[i]], b[bk[i]]);
    if (sort !== 0) {
      return sort;
    }

  }
  return (ak.length === bk.length) ? 0 :
    (ak.length > bk.length) ? 1 : -1;
};

var arrayCollate = function(a, b) {
  var len = Math.min(a.length, b.length);
  for (var i = 0; i < len; i++) {
    var sort = pouchCollate(a[i], b[i]);
    if (sort !== 0) {
      return sort;
    }
  }
  return (a.length === b.length) ? 0 :
    (a.length > b.length) ? 1 : -1;
};

// The collation is defined by erlangs ordered terms
// the atoms null, true, false come first, then numbers, strings,
// arrays, then objects
var collationIndex = function(x) {
  var id = ['boolean', 'number', 'string', 'object'];
  if (id.indexOf(typeof x) !== -1) {
    if (x === null) {
      return 1;
    }
    return id.indexOf(typeof x) + 2;
  }
  if (Array.isArray(x)) {
    return 4.5;
  }
};

// a few hacks to get things in the right place for node.js
if (typeof module !== 'undefined' && module.exports) {
  module.exports = pouchCollate;
}


'use strict';

var extend;
if (typeof module !== 'undefined' && module.exports) {
  extend = require('./deps/extend');
}


// for a better overview of what this is doing, read:
// https://github.com/apache/couchdb/blob/master/src/couchdb/couch_key_tree.erl
//
// But for a quick intro, CouchDB uses a revision tree to store a documents
// history, A -> B -> C, when a document has conflicts, that is a branch in the
// tree, A -> (B1 | B2 -> C), We store these as a nested array in the format
//
// KeyTree = [Path ... ]
// Path = {pos: position_from_root, ids: Tree}
// Tree = [Key, Opts, [Tree, ...]], in particular single node: [Key, []]

// Turn a path as a flat array into a tree with a single branch
function pathToTree(path) {
  var doc = path.shift();
  var root = [doc.id, doc.opts, []];
  var leaf = root;
  var nleaf;

  while (path.length) {
    doc = path.shift();
    nleaf = [doc.id, doc.opts, []];
    leaf[2].push(nleaf);
    leaf = nleaf;
  }
  return root;
}

// Merge two trees together
// The roots of tree1 and tree2 must be the same revision
function mergeTree(in_tree1, in_tree2) {
  var queue = [{tree1: in_tree1, tree2: in_tree2}];
  var conflicts = false;
  while (queue.length > 0) {
    var item = queue.pop();
    var tree1 = item.tree1;
    var tree2 = item.tree2;

    if (tree1[1].status || tree2[1].status) {
      tree1[1].status = (tree1[1].status ===  'available' ||
                         tree2[1].status === 'available') ? 'available' : 'missing';
    }

    for (var i = 0; i < tree2[2].length; i++) {
      if (!tree1[2][0]) {
        conflicts = 'new_leaf';
        tree1[2][0] = tree2[2][i];
        continue;
      }

      var merged = false;
      for (var j = 0; j < tree1[2].length; j++) {
        if (tree1[2][j][0] === tree2[2][i][0]) {
          queue.push({tree1: tree1[2][j], tree2: tree2[2][i]});
          merged = true;
        }
      }
      if (!merged) {
        conflicts = 'new_branch';
        tree1[2].push(tree2[2][i]);
        tree1[2].sort();
      }
    }
  }
  return {conflicts: conflicts, tree: in_tree1};
}

function doMerge(tree, path, dontExpand) {
  var restree = [];
  var conflicts = false;
  var merged = false;
  var res, branch;

  if (!tree.length) {
    return {tree: [path], conflicts: 'new_leaf'};
  }

  tree.forEach(function(branch) {
    if (branch.pos === path.pos && branch.ids[0] === path.ids[0]) {
      // Paths start at the same position and have the same root, so they need
      // merged
      res = mergeTree(branch.ids, path.ids);
      restree.push({pos: branch.pos, ids: res.tree});
      conflicts = conflicts || res.conflicts;
      merged = true;
    } else if (dontExpand !== true) {
      // The paths start at a different position, take the earliest path and
      // traverse up until it as at the same point from root as the path we want to
      // merge.  If the keys match we return the longer path with the other merged
      // After stemming we dont want to expand the trees

      var t1 = branch.pos < path.pos ? branch : path;
      var t2 = branch.pos < path.pos ? path : branch;
      var diff = t2.pos - t1.pos;

      var candidateParents = [];

      var trees = [];
      trees.push({ids: t1.ids, diff: diff, parent: null, parentIdx: null});
      while (trees.length > 0) {
        var item = trees.pop();
        if (item.diff === 0) {
          if (item.ids[0] === t2.ids[0]) {
            candidateParents.push(item);
          }
          continue;
        }
        if (!item.ids) {
          continue;
        }
        /*jshint loopfunc:true */
        item.ids[2].forEach(function(el, idx) {
          trees.push({ids: el, diff: item.diff-1, parent: item.ids, parentIdx: idx});
        });
      }

      var el = candidateParents[0];

      if (!el) {
        restree.push(branch);
      } else {
        res = mergeTree(el.ids, t2.ids);
        el.parent[2][el.parentIdx] = res.tree;
        restree.push({pos: t1.pos, ids: t1.ids});
        conflicts = conflicts || res.conflicts;
        merged = true;
      }
    } else {
      restree.push(branch);
    }
  });

  // We didnt find
  if (!merged) {
    restree.push(path);
  }

  restree.sort(function(a, b) {
    return a.pos - b.pos;
  });

  return {
    tree: restree,
    conflicts: conflicts || 'internal_node'
  };
}

// To ensure we dont grow the revision tree infinitely, we stem old revisions
function stem(tree, depth) {
  // First we break out the tree into a complete list of root to leaf paths,
  // we cut off the start of the path and generate a new set of flat trees
  var stemmedPaths = PouchMerge.rootToLeaf(tree).map(function(path) {
    var stemmed = path.ids.slice(-depth);
    return {
      pos: path.pos + (path.ids.length - stemmed.length),
      ids: pathToTree(stemmed)
    };
  });
  // Then we remerge all those flat trees together, ensuring that we dont
  // connect trees that would go beyond the depth limit
  return stemmedPaths.reduce(function(prev, current, i, arr) {
    return doMerge(prev, current, true).tree;
  }, [stemmedPaths.shift()]);
}

var PouchMerge = {};

PouchMerge.merge = function(tree, path, depth) {
  // Ugh, nicer way to not modify arguments in place?
  tree = extend(true, [], tree);
  path = extend(true, {}, path);
  var newTree = doMerge(tree, path);
  return {
    tree: stem(newTree.tree, depth),
    conflicts: newTree.conflicts
  };
};

// We fetch all leafs of the revision tree, and sort them based on tree length
// and whether they were deleted, undeleted documents with the longest revision
// tree (most edits) win
// The final sort algorithm is slightly documented in a sidebar here:
// http://guide.couchdb.org/draft/conflicts.html
PouchMerge.winningRev = function(metadata) {
  var leafs = [];
  PouchMerge.traverseRevTree(metadata.rev_tree,
                              function(isLeaf, pos, id, something, opts) {
    if (isLeaf) {
      leafs.push({pos: pos, id: id, deleted: !!opts.deleted});
    }
  });
  leafs.sort(function(a, b) {
    if (a.deleted !== b.deleted) {
      return a.deleted > b.deleted ? 1 : -1;
    }
    if (a.pos !== b.pos) {
      return b.pos - a.pos;
    }
    return a.id < b.id ? 1 : -1;
  });

  return leafs[0].pos + '-' + leafs[0].id;
};

// Pretty much all below can be combined into a higher order function to
// traverse revisions
// The return value from the callback will be passed as context to all
// children of that node
PouchMerge.traverseRevTree = function(revs, callback) {
  var toVisit = [];

  revs.forEach(function(tree) {
    toVisit.push({pos: tree.pos, ids: tree.ids});
  });
  while (toVisit.length > 0) {
    var node = toVisit.pop();
    var pos = node.pos;
    var tree = node.ids;
    var newCtx = callback(tree[2].length === 0, pos, tree[0], node.ctx, tree[1]);
    /*jshint loopfunc: true */
    tree[2].forEach(function(branch) {
      toVisit.push({pos: pos+1, ids: branch, ctx: newCtx});
    });
  }
};

PouchMerge.collectLeaves = function(revs) {
  var leaves = [];
  PouchMerge.traverseRevTree(revs, function(isLeaf, pos, id, acc, opts) {
    if (isLeaf) {
      leaves.unshift({rev: pos + "-" + id, pos: pos, opts: opts});
    }
  });
  leaves.sort(function(a, b) {
    return b.pos - a.pos;
  });
  leaves.map(function(leaf) { delete leaf.pos; });
  return leaves;
};

// returns revs of all conflicts that is leaves such that
// 1. are not deleted and
// 2. are different than winning revision
PouchMerge.collectConflicts = function(metadata) {
  var win = PouchMerge.winningRev(metadata);
  var leaves = PouchMerge.collectLeaves(metadata.rev_tree);
  var conflicts = [];
  leaves.forEach(function(leaf) {
    if (leaf.rev !== win && !leaf.opts.deleted) {
      conflicts.push(leaf.rev);
    }
  });
  return conflicts;
};

PouchMerge.rootToLeaf = function(tree) {
  var paths = [];
  PouchMerge.traverseRevTree(tree, function(isLeaf, pos, id, history, opts) {
    history = history ? history.slice(0) : [];
    history.push({id: id, opts: opts});
    if (isLeaf) {
      var rootPos = pos + 1 - history.length;
      paths.unshift({pos: rootPos, ids: history});
    }
    return history;
  });
  return paths;
};

// a few hacks to get things in the right place for node.js
if (typeof module !== 'undefined' && module.exports) {
  module.exports = PouchMerge;
}
/*globals PouchUtils: true */

'use strict';

var PouchUtils;

if (typeof module !== 'undefined' && module.exports) {
  module.exports = Pouch;
  PouchUtils = require('./pouch.utils.js');
}

// We create a basic promise so the caller can cancel the replication possibly
// before we have actually started listening to changes etc
var Promise = function() {
  var that = this;
  this.cancelled = false;
  this.cancel = function() {
    that.cancelled = true;
  };
};

// The RequestManager ensures that only one database request is active at
// at time, it ensures we dont max out simultaneous HTTP requests and makes
// the replication process easier to reason about
var RequestManager = function(promise) {

  var queue = [];
  var api = {};
  var processing = false;

  // Add a new request to the queue, if we arent currently processing anything
  // then process it immediately
  api.enqueue = function(fun, args) {
    queue.push({fun: fun, args: args});
    if (!processing) {
      api.process();
    }
  };

  // Process the next request
  api.process = function() {
    if (processing || !queue.length || promise.cancelled) {
      return;
    }
    processing = true;
    var task = queue.shift();
    task.fun.apply(null, task.args);
  };

  // We need to be notified whenever a request is complete to process
  // the next request
  api.notifyRequestComplete = function() {
    processing = false;
    api.process();
  };

  return api;
};

// TODO: check CouchDB's replication id generation, generate a unique id particular
// to this replication
var genReplicationId = function(src, target, opts) {
  var filterFun = opts.filter ? opts.filter.toString() : '';
  return '_local/' + PouchUtils.Crypto.MD5(src.id() + target.id() + filterFun);
};

// A checkpoint lets us restart replications from when they were last cancelled
var fetchCheckpoint = function(src, target, id, callback) {
  target.get(id, function(err, targetDoc) {
    if (err && err.status === 404) {
      callback(null, 0);
    } else {
      src.get(id, function(err, sourceDoc) {
        if (err && err.status === 404 || targetDoc.last_seq !== sourceDoc.last_seq) {
          callback(null, 0);
        } else {
          callback(null, sourceDoc.last_seq);
        }
      });
    }
  });
};

var writeCheckpoint = function(src, target, id, checkpoint, callback) {
  var updateCheckpoint = function (db, callback) {
    db.get(id, function(err, doc) {
      if (err && err.status === 404) {
          doc = {_id: id};
      }
      doc.last_seq = checkpoint;
      db.put(doc, callback);
    });
  };
  updateCheckpoint(target, function(err, doc) {
    updateCheckpoint(src, function(err, doc) {
      callback();
    });
  });
};

function replicate(src, target, opts, promise) {

  var requests = new RequestManager(promise);
  var writeQueue = [];
  var repId = genReplicationId(src, target, opts);
  var results = [];
  var completed = false;
  var pendingRevs = 0;
  var last_seq = 0;
  var continuous = opts.continuous || false;
  var doc_ids = opts.doc_ids;
  var result = {
    ok: true,
    start_time: new Date(),
    docs_read: 0,
    docs_written: 0
  };

  function docsWritten(err, res, len) {
    if (opts.onChange) {
      for (var i = 0; i < len; i++) {
        /*jshint validthis:true */
        opts.onChange.apply(this, [result]);
      }
    }
    pendingRevs -= len;
    result.docs_written += len;

    writeCheckpoint(src, target, repId, last_seq, function(err, res) {
      requests.notifyRequestComplete();
      isCompleted();
    });
  }

  function writeDocs() {
    if (!writeQueue.length) {
      return requests.notifyRequestComplete();
    }
    var len = writeQueue.length;
    target.bulkDocs({docs: writeQueue}, {new_edits: false}, function(err, res) {
      docsWritten(err, res, len);
    });
    writeQueue = [];
  }

  function eachRev(id, rev) {
    src.get(id, {revs: true, rev: rev, attachments: true}, function(err, doc) {
      result.docs_read++;
      requests.notifyRequestComplete();
      writeQueue.push(doc);
      requests.enqueue(writeDocs);
    });
  }

  function onRevsDiff(diffCounts) {
    return function (err, diffs) {
      requests.notifyRequestComplete();
      if (err) {
        if (continuous) {
          promise.cancel();
        }
        PouchUtils.call(opts.complete, err, null);
        return;
      }

      // We already have all diffs passed in `diffCounts`
      if (Object.keys(diffs).length === 0) {
        for (var docid in diffCounts) {
          pendingRevs -= diffCounts[docid];
        }
        isCompleted();
        return;
      }

      var _enqueuer = function (rev) {
        requests.enqueue(eachRev, [id, rev]);
      };

      for (var id in diffs) {
        var diffsAlreadyHere = diffCounts[id] - diffs[id].missing.length;
        pendingRevs -= diffsAlreadyHere;
        diffs[id].missing.forEach(_enqueuer);
      }
    };
  }

  function fetchRevsDiff(diff, diffCounts) {
    target.revsDiff(diff, onRevsDiff(diffCounts));
  }

  function onChange(change) {
    last_seq = change.seq;
    results.push(change);
    var diff = {};
    diff[change.id] = change.changes.map(function(x) { return x.rev; });
    var counts = {};
    counts[change.id] = change.changes.length;
    pendingRevs += change.changes.length;
    requests.enqueue(fetchRevsDiff, [diff, counts]);
  }

  function complete() {
    completed = true;
    isCompleted();
  }

  function isCompleted() {
    if (completed && pendingRevs === 0) {
      result.end_time = new Date();
      PouchUtils.call(opts.complete, null, result);
    }
  }

  fetchCheckpoint(src, target, repId, function(err, checkpoint) {

    if (err) {
      return PouchUtils.call(opts.complete, err);
    }

    last_seq = checkpoint;

    // Was the replication cancelled by the caller before it had a chance
    // to start. Shouldnt we be calling complete?
    if (promise.cancelled) {
      return;
    }

    var repOpts = {
      continuous: continuous,
      since: last_seq,
      style: 'all_docs',
      onChange: onChange,
      complete: complete,
      doc_ids: doc_ids
    };

    if (opts.filter) {
      repOpts.filter = opts.filter;
    }

    if (opts.query_params) {
      repOpts.query_params = opts.query_params;
    }

    var changes = src.changes(repOpts);

    if (opts.continuous) {
      var cancel = promise.cancel;
      promise.cancel = function() {
        cancel();
        changes.cancel();
      };
    }
  });

}

function toPouch(db, callback) {
  if (typeof db === 'string') {
    return new Pouch(db, callback);
  }
  callback(null, db);
}

Pouch.replicate = function(src, target, opts, callback) {
  if (opts instanceof Function) {
    callback = opts;
    opts = {};
  }
  if (opts === undefined) {
    opts = {};
  }
  if (!opts.complete) {
    opts.complete = callback;
  }
  var replicateRet = new Promise();
  toPouch(src, function(err, src) {
    if (err) {
      return PouchUtils.call(callback, err);
    }
    toPouch(target, function(err, target) {
      if (err) {
        return PouchUtils.call(callback, err);
      }
      if (opts.server) {
        if (typeof src.replicateOnServer !== 'function') {
          return PouchUtils.call(callback, { error: 'Server replication not supported for ' + src.type() + ' adapter' });
        }
        if (src.type() !== target.type()) {
          return PouchUtils.call(callback, { error: 'Server replication for different adapter types (' + src.type() + ' and ' + target.type() + ') is not supported' });
        }
        src.replicateOnServer(target, opts, replicateRet);
      } else {
        replicate(src, target, opts, replicateRet);
      }
    });
  });
  return replicateRet;
};

/*jshint strict: false */
/*global Buffer: true, escape: true, module, window, Crypto */
/*global chrome, extend, ajax, createBlob, btoa, atob, uuid, require, PouchMerge: true */

var PouchUtils = {};

if (typeof module !== 'undefined' && module.exports) {
  PouchMerge = require('./pouch.merge.js');
  PouchUtils.extend = require('./deps/extend');
  PouchUtils.ajax = require('./deps/ajax');
  PouchUtils.createBlob = require('./deps/blob');
  PouchUtils.uuid = require('./deps/uuid');
  PouchUtils.Crypto = require('./deps/md5.js');
} else {
  PouchUtils.Crypto = Crypto;
  PouchUtils.extend = extend;
  PouchUtils.ajax = ajax;
  PouchUtils.createBlob = createBlob;
  PouchUtils.uuid = uuid;
}

// List of top level reserved words for doc
var reservedWords = [
  '_id',
  '_rev',
  '_attachments',
  '_deleted',
  '_revisions',
  '_revs_info',
  '_conflicts',
  '_deleted_conflicts',
  '_local_seq',
  '_rev_tree'
];

// Determine id an ID is valid
//   - invalid IDs begin with an underescore that does not begin '_design' or '_local'
//   - any other string value is a valid id
var isValidId = function(id) {
  if (/^_/.test(id)) {
    return (/^_(design|local)/).test(id);
  }
  return true;
};

var isChromeApp = function(){
  return (typeof chrome !== "undefined" &&
          typeof chrome.storage !== "undefined" &&
          typeof chrome.storage.local !== "undefined");
};

// Pretty dumb name for a function, just wraps callback calls so we dont
// to if (callback) callback() everywhere
PouchUtils.call = function(fun) {
  if (typeof fun === typeof Function) {
    var args = Array.prototype.slice.call(arguments, 1);
    fun.apply(this, args);
  }
};

PouchUtils.isLocalId = function(id) {
  return (/^_local/).test(id);
};

// check if a specific revision of a doc has been deleted
//  - metadata: the metadata object from the doc store
//  - rev: (optional) the revision to check. defaults to winning revision
PouchUtils.isDeleted = function(metadata, rev) {
  if (!rev) {
    rev = PouchMerge.winningRev(metadata);
  }
  if (rev.indexOf('-') >= 0) {
    rev = rev.split('-')[1];
  }
  var deleted = false;
  PouchMerge.traverseRevTree(metadata.rev_tree, function(isLeaf, pos, id, acc, opts) {
    if (id === rev) {
      deleted = !!opts.deleted;
    }
  });

  return deleted;
};

PouchUtils.filterChange = function(opts) {
  return function(change) {
    var req = {};
    var hasFilter = opts.filter && typeof opts.filter === 'function';

    req.query = opts.query_params;
    if (opts.filter && hasFilter && !opts.filter.call(this, change.doc, req)) {
      return false;
    }
    if (opts.doc_ids && opts.doc_ids.indexOf(change.id) === -1) {
      return false;
    }
    if (!opts.include_docs) {
      delete change.doc;
    } else {
      for (var att in change.doc._attachments) {
        change.doc._attachments[att].stub = true;
      }
    }
    return true;
  };
};

PouchUtils.processChanges = function(opts, changes, last_seq) {
  // TODO: we should try to filter and limit as soon as possible
  changes = changes.filter(PouchUtils.filterChange(opts));
  if (opts.limit) {
    if (opts.limit < changes.length) {
      changes.length = opts.limit;
    }
  }
  changes.forEach(function(change){
    PouchUtils.call(opts.onChange, change);
  });
  PouchUtils.call(opts.complete, null, {results: changes, last_seq: last_seq});
};

// Preprocess documents, parse their revisions, assign an id and a
// revision for new writes that are missing them, etc
PouchUtils.parseDoc = function(doc, newEdits) {
  var error = null;
  var nRevNum;
  var newRevId;
  var revInfo;
  var opts = {status: 'available'};
  if (doc._deleted) {
    opts.deleted = true;
  }

  if (newEdits) {
    if (!doc._id) {
      doc._id = Pouch.uuid();
    }
    newRevId = Pouch.uuid({length: 32, radix: 16}).toLowerCase();
    if (doc._rev) {
      revInfo = /^(\d+)-(.+)$/.exec(doc._rev);
      if (!revInfo) {
        throw "invalid value for property '_rev'";
      }
      doc._rev_tree = [{
        pos: parseInt(revInfo[1], 10),
        ids: [revInfo[2], {status: 'missing'}, [[newRevId, opts, []]]]
      }];
      nRevNum = parseInt(revInfo[1], 10) + 1;
    } else {
      doc._rev_tree = [{
        pos: 1,
        ids : [newRevId, opts, []]
      }];
      nRevNum = 1;
    }
  } else {
    if (doc._revisions) {
      doc._rev_tree = [{
        pos: doc._revisions.start - doc._revisions.ids.length + 1,
        ids: doc._revisions.ids.reduce(function(acc, x) {
          if (acc === null) {
            return [x, opts, []];
          } else {
            return [x, {status: 'missing'}, [acc]];
          }
        }, null)
      }];
      nRevNum = doc._revisions.start;
      newRevId = doc._revisions.ids[0];
    }
    if (!doc._rev_tree) {
      revInfo = /^(\d+)-(.+)$/.exec(doc._rev);
      if (!revInfo) {
        return Pouch.Errors.BAD_ARG;
      }
      nRevNum = parseInt(revInfo[1], 10);
      newRevId = revInfo[2];
      doc._rev_tree = [{
        pos: parseInt(revInfo[1], 10),
        ids: [revInfo[2], opts, []]
      }];
    }
  }

  if (typeof doc._id !== 'string') {
    error = Pouch.Errors.INVALID_ID;
  }
  else if (!isValidId(doc._id)) {
    error = Pouch.Errors.RESERVED_ID;
  }

  for (var key in doc) {
    if (doc.hasOwnProperty(key) && key[0] === '_' && reservedWords.indexOf(key) === -1) {
      error = PouchUtils.extend({}, Pouch.Errors.DOC_VALIDATION);
      error.reason += ': ' + key;
    }
  }

  doc._id = decodeURIComponent(doc._id);
  doc._rev = [nRevNum, newRevId].join('-');

  if (error) {
    return error;
  }

  return Object.keys(doc).reduce(function(acc, key) {
    if (/^_/.test(key) && key !== '_attachments') {
      acc.metadata[key.slice(1)] = doc[key];
    } else {
      acc.data[key] = doc[key];
    }
    return acc;
  }, {metadata : {}, data : {}});
};

PouchUtils.isCordova = function(){
  return (typeof cordova !== "undefined" ||
          typeof PhoneGap !== "undefined" ||
          typeof phonegap !== "undefined");
};

PouchUtils.Changes = function() {

  var api = {};
  var listeners = {};

  if (isChromeApp()){
    chrome.storage.onChanged.addListener(function(e){
      // make sure it's event addressed to us
      if (e.db_name != null) {
        api.notify(e.db_name.newValue);//object only has oldValue, newValue members
      }
    });
  } else if (typeof window !== 'undefined') {
    window.addEventListener("storage", function(e) {
      api.notify(e.key);
    });
  }

  api.addListener = function(db_name, id, db, opts) {
    if (!listeners[db_name]) {
      listeners[db_name] = {};
    }
    listeners[db_name][id] = {
      db: db,
      opts: opts
    };
  };

  api.removeListener = function(db_name, id) {
    if (listeners[db_name]) {
      delete listeners[db_name][id];
    }
  };

  api.clearListeners = function(db_name) {
    delete listeners[db_name];
  };

  api.notifyLocalWindows = function(db_name){
    //do a useless change on a storage thing
    //in order to get other windows's listeners to activate
    if (!isChromeApp()){
      localStorage[db_name] = (localStorage[db_name] === "a") ? "b" : "a";
    } else {
      chrome.storage.local.set({db_name: db_name});
    }
  };

  api.notify = function(db_name) {
    if (!listeners[db_name]) { return; }

    Object.keys(listeners[db_name]).forEach(function (i) {
      var opts = listeners[db_name][i].opts;
      listeners[db_name][i].db.changes({
        include_docs: opts.include_docs,
        conflicts: opts.conflicts,
        continuous: false,
        descending: false,
        filter: opts.filter,
        since: opts.since,
        query_params: opts.query_params,
        onChange: function(c) {
          if (c.seq > opts.since && !opts.cancelled) {
            opts.since = c.seq;
            PouchUtils.call(opts.onChange, c);
          }
        }
      });
    });
  };

  return api;
};

if (typeof window === 'undefined' || !('atob' in window)) {
  PouchUtils.atob = function(str) {
    var base64 = new Buffer(str, 'base64');
    // Node.js will just skip the characters it can't encode instead of
    // throwing and exception
    if (base64.toString('base64') !== str) {
      throw("Cannot base64 encode full string");
    }
    return base64.toString('binary');
  };
} else {
  PouchUtils.atob = function(str) {
    return atob(str);
  };
}

if (typeof window === 'undefined' || !('btoa' in window)) {
  PouchUtils.btoa = function(str) {
    return new Buffer(str, 'binary').toString('base64');
  };
} else {
  PouchUtils.btoa = function(str) {
    return btoa(str);
  };
}

if (typeof module !== 'undefined' && module.exports) {
  module.exports = PouchUtils;
}

/*globals Pouch: true, cordova, PouchUtils: true, PouchMerge */

"use strict";

var PouchAdapter;
var PouchUtils;

if (typeof module !== 'undefined' && module.exports) {
  PouchUtils = require('./pouch.utils.js');
}

var call = PouchUtils.call;

/*
 * A generic pouch adapter
 */

// returns first element of arr satisfying callback predicate
function arrayFirst(arr, callback) {
  for (var i = 0; i < arr.length; i++) {
    if (callback(arr[i], i) === true) {
      return arr[i];
    }
  }
  return false;
}

// Wrapper for functions that call the bulkdocs api with a single doc,
// if the first result is an error, return an error
function yankError(callback) {
  return function(err, results) {
    if (err || results[0].error) {
      call(callback, err || results[0]);
    } else {
      call(callback, null, results[0]);
    }
  };
}

// for every node in a revision tree computes its distance from the closest
// leaf
function computeHeight(revs) {
  var height = {};
  var edges = [];
  PouchMerge.traverseRevTree(revs, function(isLeaf, pos, id, prnt) {
    var rev = pos + "-" + id;
    if (isLeaf) {
      height[rev] = 0;
    }
    if (prnt !== undefined) {
      edges.push({from: prnt, to: rev});
    }
    return rev;
  });

  edges.reverse();
  edges.forEach(function(edge) {
    if (height[edge.from] === undefined) {
      height[edge.from] = 1 + height[edge.to];
    } else {
      height[edge.from] = Math.min(height[edge.from], 1 + height[edge.to]);
    }
  });
  return height;
}

PouchAdapter = function(opts, callback) {

  var api = {};

  var customApi = Pouch.adapters[opts.adapter](opts, function(err, db) {
    if (err) {
      if (callback) {
        callback(err);
      }
      return;
    }

    for (var j in api) {
      if (!db.hasOwnProperty(j)) {
        db[j] = api[j];
      }
    }

    // Don't call Pouch.open for ALL_DBS
    // Pouch.open saves the db's name into ALL_DBS
    if (opts.name === Pouch.prefix + Pouch.ALL_DBS) {
      callback(err, db);
    } else {
      Pouch.open(opts, function(err) {
        callback(err, db);
      });
    }
  });

  var auto_compaction = (opts.auto_compaction === true);

  // wraps a callback with a function that runs compaction after each edit
  var autoCompact = function(callback) {
    if (!auto_compaction) {
      return callback;
    }
    return function(err, res) {
      if (err) {
        call(callback, err);
      } else {
        var count = res.length;
        var decCount = function() {
          count--;
          if (!count) {
            call(callback, null, res);
          }
        };
        res.forEach(function(doc) {
          if (doc.ok) {
            // TODO: we need better error handling
            compactDocument(doc.id, 1, decCount);
          } else {
            decCount();
          }
        });
      }
    };
  };

  api.post = function (doc, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (typeof doc !== 'object' || Array.isArray(doc)) {
      return call(callback, Pouch.Errors.NOT_AN_OBJECT);
    }
    return customApi.bulkDocs({docs: [doc]}, opts,
        autoCompact(yankError(callback)));
  };

  api.put = function(doc, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (typeof doc !== 'object') {
      return call(callback, Pouch.Errors.NOT_AN_OBJECT);
    }
    if (!('_id' in doc)) {
      return call(callback, Pouch.Errors.MISSING_ID);
    }
    return customApi.bulkDocs({docs: [doc]}, opts,
        autoCompact(yankError(callback)));
  };

  api.putAttachment = function (docId, attachmentId, rev, blob, type, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('putAttachment', arguments);
      return;
    }
    if (typeof type === 'function') {
      callback = type;
      type = blob;
      blob = rev;
      rev = null;
    }
    if (typeof type === 'undefined') {
      type = blob;
      blob = rev;
      rev = null;
    }

    function createAttachment(doc) {
      doc._attachments = doc._attachments || {};
      doc._attachments[attachmentId] = {
        content_type: type,
        data: blob
      };
      api.put(doc, callback);
    }

    api.get(docId, function(err, doc) {
      // create new doc
      if (err && err.error === Pouch.Errors.MISSING_DOC.error) {
        createAttachment({_id: docId});
        return;
      }
      if (err) {
        call(callback, err);
        return;
      }

      if (doc._rev !== rev) {
        call(callback, Pouch.Errors.REV_CONFLICT);
        return;
      }

      createAttachment(doc);
    });
  };

  api.removeAttachment = function (docId, attachmentId, rev, callback) {
    api.get(docId, function(err, obj) {
      if (err) {
        call(callback, err);
        return;
      }
      if (obj._rev !== rev) {
        call(callback, Pouch.Errors.REV_CONFLICT);
        return;
      }
      if (!obj._attachments) {
        return call(callback, null);
      }
      delete obj._attachments[attachmentId];
      if (Object.keys(obj._attachments).length === 0){
        delete obj._attachments;
      }
      api.put(obj, callback);
    });
  };

  api.remove = function (doc, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (opts === undefined) {
      opts = {};
    }
    opts.was_delete = true;
    var newDoc = {_id: doc._id, _rev: doc._rev};
    newDoc._deleted = true;
    return customApi.bulkDocs({docs: [newDoc]}, opts, yankError(callback));
  };

  api.revsDiff = function (req, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    var ids = Object.keys(req);
    var count = 0;
    var missing = {};

    function addToMissing(id, revId) {
      if (!missing[id]) {
        missing[id] = {missing: []};
      }
      missing[id].missing.push(revId);
    }

    function processDoc(id, rev_tree) {
      // Is this fast enough? Maybe we should switch to a set simulated by a map
      var missingForId = req[id].slice(0);
      PouchMerge.traverseRevTree(rev_tree, function(isLeaf, pos, revHash, ctx,
        opts) {
          var rev = pos + '-' + revHash;
          var idx = missingForId.indexOf(rev);
          if (idx === -1) {
            return;
          }

          missingForId.splice(idx, 1);
          if (opts.status !== 'available') {
            addToMissing(id, rev);
          }
      });

      // Traversing the tree is synchronous, so now `missingForId` contains
      // revisions that were not found in the tree
      missingForId.forEach(function(rev) {
        addToMissing(id, rev);
      });
    }

    ids.map(function(id) {
      customApi._getRevisionTree(id, function(err, rev_tree) {
        if (err && err.error === 'not_found' && err.reason === 'missing') {
          missing[id] = {missing: req[id]};
        } else if (err) {
          return call(callback, err);
        } else {
          processDoc(id, rev_tree);
        }

        if (++count === ids.length) {
          return call(callback, null, missing);
        }
      });
    });
  };

  // compact one document and fire callback
  // by compacting we mean removing all revisions which
  // are further from the leaf in revision tree than max_height
  var compactDocument = function(docId, max_height, callback) {
    customApi._getRevisionTree(docId, function(err, rev_tree){
      if (err) {
        return call(callback);
      }
      var height = computeHeight(rev_tree);
      var candidates = [];
      var revs = [];
      Object.keys(height).forEach(function(rev) {
        if (height[rev] > max_height) {
          candidates.push(rev);
        }
      });

      PouchMerge.traverseRevTree(rev_tree, function(isLeaf, pos, revHash, ctx, opts) {
        var rev = pos + '-' + revHash;
        if (opts.status === 'available' && candidates.indexOf(rev) !== -1) {
          opts.status = 'missing';
          revs.push(rev);
        }
      });
      customApi._doCompaction(docId, rev_tree, revs, callback);
    });
  };

  // compact the whole database using single document
  // compaction
  api.compact = function(opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    api.changes({complete: function(err, res) {
      if (err) {
        call(callback); // TODO: silently fail
        return;
      }
      var count = res.results.length;
      if (!count) {
        call(callback);
        return;
      }
      res.results.forEach(function(row) {
        compactDocument(row.id, 0, function() {
          count--;
          if (!count) {
            call(callback);
          }
        });
      });
    }});
  };

  /* Begin api wrappers. Specific functionality to storage belongs in the _[method] */
  api.get = function (id, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('get', arguments);
      return;
    }
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }

    var leaves = [];
    function finishOpenRevs() {
      var result = [];
      var count = leaves.length;
      if (!count) {
        return call(callback, null, result);
      }
      // order with open_revs is unspecified
      leaves.forEach(function(leaf){
        api.get(id, {rev: leaf, revs: opts.revs}, function(err, doc){
          if (!err) {
            result.push({ok: doc});
          } else {
            result.push({missing: leaf});
          }
          count--;
          if(!count) {
            call(callback, null, result);
          }
        });
      });
    }

    if (opts.open_revs) {
      if (opts.open_revs === "all") {
        customApi._getRevisionTree(id, function(err, rev_tree){
          if (err) {
            // if there's no such document we should treat this
            // situation the same way as if revision tree was empty
            rev_tree = [];
          }
          leaves = PouchMerge.collectLeaves(rev_tree).map(function(leaf){
            return leaf.rev;
          });
          finishOpenRevs();
        });
      } else {
        if (Array.isArray(opts.open_revs)) {
          leaves = opts.open_revs;
          for (var i = 0; i < leaves.length; i++) {
            var l = leaves[i];
            // looks like it's the only thing couchdb checks
            if (!(typeof(l) === "string" && /^\d+-/.test(l))) {
              return call(callback, Pouch.error(Pouch.Errors.BAD_REQUEST,
                "Invalid rev format" ));
            }
          }
          finishOpenRevs();
        } else {
          return call(callback, Pouch.error(Pouch.Errors.UNKNOWN_ERROR,
            'function_clause'));
        }
      }
      return; // open_revs does not like other options
    }

    return customApi._get(id, opts, function(err, result) {
      if (err) {
        return call(callback, err);
      }

      var doc = result.doc;
      var metadata = result.metadata;
      var ctx = result.ctx;

      if (opts.conflicts) {
        var conflicts = PouchMerge.collectConflicts(metadata);
        if (conflicts.length) {
          doc._conflicts = conflicts;
        }
      }

      if (opts.revs || opts.revs_info) {
        var paths = PouchMerge.rootToLeaf(metadata.rev_tree);
        var path = arrayFirst(paths, function(arr) {
          return arr.ids.map(function(x) { return x.id; })
            .indexOf(doc._rev.split('-')[1]) !== -1;
        });

        path.ids.splice(path.ids.map(function(x) {return x.id;})
                        .indexOf(doc._rev.split('-')[1]) + 1);
        path.ids.reverse();

        if (opts.revs) {
          doc._revisions = {
            start: (path.pos + path.ids.length) - 1,
            ids: path.ids.map(function(rev) {
              return rev.id;
            })
          };
        }
        if (opts.revs_info) {
          var pos =  path.pos + path.ids.length;
          doc._revs_info = path.ids.map(function(rev) {
            pos--;
            return {
              rev: pos + '-' + rev.id,
              status: rev.opts.status
            };
          });
        }
      }

      if (opts.local_seq) {
        doc._local_seq = result.metadata.seq;
      }

      if (opts.attachments && doc._attachments) {
        var attachments = doc._attachments;
        var count = Object.keys(attachments).length;
        if (count === 0) {
          return call(callback, null, doc);
        }
        Object.keys(attachments).forEach(function(key) {
          customApi._getAttachment(attachments[key], {encode: true, ctx: ctx}, function(err, data) {
            doc._attachments[key].data = data;
            if (!--count){
              call(callback, null, doc);
            }
          });
        });
      } else {
        if (doc._attachments){
          for (var key in doc._attachments) {
            doc._attachments[key].stub = true;
          }
        }
        call(callback, null, doc);
      }
    });
  };

  api.getAttachment = function(docId, attachmentId, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('getAttachment', arguments);
      return;
    }
    if (opts instanceof Function) {
      callback = opts;
      opts = {};
    }
    customApi._get(docId, opts, function(err, res) {
      if (err) {
        return call(callback, err);
      }
      if (res.doc._attachments && res.doc._attachments[attachmentId]) {
        opts.ctx = res.ctx;
        customApi._getAttachment(res.doc._attachments[attachmentId], opts, callback);
      } else {
        return call(callback, Pouch.Errors.MISSING_DOC);
      }
    });
  };

  api.allDocs = function(opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('allDocs', arguments);
      return;
    }
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if ('keys' in opts) {
      if ('startkey' in opts) {
        call(callback, Pouch.error(Pouch.Errors.QUERY_PARSE_ERROR,
          'Query parameter `start_key` is not compatible with multi-get'
        ));
        return;
      }
      if ('endkey' in opts) {
        call(callback, Pouch.error(Pouch.Errors.QUERY_PARSE_ERROR,
          'Query parameter `end_key` is not compatible with multi-get'
        ));
        return;
      }
    }
    if (typeof opts.skip === 'undefined') {
      opts.skip = 0;
    }

    return customApi._allDocs(opts, callback);
  };

  api.changes = function(opts) {
    if (!api.taskqueue.ready()) {
      var task = api.taskqueue.addTask('changes', arguments);
      return {
        cancel: function() {
          if (task.task) {
            return task.task.cancel();
          }
          if (Pouch.DEBUG) {
            console.log('Cancel Changes Feed');
          }
          task.parameters[0].aborted = true;
        }
      };
    }
    opts = PouchUtils.extend(true, {}, opts);

    if (!opts.since) {
      opts.since = 0;
    }
    if (opts.since === 'latest') {
      var changes;
      api.info(function (err, info) {
        if (!opts.aborted) {
          opts.since = info.update_seq  - 1;
          api.changes(opts);
        }
      });
      // Return a method to cancel this method from processing any more
      return {
        cancel: function() {
          if (changes) {
            return changes.cancel();
          }
          if (Pouch.DEBUG) {
            console.log('Cancel Changes Feed');
          }
          opts.aborted = true;
        }
      };
    }

    if (!('descending' in opts)) {
      opts.descending = false;
    }

    // 0 and 1 should return 1 document
    opts.limit = opts.limit === 0 ? 1 : opts.limit;
    return customApi._changes(opts);
  };

  api.close = function(callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('close', arguments);
      return;
    }
    return customApi._close(callback);
  };

  api.info = function(callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('info', arguments);
      return;
    }
    return customApi._info(callback);
  };

  api.id = function() {
    return customApi._id();
  };

  api.type = function() {
    return (typeof customApi._type === 'function') ? customApi._type() : opts.adapter;
  };

  api.bulkDocs = function(req, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('bulkDocs', arguments);
      return;
    }
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (!opts) {
      opts = {};
    } else {
      opts = PouchUtils.extend(true, {}, opts);
    }

    if (!req || !req.docs || req.docs.length < 1) {
      return call(callback, Pouch.Errors.MISSING_BULK_DOCS);
    }

    if (!Array.isArray(req.docs)) {
      return call(callback, Pouch.Errors.QUERY_PARSE_ERROR);
    }

    for (var i = 0; i < req.docs.length; ++i) {
      if (typeof req.docs[i] !== 'object' || Array.isArray(req.docs[i])) {
        return call(callback, Pouch.Errors.NOT_AN_OBJECT);
      }
    }

    req = PouchUtils.extend(true, {}, req);
    if (!('new_edits' in opts)) {
      opts.new_edits = true;
    }

    return customApi._bulkDocs(req, opts, autoCompact(callback));
  };

  /* End Wrappers */
  var taskqueue = {};

  taskqueue.ready = false;
  taskqueue.queue = [];

  api.taskqueue = {};

  api.taskqueue.execute = function (db) {
    if (taskqueue.ready) {
      taskqueue.queue.forEach(function(d) {
        d.task = db[d.name].apply(null, d.parameters);
      });
    }
  };

  api.taskqueue.ready = function() {
    if (arguments.length === 0) {
      return taskqueue.ready;
    }
    taskqueue.ready = arguments[0];
  };

  api.taskqueue.addTask = function(name, parameters) {
    var task = { name: name, parameters: parameters };
    taskqueue.queue.push(task);
    return task;
  };

  api.replicate = {};

  api.replicate.from = function (url, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    return Pouch.replicate(url, customApi, opts, callback);
  };

  api.replicate.to = function (dbName, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    return Pouch.replicate(customApi, dbName, opts, callback);
  };

  for (var j in api) {
    if (!customApi.hasOwnProperty(j)) {
      customApi[j] = api[j];
    }
  }

  // Http adapter can skip setup so we force the db to be ready and execute any jobs
  if (opts.skipSetup) {
    api.taskqueue.ready(true);
    api.taskqueue.execute(api);
  }

  if (PouchUtils.isCordova()) {
    //to inform websql adapter that we can use api
    cordova.fireWindowEvent(opts.name + "_pouch", {});
  }
  return customApi;
};

if (typeof module !== 'undefined' && module.exports) {
  module.exports = PouchAdapter;
}

/*globals Pouch: true, PouchUtils: true, require, console */

"use strict";

var PouchUtils;

if (typeof module !== 'undefined' && module.exports) {
  Pouch = require('../pouch.js');
  PouchUtils = require('../pouch.utils.js');
}



var HTTP_TIMEOUT = 10000;

// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License
function parseUri (str) {
  var o = parseUri.options;
  var m = o.parser[o.strictMode ? "strict" : "loose"].exec(str);
  var uri = {};
  var i = 14;

  while (i--) {
    uri[o.key[i]] = m[i] || "";
  }

  uri[o.q.name] = {};
  uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
    if ($1) {
      uri[o.q.name][$1] = $2;
    }
  });

  return uri;
}

function encodeDocId(id) {
  if (/^_design/.test(id)) {
    return id;
  }
  return encodeURIComponent(id);
}

parseUri.options = {
  strictMode: false,
  key: ["source","protocol","authority","userInfo","user","password","host",
        "port","relative","path","directory","file","query","anchor"],
  q:   {
    name:   "queryKey",
    parser: /(?:^|&)([^&=]*)=?([^&]*)/g
  },
  parser: {
    strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
    loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
  }
};

// Get all the information you possibly can about the URI given by name and
// return it as a suitable object.
function getHost(name, opts) {
  // If the given name contains "http:"
  if (/http(s?):/.test(name)) {
    // Prase the URI into all its little bits
    var uri = parseUri(name);

    // Store the fact that it is a remote URI
    uri.remote = true;

    // Store the user and password as a separate auth object
    if (uri.user || uri.password) {
      uri.auth = {username: uri.user, password: uri.password};
    }

    // Split the path part of the URI into parts using '/' as the delimiter
    // after removing any leading '/' and any trailing '/'
    var parts = uri.path.replace(/(^\/|\/$)/g, '').split('/');

    // Store the first part as the database name and remove it from the parts
    // array
    uri.db = parts.pop();

    // Restore the path by joining all the remaining parts (all the parts
    // except for the database name) with '/'s
    uri.path = parts.join('/');
    opts = opts || {};
    uri.headers = opts.headers || {};
    
    if (opts.auth || uri.auth) { 
      var nAuth = opts.auth || uri.auth;
      var token = PouchUtils.btoa(nAuth.username + ':' + nAuth.password);
      uri.headers.Authorization = 'Basic ' + token; 
    }

    if (opts.headers) {
      uri.headers = opts.headers;
    }

    return uri;
  }

  // If the given name does not contain 'http:' then return a very basic object
  // with no host, the current path, the given name as the database name and no
  // username/password
  return {host: '', path: '/', db: name, auth: false};
}

// Generate a URL with the host data given by opts and the given path
function genDBUrl(opts, path) {
  // If the host is remote
  if (opts.remote) {
    // If the host already has a path, then we need to have a path delimiter
    // Otherwise, the path delimiter is the empty string
    var pathDel = !opts.path ? '' : '/';

    // Return the URL made up of all the host's information and the given path
    return opts.protocol + '://' + opts.host + ':' + opts.port + '/' +
      opts.path + pathDel + opts.db + '/' + path;
  }

  // If the host is not remote, then return the URL made up of just the
  // database name and the given path
  return '/' + opts.db + '/' + path;
}

// Generate a URL with the host data given by opts and the given path
function genUrl(opts, path) {
  if (opts.remote) {
    // If the host already has a path, then we need to have a path delimiter
    // Otherwise, the path delimiter is the empty string
    var pathDel = !opts.path ? '' : '/';

    // If the host already has a path, then we need to have a path delimiter
    // Otherwise, the path delimiter is the empty string
    return opts.protocol + '://' + opts.host + ':' + opts.port + '/' + opts.path + pathDel + path;
  }

  return '/' + path;
}

// Implements the PouchDB API for dealing with CouchDB instances over HTTP
function HttpPouch(opts, callback) {

  // Parse the URI given by opts.name into an easy-to-use object
  var host = getHost(opts.name, opts);

  // Generate the database URL based on the host
  var db_url = genDBUrl(host, '');

  // The functions that will be publically available for HttpPouch
  var api = {};
  var ajaxOpts = opts.ajax || {};
  function ajax(options, callback) {
    return PouchUtils.ajax(PouchUtils.extend({}, ajaxOpts, options), callback);
  }
  var uuids = {
    list: [],
    get: function(opts, callback) {
      if (typeof opts === 'function') {
        callback = opts;
        opts = {count: 10};
      }
      var cb = function(err, body) {
        if (err || !('uuids' in body)) {
          PouchUtils.call(callback, err || Pouch.Errors.UNKNOWN_ERROR);
        } else {
          uuids.list = uuids.list.concat(body.uuids);
          PouchUtils.call(callback, null, "OK");
        }
      };
      var params = '?count=' + opts.count;
      ajax({
        headers: host.headers,
        method: 'GET',
        url: genUrl(host, '_uuids') + params
      }, cb);
    }
  };

  // Create a new CouchDB database based on the given opts
  var createDB = function(){
    ajax({headers: host.headers, method: 'PUT', url: db_url}, function(err, ret) {
      // If we get an "Unauthorized" error
      if (err && err.status === 401) {
        // Test if the database already exists
        ajax({headers: host.headers, method: 'HEAD', url: db_url}, function (err, ret) {
          // If there is still an error
          if (err) {
            // Give the error to the callback to deal with
            PouchUtils.call(callback, err);
          } else {
            // Continue as if there had been no errors
            PouchUtils.call(callback, null, api);
          }
        });
        // If there were no errros or if the only error is "Precondition Failed"
        // (note: "Precondition Failed" occurs when we try to create a database
        // that already exists)
      } else if (!err || err.status === 412) {
        // Continue as if there had been no errors
        PouchUtils.call(callback, null, api);
      } else {
        PouchUtils.call(callback, Pouch.Errors.UNKNOWN_ERROR);
      }
    });
  };
  if (!opts.skipSetup) {
    ajax({headers: host.headers, method: 'GET', url: db_url}, function(err, ret) {
      //check if the db exists
      if (err) {
        if (err.status === 404) {
          //if it doesn't, create it
          createDB();
        } else {
          PouchUtils.call(callback, err);
        }
      } else {
        //go do stuff with the db
        PouchUtils.call(callback, null, api);
      }
    });
  }

  api.type = function() {
    return 'http';
  };

  // The HttpPouch's ID is its URL
  api.id = function() {
    return genDBUrl(host, '');
  };

  api.request = function(options, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('request', arguments);
      return;
    }
    options.headers = host.headers;
    options.url = genDBUrl(host, options.url);
    ajax(options, callback);
  };

  // Sends a POST request to the host calling the couchdb _compact function
  //    version: The version of CouchDB it is running
  api.compact = function(opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('compact', arguments);
      return;
    }
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    ajax({
      headers: host.headers,
      url: genDBUrl(host, '_compact'),
      method: 'POST'
    }, function() {
      function ping() {
        api.info(function(err, res) {
          if (!res.compact_running) {
            PouchUtils.call(callback, null);
          } else {
            setTimeout(ping, opts.interval || 200);
          }
        });
      }
      // Ping the http if it's finished compaction
      if (typeof callback === "function") {
        ping();
      }
    });
  };

  // Calls GET on the host, which gets back a JSON string containing
  //    couchdb: A welcome string
  //    version: The version of CouchDB it is running
  api.info = function(callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('info', arguments);
      return;
    }
    ajax({
      headers: host.headers,
      method:'GET',
      url: genDBUrl(host, '')
    }, callback);
  };

  // Get the document with the given id from the database given by host.
  // The id could be solely the _id in the database, or it may be a
  // _design/ID or _local/ID path
  api.get = function(id, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('get', arguments);
      return;
    }
    // If no options were given, set the callback to the second parameter
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }

    if (opts.auto_encode === undefined) {
      opts.auto_encode = true;
    }

    // List of parameters to add to the GET request
    var params = [];

    // If it exists, add the opts.revs value to the list of parameters.
    // If revs=true then the resulting JSON will include a field
    // _revisions containing an array of the revision IDs.
    if (opts.revs) {
      params.push('revs=true');
    }

    // If it exists, add the opts.revs_info value to the list of parameters.
    // If revs_info=true then the resulting JSON will include the field
    // _revs_info containing an array of objects in which each object
    // representing an available revision.
    if (opts.revs_info) {
      params.push('revs_info=true');
    }

    if (opts.local_seq) {
      params.push('local_seq=true');
    }
    // If it exists, add the opts.open_revs value to the list of parameters.
    // If open_revs=all then the resulting JSON will include all the leaf
    // revisions. If open_revs=["rev1", "rev2",...] then the resulting JSON
    // will contain an array of objects containing data of all revisions
    if (opts.open_revs) {
      if (opts.open_revs !== "all") {
        opts.open_revs = JSON.stringify(opts.open_revs);
      }
      params.push('open_revs=' + opts.open_revs);
    }

    // If it exists, add the opts.attachments value to the list of parameters.
    // If attachments=true the resulting JSON will include the base64-encoded
    // contents in the "data" property of each attachment.
    if (opts.attachments) {
      params.push('attachments=true');
    }

    // If it exists, add the opts.rev value to the list of parameters.
    // If rev is given a revision number then get the specified revision.
    if (opts.rev) {
      params.push('rev=' + opts.rev);
    }

    // If it exists, add the opts.conflicts value to the list of parameters.
    // If conflicts=true then the resulting JSON will include the field
    // _conflicts containing all the conflicting revisions.
    if (opts.conflicts) {
      params.push('conflicts=' + opts.conflicts);
    }

    // Format the list of parameters into a valid URI query string
    params = params.join('&');
    params = params === '' ? '' : '?' + params;

    if (opts.auto_encode) {
      id = encodeDocId(id);
    }

    // Set the options for the ajax call
    var options = {
      headers: host.headers,
      method: 'GET',
      url: genDBUrl(host, id + params)
    };

    // If the given id contains at least one '/' and the part before the '/'
    // is NOT "_design" and is NOT "_local"
    // OR
    // If the given id contains at least two '/' and the part before the first
    // '/' is "_design".
    // TODO This second condition seems strange since if parts[0] === '_design'
    // then we already know that parts[0] !== '_local'.
    var parts = id.split('/');
    if ((parts.length > 1 && parts[0] !== '_design' && parts[0] !== '_local') ||
        (parts.length > 2 && parts[0] === '_design' && parts[0] !== '_local')) {
      // Binary is expected back from the server
      options.binary = true;
    }

    // Get the document
    ajax(options, function(err, doc, xhr) {
      // If the document does not exist, send an error to the callback
      if (err) {
        return PouchUtils.call(callback, err);
      }

      // Send the document to the callback
      PouchUtils.call(callback, null, doc, xhr);
    });
  };

  // Delete the document given by doc from the database given by host.
  api.remove = function(doc, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('remove', arguments);
      return;
    }
    // If no options were given, set the callback to be the second parameter
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }

    // Delete the document
    ajax({
      headers: host.headers,
      method:'DELETE',
      url: genDBUrl(host, encodeDocId(doc._id)) + '?rev=' + doc._rev
    }, callback);
  };

  // Get the attachment
  api.getAttachment = function(docId, attachmentId, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (opts.auto_encode === undefined) {
      opts.auto_encode = true;
    }
    if (opts.auto_encode) {
      docId = encodeDocId(docId);
    }
    opts.auto_encode = false;
    api.get(docId + '/' + attachmentId, opts, callback);
  };

  // Remove the attachment given by the id and rev
  api.removeAttachment = function(docId, attachmentId, rev, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('removeAttachment', arguments);
      return;
    }
    ajax({
      headers: host.headers,
      method: 'DELETE',
      url: genDBUrl(host, encodeDocId(docId) + '/' + attachmentId) + '?rev=' + rev
    }, callback);
  };

  // Add the attachment given by blob and its contentType property
  // to the document with the given id, the revision given by rev, and
  // add it to the database given by host.
  api.putAttachment = function(docId, attachmentId, rev, blob, type, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('putAttachment', arguments);
      return;
    }
    if (typeof type === 'function') {
      callback = type;
      type = blob;
      blob = rev;
      rev = null;
    }
    if (typeof type === 'undefined') {
      type = blob;
      blob = rev;
      rev = null;
    }
    var id = encodeDocId(docId) + '/' + attachmentId;
    var url = genDBUrl(host, id);
    if (rev) {
      url += '?rev=' + rev;
    }

    var opts = {
      headers: host.headers,
      method:'PUT',
      url: url,
      processData: false,
      body: blob,
      timeout: 60000
    };
    opts.headers['Content-Type'] = type;
    // Add the attachment
    ajax(opts, callback);
  };

  // Add the document given by doc (in JSON string format) to the database
  // given by host. This fails if the doc has no _id field.
  api.put = function(doc, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('put', arguments);
      return;
    }
    // If no options were given, set the callback to be the second parameter
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (typeof doc !== 'object') {
      return PouchUtils.call(callback, Pouch.Errors.NOT_AN_OBJECT);
    }
    if (!('_id' in doc)) {
      return PouchUtils.call(callback, Pouch.Errors.MISSING_ID);
    }

    // List of parameter to add to the PUT request
    var params = [];

    // If it exists, add the opts.new_edits value to the list of parameters.
    // If new_edits = false then the database will NOT assign this document a
    // new revision number
    if (opts && typeof opts.new_edits !== 'undefined') {
      params.push('new_edits=' + opts.new_edits);
    }

    // Format the list of parameters into a valid URI query string
    params = params.join('&');
    if (params !== '') {
      params = '?' + params;
    }

    // Add the document
    ajax({
      headers: host.headers,
      method: 'PUT',
      url: genDBUrl(host, encodeDocId(doc._id)) + params,
      body: doc
    }, callback);
  };

  // Add the document given by doc (in JSON string format) to the database
  // given by host. This does not assume that doc is a new document (i.e. does not
  // have a _id or a _rev field.
  api.post = function(doc, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('post', arguments);
      return;
    }
    // If no options were given, set the callback to be the second parameter
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (typeof doc !== 'object') {
      return PouchUtils.call(callback, Pouch.Errors.NOT_AN_OBJECT);
    }
    if (! ("_id" in doc)) {
      if (uuids.list.length > 0) {
        doc._id = uuids.list.pop();
        api.put(doc, opts, callback);
      }else {
        uuids.get(function(err, resp) {
          if (err) {
            return PouchUtils.call(callback, Pouch.Errors.UNKNOWN_ERROR);
          }
          doc._id = uuids.list.pop();
          api.put(doc, opts, callback);
        });
      }
    } else {
      api.put(doc, opts, callback);
    }
  };

  // Update/create multiple documents given by req in the database
  // given by host.
  api.bulkDocs = function(req, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('bulkDocs', arguments);
      return;
    }
    // If no options were given, set the callback to be the second parameter
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }
    if (!opts) {
      opts = {};
    }

    // If opts.new_edits exists add it to the document data to be
    // send to the database.
    // If new_edits=false then it prevents the database from creating
    // new revision numbers for the documents. Instead it just uses
    // the old ones. This is used in database replication.
    if (typeof opts.new_edits !== 'undefined') {
      req.new_edits = opts.new_edits;
    }

    // Update/create the documents
    ajax({
      headers: host.headers,
      method:'POST',
      url: genDBUrl(host, '_bulk_docs'),
      body: req
    }, callback);
  };

  // Get a listing of the documents in the database given
  // by host and ordered by increasing id.
  api.allDocs = function(opts, callback) {
    // If no options were given, set the callback to be the second parameter
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('allDocs', arguments);
      return;
    }
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }

    // List of parameters to add to the GET request
    var params = [];
    var body;
    var method = 'GET';

    // TODO I don't see conflicts as a valid parameter for a
    // _all_docs request (see http://wiki.apache.org/couchdb/HTTP_Document_API#all_docs)
    if (opts.conflicts) {
      params.push('conflicts=true');
    }

    // If opts.descending is truthy add it to params
    if (opts.descending) {
      params.push('descending=true');
    }

    // If opts.include_docs exists, add the include_docs value to the
    // list of parameters.
    // If include_docs=true then include the associated document with each
    // result.
    if (opts.include_docs) {
      params.push('include_docs=true');
    }

    // If opts.startkey exists, add the startkey value to the list of
    // parameters.
    // If startkey is given then the returned list of documents will
    // start with the document whose id is startkey.
    if (opts.startkey) {
      params.push('startkey=' +
                  encodeURIComponent(JSON.stringify(opts.startkey)));
    }

    // If opts.endkey exists, add the endkey value to the list of parameters.
    // If endkey is given then the returned list of docuemnts will
    // end with the document whose id is endkey.
    if (opts.endkey) {
      params.push('endkey=' + encodeURIComponent(JSON.stringify(opts.endkey)));
    }

    // If opts.limit exists, add the limit value to the parameter list.
    if (opts.limit) {
      params.push('limit=' + opts.limit);
    }

    if (typeof opts.skip !== 'undefined') {
      params.push('skip=' + opts.skip);
    }

    // Format the list of parameters into a valid URI query string
    params = params.join('&');
    if (params !== '') {
      params = '?' + params;
    }

    // If keys are supplied, issue a POST request to circumvent GET query string limits
    // see http://wiki.apache.org/couchdb/HTTP_view_API#Querying_Options
    if (typeof opts.keys !== 'undefined') {
      method = 'POST';
      body = JSON.stringify({keys:opts.keys});
    }

    // Get the document listing
    ajax({
      headers: host.headers,
      method: method,
      url: genDBUrl(host, '_all_docs' + params),
      body: body
    }, callback);
  };

  // Get a list of changes made to documents in the database given by host.
  // TODO According to the README, there should be two other methods here,
  // api.changes.addListener and api.changes.removeListener.
  api.changes = function(opts) {

    // We internally page the results of a changes request, this means
    // if there is a large set of changes to be returned we can start
    // processing them quicker instead of waiting on the entire
    // set of changes to return and attempting to process them at once
    var CHANGES_LIMIT = 25;

    if (!api.taskqueue.ready()) {
      var task = api.taskqueue.addTask('changes', arguments);
      return {
        cancel: function() {
          if (task.task) {
            return task.task.cancel();
          }
          if (Pouch.DEBUG) {
            console.log(db_url + ': Cancel Changes Feed');
          }
          task.parameters[0].aborted = true;
        }
      };
    }
    
    if (opts.since === 'latest') {
      var changes;
      api.info(function (err, info) {
        if (!opts.aborted) {
          opts.since = info.update_seq;
          changes = api.changes(opts);
        }
      });
      // Return a method to cancel this method from processing any more
      return {
        cancel: function() {
          if (changes) {
            return changes.cancel();
          }
          if (Pouch.DEBUG) {
            console.log(db_url + ': Cancel Changes Feed');
          }
          opts.aborted = true;
        }
      };
    }

    if (Pouch.DEBUG) {
      console.log(db_url + ': Start Changes Feed: continuous=' + opts.continuous);
    }

    var params = {};
    var limit = (typeof opts.limit !== 'undefined') ? opts.limit : false;
    if (limit === 0) {
      limit = 1;
    }
    //
    var leftToFetch = limit;

    if (opts.style) {
      params.style = opts.style;
    }

    if (opts.include_docs || opts.filter && typeof opts.filter === 'function') {
      params.include_docs = true;
    }

    if (opts.continuous) {
      params.feed = 'longpoll';
    }

    if (opts.conflicts) {
      params.conflicts = true;
    }

    if (opts.descending) {
      params.descending = true;
    }

    if (opts.filter && typeof opts.filter === 'string') {
      params.filter = opts.filter;
    }

    // If opts.query_params exists, pass it through to the changes request.
    // These parameters may be used by the filter on the source database.
    if (opts.query_params && typeof opts.query_params === 'object') {
      for (var param_name in opts.query_params) {
        if (opts.query_params.hasOwnProperty(param_name)) {
          params[param_name] = opts.query_params[param_name];
        }
      }
    }

    var xhr;
    var lastFetchedSeq;
    var remoteLastSeq;
    var pagingCount;

    // Get all the changes starting wtih the one immediately after the
    // sequence number given by since.
    var fetch = function(since, callback) {
      params.since = since;
      if (!opts.continuous && !pagingCount) {
        pagingCount = remoteLastSeq;
      }
      params.limit = (!limit || leftToFetch > CHANGES_LIMIT) ?
        CHANGES_LIMIT : leftToFetch;

      var paramStr = '?' + Object.keys(params).map(function(k) {
        return k + '=' + params[k];
      }).join('&');

      // Set the options for the ajax call
      var xhrOpts = {
        headers: host.headers, method:'GET',
        url: genDBUrl(host, '_changes' + paramStr),
        // _changes can take a long time to generate, especially when filtered
        timeout: null
      };
      lastFetchedSeq = since;

      if (opts.aborted) {
        return;
      }

      // Get the changes
      xhr = ajax(xhrOpts, callback);
    };

    // If opts.since exists, get all the changes from the sequence
    // number given by opts.since. Otherwise, get all the changes
    // from the sequence number 0.
    var fetchTimeout = 10;
    var fetchRetryCount = 0;

    var results = {results: []};

    var fetched = function(err, res) {
      // If the result of the ajax call (res) contains changes (res.results)
      if (res && res.results) {
        results.last_seq = res.last_seq;
        // For each change
        var req = {};
        req.query = opts.query_params;
        res.results = res.results.filter(function(c) {
          leftToFetch--;
          var ret = PouchUtils.filterChange(opts)(c);
          if (ret) {
            results.results.push(c);
            PouchUtils.call(opts.onChange, c);
          }
          return ret;
        });
      }

      // The changes feed may have timed out with no results
      // if so reuse last update sequence
      if (res && res.last_seq) {
        lastFetchedSeq = res.last_seq;
      }

      var resultsLength = res && res.results.length || 0;

      pagingCount -= CHANGES_LIMIT;

      var finished = (limit && leftToFetch <= 0) ||
        (res && !resultsLength && pagingCount <= 0) ||
        (resultsLength && res.last_seq === remoteLastSeq) ||
        (opts.descending && lastFetchedSeq !== 0);

      if (opts.continuous || !finished) {
        // Increase retry delay exponentially as long as errors persist
        if (err) {
          fetchRetryCount += 1;
        } else {
          fetchRetryCount = 0;
        }
        var timeoutMultiplier = 1 << fetchRetryCount;
        var retryWait = fetchTimeout * timeoutMultiplier;
        var maximumWait = opts.maximumWait || 30000;

        if (retryWait > maximumWait) {
          PouchUtils.call(opts.complete, err || Pouch.Errors.UNKNOWN_ERROR, null);
        }

        // Queue a call to fetch again with the newest sequence number
        setTimeout(function() { fetch(lastFetchedSeq, fetched); }, retryWait);
      } else {
        // We're done, call the callback
        PouchUtils.call(opts.complete, null, results);
      }
    };

    // If we arent doing a continuous changes request we need to know
    // the current update_seq so we know when to stop processing the
    // changes
    if (opts.continuous) {
      fetch(opts.since || 0, fetched);
    } else {
      api.info(function(err, res) {
        if (err) {
          return PouchUtils.call(opts.complete, err);
        }
        remoteLastSeq = res.update_seq;
        fetch(opts.since || 0, fetched);
      });
    }

    // Return a method to cancel this method from processing any more
    return {
      cancel: function() {
        if (Pouch.DEBUG) {
          console.log(db_url + ': Cancel Changes Feed');
        }
        opts.aborted = true;
        xhr.abort();
      }
    };
  };

  // Given a set of document/revision IDs (given by req), tets the subset of
  // those that do NOT correspond to revisions stored in the database.
  // See http://wiki.apache.org/couchdb/HttpPostRevsDiff
  api.revsDiff = function(req, opts, callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('revsDiff', arguments);
      return;
    }
    // If no options were given, set the callback to be the second parameter
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }

    // Get the missing document/revision IDs
    ajax({
      headers: host.headers,
      method:'POST',
      url: genDBUrl(host, '_revs_diff'),
      body: req
    }, function(err, res) {
      PouchUtils.call(callback, err, res);
    });
  };

  api.close = function(callback) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('close', arguments);
      return;
    }
    PouchUtils.call(callback, null);
  };

  api.replicateOnServer = function(target, opts, promise) {
    if (!api.taskqueue.ready()) {
      api.taskqueue.addTask('replicateOnServer', arguments);
      return promise;
    }
    
    var targetHost = getHost(target.id());
    var params = {
      source: host.db,
      target: targetHost.protocol === host.protocol && targetHost.authority === host.authority ? targetHost.db : targetHost.source
    };

    if (opts.continuous) {
      params.continuous = true;
    }

    if (opts.create_target) {
      params.create_target = true;
    }

    if (opts.doc_ids) {
      params.doc_ids = opts.doc_ids;
    }

    if (opts.filter && typeof opts.filter === 'string') {
      params.filter = opts.filter;
    }

    if (opts.query_params) {
      params.query_params = opts.query_params;
    }

    var result = {};
    var repOpts = {
      headers: host.headers,
      method: 'POST',
      url: host.protocol + '://' + host.host + (host.port === 80 ? '' : (':' + host.port)) + '/_replicate',
      body: params
    };
    var xhr;
    promise.cancel = function() {
      this.cancelled = true;
      if (xhr && !result.ok) {
        xhr.abort();
      }
      if (result._local_id) {
        repOpts.body = {
          replication_id: result._local_id
        };
      }
      repOpts.body.cancel = true;
      ajax(repOpts, function(err, resp, xhr) {
        // If the replication cancel request fails, send an error to the callback
        if (err) {
          return PouchUtils.call(callback, err);
        }
        // Send the replication cancel result to the complete callback
        PouchUtils.call(opts.complete, null, result, xhr);
      });
    };

    if (promise.cancelled) {
      return;
    }

    xhr = ajax(repOpts, function(err, resp, xhr) {
      // If the replication fails, send an error to the callback
      if (err) {
        return PouchUtils.call(callback, err);
      }

      result.ok = true;

      // Provided by CouchDB from 1.2.0 onward to cancel replication
      if (resp._local_id) {
        result._local_id = resp._local_id;
      }

      // Send the replication result to the complete callback
      PouchUtils.call(opts.complete, null, resp, xhr);
    });
  };

  return api;
}

// Delete the HttpPouch specified by the given name.
HttpPouch.destroy = function(name, opts, callback) {
  var host = getHost(name, opts);
  opts = opts || {};
  if (typeof opts === 'function') {
    callback = opts;
    opts = {};
  }
  opts.headers = host.headers;
  opts.method = 'DELETE';
  opts.url = genDBUrl(host, '');
  PouchUtils.ajax(opts, callback);
};

// HttpPouch is a valid adapter.
HttpPouch.valid = function() {
  return true;
};

// Set HttpPouch to be the adapter used with the http scheme.
Pouch.adapter('http', HttpPouch);
Pouch.adapter('https', HttpPouch);

/*globals PouchUtils: true, PouchMerge */

'use strict';

var PouchUtils;

if (typeof module !== 'undefined' && module.exports) {
  PouchUtils = require('../pouch.utils.js');
}

var idbError = function(callback) {
  return function(event) {
    PouchUtils.call(callback, {
      status: 500,
      error: event.type,
      reason: event.target
    });
  };
};

var IdbPouch = function(opts, callback) {

  // IndexedDB requires a versioned database structure, this is going to make
  // it hard to dynamically create object stores if we needed to for things
  // like views
  var POUCH_VERSION = 1;

  // The object stores created for each database
  // DOC_STORE stores the document meta data, its revision history and state
  var DOC_STORE = 'document-store';
  // BY_SEQ_STORE stores a particular version of a document, keyed by its
  // sequence id
  var BY_SEQ_STORE = 'by-sequence';
  // Where we store attachments
  var ATTACH_STORE = 'attach-store';
  // Where we store meta data
  var META_STORE = 'meta-store';
  // Where we detect blob support
  var DETECT_BLOB_SUPPORT_STORE = 'detect-blob-support';


  var name = opts.name;
  var req = window.indexedDB.open(name, POUCH_VERSION);

  if (Pouch.openReqList) {
    Pouch.openReqList[name] = req;
  }

  var blobSupport = null;

  var instanceId = null;
  var api = {};
  var idb = null;

  if (Pouch.DEBUG) {
    console.log(name + ': Open Database');
  }

  req.onupgradeneeded = function(e) {
    var db = e.target.result;
    var currentVersion = e.oldVersion;
    while (currentVersion !== e.newVersion) {
      if (currentVersion === 0) {
        createSchema(db);
      }
      currentVersion++;
    }
  };

  function createSchema(db) {
    db.createObjectStore(DOC_STORE, {keyPath : 'id'})
      .createIndex('seq', 'seq', {unique: true});
    db.createObjectStore(BY_SEQ_STORE, {autoIncrement : true})
      .createIndex('_doc_id_rev', '_doc_id_rev', {unique: true});
    db.createObjectStore(ATTACH_STORE, {keyPath: 'digest'});
    db.createObjectStore(META_STORE, {keyPath: 'id', autoIncrement: false});
    db.createObjectStore(DETECT_BLOB_SUPPORT_STORE);
  }

  // From http://stackoverflow.com/questions/14967647/encode-decode-image-with-base64-breaks-image (2013-04-21)
  function fixBinary(bin) {
    var length = bin.length;
    var buf = new ArrayBuffer(length);
    var arr = new Uint8Array(buf);
    for (var i = 0; i < length; i++) {
      arr[i] = bin.charCodeAt(i);
    }
    return buf;
  }

  req.onsuccess = function(e) {

    idb = e.target.result;

    var txn = idb.transaction([META_STORE, DETECT_BLOB_SUPPORT_STORE],
                              'readwrite');

    idb.onversionchange = function() {
      idb.close();
    };

    // polyfill the new onupgradeneeded api for chrome. can get rid of when
    // saucelabs moves to chrome 23
    if (idb.setVersion && Number(idb.version) !== POUCH_VERSION) {
      var versionReq = idb.setVersion(POUCH_VERSION);
      versionReq.onsuccess = function(evt) {
        function setVersionComplete() {
          req.onsuccess(e);
        }
        evt.target.result.oncomplete = setVersionComplete;
        req.onupgradeneeded(e);
      };
      return;
    }

    var req = txn.objectStore(META_STORE).get(META_STORE);

    req.onsuccess = function(e) {
      var meta = e.target.result || {id: META_STORE};
      if (name + '_id' in meta) {
        instanceId = meta[name + '_id'];
      } else {
        instanceId = Pouch.uuid();
        meta[name + '_id'] = instanceId;
        txn.objectStore(META_STORE).put(meta);
      }

      // detect blob support
      try {
        txn.objectStore(DETECT_BLOB_SUPPORT_STORE).put(PouchUtils.createBlob(), "key");
        blobSupport = true;
      } catch (err) {
        blobSupport = false;
      } finally {
        PouchUtils.call(callback, null, api);
      }
    };
  };

  req.onerror = idbError(callback);

  api.type = function() {
    return 'idb';
  };

  // Each database needs a unique id so that we can store the sequence
  // checkpoint without having other databases confuse itself.
  api.id = function idb_id() {
    return instanceId;
  };

  api._bulkDocs = function idb_bulkDocs(req, opts, callback) {
    var newEdits = opts.new_edits;
    var userDocs = req.docs;
    // Parse the docs, give them a sequence number for the result
    var docInfos = userDocs.map(function(doc, i) {
      var newDoc = PouchUtils.parseDoc(doc, newEdits);
      newDoc._bulk_seq = i;
      return newDoc;
    });

    var docInfoErrors = docInfos.filter(function(docInfo) {
      return docInfo.error;
    });
    if (docInfoErrors.length) {
      return PouchUtils.call(callback, docInfoErrors[0]);
    }

    var results = [];
    var docsWritten = 0;

    function writeMetaData(e) {
      var meta = e.target.result;
      meta.updateSeq = (meta.updateSeq || 0) + docsWritten;
      txn.objectStore(META_STORE).put(meta);
    }

    function processDocs() {
      if (!docInfos.length) {
        txn.objectStore(META_STORE).get(META_STORE).onsuccess = writeMetaData;
        return;
      }
      var currentDoc = docInfos.shift();
      var req = txn.objectStore(DOC_STORE).get(currentDoc.metadata.id);
      req.onsuccess = function process_docRead(event) {
        var oldDoc = event.target.result;
        if (!oldDoc) {
          insertDoc(currentDoc);
        } else {
          updateDoc(oldDoc, currentDoc);
        }
      };
    }

    function complete(event) {
      var aresults = [];
      results.sort(sortByBulkSeq);
      results.forEach(function(result) {
        delete result._bulk_seq;
        if (result.error) {
          aresults.push(result);
          return;
        }
        var metadata = result.metadata;
        var rev = PouchMerge.winningRev(metadata);

        aresults.push({
          ok: true,
          id: metadata.id,
          rev: rev
        });

        if (PouchUtils.isLocalId(metadata.id)) {
          return;
        }

        IdbPouch.Changes.notify(name);
        IdbPouch.Changes.notifyLocalWindows(name);
      });
      PouchUtils.call(callback, null, aresults);
    }

    function preprocessAttachment(att, finish) {
      if (att.stub) {
        return finish();
      }
      if (typeof att.data === 'string') {
        var data;
        try {
          data = atob(att.data);
        } catch (e) {
          var err = Pouch.error(Pouch.Errors.BAD_ARG,
                                "Attachments need to be base64 encoded");
          return PouchUtils.call(callback, err);
        }
        att.digest = 'md5-' + PouchUtils.Crypto.MD5(data);
        if (blobSupport) {
          var type = att.content_type;
          data = fixBinary(data);
          att.data = PouchUtils.createBlob([data], {type: type});
        }
        return finish();
      }
      var reader = new FileReader();
      reader.onloadend = function(e) {
        att.digest = 'md5-' + PouchUtils.Crypto.MD5(this.result);
        if (!blobSupport) {
          att.data = btoa(this.result);
        }
        finish();
      };
      reader.readAsBinaryString(att.data);
    }

    function preprocessAttachments(callback) {
      if (!docInfos.length) {
        return callback();
      }

      var docv = 0;
      docInfos.forEach(function(docInfo) {
        var attachments = docInfo.data && docInfo.data._attachments ?
          Object.keys(docInfo.data._attachments) : [];

        if (!attachments.length) {
          return done();
        }

        var recv = 0;
        function attachmentProcessed() {
          recv++;
          if (recv === attachments.length) {
            done();
          }
        }

        for (var key in docInfo.data._attachments) {
          preprocessAttachment(docInfo.data._attachments[key], attachmentProcessed);
        }
      });

      function done() {
        docv++;
        if (docInfos.length === docv) {
          callback();
        }
      }
    }

    function writeDoc(docInfo, callback) {
      var err = null;
      var recv = 0;
      docInfo.data._id = docInfo.metadata.id;
      docInfo.data._rev = docInfo.metadata.rev;

      docsWritten++;

      if (PouchUtils.isDeleted(docInfo.metadata, docInfo.metadata.rev)) {
        docInfo.data._deleted = true;
      }

      var attachments = docInfo.data._attachments ?
        Object.keys(docInfo.data._attachments) : [];

      function collectResults(attachmentErr) {
        if (!err) {
          if (attachmentErr) {
            err = attachmentErr;
            PouchUtils.call(callback, err);
          } else if (recv === attachments.length) {
            finish();
          }
        }
      }

      function attachmentSaved(err) {
        recv++;
        collectResults(err);
      }

      for (var key in docInfo.data._attachments) {
        if (!docInfo.data._attachments[key].stub) {
          var data = docInfo.data._attachments[key].data;
          delete docInfo.data._attachments[key].data;
          var digest = docInfo.data._attachments[key].digest;
          saveAttachment(docInfo, digest, data, attachmentSaved);
        } else {
          recv++;
          collectResults();
        }
      }

      function finish() {
        docInfo.data._doc_id_rev = docInfo.data._id + "::" + docInfo.data._rev;
        var dataReq = txn.objectStore(BY_SEQ_STORE).put(docInfo.data);
        dataReq.onsuccess = function(e) {
          if (Pouch.DEBUG) {
            console.log(name + ': Wrote Document ', docInfo.metadata.id);
          }
          docInfo.metadata.seq = e.target.result;
          // Current _rev is calculated from _rev_tree on read
          delete docInfo.metadata.rev;
          var metaDataReq = txn.objectStore(DOC_STORE).put(docInfo.metadata);
          metaDataReq.onsuccess = function() {
            results.push(docInfo);
            PouchUtils.call(callback);
          };
        };
      }

      if (!attachments.length) {
        finish();
      }
    }

    function updateDoc(oldDoc, docInfo) {
      var merged = PouchMerge.merge(oldDoc.rev_tree, docInfo.metadata.rev_tree[0], 1000);
      var wasPreviouslyDeleted = PouchUtils.isDeleted(oldDoc);
      var inConflict = (wasPreviouslyDeleted &&
                        PouchUtils.isDeleted(docInfo.metadata)) ||
        (!wasPreviouslyDeleted && newEdits && merged.conflicts !== 'new_leaf');

      if (inConflict) {
        results.push(makeErr(Pouch.Errors.REV_CONFLICT, docInfo._bulk_seq));
        return processDocs();
      }

      docInfo.metadata.rev_tree = merged.tree;
      writeDoc(docInfo, processDocs);
    }

    function insertDoc(docInfo) {
      // Cant insert new deleted documents
      if ('was_delete' in opts && PouchUtils.isDeleted(docInfo.metadata)) {
        results.push(Pouch.Errors.MISSING_DOC);
        return processDocs();
      }
      writeDoc(docInfo, processDocs);
    }

    // Insert sequence number into the error so we can sort later
    function makeErr(err, seq) {
      err._bulk_seq = seq;
      return err;
    }

    function saveAttachment(docInfo, digest, data, callback) {
      var objectStore = txn.objectStore(ATTACH_STORE);
      var getReq = objectStore.get(digest).onsuccess = function(e) {
        var originalRefs = e.target.result && e.target.result.refs || {};
        var ref = [docInfo.metadata.id, docInfo.metadata.rev].join('@');
        var newAtt = {
          digest: digest,
          body: data,
          refs: originalRefs
        };
        newAtt.refs[ref] = true;
        var putReq = objectStore.put(newAtt).onsuccess = function(e) {
          PouchUtils.call(callback);
        };
      };
    }

    var txn;
    preprocessAttachments(function() {
      txn = idb.transaction([DOC_STORE, BY_SEQ_STORE, ATTACH_STORE, META_STORE],
                            'readwrite');
      txn.onerror = idbError(callback);
      txn.ontimeout = idbError(callback);
      txn.oncomplete = complete;

      processDocs();
    });
  };

  function sortByBulkSeq(a, b) {
    return a._bulk_seq - b._bulk_seq;
  }

  // First we look up the metadata in the ids database, then we fetch the
  // current revision(s) from the by sequence store
  api._get = function idb_get(id, opts, callback) {
    var doc;
    var metadata;
    var err;
    var txn;
    if (opts.ctx) {
      txn = opts.ctx;
    } else {
      txn = idb.transaction([DOC_STORE, BY_SEQ_STORE, ATTACH_STORE], 'readonly');
    }

    function finish(){
      PouchUtils.call(callback, err, {doc: doc, metadata: metadata, ctx: txn});
    }

    txn.objectStore(DOC_STORE).get(id).onsuccess = function(e) {
      metadata = e.target.result;
      // we can determine the result here if:
      // 1. there is no such document
      // 2. the document is deleted and we don't ask about specific rev
      // When we ask with opts.rev we expect the answer to be either
      // doc (possibly with _deleted=true) or missing error
      if (!metadata) {
        err = Pouch.Errors.MISSING_DOC;
        return finish();
      }
      if (PouchUtils.isDeleted(metadata) && !opts.rev) {
        err = Pouch.error(Pouch.Errors.MISSING_DOC, "deleted");
        return finish();
      }

      var rev = PouchMerge.winningRev(metadata);
      var key = metadata.id + '::' + (opts.rev ? opts.rev : rev);
      var index = txn.objectStore(BY_SEQ_STORE).index('_doc_id_rev');

      index.get(key).onsuccess = function(e) {
        doc = e.target.result;
        if(doc && doc._doc_id_rev) {
          delete(doc._doc_id_rev);
        }
        if (!doc) {
          err = Pouch.Errors.MISSING_DOC;
          return finish();
        }
        finish();
      };
    };
  };

  api._getAttachment = function(attachment, opts, callback) {
    var result;
    var txn;
    if (opts.ctx) {
      txn = opts.ctx;
    } else {
      txn = idb.transaction([DOC_STORE, BY_SEQ_STORE, ATTACH_STORE], 'readonly');
    }
    var digest = attachment.digest;
    var type = attachment.content_type;

    txn.objectStore(ATTACH_STORE).get(digest).onsuccess = function(e) {
      var data = e.target.result.body;
      if (opts.encode) {
        if (blobSupport) {
          var reader = new FileReader();
          reader.onloadend = function(e) {
            result = btoa(this.result);
            PouchUtils.call(callback, null, result);
          };
          reader.readAsBinaryString(data);
        } else {
          result = data;
          PouchUtils.call(callback, null, result);
        }
      } else {
        if (blobSupport) {
          result = data;
        } else {
          data = fixBinary(atob(data));
          result = PouchUtils.createBlob([data], {type: type});
        }
        PouchUtils.call(callback, null, result);
      }
    };
  };

  api._allDocs = function idb_allDocs(opts, callback) {
    var start = 'startkey' in opts ? opts.startkey : false;
    var end = 'endkey' in opts ? opts.endkey : false;

    var descending = 'descending' in opts ? opts.descending : false;
    descending = descending ? 'prev' : null;

    var keyRange = start && end ? window.IDBKeyRange.bound(start, end)
      : start ? window.IDBKeyRange.lowerBound(start)
      : end ? window.IDBKeyRange.upperBound(end) : null;

    var transaction = idb.transaction([DOC_STORE, BY_SEQ_STORE], 'readonly');
    transaction.oncomplete = function() {
      if ('keys' in opts) {
        opts.keys.forEach(function(key) {
          if (key in resultsMap) {
            results.push(resultsMap[key]);
          } else {
            results.push({"key": key, "error": "not_found"});
          }
        });
        if (opts.descending) {
          results.reverse();
        }
      }
      PouchUtils.call(callback, null, {
        total_rows: results.length,
        offset: opts.skip,
        rows: ('limit' in opts) ? results.slice(opts.skip, opts.limit + opts.skip) :
          (opts.skip > 0) ? results.slice(opts.skip) : results
      });
    };

    var oStore = transaction.objectStore(DOC_STORE);
    var oCursor = descending ? oStore.openCursor(keyRange, descending)
      : oStore.openCursor(keyRange);
    var results = [];
    var resultsMap = {};
    oCursor.onsuccess = function(e) {
      if (!e.target.result) {
        return;
      }
      var cursor = e.target.result;
      var metadata = cursor.value;
      // If opts.keys is set we want to filter here only those docs with
      // key in opts.keys. With no performance tests it is difficult to
      // guess if iteration with filter is faster than many single requests
      function allDocsInner(metadata, data) {
        if (PouchUtils.isLocalId(metadata.id)) {
          return cursor['continue']();
        }
        var doc = {
          id: metadata.id,
          key: metadata.id,
          value: {
            rev: PouchMerge.winningRev(metadata)
          }
        };
        if (opts.include_docs) {
          doc.doc = data;
          doc.doc._rev = PouchMerge.winningRev(metadata);
          if (doc.doc._doc_id_rev) {
              delete(doc.doc._doc_id_rev);
          }
          if (opts.conflicts) {
            doc.doc._conflicts = PouchMerge.collectConflicts(metadata);
          }
          for (var att in doc.doc._attachments) {
            doc.doc._attachments[att].stub = true;
          }
        }
        if ('keys' in opts) {
          if (opts.keys.indexOf(metadata.id) > -1) {
            if (PouchUtils.isDeleted(metadata)) {
              doc.value.deleted = true;
              doc.doc = null;
            }
            resultsMap[doc.id] = doc;
          }
        } else {
          if (!PouchUtils.isDeleted(metadata)) {
            results.push(doc);
          }
        }
        cursor['continue']();
      }

      if (!opts.include_docs) {
        allDocsInner(metadata);
      } else {
        var index = transaction.objectStore(BY_SEQ_STORE).index('_doc_id_rev');
        var mainRev = PouchMerge.winningRev(metadata);
        var key = metadata.id + "::" + mainRev;
        index.get(key).onsuccess = function(event) {
          allDocsInner(cursor.value, event.target.result);
        };
      }
    };
  };

  api._info = function idb_info(callback) {
    var count = 0;
    var update_seq = 0;
    var txn = idb.transaction([DOC_STORE, META_STORE], 'readonly');

    function fetchUpdateSeq(e) {
      update_seq = e.target.result && e.target.result.updateSeq || 0;
    }

    function countDocs(e) {
      var cursor = e.target.result;
      if (!cursor) {
        txn.objectStore(META_STORE).get(META_STORE).onsuccess = fetchUpdateSeq;
        return;
      }
      if (cursor.value.deleted !== true) {
        count++;
      }
      cursor['continue']();
    }

    txn.oncomplete = function() {
      callback(null, {
        db_name: name,
        doc_count: count,
        update_seq: update_seq
      });
    };

    txn.objectStore(DOC_STORE).openCursor().onsuccess = countDocs;
  };

  api._changes = function idb_changes(opts) {
    if (Pouch.DEBUG) {
      console.log(name + ': Start Changes Feed: continuous=' + opts.continuous);
    }

    if (opts.continuous) {
      var id = name + ':' + Pouch.uuid();
      opts.cancelled = false;
      IdbPouch.Changes.addListener(name, id, api, opts);
      IdbPouch.Changes.notify(name);
      return {
        cancel: function() {
          if (Pouch.DEBUG) {
            console.log(name + ': Cancel Changes Feed');
          }
          opts.cancelled = true;
          IdbPouch.Changes.removeListener(name, id);
        }
      };
    }

    var descending = opts.descending ? 'prev' : null;
    var last_seq = 0;

    // Ignore the `since` parameter when `descending` is true
    opts.since = opts.since && !descending ? opts.since : 0;

    var results = [], resultIndices = {}, dedupResults = [];
    var txn;

    function fetchChanges() {
      txn = idb.transaction([DOC_STORE, BY_SEQ_STORE]);
      txn.oncomplete = onTxnComplete;

      var req;

      if (descending) {
        req = txn.objectStore(BY_SEQ_STORE)
            .openCursor(window.IDBKeyRange.lowerBound(opts.since, true), descending);
      } else {
        req = txn.objectStore(BY_SEQ_STORE)
            .openCursor(window.IDBKeyRange.lowerBound(opts.since, true));
      }

      req.onsuccess = onsuccess;
      req.onerror = onerror;
    }

    if (opts.filter && typeof opts.filter === 'string') {
      var filterName = opts.filter.split('/');
      api.get('_design/' + filterName[0], function(err, ddoc) {
        /*jshint evil: true */
        var filter = eval('(function() { return ' +
                          ddoc.filters[filterName[1]] + ' })()');
        opts.filter = filter;
        fetchChanges();
      });
    } else {
      fetchChanges();
    }

    function onsuccess(event) {
      if (!event.target.result) {
        // Filter out null results casued by deduping
        for (var i = 0, l = results.length; i < l; i++ ) {
          var result = results[i];
          if (result) {
            dedupResults.push(result);
          }
        }
        return false;
      }

      var cursor = event.target.result;

      // Try to pre-emptively dedup to save us a bunch of idb calls
      var changeId = cursor.value._id;
      var changeIdIndex = resultIndices[changeId];
      if (changeIdIndex !== undefined) {
        results[changeIdIndex].seq = cursor.key;
        // update so it has the later sequence number
        results.push(results[changeIdIndex]);
        results[changeIdIndex] = null;
        resultIndices[changeId] = results.length - 1;
        return cursor['continue']();
      }

      var index = txn.objectStore(DOC_STORE);
      index.get(cursor.value._id).onsuccess = function(event) {
        var metadata = event.target.result;
        if (PouchUtils.isLocalId(metadata.id)) {
          return cursor['continue']();
        }

        if(last_seq < metadata.seq){
          last_seq = metadata.seq;
        }

        var mainRev = PouchMerge.winningRev(metadata);
        var key = metadata.id + "::" + mainRev;
        var index = txn.objectStore(BY_SEQ_STORE).index('_doc_id_rev');
        index.get(key).onsuccess = function(docevent) {
          var doc = docevent.target.result;
          delete doc['_doc_id_rev'];
          var changeList = [{rev: mainRev}];
          if (opts.style === 'all_docs') {
            changeList = PouchMerge.collectLeaves(metadata.rev_tree)
              .map(function(x) { return {rev: x.rev}; });
          }
          var change = {
            id: metadata.id,
            seq: cursor.key,
            changes: changeList,
            doc: doc
          };

          if (PouchUtils.isDeleted(metadata, mainRev)) {
            change.deleted = true;
          }
          if (opts.conflicts) {
            change.doc._conflicts = PouchMerge.collectConflicts(metadata);
          }

          // Dedupe the changes feed
          var changeId = change.id, changeIdIndex = resultIndices[changeId];
          if (changeIdIndex !== undefined) {
            results[changeIdIndex] = null;
          }
          results.push(change);
          resultIndices[changeId] = results.length - 1;
          cursor['continue']();
        };
      };
    }

    function onTxnComplete() {
      PouchUtils.processChanges(opts, dedupResults, last_seq);
    }

    function onerror(error) {
      // TODO: shouldn't we pass some params here?
      PouchUtils.call(opts.complete);
    }
  };

  api._close = function(callback) {
    if (idb === null) {
      return PouchUtils.call(callback, Pouch.Errors.NOT_OPEN);
    }

    // https://developer.mozilla.org/en-US/docs/IndexedDB/IDBDatabase#close
    // "Returns immediately and closes the connection in a separate thread..."
    idb.close();
    PouchUtils.call(callback, null);
  };

  api._getRevisionTree = function(docId, callback) {
    var txn = idb.transaction([DOC_STORE], 'readonly');
    var req = txn.objectStore(DOC_STORE).get(docId);
    req.onsuccess = function (event) {
      var doc = event.target.result;
      if (!doc) {
        PouchUtils.call(callback, Pouch.Errors.MISSING_DOC);
      } else {
        PouchUtils.call(callback, null, doc.rev_tree);
      }
    };
  };

  // This function removes revisions of document docId
  // which are listed in revs and sets this document
  // revision to to rev_tree
  api._doCompaction = function(docId, rev_tree, revs, callback) {
    var txn = idb.transaction([DOC_STORE, BY_SEQ_STORE], 'readwrite');

    var index = txn.objectStore(DOC_STORE);
    index.get(docId).onsuccess = function(event) {
      var metadata = event.target.result;
      metadata.rev_tree = rev_tree;

      var count = revs.length;
      revs.forEach(function(rev) {
        var index = txn.objectStore(BY_SEQ_STORE).index('_doc_id_rev');
        var key = docId + "::" + rev;
        index.getKey(key).onsuccess = function(e) {
          var seq = e.target.result;
          if (!seq) {
            return;
          }
          var req = txn.objectStore(BY_SEQ_STORE)['delete'](seq);

          count--;
          if (!count) {
            txn.objectStore(DOC_STORE).put(metadata);
          }
        };
      });
    };
    txn.oncomplete = function() {
      PouchUtils.call(callback);
    };
  };

  return api;
};

IdbPouch.valid = function idb_valid() {
  return typeof window !== 'undefined' && !!window.indexedDB;
};

IdbPouch.destroy = function idb_destroy(name, opts, callback) {
  if (Pouch.DEBUG) {
    console.log(name + ': Delete Database');
  }
  IdbPouch.Changes.clearListeners(name);

  //Close open request for "name" database to fix ie delay.
  if (Pouch.openReqList[name] && Pouch.openReqList[name].result) {
    Pouch.openReqList[name].result.close();
  }
  var req = window.indexedDB.deleteDatabase(name);

  req.onsuccess = function() {
    //Remove open request from the list.
    if (Pouch.openReqList[name]) {
      Pouch.openReqList[name] = null;
    }
    PouchUtils.call(callback, null);
  };

  req.onerror = idbError(callback);
};

IdbPouch.Changes = new PouchUtils.Changes();

Pouch.adapter('idb', IdbPouch);

/*globals PouchUtils: true, PouchMerge */

'use strict';

var PouchUtils;

if (typeof module !== 'undefined' && module.exports) {
  PouchUtils = require('../pouch.utils.js');
}

function quote(str) {
  return "'" + str + "'";
}

var POUCH_VERSION = 1;
var POUCH_SIZE = 5 * 1024 * 1024;

// The object stores created for each database
// DOC_STORE stores the document meta data, its revision history and state
var DOC_STORE = quote('document-store');
// BY_SEQ_STORE stores a particular version of a document, keyed by its
// sequence id
var BY_SEQ_STORE = quote('by-sequence');
// Where we store attachments
var ATTACH_STORE = quote('attach-store');
var META_STORE = quote('metadata-store');

var unknownError = function(callback) {
  return function(event) {
    PouchUtils.call(callback, {
      status: 500,
      error: event.type,
      reason: event.target
    });
  };
};

var webSqlPouch = function(opts, callback) {

  var api = {};
  var instanceId = null;
  var name = opts.name;

  var db = openDatabase(name, POUCH_VERSION, name, POUCH_SIZE);
  if (!db) {
    return PouchUtils.call(callback, Pouch.Errors.UNKNOWN_ERROR);
  }

  function dbCreated() {
    callback(null, api);
  }

  function setup(){
    db.transaction(function (tx) {
      var meta = 'CREATE TABLE IF NOT EXISTS ' + META_STORE +
        ' (update_seq, dbid)';
      var attach = 'CREATE TABLE IF NOT EXISTS ' + ATTACH_STORE +
        ' (digest, json, body BLOB)';
      var doc = 'CREATE TABLE IF NOT EXISTS ' + DOC_STORE +
        ' (id unique, seq, json, winningseq)';
      var seq = 'CREATE TABLE IF NOT EXISTS ' + BY_SEQ_STORE +
        ' (seq INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, doc_id_rev UNIQUE, json)';

      tx.executeSql(attach);
      tx.executeSql(doc);
      tx.executeSql(seq);
      tx.executeSql(meta);

      var updateseq = 'SELECT update_seq FROM ' + META_STORE;
      tx.executeSql(updateseq, [], function(tx, result) {
        if (!result.rows.length) {
          var initSeq = 'INSERT INTO ' + META_STORE + ' (update_seq) VALUES (?)';
          tx.executeSql(initSeq, [0]);
          return;
        }
      });

      var dbid = 'SELECT dbid FROM ' + META_STORE + ' WHERE dbid IS NOT NULL';
      tx.executeSql(dbid, [], function(tx, result) {
        if (!result.rows.length) {
          var initDb = 'UPDATE ' + META_STORE + ' SET dbid=?';
          instanceId = Pouch.uuid();
          tx.executeSql(initDb, [instanceId]);
          return;
        }
        instanceId = result.rows.item(0).dbid;
      });
    }, unknownError(callback), dbCreated);
  }
  if (PouchUtils.isCordova() && typeof window !== 'undefined') {
    //to wait until custom api is made in pouch.adapters before doing setup
    window.addEventListener(name + '_pouch', function cordova_init() {
      window.removeEventListener(name + '_pouch', cordova_init, false);
      setup();
    }, false);
  } else {
    setup();
  }


  api.type = function() {
    return 'websql';
  };

  api.id = function() {
    return instanceId;
  };

  api._info = function(callback) {
    db.transaction(function(tx) {
      var sql = 'SELECT COUNT(id) AS count FROM ' + DOC_STORE;
      tx.executeSql(sql, [], function(tx, result) {
        var doc_count = result.rows.item(0).count;
        var updateseq = 'SELECT update_seq FROM ' + META_STORE;
        tx.executeSql(updateseq, [], function(tx, result) {
          var update_seq = result.rows.item(0).update_seq;
          callback(null, {
            db_name: name,
            doc_count: doc_count,
            update_seq: update_seq
          });
        });
      });
    });
  };

  api._bulkDocs = function idb_bulkDocs(req, opts, callback) {

    var newEdits = opts.new_edits;
    var userDocs = req.docs;
    var docsWritten = 0;

    // Parse the docs, give them a sequence number for the result
    var docInfos = userDocs.map(function(doc, i) {
      var newDoc = PouchUtils.parseDoc(doc, newEdits);
      newDoc._bulk_seq = i;
      return newDoc;
    });

    var docInfoErrors = docInfos.filter(function(docInfo) {
      return docInfo.error;
    });
    if (docInfoErrors.length) {
      return PouchUtils.call(callback, docInfoErrors[0]);
    }

    var tx;
    var results = [];
    var fetchedDocs = {};

    function sortByBulkSeq(a, b) {
      return a._bulk_seq - b._bulk_seq;
    }

    function complete(event) {
      var aresults = [];
      results.sort(sortByBulkSeq);
      results.forEach(function(result) {
        delete result._bulk_seq;
        if (result.error) {
          aresults.push(result);
          return;
        }
        var metadata = result.metadata;
        var rev = PouchMerge.winningRev(metadata);

        aresults.push({
          ok: true,
          id: metadata.id,
          rev: rev
        });

        if (PouchUtils.isLocalId(metadata.id)) {
          return;
        }

        docsWritten++;

        webSqlPouch.Changes.notify(name);
        webSqlPouch.Changes.notifyLocalWindows(name);
      });

      var updateseq = 'SELECT update_seq FROM ' + META_STORE;
      tx.executeSql(updateseq, [], function(tx, result) {
        var update_seq = result.rows.item(0).update_seq + docsWritten;
        var sql = 'UPDATE ' + META_STORE + ' SET update_seq=?';
        tx.executeSql(sql, [update_seq], function() {
          PouchUtils.call(callback, null, aresults);
        });
      });
    }

    function preprocessAttachment(att, finish) {
      if (att.stub) {
        return finish();
      }
      if (typeof att.data === 'string') {
        try {
          att.data = atob(att.data);
        } catch (e) {
          var err = Pouch.error(Pouch.Errors.BAD_ARG,
                                "Attachments need to be base64 encoded");
          return PouchUtils.call(callback, err);
        }
        att.digest = 'md5-' + PouchUtils.Crypto.MD5(att.data);
        return finish();
      }
      var reader = new FileReader();
      reader.onloadend = function(e) {
        att.data = this.result;
        att.digest = 'md5-' + PouchUtils.Crypto.MD5(this.result);
        finish();
      };
      reader.readAsBinaryString(att.data);
    }

    function preprocessAttachments(callback) {
      if (!docInfos.length) {
        return callback();
      }

      var docv = 0;
      var recv = 0;

      docInfos.forEach(function(docInfo) {
        var attachments = docInfo.data && docInfo.data._attachments ?
          Object.keys(docInfo.data._attachments) : [];

        if (!attachments.length) {
          return done();
        }

        function processedAttachment() {
          recv++;
          if (recv === attachments.length) {
            done();
          }
        }

        for (var key in docInfo.data._attachments) {
          preprocessAttachment(docInfo.data._attachments[key], processedAttachment);
        }
      });

      function done() {
        docv++;
        if (docInfos.length === docv) {
          callback();
        }
      }
    }

    function writeDoc(docInfo, callback, isUpdate) {

      function finish() {
        var data = docInfo.data;
        var sql = 'INSERT INTO ' + BY_SEQ_STORE + ' (doc_id_rev, json) VALUES (?, ?);';
        tx.executeSql(sql, [data._id + "::" + data._rev,
                            JSON.stringify(data)], dataWritten);
      }

      function collectResults(attachmentErr) {
        if (!err) {
          if (attachmentErr) {
            err = attachmentErr;
            PouchUtils.call(callback, err);
          } else if (recv === attachments.length) {
            finish();
          }
        }
      }

      var err = null;
      var recv = 0;

      docInfo.data._id = docInfo.metadata.id;
      docInfo.data._rev = docInfo.metadata.rev;

      if (PouchUtils.isDeleted(docInfo.metadata, docInfo.metadata.rev)) {
        docInfo.data._deleted = true;
      }

      var attachments = docInfo.data._attachments ?
        Object.keys(docInfo.data._attachments) : [];

      function attachmentSaved(err) {
        recv++;
        collectResults(err);
      }

      for (var key in docInfo.data._attachments) {
        if (!docInfo.data._attachments[key].stub) {
          var data = docInfo.data._attachments[key].data;
          delete docInfo.data._attachments[key].data;
          var digest = docInfo.data._attachments[key].digest;
          saveAttachment(docInfo, digest, data, attachmentSaved);
        } else {
          recv++;
          collectResults();
        }
      }

      if (!attachments.length) {
        finish();
      }

      function dataWritten(tx, result) {
        var seq = docInfo.metadata.seq = result.insertId;
        delete docInfo.metadata.rev;

        var mainRev = PouchMerge.winningRev(docInfo.metadata);

        var sql = isUpdate ?
          'UPDATE ' + DOC_STORE + ' SET seq=?, json=?, winningseq=(SELECT seq FROM ' +
          BY_SEQ_STORE + ' WHERE doc_id_rev=?) WHERE id=?' :
          'INSERT INTO ' + DOC_STORE + ' (id, seq, winningseq, json) VALUES (?, ?, ?, ?);';
        var metadataStr = JSON.stringify(docInfo.metadata);
        var key = docInfo.metadata.id + "::" + mainRev;
        var params = isUpdate ?
          [seq, metadataStr, key, docInfo.metadata.id] :
          [docInfo.metadata.id, seq, seq, metadataStr];
        tx.executeSql(sql, params, function(tx, result) {
          results.push(docInfo);
          PouchUtils.call(callback, null);
        });
      }
    }

    function updateDoc(oldDoc, docInfo) {
      var merged = PouchMerge.merge(oldDoc.rev_tree, docInfo.metadata.rev_tree[0], 1000);
      var inConflict = (PouchUtils.isDeleted(oldDoc) &&
                        PouchUtils.isDeleted(docInfo.metadata)) ||
        (!PouchUtils.isDeleted(oldDoc) &&
         newEdits && merged.conflicts !== 'new_leaf');

      if (inConflict) {
        results.push(makeErr(Pouch.Errors.REV_CONFLICT, docInfo._bulk_seq));
        return processDocs();
      }

      docInfo.metadata.rev_tree = merged.tree;
      writeDoc(docInfo, processDocs, true);
    }

    function insertDoc(docInfo) {
      // Cant insert new deleted documents
      if ('was_delete' in opts && PouchUtils.isDeleted(docInfo.metadata)) {
        results.push(Pouch.Errors.MISSING_DOC);
        return processDocs();
      }
      writeDoc(docInfo, processDocs, false);
    }

    function processDocs() {
      if (!docInfos.length) {
        return complete();
      }
      var currentDoc = docInfos.shift();
      var id = currentDoc.metadata.id;
      if (id in fetchedDocs) {
        updateDoc(fetchedDocs[id], currentDoc);
      } else {
        // if we have newEdits=false then we can update the same
        // document twice in a single bulk docs call
        fetchedDocs[id] = currentDoc.metadata;
        insertDoc(currentDoc);
      }
    }

    // Insert sequence number into the error so we can sort later
    function makeErr(err, seq) {
      err._bulk_seq = seq;
      return err;
    }

    function saveAttachment(docInfo, digest, data, callback) {
      var ref = [docInfo.metadata.id, docInfo.metadata.rev].join('@');
      var newAtt = {digest: digest};
      var sql = 'SELECT digest, json FROM ' + ATTACH_STORE + ' WHERE digest=?';
      tx.executeSql(sql, [digest], function(tx, result) {
        if (!result.rows.length) {
          newAtt.refs = {};
          newAtt.refs[ref] = true;
          sql = 'INSERT INTO ' + ATTACH_STORE + '(digest, json, body) VALUES (?, ?, ?)';
          tx.executeSql(sql, [digest, JSON.stringify(newAtt), data], function() {
            PouchUtils.call(callback, null);
          });
        } else {
          newAtt.refs = JSON.parse(result.rows.item(0).json).refs;
          sql = 'UPDATE ' + ATTACH_STORE + ' SET json=?, body=? WHERE digest=?';
          tx.executeSql(sql, [JSON.stringify(newAtt), data, digest], function() {
            PouchUtils.call(callback, null);
          });
        }
      });
    }

    function metadataFetched(tx, results) {
      for (var j=0; j<results.rows.length; j++) {
        var row = results.rows.item(j);
        fetchedDocs[row.id] = JSON.parse(row.json);
      }
      processDocs();
    }

    preprocessAttachments(function() {
      db.transaction(function(txn) {
        tx = txn;
        var ids = '(' + docInfos.map(function(d) {
          return quote(d.metadata.id);
        }).join(',') + ')';
        var sql = 'SELECT * FROM ' + DOC_STORE + ' WHERE id IN ' + ids;
        tx.executeSql(sql, [], metadataFetched);
      }, unknownError(callback));
    });
  };

  api._get = function(id, opts, callback) {
    var doc;
    var metadata;
    var err;
    if (!opts.ctx) {
      db.transaction(function(txn) {
        opts.ctx = txn;
        api._get(id, opts, callback);
      });
      return;
    }
    var tx = opts.ctx;

    function finish() {
      PouchUtils.call(callback, err, {doc: doc, metadata: metadata, ctx: tx});
    }

    var sql = 'SELECT * FROM ' + DOC_STORE + ' WHERE id=?';
    tx.executeSql(sql, [id], function(a, results) {
      if (!results.rows.length) {
        err = Pouch.Errors.MISSING_DOC;
        return finish();
      }
      metadata = JSON.parse(results.rows.item(0).json);
      if (PouchUtils.isDeleted(metadata) && !opts.rev) {
        err = Pouch.error(Pouch.Errors.MISSING_DOC, "deleted");
        return finish();
      }

      var rev = PouchMerge.winningRev(metadata);
      var key = opts.rev ? opts.rev : rev;
      key = metadata.id + '::' + key;
      var sql = 'SELECT * FROM ' + BY_SEQ_STORE + ' WHERE doc_id_rev=?';
      tx.executeSql(sql, [key], function(tx, results) {
        if (!results.rows.length) {
          err = Pouch.Errors.MISSING_DOC;
          return finish();
        }
        doc = JSON.parse(results.rows.item(0).json);

        finish();
      });
    });
  };

  function makeRevs(arr) {
    return arr.map(function(x) { return {rev: x.rev}; });
  }

  api._allDocs = function(opts, callback) {
    var results = [];
    var resultsMap = {};
    var start = 'startkey' in opts ? opts.startkey : false;
    var end = 'endkey' in opts ? opts.endkey : false;
    var descending = 'descending' in opts ? opts.descending : false;
    var sql = 'SELECT ' + DOC_STORE + '.id, ' + BY_SEQ_STORE + '.seq, ' +
      BY_SEQ_STORE + '.json AS data, ' + DOC_STORE + '.json AS metadata FROM ' +
      BY_SEQ_STORE + ' JOIN ' + DOC_STORE + ' ON ' + BY_SEQ_STORE + '.seq = ' +
      DOC_STORE + '.winningseq';

    if ('keys' in opts) {
      sql += ' WHERE ' + DOC_STORE + '.id IN (' + opts.keys.map(function(key){
        return quote(key);
      }).join(',') + ')';
    } else {
      if (start) {
        sql += ' WHERE ' + DOC_STORE + '.id >= "' + start + '"';
      }
      if (end) {
        sql += (start ? ' AND ' : ' WHERE ') + DOC_STORE + '.id <= "' + end + '"';
      }
      sql += ' ORDER BY ' + DOC_STORE + '.id ' + (descending ? 'DESC' : 'ASC');
    }

    db.transaction(function(tx) {
      tx.executeSql(sql, [], function(tx, result) {
        for (var i = 0, l = result.rows.length; i < l; i++ ) {
          var doc = result.rows.item(i);
          var metadata = JSON.parse(doc.metadata);
          var data = JSON.parse(doc.data);
          if (!(PouchUtils.isLocalId(metadata.id))) {
            doc = {
              id: metadata.id,
              key: metadata.id,
              value: {rev: PouchMerge.winningRev(metadata)}
            };
            if (opts.include_docs) {
              doc.doc = data;
              doc.doc._rev = PouchMerge.winningRev(metadata);
              if (opts.conflicts) {
                doc.doc._conflicts = PouchMerge.collectConflicts(metadata);
              }
              for (var att in doc.doc._attachments) {
                doc.doc._attachments[att].stub = true;
              }
            }
            if ('keys' in opts) {
              if (opts.keys.indexOf(metadata.id) > -1) {
                if (PouchUtils.isDeleted(metadata)) {
                  doc.value.deleted = true;
                  doc.doc = null;
                }
                resultsMap[doc.id] = doc;
              }
            } else {
              if(!PouchUtils.isDeleted(metadata)) {
                results.push(doc);
              }
            }
          }
        }
      });
    }, unknownError(callback), function() {
      if ('keys' in opts) {
        opts.keys.forEach(function(key) {
          if (key in resultsMap) {
            results.push(resultsMap[key]);
          } else {
            results.push({"key": key, "error": "not_found"});
          }
        });
        if (opts.descending) {
          results.reverse();
        }
      }
      PouchUtils.call(callback, null, {
        total_rows: results.length,
        offset: opts.skip,
        rows: ('limit' in opts) ? results.slice(opts.skip, opts.limit + opts.skip) :
          (opts.skip > 0) ? results.slice(opts.skip) : results
      });
    });
  };

  api._changes = function idb_changes(opts) {

    if (Pouch.DEBUG) {
      console.log(name + ': Start Changes Feed: continuous=' + opts.continuous);
    }

    if (opts.continuous) {
      var id = name + ':' + Pouch.uuid();
      opts.cancelled = false;
      webSqlPouch.Changes.addListener(name, id, api, opts);
      webSqlPouch.Changes.notify(name);
      return {
        cancel: function() {
          if (Pouch.DEBUG) {
            console.log(name + ': Cancel Changes Feed');
          }
          opts.cancelled = true;
          webSqlPouch.Changes.removeListener(name, id);
        }
      };
    }

    var descending = opts.descending;

    // Ignore the `since` parameter when `descending` is true
    opts.since = opts.since && !descending ? opts.since : 0;

    var results = [];
    var txn;

    function fetchChanges() {
      var sql = 'SELECT ' + DOC_STORE + '.id, ' + BY_SEQ_STORE + '.seq, ' +
        BY_SEQ_STORE + '.json AS data, ' + DOC_STORE + '.json AS metadata FROM ' +
        BY_SEQ_STORE + ' JOIN ' + DOC_STORE + ' ON ' + BY_SEQ_STORE + '.seq = ' +
        DOC_STORE + '.winningseq WHERE ' + DOC_STORE + '.seq > ' + opts.since +
        ' ORDER BY ' + DOC_STORE + '.seq ' + (descending ? 'DESC' : 'ASC');

      db.transaction(function(tx) {
        tx.executeSql(sql, [], function(tx, result) {
          var last_seq = 0;
          for (var i = 0, l = result.rows.length; i < l; i++ ) {
            var res = result.rows.item(i);
            var metadata = JSON.parse(res.metadata);
            if (!PouchUtils.isLocalId(metadata.id)) {
              if (last_seq < res.seq) {
                last_seq = res.seq;
              }
              var doc = JSON.parse(res.data);
              var mainRev = doc._rev;
              var changeList = [{rev: mainRev}];
              if (opts.style === 'all_docs') {
                changeList = makeRevs(PouchMerge.collectLeaves(metadata.rev_tree));
              }
              var change = {
                id: metadata.id,
                seq: res.seq,
                changes: changeList,
                doc: doc
              };
              if (PouchUtils.isDeleted(metadata, mainRev)) {
                change.deleted = true;
              }
              if (opts.conflicts) {
                change.doc._conflicts = PouchMerge.collectConflicts(metadata);
              }
              results.push(change);
            }
          }
          PouchUtils.processChanges(opts, results, last_seq);
        });
      });
    }

    if (opts.filter && typeof opts.filter === 'string') {
      var filterName = opts.filter.split('/');
      api.get('_design/' + filterName[0], function(err, ddoc) {
        /*jshint evil: true */
        var filter = eval('(function() { return ' +
                          ddoc.filters[filterName[1]] + ' })()');
        opts.filter = filter;
        fetchChanges();
      });
    } else {
      fetchChanges();
    }
  };

  api._close = function(callback) {
    //WebSQL databases do not need to be closed
    PouchUtils.call(callback, null);
  };

  api._getAttachment = function(attachment, opts, callback) {
    var res;
    var tx = opts.ctx;
    var digest = attachment.digest;
    var type = attachment.content_type;
    var sql = 'SELECT body FROM ' + ATTACH_STORE + ' WHERE digest=?';
    tx.executeSql(sql, [digest], function(tx, result) {
      var data = result.rows.item(0).body;
      if (opts.encode) {
        res = btoa(data);
      } else {
        res = PouchUtils.createBlob([data], {type: type});
      }
      PouchUtils.call(callback, null, res);
    });
  };

  api._getRevisionTree = function(docId, callback) {
    db.transaction(function (tx) {
      var sql = 'SELECT json AS metadata FROM ' + DOC_STORE + ' WHERE id = ?';
      tx.executeSql(sql, [docId], function(tx, result) {
        if (!result.rows.length) {
          PouchUtils.call(callback, Pouch.Errors.MISSING_DOC);
        } else {
          var data = JSON.parse(result.rows.item(0).metadata);
          PouchUtils.call(callback, null, data.rev_tree);
        }
      });
    });
  };

  api._doCompaction = function(docId, rev_tree, revs, callback) {
    db.transaction(function (tx) {
      var sql = 'SELECT json AS metadata FROM ' + DOC_STORE + ' WHERE id = ?';
      tx.executeSql(sql, [docId], function(tx, result) {
        if (!result.rows.length) {
          return PouchUtils.call(callback);
        }
        var metadata = JSON.parse(result.rows.item(0).metadata);
        metadata.rev_tree = rev_tree;

        var sql = 'DELETE FROM ' + BY_SEQ_STORE + ' WHERE doc_id_rev IN (' +
          revs.map(function(rev){return quote(docId + '::' + rev);}).join(',') + ')';

        tx.executeSql(sql, [], function(tx, result) {
          var sql = 'UPDATE ' + DOC_STORE + ' SET json = ? WHERE id = ?';

          tx.executeSql(sql, [JSON.stringify(metadata), docId], function(tx, result) {
            callback();
          });
        });
      });
    });
  };

  return api;
};

webSqlPouch.valid = function() {
  return typeof window !== 'undefined' && !!window.openDatabase;
};

webSqlPouch.destroy = function(name, opts, callback) {
  var db = openDatabase(name, POUCH_VERSION, name, POUCH_SIZE);
  db.transaction(function (tx) {
    tx.executeSql('DROP TABLE IF EXISTS ' + DOC_STORE, []);
    tx.executeSql('DROP TABLE IF EXISTS ' + BY_SEQ_STORE, []);
    tx.executeSql('DROP TABLE IF EXISTS ' + ATTACH_STORE, []);
    tx.executeSql('DROP TABLE IF EXISTS ' + META_STORE, []);
  }, unknownError(callback), function() {
    PouchUtils.call(callback, null);
  });
};

webSqlPouch.Changes = new PouchUtils.Changes();

Pouch.adapter('websql', webSqlPouch);

/*global Pouch: true, pouchCollate: true */

"use strict";

var pouchCollate;
if (typeof module !== 'undefined' && module.exports) {
  pouchCollate = require('../pouch.collate.js');
}

// This is the first implementation of a basic plugin, we register the
// plugin object with pouch and it is mixin'd to each database created
// (regardless of adapter), adapters can override plugins by providing
// their own implementation. functions on the plugin object that start
// with _ are reserved function that are called by pouchdb for special
// notifications.

// If we wanted to store incremental views we can do it here by listening
// to the changes feed (keeping track of our last update_seq between page loads)
// and storing the result of the map function (possibly using the upcoming
// extracted adapter functions)

var MapReduce = function(db) {

  function viewQuery(fun, options) {
    if (!options.complete) {
      return;
    }

    if (!options.skip) {
      options.skip = 0;
    }

    if (!fun.reduce) {
      options.reduce = false;
    }

    function sum(values) {
      return values.reduce(function(a, b) { return a + b; }, 0);
    }

    var builtInReduce = {
      "_sum": function(keys, values){
        return sum(values);
      },

      "_count": function(keys, values, rereduce){
        if (rereduce){
          return sum(values);
        } else {
          return values.length;
        }
      },

      "_stats": function(keys, values, rereduce) {
        return {
          'sum': sum(values),
          'min': Math.min.apply(null, values),
          'max': Math.max.apply(null, values),
          'count': values.length,
          'sumsqr': (function(){
            var _sumsqr = 0;
            for(var idx in values) {
              if (typeof values[idx] === 'number') {
              _sumsqr += values[idx] * values[idx];
              }
            }
            return _sumsqr;
          })()
        };
      }
    };

    var results = [];
    var current = null;
    var num_started= 0;
    var completed= false;

    var emit = function(key, val) {
      var viewRow = {
        id: current.doc._id,
        key: key,
        value: val
      };

      if (options.startkey && pouchCollate(key, options.startkey) < 0) return;
      if (options.endkey && pouchCollate(key, options.endkey) > 0) return;
      if (options.key && pouchCollate(key, options.key) !== 0) return;

      num_started++;
      if (options.include_docs) {
        //in this special case, join on _id (issue #106)
        if (val && typeof val === 'object' && val._id){
          db.get(val._id,
              function(_, joined_doc){
                if (joined_doc) {
                  viewRow.doc = joined_doc;
                }
                results.push(viewRow);
                checkComplete();
              });
          return;
        } else {
          viewRow.doc = current.doc;
        }
      }
      results.push(viewRow);
    };

    // ugly way to make sure references to 'emit' in map/reduce bind to the
    // above emit
    eval('fun.map = ' + fun.map.toString() + ';');
    if (fun.reduce) {
      if (builtInReduce[fun.reduce]) {
        fun.reduce = builtInReduce[fun.reduce];
      }

      eval('fun.reduce = ' + fun.reduce.toString() + ';');
    }

    //only proceed once all documents are mapped and joined
    var checkComplete= function(){
      if (completed && results.length == num_started){
        results.sort(function(a, b) {
          return pouchCollate(a.key, b.key);
        });
        if (options.descending) {
          results.reverse();
        }
        if (options.reduce === false) {
          return options.complete(null, {
            total_rows: results.length,
            offset: options.skip,
            rows: ('limit' in options) ? results.slice(options.skip, options.limit + options.skip) :
              (options.skip > 0) ? results.slice(options.skip) : results
          });
        }

        var groups = [];
        results.forEach(function(e) {
          var last = groups[groups.length-1] || null;
          if (last && pouchCollate(last.key[0][0], e.key) === 0) {
            last.key.push([e.key, e.id]);
            last.value.push(e.value);
            return;
          }
          groups.push({key: [[e.key, e.id]], value: [e.value]});
        });
        groups.forEach(function(e) {
          e.value = fun.reduce(e.key, e.value);
          e.value = (typeof e.value === 'undefined') ? null : e.value;
          e.key = e.key[0][0];
        });

        options.complete(null, {
          total_rows: groups.length,
          offset: options.skip,
          rows: ('limit' in options) ? groups.slice(options.skip, options.limit + options.skip) :
            (options.skip > 0) ? groups.slice(options.skip) : groups
        });
      }
    };

    db.changes({
      conflicts: true,
      include_docs: true,
      onChange: function(doc) {
        if (!('deleted' in doc)) {
          current = {doc: doc.doc};
          fun.map.call(this, doc.doc);
        }
      },
      complete: function() {
        completed= true;
        checkComplete();
      }
    });
  }

  function httpQuery(fun, opts, callback) {

    // List of parameters to add to the PUT request
    var params = [];
    var body = undefined;
    var method = 'GET';

    // If opts.reduce exists and is defined, then add it to the list
    // of parameters.
    // If reduce=false then the results are that of only the map function
    // not the final result of map and reduce.
    if (typeof opts.reduce !== 'undefined') {
      params.push('reduce=' + opts.reduce);
    }
    if (typeof opts.include_docs !== 'undefined') {
      params.push('include_docs=' + opts.include_docs);
    }
    if (typeof opts.limit !== 'undefined') {
      params.push('limit=' + opts.limit);
    }
    if (typeof opts.descending !== 'undefined') {
      params.push('descending=' + opts.descending);
    }
    if (typeof opts.startkey !== 'undefined') {
      params.push('startkey=' + encodeURIComponent(JSON.stringify(opts.startkey)));
    }
    if (typeof opts.endkey !== 'undefined') {
      params.push('endkey=' + encodeURIComponent(JSON.stringify(opts.endkey)));
    }
    if (typeof opts.key !== 'undefined') {
      params.push('key=' + encodeURIComponent(JSON.stringify(opts.key)));
    }
    if (typeof opts.group !== 'undefined') {
      params.push('group=' + opts.group);
    }
    if (typeof opts.group_level !== 'undefined') {
      params.push('group_level=' + opts.group_level);
    }
    if (typeof opts.skip !== 'undefined') {
      params.push('skip=' + opts.skip);
    }

    // If keys are supplied, issue a POST request to circumvent GET query string limits
    // see http://wiki.apache.org/couchdb/HTTP_view_API#Querying_Options
    if (typeof opts.keys !== 'undefined') {
      method = 'POST';
      body = JSON.stringify({keys:opts.keys});
    }

    // Format the list of parameters into a valid URI query string
    params = params.join('&');
    params = params === '' ? '' : '?' + params;

    // We are referencing a query defined in the design doc
    if (typeof fun === 'string') {
      var parts = fun.split('/');
      db.request({
        method: method,
        url: '_design/' + parts[0] + '/_view/' + parts[1] + params,
        body: body
      }, callback);
      return;
    }

    // We are using a temporary view, terrible for performance but good for testing
    var queryObject = JSON.parse(JSON.stringify(fun, function(key, val) {
      if (typeof val === 'function') {
        return val + ''; // implicitly `toString` it
      }
      return val;
    }));

    db.request({
      method:'POST',
      url: '_temp_view' + params,
      body: queryObject
    }, callback);
  }

  function query(fun, opts, callback) {
    if (typeof opts === 'function') {
      callback = opts;
      opts = {};
    }

    if (callback) {
      opts.complete = callback;
    }

    if (db.type() === 'http') {
	  if (typeof fun === 'function'){
	    return httpQuery({map: fun}, opts, callback);
	  }
	  return httpQuery(fun, opts, callback);
    }

    if (typeof fun === 'object') {
      return viewQuery(fun, opts);
    }

    if (typeof fun === 'function') {
      return viewQuery({map: fun}, opts);
    }

    var parts = fun.split('/');
    db.get('_design/' + parts[0], function(err, doc) {
      if (err) {
        if (callback) callback(err);
        return;
      }

      if (!doc.views[parts[1]]) {
        if (callback) callback({ error: 'not_found', reason: 'missing_named_view' });
        return;
      }

      viewQuery({
        map: doc.views[parts[1]].map,
        reduce: doc.views[parts[1]].reduce
      }, opts);
    });
  }

  return {'query': query};
};

// Deletion is a noop since we dont store the results of the view
MapReduce._delete = function() { };

Pouch.plugin('mapreduce', MapReduce);

 })(this);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\base\js\lib\tablet\jquery.js */ 
/*
 * jQuery JavaScript Library v2.0.0
 * http://jquery.com/
 *
 * Includes Sizzle.js
 * http://sizzlejs.com/
 *
 * Copyright 2005, 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2013-04-18
 */
(function(e,b){var tb,G,y=typeof b,sc=e.location,d=e.document,Db=d.documentElement,Bc=e.jQuery,Jc=e.$,B={},z=[],K="2.0.0",mb=z.concat,S=z.push,n=z.slice,A=z.indexOf,Tb=B.toString,N=B.hasOwnProperty,lc=K.trim,a=function(b,c){return new a.fn.init(b,c,tb)},E=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,j=/\S+/g,kc=/^(?:(<[\w\W]+>)[^>]*|#([\w-]*))$/,ub=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,nc=/^-ms-/,gc=/-([\da-z])/gi,ec=function(b,a){return a.toUpperCase()},D=function(){d.removeEventListener("DOMContentLoaded",D,false);e.removeEventListener("load",D,false);a.ready()};a.fn=a.prototype={jquery:K,constructor:a,init:function(c,e,h){var f,g;if(!c)return this;if(typeof c==="string"){if(c.charAt(0)==="<"&&c.charAt(c.length-1)===">"&&c.length>=3)f=[null,c,null];else f=kc.exec(c);if(f&&(f[1]||!e))if(f[1]){e=e instanceof a?e[0]:e;a.merge(this,a.parseHTML(f[1],e&&e.nodeType?e.ownerDocument||e:d,true));if(ub.test(f[1])&&a.isPlainObject(e))for(f in e)if(a.isFunction(this[f]))this[f](e[f]);else this.attr(f,e[f]);return this}else{g=d.getElementById(f[2]);if(g&&g.parentNode){this.length=1;this[0]=g}this.context=d;this.selector=c;return this}else return!e||e.jquery?(e||h).find(c):this.constructor(e).find(c)}else if(c.nodeType){this.context=this[0]=c;this.length=1;return this}else if(a.isFunction(c))return h.ready(c);if(c.selector!==b){this.selector=c.selector;this.context=c.context}return a.makeArray(c,this)},selector:"",length:0,toArray:function(){return n.call(this)},"get":function(a){return a==null?this.toArray():a<0?this[this.length+a]:this[a]},pushStack:function(c){var b=a.merge(this.constructor(),c);b.prevObject=this;b.context=this.context;return b},each:function(b,c){return a.each(this,b,c)},ready:function(b){a.ready.promise().done(b);return this},slice:function(){return this.pushStack(n.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(c){var b=this.length,a=+c+(c<0?b:0);return this.pushStack(a>=0&&a<b?[this[a]]:[])},map:function(b){return this.pushStack(a.map(this,function(a,c){return b.call(a,c,a)}))},end:function(){return this.prevObject||this.constructor(null)},push:S,sort:[].sort,splice:[].splice};a.fn.init.prototype=a.fn;a.extend=a.fn.extend=function(){var i,g,e,d,h,j,c=arguments[0]||{},f=1,l=arguments.length,k=false;if(typeof c==="boolean"){k=c;c=arguments[1]||{};f=2}if(typeof c!=="object"&&!a.isFunction(c))c={};if(l===f){c=this;--f}for(;f<l;f++)if((i=arguments[f])!=null)for(g in i){e=c[g];d=i[g];if(c===d)continue;if(k&&d&&(a.isPlainObject(d)||(h=a.isArray(d)))){if(h){h=false;j=e&&a.isArray(e)?e:[]}else j=e&&a.isPlainObject(e)?e:{};c[g]=a.extend(k,j,d)}else if(d!==b)c[g]=d}return c};a.extend({expando:"jQuery"+(K+Math.random()).replace(/\D/g,""),noConflict:function(b){if(e.$===a)e.$=Jc;if(b&&e.jQuery===a)e.jQuery=Bc;return a},isReady:false,readyWait:1,holdReady:function(b){if(b)a.readyWait++;else a.ready(true)},ready:function(b){if(b===true?--a.readyWait:a.isReady)return;a.isReady=true;if(b!==true&&--a.readyWait>0)return;G.resolveWith(d,[a]);a.fn.trigger&&a(d).trigger("ready").off("ready")},isFunction:function(b){return a.type(b)==="function"},isArray:Array.isArray,isWindow:function(a){return a!=null&&a===a.window},isNumeric:function(a){return!isNaN(parseFloat(a))&&isFinite(a)},type:function(a){return a==null?String(a):typeof a==="object"||typeof a==="function"?B[Tb.call(a)]||"object":typeof a},isPlainObject:function(b){if(a.type(b)!=="object"||b.nodeType||a.isWindow(b))return false;try{if(b.constructor&&!N.call(b.constructor.prototype,"isPrototypeOf"))return false}catch(c){return false}return true},isEmptyObject:function(b){var a;for(a in b)return false;return true},error:function(a){throw new Error(a);},parseHTML:function(e,b,g){if(!e||typeof e!=="string")return null;if(typeof b==="boolean"){g=b;b=false}b=b||d;var c=ub.exec(e),f=!g&&[];if(c)return[b.createElement(c[1])];c=a.buildFragment([e],b,f);f&&a(f).remove();return a.merge([],c.childNodes)},parseJSON:JSON.parse,parseXML:function(d){var c,e;if(!d||typeof d!=="string")return null;try{e=new DOMParser;c=e.parseFromString(d,"text/xml")}catch(f){c=b}(!c||c.getElementsByTagName("parsererror").length)&&a.error("Invalid XML: "+d);return c},noop:function(){},globalEval:function(b){var c,e=eval;b=a.trim(b);if(b)if(b.indexOf("use strict")===1){c=d.createElement("script");c.text=b;d.head.appendChild(c).parentNode.removeChild(c)}else e(b)},camelCase:function(a){return a.replace(nc,"ms-").replace(gc,ec)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(b,d,e){var c,a=0,g=b.length,f=O(b);if(e)if(f)for(;a<g;a++){c=d.apply(b[a],e);if(c===false)break}else for(a in b){c=d.apply(b[a],e);if(c===false)break}else if(f)for(;a<g;a++){c=d.call(b[a],a,b[a]);if(c===false)break}else for(a in b){c=d.call(b[a],a,b[a]);if(c===false)break}return b},trim:function(a){return a==null?"":lc.call(a)},makeArray:function(b,d){var c=d||[];if(b!=null)if(O(Object(b)))a.merge(c,typeof b==="string"?[b]:b);else S.call(c,b);return c},inArray:function(b,a,c){return a==null?-1:A.call(a,b,c)},merge:function(a,d){var f=d.length,e=a.length,c=0;if(typeof f==="number")for(;c<f;c++)a[e++]=d[c];else while(d[c]!==b)a[e++]=d[c++];a.length=e;return a},grep:function(b,f,c){var d,e=[],a=0,g=b.length;c=!!c;for(;a<g;a++){d=!!f(b[a],a);c!==d&&e.push(b[a])}return e},map:function(c,e,f){var b,a=0,h=c.length,g=O(c),d=[];if(g)for(;a<h;a++){b=e(c[a],a,f);if(b!=null)d[d.length]=b}else for(a in c){b=e(c[a],a,f);if(b!=null)d[d.length]=b}return mb.apply([],d)},guid:1,proxy:function(c,d){var g,f,e;if(typeof d==="string"){g=c[d];d=c;c=g}if(!a.isFunction(c))return b;f=n.call(arguments,2);e=function(){return c.apply(d||this,f.concat(n.call(arguments)))};e.guid=c.guid=c.guid||a.guid++;return e},access:function(d,c,f,g,j,k,i){var e=0,l=d.length,h=f==null;if(a.type(f)==="object"){j=true;for(e in f)a.access(d,c,e,f[e],true,k,i)}else if(g!==b){j=true;if(!a.isFunction(g))i=true;if(h)if(i){c.call(d,g);c=null}else{h=c;c=function(c,d,b){return h.call(a(c),b)}}if(c)for(;e<l;e++)c(d[e],f,i?g:g.call(d[e],e,c(d[e],f)))}return j?d:h?c.call(d):l?c(d[0],f):k},now:Date.now,swap:function(b,c,f,g){var e,a,d={};for(a in c){d[a]=b.style[a];b.style[a]=c[a]}e=f.apply(b,g||[]);for(a in c)b.style[a]=d[a];return e}});a.ready.promise=function(b){if(!G){G=a.Deferred();if(d.readyState==="complete")setTimeout(a.ready);else{d.addEventListener("DOMContentLoaded",D,false);e.addEventListener("load",D,false)}}return G.promise(b)};a.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(b,a){B["[object "+a+"]"]=a.toLowerCase()});function O(c){var b=c.length,d=a.type(c);return a.isWindow(c)?false:c.nodeType===1&&b?true:d==="array"||d!=="function"&&(b===0||typeof b==="number"&&b>0&&b-1 in c)}tb=a(d);
/*
 * Sizzle CSS Selector Engine v1.9.2-pre
 * http://sizzlejs.com/
 *
 * Copyright 2013 jQuery Foundation, Inc. and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2013-04-16
 */
(function(X,B){var z,E,b,K,gb,V,D,v,y,g,k,i,h,t,L,C,f="sizzle"+-new Date,m=X.document,e={},n=0,Ab=0,cb=Q(),fb=Q(),Y=Q(),A=false,H=function(){return 0},u=typeof B,Z=1<<31,r=[],Bb=r.pop,qb=r.push,q=r.push,hb=r.slice,w=r.indexOf||function(b){for(var a=0,c=this.length;a<c;a++)if(this[a]===b)return a;return-1},S="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",d="[\\x20\\t\\r\\n\\f]",x="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",db=x.replace("w","w#"),bb="\\["+d+"*("+x+")"+d+"*(?:([*^$|!~]?=)"+d+"*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|("+db+")|)|)"+d+"*\\]",W=":("+x+")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|"+bb.replace(3,8)+")*)|.*)\\)|)",M=new RegExp("^"+d+"+|((?:^|[^\\\\])(?:\\\\.)*)"+d+"+$","g"),yb=new RegExp("^"+d+"*,"+d+"*"),ob=new RegExp("^"+d+"*([>+~]|"+d+")"+d+"*"),U=new RegExp(d+"*[+~]"),nb=new RegExp("="+d+"*([^\\]'\"]*)"+d+"*\\]","g"),xb=new RegExp(W),rb=new RegExp("^"+db+"$"),G={ID:new RegExp("^#("+x+")"),CLASS:new RegExp("^\\.("+x+")"),TAG:new RegExp("^("+x.replace("w","w*")+")"),ATTR:new RegExp("^"+bb),PSEUDO:new RegExp("^"+W),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+d+"*(even|odd|(([+-]|)(\\d*)n|)"+d+"*(?:([+-]|)"+d+"*(\\d+)|))"+d+"*\\)|)","i"),"boolean":new RegExp("^(?:"+S+")$","i"),needsContext:new RegExp("^"+d+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+d+"*((?:-\\d)?\\d*)"+d+"*\\)|)(?=[^-]|$)","i")},wb=/^[^{]+\{\s*\[native \w/,sb=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,vb=/^(?:input|select|textarea|button)$/i,ub=/^h\d$/i,tb=/'|\\/g,p=/\\([\da-fA-F]{1,6}[\x20\t\r\n\f]?|.)/g,o=function(c,b){var a="0x"+b-65536;return a!==a?b:a<0?String.fromCharCode(a+65536):String.fromCharCode(a>>10|55296,a&1023|56320)};try{q.apply(r=hb.call(m.childNodes),m.childNodes);r[m.childNodes.length].nodeType}catch(Cb){q={apply:r.length?function(a,b){qb.apply(a,hb.call(b))}:function(a,c){var b=a.length,d=0;while(a[b++]=c[d++]);a.length=b-1}}}function T(a){return wb.test(a+"")}function Q(){var a,c=[];return a=function(d,e){if(c.push(d+=" ")>b.cacheLength)delete a[c.shift()];return a[d]=e}}function j(a){a[f]=true;return a}function l(b){var a=g.createElement("div");try{return!!b(a)}catch(c){return false}finally{a.parentNode&&a.parentNode.removeChild(a);a=null}}function c(c,a,b,u){var p,d,j,l,s,n,r,k,t,o;(a?a.ownerDocument||a:m)!==g&&y(a);a=a||g;b=b||[];if(!c||typeof c!=="string")return b;if((l=a.nodeType)!==1&&l!==9)return[];if(i&&!u){if(p=sb.exec(c))if(j=p[1]){if(l===9){d=a.getElementById(j);if(d&&d.parentNode){if(d.id===j){b.push(d);return b}}else return b}else if(a.ownerDocument&&(d=a.ownerDocument.getElementById(j))&&C(a,d)&&d.id===j){b.push(d);return b}}else if(p[2]){q.apply(b,a.getElementsByTagName(c));return b}else if((j=p[3])&&e.getElementsByClassName&&a.getElementsByClassName){q.apply(b,a.getElementsByClassName(j));return b}if(e.qsa&&(!h||!h.test(c))){k=r=f;t=a;o=l===9&&c;if(l===1&&a.nodeName.toLowerCase()!=="object"){n=J(c);if(r=a.getAttribute("id"))k=r.replace(tb,"\\$&");else a.setAttribute("id",k);k="[id='"+k+"'] ";s=n.length;while(s--)n[s]=k+F(n[s]);t=U.test(c)&&a.parentNode||a;o=n.join(",")}if(o)try{q.apply(b,t.querySelectorAll(o));return b}catch(v){}finally{!r&&a.removeAttribute("id")}}}return zb(c.replace(M,"$1"),a,b,u)}gb=c.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?b.nodeName!=="HTML":false};y=c.setDocument=function(c){var a=c?c.ownerDocument||c:m;if(a===g||a.nodeType!==9||!a.documentElement)return g;g=a;k=a.documentElement;i=!gb(a);e.getElementsByTagName=l(function(b){b.appendChild(a.createComment(""));return!b.getElementsByTagName("*").length});e.attributes=l(function(a){a.className="i";return!a.getAttribute("className")});e.getElementsByClassName=l(function(a){a.innerHTML="<div class='a'></div><div class='a i'></div>";a.firstChild.className="i";return a.getElementsByClassName("i").length===2});e.sortDetached=l(function(a){return a.compareDocumentPosition(g.createElement("div"))&1});e.getById=l(function(b){k.appendChild(b).id=f;return!a.getElementsByName||!a.getElementsByName(f).length});if(e.getById){b.find.ID=function(c,b){if(typeof b.getElementById!==u&&i){var a=b.getElementById(c);return a&&a.parentNode?[a]:[]}};b.filter.ID=function(b){var a=b.replace(p,o);return function(b){return b.getAttribute("id")===a}}}else{b.find.ID=function(b,c){if(typeof c.getElementById!==u&&i){var a=c.getElementById(b);return a?a.id===b||typeof a.getAttributeNode!==u&&a.getAttributeNode("id").value===b?[a]:B:[]}};b.filter.ID=function(b){var a=b.replace(p,o);return function(b){var c=typeof b.getAttributeNode!==u&&b.getAttributeNode("id");return c&&c.value===a}}}b.find.TAG=e.getElementsByTagName?function(b,a){if(typeof a.getElementsByTagName!==u)return a.getElementsByTagName(b)}:function(c,e){var a,d=[],f=0,b=e.getElementsByTagName(c);if(c==="*"){while(a=b[f++])a.nodeType===1&&d.push(a);return d}return b};b.find.CLASS=e.getElementsByClassName&&function(b,a){if(typeof a.getElementsByClassName!==u&&i)return a.getElementsByClassName(b)};t=[];h=[];if(e.qsa=T(a.querySelectorAll)){l(function(a){a.innerHTML="<select><option selected=''></option></select>";!a.querySelectorAll("[selected]").length&&h.push("\\["+d+"*(?:value|"+S+")");!a.querySelectorAll(":checked").length&&h.push(":checked")});l(function(a){var b=g.createElement("input");b.setAttribute("type","hidden");a.appendChild(b).setAttribute("t","");a.querySelectorAll("[t^='']").length&&h.push("[*^$]="+d+"*(?:''|\"\")");!a.querySelectorAll(":enabled").length&&h.push(":enabled",":disabled");a.querySelectorAll("*,:x");h.push(",.*:")})}(e.matchesSelector=T(L=k.webkitMatchesSelector||k.mozMatchesSelector||k.oMatchesSelector||k.msMatchesSelector))&&l(function(a){e.disconnectedMatch=L.call(a,"div");L.call(a,"[s!='']:x");t.push("!=",W)});h=h.length&&new RegExp(h.join("|"));t=t.length&&new RegExp(t.join("|"));C=T(k.contains)||k.compareDocumentPosition?function(a,d){var c=a.nodeType===9?a.documentElement:a,b=d&&d.parentNode;return a===b||!!(b&&b.nodeType===1&&(c.contains?c.contains(b):a.compareDocumentPosition&&a.compareDocumentPosition(b)&16))}:function(b,a){if(a)while(a=a.parentNode)if(a===b)return true;return false};H=k.compareDocumentPosition?function(b,c){if(b===c){A=true;return 0}var d=c.compareDocumentPosition&&b.compareDocumentPosition&&b.compareDocumentPosition(c);return d?d&1||!e.sortDetached&&c.compareDocumentPosition(b)===d?b===a||C(m,b)?-1:c===a||C(m,c)?1:v?w.call(v,b)-w.call(v,c):0:d&4?-1:1:b.compareDocumentPosition?-1:1}:function(d,e){var b,c=0,h=d.parentNode,i=e.parentNode,f=[d],g=[e];if(d===e){A=true;return 0}else if(!h||!i)return d===a?-1:e===a?1:h?-1:i?1:v?w.call(v,d)-w.call(v,e):0;else if(h===i)return ab(d,e);b=d;while(b=b.parentNode)f.unshift(b);b=e;while(b=b.parentNode)g.unshift(b);while(f[c]===g[c])c++;return c?ab(f[c],g[c]):f[c]===m?-1:g[c]===m?1:0};return g};c.matches=function(b,a){return c(b,null,null,a)};c.matchesSelector=function(a,b){(a.ownerDocument||a)!==g&&y(a);b=b.replace(nb,"='$1']");if(e.matchesSelector&&i&&(!t||!t.test(b))&&(!h||!h.test(b)))try{var d=L.call(a,b);if(d||e.disconnectedMatch||a.document&&a.document.nodeType!==11)return d}catch(f){}return c(b,g,null,[a]).length>0};c.contains=function(a,b){(a.ownerDocument||a)!==g&&y(a);return C(a,b)};c.attr=function(a,d){(a.ownerDocument||a)!==g&&y(a);var f=b.attrHandle[d.toLowerCase()],c=f&&f(a,d,!i);return c===B?e.attributes||!i?a.getAttribute(d):(c=a.getAttributeNode(d))&&c.specified?c.value:null:c};c.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a);};c.uniqueSort=function(a){var f,d=[],c=0,b=0;A=!e.detectDuplicates;v=!e.sortStable&&a.slice(0);a.sort(H);if(A){while(f=a[b++])if(f===a[b])c=d.push(b);while(c--)a.splice(d[c],1)}return a};function ab(b,c){var a=c&&b,d=a&&(~c.sourceIndex||Z)-(~b.sourceIndex||Z);if(d)return d;if(a)while(a=a.nextSibling)if(a===c)return-1;return b?1:-1}function pb(c,a,d){var b;return d?B:(b=c.getAttributeNode(a))&&b.specified?b.value:c[a]===true?a.toLowerCase():null}function jb(c,a,b){var d;return b?B:(d=c.getAttribute(a,a.toLowerCase()==="type"?1:2))}function lb(a){return function(b){var c=b.nodeName.toLowerCase();return c==="input"&&b.type===a}}function kb(a){return function(b){var c=b.nodeName.toLowerCase();return(c==="input"||c==="button")&&b.type===a}}function s(a){return j(function(b){b=+b;return j(function(c,g){var d,e=a([],c.length,b),f=e.length;while(f--)if(c[d=e[f]])c[d]=!(g[d]=c[d])})})}K=c.getText=function(a){var d,c="",e=0,b=a.nodeType;if(!b)for(;d=a[e];e++)c+=K(d);else if(b===1||b===9||b===11)if(typeof a.textContent==="string")return a.textContent;else for(a=a.firstChild;a;a=a.nextSibling)c+=K(a);else if(b===3||b===4)return a.nodeValue;return c};b=c.selectors={cacheLength:50,createPseudo:j,match:G,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:true}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:true},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){a[1]=a[1].replace(p,o);a[3]=(a[4]||a[5]||"").replace(p,o);if(a[2]==="~=")a[3]=" "+a[3]+" ";return a.slice(0,4)},CHILD:function(a){a[1]=a[1].toLowerCase();if(a[1].slice(0,3)==="nth"){!a[3]&&c.error(a[0]);a[4]=+(a[4]?a[5]+(a[6]||1):2*(a[3]==="even"||a[3]==="odd"));a[5]=+(a[7]+a[8]||a[3]==="odd")}else a[3]&&c.error(a[0]);return a},PSEUDO:function(a){var c,b=!a[5]&&a[2];if(G.CHILD.test(a[0]))return null;if(a[4])a[2]=a[4];else if(b&&xb.test(b)&&(c=J(b,true))&&(c=b.indexOf(")",b.length-c)-b.length)){a[0]=a[0].slice(0,c);a[2]=b.slice(0,c)}return a.slice(0,3)}},filter:{TAG:function(a){var b=a.replace(p,o).toLowerCase();return a==="*"?function(){return true}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=cb[a+" "];return b||(b=new RegExp("(^|"+d+")"+a+"("+d+"|$)"))&&cb(a,function(a){return b.test(typeof a.className==="string"&&a.className||typeof a.getAttribute!==u&&a.getAttribute("class")||"")})},ATTR:function(d,b,a){return function(f){var e=c.attr(f,d);if(e==null)return b==="!=";if(!b)return true;e+="";return b==="="?e===a:b==="!="?e!==a:b==="^="?a&&e.indexOf(a)===0:b==="*="?a&&e.indexOf(a)>-1:b==="$="?a&&e.slice(-a.length)===a:b==="~="?(" "+e+" ").indexOf(a)>-1:b==="|="?e===a||e.slice(0,a.length+1)===a+"-":false}},CHILD:function(a,h,i,c,g){var e=a.slice(0,3)!=="nth",d=a.slice(-4)!=="last",b=h==="of-type";return c===1&&g===0?function(a){return!!a.parentNode}:function(l,u,t){var k,q,h,i,j,o,p=e!==d?"nextSibling":"previousSibling",m=l.parentNode,s=b&&l.nodeName.toLowerCase(),r=!t&&!b;if(m){if(e){while(p){h=l;while(h=h[p])if(b?h.nodeName.toLowerCase()===s:h.nodeType===1)return false;o=p=a==="only"&&!o&&"nextSibling"}return true}o=[d?m.firstChild:m.lastChild];if(d&&r){q=m[f]||(m[f]={});k=q[a]||[];j=k[0]===n&&k[1];i=k[0]===n&&k[2];h=j&&m.childNodes[j];while(h=++j&&h&&h[p]||(i=j=0)||o.pop())if(h.nodeType===1&&++i&&h===l){q[a]=[n,j,i];break}}else if(r&&(k=(l[f]||(l[f]={}))[a])&&k[0]===n)i=k[1];else while(h=++j&&h&&h[p]||(i=j=0)||o.pop())if((b?h.nodeName.toLowerCase()===s:h.nodeType===1)&&++i){if(r)(h[f]||(h[f]={}))[a]=[n,i];if(h===l)break}i-=g;return i===c||i%c===0&&i/c>=0}}},PSEUDO:function(a,e){var g,d=b.pseudos[a]||b.setFilters[a.toLowerCase()]||c.error("unsupported pseudo: "+a);if(d[f])return d(e);if(d.length>1){g=[a,a,"",e];return b.setFilters.hasOwnProperty(a.toLowerCase())?j(function(b,g){var c,a=d(b,e),f=a.length;while(f--){c=w.call(b,a[f]);b[c]=!(g[c]=a[f])}}):function(a){return d(a,0,g)}}return d}},pseudos:{not:j(function(d){var c=[],b=[],a=V(d.replace(M,"$1"));return a[f]?j(function(c,f,h,g){var d,e=a(c,null,g,[]),b=c.length;while(b--)if(d=e[b])c[b]=!(f[b]=d)}):function(d,f,e){c[0]=d;a(c,null,e,b);return!b.pop()}}),has:j(function(a){return function(b){return c(a,b).length>0}}),contains:j(function(a){return function(b){return(b.textContent||b.innerText||K(b)).indexOf(a)>-1}}),lang:j(function(a){!rb.test(a||"")&&c.error("unsupported lang: "+a);a=a.replace(p,o).toLowerCase();return function(b){var c;do if(c=i?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang")){c=c.toLowerCase();return c===a||c.indexOf(a+"-")===0}while((b=b.parentNode)&&b.nodeType===1);return false}}),target:function(b){var a=X.location&&X.location.hash;return a&&a.slice(1)===b.id},root:function(a){return a===k},focus:function(a){return a===g.activeElement&&(!g.hasFocus||g.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===false},disabled:function(a){return a.disabled===true},checked:function(a){var b=a.nodeName.toLowerCase();return b==="input"&&!!a.checked||b==="option"&&!!a.selected},selected:function(a){if(a.parentNode)a.parentNode.selectedIndex;return a.selected===true},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeName>"@"||a.nodeType===3||a.nodeType===4)return false;return true},parent:function(a){return!b.pseudos.empty(a)},header:function(a){return ub.test(a.nodeName)},input:function(a){return vb.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return b==="input"&&a.type==="button"||b==="button"},text:function(a){var b;return a.nodeName.toLowerCase()==="input"&&a.type==="text"&&((b=a.getAttribute("type"))==null||b.toLowerCase()===a.type)},first:s(function(){return[0]}),last:s(function(b,a){return[a-1]}),eq:s(function(c,b,a){return[a<0?a+b:a]}),even:s(function(b,c){for(var a=0;a<c;a+=2)b.push(a);return b}),odd:s(function(b,c){for(var a=1;a<c;a+=2)b.push(a);return b}),lt:s(function(b,d,a){for(var c=a<0?a+d:a;--c>=0;)b.push(c);return b}),gt:s(function(b,c,a){for(var d=a<0?a+c:a;++d<c;)b.push(d);return b})}};for(z in{radio:true,checkbox:true,file:true,password:true,image:true})b.pseudos[z]=lb(z);for(z in{submit:true,reset:true})b.pseudos[z]=kb(z);function J(g,k){var e,d,j,f,a,i,h,l=fb[g+" "];if(l)return k?0:l.slice(0);a=g;i=[];h=b.preFilter;while(a){if(!e||(d=yb.exec(a))){if(d)a=a.slice(d[0].length)||a;i.push(j=[])}e=false;if(d=ob.exec(a)){e=d.shift();j.push({value:e,type:d[0].replace(M," ")});a=a.slice(e.length)}for(f in b.filter)if((d=G[f].exec(a))&&(!h[f]||(d=h[f](d)))){e=d.shift();j.push({value:e,type:f,matches:d});a=a.slice(e.length)}if(!e)break}return k?a.length:a?c.error(g):fb(g,i).slice(0)}function F(c){for(var a=0,d=c.length,b="";a<d;a++)b+=c[a].value;return b}function P(c,d,g){var a=d.dir,b=g&&a==="parentNode",e=Ab++;return d.first?function(d,e,f){while(d=d[a])if(d.nodeType===1||b)return c(d,e,f)}:function(d,k,j){var i,g,h,l=n+" "+e;if(j){while(d=d[a])if(d.nodeType===1||b)if(c(d,k,j))return true}else while(d=d[a])if(d.nodeType===1||b){h=d[f]||(d[f]={});if((g=h[a])&&g[0]===l){if((i=g[1])===true||i===E)return i===true}else{g=h[a]=[l];g[1]=c(d,k,j)||E;if(g[1]===true)return true}}}}function O(a){return a.length>1?function(d,c,e){var b=a.length;while(b--)if(!a[b](d,c,e))return false;return true}:a[0]}function I(d,f,e,g,j){for(var b,c=[],a=0,i=d.length,h=f!=null;a<i;a++)if(b=d[a])if(!e||e(b,g,j)){c.push(b);h&&f.push(a)}return c}function R(c,e,d,b,a,g){if(b&&!b[f])b=R(b);if(a&&!a[f])a=R(a,g);return j(function(l,k,j,m){var h,g,i,r=[],o=[],p=k.length,s=l||mb(e||"*",j.nodeType?[j]:j,[]),n=c&&(l||!e)?I(s,r,c,j,m):s,f=d?a||(l?c:p||b)?[]:k:n;d&&d(n,f,j,m);if(b){h=I(f,o);b(h,[],j,m);g=h.length;while(g--)if(i=h[g])f[o[g]]=!(n[o[g]]=i)}if(l){if(a||c){if(a){h=[];g=f.length;while(g--)(i=f[g])&&h.push(n[g]=i);a(null,f=[],h,m)}g=f.length;while(g--)if((i=f[g])&&(h=a?w.call(l,i):r[g])>-1)l[h]=!(k[h]=i)}}else{f=I(f===k?f.splice(p,f.length):f);if(a)a(null,k,f,m);else q.apply(k,f)}})}function N(a){for(var j,e,d,h=a.length,i=b.relative[a[0].type],k=i||b.relative[" "],c=i?1:0,m=P(function(a){return a===j},k,true),l=P(function(a){return w.call(j,a)>-1},k,true),g=[function(c,a,b){return!i&&(b||a!==D)||((j=a).nodeType?m(c,a,b):l(c,a,b))}];c<h;c++)if(e=b.relative[a[c].type])g=[P(O(g),e)];else{e=b.filter[a[c].type].apply(null,a[c].matches);if(e[f]){d=++c;for(;d<h;d++)if(b.relative[a[d].type])break;return R(c>1&&O(g),c>1&&F(a.slice(0,c-1)).replace(M,"$1"),e,c<d&&N(a.slice(c,d)),d<h&&N(a=a.slice(d)),d<h&&F(a))}g.push(e)}return O(g)}function ib(f,a){var e=0,d=a.length>0,i=f.length>0,h=function(p,l,y,t,x){var k,u,o,j=[],m=0,h="0",s=p&&[],r=x!=null,v=D,z=p||i&&b.find.TAG("*",x&&l.parentNode||l),w=n+=v==null?1:Math.random()||.1;if(r){D=l!==g&&l;E=e}for(;(k=z[h])!=null;h++){if(i&&k){u=0;while(o=f[u++])if(o(k,l,y)){t.push(k);break}if(r){n=w;E=++e}}if(d){if(k=!o&&k)m--;p&&s.push(k)}}m+=h;if(d&&h!==m){u=0;while(o=a[u++])o(s,j,l,y);if(p){if(m>0)while(h--)if(!(s[h]||j[h]))j[h]=Bb.call(t);j=I(j)}q.apply(t,j);r&&!p&&j.length>0&&m+a.length>1&&c.uniqueSort(t)}if(r){n=w;D=v}return s};return d?j(h):h}V=c.compile=function(c,b){var d,g=[],e=[],a=Y[c+" "];if(!a){if(!b)b=J(c);d=b.length;while(d--){a=N(b[d]);if(a[f])g.push(a);else e.push(a)}a=Y(c,ib(e,g))}return a};function mb(e,b,d){for(var a=0,f=b.length;a<f;a++)c(e,b[a],d);return d}function zb(c,d,e,g){var j,a,f,l,k,h=J(c);if(!g)if(h.length===1){a=h[0]=h[0].slice(0);if(a.length>2&&(f=a[0]).type==="ID"&&d.nodeType===9&&i&&b.relative[a[1].type]){d=(b.find.ID(f.matches[0].replace(p,o),d)||[])[0];if(!d)return e;c=c.slice(a.shift().value.length)}j=G.needsContext.test(c)?0:a.length;while(j--){f=a[j];if(b.relative[l=f.type])break;if(k=b.find[l])if(g=k(f.matches[0].replace(p,o),U.test(a[0].type)&&d.parentNode||d)){a.splice(j,1);c=g.length&&F(a);if(!c){q.apply(e,g);return e}break}}}V(c,h)(g,d,!i,e,U.test(c));return e}b.pseudos.nth=b.pseudos.eq;function eb(){}eb.prototype=b.filters=b.pseudos;b.setFilters=new eb;e.sortStable=f.split("").sort(H).join("")===f;y();[0,0].sort(H);e.detectDuplicates=A;l(function(c){c.innerHTML="<a href='#'></a>";if(c.firstChild.getAttribute("href")!=="#"){var a="type|href|height|width".split("|"),d=a.length;while(d--)b.attrHandle[a[d]]=jb}});l(function(d){if(d.getAttribute("disabled")!=null){var a=S.split("|"),c=a.length;while(c--)b.attrHandle[a[c]]=pb}});a.find=c;a.expr=c.selectors;a.expr[":"]=a.expr.pseudos;a.unique=c.uniqueSort;a.text=c.getText;a.isXMLDoc=c.isXML;a.contains=c.contains})(e);var lb={};function Ub(b){var c=lb[b]={};a.each(b.match(j)||[],function(b,a){c[a]=true});return c}a.Callbacks=function(d){d=typeof d==="string"?lb[d]||Ub(d):a.extend({},d);var f,l,i,k,g,h,c=[],e=!d.once&&[],m=function(a){f=d.memory&&a;l=true;h=k||0;k=0;g=c.length;i=true;for(;c&&h<g;h++)if(c[h].apply(a[0],a[1])===false&&d.stopOnFalse){f=false;break}i=false;if(c)if(e)e.length&&m(e.shift());else if(f)c=[];else j.disable()},j={add:function(){if(c){var b=c.length;(function e(b){a.each(b,function(g,b){var f=a.type(b);if(f==="function")(!d.unique||!j.has(b))&&c.push(b);else b&&b.length&&f!=="string"&&e(b)})})(arguments);if(i)g=c.length;else if(f){k=b;m(f)}}return this},remove:function(){c&&a.each(arguments,function(e,d){var b;while((b=a.inArray(d,c,b))>-1){c.splice(b,1);if(i){if(b<=g)g--;if(b<=h)h--}}});return this},has:function(b){return b?a.inArray(b,c)>-1:!!(c&&c.length)},empty:function(){c=[];g=0;return this},disable:function(){c=e=f=b;return this},disabled:function(){return!c},lock:function(){e=b;!f&&j.disable();return this},locked:function(){return!e},fireWith:function(b,a){a=a||[];a=[b,a.slice?a.slice():a];if(c&&(!l||e))if(i)e.push(a);else m(a);return this},fire:function(){j.fireWith(this,arguments);return this},fired:function(){return!!l}};return j};a.extend({Deferred:function(f){var d=[["resolve","done",a.Callbacks("once memory"),"resolved"],["reject","fail",a.Callbacks("once memory"),"rejected"],["notify","progress",a.Callbacks("memory")]],e="pending",c={state:function(){return e},always:function(){b.done(arguments).fail(arguments);return this},then:function(){var e=arguments;return a.Deferred(function(f){a.each(d,function(h,g){var i=g[0],d=a.isFunction(e[h])&&e[h];b[g[1]](function(){var b=d&&d.apply(this,arguments);if(b&&a.isFunction(b.promise))b.promise().done(f.resolve).fail(f.reject).progress(f.notify);else f[i+"With"](this===c?f.promise():this,d?[b]:arguments)})});e=null}).promise()},promise:function(b){return b!=null?a.extend(b,c):c}},b={};c.pipe=c.then;a.each(d,function(h,a){var f=a[2],g=a[3];c[a[1]]=f.add;g&&f.add(function(){e=g},d[h^1][2].disable,d[2][2].lock);b[a[0]]=function(){b[a[0]+"With"](this===b?c:this,arguments);return this};b[a[0]+"With"]=f.fireWith});c.promise(b);f&&f.call(b,b);return b},when:function(i){var c=0,d=n.call(arguments),b=d.length,f=b!==1||i&&a.isFunction(i.promise)?b:0,e=f===1?i:a.Deferred(),k=function(c,b,a){return function(d){b[c]=this;a[c]=arguments.length>1?n.call(arguments):d;if(a===h)e.notifyWith(b,a);else!--f&&e.resolveWith(b,a)}},h,j,g;if(b>1){h=new Array(b);j=new Array(b);g=new Array(b);for(;c<b;c++)if(d[c]&&a.isFunction(d[c].promise))d[c].promise().done(k(c,g,d)).fail(e.reject).progress(k(c,j,h));else--f}!f&&e.resolveWith(g,d);return e.promise()}});a.support=function(b){var c=d.createElement("input"),g=d.createDocumentFragment(),f=d.createElement("div"),h=d.createElement("select"),i=h.appendChild(d.createElement("option"));if(!c.type)return b;c.type="checkbox";b.checkOn=c.value!=="";b.optSelected=i.selected;b.reliableMarginRight=true;b.boxSizingReliable=true;b.pixelPosition=false;c.checked=true;b.noCloneChecked=c.cloneNode(true).checked;h.disabled=true;b.optDisabled=!i.disabled;c=d.createElement("input");c.value="t";c.type="radio";b.radioValue=c.value==="t";c.setAttribute("checked","t");c.setAttribute("name","t");g.appendChild(c);b.checkClone=g.cloneNode(true).cloneNode(true).lastChild.checked;b.focusinBubbles="onfocusin"in e;f.style.backgroundClip="content-box";f.cloneNode(true).style.backgroundClip="";b.clearCloneStyle=f.style.backgroundClip==="content-box";a(function(){var h,c,i="padding:0;margin:0;border:0;display:block;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box",g=d.getElementsByTagName("body")[0];if(!g)return;h=d.createElement("div");h.style.cssText="border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px";g.appendChild(h).appendChild(f);f.innerHTML="";f.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:1px;border:1px;display:block;width:4px;margin-top:1%;position:absolute;top:1%";a.swap(g,g.style.zoom!=null?{zoom:1}:{},function(){b.boxSizing=f.offsetWidth===4});if(e.getComputedStyle){b.pixelPosition=(e.getComputedStyle(f,null)||{}).top!=="1%";b.boxSizingReliable=(e.getComputedStyle(f,null)||{width:"4px"}).width==="4px";c=f.appendChild(d.createElement("div"));c.style.cssText=f.style.cssText=i;c.style.marginRight=c.style.width="0";f.style.width="1px";b.reliableMarginRight=!parseFloat((e.getComputedStyle(c,null)||{}).marginRight)}g.removeChild(h)});return b}({});var f,c,Cc=/(?:\{[\s\S]*\}|\[[\s\S]*\])$/,ic=/([A-Z])/g;function l(){Object.defineProperty(this.cache={},0,{"get":function(){return{}}});this.expando=a.expando+Math.random()}l.uid=1;l.accepts=function(a){return a.nodeType?a.nodeType===1||a.nodeType===9:true};l.prototype={key:function(d){if(!l.accepts(d))return 0;var c={},b=d[this.expando];if(!b){b=l.uid++;try{c[this.expando]={value:b};Object.defineProperties(d,c)}catch(e){c[this.expando]=b;a.extend(d,c)}}if(!this.cache[b])this.cache[b]={};return b},"set":function(f,b,g){var d,e=this.key(f),c=this.cache[e];if(typeof b==="string")c[b]=g;else if(a.isEmptyObject(c))this.cache[e]=b;else for(d in b)c[d]=b[d]},"get":function(d,c){var a=this.cache[this.key(d)];return c===b?a:a[c]},access:function(d,a,c){if(a===b||a&&typeof a==="string"&&c===b)return this.get(d,a);this.set(d,a,c);return c!==b?c:a},remove:function(h,d){var f,c,g=this.key(h),e=this.cache[g];if(d===b)this.cache[g]={};else{if(a.isArray(d))c=d.concat(d.map(a.camelCase));else if(d in e)c=[d];else{c=a.camelCase(d);c=c in e?[c]:c.match(j)||[]}f=c.length;while(f--)delete e[c[f]]}},hasData:function(b){return!a.isEmptyObject(this.cache[b[this.expando]]||{})},discard:function(a){delete this.cache[this.key(a)]}};f=new l;c=new l;a.extend({acceptData:l.accepts,hasData:function(a){return f.hasData(a)||c.hasData(a)},data:function(b,c,a){return f.access(b,c,a)},removeData:function(a,b){f.remove(a,b)},_data:function(b,d,a){return c.access(b,d,a)},_removeData:function(a,b){c.remove(a,b)}});a.fn.extend({data:function(e,k){var h,g,d=this[0],j=0,i=null;if(e===b){if(this.length){i=f.get(d);if(d.nodeType===1&&!c.get(d,"hasDataAttrs")){h=d.attributes;for(;j<h.length;j++){g=h[j].name;if(g.indexOf("data-")===0){g=a.camelCase(g.substring(5));Ab(d,g,i[g])}}c.set(d,"hasDataAttrs",true)}}return i}return typeof e==="object"?this.each(function(){f.set(this,e)}):a.access(this,function(h){var c,g=a.camelCase(e);if(d&&h===b){c=f.get(d,e);if(c!==b)return c;c=f.get(d,g);if(c!==b)return c;c=Ab(d,g,b);if(c!==b)return c;return}this.each(function(){var a=f.get(this,g);f.set(this,g,h);e.indexOf("-")!==-1&&a!==b&&f.set(this,e,h)})},null,k,arguments.length>1,null,true)},removeData:function(a){return this.each(function(){f.remove(this,a)})}});function Ab(c,e,a){var d;if(a===b&&c.nodeType===1){d="data-"+e.replace(ic,"-$1").toLowerCase();a=c.getAttribute(d);if(typeof a==="string"){try{a=a==="true"?true:a==="false"?false:a==="null"?null:+a+""===a?+a:Cc.test(a)?JSON.parse(a):a}catch(g){}f.set(c,e,a)}else a=b}return a}a.extend({queue:function(f,e,d){var b;if(f){e=(e||"fx")+"queue";b=c.get(f,e);if(d)if(!b||a.isArray(d))b=c.access(f,e,a.makeArray(d));else b.push(d);return b||[]}},dequeue:function(f,b){b=b||"fx";var e=a.queue(f,b),g=e.length,d=e.shift(),c=a._queueHooks(f,b),h=function(){a.dequeue(f,b)};if(d==="inprogress"){d=e.shift();g--}c.cur=d;if(d){b==="fx"&&e.unshift("inprogress");delete c.stop;d.call(f,h,c)}!g&&c&&c.empty.fire()},_queueHooks:function(b,e){var d=e+"queueHooks";return c.get(b,d)||c.access(b,d,{empty:a.Callbacks("once memory").add(function(){c.remove(b,[e+"queue",d])})})}});a.fn.extend({queue:function(c,d){var e=2;if(typeof c!=="string"){d=c;c="fx";e--}return arguments.length<e?a.queue(this[0],c):d===b?this:this.each(function(){var b=a.queue(this,c,d);a._queueHooks(this,c);c==="fx"&&b[0]!=="inprogress"&&a.dequeue(this,c)})},dequeue:function(b){return this.each(function(){a.dequeue(this,b)})},delay:function(b,c){b=a.fx?a.fx.speeds[b]||b:b;c=c||"fx";return this.queue(c,function(d,c){var a=setTimeout(d,b);c.stop=function(){clearTimeout(a)}})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(d,j){var e,h=1,i=a.Deferred(),f=this,k=this.length,g=function(){!--h&&i.resolveWith(f,[f])};if(typeof d!=="string"){j=d;d=b}d=d||"fx";while(k--){e=c.get(f[k],d+"queueHooks");if(e&&e.empty){h++;e.empty.add(g)}}g();return i.promise(j)}});var tc,zb,U=/[\t\r\n]/g,Ac=/\r/g,hc=/^(?:input|select|textarea|button)$/i;a.fn.extend({attr:function(c,b){return a.access(this,a.attr,c,b,arguments.length>1)},removeAttr:function(b){return this.each(function(){a.removeAttr(this,b)})},prop:function(c,b){return a.access(this,a.prop,c,b,arguments.length>1)},removeProp:function(b){return this.each(function(){delete this[a.propFix[b]||b]})},addClass:function(b){var g,c,d,e,h,f=0,k=this.length,i=typeof b==="string"&&b;if(a.isFunction(b))return this.each(function(c){a(this).addClass(b.call(this,c,this.className))});if(i){g=(b||"").match(j)||[];for(;f<k;f++){c=this[f];d=c.nodeType===1&&(c.className?(" "+c.className+" ").replace(U," "):" ");if(d){h=0;while(e=g[h++])if(d.indexOf(" "+e+" ")<0)d+=e+" ";c.className=a.trim(d)}}}return this},removeClass:function(b){var g,d,c,e,h,f=0,k=this.length,i=arguments.length===0||typeof b==="string"&&b;if(a.isFunction(b))return this.each(function(c){a(this).removeClass(b.call(this,c,this.className))});if(i){g=(b||"").match(j)||[];for(;f<k;f++){d=this[f];c=d.nodeType===1&&(d.className?(" "+d.className+" ").replace(U," "):"");if(c){h=0;while(e=g[h++])while(c.indexOf(" "+e+" ")>=0)c=c.replace(" "+e+" "," ");d.className=b?a.trim(c):""}}}return this},toggleClass:function(b,d){var e=typeof b,f=typeof d==="boolean";return a.isFunction(b)?this.each(function(c){a(this).toggleClass(b.call(this,c,this.className,d),d)}):this.each(function(){if(e==="string"){var g,l=0,i=a(this),h=d,k=b.match(j)||[];while(g=k[l++]){h=f?h:!i.hasClass(g);i[h?"addClass":"removeClass"](g)}}else if(e===y||e==="boolean"){this.className&&c.set(this,"__className__",this.className);this.className=this.className||b===false?"":c.get(this,"__className__")||""}})},hasClass:function(c){for(var b=" "+c+" ",a=0,d=this.length;a<d;a++)if(this[a].nodeType===1&&(" "+this[a].className+" ").replace(U," ").indexOf(b)>=0)return true;return false},val:function(f){var c,d,g,e=this[0];if(!arguments.length){if(e){c=a.valHooks[e.type]||a.valHooks[e.nodeName.toLowerCase()];if(c&&"get"in c&&(d=c.get(e,"value"))!==b)return d;d=e.value;return typeof d==="string"?d.replace(Ac,""):d==null?"":d}return}g=a.isFunction(f);return this.each(function(h){var d,e=a(this);if(this.nodeType!==1)return;if(g)d=f.call(this,h,e.val());else d=f;if(d==null)d="";else if(typeof d==="number")d+="";else if(a.isArray(d))d=a.map(d,function(a){return a==null?"":a+""});c=a.valHooks[this.type]||a.valHooks[this.nodeName.toLowerCase()];if(!c||!("set"in c)||c.set(this,d,"value")===b)this.value=d})}});a.extend({valHooks:{option:{"get":function(a){var b=a.attributes.value;return!b||b.specified?a.value:a.text}},select:{"get":function(g){for(var f,b,h=g.options,c=g.selectedIndex,d=g.type==="select-one"||c<0,i=d?null:[],j=d?c+1:h.length,e=c<0?j:d?c:0;e<j;e++){b=h[e];if((b.selected||e===c)&&(a.support.optDisabled?!b.disabled:b.getAttribute("disabled")===null)&&(!b.parentNode.disabled||!a.nodeName(b.parentNode,"optgroup"))){f=a(b).val();if(d)return f;i.push(f)}}return i},"set":function(f,h){var c,b,d=f.options,e=a.makeArray(h),g=d.length;while(g--){b=d[g];if(b.selected=a.inArray(a(b).val(),e)>=0)c=true}if(!c)f.selectedIndex=-1;return e}}},attr:function(c,d,g){var e,f,h=c.nodeType;if(!c||h===3||h===8||h===2)return;if(typeof c.getAttribute===y)return a.prop(c,d,g);if(h!==1||!a.isXMLDoc(c)){d=d.toLowerCase();e=a.attrHooks[d]||(a.expr.match.boolean.test(d)?zb:tc)}if(g!==b)if(g===null)a.removeAttr(c,d);else if(e&&"set"in e&&(f=e.set(c,g,d))!==b)return f;else{c.setAttribute(d,g+"");return g}else if(e&&"get"in e&&(f=e.get(c,d))!==null)return f;else{f=a.find.attr(c,d);return f==null?b:f}},removeAttr:function(c,f){var b,e,g=0,d=f&&f.match(j);if(d&&c.nodeType===1)while(b=d[g++]){e=a.propFix[b]||b;if(a.expr.match.boolean.test(b))c[e]=false;c.removeAttribute(b)}},attrHooks:{type:{"set":function(b,c){if(!a.support.radioValue&&c==="radio"&&a.nodeName(b,"input")){var d=b.value;b.setAttribute("type",c);if(d)b.value=d;return c}}}},propFix:{"for":"htmlFor","class":"className"},prop:function(e,c,h){var g,d,i,f=e.nodeType;if(!e||f===3||f===8||f===2)return;i=f!==1||!a.isXMLDoc(e);if(i){c=a.propFix[c]||c;d=a.propHooks[c]}return h!==b?d&&"set"in d&&(g=d.set(e,h,c))!==b?g:(e[c]=h):d&&"get"in d&&(g=d.get(e,c))!==null?g:e[c]},propHooks:{tabIndex:{"get":function(a){return a.hasAttribute("tabindex")||hc.test(a.nodeName)||a.href?a.tabIndex:-1}}}});zb={"set":function(c,d,b){if(d===false)a.removeAttr(c,b);else c.setAttribute(b,b);return b}};a.each(a.expr.match.boolean.source.match(/\w+/g),function(e,c){var d=a.expr.attrHandle[c]||a.find.attr;a.expr.attrHandle[c]=function(f,c,e){var h=a.expr.attrHandle[c],g=e?b:(a.expr.attrHandle[c]=b)!=d(f,c,e)?c.toLowerCase():null;a.expr.attrHandle[c]=h;return g}});if(!a.support.optSelected)a.propHooks.selected={"get":function(b){var a=b.parentNode;if(a&&a.parentNode)a.parentNode.selectedIndex;return null}};a.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){a.propFix[this.toLowerCase()]=this});a.each(["radio","checkbox"],function(){a.valHooks[this]={"set":function(c,b){if(a.isArray(b))return c.checked=a.inArray(a(c).val(),b)>=0}};if(!a.support.checkOn)a.valHooks[this].get=function(a){return a.getAttribute("value")===null?"on":a.value}});var mc=/^key/,dc=/^(?:mouse|contextmenu)|click/,qb=/^(?:focusinfocus|focusoutblur)$/,gb=/^([^.]*)(?:\.(.+)|)$/;function C(){return true}function p(){return false}function cb(){try{return d.activeElement}catch(a){}}a.event={global:{},add:function(g,p,e,u,l){var n,h,r,o,s,i,f,k,d,q,t,m=c.get(g);if(!m)return;if(e.handler){n=e;e=n.handler;l=n.selector}if(!e.guid)e.guid=a.guid++;if(!(o=m.events))o=m.events={};if(!(h=m.handle)){h=m.handle=function(c){return typeof a!==y&&(!c||a.event.triggered!==c.type)?a.event.dispatch.apply(h.elem,arguments):b};h.elem=g}p=(p||"").match(j)||[""];s=p.length;while(s--){r=gb.exec(p[s])||[];d=t=r[1];q=(r[2]||"").split(".").sort();if(!d)continue;f=a.event.special[d]||{};d=(l?f.delegateType:f.bindType)||d;f=a.event.special[d]||{};i=a.extend({type:d,origType:t,data:u,handler:e,guid:e.guid,selector:l,needsContext:l&&a.expr.match.needsContext.test(l),"namespace":q.join(".")},n);if(!(k=o[d])){k=o[d]=[];k.delegateCount=0;if(!f.setup||f.setup.call(g,u,q,h)===false)g.addEventListener&&g.addEventListener(d,h,false)}if(f.add){f.add.call(g,i);if(!i.handler.guid)i.handler.guid=e.guid}if(l)k.splice(k.delegateCount++,0,i);else k.push(i);a.event.global[d]=true}g=null},remove:function(f,m,q,k,t){var n,r,g,l,o,d,e,h,b,p,s,i=c.hasData(f)&&c.get(f);if(!i||!(l=i.events))return;m=(m||"").match(j)||[""];o=m.length;while(o--){g=gb.exec(m[o])||[];b=s=g[1];p=(g[2]||"").split(".").sort();if(!b){for(b in l)a.event.remove(f,b+m[o],q,k,true);continue}e=a.event.special[b]||{};b=(k?e.delegateType:e.bindType)||b;h=l[b]||[];g=g[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)");r=n=h.length;while(n--){d=h[n];if((t||s===d.origType)&&(!q||q.guid===d.guid)&&(!g||g.test(d.namespace))&&(!k||k===d.selector||k==="**"&&d.selector)){h.splice(n,1);if(d.selector)h.delegateCount--;e.remove&&e.remove.call(f,d)}}if(r&&!h.length){(!e.teardown||e.teardown.call(f,p,i.handle)===false)&&a.removeEvent(f,b,i.handle);delete l[b]}}if(a.isEmptyObject(l)){delete i.handle;c.remove(f,"events")}},trigger:function(f,n,g,p){var s,i,j,r,m,l,k,q=[g||d],h=N.call(f,"type")?f.type:f,o=N.call(f,"namespace")?f.namespace.split("."):[];i=j=g=g||d;if(g.nodeType===3||g.nodeType===8)return;if(qb.test(h+a.event.triggered))return;if(h.indexOf(".")>=0){o=h.split(".");h=o.shift();o.sort()}m=h.indexOf(":")<0&&"on"+h;f=f[a.expando]?f:new a.Event(h,typeof f==="object"&&f);f.isTrigger=p?2:3;f.namespace=o.join(".");f.namespace_re=f.namespace?new RegExp("(^|\\.)"+o.join("\\.(?:.*\\.|)")+"(\\.|$)"):null;f.result=b;if(!f.target)f.target=g;n=n==null?[f]:a.makeArray(n,[f]);k=a.event.special[h]||{};if(!p&&k.trigger&&k.trigger.apply(g,n)===false)return;if(!p&&!k.noBubble&&!a.isWindow(g)){r=k.delegateType||h;if(!qb.test(r+h))i=i.parentNode;for(;i;i=i.parentNode){q.push(i);j=i}j===(g.ownerDocument||d)&&q.push(j.defaultView||j.parentWindow||e)}s=0;while((i=q[s++])&&!f.isPropagationStopped()){f.type=s>1?r:k.bindType||h;l=(c.get(i,"events")||{})[f.type]&&c.get(i,"handle");l&&l.apply(i,n);l=m&&i[m];l&&a.acceptData(i)&&l.apply&&l.apply(i,n)===false&&f.preventDefault()}f.type=h;if(!p&&!f.isDefaultPrevented())if((!k._default||k._default.apply(q.pop(),n)===false)&&a.acceptData(g))if(m&&a.isFunction(g[h])&&!a.isWindow(g)){j=g[m];if(j)g[m]=null;a.event.triggered=h;g[h]();a.event.triggered=b;if(j)g[m]=j}return f.result},dispatch:function(d){d=a.event.fix(d);var k,l,h,f,e,i=[],j=n.call(arguments),m=(c.get(this,"events")||{})[d.type]||[],g=a.event.special[d.type]||{};j[0]=d;d.delegateTarget=this;if(g.preDispatch&&g.preDispatch.call(this,d)===false)return;i=a.event.handlers.call(this,d,m);k=0;while((f=i[k++])&&!d.isPropagationStopped()){d.currentTarget=f.elem;l=0;while((e=f.handlers[l++])&&!d.isImmediatePropagationStopped())if(!d.namespace_re||d.namespace_re.test(e.namespace)){d.handleObj=e;d.data=e.data;h=((a.event.special[e.origType]||{}).handle||e.handler).apply(f.elem,j);if(h!==b)if((d.result=h)===false){d.preventDefault();d.stopPropagation()}}}g.postDispatch&&g.postDispatch.call(this,d);return d.result},handlers:function(i,h){var j,d,e,g,k=[],f=h.delegateCount,c=i.target;if(f&&c.nodeType&&(!i.button||i.type!=="click"))for(;c!==this;c=c.parentNode||this)if(c.disabled!==true||i.type!=="click"){d=[];for(j=0;j<f;j++){g=h[j];e=g.selector+" ";if(d[e]===b)d[e]=g.needsContext?a(e,this).index(c)>=0:a.find(e,this,null,[c]).length;d[e]&&d.push(g)}d.length&&k.push({elem:c,handlers:d})}f<h.length&&k.push({elem:this,handlers:h.slice(f)});return k},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(b,a){if(b.which==null)b.which=a.charCode!=null?a.charCode:a.keyCode;return b}},mouseHooks:{props:"button buttons clientX clientY offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(e,f){var h,c,a,g=f.button;if(e.pageX==null&&f.clientX!=null){h=e.target.ownerDocument||d;c=h.documentElement;a=h.body;e.pageX=f.clientX+(c&&c.scrollLeft||a&&a.scrollLeft||0)-(c&&c.clientLeft||a&&a.clientLeft||0);e.pageY=f.clientY+(c&&c.scrollTop||a&&a.scrollTop||0)-(c&&c.clientTop||a&&a.clientTop||0)}if(!e.which&&g!==b)e.which=g&1?1:g&2?3:g&4?2:0;return e}},fix:function(b){if(b[a.expando])return b;var h,g,f,d=b.type,e=b,c=this.fixHooks[d];if(!c)this.fixHooks[d]=c=dc.test(d)?this.mouseHooks:mc.test(d)?this.keyHooks:{};f=c.props?this.props.concat(c.props):this.props;b=new a.Event(e);h=f.length;while(h--){g=f[h];b[g]=e[g]}if(b.target.nodeType===3)b.target=b.target.parentNode;return c.filter?c.filter(b,e):b},special:{load:{noBubble:true},focus:{trigger:function(){if(this!==cb()&&this.focus){this.focus();return false}},delegateType:"focusin"},blur:{trigger:function(){if(this===cb()&&this.blur){this.blur();return false}},delegateType:"focusout"},click:{trigger:function(){if(this.type==="checkbox"&&this.click&&a.nodeName(this,"input")){this.click();return false}},_default:function(b){return a.nodeName(b.target,"a")}},beforeunload:{postDispatch:function(a){if(a.result!==b)a.originalEvent.returnValue=a.result}}},simulate:function(f,d,c,e){var b=a.extend(new a.Event,c,{type:f,isSimulated:true,originalEvent:{}});if(e)a.event.trigger(b,null,d);else a.event.dispatch.call(d,b);b.isDefaultPrevented()&&c.preventDefault()}};a.removeEvent=function(a,c,b){a.removeEventListener&&a.removeEventListener(c,b,false)};a.Event=function(b,c){if(!(this instanceof a.Event))return new a.Event(b,c);if(b&&b.type){this.originalEvent=b;this.type=b.type;this.isDefaultPrevented=b.defaultPrevented||b.getPreventDefault&&b.getPreventDefault()?C:p}else this.type=b;c&&a.extend(this,c);this.timeStamp=b&&b.timeStamp||a.now();this[a.expando]=true};a.Event.prototype={isDefaultPrevented:p,isPropagationStopped:p,isImmediatePropagationStopped:p,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=C;a&&a.preventDefault&&a.preventDefault()},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=C;a&&a.stopPropagation&&a.stopPropagation()},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=C;this.stopPropagation()}};a.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(c,b){a.event.special[c]={delegateType:b,bindType:b,handle:function(c){var g,f=this,d=c.relatedTarget,e=c.handleObj;if(!d||d!==f&&!a.contains(f,d)){c.type=e.origType;g=e.handler.apply(this,arguments);c.type=b}return g}}});!a.support.focusinBubbles&&a.each({focus:"focusin",blur:"focusout"},function(e,f){var b=0,c=function(b){a.event.simulate(f,b.target,a.event.fix(b),true)};a.event.special[f]={setup:function(){b++===0&&d.addEventListener(e,c,true)},teardown:function(){--b===0&&d.removeEventListener(e,c,true)}}});a.fn.extend({on:function(g,d,e,c,i){var f,h;if(typeof g==="object"){if(typeof d!=="string"){e=e||d;d=b}for(h in g)this.on(h,d,e,g[h],i);return this}if(e==null&&c==null){c=d;e=d=b}else if(c==null)if(typeof d==="string"){c=e;e=b}else{c=e;e=d;d=b}if(c===false)c=p;else if(!c)return this;if(i===1){f=c;c=function(b){a().off(b);return f.apply(this,arguments)};c.guid=f.guid||(f.guid=a.guid++)}return this.each(function(){a.event.add(this,g,c,e,d)})},one:function(b,a,c,d){return this.on(b,a,c,d,1)},off:function(c,e,f){var d,g;if(c&&c.preventDefault&&c.handleObj){d=c.handleObj;a(c.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler);return this}if(typeof c==="object"){for(g in c)this.off(g,e,c[g]);return this}if(e===false||typeof e==="function"){f=e;e=b}if(f===false)f=p;return this.each(function(){a.event.remove(this,c,f,e)})},trigger:function(c,b){return this.each(function(){a.event.trigger(c,b,this)})},triggerHandler:function(d,c){var b=this[0];if(b)return a.event.trigger(d,c,b,true)}});var rc=/^.[^:#\[\.,]*$/,jb=a.expr.match.needsContext,Pb={children:true,contents:true,next:true,prev:true};a.fn.extend({find:function(d){var f,c,b,e=this.length;if(typeof d!=="string"){f=this;return this.pushStack(a(d).filter(function(){for(b=0;b<e;b++)if(a.contains(f[b],this))return true}))}c=[];for(b=0;b<e;b++)a.find(d,this[b],c);c=this.pushStack(e>1?a.unique(c):c);c.selector=(this.selector?this.selector+" ":"")+d;return c},has:function(c){var b=a(c,this),d=b.length;return this.filter(function(){for(var c=0;c<d;c++)if(a.contains(this,b[c]))return true})},not:function(a){return this.pushStack(Gb(this,a||[],true))},filter:function(a){return this.pushStack(Gb(this,a||[],false))},is:function(b){return!!b&&(typeof b==="string"?jb.test(b)?a(b,this.context).index(this[0])>=0:a.filter(b,this).length>0:this.filter(b).length>0)},closest:function(c,f){for(var b,e=0,h=this.length,d=[],g=jb.test(c)||typeof c!=="string"?a(c,f||this.context):0;e<h;e++)for(b=this[e];b&&b!==f;b=b.parentNode)if(b.nodeType<11&&(g?g.index(b)>-1:b.nodeType===1&&a.find.matchesSelector(b,c))){b=d.push(b);break}return this.pushStack(d.length>1?a.unique(d):d)},index:function(b){return!b?this[0]&&this[0].parentNode?this.first().prevAll().length:-1:typeof b==="string"?A.call(a(b),this[0]):A.call(this,b.jquery?b[0]:b)},add:function(b,c){var e=typeof b==="string"?a(b,c):a.makeArray(b&&b.nodeType?[b]:b),d=a.merge(this.get(),e);return this.pushStack(a.unique(d))},addBack:function(a){return this.add(a==null?this.prevObject:this.prevObject.filter(a))}});function Fb(a,b){while((a=a[b])&&a.nodeType!==1);return a}a.each({parent:function(b){var a=b.parentNode;return a&&a.nodeType!==11?a:null},parents:function(b){return a.dir(b,"parentNode")},parentsUntil:function(c,d,b){return a.dir(c,"parentNode",b)},next:function(a){return Fb(a,"nextSibling")},prev:function(a){return Fb(a,"previousSibling")},nextAll:function(b){return a.dir(b,"nextSibling")},prevAll:function(b){return a.dir(b,"previousSibling")},nextUntil:function(c,d,b){return a.dir(c,"nextSibling",b)},prevUntil:function(c,d,b){return a.dir(c,"previousSibling",b)},siblings:function(b){return a.sibling((b.parentNode||{}).firstChild,b)},children:function(b){return a.sibling(b.firstChild)},contents:function(b){return a.nodeName(b,"iframe")?b.contentDocument||b.contentWindow.document:a.merge([],b.childNodes)}},function(b,c){a.fn[b]=function(f,e){var d=a.map(this,c,f);if(b.slice(-5)!=="Until")e=f;if(e&&typeof e==="string")d=a.filter(e,d);if(this.length>1){!Pb[b]&&a.unique(d);b[0]==="p"&&d.reverse()}return this.pushStack(d)}});a.extend({filter:function(b,c,e){var d=c[0];if(e)b=":not("+b+")";return c.length===1&&d.nodeType===1?a.find.matchesSelector(d,b)?[d]:[]:a.find.matches(b,a.grep(c,function(a){return a.nodeType===1}))},dir:function(c,g,e){var d=[],f=e!==b;while((c=c[g])&&c.nodeType!==9)if(c.nodeType===1){if(f&&a(c).is(e))break;d.push(c)}return d},sibling:function(a,c){for(var b=[];a;a=a.nextSibling)a.nodeType===1&&a!==c&&b.push(a);return b}});function Gb(c,b,d){if(a.isFunction(b))return a.grep(c,function(a,c){return!!b.call(a,c,a)!==d});if(b.nodeType)return a.grep(c,function(a){return a===b!==d});if(typeof b==="string"){if(rc.test(b))return a.filter(b,c,d);b=a.filter(b,c)}return a.grep(c,function(a){return A.call(b,a)>=0!==d})}var xb=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,Bb=/<([\w:]+)/,Fc=/<|&#?\w+;/,ac=/<(?:script|style|link)/i,Y=/^(?:checkbox|radio)$/i,vc=/checked\s*(?:[^=]|=\s*.checked.)/i,rb=/^$|\/(?:java|ecma)script/i,Nb=/^true\/(.*)/,Yb=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,h={option:[1,"<select multiple='multiple'>","</select>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:[0,"",""]};h.optgroup=h.option;h.tbody=h.tfoot=h.colgroup=h.caption=h.col=h.thead;h.th=h.td;a.fn.extend({text:function(c){return a.access(this,function(c){return c===b?a.text(this):this.empty().append((this[0]&&this[0].ownerDocument||d).createTextNode(c))},null,c,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(this.nodeType===1||this.nodeType===11||this.nodeType===9){var b=bb(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(b){if(this.nodeType===1||this.nodeType===11||this.nodeType===9){var a=bb(this,b);a.insertBefore(b,a.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(d,c){for(var b,f=d?a.filter(d,this):this,e=0;(b=f[e])!=null;e++){!c&&b.nodeType===1&&a.cleanData(i(b));if(b.parentNode){c&&a.contains(b.ownerDocument,b)&&J(i(b,"script"));b.parentNode.removeChild(b)}}return this},empty:function(){for(var b,c=0;(b=this[c])!=null;c++)if(b.nodeType===1){a.cleanData(i(b,false));b.textContent=""}return this},clone:function(b,c){b=b==null?false:b;c=c==null?b:c;return this.map(function(){return a.clone(this,b,c)})},html:function(c){return a.access(this,function(c){var d=this[0]||{},e=0,f=this.length;if(c===b&&d.nodeType===1)return d.innerHTML;if(typeof c==="string"&&!ac.test(c)&&!h[(Bb.exec(c)||["",""])[1].toLowerCase()]){c=c.replace(xb,"<$1></$2>");try{for(;e<f;e++){d=this[e]||{};if(d.nodeType===1){a.cleanData(i(d,false));d.innerHTML=c}}d=0}catch(g){}}d&&this.empty().append(c)},null,c,arguments.length)},replaceWith:function(){var c=a.map(this,function(a){return[a.nextSibling,a.parentNode]}),b=0;this.domManip(arguments,function(e){var f=c[b++],d=c[b++];if(d){a(this).remove();d.insertBefore(e,f)}},true);return b?this:this.remove()},detach:function(a){return this.remove(a,true)},domManip:function(g,o,m){g=mb.apply([],g);var f,l,e,h,b,p,d=0,k=this.length,r=this,q=k-1,j=g[0],n=a.isFunction(j);if(n||!(k<=1||typeof j!=="string"||a.support.checkClone||!vc.test(j)))return this.each(function(a){var b=r.eq(a);if(n)g[0]=j.call(this,a,b.html());b.domManip(g,o,m)});if(k){f=a.buildFragment(g,this[0].ownerDocument,false,!m&&this);l=f.firstChild;if(f.childNodes.length===1)f=l;if(l){e=a.map(i(f,"script"),Vb);h=e.length;for(;d<k;d++){b=f;if(d!==q){b=a.clone(b,true,true);h&&a.merge(e,i(b,"script"))}o.call(this[d],b,d)}if(h){p=e[e.length-1].ownerDocument;a.map(e,Wb);for(d=0;d<h;d++){b=e[d];if(rb.test(b.type||"")&&!c.access(b,"globalEval")&&a.contains(p,b))if(b.src)a._evalUrl(b.src);else a.globalEval(b.textContent.replace(Yb,""))}}}}return this}});a.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(c,b){a.fn[c]=function(h){for(var d,g=[],e=a(h),f=e.length-1,c=0;c<=f;c++){d=c===f?this:this.clone(true);a(e[c])[b](d);S.apply(g,d.get())}return this.pushStack(g)}});a.extend({clone:function(b,j,h){var c,g,e,d,f=b.cloneNode(true),k=a.contains(b.ownerDocument,b);if(!a.support.noCloneChecked&&(b.nodeType===1||b.nodeType===11)&&!a.isXMLDoc(b)){d=i(f);e=i(b);for(c=0,g=e.length;c<g;c++)qc(e[c],d[c])}if(j)if(h){e=e||i(b);d=d||i(f);for(c=0,g=e.length;c<g;c++)fb(e[c],d[c])}else fb(b,f);d=i(f,"script");d.length>0&&J(d,!k&&i(b,"script"));return f},buildFragment:function(o,k,n,l){for(var b,c,p,g,m,j,e=0,q=o.length,d=k.createDocumentFragment(),f=[];e<q;e++){b=o[e];if(b||b===0)if(a.type(b)==="object")a.merge(f,b.nodeType?[b]:b);else if(!Fc.test(b))f.push(k.createTextNode(b));else{c=c||d.appendChild(k.createElement("div"));p=(Bb.exec(b)||["",""])[1].toLowerCase();g=h[p]||h._default;c.innerHTML=g[1]+b.replace(xb,"<$1></$2>")+g[2];j=g[0];while(j--)c=c.firstChild;a.merge(f,c.childNodes);c=d.firstChild;c.textContent=""}}d.textContent="";e=0;while(b=f[e++]){if(l&&a.inArray(b,l)!==-1)continue;m=a.contains(b.ownerDocument,b);c=i(d.appendChild(b),"script");m&&J(c);if(n){j=0;while(b=c[j++])rb.test(b.type||"")&&n.push(b)}}return d},cleanData:function(h){for(var d,b,e,j=h.length,g=0,i=a.event.special;g<j;g++){b=h[g];if(a.acceptData(b)){d=c.access(b);if(d)for(e in d.events)if(i[e])a.event.remove(b,e);else a.removeEvent(b,e,d.handle)}f.discard(b);c.discard(b)}},_evalUrl:function(b){return a.ajax({url:b,type:"GET",dataType:"text",async:false,global:false,success:a.globalEval})}});function bb(b,c){return a.nodeName(b,"table")&&a.nodeName(c.nodeType===1?c:c.firstChild,"tr")?b.getElementsByTagName("tbody")[0]||b.appendChild(b.ownerDocument.createElement("tbody")):b}function Vb(a){a.type=(a.getAttribute("type")!==null)+"/"+a.type;return a}function Wb(a){var b=Nb.exec(a.type);if(b)a.type=b[1];else a.removeAttribute("type");return a}function J(d,b){for(var e=d.length,a=0;a<e;a++)c.set(d[a],"globalEval",!b||c.get(b[a],"globalEval"))}function fb(h,e){var i,m,g,j,d,l,k,b;if(e.nodeType!==1)return;if(c.hasData(h)){j=c.access(h);d=a.extend({},j);b=j.events;c.set(e,d);if(b){delete d.handle;d.events={};for(g in b)for(i=0,m=b[g].length;i<m;i++)a.event.add(e,g,b[g][i])}}if(f.hasData(h)){l=f.access(h);k=a.extend({},l);f.set(e,k)}}function i(c,d){var e=c.getElementsByTagName?c.getElementsByTagName(d||"*"):c.querySelectorAll?c.querySelectorAll(d||"*"):[];return d===b||d&&a.nodeName(c,d)?a.merge([c],e):e}function qc(c,b){var a=b.nodeName.toLowerCase();if(a==="input"&&Y.test(c.type))b.checked=c.checked;else if(a==="input"||a==="textarea")b.defaultValue=c.defaultValue}a.fn.extend({wrapAll:function(b){var c;if(a.isFunction(b))return this.each(function(c){a(this).wrapAll(b.call(this,c))});if(this[0]){c=a(b,this[0].ownerDocument).eq(0).clone(true);this[0].parentNode&&c.insertBefore(this[0]);c.map(function(){var a=this;while(a.firstElementChild)a=a.firstElementChild;return a}).append(this)}return this},wrapInner:function(b){return a.isFunction(b)?this.each(function(c){a(this).wrapInner(b.call(this,c))}):this.each(function(){var d=a(this),c=d.contents();if(c.length)c.wrapAll(b);else d.append(b)})},wrap:function(b){var c=a.isFunction(b);return this.each(function(d){a(this).wrapAll(c?b.call(this,d):b)})},unwrap:function(){return this.parent().each(function(){!a.nodeName(this,"body")&&a(this).replaceWith(this.childNodes)}).end()}});var q,w,Zb=/^(none|table(?!-c[ea]).+)/,Eb=/^margin/,oc=new RegExp("^("+E+")(.*)$","i"),T=new RegExp("^("+E+")(?!px)[a-z%]+$","i"),zc=new RegExp("^([+-])=("+E+")","i"),pb={BODY:"block"},yc={position:"absolute",visibility:"hidden",display:"block"},ab={letterSpacing:0,fontWeight:400},o=["Top","Right","Bottom","Left"],ob=["Webkit","O","Moz","ms"];function hb(b,a){if(a in b)return a;var e=a.charAt(0).toUpperCase()+a.slice(1),d=a,c=ob.length;while(c--){a=ob[c]+e;if(a in b)return a}return d}function u(b,c){b=c||b;return a.css(b,"display")==="none"||!a.contains(b.ownerDocument,b)}function F(a){return e.getComputedStyle(a,null)}function Cb(g,i){for(var e,b,h,f=[],d=0,j=g.length;d<j;d++){b=g[d];if(!b.style)continue;f[d]=c.get(b,"olddisplay");e=b.style.display;if(i){if(!f[d]&&e==="none")b.style.display="";if(b.style.display===""&&u(b))f[d]=c.access(b,"olddisplay",Mb(b.nodeName))}else if(!f[d]){h=u(b);(e&&e!=="none"||!h)&&c.set(b,"olddisplay",h?e:a.css(b,"display"))}}for(d=0;d<j;d++){b=g[d];if(!b.style)continue;if(!i||b.style.display==="none"||b.style.display==="")b.style.display=i?f[d]||"":"none"}return g}a.fn.extend({css:function(d,c){return a.access(this,function(d,c,g){var f,h,i={},e=0;if(a.isArray(c)){f=F(d);h=c.length;for(;e<h;e++)i[c[e]]=a.css(d,c[e],false,f);return i}return g!==b?a.style(d,c,g):a.css(d,c)},d,c,arguments.length>1)},show:function(){return Cb(this,true)},hide:function(){return Cb(this)},toggle:function(b){var c=typeof b==="boolean";return this.each(function(){if(c?b:u(this))a(this).show();else a(this).hide()})}});a.extend({cssHooks:{opacity:{"get":function(c,b){if(b){var a=q(c,"opacity");return a===""?"1":a}}}},cssNumber:{columnCount:true,fillOpacity:true,fontWeight:true,lineHeight:true,opacity:true,orphans:true,widows:true,zIndex:true,zoom:true},cssProps:{"float":"cssFloat"},style:function(d,e,c,k){if(!d||d.nodeType===3||d.nodeType===8||!d.style)return;var i,h,f,g=a.camelCase(e),j=d.style;e=a.cssProps[g]||(a.cssProps[g]=hb(j,g));f=a.cssHooks[e]||a.cssHooks[g];if(c!==b){h=typeof c;if(h==="string"&&(i=zc.exec(c))){c=(i[1]+1)*i[2]+parseFloat(a.css(d,e));h="number"}if(c==null||h==="number"&&isNaN(c))return;if(h==="number"&&!a.cssNumber[g])c+="px";if(!a.support.clearCloneStyle&&c===""&&e.indexOf("background")===0)j[e]="inherit";if(!f||!("set"in f)||(c=f.set(d,c,k))!==b)j[e]=c}else return f&&"get"in f&&(i=f.get(d,false,k))!==b?i:j[e]},css:function(h,d,f,j){var c,i,g,e=a.camelCase(d);d=a.cssProps[e]||(a.cssProps[e]=hb(h.style,e));g=a.cssHooks[d]||a.cssHooks[e];if(g&&"get"in g)c=g.get(h,true,f);if(c===b)c=q(h,d,j);if(c==="normal"&&d in ab)c=ab[d];if(f===""||f){i=parseFloat(c);return f===true||a.isNumeric(i)?i||0:c}return c}});q=function(f,g,k){var j,i,h,e=k||F(f),d=e?e.getPropertyValue(g)||e[g]:b,c=f.style;if(e){if(d===""&&!a.contains(f.ownerDocument,f))d=a.style(f,g);if(T.test(d)&&Eb.test(g)){j=c.width;i=c.minWidth;h=c.maxWidth;c.minWidth=c.maxWidth=c.width=d;d=e.width;c.width=j;c.minWidth=i;c.maxWidth=h}}return d};function db(d,b,c){var a=oc.exec(b);return a?Math.max(0,a[1]-(c||0))+(a[2]||"px"):b}function Z(f,h,c,g,e){for(var b=c===(g?"border":"content")?4:h==="width"?1:0,d=0;b<4;b+=2){if(c==="margin")d+=a.css(f,c+o[b],true,e);if(g){if(c==="content")d-=a.css(f,"padding"+o[b],true,e);if(c!=="margin")d-=a.css(f,"border"+o[b]+"Width",true,e)}else{d+=a.css(f,"padding"+o[b],true,e);if(c!=="padding")d+=a.css(f,"border"+o[b]+"Width",true,e)}}return d}function eb(c,d,h){var f=true,b=d==="width"?c.offsetWidth:c.offsetHeight,e=F(c),g=a.support.boxSizing&&a.css(c,"boxSizing",false,e)==="border-box";if(b<=0||b==null){b=q(c,d,e);if(b<0||b==null)b=c.style[d];if(T.test(b))return b;f=g&&(a.support.boxSizingReliable||b===c.style[d]);b=parseFloat(b)||0}return b+Z(c,d,h||(g?"border":"content"),f,e)+"px"}function Mb(e){var c=d,b=pb[e];if(!b){b=ib(e,c);if(b==="none"||!b){w=(w||a("<iframe frameborder='0' width='0' height='0'/>").css("cssText","display:block !important")).appendTo(c.documentElement);c=(w[0].contentWindow||w[0].contentDocument).document;c.write("<!doctype html><html><body>");c.close();b=ib(e,c);w.detach()}pb[e]=b}return b}function ib(e,c){var b=a(c.createElement(e)).appendTo(c.body),d=a.css(b[0],"display");b.remove();return d}a.each(["height","width"],function(c,b){a.cssHooks[b]={"get":function(c,e,d){if(e)return c.offsetWidth===0&&Zb.test(a.css(c,"display"))?a.swap(c,yc,function(){return eb(c,b,d)}):eb(c,b,d)},"set":function(c,f,d){var e=d&&F(c);return db(c,f,d?Z(c,b,d,a.support.boxSizing&&a.css(c,"boxSizing",false,e)==="border-box",e):0)}}});a(function(){if(!a.support.reliableMarginRight)a.cssHooks.marginRight={"get":function(b,c){if(c)return a.swap(b,{display:"inline-block"},q,[b,"marginRight"])}};!a.support.pixelPosition&&a.fn.position&&a.each(["top","left"],function(c,b){a.cssHooks[b]={"get":function(d,c){if(c){c=q(d,b);return T.test(c)?a(d).position()[b]+"px":c}}}})});if(a.expr&&a.expr.filters){a.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0};a.expr.filters.visible=function(b){return!a.expr.filters.hidden(b)}}a.each({margin:"",padding:"",border:"Width"},function(b,c){a.cssHooks[b+c]={expand:function(e){for(var a=0,f={},d=typeof e==="string"?e.split(" "):[e];a<4;a++)f[b+o[a]+c]=d[a]||d[a-2]||d[0];return f}};if(!Eb.test(b))a.cssHooks[b+c].set=db});var Ic=/%20/g,uc=/\[\]$/,Hb=/\r?\n/g,Rb=/^(?:submit|button|image|reset|file)$/i,bc=/^(?:input|select|textarea|keygen)/i;a.fn.extend({serialize:function(){return a.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var b=a.prop(this,"elements");return b?a.makeArray(b):this}).filter(function(){var b=this.type;return this.name&&!a(this).is(":disabled")&&bc.test(this.nodeName)&&!Rb.test(b)&&(this.checked||!Y.test(b))}).map(function(d,c){var b=a(this).val();return b==null?null:a.isArray(b)?a.map(b,function(a){return{name:c.name,value:a.replace(Hb,"\r\n")}}):{name:c.name,value:b.replace(Hb,"\r\n")}}).get()}});a.param=function(c,d){var e,f=[],g=function(c,b){b=a.isFunction(b)?b():b==null?"":b;f[f.length]=encodeURIComponent(c)+"="+encodeURIComponent(b)};if(d===b)d=a.ajaxSettings&&a.ajaxSettings.traditional;if(a.isArray(c)||c.jquery&&!a.isPlainObject(c))a.each(c,function(){g(this.name,this.value)});else for(e in c)M(e,c[e],d,g);return f.join("&").replace(Ic,"+")};function M(c,b,d,e){var f;if(a.isArray(b))a.each(b,function(b,a){if(d||uc.test(c))e(c,a);else M(c+"["+(typeof a==="object"?b:"")+"]",a,d,e)});else if(!d&&a.type(b)==="object")for(f in b)M(c+"["+f+"]",b[f],d,e);else e(c,b)}a.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(c,b){a.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}});a.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return arguments.length===1?this.off(a,"**"):this.off(b,a||"**",c)}});var m,k,Q=a.now(),L=/\?/,Ec=/#.*$/,Kb=/([?&])_=[^&]*/,xc=/^(.*?):[ \t]*([^\r\n]*)$/mg,Sb=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,jc=/^(?:GET|HEAD)$/,pc=/^\/\//,Jb=/^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,Ib=a.fn.load,sb={},R={},yb="*/".concat("*");try{k=sc.href}catch(Kc){k=d.createElement("a");k.href="";k=k.href}m=Jb.exec(k.toLowerCase())||[];function X(b){return function(d,e){if(typeof d!=="string"){e=d;d="*"}var c,g=0,f=d.toLowerCase().match(j)||[];if(a.isFunction(e))while(c=f[g++])if(c[0]==="+"){c=c.slice(1)||"*";(b[c]=b[c]||[]).unshift(e)}else(b[c]=b[c]||[]).push(e)}}function W(f,d,g,h){var b={},e=f===R;function c(i){var j;b[i]=true;a.each(f[i]||[],function(i,f){var a=f(d,g,h);if(typeof a==="string"&&!e&&!b[a]){d.dataTypes.unshift(a);c(a);return false}else if(e)return!(j=a)});return j}return c(d.dataTypes[0])||!b["*"]&&c("*")}function P(e,f){var c,d,g=a.ajaxSettings.flatOptions||{};for(c in f)if(f[c]!==b)(g[c]?e:d||(d={}))[c]=f[c];d&&a.extend(true,e,d);return e}a.fn.load=function(d,c,e){if(typeof d!=="string"&&Ib)return Ib.apply(this,arguments);var f,j,i,g=this,h=d.indexOf(" ");if(h>=0){f=d.slice(h);d=d.slice(0,h)}if(a.isFunction(c)){e=c;c=b}else if(c&&typeof c==="object")j="POST";g.length>0&&a.ajax({url:d,type:j,dataType:"html",data:c}).done(function(b){i=arguments;g.html(f?a("<div>").append(a.parseHTML(b)).find(f):b)}).complete(e&&function(a,b){g.each(e,i||[a.responseText,b,a])});return this};a.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(c,b){a.fn[b]=function(a){return this.on(b,a)}});a.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:k,type:"GET",isLocal:Sb.test(m[1]),global:true,processData:true,async:true,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":yb,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":true,"text json":a.parseJSON,"text xml":a.parseXML},flatOptions:{url:true,context:true}},ajaxSetup:function(c,b){return b?P(P(c,a.ajaxSettings),b):P(a.ajaxSettings,c)},ajaxPrefilter:X(sb),ajaxTransport:X(R),ajax:function(s,g){if(typeof s==="object"){g=s;s=b}g=g||{};var i,e,t,p,v,l,o,n,c=a.ajaxSetup({},g),h=c.context||c,u=c.context&&(h.nodeType||h.jquery)?a(h):a.event,w=a.Deferred(),y=a.Callbacks("once memory"),q=c.statusCode||{},z={},x={},f=0,A="canceled",d={readyState:0,getResponseHeader:function(b){var a;if(f===2){if(!p){p={};while(a=xc.exec(t))p[a[1].toLowerCase()]=a[2]}a=p[b.toLowerCase()]}return a==null?null:a},getAllResponseHeaders:function(){return f===2?t:null},setRequestHeader:function(a,c){var b=a.toLowerCase();if(!f){a=x[b]=x[b]||a;z[a]=c}return this},overrideMimeType:function(a){if(!f)c.mimeType=a;return this},statusCode:function(b){var a;if(b)if(f<2)for(a in b)q[a]=[q[a],b[a]];else d.always(b[d.status]);return this},abort:function(b){var a=b||A;i&&i.abort(a);r(0,a);return this}};w.promise(d).complete=y.add;d.success=d.done;d.error=d.fail;c.url=((s||c.url||k)+"").replace(Ec,"").replace(pc,m[1]+"//");c.type=g.method||g.type||c.method||c.type;c.dataTypes=a.trim(c.dataType||"*").toLowerCase().match(j)||[""];if(c.crossDomain==null){l=Jb.exec(c.url.toLowerCase());c.crossDomain=!!(l&&(l[1]!==m[1]||l[2]!==m[2]||(l[3]||(l[1]==="http:"?"80":"443"))!==(m[3]||(m[1]==="http:"?"80":"443"))))}if(c.data&&c.processData&&typeof c.data!=="string")c.data=a.param(c.data,c.traditional);W(sb,c,g,d);if(f===2)return d;o=c.global;o&&a.active++===0&&a.event.trigger("ajaxStart");c.type=c.type.toUpperCase();c.hasContent=!jc.test(c.type);e=c.url;if(!c.hasContent){if(c.data){e=c.url+=(L.test(e)?"&":"?")+c.data;delete c.data}if(c.cache===false)c.url=Kb.test(e)?e.replace(Kb,"$1_="+Q++):e+(L.test(e)?"&":"?")+"_="+Q++}if(c.ifModified){a.lastModified[e]&&d.setRequestHeader("If-Modified-Since",a.lastModified[e]);a.etag[e]&&d.setRequestHeader("If-None-Match",a.etag[e])}(c.data&&c.hasContent&&c.contentType!==false||g.contentType)&&d.setRequestHeader("Content-Type",c.contentType);d.setRequestHeader("Accept",c.dataTypes[0]&&c.accepts[c.dataTypes[0]]?c.accepts[c.dataTypes[0]]+(c.dataTypes[0]!=="*"?", "+yb+"; q=0.01":""):c.accepts["*"]);for(n in c.headers)d.setRequestHeader(n,c.headers[n]);if(c.beforeSend&&(c.beforeSend.call(h,d,c)===false||f===2))return d.abort();A="abort";for(n in{success:1,error:1,complete:1})d[n](c[n]);i=W(R,c,g,d);if(!i)r(-1,"No Transport");else{d.readyState=1;o&&u.trigger("ajaxSend",[d,c]);if(c.async&&c.timeout>0)v=setTimeout(function(){d.abort("timeout")},c.timeout);try{f=1;i.send(z,r)}catch(B){if(f<2)r(-1,B);else throw B;}}function r(j,r,s,x){var k,p,n,m,l,g=r;if(f===2)return;f=2;v&&clearTimeout(v);i=b;t=x||"";d.readyState=j>0?4:0;k=j>=200&&j<300||j===304;if(s)m=Lb(c,d,s);m=cc(c,m,d,k);if(k){if(c.ifModified){l=d.getResponseHeader("Last-Modified");if(l)a.lastModified[e]=l;l=d.getResponseHeader("etag");if(l)a.etag[e]=l}if(j===204)g="nocontent";else if(j===304)g="notmodified";else{g=m.state;p=m.data;n=m.error;k=!n}}else{n=g;if(j||!g){g="error";if(j<0)j=0}}d.status=j;d.statusText=(r||g)+"";if(k)w.resolveWith(h,[p,g,d]);else w.rejectWith(h,[d,g,n]);d.statusCode(q);q=b;o&&u.trigger(k?"ajaxSuccess":"ajaxError",[d,c,k?p:n]);y.fireWith(h,[d,g]);if(o){u.trigger("ajaxComplete",[d,c]);!--a.active&&a.event.trigger("ajaxStop")}}return d},getJSON:function(d,c,b){return a.get(d,c,b,"json")},getScript:function(d,c){return a.get(d,b,c,"script")}});a.each(["get","post"],function(d,c){a[c]=function(g,d,e,f){if(a.isFunction(d)){f=f||e;e=d;d=b}return a.ajax({url:g,type:c,dataType:f,data:d,success:e})}});function Lb(f,j,h){var e,d,c,g,i=f.contents,a=f.dataTypes;while(a[0]==="*"){a.shift();if(e===b)e=f.mimeType||j.getResponseHeader("Content-Type")}if(e)for(d in i)if(i[d]&&i[d].test(e)){a.unshift(d);break}if(a[0]in h)c=a[0];else{for(d in h){if(!a[0]||f.converters[d+" "+a[0]]){c=d;break}if(!g)g=d}c=c||g}if(c){c!==a[0]&&a.unshift(c);return h[c]}}function cc(c,e,k,j){var i,a,b,g,f,d={},h=c.dataTypes.slice();if(h[1])for(b in c.converters)d[b.toLowerCase()]=c.converters[b];a=h.shift();while(a){if(c.responseFields[a])k[c.responseFields[a]]=e;if(!f&&j&&c.dataFilter)e=c.dataFilter(e,c.dataType);f=a;a=h.shift();if(a)if(a==="*")a=f;else if(f!=="*"&&f!==a){b=d[f+" "+a]||d["* "+a];if(!b)for(i in d){g=i.split(" ");if(g[1]===a){b=d[f+" "+g[0]]||d["* "+g[0]];if(b){if(b===true)b=d[i];else if(d[i]!==true){a=g[0];h.unshift(g[1])}break}}}if(b!==true)if(b&&c["throws"])e=b(e);else try{e=b(e)}catch(l){return{state:"parsererror",error:b?l:"No conversion from "+f+" to "+a}}}}return{state:"success",data:e}}a.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(b){a.globalEval(b);return b}}});a.ajaxPrefilter("script",function(a){if(a.cache===b)a.cache=false;if(a.crossDomain)a.type="GET"});a.ajaxTransport("script",function(e){if(e.crossDomain){var c,b;return{send:function(g,f){c=a("<script>").prop({async:true,charset:e.scriptCharset,src:e.url}).on("load error",b=function(a){c.remove();b=null;a&&f(a.type==="error"?404:200,a.type)});d.head.appendChild(c[0])},abort:function(){b&&b()}}}});var kb=[],V=/(=)\?(?=&|$)|\?\?/;a.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var b=kb.pop()||a.expando+"_"+Q++;this[b]=true;return b}});a.ajaxPrefilter("json jsonp",function(c,i,j){var d,g,f,h=c.jsonp!==false&&(V.test(c.url)?"url":typeof c.data==="string"&&!(c.contentType||"").indexOf("application/x-www-form-urlencoded")&&V.test(c.data)&&"data");if(h||c.dataTypes[0]==="jsonp"){d=c.jsonpCallback=a.isFunction(c.jsonpCallback)?c.jsonpCallback():c.jsonpCallback;if(h)c[h]=c[h].replace(V,"$1"+d);else if(c.jsonp!==false)c.url+=(L.test(c.url)?"&":"?")+c.jsonp+"="+d;c.converters["script json"]=function(){!f&&a.error(d+" was not called");return f[0]};c.dataTypes[0]="json";g=e[d];e[d]=function(){f=arguments};j.always(function(){e[d]=g;if(c[d]){c.jsonpCallback=i.jsonpCallback;kb.push(d)}f&&a.isFunction(g)&&g(f[0]);f=g=b});return"script"}});a.ajaxSettings.xhr=function(){try{return new XMLHttpRequest}catch(a){}};var t=a.ajaxSettings.xhr(),Qb={0:200,1223:204},Gc=0,s={};if(e.ActiveXObject)a(e).on("unload",function(){for(var a in s)s[a]();s=b});a.support.cors=!!t&&"withCredentials"in t;a.support.ajax=t=!!t;a.ajaxTransport(function(c){var d;if(a.support.cors||t&&!c.crossDomain)return{send:function(f,g){var e,h,a=c.xhr();a.open(c.type,c.url,c.async,c.username,c.password);if(c.xhrFields)for(e in c.xhrFields)a[e]=c.xhrFields[e];c.mimeType&&a.overrideMimeType&&a.overrideMimeType(c.mimeType);if(!c.crossDomain&&!f["X-Requested-With"])f["X-Requested-With"]="XMLHttpRequest";for(e in f)a.setRequestHeader(e,f[e]);d=function(c){return function(){if(d){delete s[h];d=a.onload=a.onerror=null;if(c==="abort")a.abort();else if(c==="error")g(a.status||404,a.statusText);else g(Qb[a.status]||a.status,a.statusText,typeof a.responseText==="string"?{text:a.responseText}:b,a.getAllResponseHeaders())}}};a.onload=d();a.onerror=d("error");d=s[h=Gc++]=d("abort");a.send(c.hasContent&&c.data||null)},abort:function(){d&&d()}}});var r,H,wc=/^(?:toggle|show|hide)$/,Dc=new RegExp("^(?:([+-])=|)("+E+")([a-z%]*)$","i"),Hc=/queueHooks$/,x=[Ob],v={"*":[function(f,j){var h,g,b=this.createTween(f,j),e=Dc.exec(j),i=b.cur(),c=+i||0,d=1,k=20;if(e){h=+e[2];g=e[3]||(a.cssNumber[f]?"":"px");if(g!=="px"&&c){c=a.css(b.elem,f,true)||h||1;do{d=d||".5";c=c/d;a.style(b.elem,f,c+g)}while(d!==(d=b.cur()/i)&&d!==1&&--k)}b.unit=g;b.start=c;b.end=e[1]?c+(e[1]+1)*h:h}return b}]};function nb(){setTimeout(function(){r=b});return r=a.now()}function Xb(b,c){a.each(c,function(d,f){for(var c=(v[d]||[]).concat(v["*"]),a=0,e=c.length;a<e;a++)if(c[a].call(b,d,f))return})}function vb(c,j,e){var g,f,h=0,l=x.length,d=a.Deferred().always(function(){delete k.elem}),k=function(){if(f)return false;for(var i=r||nb(),a=Math.max(0,b.startTime+b.duration-i),j=a/b.duration||0,e=1-j,g=0,h=b.tweens.length;g<h;g++)b.tweens[g].run(e);d.notifyWith(c,[b,e,a]);if(e<1&&h)return a;else{d.resolveWith(c,[b]);return false}},b=d.promise({elem:c,props:a.extend({},j),opts:a.extend(true,{specialEasing:{}},e),originalProperties:j,originalOptions:e,startTime:r||nb(),duration:e.duration,tweens:[],createTween:function(e,f){var d=a.Tween(c,b.opts,e,f,b.opts.specialEasing[e]||b.opts.easing);b.tweens.push(d);return d},stop:function(a){var e=0,g=a?b.tweens.length:0;if(f)return this;f=true;for(;e<g;e++)b.tweens[e].run(1);if(a)d.resolveWith(c,[b,a]);else d.rejectWith(c,[b,a]);return this}}),i=b.props;fc(i,b.opts.specialEasing);for(;h<l;h++){g=x[h].call(b,c,i,b.opts);if(g)return g}Xb(b,i);a.isFunction(b.opts.start)&&b.opts.start.call(c,b);a.fx.timer(a.extend(k,{elem:c,anim:b,queue:b.opts.queue}));return b.progress(b.opts.progress).done(b.opts.done,b.opts.complete).fail(b.opts.fail).always(b.opts.always)}function fc(d,h){var b,e,f,c,g;for(b in d){e=a.camelCase(b);f=h[e];c=d[b];if(a.isArray(c)){f=c[1];c=d[b]=c[0]}if(b!==e){d[e]=c;delete d[b]}g=a.cssHooks[e];if(g&&"expand"in g){c=g.expand(c);delete d[e];for(b in c)if(!(b in d)){d[b]=c[b];h[b]=f}}else h[e]=f}}a.Animation=a.extend(vb,{tweener:function(b,e){if(a.isFunction(b)){e=b;b=["*"]}else b=b.split(" ");for(var c,d=0,f=b.length;d<f;d++){c=b[d];v[c]=v[c]||[];v[c].unshift(e)}},prefilter:function(a,b){if(b)x.unshift(a);else x.push(a)}});function Ob(d,m,l){var h,f,o,q,e,r,n,g,t,k=this,i=d.style,s={},p=[],j=d.nodeType&&u(d);if(!l.queue){g=a._queueHooks(d,"fx");if(g.unqueued==null){g.unqueued=0;t=g.empty.fire;g.empty.fire=function(){!g.unqueued&&t()}}g.unqueued++;k.always(function(){k.always(function(){g.unqueued--;!a.queue(d,"fx").length&&g.empty.fire()})})}if(d.nodeType===1&&("height"in m||"width"in m)){l.overflow=[i.overflow,i.overflowX,i.overflowY];if(a.css(d,"display")==="inline"&&a.css(d,"float")==="none")i.display="inline-block"}if(l.overflow){i.overflow="hidden";k.always(function(){i.overflow=l.overflow[0];i.overflowX=l.overflow[1];i.overflowY=l.overflow[2]})}e=c.get(d,"fxshow");for(h in m){o=m[h];if(wc.exec(o)){delete m[h];r=r||o==="toggle";if(o===(j?"hide":"show"))if(o==="show"&&e!==b&&e[h]!==b)j=true;else continue;p.push(h)}}q=p.length;if(q){e=c.get(d,"fxshow")||c.access(d,"fxshow",{});if("hidden"in e)j=e.hidden;if(r)e.hidden=!j;if(j)a(d).show();else k.done(function(){a(d).hide()});k.done(function(){var b;c.remove(d,"fxshow");for(b in s)a.style(d,b,s[b])});for(h=0;h<q;h++){f=p[h];n=k.createTween(f,j?e[f]:0);s[f]=e[f]||a.style(d,f);if(!(f in e)){e[f]=n.start;if(j){n.end=n.start;n.start=f==="width"||f==="height"?1:0}}}}}function g(c,a,d,e,b){return new g.prototype.init(c,a,d,e,b)}a.Tween=g;g.prototype={constructor:g,init:function(e,c,b,g,d,f){this.elem=e;this.prop=b;this.easing=d||"swing";this.options=c;this.start=this.now=this.cur();this.end=g;this.unit=f||(a.cssNumber[b]?"":"px")},cur:function(){var a=g.propHooks[this.prop];return a&&a.get?a.get(this):g.propHooks._default.get(this)},run:function(b){var c,d=g.propHooks[this.prop];if(this.options.duration)this.pos=c=a.easing[this.easing](b,this.options.duration*b,0,1,this.options.duration);else this.pos=c=b;this.now=(this.end-this.start)*c+this.start;this.options.step&&this.options.step.call(this.elem,this.now,this);if(d&&d.set)d.set(this);else g.propHooks._default.set(this);return this}};g.prototype.init.prototype=g.prototype;g.propHooks={_default:{"get":function(b){var c;if(b.elem[b.prop]!=null&&(!b.elem.style||b.elem.style[b.prop]==null))return b.elem[b.prop];c=a.css(b.elem,b.prop,"");return!c||c==="auto"?0:c},"set":function(b){if(a.fx.step[b.prop])a.fx.step[b.prop](b);else if(b.elem.style&&(b.elem.style[a.cssProps[b.prop]]!=null||a.cssHooks[b.prop]))a.style(b.elem,b.prop,b.now+b.unit);else b.elem[b.prop]=b.now}}};g.propHooks.scrollTop=g.propHooks.scrollLeft={"set":function(a){if(a.elem.nodeType&&a.elem.parentNode)a.elem[a.prop]=a.now}};a.each(["toggle","show","hide"],function(d,b){var c=a.fn[b];a.fn[b]=function(a,e,d){return a==null||typeof a==="boolean"?c.apply(this,arguments):this.animate(I(b,true),a,e,d)}});a.fn.extend({fadeTo:function(c,d,b,a){return this.filter(u).css("opacity",0).show().end().animate({opacity:d},c,b,a)},animate:function(f,i,h,g){var e=a.isEmptyObject(f),d=a.speed(i,h,g),b=function(){var g=vb(this,a.extend({},f),d);b.finish=function(){g.stop(true)};(e||c.get(this,"finish"))&&g.stop(true)};b.finish=b;return e||d.queue===false?this.each(b):this.queue(d.queue,b)},stop:function(d,f,e){var g=function(a){var b=a.stop;delete a.stop;b(e)};if(typeof d!=="string"){e=f;f=d;d=b}f&&d!==false&&this.queue(d||"fx",[]);return this.each(function(){var i=true,b=d!=null&&d+"queueHooks",h=a.timers,f=c.get(this);if(b)f[b]&&f[b].stop&&g(f[b]);else for(b in f)f[b]&&f[b].stop&&Hc.test(b)&&g(f[b]);for(b=h.length;b--;)if(h[b].elem===this&&(d==null||h[b].queue===d)){h[b].anim.stop(e);i=false;h.splice(b,1)}(i||!e)&&a.dequeue(this,d)})},finish:function(b){if(b!==false)b=b||"fx";return this.each(function(){var d,h=c.get(this),f=h[b+"queue"],g=h[b+"queueHooks"],e=a.timers,i=f?f.length:0;h.finish=true;a.queue(this,b,[]);g&&g.cur&&g.cur.finish&&g.cur.finish.call(this);for(d=e.length;d--;)if(e[d].elem===this&&e[d].queue===b){e[d].anim.stop(true);e.splice(d,1)}for(d=0;d<i;d++)f[d]&&f[d].finish&&f[d].finish.call(this);delete h.finish})}});function I(d,b){var c,a={height:d},e=0;b=b?1:0;for(;e<4;e+=2-b){c=o[e];a["margin"+c]=a["padding"+c]=d}if(b)a.opacity=a.width=d;return a}a.each({slideDown:I("show"),slideUp:I("hide"),slideToggle:I("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(c,b){a.fn[c]=function(d,c,a){return this.animate(b,d,c,a)}});a.speed=function(c,d,e){var b=c&&typeof c==="object"?a.extend({},c):{complete:e||!e&&d||a.isFunction(c)&&c,duration:c,easing:e&&d||d&&!a.isFunction(d)&&d};b.duration=a.fx.off?0:typeof b.duration==="number"?b.duration:b.duration in a.fx.speeds?a.fx.speeds[b.duration]:a.fx.speeds._default;if(b.queue==null||b.queue===true)b.queue="fx";b.old=b.complete;b.complete=function(){a.isFunction(b.old)&&b.old.call(this);b.queue&&a.dequeue(this,b.queue)};return b};a.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}};a.timers=[];a.fx=g.prototype.init;a.fx.tick=function(){var e,c=a.timers,d=0;r=a.now();for(;d<c.length;d++){e=c[d];!e()&&c[d]===e&&c.splice(d--,1)}!c.length&&a.fx.stop();r=b};a.fx.timer=function(b){b()&&a.timers.push(b)&&a.fx.start()};a.fx.interval=13;a.fx.start=function(){if(!H)H=setInterval(a.fx.tick,a.fx.interval)};a.fx.stop=function(){clearInterval(H);H=null};a.fx.speeds={slow:600,fast:200,_default:400};a.fx.step={};if(a.expr&&a.expr.filters)a.expr.filters.animated=function(b){return a.grep(a.timers,function(a){return b===a.elem}).length};a.fn.offset=function(h){if(arguments.length)return h===b?this:this.each(function(b){a.offset.setOffset(this,h,b)});var d,g,c=this[0],e={top:0,left:0},f=c&&c.ownerDocument;if(!f)return;d=f.documentElement;if(!a.contains(d,c))return e;if(typeof c.getBoundingClientRect!==y)e=c.getBoundingClientRect();g=wb(f);return{top:e.top+g.pageYOffset-d.clientTop,left:e.left+g.pageXOffset-d.clientLeft}};a.offset={setOffset:function(c,b,n){var f,k,h,l,d,g,m,i=a.css(c,"position"),j=a(c),e={};if(i==="static")c.style.position="relative";d=j.offset();h=a.css(c,"top");g=a.css(c,"left");m=(i==="absolute"||i==="fixed")&&(h+g).indexOf("auto")>-1;if(m){f=j.position();l=f.top;k=f.left}else{l=parseFloat(h)||0;k=parseFloat(g)||0}if(a.isFunction(b))b=b.call(c,n,d);if(b.top!=null)e.top=b.top-d.top+l;if(b.left!=null)e.left=b.left-d.left+k;if("using"in b)b.using.call(c,e);else j.css(e)}};a.fn.extend({position:function(){if(!this[0])return;var b,d,e=this[0],c={top:0,left:0};if(a.css(e,"position")==="fixed")d=e.getBoundingClientRect();else{b=this.offsetParent();d=this.offset();if(!a.nodeName(b[0],"html"))c=b.offset();c.top+=a.css(b[0],"borderTopWidth",true);c.left+=a.css(b[0],"borderLeftWidth",true)}return{top:d.top-c.top-a.css(e,"marginTop",true),left:d.left-c.left-a.css(e,"marginLeft",true)}},offsetParent:function(){return this.map(function(){var b=this.offsetParent||Db;while(b&&(!a.nodeName(b,"html")&&a.css(b,"position")==="static"))b=b.offsetParent;return b||Db})}});a.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(c,d){var f="pageYOffset"===d;a.fn[c]=function(g){return a.access(this,function(g,h,a){var c=wb(g);if(a===b)return c?c[d]:g[h];if(c)c.scrollTo(!f?a:e.pageXOffset,f?a:e.pageYOffset);else g[h]=a},c,g,arguments.length,null)}});function wb(b){return a.isWindow(b)?b:b.nodeType===9&&b.defaultView}a.each({Height:"height",Width:"width"},function(c,d){a.each({padding:"inner"+c,content:d,"":"outer"+c},function(e,f){a.fn[f]=function(f,i){var g=arguments.length&&(e||typeof f!=="boolean"),h=e||(f===true||i===true?"margin":"border");return a.access(this,function(d,g,f){var e;if(a.isWindow(d))return d.document.documentElement["client"+c];if(d.nodeType===9){e=d.documentElement;return Math.max(d.body["scroll"+c],e["scroll"+c],d.body["offset"+c],e["offset"+c],e["client"+c])}return f===b?a.css(d,g,h):a.style(d,g,f,h)},d,g?f:b,g,null)}})});a.fn.size=function(){return this.length};a.fn.andSelf=a.fn.addBack;if(typeof module==="object"&&typeof module.exports==="object")module.exports=a;else typeof define==="function"&&define.amd&&define("jquery",[],function(){return a});if(typeof e==="object"&&typeof e.document==="object")e.jQuery=e.$=a})(window);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\base\js\lib\jquery.rest.js */ 
(function(a){a.support.cors=true;var f=a.ajax;a.restSetup={methodParam:"_method",useMethodOverride:false,verbs:{create:"POST",update:"PUT",destroy:"DELETE"}};a(document).ready(function(){a.restSetup.csrfParam=a.restSetup.csrfParam||a("meta[name=csrf-param]").attr("content");a.restSetup.csrfToken=a.restSetup.csrfToken||a("meta[name=csrf-token]").attr("content")});function c(a){return{url:a[0],data:a.length>=2?a[1]:undefined,success:a.length>=3?a[2]:undefined,error:a.length>=4?a[3]:undefined}}function b(c){var b={dataType:"json"};if(arguments.length===1&&typeof arguments[0]!=="string"){b=a.extend(b,c);if("url"in b)if("data"in b)b.url=e(b.url,b.data)}else{if(a.isFunction(data)){error=success;success=data;data=null}c=e(c,data);b=a.extend(b,{url:c,data:data,cache:true,success:function(c,e,a){success&&success.call(b.context||b,c,d(a),a)},error:function(a){error&&error.call(b.context||b,a,d(a))}})}return b}function e(b,c){var a,d,e;for(a in c){e=c[a];d=b.replace("{"+a+"}",e);if(d!=b){b=d;delete c[a]}}return b}function d(c){var b={},d=a.trim(c.getAllResponseHeaders());a.each(d.split("\n"),function(e,d){if(d.length){var c=d.match(/^([\w\-]+):(.*)/);if(c.length===3)b[c[1]]=a.trim(c[2])}});c.responseHeaders=b;return b}a.ajax=function(b){var e=new RegExp("("+a.restSetup.csrfParam+"=)","i"),d=b.beforeSend,c;if(typeof b.data!=="string"&&Object.prototype.toString.apply(b.data)!=="[object FormData]")if(b.data!=null)b.data=a.param(b.data);b.data=b.data||"";if(a.restSetup.csrfParam&&a.restSetup.csrfToken)if(!/^(get)$/i.test(b.type))if(!e.test(b.data))b.data+=(b.data?"&":"")+a.restSetup.csrfParam+"="+a.restSetup.csrfToken;if(a.restSetup.useMethodOverride)if(!/^(get|post)$/i.test(b.type)){c=b.type.toUpperCase();b.data+=(b.data?"&":"")+a.restSetup.methodParam+"="+b.type.toLowerCase();b.type="POST"}b.beforeSend=function(g,h){var i=b.context||b,e=b.contentType,f=/.*\.(json|xml)/i.exec(b.url);if(!e)e=a.restSetup.contentType;if(!e&&f)e="application/"+f[1].toLowerCase();b.contentType!=e&&a.extend(b,{contentType:e});c&&g.setRequestHeader("X-HTTP-Method-Override",c);a.isFunction(d)&&d.call(i,g,h)};return f.call(this,b)};a.read=function(e){if(!e.url)e=c(arguments);var d=b(e);d.type="GET";d.crossDomain="true";d.xhrFields={withCredentials:true};return a.ajax(d)};a.create=function(e){if(!e.url)e=c(arguments);var d=b(e);d.type=a.restSetup.verbs.create;d.crossDomain="true";d.xhrFields={withCredentials:true};return a.ajax(d)};a.update=function(e){if(!e.url)e=c(arguments);var d=b(e);d.type=a.restSetup.verbs.update;d.crossDomain="true";d.xhrFields={withCredentials:true};return a.ajax(d)};a.destroy=function(e){if(!e.url)e=c(arguments);var d=b(e);d.type=a.restSetup.verbs.destroy;d.crossDomain="true";d.xhrFields={withCredentials:true};return a.ajax(d)}})(jQuery);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\base\js\lib\tablet\kendo.mobile.min.js */ 
(function(a){a()})(function(){(function(c,d){function C(){}function Ib(a,d){if(d)return"'"+a.split("'").join("\\'").split('\\"').join('\\\\\\"').replace(/\n/g,"\\n").replace(/\r/g,"\\r").replace(/\t/g,"\\t")+"'";var b=a.charAt(0),c=a.substring(1);return"="===b?"+("+c+")+":":"===b?"+e("+c+")+":";"+a+";o+="}function g(a,b,c){return a+="",b=b||2,c=b-a.length,c?jb[b].substring(0,c)+a:a}function bb(d){var e=d.css(a.support.transitions.css+"box-shadow")||d.css("box-shadow"),b=e?e.match(xb)||[0,0,0,0,0]:[0,0,0,0,0],c=r.max(+b[3],+(b[4]||0));return{left:-b[1]+c,right:+b[1]+c,bottom:+b[2]+c}}function yb(a,h){var i,d,e,g,m,l,f,j,k=b.browser,n="rtl"==a.css("direction");return a.parent().hasClass("k-animation-container")?(f=a.parent(".k-animation-container"),j=f[0].style,f.is(":hidden")&&f.show(),i=z.test(j.width)||z.test(j.height),i||f.css({width:a.outerWidth(),height:a.outerHeight(),boxSizing:"content-box",mozBoxSizing:"content-box",webkitBoxSizing:"content-box"})):(d=bb(a),e=a[0].style.width,g=a[0].style.height,m=z.test(e),l=z.test(g),k.opera&&(d.left=d.right=d.bottom=5),i=m||l,!m&&(!h||h&&e)&&(e=a.outerWidth()),!l&&(!h||h&&g)&&(g=a.outerHeight()),a.wrap(c("<div/>").addClass("k-animation-container").css({width:e,height:g,marginLeft:d.left*(n?1:-1),paddingLeft:d.left,paddingRight:d.right,paddingBottom:d.bottom})),i&&a.css({width:"100%",height:"100%",boxSizing:"border-box",mozBoxSizing:"border-box",webkitBoxSizing:"border-box"})),k.msie&&r.floor(k.version)<=7&&(a.css({zoom:1}),a.children(".k-menu").width(a.width())),a.parent()}function lb(b){for(var a=1,c=arguments.length,a=1;c>a;a++)nb(b,arguments[a]);return b}function nb(d,g){var c,b,f,e,h=a.data.ObservableArray,j=a.data.DataSource,i=a.data.HierarchicalDataSource;for(c in g)b=g[c],f=typeof b,f===n&&null!==b&&b.constructor!==Array&&b.constructor!==h&&b.constructor!==j&&b.constructor!==i?b instanceof Date?(d[c]=new Date(b.getTime())):s(b.clone)?(d[c]=b.clone()):(e=d[c],d[c]=typeof e===n?e||{}:{},nb(d[c],b)):f!==x&&(d[c]=b);return d}function I(c,a,e){for(var b in a)if(a.hasOwnProperty(b)&&a[b].test(c))return b;return e!==d?e:c}function Ab(a){return a.replace(/([a-z][A-Z])/g,function(a){return a.charAt(0)+"-"+a.charAt(1).toLowerCase()})}function X(a){return a.replace(/\-(\w)/g,function(b,a){return a.toUpperCase()})}function Cb(f,e){var d,b={};return document.defaultView&&document.defaultView.getComputedStyle?(d=document.defaultView.getComputedStyle(f,""),e&&c.each(e,function(c,a){b[a]=d.getPropertyValue(a)})):(d=f.currentStyle,e&&c.each(e,function(c,a){b[a]=d[X(a)]})),a.size(b)||(b=d),b}function Hb(b){var a,c=0;for(a in b)b.hasOwnProperty(a)&&"toJSON"!=a&&c++;return c}function Fb(d,c,e){c||(c="offset");var a=d[c]();return b.browser.msie&&(b.pointers||b.msPointers)&&!e&&(a.top-=window.pageYOffset-document.documentElement.scrollTop,a.left-=window.pageXOffset-document.documentElement.scrollLeft),a}function Eb(a){var b={};return w("string"==typeof a?a.split(" "):a,function(a){b[a]=this}),b}function Db(b){return new a.effects.Element(b)}function Mb(c,a,b,d){return typeof c===f&&(s(a)&&(d=a,a=400,b=!1),s(b)&&(d=b,b=!1),typeof a===L&&(b=a,a=400),c={effects:c,duration:a,reverse:b,complete:d}),i({effects:{},duration:400,reverse:!1,init:A,teardown:A,hide:!1},c,{completeCallback:c.complete,complete:A})}function ob(d,f,h,g,e){for(var a,b=0,i=d.length;i>b;b++)a=c(d[b]),a.queue(function(){p.promise(a,Mb(f,h,g,e))});return d}function Kb(b,a,d,c){return a&&(a=a.split(" "),w(a,function(d,a){b.toggleClass(a,c)})),b}function zb(a){return(""+a).replace(eb,"&amp;").replace(W,"&lt;").replace(rb,"&gt;").replace(U,"&quot;").replace(db,"&#39;")}function J(e,c){var b;return 0===c.indexOf("data")&&(c=c.substring(4),c=c.charAt(0).toLowerCase()+c.substring(1)),c=c.replace(O,"-$1"),b=e.getAttribute("data-"+a.ns+c),null===b?(b=d):"null"===b?(b=null):"true"===b?(b=!0):"false"===b?(b=!1):ub.test(b)?(b=parseFloat(b)):fb.test(b)&&!qb.test(b)&&(b=Function("return ("+b+")")()),b}function kb(g,h){var e,b,f={};for(e in h)b=J(g,e),b!==d&&(sb.test(e)&&(b=a.template(c("#"+b).html())),f[e]=b);return f}function Jb(b,a){return c.contains(b,a)?-1:1}function Lb(){var a=c(this);return c.inArray(a.attr("data-role"),["slider","rangeslider"])>0||a.is(":visible")}function Bb(a,c){var b=a.nodeName.toLowerCase();return(/input|select|textarea|button|object/.test(b)?!a.disabled:"a"===b?a.href||c:c)&&Gb(a)}function Gb(a){return!c(a).parents().addBack().filter(function(){return"hidden"===c.css(this,"visibility")||c.expr.filters.hidden(this)}).length}function h(a,b){return new h.fn.init(a,b)}var q,s,Y,V,B,gb,hb,tb,Z,cb,mb,jb,pb,p,eb,W,U,db,rb,K,ib,ab,t,sb,fb,qb,O,E,Q,D,R,e,m,T,F,y,M,N,k,a=window.kendo=window.kendo||{cultures:{}},i=c.extend,w=c.each,vb=c.isArray,G=c.proxy,A=c.noop,r=Math,u=window.JSON||{},b={},z=/%/,wb=/\{(\d+)(:[^\}]+)?\}/g,xb=/(\d+(?:\.?)\d*)px\s*(\d+(?:\.?)\d*)px\s*(\d+(?:\.?)\d*)px\s*(\d+)?/i,ub=/^(\+|-?)\d+(\.?)\d*$/,j="function",f="string",l="number",n="object",v="null",L="boolean",x="undefined",P={},S={},H=[].slice,o=window.Globalize;a.version="2014.1.416",C.extend=function(a){var b,d,f=function(){},e=this,c=a&&a.init?a.init:function(){e.apply(this,arguments)};f.prototype=e.prototype,d=c.fn=c.prototype=new f;for(b in a)d[b]=null!=a[b]&&a[b].constructor===Object?i(!0,{},f.prototype[b],a[b]):a[b];return d.constructor=c,c.extend=e.extend,c},C.prototype._initOptions=function(a){this.options=lb({},this.options,a)},s=a.isFunction=function(a){return"function"==typeof a},Y=function(){this._defaultPrevented=!0},V=function(){return this._defaultPrevented===!0},B=C.extend({init:function(){this._events={}},bind:function(a,g,n){var c,k,h,e,l,b=this,i=typeof a===f?[a]:a,m=typeof g===j;if(g===d){for(c in a)b.bind(c,a[c]);return b}for(c=0,k=i.length;k>c;c++)a=i[c],e=m?g:g[a],e&&(n&&(h=e,e=function(){b.unbind(a,e),h.apply(b,arguments)},e.original=h),l=b._events[a]=b._events[a]||[],l.push(e));return b},one:function(a,b){return this.bind(a,b,!0)},first:function(a,e){var b,h,d,g,c=this,i=typeof a===f?[a]:a,k=typeof e===j;for(b=0,h=i.length;h>b;b++)a=i[b],d=k?e:e[a],d&&(g=c._events[a]=c._events[a]||[],g.unshift(d));return c},trigger:function(f,a){var c,e,d=this,b=d._events[f];if(b){for(a=a||{},a.sender=d,a._defaultPrevented=!1,a.preventDefault=Y,a.isDefaultPrevented=V,b=b.slice(),c=0,e=b.length;e>c;c++)b[c].call(d,a);return a._defaultPrevented===!0}return!1},unbind:function(e,f){var a,c=this,b=c._events[e];if(e===d)c._events={};else if(b)if(f)for(a=b.length-1;a>=0;a--)(b[a]===f||b[a].original===f)&&b.splice(a,1);else c._events[e]=[];return c}}),gb=/^\w+/,hb=/\$\{([^}]*)\}/g,tb=/\\\}/g,Z=/__CURLY__/g,cb=/\\#/g,mb=/__SHARP__/g,jb=["","0","00","000","0000"],q={paramName:"data",useWithBlock:!0,render:function(e,d){var a,c,b="";for(a=0,c=d.length;c>a;a++)b+=e(d[a]);return b},compile:function(d,m){var g,f,e,j=i({},this,m),k=j.paramName,l=k.match(gb)[0],h=j.useWithBlock,b="var o,e=kendo.htmlEncode;";if(s(d))return 2===d.length?function(a){return d(c,{data:a}).join("")}:d;for(b+=h?"with("+k+"){":"",b+="o=",f=d.replace(tb,"__CURLY__").replace(hb,"#=e($1)#").replace(Z,"}").replace(cb,"__SHARP__").split("#"),e=0;f.length>e;e++)b+=Ib(f[e],e%2===0);b+=h?";}":";",b+="return o;",b=b.replace(mb,"#");try{return g=Function(l,b),g._slotCount=Math.floor(f.length/2),g}catch(n){throw Error(a.format("Invalid template:'{0}' Generated code:'{1}'",d,b));}}},function(){function h(a){return e.lastIndex=0,e.test(a)?'"'+a.replace(e,function(a){var b=i[a];return typeof b===f?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function d(s,t){var m,o,g,q,i,p,r=a,e=t[s];if(e&&typeof e===n&&typeof e.toJSON===j&&(e=e.toJSON(s)),typeof b===j&&(e=b.call(t,s,e)),p=typeof e,p===f)return h(e);if(p===l)return isFinite(e)?e+"":v;if(p===L||p===v)return e+"";if(p===n){if(!e)return v;if(a+=c,i=[],"[object Array]"===k.apply(e)){for(q=e.length,m=0;q>m;m++)i[m]=d(m,e)||v;return g=0===i.length?"[]":a?"[\n"+a+i.join(",\n"+a)+"\n"+r+"]":"["+i.join(",")+"]",a=r,g}if(b&&typeof b===n)for(q=b.length,m=0;q>m;m++)typeof b[m]===f&&(o=b[m],g=d(o,e),g&&i.push(h(o)+(a?": ":":")+g));else for(o in e)Object.hasOwnProperty.call(e,o)&&(g=d(o,e),g&&i.push(h(o)+(a?": ":":")+g));return g=0===i.length?"{}":a?"{\n"+a+i.join(",\n"+a)+"\n"+r+"}":"{"+i.join(",")+"}",a=r,g}}var a,c,b,e=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,i={"\b":"\\b","   ":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},k={}.toString;typeof Date.prototype.toJSON!==j&&(Date.prototype.toJSON=function(){var a=this;return isFinite(a.valueOf())?g(a.getUTCFullYear(),4)+"-"+g(a.getUTCMonth()+1)+"-"+g(a.getUTCDate())+"T"+g(a.getUTCHours())+":"+g(a.getUTCMinutes())+":"+g(a.getUTCSeconds())+"Z":null},String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()}),typeof u.stringify!==j&&(u.stringify=function(i,e,g){var h;if(a="",c="",typeof g===l)for(h=0;g>h;h+=1)c+=" ";else typeof g===f&&(c=g);if(b=e,e&&typeof e!==j&&(typeof e!==n||typeof e.length!==l))throw Error("JSON.stringify");return d("",{"":i})})}(),function(){function p(b){if(b){if(b.numberFormat)return b;if(typeof b===f){var c=a.cultures;return c[b]||c[b.split("-")[0]]||null}return null}return null}function q(b){return b&&(b=p(b)),b||a.cultures.current}function x(a){a.groupSizes=a.groupSize,a.percent.groupSizes=a.percent.groupSize,a.currency.groupSizes=a.currency.groupSize}function t(a,c,e){e=q(e);var b=e.calendars.standard,f=b.days,h=b.months;return c=b.patterns[c]||c,c.replace(u,function(e){var c;return"d"===e?(c=a.getDate()):"dd"===e?(c=g(a.getDate())):"ddd"===e?(c=f.namesAbbr[a.getDay()]):"dddd"===e?(c=f.names[a.getDay()]):"M"===e?(c=a.getMonth()+1):"MM"===e?(c=g(a.getMonth()+1)):"MMM"===e?(c=h.namesAbbr[a.getMonth()]):"MMMM"===e?(c=h.names[a.getMonth()]):"yy"===e?(c=g(a.getFullYear()%100)):"yyyy"===e?(c=g(a.getFullYear(),4)):"h"===e?(c=a.getHours()%12||12):"hh"===e?(c=g(a.getHours()%12||12)):"H"===e?(c=a.getHours()):"HH"===e?(c=g(a.getHours())):"m"===e?(c=a.getMinutes()):"mm"===e?(c=g(a.getMinutes())):"s"===e?(c=a.getSeconds()):"ss"===e?(c=g(a.getSeconds())):"f"===e?(c=r.floor(a.getMilliseconds()/100)):"ff"===e?(c=a.getMilliseconds(),c>99&&(c=r.floor(c/10)),c=g(c)):"fff"===e?(c=g(a.getMilliseconds(),3)):"tt"===e&&(c=a.getHours()<12?b.AM[0]:b.PM[0]),c!==d?c:e.slice(1,e.length-1)})}function A(c,a,L){L=q(L);var I,y,D,K,O,p,o,u,W,f,x,t,P,V,r,m,l,H,Q,U,S,T,E,g=L.numberFormat,C=g.groupSize[0],G=g[k],J=g[e],F=g.decimals,N=g.pattern[0],M=[],A=0>c,R=b,n=b,B=-1;if(c===d)return b;if(!isFinite(c))return c;if(!a)return L.name.length?c.toLocaleString():""+c;if(O=v.exec(a)){if(a=O[1].toLowerCase(),y="c"===a,D="p"===a,(y||D)&&(g=y?g.currency:g.percent,C=g.groupSize[0],G=g[k],J=g[e],F=g.decimals,I=g.symbol,N=g.pattern[A?0:1]),K=O[2],K&&(F=+K),"e"===a)return K?c.toExponential(F):c.toExponential();if(D&&(c*=100),c=h(c,F),A=0>c,c=c.split(e),p=c[0],o=c[1],A&&(p=p.substring(1)),n=p,u=p.length,u>=C)for(n=b,f=0;u>f;f++)f>0&&(u-f)%C===0&&(n+=G),n+=p.charAt(f);if(o&&(n+=J+o),"n"===a&&!A)return n;for(c=b,f=0,x=N.length;x>f;f++)t=N.charAt(f),c+="n"===t?n:"$"===t||"%"===t?I:t;return c}if(A&&(c=-c),(a.indexOf("'")>-1||a.indexOf('"')>-1||a.indexOf("\\")>-1)&&(a=a.replace(w,function(a){var c=a.charAt(0).replace("\\",""),b=a.slice(1).replace(c,"");return M.push(b),s})),a=a.split(";"),A&&a[1])a=a[1],V=!0;else if(0===c){if(a=a[2]||a[0],-1==a.indexOf(j)&&-1==a.indexOf(i))return a}else a=a[0];if(U=a.indexOf("%"),S=a.indexOf("$"),D=-1!=U,y=-1!=S,D&&(c*=100),y&&"\\"===a[S-1]&&(a=a.split("\\").join(""),y=!1),(y||D)&&(g=y?g.currency:g.percent,C=g.groupSize[0],G=g[k],J=g[e],F=g.decimals,I=g.symbol),P=a.indexOf(k)>-1,P&&(a=a.replace(z,b)),r=a.indexOf(e),x=a.length,-1!=r?(o=(""+c).split("e"),o=o[1]?h(c,Math.abs(o[1])):o[0],o=o.split(e)[1]||b,l=a.lastIndexOf(i)-r,m=a.lastIndexOf(j)-r,H=l>-1,Q=m>-1,f=o.length,H||Q||(a=a.substring(0,r)+a.substring(r+1),x=a.length,r=-1,f=0),H&&l>m?(f=l):m>l&&(Q&&f>m?(f=m):H&&l>f&&(f=l)),f>-1&&(c=h(c,f))):(c=h(c)),m=a.indexOf(j),T=l=a.indexOf(i),B=-1==m&&-1!=l?l:-1!=m&&-1==l?m:m>l?l:m,m=a.lastIndexOf(j),l=a.lastIndexOf(i),E=-1==m&&-1!=l?l:-1!=m&&-1==l?m:m>l?m:l,B==x&&(E=B),-1!=B){if(n=(""+c).split(e),p=n[0],o=n[1]||b,u=p.length,W=o.length,A&&-1*c>=0&&(A=!1),P)if(u===C&&r-T>u)p=G+p;else if(u>C){for(n=b,f=0;u>f;f++)f>0&&(u-f)%C===0&&(n+=G),n+=p.charAt(f);p=n}for(c=a.substring(0,B),A&&!V&&(c+="-"),f=B;x>f;f++){if(t=a.charAt(f),-1==r){if(u>E-f){c+=p;break}}else if(-1!=l&&f>l&&(R=b),u>=r-f&&r-f>-1&&(c+=p,f=r),r===f){c+=(o?J:b)+o,f+=E-r+1;continue}t===i?(c+=t,R=t):t===j&&(c+=R)}if(E>=B&&(c+=a.substring(E+1)),y||D){for(n=b,f=0,x=c.length;x>f;f++)t=c.charAt(f),n+="$"===t||"%"===t?I:t;c=n}if(x=M.length)for(f=0;x>f;f++)c=c.replace(s,M[f])}return c}var h,m,u=/dddd|ddd|dd|d|MMMM|MMM|MM|M|yyyy|yy|HH|H|hh|h|mm|m|fff|ff|f|tt|ss|s|"[^"]*"|'[^']*'/g,v=/^(n|c|p|e)(\d*)$/i,w=/(\\.)|(['][^']*[']?)|(["][^"]*["]?)/g,z=/\,/g,b="",e=".",k=",",j="#",i="0",s="??",n="en-US",y={}.toString;a.cultures["en-US"]={name:n,numberFormat:{pattern:["-n"],decimals:2,",":",",".":".",groupSize:[3],percent:{pattern:["-n %","n %"],decimals:2,",":",",".":".",groupSize:[3],symbol:"%"},currency:{pattern:["($n)","$n"],decimals:2,",":",",".":".",groupSize:[3],symbol:"$"}},calendars:{standard:{days:{names:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],namesAbbr:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],namesShort:["Su","Mo","Tu","We","Th","Fr","Sa"]},months:{names:["January","February","March","April","May","June","July","August","September","October","November","December"],namesAbbr:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]},AM:["AM","am","AM"],PM:["PM","pm","PM"],patterns:{d:"M/d/yyyy",D:"dddd, MMMM dd, yyyy",F:"dddd, MMMM dd, yyyy h:mm:ss tt",g:"M/d/yyyy h:mm tt",G:"M/d/yyyy h:mm:ss tt",m:"MMMM dd",M:"MMMM dd",s:"yyyy'-'MM'-'ddTHH':'mm':'ss",t:"h:mm tt",T:"h:mm:ss tt",u:"yyyy'-'MM'-'dd HH':'mm':'ss'Z'",y:"MMMM, yyyy",Y:"MMMM, yyyy"},"/":"/",":":":",firstDay:0,twoDigitYearMax:2029}}},a.culture=function(e){var b,c=a.cultures;return e===d?c.current:(b=p(e)||c[n],b.calendar=b.calendars.standard,c.current=b,o&&x(b.numberFormat),d)},a.findCulture=p,a.getCulture=q,a.culture(n),h=function(a,b){return b=b||0,a=(""+a).split("e"),a=Math.round(+(a[0]+"e"+(a[1]?+a[1]+b:b))),a=(""+a).split("e"),a=+(a[0]+"e"+(a[1]?+a[1]-b:-b)),a.toFixed(b)},m=function(a,b,c){if(b){if("[object Date]"===y.call(a))return t(a,b,c);if(typeof a===l)return A(a,b,c)}return a!==d?a:""},o&&(m=function(d,b,a){return c.isPlainObject(a)&&(a=a.name),o.format(d,b,a)}),a.format=function(a){var b=arguments;return a.replace(wb,function(e,c,a){var d=b[parseInt(c,10)+1];return m(d,a?a.substring(1):"")})},a._extractFormat=function(a){return"{0:"===a.slice(0,3)&&(a=a.slice(3,a.length-1)),a},a._activeElement=function(){try{return document.activeElement}catch(a){return document.documentElement.activeElement}},a._round=h,a.toString=m}(),function(){function b(a,c,b){return!(a>=c&&b>=a)}function t(a){return a.charAt(0)}function h(a){return c.map(a,t)}function q(a,b){b||23!==a.getHours()||a.setHours(a.getHours()+2)}function k(b){for(var a=0,d=b.length,c=[];d>a;a++)c[a]=(b[a]+"").toLowerCase();return c}function i(b){var a,c={};for(a in b)c[a]=k(b[a]);return c}function u(j,o,Q){if(!j)return null;var g,a,N,K,J,I,P,t,H,D,G,A,O,B,l=function(b){for(var a=0;o[w]===b;)a++,w++;return a>0&&(w-=1),a},v=function(b){var c=e[b]||RegExp("^\\d{1,"+b+"}"),a=j.substr(r,b).match(c);return a?(a=a[0],r+=a.length,parseInt(a,10)):null},E=function(e,f){for(var d,c,b,a=0,g=e.length;g>a;a++)if(d=e[a],c=d.length,b=j.substr(r,c),f&&(b=b.toLowerCase()),b==d)return r+=c,a+1;return null},C=function(){var a=!1;return j.charAt(r)===o[w]&&(r++,a=!0),a},k=Q.calendars.standard,f=null,u=null,m=null,c=null,x=null,y=null,s=null,w=0,r=0,L=!1,M=new Date,F=k.twoDigitYearMax||2029,z=M.getFullYear();for(o||(o="d"),K=k.patterns[o],K&&(o=K),o=o.split(""),N=o.length;N>w;w++)if(g=o[w],L)"'"===g?(L=!1):C();else if("d"===g){if(a=l("d"),k._lowerDays||(k._lowerDays=i(k.days)),m=3>a?v(2):E(k._lowerDays[3==a?"namesAbbr":"names"],!0),null===m||b(m,1,31))return null}else if("M"===g){if(a=l("M"),k._lowerMonths||(k._lowerMonths=i(k.months)),u=3>a?v(2):E(k._lowerMonths[3==a?"namesAbbr":"names"],!0),null===u||b(u,1,12))return null;u-=1}else if("y"===g){if(a=l("y"),f=v(a),null===f)return null;2==a&&("string"==typeof F&&(F=z+parseInt(F,10)),f=z-z%100+f,f>F&&(f-=100))}else if("h"===g){if(l("h"),c=v(2),12==c&&(c=0),null===c||b(c,0,11))return null}else if("H"===g){if(l("H"),c=v(2),null===c||b(c,0,23))return null}else if("m"===g){if(l("m"),x=v(2),null===x||b(x,0,59))return null}else if("s"===g){if(l("s"),y=v(2),null===y||b(y,0,59))return null}else if("f"===g){if(a=l("f"),B=j.substr(r,a).match(e[3]),s=v(a),null!==s&&(B=B[0].length,3>B&&(s*=Math.pow(10,3-B)),a>3&&(s=parseInt((""+s).substring(0,3),10))),null===s||b(s,0,999))return null}else if("t"===g){if(a=l("t"),H=k.AM,D=k.PM,1===a&&(H=h(H),D=h(D)),J=E(D),!J&&!E(H))return null}else if("z"===g){if(I=!0,a=l("z"),"Z"===j.substr(r,1)){if(!P)return null;C();continue}if(t=j.substr(r,6).match(a>2?p:n),!t)return null;if(t=t[0],r=t.length,t=t.split(":"),G=parseInt(t[0],10),b(G,-12,13))return null;if(a>2&&(A=parseInt(t[1],10),isNaN(A)||b(A,0,59)))return null}else if("T"===g)P=C();else if("'"===g)L=!0,C();else if(!C())return null;return O=null!==c||null!==x||y||null,null===f&&null===u&&null===m&&O?(f=z,u=M.getMonth(),m=M.getDate()):(null===f&&(f=z),null===m&&(m=1)),J&&12>c&&(c+=12),I?(G&&(c+=-G),A&&(x+=-A),j=new Date(Date.UTC(f,u,m,c,x,y,s))):(j=new Date(f,u,m,c,x,y,s),q(j,c)),100>f&&j.setFullYear(f),j.getDate()!==m&&I===d?null:j}var g=/\u00A0/g,m=/[eE][\-+]?[0-9]+/,n=/[+|\-]\d{1,2}/,p=/[+|\-]\d{1,2}:\d{2}/,s=/^\/Date\((.*?)\)\/$/,r=/[+-]/,f=["G","g","d","F","D","y","m","T","t"],e={2:/^\d{1,2}/,3:/^\d{1,3}/,4:/^\d{4}/},j={}.toString;a.parseDate=function(e,c,i){var d,b,h,k,g;if("[object Date]"===j.call(e))return e;if(d=0,b=null,e&&0===e.indexOf("/D")&&(b=s.exec(e)))return g=b=b[1],b=parseInt(b,10),g=g.substring(1).split(r)[1],g&&(b-=parseInt(g,10)*a.date.MS_PER_MINUTE),new Date(b);if(i=a.getCulture(i),!c){for(c=[],k=i.calendar.patterns,h=f.length;h>d;d++)c[d]=k[f[d]];d=0,c=["yyyy/MM/dd HH:mm:ss","yyyy/MM/dd HH:mm","yyyy/MM/dd","ddd MMM dd yyyy HH:mm:ss","yyyy-MM-ddTHH:mm:ss.fffffffzzz","yyyy-MM-ddTHH:mm:ss.fffzzz","yyyy-MM-ddTHH:mm:sszzz","yyyy-MM-ddTHH:mmzzz","yyyy-MM-ddTHH:mmzz","yyyy-MM-ddTHH:mm:ss","yyyy-MM-ddTHH:mm","yyyy-MM-dd HH:mm:ss","yyyy-MM-dd HH:mm","yyyy-MM-dd","HH:mm:ss","HH:mm"].concat(c)}for(c=vb(c)?c:[c],h=c.length;h>d;d++)if(b=u(e,c[d],i))return b;return b},a.parseInt=function(c,d){var b=a.parseFloat(c,d);return b&&(b=0|b),b},a.parseFloat=function(b,h,k){if(!b&&0!==b)return null;if(typeof b===l)return b;b=""+b,h=a.getCulture(h);var e,n,c=h.numberFormat,i=c.percent,o=c.currency,f=o.symbol,j=i.symbol,d=b.indexOf("-");return m.test(b)?(b=parseFloat(b.replace(c["."],".")),isNaN(b)&&(b=null),b):d>0?null:(d=d>-1,b.indexOf(f)>-1||k&&k.toLowerCase().indexOf("c")>-1?(c=o,e=c.pattern[0].replace("$",f).split("n"),b.indexOf(e[0])>-1&&b.indexOf(e[1])>-1&&(b=b.replace(e[0],"").replace(e[1],""),d=!0)):b.indexOf(j)>-1&&(n=!0,c=i,f=j),b=b.replace("-","").replace(f,"").replace(g," ").split(c[","].replace(g," ")).join("").replace(c["."],"."),b=parseFloat(b),isNaN(b)?(b=null):d&&(b*=-1),b&&n&&(b/=100),b)},o&&(a.parseDate=function(a,c,b){return"[object Date]"===j.call(a)?a:o.parseDate(a,c,b)},a.parseFloat=function(a,b){return typeof a===l?a:a===d||null===a?null:(c.isPlainObject(b)&&(b=b.name),a=o.parseFloat(a,b),isNaN(a)?null:a)})}(),function(){var i,h,j,g,e,a,k;b.scrollbar=function(){var b,a=document.createElement("div");return a.style.cssText="overflow:scroll;overflow-x:hidden;zoom:1;clear:both",a.innerHTML="&nbsp;",document.body.appendChild(a),b=a.offsetWidth-a.scrollWidth,document.body.removeChild(a),b},b.isRtl=function(a){return c(a).closest(".k-rtl").length>0},i=document.createElement("table");try{i.innerHTML="<tr><td></td></tr>",b.tbodyInnerHtml=!0}catch(l){b.tbodyInnerHtml=!1}b.touch="ontouchstart"in window,b.msPointers=window.MSPointerEvent,b.pointers=window.PointerEvent,h=b.transitions=!1,j=b.transforms=!1,g="HTMLElement"in window?HTMLElement.prototype:[],b.hasHW3D="WebKitCSSMatrix"in window&&"m11"in new window.WebKitCSSMatrix||"MozPerspective"in document.documentElement.style||"msPerspective"in document.documentElement.style,w(["Moz","webkit","O","ms"],function(){var a,b=""+this,c=typeof i.style[b+"Transition"]===f;return c||typeof i.style[b+"Transform"]===f?(a=b.toLowerCase(),j={css:"ms"!=a?"-"+a+"-":"",prefix:b,event:"o"===a||"webkit"===a?a:""},c&&(h=j,h.event=h.event?h.event+"TransitionEnd":"transitionend"),!1):d}),i=null,b.transforms=j,b.transitions=h,b.devicePixelRatio=window.devicePixelRatio===d?1:window.devicePixelRatio;try{b.screenWidth=window.outerWidth||window.screen?window.screen.availWidth:window.innerWidth,b.screenHeight=window.outerHeight||window.screen?window.screen.availHeight:window.innerHeight}catch(l){b.screenWidth=window.screen.availWidth,b.screenHeight=window.screen.availHeight}b.detectOS=function(f){var d,c,a=!1,g=[],i=!/mobile safari/i.test(f),e={fire:/(Silk)\/(\d+)\.(\d+(\.\d+)?)/,android:/(Android|Android.*(?:Opera|Firefox).*?\/)\s*(\d+)\.(\d+(\.\d+)?)/,iphone:/(iPhone|iPod).*OS\s+(\d+)[\._]([\d\._]+)/,ipad:/(iPad).*OS\s+(\d+)[\._]([\d_]+)/,meego:/(MeeGo).+NokiaBrowser\/(\d+)\.([\d\._]+)/,webos:/(webOS)\/(\d+)\.(\d+(\.\d+)?)/,blackberry:/(BlackBerry|BB10).*?Version\/(\d+)\.(\d+(\.\d+)?)/,playbook:/(PlayBook).*?Tablet\s*OS\s*(\d+)\.(\d+(\.\d+)?)/,wp:/(Windows Phone(?: OS)?)\s(\d+)\.(\d+(\.\d+)?)/,windows:/(MSIE)\s+(\d+)\.(\d+(\.\d+)?)/,tizen:/(tizen).*?Version\/(\d+)\.(\d+(\.\d+)?)/i,sailfish:/(sailfish).*rv:(\d+)\.(\d+(\.\d+)?).*firefox/i,ffos:/(Mobile).*rv:(\d+)\.(\d+(\.\d+)?).*Firefox/},j={ios:/^i(phone|pad|pod)$/i,android:/^android|fire$/i,blackberry:/^blackberry|playbook/i,windows:/windows/,wp:/wp/,flat:/sailfish|ffos|tizen/i,meego:/meego/},k={tablet:/playbook|ipad|fire/i},h={omini:/Opera\sMini/i,omobile:/Opera\sMobi/i,firefox:/Firefox|Fennec/i,mobilesafari:/version\/.*safari/i,chrome:/chrome|crios/i,webkit:/webkit/i,ie:/MSIE|Windows\sPhone/i};for(c in e)if(e.hasOwnProperty(c)&&(g=f.match(e[c]))){if("windows"==c&&"plugins"in navigator)return!1;a={},a.device=c,a.tablet=I(c,k,!1),a.browser=I(f,h,"default"),a.name=I(c,j),a[a.name]=!0,a.majorVersion=g[2],a.minorVersion=g[3].replace("_","."),d=a.minorVersion.replace(".","").substr(0,2),a.flatVersion=a.majorVersion+d+Array(3-(3>d.length?d.length:2)).join("0"),a.cordova=typeof window.PhoneGap!==x||typeof window.cordova!==x,a.appMode=window.navigator.standalone||/file|local|wmapp/.test(window.location.protocol)||a.cordova,a.android&&(1.5>b.devicePixelRatio&&400>a.flatVersion||i)&&(b.screenWidth>800||b.screenHeight>800)&&(a.tablet=c);break}return a},e=b.mobileOS=b.detectOS(navigator.userAgent),b.wpDevicePixelRatio=e.wp?screen.width/320:0,b.kineticScrollNeeded=e&&(b.touch||b.msPointers||b.pointers),b.hasNativeScrolling=!1,(e.ios||e.android&&e.majorVersion>2||e.wp)&&(b.hasNativeScrolling=e),b.mouseAndTouchPresent=b.touch&&!(b.mobileOS.ios||b.mobileOS.android),b.detectBrowser=function(e){var b,a=!1,d=[],c={webkit:/(chrome)[ \/]([\w.]+)/i,safari:/(webkit)[ \/]([\w.]+)/i,opera:/(opera)(?:.*version|)[ \/]([\w.]+)/i,msie:/(msie\s|trident.*? rv:)([\w.]+)/i,mozilla:/(mozilla)(?:.*? rv:([\w.]+)|)/i};for(b in c)if(c.hasOwnProperty(b)&&(d=e.match(c[b]))){a={},a[b]=!0,a[d[1].toLowerCase()]=!0,a.version=parseInt(document.documentMode||d[2],10);break}return a},b.browser=b.detectBrowser(navigator.userAgent),b.zoomLevel=function(){try{return b.touch?document.documentElement.clientWidth/window.innerWidth:b.browser.msie&&b.browser.version>=10?(top||window).document.documentElement.offsetWidth/(top||window).innerWidth:1}catch(a){return 1}},b.cssBorderSpacing=d!==document.documentElement.style.borderSpacing&&!(b.browser.msie&&8>b.browser.version),function(d){var a="",f=c(document.documentElement),e=parseInt(d.version,10);d.msie?(a="ie"):d.mozilla?(a="ff"):d.safari?(a="safari"):d.webkit?(a="webkit"):d.opera&&(a="opera"),a&&(a="k-"+a+" k-"+a+e),b.mobileOS&&(a+=" k-mobile"),f.addClass(a)}(b.browser),b.eventCapture=document.documentElement.addEventListener,a=document.createElement("input"),b.placeholder="placeholder"in a,b.propertyChangeEvent="onpropertychange"in a,b.input=function(){for(var b,f=["number","date","time","month","week","datetime","datetime-local"],g=f.length,e="test",d={},c=0;g>c;c++)b=f[c],a.setAttribute("type",b),a.value=e,d[b.replace("-","")]="text"!==a.type&&a.value!==e;return d}(),a.style.cssText="float:left;",b.cssFloat=!!a.style.cssFloat,a=null,b.stableSort=function(){for(var c=513,b=[{index:0,field:"b"}],a=1;c>a;a++)b.push({index:a,field:"a"});return b.sort(function(a,b){return a.field>b.field?1:b.field>a.field?-1:0}),1===b[0].index}(),b.matchesSelector=g.webkitMatchesSelector||g.mozMatchesSelector||g.msMatchesSelector||g.oMatchesSelector||g.matchesSelector||function(d){for(var a=document.querySelectorAll?(this.parentNode||document).querySelectorAll(d)||[]:c(d),b=a.length;b--;)if(a[b]==this)return!0;return!1},b.pushState=window.history&&window.history.pushState,k=document.documentMode,b.hashChange="onhashchange"in window&&!(b.browser.msie&&(!k||8>=k))}(),pb={left:{reverse:"right"},right:{reverse:"left"},down:{reverse:"up"},up:{reverse:"down"},top:{reverse:"bottom"},bottom:{reverse:"top"},"in":{reverse:"out"},out:{reverse:"in"}},p={},c.extend(p,{enabled:!0,Element:function(a){this.element=c(a)},promise:function(a,b){a.is(":visible")||a.css({display:a.data("olddisplay")||"block"}).css("display"),b.hide&&a.data("olddisplay",a.css("display")).hide(),b.init&&b.init(),b.completeCallback&&b.completeCallback(a),a.dequeue()},disable:function(){this.enabled=!1,this.promise=this.promiseShim},enable:function(){this.enabled=!0,this.promise=this.animatedPromise}}),p.promiseShim=p.promise,"kendoAnimate"in c.fn||i(c.fn,{kendoStop:function(a,b){return this.stop(a,b)},kendoAnimate:function(a,d,b,c){return ob(this,a,d,b,c)},kendoAddClass:function(b,c){return a.toggleClass(this,b,c,!0)},kendoRemoveClass:function(b,c){return a.toggleClass(this,b,c,!1)},kendoToggleClass:function(b,d,c){return a.toggleClass(this,b,d,c)}}),eb=/&/g,W=/</g,U=/"/g,db=/'/g,rb=/>/g,K=function(a){return a.target},b.touch&&(K=function(a){var b="originalEvent"in a?a.originalEvent.changedTouches:"changedTouches"in a?a.changedTouches:null;return b?document.elementFromPoint(b[0].clientX,b[0].clientY):a.target},w(["swipe","swipeLeft","swipeRight","swipeUp","swipeDown","doubleTap","tap"],function(b,a){c.fn[a]=function(b){return this.bind(a,b)}})),b.touch?b.mobileOS?(b.mousedown="touchstart",b.mouseup="touchend",b.mousemove="touchmove",b.mousecancel="touchcancel",b.click="touchend",b.resize="orientationchange"):(b.mousedown="mousedown touchstart",b.mouseup="mouseup touchend",b.mousemove="mousemove touchmove",b.mousecancel="mouseleave touchcancel",b.click="click",b.resize="resize"):b.pointers?(b.mousemove="pointermove",b.mousedown="pointerdown",b.mouseup="pointerup",b.mousecancel="pointercancel",b.click="pointerup",b.resize="orientationchange resize"):b.msPointers?(b.mousemove="MSPointerMove",b.mousedown="MSPointerDown",b.mouseup="MSPointerUp",b.mousecancel="MSPointerCancel",b.click="MSPointerUp",b.resize="orientationchange resize"):(b.mousemove="mousemove",b.mousedown="mousedown",b.mouseup="mouseup",b.mousecancel="mouseleave",b.click="click",b.resize="resize"),ib=function(g,h){var b,c,d,a,f=h||"d",e=1;for(c=0,d=g.length;d>c;c++)a=g[c],""!==a&&(b=a.indexOf("["),0!==b&&(-1==b?(a="."+a):(e++,a="."+a.substring(0,b)+" || {})"+a.substring(b))),e++,f+=a+(d-1>c?" || {})":")"));return Array(e).join("(")+f},ab=/^([a-z]+:)?\/\//i,i(a,{ui:a.ui||{},fx:a.fx||Db,effects:a.effects||p,mobile:a.mobile||{},data:a.data||{},dataviz:a.dataviz||{ui:{roles:{}}},keys:{INSERT:45,DELETE:46,BACKSPACE:8,TAB:9,ENTER:13,ESC:27,LEFT:37,UP:38,RIGHT:39,DOWN:40,END:35,HOME:36,SPACEBAR:32,PAGEUP:33,PAGEDOWN:34,F2:113,F10:121,F12:123,NUMPAD_PLUS:107,NUMPAD_MINUS:109,NUMPAD_DOT:110},support:a.support||b,animate:a.animate||ob,ns:"",attr:function(b){return"data-"+a.ns+b},getShadows:bb,wrap:yb,deepExtend:lb,getComputedStyles:Cb,size:Hb,toCamelCase:X,toHyphens:Ab,getOffset:a.getOffset||Fb,parseEffects:a.parseEffects||Eb,toggleClass:a.toggleClass||Kb,directions:a.directions||pb,Observable:B,Class:C,Template:q,template:G(q.compile,q),render:G(q.render,q),stringify:G(u.stringify,u),eventTarget:K,htmlEncode:zb,isLocalUrl:function(a){return a&&!ab.test(a)},expr:function(a,c,b){return a=a||"",typeof c==f&&(b=c,c=!1),b=b||"d",a&&"["!==a.charAt(0)&&(a="."+a),a=c?ib(a.split("."),b):b+a},getter:function(b,c){return P[b]=P[b]||Function("d","return "+a.expr(b,c))},setter:function(b){return S[b]=S[b]||Function("d,value",a.expr(b)+"=value")},accessor:function(b){return{"get":a.getter(b),"set":a.setter(b)}},guid:function(){for(var c,b="",a=0;32>a;a++)c=16*r.random()|0,(8==a||12==a||16==a||20==a)&&(b+="-"),b+=(12==a?4:16==a?3&c|8:c).toString(16);return b},roleSelector:function(b){return b.replace(/(\S+)/g,"["+a.attr("role")+"=$1],").slice(0,-1)},triggeredByInput:function(a){return/^(label|input|textarea|select)$/i.test(a.target.tagName)},logToConsole:function(b){var a=window.console;d!==a&&a.log&&a.log(b)}}),t=B.extend({init:function(d,c){var b=this;b.element=a.jQuery(d).handler(b),B.fn.init.call(b),c=b.options=i(!0,{},b.options,c),b.element.attr(a.attr("role"))||b.element.attr(a.attr("role"),(c.name||"").toLowerCase()),b.element.data("kendo"+c.prefix+c.name,b),b.bind(b.events,c)},events:[],options:{prefix:""},_hasBindingTarget:function(){return!!this.element[0].kendoBindingTarget},_tabindex:function(a){a=a||this.wrapper;var d=this.element,b="tabindex",c=a.attr(b)||d.attr(b);d.removeAttr(b),a.attr(b,isNaN(c)?0:c)},setOptions:function(a){this._setEvents(a),c.extend(this.options,a)},_setEvents:function(d){for(var b,a=this,c=0,e=a.events.length;e>c;c++)b=a.events[c],a.options[b]&&d[b]&&a.unbind(b,a.options[b]);a.bind(a.events,d)},resize:function(c){var a=this.getSize(),b=this._size;(c||!b||a.width!==b.width||a.height!==b.height)&&(this._resize(a),this.trigger("resize",a),this._size=a)},getSize:function(){return a.dimensions(this.element)},size:function(a){return a?(this.setSize(a),d):this.getSize()},setSize:c.noop,_resize:c.noop,destroy:function(){var a=this;a.element.removeData("kendo"+a.options.prefix+a.options.name),a.element.removeData("handler"),a.unbind()}}),a.dimensions=function(a,c){var b=a[0];return c&&a.css(c),{width:b.offsetWidth,height:b.offsetHeight}},a.notify=A,sb=/template$/i,fb=/^\s*(?:\{(?:.|\r\n|\n)*\}|\[(?:.|\r\n|\n)*\])\s*$/,qb=/^\{(\d+)(:[^\}]+)?\}|^\[[A-Za-z_]*\]$/,O=/([A-Z])/g,a.initWidget=function(b,h,g){var k,m,e,l,o,i,n,j;if(g?g.roles&&(g=g.roles):(g=a.ui.roles),b=b.nodeType?b:b[0],i=b.getAttribute("data-"+a.ns+"role"),i&&(e=-1===i.indexOf(".")?g[i]:a.getter(i)(window))){for(j=J(b,"dataSource"),h=c.extend({},kb(b,e.fn.options),h),j&&(h.dataSource=typeof j===f?a.getter(j)(window):j),l=0,o=e.fn.events.length;o>l;l++)m=e.fn.events[l],n=J(b,m),n!==d&&(h[m]=a.getter(n)(window));return k=c(b).data("kendo"+e.fn.options.prefix+e.fn.options.name),k?k.setOptions(h):(k=new e(b,h)),k}},a.rolesFromNamespaces=function(c){var b,d,e=[];for(c[0]||(c=[a.ui,a.dataviz.ui]),b=0,d=c.length;d>b;b++)e[b]=c[b].roles;return i.apply(null,[{}].concat(e.reverse()))},a.init=function(d){var b=a.rolesFromNamespaces(H.call(arguments,1));c(d).find("[data-"+a.ns+"role]").addBack().each(function(){a.initWidget(this,{},b)})},a.destroy=function(b){c(b).find("[data-"+a.ns+"role]").addBack().each(function(){var b=a.widgetInstance(c(this));b&&b.destroy()})},a.resize=function(e){var b,d=c(e).find("[data-"+a.ns+"role]").addBack().filter(Lb);d.length&&(b=c.makeArray(d),b.sort(Jb),c.each(b,function(){var b=a.widgetInstance(c(this));b&&b.resize()}))},a.parseOptions=kb,i(a.ui,{Widget:t,roles:{},progress:function(b,k){var f,h,g,e,d=b.find(".k-loading-mask"),i=a.support,j=i.browser;k?d.length||(f=i.isRtl(b),h=f?"right":"left",e=b.scrollLeft(),g=j.webkit?f?b[0].scrollWidth-b.width()-2*e:0:0,d=c("<div class='k-loading-mask'><span class='k-loading-text'>Loading...</span><div class='k-loading-image'/><div class='k-loading-color'/></div>").width("100%").height("100%").css("top",b.scrollTop()).css(h,Math.abs(e)+g).prependTo(b)):d&&d.remove()},plugin:function(e,h,g){var i,b=e.fn.options.name;h=h||a.ui,g=g||"",h[b]=e,h.roles[b.toLowerCase()]=e,i="getKendo"+g+b,b="kendo"+g+b,c.fn[b]=function(g){var i,h=this;return typeof g===f?(i=H.call(arguments,1),this.each(function(){var e,f,k=c.data(this,b);if(!k)throw Error(a.format("Cannot call method '{0}' of {1} before it is initialized",g,b));if(e=k[g],typeof e!==j)throw Error(a.format("Cannot find method '{0}' of {1}",g,b));return f=e.apply(k,i),f!==d?(h=f,!1):d})):this.each(function(){new e(this,g)}),h},c.fn[i]=function(){return this.data(b)}}}),E={bind:function(){return this},nullObject:!0},Q=t.extend({init:function(a,b){t.fn.init.call(this,a,b),this.element.autoApplyNS(),this.wrapper=this.element,this.element.addClass("km-widget")},destroy:function(){t.fn.destroy.call(this),this.element.kendoDestroy()},options:{prefix:"Mobile"},events:[],view:function(){var b=this.element.closest(a.roleSelector("view splitview modalview drawer"));return a.widgetInstance(b,a.mobile.ui)||E},viewHasNativeScrolling:function(){var a=this.view();return a&&a.options.useNativeScrolling},container:function(){var b=this.element.closest(a.roleSelector("view layout modalview drawer"));return a.widgetInstance(b,a.mobile.ui)||E}}),i(a.mobile,{init:function(b){a.init(b,a.mobile.ui,a.ui,a.dataviz.ui)},appLevelNativeScrolling:function(){return a.mobile.application&&a.mobile.application.options&&a.mobile.application.options.useNativeScrolling},ui:{Widget:Q,roles:{},plugin:function(b){a.ui.plugin(b,a.mobile.ui,"Mobile")}}}),a.touchScroller=function(e,d){return c(e).map(function(f,e){return e=c(e),b.kineticScrollNeeded&&a.mobile.ui.Scroller&&!e.data("kendoMobileScroller")?(e.kendoMobileScroller(d),e.data("kendoMobileScroller")):!1})[0]},a.preventDefault=function(a){a.preventDefault()},a.widgetInstance=function(h,e){var c,g,f,i,b=h.data(a.ns+"role"),d=[];if(b){if("content"===b&&(b="scroller"),e)if(e[0])for(c=0,g=e.length;g>c;c++)d.push(e[c].roles[b]);else d.push(e.roles[b]);else d=[a.ui.roles[b],a.dataviz.ui.roles[b],a.mobile.ui.roles[b]];for(b.indexOf(".")>=0&&(d=[a.getter(b)(window)]),c=0,g=d.length;g>c;c++)if(f=d[c],f&&(i=h.data("kendo"+f.fn.options.prefix+f.fn.options.name)))return i}},a.onResize=function(d){var a=d;return b.mobileOS.android&&(a=function(){setTimeout(d,600)}),c(window).on(b.resize,a),a},a.unbindResize=function(a){c(window).off(b.resize,a)},a.attrValue=function(b,c){return b.data(a.ns+c)},a.days={Sunday:0,Monday:1,Tuesday:2,Wednesday:3,Thursday:4,Friday:5,Saturday:6},c.extend(c.expr[":"],{kendoFocusable:function(b){var a=c.attr(b,"tabindex");return Bb(b,!isNaN(a)&&a>-1)}}),D=["mousedown","mousemove","mouseenter","mouseleave","mouseover","mouseout","mouseup","click"],R="label, input, [data-rel=external]",e={setupMouseMute:function(){var d,a=0,g=D.length,f=document.documentElement;if(!e.mouseTrap&&b.eventCapture)for(e.mouseTrap=!0,e.bustClick=!1,e.captureMouse=!1,d=function(a){e.captureMouse&&("click"===a.type?e.bustClick&&!c(a.target).is(R)&&(a.preventDefault(),a.stopPropagation()):a.stopPropagation())};g>a;a++)f.addEventListener(D[a],d,!0)},muteMouse:function(a){e.captureMouse=!0,a.data.bustClick&&(e.bustClick=!0),clearTimeout(e.mouseTrapTimeoutID)},unMuteMouse:function(){clearTimeout(e.mouseTrapTimeoutID),e.mouseTrapTimeoutID=setTimeout(function(){e.captureMouse=!1,e.bustClick=!1},400)}},m={down:"touchstart mousedown",move:"mousemove touchmove",up:"mouseup touchend touchcancel",cancel:"mouseleave touchcancel"},b.touch&&(b.mobileOS.ios||b.mobileOS.android)?(m={down:"touchstart",move:"touchmove",up:"touchend touchcancel",cancel:"touchcancel"}):b.pointers?(m={down:"pointerdown",move:"pointermove",up:"pointerup",cancel:"pointercancel pointerleave"}):b.msPointers&&(m={down:"MSPointerDown",move:"MSPointerMove",up:"MSPointerUp",cancel:"MSPointerCancel MSPointerLeave"}),!b.msPointers||"onmspointerenter"in window||c.each({MSPointerEnter:"MSPointerOver",MSPointerLeave:"MSPointerOut"},function(b,a){c.event.special[b]={delegateType:a,bindType:a,handle:function(b){var g,f=this,d=b.relatedTarget,e=b.handleObj;return(!d||d!==f&&!c.contains(f,d))&&(b.type=e.origType,g=e.handler.apply(this,arguments),b.type=a),g}}}),T=function(a){return m[a]||a},F=/([^ ]+)/g,a.applyEventMap=function(a,b){return a=a.replace(F,T),b&&(a=a.replace(F,"$1."+b)),a},y=c.fn.on,i(!0,h,c),h.fn=h.prototype=new c,h.fn.constructor=h,h.fn.init=function(b,a){return a&&a instanceof c&&!(a instanceof h)&&(a=h(a)),c.fn.init.call(this,b,a,M)},h.fn.init.prototype=h.fn,M=h(document),i(h.fn,{handler:function(a){return this.data("handler",a),this},autoApplyNS:function(b){return this.data("kendoNS",b||a.guid()),this},on:function(){var i,c,g,h,k,j,d=this,l=d.data("kendoNS");return 1===arguments.length?y.call(d,arguments[0]):(i=d,c=H.call(arguments),typeof c[c.length-1]===x&&c.pop(),g=c[c.length-1],h=a.applyEventMap(c[0],l),b.mouseAndTouchPresent&&h.search(/mouse|click/)>-1&&this[0]!==document.documentElement&&(e.setupMouseMute(),k=2===c.length?null:c[1],j=h.indexOf("click")>-1&&h.indexOf("touchend")>-1,y.call(this,{touchstart:e.muteMouse,touchend:e.unMuteMouse},k,{bustClick:j})),typeof g===f&&(i=d.data("handler"),g=i[g],c[c.length-1]=function(a){g.call(i,a)}),c[0]=h,y.apply(d,c),d)},kendoDestroy:function(a){return a=a||this.data("kendoNS"),a&&this.off("."+a),this}}),a.jQuery=h,a.eventMap=m,a.timezone=function(){function c(f,i){var c,h,g,k=i[3],e=i[4],d=i[5],j=i[8];return j||(i[8]=j={}),j[f]?j[f]:(isNaN(e)?0===e.indexOf("last")?(c=new Date(Date.UTC(f,a[k]+1,1,d[0]-24,d[1],d[2],0)),h=b[e.substr(4,3)],g=c.getUTCDay(),c.setUTCDate(c.getUTCDate()+h-g-(h>g?7:0))):e.indexOf(">=")>=0&&(c=new Date(Date.UTC(f,a[k],e.substr(5),d[0],d[1],d[2],0)),h=b[e.substr(0,3)],g=c.getUTCDay(),c.setUTCDate(c.getUTCDate()+h-g+(g>h?7:0))):(c=new Date(Date.UTC(f,a[k],e,d[0],d[1],d[2],0))),j[f]=c)}function m(g,a,h){var d,e,b,f;return(a=a[h])?(b=(new Date(g)).getUTCFullYear(),a=jQuery.grep(a,function(c){var d=c[0],a=c[1];return b>=d&&(a>=b||d==b&&"only"==a||"max"==a)}),a.push(g),a.sort(function(d,a){return"number"!=typeof d&&(d=+c(b,d)),"number"!=typeof a&&(a=+c(b,a)),d-a}),f=a[jQuery.inArray(g,a)-1]||a[a.length-1],isNaN(f)?f:null):(d=h.split(":"),e=0,d.length>1&&(e=60*d[0]+ +d[1]),[-1e6,"max","-","Jan",1,[0,0,0],e,"-"])}function i(f,g,d){var b,e,c,a=g[d];if("string"==typeof a&&(a=g[a]),!a)throw Error('Timezone "'+d+'" is either incorrect, or kendo.timezones.min.js is not included.');for(b=a.length-1;b>=0&&(e=a[b][3],!(e&&f>e));b--);if(c=a[b+1],!c)throw Error('Timezone "'+d+'" not found on '+f+".");return c}function d(a,e,d,c){typeof a!=l&&(a=Date.UTC(a.getFullYear(),a.getMonth(),a.getDate(),a.getHours(),a.getMinutes(),a.getSeconds(),a.getMilliseconds()));var b=i(a,e,c);return{zone:b,rule:m(a,d,b[1])}}function j(f,e){var b,c,a;return"Etc/UTC"==e||"Etc/GMT"==e?0:(b=d(f,this.zones,this.rules,e),c=b.zone,a=b.rule,a?c[0]-a[6]:c[0])}function g(e,g){var c=d(e,this.zones,this.rules,g),f=c.zone,b=c.rule,a=f[2];return a.indexOf("/")>=0?a.split("/")[b&&+b[6]?1:0]:a.indexOf("%s")>=0?a.replace("%s",b&&"-"!=b[7]?b[7]:""):a}function e(a,c,b){var e,d;return typeof c==f&&(c=this.offset(a,c)),typeof b==f&&(b=this.offset(a,b)),e=a.getTimezoneOffset(),a=new Date(a.getTime()+6e4*(c-b)),d=a.getTimezoneOffset(),new Date(a.getTime()+6e4*(d-e))}function k(a,b){return this.convert(a,a.getTimezoneOffset(),b)}function n(a,b){return this.convert(a,b,a.getTimezoneOffset())}function h(a){return this.apply(new Date(a),"Etc/UTC")}var a={Jan:0,Feb:1,Mar:2,Apr:3,May:4,Jun:5,Jul:6,Aug:7,Sep:8,Oct:9,Nov:10,Dec:11},b={Sun:0,Mon:1,Tue:2,Wed:3,Thu:4,Fri:5,Sat:6};return{zones:{},rules:{},offset:j,convert:e,apply:k,remove:n,abbr:g,toLocalDate:h}}(),a.date=function(){function b(a,b){return 0===b&&23===a.getHours()?(a.setHours(a.getHours()+2),!0):!1}function j(a,c,d){var e=a.getHours();d=d||1,c=(c-a.getDay()+7*d)%7,a.setDate(a.getDate()+c),b(a,e)}function n(a,b,c){return a=new Date(a),j(a,b,c),a}function i(a){return new Date(a.getFullYear(),a.getMonth(),1)}function o(a){var b=new Date(a.getFullYear(),a.getMonth()+1,0),c=i(a),d=Math.abs(b.getTimezoneOffset()-c.getTimezoneOffset());return d&&b.setHours(c.getHours()+d/60),b}function c(a){return a=new Date(a.getFullYear(),a.getMonth(),a.getDate(),0,0,0),b(a,0),a}function k(a){return Date.UTC(a.getFullYear(),a.getMonth(),a.getDate(),a.getHours(),a.getMinutes(),a.getSeconds(),a.getMilliseconds())}function d(a){return a.getTime()-c(a)}function q(g,h,f){var b,e=d(h),c=d(f);return g&&e!=c?(h>=f&&(f+=a),b=d(g),e>b&&(b+=a),e>c&&(c+=a),b>=e&&c>=b):!0}function l(e,g,f){var c,d=g.getTime(),b=f.getTime();return d>=b&&(b+=a),c=e.getTime(),c>=d&&b>=c}function e(c,d){var e=c.getHours();return c=new Date(c),g(c,d*a),b(c,e),c}function g(a,e,c){var b,d=a.getTimezoneOffset();a.setTime(a.getTime()+e),c||(b=a.getTimezoneOffset()-d,a.setTime(a.getTime()+b*f))}function h(){return c(new Date)}function p(a){return c(a).getTime()==h().getTime()}function m(a){var b=new Date(1980,1,1,0,0,0);return a&&b.setHours(a.getHours(),a.getMinutes(),a.getSeconds(),a.getMilliseconds()),b}var f=6e4,a=8.64e7;return{adjustDST:b,dayOfWeek:n,setDayOfWeek:j,getDate:c,isInDateRange:l,isInTimeRange:q,isToday:p,nextDay:function(a){return e(a,1)},previousDay:function(a){return e(a,-1)},toUtcTime:k,MS_PER_DAY:a,MS_PER_HOUR:60*f,MS_PER_MINUTE:f,setTime:g,addDays:e,today:h,toInvariantTime:m,firstDayOfMonth:i,lastDayOfMonth:o,getMilliseconds:d}}(),a.stripWhitespace=function(d){var c,e,b;if(document.createNodeIterator)for(c=document.createNodeIterator(d,NodeFilter.SHOW_TEXT,function(a){return a.parentNode==d?NodeFilter.FILTER_ACCEPT:NodeFilter.FILTER_REJECT},!1);c.nextNode();)c.referenceNode&&!c.referenceNode.textContent.trim()&&c.referenceNode.parentNode.removeChild(c.referenceNode);else for(e=0;d.childNodes.length>e;e++)b=d.childNodes[e],3!=b.nodeType||/\S/.test(b.nodeValue)||(d.removeChild(b),e--),1==b.nodeType&&a.stripWhitespace(b)},N=window.requestAnimationFrame||window.webkitRequestAnimationFrame||window.mozRequestAnimationFrame||window.oRequestAnimationFrame||window.msRequestAnimationFrame||function(a){setTimeout(a,1e3/60)},a.animationFrame=function(a){N.call(window,a)},k=[],a.queueAnimation=function(b){k[k.length]=b,1===k.length&&a.runNextAnimation()},a.runNextAnimation=function(){a.animationFrame(function(){k[0]&&(k.shift()(),k[0]&&a.runNextAnimation())})},a.parseQueryStringParams=function(d){for(var f=d.split("?")[1]||"",c={},b=f.split(/&|=/),e=b.length,a=0;e>a;a+=2)""!==b[a]&&(c[decodeURIComponent(b[a])]=decodeURIComponent(b[a+1]));return c},a.elementUnderCursor=function(a){return document.elementFromPoint(a.x.client,a.y.client)},a.wheelDeltaY=function(e){var b,a=e.originalEvent,c=a.wheelDeltaY;return a.wheelDelta?(c===d||c)&&(b=a.wheelDelta):a.detail&&a.axis===a.VERTICAL_AXIS&&(b=10*-a.detail),b},a.caret=function(a,e,f){var b,g,i,j,h=e!==d;if(f===d&&(f=e),a[0]&&(a=a[0]),!h||!a.disabled){try{a.selectionStart!==d?h?(a.focus(),a.setSelectionRange(e,f)):(e=[a.selectionStart,a.selectionEnd]):document.selection&&(c(a).is(":visible")&&a.focus(),b=a.createTextRange(),h?(b.collapse(!0),b.moveStart("character",e),b.moveEnd("character",f-e),b.select()):(g=b.duplicate(),b.moveToBookmark(document.selection.createRange().getBookmark()),g.setEndPoint("EndToStart",b),i=g.text.length,j=i+b.text.length,e=[i,j]))}catch(k){e=[]}return e}}})(jQuery);(function(a,k){function T(a){return parseInt(a,10)}function n(a,b){return T(a.css(b))}function jb(c){var b,a=[];for(b in c)a.push(b);return a}function E(a){for(var b in a)-1!=C.indexOf(b)&&-1==u.indexOf(b)&&delete a[b];return a}function z(j,i){var c,a,f,d,b=[],h={};for(a in i)c=a.toLowerCase(),d=e&&-1!=C.indexOf(c),!r.hasHW3D&&d&&-1==u.indexOf(c)?delete i[a]:(f=i[a],d?b.push(a+"("+f+")"):(h[a]=f));return b.length&&(h[g]=b.join(" ")),h}function V(f,b){var d,a,c;return e?(d=f.css(g),d==fb?"scale"==b?1:0:(a=d.match(RegExp(b+"\\s*\\(([\\d\\w\\.]+)")),c=0,a?(c=T(a[1])):(a=d.match(cb)||[0,0,0,0,0],b=b.toLowerCase(),ib.test(b)?(c=parseFloat(a[3]/a[2])):"translatey"==b?(c=parseFloat(a[4]/a[2])):"scale"==b?(c=parseFloat(a[2])):"rotate"==b&&(c=parseFloat(Math.atan2(a[2],a[1])))),c)):parseFloat(f.css(b))}function G(a){return a.charAt(0).toUpperCase()+a.substring(1)}function f(b,e){var a=i.extend(e),c=a.prototype.directions;d[G(b)]=a,d.Element.prototype[b]=function(b,e,c,d){return new a(this.element,b,e,c,d)},o(c,function(e,c){d.Element.prototype[b+G(c)]=function(b,e,d){return new a(this.element,c,b,e,d)}})}function O(d,a,b,c){f(d,{directions:S,startValue:function(a){return this._startValue=a,this},endValue:function(a){return this._endValue=a,this},shouldHide:function(){return this._shouldHide},prepare:function(h,f){var d,g,j=this,i="out"===this._direction,e=j.element.data(a),l=!(isNaN(e)||e==b);d=l?e:k!==this._startValue?this._startValue:i?b:c,g=k!==this._endValue?this._endValue:i?c:b,this._reverse?(h[a]=g,f[a]=d):(h[a]=d,f[a]=g),j._shouldHide=f[a]===c}})}function P(c,a){var e=b.directions[a].vertical,d=c[e?x:F]()/2+"px";return Y[a].replace("$size",d)}var Q,B,i,q,S,W,Y,Z,A,w,H,b=window.kendo,d=b.effects,o=a.each,c=a.extend,kb=a.proxy,r=b.support,nb=r.browser,e=r.transforms,h=r.transitions,db={scale:0,scalex:0,scaley:0,scale3d:0},gb={translate:0,translatex:0,translatey:0,translate3d:0},U=k!==document.documentElement.style.zoom&&!e,cb=/matrix3?d?\s*\(.*,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?,\s*([\d\.\-]+)\w*?/i,R=/^(-?[\d\.\-]+)?[\w\s]*,?\s*(-?[\d\.\-]+)?[\w\s]*/i,ib=/translatex?$/i,eb=/(zoom|fade|expand)(\w+)/,N=/(zoom|fade|expand)/,hb=/[xy]$/i,C=["perspective","rotate","rotatex","rotatey","rotatez","rotate3d","scale","scalex","scaley","scalez","scale3d","skew","skewx","skewy","translate","translatex","translatey","translatez","translate3d","matrix","matrix3d"],u=["rotate","scale","scalex","scaley","skew","skewx","skewy","translate","translatex","translatey","matrix"],lb={rotate:"deg",scale:"",skew:"px",translate:"px"},v=e.css,bb=Math.round,mb="",p="px",fb="none",I="auto",F="width",x="height",X="hidden",D="origin",K="abortId",t="overflow",l="translate",s="position",L="completeCallback",m=v+"transition",g=v+"transform",ab=v+"backface-visibility",M=v+"perspective",y="1500px",J="perspective("+y+")",j={left:{reverse:"right",property:"left",transition:"translatex",vertical:!1,modifier:-1},right:{reverse:"left",property:"left",transition:"translatex",vertical:!1,modifier:1},down:{reverse:"up",property:"top",transition:"translatey",vertical:!0,modifier:1},up:{reverse:"down",property:"top",transition:"translatey",vertical:!0,modifier:-1},top:{reverse:"bottom"},bottom:{reverse:"top"},"in":{reverse:"out",modifier:-1},out:{reverse:"in",modifier:1},vertical:{reverse:"vertical"},horizontal:{reverse:"horizontal"}};b.directions=j,c(a.fn,{kendoStop:function(a,b){return h?d.stopQueue(this,a||!1,b||!1):this.stop(a,b)}}),e&&!h&&(o(u,function(c,b){a.fn[b]=function(d){if(k===d)return V(this,b);var c=a(this)[0],e=b+"("+d+lb[b.replace(hb,"")]+")";return-1==c.style.cssText.indexOf(g)?a(this).css(g,e):(c.style.cssText=c.style.cssText.replace(RegExp(b+"\\(.*?\\)","i"),e)),this},a.fx.step[b]=function(c){a(c.elem)[b](c.now)}}),Q=a.fx.prototype.cur,a.fx.prototype.cur=function(){return-1!=u.indexOf(this.prop)?parseFloat(a(this.elem)[this.prop]()):Q.apply(this,arguments)}),b.toggleClass=function(b,d,a,e){return d&&(d=d.split(" "),h&&(a=c({exclusive:"all",duration:400,ease:"ease-out"},a),b.css(m,a.exclusive+" "+a.duration+"ms "+a.ease),setTimeout(function(){b.css(m,"").css(x)},a.duration)),o(d,function(c,a){b.toggleClass(a,e)})),b},b.parseEffects=function(a,c){var b={};return"string"==typeof a?o(a.split(" "),function(i,d){var h=!N.test(d),g=d.replace(eb,function(c,b,a){return b+":"+a.toLowerCase()}),a=g.split(":"),e=a[1],f={};a.length>1&&(f.direction=c&&h?j[e].reverse:e),b[a[0]]=f}):o(a,function(a){var d=this.direction;d&&c&&!N.test(a)&&(this.direction=j[d].reverse),b[a]=this}),b},h&&c(d,{transition:function(d,n,b){var i,e,j,f,k=0,l=d.data("keys")||[];b=c({duration:200,ease:"ease-out",complete:null,exclusive:"all"},b),j=!1,f=function(){j||(j=!0,e&&(clearTimeout(e),e=null),d.removeData(K).dequeue().css(m,"").css(m),b.complete.call(d))},b.duration=a.fx?a.fx.speeds[b.duration]||b.duration:b.duration,i=z(d,n),a.merge(l,jb(i)),d.data("keys",a.unique(l)).height(),d.css(m,b.exclusive+" "+b.duration+"ms "+b.ease).css(m),d.css(i).css(g),h.event&&(d.one(h.event,f),0!==b.duration&&(k=500)),e=setTimeout(f,b.duration+k),d.data(K,e),d.data(L,f)},stopQueue:function(a,h,g){var d,f=a.data("keys"),e=!g&&f,c=a.data(L);return e&&(d=b.getComputedStyles(a[0],f)),c&&c(),e&&a.css(d),a.removeData("keys").stop(h)}}),B=b.Class.extend({init:function(b,c){var a=this;a.element=b,a.effects=[],a.options=c,a.restore=[]},run:function(n){var k,j,m,q,r,u,s,l=this,t=n.length,i=l.element,f=l.options,p=a.Deferred(),b={},o={};for(l.effects=n,p.then(a.proxy(l,"complete")),i.data("animating",!0),j=0;t>j;j++)for(k=n[j],k.setReverse(f.reverse),k.setOptions(f),l.addRestoreProperties(k.restore),k.prepare(b,o),r=k.children(),m=0,u=r.length;u>m;m++)r[m].duration(f.duration).run();for(s in f.effects)c(o,f.effects[s].properties);for(i.is(":visible")||c(b,{display:i.data("olddisplay")||"block"}),e&&!f.reset&&(q=i.data("targetTransform"),q&&(b=c(q,b))),b=z(i,b),e&&!h&&(b=E(b)),i.css(b).css(g),j=0;t>j;j++)n[j].setup();return f.init&&f.init(),i.data("targetTransform",o),d.animate(i,o,c({},f,{complete:p.resolve})),p.promise()},stop:function(){a(this.element).kendoStop(!0,!0)},addRestoreProperties:function(d){for(var a,c=this.element,b=0,e=d.length;e>b;b++)a=d[b],this.restore.push(a),c.data(a)||c.data(a,c.css(a))},restoreCallback:function(){var a,d,b,c=this.element;for(a=0,d=this.restore.length;d>a;a++)b=this.restore[a],c.css(b,c.data(b))},complete:function(){var f=this,c=0,b=f.element,d=f.options,g=f.effects,h=g.length;for(b.removeData("animating").dequeue(),d.hide&&b.data("olddisplay",b.css("display")).hide(),this.restoreCallback(),U&&!e&&setTimeout(a.proxy(this,"restoreCallback"),0);h>c;c++)g[c].teardown();d.completeCallback&&d.completeCallback(b)}}),d.promise=function(a,c){var e,j,g,f=[],i=new B(a,c),h=b.parseEffects(c.effects);c.effects=h;for(g in h)e=d[G(g)],e&&(j=new e(a,h[g].direction),f.push(j));f[0]?i.run(f):(a.is(":visible")||a.css({display:a.data("olddisplay")||"block"}).css("display"),c.init&&c.init(),a.dequeue(),i.complete())},c(d,{animate:function(g,f,b){var i=b.transition!==!1;delete b.transition,h&&"transition"in d&&i?d.transition(g,f,b):e?g.animate(E(f),{queue:!1,show:!1,hide:!1,duration:b.duration,complete:b.complete}):g.each(function(){var d=a(this),g={};o(C,function(r,b){var m,a,p,o,h,i,j,q=f?f[b]+" ":null;q&&(a=f,b in db&&f[b]!==k?(m=q.match(R),e&&c(a,{scale:+m[0]})):b in gb&&f[b]!==k&&(p=d.css(s),o="absolute"==p||"fixed"==p,d.data(l)||(o?d.data(l,{top:n(d,"top")||0,left:n(d,"left")||0,bottom:n(d,"bottom"),right:n(d,"right")}):d.data(l,{top:n(d,"marginTop")||0,left:n(d,"marginLeft")||0})),h=d.data(l),m=q.match(R),m&&(i=b==l+"y"?0:+m[1],j=b==l+"y"?+m[1]:+m[2],o?(isNaN(h.right)?isNaN(i)||c(a,{left:h.left+i}):isNaN(i)||c(a,{right:h.right-i}),isNaN(h.bottom)?isNaN(j)||c(a,{top:h.top+j}):isNaN(j)||c(a,{bottom:h.bottom-j})):(isNaN(i)||c(a,{marginLeft:h.left+i}),isNaN(j)||c(a,{marginTop:h.top+j})))),!e&&"scale"!=b&&b in a&&delete a[b],a&&c(g,a))}),nb.msie&&delete g.scale,d.animate(g,{queue:!1,show:!1,hide:!1,duration:b.duration,complete:b.complete})})}}),d.animatedPromise=d.promise,i=b.Class.extend({init:function(b,c){var a=this;a.element=b,a._direction=c,a.options={},a._additionalEffects=[],a.restore||(a.restore=[])},reverse:function(){return this._reverse=!0,this.run()},play:function(){return this._reverse=!1,this.run()},add:function(a){return this._additionalEffects.push(a),this},direction:function(a){return this._direction=a,this},duration:function(a){return this._duration=a,this},compositeRun:function(){var a=this,c=new B(a.element,{reverse:a._reverse,duration:a._duration}),b=a._additionalEffects.concat([a]);return c.run(b)},run:function(){if(this._additionalEffects&&this._additionalEffects[0])return this.compositeRun();var k,n,i=this,b=i.element,j=0,p=i.restore,q=p.length,l=a.Deferred(),f={},m={},o=i.children(),r=o.length;for(l.then(a.proxy(i,"_complete")),b.data("animating",!0),j=0;q>j;j++)k=p[j],b.data(k)||b.data(k,b.css(k));for(j=0;r>j;j++)o[j].duration(i._duration).run();return i.prepare(f,m),b.is(":visible")||c(f,{display:b.data("olddisplay")||"block"}),e&&(n=b.data("targetTransform"),n&&(f=c(n,f))),f=z(b,f),e&&!h&&(f=E(f)),b.css(f).css(g),i.setup(),b.data("targetTransform",m),d.animate(b,m,{duration:i._duration,complete:l.resolve}),l.promise()},stop:function(){for(var b=0,c=this.children(),d=c.length,b=0;d>b;b++)c[b].stop();return a(this.element).kendoStop(!0,!0),this},restoreCallback:function(){var a,d,b,c=this.element;for(a=0,d=this.restore.length;d>a;a++)b=this.restore[a],c.css(b,c.data(b))},_complete:function(){var b=this,c=b.element;c.removeData("animating").dequeue(),b.restoreCallback(),b.shouldHide()&&c.data("olddisplay",c.css("display")).hide(),U&&!e&&setTimeout(a.proxy(b,"restoreCallback"),0),b.teardown()},setOptions:function(a){c(!0,this.options,a)},children:function(){return[]},shouldHide:a.noop,setup:a.noop,prepare:a.noop,teardown:a.noop,directions:[],setReverse:function(a){return this._reverse=a,this}}),q=["left","right","up","down"],S=["in","out"],f("slideIn",{directions:q,divisor:function(a){return this.options.divisor=a,this},prepare:function(c,d){var f,b=this,g=b.element,a=j[b._direction],k=-a.modifier*(a.vertical?g.outerHeight():g.outerWidth()),h=k/(b.options&&b.options.divisor||1)+p,i="0px";b._reverse&&(f=c,c=d,d=f),e?(c[a.transition]=h,d[a.transition]=i):(c[a.property]=h,d[a.property]=i)}}),f("tile",{directions:q,init:function(a,c,b){i.prototype.init.call(this,a,c),this.options={previous:b}},previousDivisor:function(a){return this.options.previousDivisor=a,this},children:function(){var a=this,f=a._reverse,c=a.options.previous,g=a.options.previousDivisor||1,e=a._direction,d=[b.fx(a.element).slideIn(e).setReverse(f)];return c&&d.push(b.fx(c).slideIn(j[e].reverse).divisor(g).setReverse(!f)),d}}),O("fade","opacity",1,0),O("zoom","scale",1,.01),f("slideMargin",{prepare:function(i,h){var c,b=this,a=b.element,d=b.options,f=a.data(D),g=d.offset,e=b._reverse;e||null!==f||a.data(D,parseFloat(a.css("margin-"+d.axis))),c=a.data(D)||0,h["margin-"+d.axis]=e?c:c+g}}),f("slideTo",{prepare:function(h,c){var d=this,f=d.element,g=d.options,b=g.offset.split(","),a=d._reverse;e?(c.translatex=a?0:b[0],c.translatey=a?0:b[1]):(c.left=a?0:b[0],c.top=a?0:b[1]),f.css("left")}}),f("expand",{directions:["horizontal","vertical"],restore:[t],prepare:function(g,l){var c=this,d=c.element,i=c.options,e=c._reverse,a="vertical"===c._direction?x:F,j=d[0].style[a],h=d.data(a),b=parseFloat(h||j),f=bb(d.css(a,I)[a]());g.overflow=X,b=i&&i.reset?f||b:b||f,l[a]=(e?0:b)+p,g[a]=(e?b:0)+p,h===k&&d.data(a,j)},shouldHide:function(){return this._reverse},teardown:function(){var b=this,d=b.element,a="vertical"===b._direction?x:F,c=d.data(a);(c==I||c===mb)&&setTimeout(function(){d.css(a,I).css(a)},0)}}),W={position:"absolute",marginLeft:0,marginTop:0,scale:1},f("transfer",{init:function(a,b){this.element=a,this.options={target:b},this.restore=[]},setup:function(){this.element.appendTo(document.body)},prepare:function(b,w){var e,d,f,m,i,o,k,r,s,h,j,n,t,l=this,a=l.element,z=l.options,x=l._reverse,q=z.target,y=V(a,"scale"),v=q.offset(),u=q.outerHeight()/a.outerHeight();c(b,W),w.scale=1,a.css(g,"scale(1)").css(g),e=a.offset(),a.css(g,"scale("+y+")"),d=0,f=0,m=v.left-e.left,i=v.top-e.top,o=d+a.outerWidth(),k=f,r=m+q.outerWidth(),s=i,h=(i-f)/(m-d),j=(s-k)/(r-o),n=(f-k-h*d+j*o)/(j-h),t=f+h*(n-d),b.top=e.top,b.left=e.left,b.transformOrigin=n+p+" "+t+p,x?(b.scale=u):(w.scale=u)}}),Y={top:"rect(auto auto $size auto)",bottom:"rect($size auto auto auto)",left:"rect(auto $size auto auto)",right:"rect(auto auto auto $size)"},Z={top:{start:"rotatex(0deg)",end:"rotatex(180deg)"},bottom:{start:"rotatex(-180deg)",end:"rotatex(0deg)"},left:{start:"rotatey(0deg)",end:"rotatey(-180deg)"},right:{start:"rotatey(180deg)",end:"rotatey(0deg)"}},f("turningPage",{directions:q,init:function(a,c,b){i.prototype.init.call(this,a,c),this._container=b},prepare:function(c,h){var a=this,e=a._reverse,f=e?j[a._direction].reverse:a._direction,d=Z[f];c.zIndex=1,a._clipInHalf&&(c.clip=P(a._container,b.directions[f].reverse)),c[ab]=X,h[g]=J+(e?d.start:d.end),c[g]=J+(e?d.end:d.start)},setup:function(){this._container.append(this.element)},face:function(a){return this._face=a,this},shouldHide:function(){var a=this,c=a._reverse,b=a._face;return c&&!b||!c&&b},clipInHalf:function(a){return this._clipInHalf=a,this},temporary:function(){return this.element.addClass("temp-page"),this}}),f("staticPage",{directions:q,init:function(a,c,b){i.prototype.init.call(this,a,c),this._container=b},restore:["clip"],prepare:function(b,d){var a=this,c=a._reverse?j[a._direction].reverse:a._direction;b.clip=P(a._container,c),b.opacity=.999,d.opacity=1},shouldHide:function(){var a=this,c=a._reverse,b=a._face;return c&&!b||!c&&b},face:function(a){return this._face=a,this}}),f("pageturn",{directions:["horizontal","vertical"],init:function(a,d,c,b){i.prototype.init.call(this,a,d),this.options={},this.options.face=c,this.options.back=b},children:function(){var h,f=this,d=f.options,a="horizontal"===f._direction?"left":"top",e=b.directions[a].reverse,c=f._reverse,i=d.face.clone(!0).removeAttr("id"),j=d.back.clone(!0).removeAttr("id"),g=f.element;return c&&(h=a,a=e,e=h),[b.fx(d.face).staticPage(a,g).face(!0).setReverse(c),b.fx(d.back).staticPage(e,g).setReverse(c),b.fx(i).turningPage(a,g).face(!0).clipInHalf(!0).temporary().setReverse(c),b.fx(j).turningPage(e,g).clipInHalf(!0).temporary().setReverse(c)]},prepare:function(a,b){a[M]=y,a.transformStyle="preserve-3d",a.opacity=.999,b.opacity=1},teardown:function(){this.element.find(".temp-page").remove()}}),f("flip",{directions:["horizontal","vertical"],init:function(a,d,c,b){i.prototype.init.call(this,a,d),this.options={},this.options.face=c,this.options.back=b},children:function(){var g,c=this,h=c.options,a="horizontal"===c._direction?"left":"top",e=b.directions[a].reverse,d=c._reverse,f=c.element;return d&&(g=a,a=e,e=g),[b.fx(h.face).turningPage(a,f).face(!0).setReverse(d),b.fx(h.back).turningPage(e,f).setReverse(d)]},prepare:function(a){a[M]=y,a.transformStyle="preserve-3d"}}),A=!r.mobileOS.android,f("replace",{init:function(d,c,b){i.prototype.init.call(this,d),this._previous=a(c),this._transitionClass=b},duration:function(){throw Error("The replace effect does not support duration setting; the effect duration may be customized through the transition class rule");},_both:function(){return a().add(this._element).add(this._previous)},_containerClass:function(){var b=this._direction,a="k-fx k-fx-start k-fx-"+this._transitionClass;return b&&(a+=" k-fx-"+b),this._reverse&&(a+=" k-fx-reverse"),a},complete:function(){if(this.deferred){var a=this.container;a.removeClass("k-fx-end").removeClass(this._containerClass()),this._previous.hide().removeClass("k-fx-current"),this.element.removeClass("k-fx-next"),A&&a.css(t,""),this.isAbsolute||this._both().css(s,""),this.deferred.resolve(),delete this.deferred}},run:function(){if(this._additionalEffects&&this._additionalEffects[0])return this.compositeRun();var j,e=this,d=e.element,f=e._previous,c=d.parents().filter(f.parents()).first(),i=e._both(),g=a.Deferred(),k=d.css(s);return c.length||(c=d.parent()),this.container=c,this.deferred=g,this.isAbsolute="absolute"==k,this.isAbsolute||i.css(s,"absolute"),A&&(j=c.css(t),c.css(t,"hidden")),h?(d.addClass("k-fx-hidden"),c.addClass(this._containerClass()),c.one(h.event,a.proxy(this,"complete")),b.animationFrame(function(){d.removeClass("k-fx-hidden").addClass("k-fx-next"),f.css("display","").addClass("k-fx-current"),b.animationFrame(function(){c.removeClass("k-fx-start").addClass("k-fx-end")})})):this.complete(),g.promise()},stop:function(){this.complete()}}),w=b.Class.extend({init:function(){var a=this;a._tickProxy=kb(a._tick,a),a._started=!1},tick:a.noop,done:a.noop,onEnd:a.noop,onCancel:a.noop,start:function(){this.enabled()&&(this.done()?this.onEnd():(this._started=!0,b.animationFrame(this._tickProxy)))},enabled:function(){return!0},cancel:function(){this._started=!1,this.onCancel()},_tick:function(){var a=this;a._started&&(a.tick(),a.done()?(a._started=!1,a.onEnd()):b.animationFrame(a._tickProxy))}}),H=w.extend({init:function(b){var a=this;c(a,b),w.fn.init.call(a)},done:function(){return this.timePassed()>=this.duration},timePassed:function(){return Math.min(this.duration,new Date-this.startDate)},moveTo:function(b){var a=this,c=a.movable;a.initial=c[a.axis],a.delta=b.location-a.initial,a.duration="number"==typeof b.duration?b.duration:300,a.tick=a._easeProxy(b.ease),a.startDate=new Date,a.start()},_easeProxy:function(b){var a=this;return function(){a.movable.moveAxis(a.axis,b(a.timePassed(),a.initial,a.delta,a.duration))}}}),c(H,{easeOutExpo:function(a,d,c,b){return a==b?d+c:c*(-Math.pow(2,-10*a/b)+1)+d},easeOutBack:function(a,e,d,c,b){return b=1.70158,d*((a=a/c-1)*a*((b+1)*a+b)+1)+e}}),d.Animation=w,d.Transition=H,d.createEffect=f})(window.kendo.jQuery);(function(a,h){function d(c){var j,p,i,m,e,k,g,o,l=[],q=c.logic||"and",n=c.filters;for(j=0,p=n.length;p>j;j++)c=n[j],i=c.field,g=c.value,k=c.operator,c.filters?(c=d(c)):(o=c.ignoreCase,i=i.replace(/\./g,"/"),c=f[k],c&&g!==h&&(m=a.type(g),"string"===m?(e="'{1}'",g=g.replace(/'/g,"''"),o===!0&&(i="tolower("+i+")")):(e="date"===m?"datetime'{1:yyyy-MM-ddTHH:mm:ss}'":"{1}"),c.length>3?"substringof"!==c?(e="{0}({2},"+e+")"):(e="{0}("+e+",{2})","doesnotcontain"===k&&(e+=" eq false")):(e="{2} {0} "+e),c=b.format(e,c,g,i))),l.push(c);return c=l.join(" "+q+" "),l.length>1&&(c="("+c+")"),c}var b=window.kendo,e=a.extend,f={eq:"eq",neq:"ne",gt:"gt",gte:"ge",lt:"lt",lte:"le",contains:"substringof",doesnotcontain:"substringof",endswith:"endswith",startswith:"startswith"},c={pageSize:a.noop,page:a.noop,filter:function(b,a){a&&(b.$filter=d(a))},sort:function(d,c){var b=a.map(c,function(a){var b=a.field.replace(/\./g,"/");return"desc"===a.dir&&(b+=" desc"),b}).join(",");b&&(d.$orderby=b)},skip:function(b,a){a&&(b.$skip=a)},take:function(b,a){a&&(b.$top=a)}},g={read:{dataType:"jsonp"}};e(!0,b.data,{schemas:{odata:{type:"json",data:function(a){return a.d.results||[a.d]},total:"d.__count"}},transports:{odata:{read:{cache:!0,dataType:"jsonp",jsonp:"$callback"},update:{cache:!0,dataType:"json",contentType:"application/json",type:"PUT"},create:{cache:!0,dataType:"json",contentType:"application/json",type:"POST"},destroy:{cache:!0,dataType:"json",type:"DELETE"},parameterMap:function(a,f){var e,i,d,h;if(a=a||{},f=f||"read",h=(this.options||g)[f],h=h?h.dataType:"json","read"===f){e={$inlinecount:"allpages"},"json"!=h&&(e.$format="json");for(d in a)c[d]?c[d](e,a[d]):(e[d]=a[d])}else{if("json"!==h)throw Error("Only json dataType can be used for "+f+" operation.");if("destroy"!==f){for(d in a)i=a[d],"number"==typeof i&&(a[d]=i+"");e=b.stringify(a)}}return e}}}})})(window.kendo.jQuery);(function(a,j){var b=window.kendo,c=a.isArray,e=a.isPlainObject,i=a.map,g=a.each,f=a.extend,k=b.getter,h=b.Class,d=h.extend({init:function(h){var r,m,n,o,a=this,j=h.total,d=h.model,q=h.parse,k=h.errors,p=h.serialize,l=h.data;d&&(e(d)&&(r=h.modelBase||b.data.Model,d.fields&&g(d.fields,function(c,b){b=e(b)&&b.field?f(b,{field:a.getter(b.field)}):{field:a.getter(b)},d.fields[c]=b}),m=d.id,m&&(n={},n[a.xpathToMember(m,!0)]={field:a.getter(m)},d.fields=f(n,d.fields),d.id=a.xpathToMember(m)),d=r.define(d)),a.model=d),j&&("string"==typeof j?(j=a.getter(j),a.total=function(a){return parseInt(j(a),10)}):"function"==typeof j&&(a.total=j)),k&&("string"==typeof k?(k=a.getter(k),a.errors=function(a){return k(a)||null}):"function"==typeof k&&(a.errors=k)),l&&("string"==typeof l?(l=a.xpathToMember(l),a.data=function(f){var e,b=a.evaluate(f,l);return b=c(b)?b:[b],a.model&&d.fields?(e=new a.model,i(b,function(b){if(b){var a,c={};for(a in d.fields)c[a]=e._parse(a,d.fields[a].field(b));return c}})):b}):"function"==typeof l&&(a.data=l)),"function"==typeof q&&(o=a.parse,a.parse=function(b){var c=q.call(a,b);return o.call(a,c)}),"function"==typeof p&&(a.serialize=p)},total:function(a){return this.data(a).length},errors:function(a){return a?a.errors:null},serialize:function(a){return a},parseDOM:function(l){for(var e,a,f,g,b,i,d={},k=l.attributes,m=k.length,h=0;m>h;h++)i=k[h],d["@"+i.nodeName]=i.nodeValue;for(a=l.firstChild;a;a=a.nextSibling)f=a.nodeType,3===f||4===f?(d["#text"]=a.nodeValue):1===f&&(e=this.parseDOM(a),g=a.nodeName,b=d[g],c(b)?b.push(e):(b=b!==j?[b,e]:e),d[g]=b);return d},evaluate:function(b,f){for(var g,d,h,a,e,i=f.split(".");g=i.shift();)if(b=b[g],c(b)){for(d=[],f=i.join("."),e=0,h=b.length;h>e;e++)a=this.evaluate(b[e],f),a=c(a)?a:[a],d.push.apply(d,a);return d}return b},parse:function(e){var b,d,c={};return b=e.documentElement||a.parseXML(e).documentElement,d=this.parseDOM(b),c[b.nodeName]=d,c},xpathToMember:function(a,b){return a?(a=a.replace(/^\//,"").replace(/\//g,"."),a.indexOf("@")>=0?a.replace(/\.?(@.*)/,b?"$1":'["$1"]'):a.indexOf("text()")>=0?a.replace(/(\.?text\(\))/,b?"#text":'["#text"]'):a):""},getter:function(a){return k(this.xpathToMember(a),!0)}});a.extend(!0,b.data,{XmlDataReader:d,readers:{xml:d}})})(window.kendo.jQuery);(function(e,a){function U(a,d,b,e){return function(g){var h,f={};for(h in g)f[h]=g[h];f.field=e?b+"."+g.field:b,d==c&&a._notifyChange&&a._notifyChange(f),a.trigger(d,f)}}function sb(a,b){if(a===b)return!0;var d,c=e.type(a),f=e.type(b);if(c!==f)return!1;if("date"===c)return a.getTime()===b.getTime();if("object"!==c&&"array"!==c)return!1;for(d in a)if(!sb(a[d],b[d]))return!1;return!0}function Sb(b,d){var a,c;for(c in b){if(a=b[c],r(a)&&a.field&&a.field===d)return a;if(a===d)return a}return null}function d(a){this.data=a||[]}function A(b,e){if(b){var c=typeof b===h?{field:b,dir:e}:b,d=k(c)?c:c!==a?[c]:[];return Jb(d,function(a){return!!a.dir})}}function tb(f){var c,e,b,a,d=f.filters;if(d)for(c=0,e=d.length;e>c;c++)b=d[c],a=b.operator,a&&typeof a===h&&(b.operator=lb[a.toLowerCase()]||a),tb(b)}function H(b){return b&&!w(b)?((k(b)||!b.filters)&&(b={logic:"and",filters:k(b)?b:[b]}),tb(b),b):a}function Tb(a){return k(a)?a:[a]}function B(c,e){var b=typeof c===h?{field:c,dir:e}:c,d=k(b)?b:b!==a?[b]:[];return R(d,function(a){return{field:a.field,dir:a.dir||"asc",aggregates:a.aggregates}})}function Pb(a,b){return a&&a.getTime&&b&&b.getTime?a.getTime()===b.getTime():a===b}function Rb(f,g,k,l,j,d){var e,h,c,i,a;for(g=g||[],i=g.length,e=0;i>e;e++)h=g[e],c=h.aggregate,a=h.field,f[a]=f[a]||{},d[a]=d[a]||{},d[a][c]=d[a][c]||{},f[a][c]=nb[c.toLowerCase()](f[a][c],k,b.accessor(a),l,j,d[a][c])}function o(a){return"number"==typeof a&&!isNaN(a)}function Vb(b){for(var c=b.length,d=Array(c),a=0;c>a;a++)d[a]=b[a].toJSON();return d}function kb(e,a){a=a||{};var b=new d(e),f=a.aggregate,c=a.filter;return c&&(b=b.filter(c)),b.aggregate(f)}function Ub(e,g,i,j,h){var b,a,c,d,f;for(d=0,f=e.length;f>d;d++){b=e[d];for(a in g)c=h[a],c&&c!==a&&(b[c]=g[a](b),delete b[a])}}function wb(e,g,i,j,h){var b,a,c,d,f;for(d=0,f=e.length;f>d;d++){b=e[d];for(a in g)b[a]=i._parse(a,g[a](b)),c=h[a],c&&c!==a&&delete b[c]}}function yb(f,h,d,e,g){var a,b,c,i;for(b=0,i=f.length;i>b;b++)a=f[b],c=e[a.field],c&&c!=a.field&&(a.field=c),a.value=d._parse(a.field,a.value),a.hasSubgroups?yb(a.items,h,d,e,g):wb(a.items,h,d,e,g)}function Z(b,f,d,a,c,e){return function(g){return g=b(g),g&&!w(a)&&("[object Array]"===D.call(g)||g instanceof p||(g=[g]),d(g,a,new f,c,e)),g||[]}}function Y(b,e,d,g){for(var c,a,h,f=0;e.length&&g&&(c=e[f],a=c.items,h=a.length,b&&b.field===c.field&&b.value===c.value?(b.hasSubgroups&&b.items.length?Y(b.items[b.items.length-1],c.items,d,g):(a=a.slice(d,d+g),b.items=b.items.concat(a)),e.splice(f--,1)):c.hasSubgroups&&a.length?Y(c,a,d,g):(a=a.slice(d,d+g),c.items=a,c.items.length||e.splice(f--,1)),0===a.length?(d-=h):(d=0,g-=a.length),!(++f>=e.length)););e.length>f&&e.splice(f,e.length-f)}function gb(b){var a,d,c=[];for(a=0,d=b.length;d>a;a++)c=b[a].hasSubgroups?c.concat(gb(b[a].items)):c.concat(b[a].items.slice());return c}function ub(e,c){var b,f,d,a;if(c)for(b=0,f=e.length;f>b;b++)d=e[b],a=d.items,d.hasSubgroups?ub(a,c):!a.length||a[0]instanceof c||(a.type=c,a.wrapAll(a,a))}function ob(b,d){var a,c;for(a=0,c=b.length;c>a;a++)if(b[a].hasSubgroups){if(ob(b[a].items,d))return!0}else if(d(b[a].items,b[a]))return!0}function xb(b,c){var a,d;for(a=0,d=b.length;d>a;a++)if(b[a].uid==c.uid)return c=b[a],b.splice(a,1),c}function T(d,f){var a,c,b,e;for(b=d.length-1,e=0;b>=e;b--)c=d[b],a={value:f.get(c.field),field:c.field,items:a?[a]:[f],hasSubgroups:!!a,aggregates:{}};return a}function vb(b,a){return a?qb(b,function(b){return b[a.idField]===a.id}):-1}function hb(b,a){return a?qb(b,function(b){return b.uid==a.uid}):-1}function qb(b,d){var a,c;for(a=0,c=b.length;c>a;a++)if(d(b[a]))return a;return-1}function Ab(b,a){var c,d;return b&&!w(b)?(c=b[a],d=r(c)?c.from||c.field||a:b[a]||a,n(d)?a:d):a}function ib(b,f){var c,e,d,a={};for(d in b)"filters"!==d&&(a[d]=b[d]);if(b.filters)for(a.filters=[],c=0,e=b.filters.length;e>c;c++)a.filters[c]=ib(b.filters[c],f);else a.field=Ab(f.fields,a.field);return a}function F(e,h){var b,f,a,d,c,g=[];for(b=0,f=e.length;f>b;b++){a={},d=e[b];for(c in d)a[c]=d[c];a.field=Ab(h.fields,a.field),a.aggregates&&k(a.aggregates)&&(a.aggregates=F(a.aggregates,h)),g.push(a)}return g}function Ob(l,h){var d,f,c,b,a,i=e(l)[0].children,g=[],j=h[0],k=h[1];for(d=0,f=i.length;f>d;d++)c={},a=i[d],a.disabled||(c[j.field]=a.text,b=a.attributes.value,b=b&&b.specified?a.value:a.text,c[k.field]=b,g.push(c));return g}function Qb(n,l){var b,k,a,g,d,f,c,h=e(n)[0].tBodies[0],i=h?h.rows:[],m=l.length,j=[];for(b=0,k=i.length;k>b;b++){for(d={},c=!0,g=i[b].cells,a=0;m>a;a++)f=g[a],"th"!==f.nodeName.toLowerCase()&&(c=!1,d[l[a].field]=f.innerHTML);c||j.push(d)}return j}function pb(a){return function(){var c=this._data,b=j.fn[a].apply(this,L.call(arguments));return this._data!=c&&this._attachBubbleHandlers(),b}}function jb(h,c){function j(a,b){return a.filter(b).add(a.find(b))}var f,p,a,d,k,g,i,b,l=e(h).children(),n=[],r=c[0].field,m=c[1]&&c[1].field,o=c[2]&&c[2].field,q=c[3]&&c[3].field;for(f=0,p=l.length;p>f;f++)a={_loaded:!0},d=l.eq(f),g=d[0].firstChild,b=d.children(),h=b.filter("ul"),b=b.filter(":not(ul)"),k=d.attr("data-id"),k&&(a.id=k),g&&(a[r]=3==g.nodeType?g.nodeValue:b.text()),m&&(a[m]=j(b,"a").attr("href")),q&&(a[q]=j(b,"img").attr("src")),o&&(i=j(b,".k-sprite").prop("className"),a[o]=i&&e.trim(i.replace("k-sprite",""))),h.length&&(a.items=jb(h.eq(0),c)),"true"==d.attr("data-hasChildren")&&(a.hasChildren=!0),n.push(a);return n}var R,G,rb,mb,lb,nb,W,Q,x,P,j,S,X,s,V,zb,g=e.extend,l=e.proxy,r=e.isPlainObject,w=e.isEmptyObject,k=e.isArray,Jb=e.grep,K=e.ajax,M=e.each,t=e.noop,b=window.kendo,n=b.isFunction,u=b.Observable,C=b.Class,h="string",J="function",cb="create",eb="read",bb="update",db="destroy",c="change",fb="sync",O="get",q="error",y="requestStart",N="progress",v="requestEnd",Mb=[cb,eb,bb,db],z=function(a){return a},E=b.getter,I=b.stringify,f=Math,Nb=[].push,Cb=[].join,Hb=[].pop,Bb=[].splice,Eb=[].shift,L=[].slice,Db=[].unshift,D={}.toString,Kb=b.support.stableSort,Ib=/^\/Date\((.*?)\)\/$/,Gb=/(\r+|\n+)/g,Fb=/(?=['\\])/g,p=u.extend({init:function(b,c){var a=this;a.type=c||i,u.fn.init.call(a),a.length=b.length,a.wrapAll(b,a)},toJSON:function(){for(var a,c=this.length,d=Array(c),b=0;c>b;b++)a=this[b],a instanceof i&&(a=a.toJSON()),d[b]=a;return d},parent:t,wrapAll:function(c,b){var a,e,d=this,f=function(){return d};for(b=b||[],a=0,e=c.length;e>a;a++)b[a]=d.wrap(c[a],f);return b},wrap:function(a,e){var d,b=this;return null!==a&&"[object Object]"===D.call(a)&&(d=a instanceof b.type||a instanceof m,d||(a=a instanceof i?a.toJSON():a,a=new b.type(a)),a.parent=e,a.bind(c,function(a){b.trigger(c,{field:a.field,node:a.node,index:a.index,items:a.items||[this],action:a.node?a.action||"itemchange":"itemchange"})})),a},push:function(){var a,d=this.length,b=this.wrapAll(arguments);return a=Nb.apply(this,b),this.trigger(c,{action:"add",index:d,items:b}),a},slice:L,join:Cb,pop:function(){var a=this.length,b=Hb.apply(this);return a&&this.trigger(c,{action:"remove",index:a-1,items:[b]}),b},splice:function(d,h,g){var a,b,f,e=this.wrapAll(L.call(arguments,2));if(a=Bb.apply(this,[d,h].concat(e)),a.length)for(this.trigger(c,{action:"remove",index:d,items:a}),b=0,f=a.length;f>b;b++)a[b].children&&a[b].unbind(c);return g&&this.trigger(c,{action:"add",index:d,items:e}),a},shift:function(){var b=this.length,a=Eb.apply(this);return b&&this.trigger(c,{action:"remove",index:0,items:[a]}),a},unshift:function(){var a,b=this.wrapAll(arguments);return a=Db.apply(this,b),this.trigger(c,{action:"add",index:0,items:b}),a},indexOf:function(d){var a,b,c=this;for(a=0,b=c.length;b>a;a++)if(c[a]===d)return a;return-1},forEach:function(b){for(var a=0,c=this.length;c>a;a++)b(this[a],a,this)},map:function(c){for(var a=0,b=[],d=this.length;d>a;a++)b[a]=c(this[a],a,this);return b},filter:function(d){for(var c,a=0,b=[],e=this.length;e>a;a++)c=this[a],d(c,a,this)&&(b[b.length]=c);return b},find:function(c){for(var b,a=0,d=this.length;d>a;a++)if(b=this[a],c(b,a,this))return b},every:function(c){for(var b,a=0,d=this.length;d>a;a++)if(b=this[a],!c(b,a,this))return!1;return!0},some:function(c){for(var b,a=0,d=this.length;d>a;a++)if(b=this[a],c(b,a,this))return!0;return!1},remove:function(a){this.splice(this.indexOf(a),1)},empty:function(){this.splice(0,this.length)}}),i=u.extend({init:function(e){var d,a,c=this,f=function(){return c};u.fn.init.call(this);for(a in e)d=e[a],"_"!=a.charAt(0)&&(d=c.wrap(d,a,f)),c[a]=d;c.uid=b.guid()},shouldSerialize:function(a){return this.hasOwnProperty(a)&&"_events"!==a&&typeof this[a]!==J&&"uid"!==a},forEach:function(b){for(var a in this)this.shouldSerialize(a)&&b(this[a],a)},toJSON:function(){var a,b,c={};for(b in this)this.shouldSerialize(b)&&(a=this[b],(a instanceof i||a instanceof p)&&(a=a.toJSON()),c[b]=a);return c},"get":function(a){var d,c=this;return c.trigger(O,{field:a}),d="this"===a?c:b.getter(a,!0)(c)},_set:function(e,h){var a,c,f,g=this,d=e.indexOf(".")>=0;if(d)for(a=e.split("."),c="";a.length>1;){if(c+=a.shift(),f=b.getter(c,!0)(g),f instanceof i)return f.set(a.join("."),h),d;c+="."}return b.setter(e)(g,h),d},"set":function(a,e){var d=this,g=a.indexOf(".")>=0,f=b.getter(a,!0)(d);f!==e&&(d.trigger("set",{field:a,value:e})||(g||(e=d.wrap(e,a,function(){return d})),(!d._set(a,e)||a.indexOf("(")>=0||a.indexOf("[")>=0)&&d.trigger(c,{field:a})))},parent:t,wrap:function(a,h,g){var b,d,f=this,e=D.call(a);return null==a||"[object Object]"!==e&&"[object Array]"!==e||(b=a instanceof p,d=a instanceof j,"[object Object]"!==e||d||b?("[object Array]"===e||b||d)&&(b||d||(a=new p(a)),a.parent()!=g()&&a.bind(c,U(f,c,h,!1))):(a instanceof i||(a=new i(a)),a.parent()!=g()&&(a.bind(O,U(f,O,h,!0)),a.bind(c,U(f,c,h,!0)))),a.parent=g),a}}),ab={number:function(a){return b.parseFloat(a)},date:function(a){return b.parseDate(a)},"boolean":function(a){return typeof a===h?"true"===a.toLowerCase():null!=a?!!a:a},string:function(a){return null!=a?a+"":a},"default":function(a){return a}},Lb={string:"",number:0,date:new Date,"boolean":!1,"default":""},m=i.extend({init:function(c){var b=this;(!c||e.isEmptyObject(c))&&(c=e.extend({},b.defaults,c)),i.fn.init.call(b,c),b.dirty=!1,b.idField&&(b.id=b.get(b.idField),b.id===a&&(b.id=b._defaultId))},shouldSerialize:function(a){return i.fn.shouldSerialize.call(this,a)&&"uid"!==a&&!("id"!==this.idField&&"id"===a)&&"dirty"!==a&&"_accessors"!==a},_parse:function(a,d){var b,f=this,e=a,c=f.fields||{};return a=c[a],a||(a=Sb(c,e)),a&&(b=a.parse,!b&&a.type&&(b=ab[a.type.toLowerCase()])),b?b(d):d},_notifyChange:function(b){var a=b.action;("add"==a||"remove"==a)&&(this.dirty=!0)},editable:function(a){return a=(this.fields||{})[a],a?a.editable!==!1:!0},"set":function(b,c,d){var a=this;a.editable(b)&&(c=a._parse(b,c),sb(c,a.get(b))||(a.dirty=!0,i.fn.set.call(a,b,c,d)))},accept:function(c){var b,d,a=this,e=function(){return a};for(b in c)d=c[b],"_"!=b.charAt(0)&&(d=a.wrap(c[b],b,e)),a._set(b,d);a.idField&&(a.id=a.get(a.idField)),a.dirty=!1},isNew:function(){return this.id===this._defaultId}});m.define=function(o,j){j===a&&(j=o,o=m);var e,d,c,n,k,l,q,f,b=g({defaults:{}},j),p={},i=b.id;if(i&&(b.idField=i),b.id&&delete b.id,i&&(b.defaults[i]=b._defaultId=""),"[object Array]"===D.call(b.fields)){for(l=0,q=b.fields.length;q>l;l++)c=b.fields[l],typeof c===h?(p[c]={}):c.field&&(p[c.field]=c);b.fields=p}for(d in b.fields)c=b.fields[d],n=c.type||"default",k=null,f=d,d=typeof c.field===h?c.field:d,c.nullable||(k=b.defaults[f!==d?f:d]=c.defaultValue!==a?c.defaultValue:Lb[n.toLowerCase()]),j.id===d&&(b._defaultId=k),b.defaults[f!==d?f:d]=k,c.parse=c.parse||ab[n];return e=o.extend(b),e.define=function(a){return m.define(e,a)},b.fields&&(e.fields=b.fields,e.idField=b.idField),e},G={selector:function(a){return n(a)?a:E(a)},compare:function(b){var a=this.selector(b);return function(b,c){return b=a(b),c=a(c),null==b&&null==c?0:null==b?-1:null==c?1:b.localeCompare?b.localeCompare(c):b>c?1:c>b?-1:0}},create:function(a){var b=a.compare||this.compare(a.field);return"desc"==a.dir?function(a,c){return b(c,a,!0)}:b},combine:function(a){return function(f,e){var b,d,c=a[0](f,e);for(b=1,d=a.length;d>b;b++)c=c||a[b](f,e);return c}}},rb=g({},G,{asc:function(b){var a=this.selector(b);return function(d,e){var b=a(d),c=a(e);return b&&b.getTime&&c&&c.getTime&&(b=b.getTime(),c=c.getTime()),b===c?d.__position-e.__position:null==b?-1:null==c?1:b.localeCompare?b.localeCompare(c):b>c?1:-1}},desc:function(b){var a=this.selector(b);return function(d,e){var c=a(d),b=a(e);return c&&c.getTime&&b&&b.getTime&&(c=c.getTime(),b=b.getTime()),c===b?d.__position-e.__position:null==c?1:null==b?-1:b.localeCompare?b.localeCompare(c):b>c?1:-1}},create:function(a){return this[a.dir](a.field)}}),R=function(b,e){for(var d=b.length,c=Array(d),a=0;d>a;a++)c[a]=e(b[a],a,b);return c},mb=function(){function b(a){return a.replace(Fb,"\\").replace(Gb,"")}function a(f,c,a,e){var d;return null!=a&&(typeof a===h&&(a=b(a),d=Ib.exec(a),d?(a=new Date(+d[1])):e?(a="'"+a.toLowerCase()+"'",c="("+c+" || '').toLowerCase()"):(a="'"+a+"'")),a.getTime&&(c="("+c+"?"+c+".getTime():"+c+")",a=a.getTime())),c+" "+f+" "+a}return{eq:function(b,c,d){return a("==",b,c,d)},neq:function(b,c,d){return a("!=",b,c,d)},gt:function(b,c,d){return a(">",b,c,d)},gte:function(b,c,d){return a(">=",b,c,d)},lt:function(b,c,d){return a("<",b,c,d)},lte:function(b,c,d){return a("<=",b,c,d)},startswith:function(c,a,d){return d&&(c="("+c+" || '').toLowerCase()",a&&(a=a.toLowerCase())),a&&(a=b(a)),c+".lastIndexOf('"+a+"', 0) == 0"},endswith:function(c,a,d){return d&&(c="("+c+" || '').toLowerCase()",a&&(a=a.toLowerCase())),a&&(a=b(a)),c+".indexOf('"+a+"', "+c+".length - "+(a||"").length+") >= 0"},contains:function(c,a,d){return d&&(c="("+c+" || '').toLowerCase()",a&&(a=a.toLowerCase())),a&&(a=b(a)),c+".indexOf('"+a+"') >= 0"},doesnotcontain:function(c,a,d){return d&&(c="("+c+" || '').toLowerCase()",a&&(a=a.toLowerCase())),a&&(a=b(a)),c+".indexOf('"+a+"') == -1"}}}(),d.filterExpr=function(k){var h,n,c,e,i,j,m=[],o={and:" && ",or:" || "},f=[],g=[],l=k.filters;for(h=0,n=l.length;n>h;h++)c=l[h],i=c.field,j=c.operator,c.filters?(e=d.filterExpr(c),c=e.expression.replace(/__o\[(\d+)\]/g,function(b,a){return a=+a,"__o["+(g.length+a)+"]"}).replace(/__f\[(\d+)\]/g,function(b,a){return a=+a,"__f["+(f.length+a)+"]"}),g.push.apply(g,e.operators),f.push.apply(f,e.fields)):(typeof i===J?(e="__f["+f.length+"](d)",f.push(i)):(e=b.expr(i)),typeof j===J?(c="__o["+g.length+"]("+e+", "+c.value+")",g.push(j)):(c=mb[(j||"eq").toLowerCase()](e,c.value,c.ignoreCase!==a?c.ignoreCase:!0))),m.push(c);return{expression:"("+m.join(o[k.logic])+")",fields:f,operators:g}},lb={"==":"eq",equals:"eq",isequalto:"eq",equalto:"eq",equal:"eq","!=":"neq",ne:"neq",notequals:"neq",isnotequalto:"neq",notequalto:"neq",notequal:"neq","<":"lt",islessthan:"lt",lessthan:"lt",less:"lt","<=":"lte",le:"lte",islessthanorequalto:"lte",lessthanequal:"lte",">":"gt",isgreaterthan:"gt",greaterthan:"gt",greater:"gt",">=":"gte",isgreaterthanorequalto:"gte",greaterthanequal:"gte",ge:"gte",notsubstringof:"doesnotcontain"},d.normalizeFilter=H,d.prototype={toArray:function(){return this.data},range:function(a,b){return new d(this.data.slice(a,a+b))},skip:function(a){return new d(this.data.slice(a))},take:function(a){return new d(this.data.slice(0,a))},select:function(a){return new d(R(this.data,a))},order:function(a,c){var b={dir:c};return a&&(a.compare?(b.compare=a.compare):(b.field=a)),new d(this.data.slice(0).sort(G.create(b)))},orderBy:function(a){return this.order(a,"asc")},orderByDescending:function(a){return this.order(a,"desc")},sort:function(f,g,a){var b,d,c=A(f,g),e=[];if(a=a||G,c.length){for(b=0,d=c.length;d>b;b++)e.push(a.create(c[b]));return this.orderBy({compare:a.combine(e)})}return this},filter:function(a){var c,g,l,b,i,h,f,e,j=this.data,k=[];if(a=H(a),!a||0===a.filters.length)return this;for(b=d.filterExpr(a),h=b.fields,f=b.operators,i=e=Function("d, __f, __o","return "+b.expression),(h.length||f.length)&&(e=function(a){return i(a,h,f)}),c=0,l=j.length;l>c;c++)g=j[c],e(g)&&k.push(g);return new d(k)},group:function(a,e){a=B(a||[]),e=e||this.data;var c,f=this,b=new d(f.data);return a.length>0&&(c=a[0],b=b.groupBy(c).select(function(b){var f=(new d(e)).filter([{field:b.field,operator:"eq",value:b.value,ignoreCase:!1}]);return{field:b.field,value:b.value,items:a.length>1?(new d(b.items)).group(a.slice(1),f.toArray()).toArray():b.items,hasSubgroups:a.length>1,aggregates:f.aggregate(c.aggregates)}})),b},groupBy:function(g){if(w(g)||!this.data.length)return new d([]);var j,h,f,l,a=g.field,i=this._sortForGrouping(a,g.dir||"asc"),m=b.accessor(a),e=m.get(i[0],a),c={field:a,value:e,items:[]},k=[c];for(f=0,l=i.length;l>f;f++)j=i[f],h=m.get(j,a),Pb(e,h)||(e=h,c={field:a,value:e,items:[]},k.push(c)),c.items.push(j);return new d(k)},_sortForGrouping:function(e,f){var a,c,b=this.data;if(!Kb){for(a=0,c=b.length;c>a;a++)b[a].__position=a;for(b=(new d(b)).sort(e,f,rb).toArray(),a=0,c=b.length;c>a;a++)delete b[a].__position;return b}return this.sort(e,f).toArray()},aggregate:function(b){var a,c,d={},e={};if(b&&b.length)for(a=0,c=this.data.length;c>a;a++)Rb(d,b,this.data[a],a,c,e);return d}},nb={sum:function(a,d,c){var b=c.get(d);return o(a)?o(b)&&(a+=b):(a=b),a},count:function(a){return(a||0)+1},average:function(b,f,h,e,g,c){var d=h.get(f);return c.count===a&&(c.count=0),o(b)?o(d)&&(b+=d):(b=d),o(d)&&c.count++,e==g-1&&o(b)&&(b/=c.count),b},max:function(a,d,c){var b=c.get(d);return o(a)||(a=b),b>a&&o(b)&&(a=b),a},min:function(a,d,c){var b=c.get(d);return o(a)||(a=b),a>b&&o(b)&&(a=b),a}},d.process=function(f,c){c=c||{};var j,b=new d(f),e=c.group,k=B(e||[]).concat(A(c.sort||[])),i=c.filter,g=c.skip,h=c.take;return i&&(b=b.filter(i),j=b.toArray().length),k&&(b=b.sort(k),e&&(f=b.toArray())),g!==a&&h!==a&&(b=b.range(g,h)),e&&(b=b.group(e,f)),{total:j,data:b.toArray()}},W=C.extend({init:function(a){this.data=a.data},read:function(a){a.success(this.data)},update:function(a){a.success(a.data)},create:function(a){a.success(a.data)},destroy:function(a){a.success(a.data)}}),Q=C.extend({init:function(a){var c,b=this;a=b.options=g({},b.options,a),M(Mb,function(c,b){typeof a[b]===h&&(a[b]={url:a[b]})}),b.cache=a.cache?x.create(a.cache):{find:t,add:t},c=a.parameterMap,n(a.push)&&(b.push=a.push),b.push||(b.push=z),b.parameterMap=n(c)?c:function(b){var a={};return M(b,function(b,d){b in c&&(b=c[b],r(b)&&(d=b.value(d),b=b.key)),a[b]=d}),a}},options:{parameterMap:z},create:function(a){return K(this.setup(a,cb))},read:function(b){var d,h,c,f=this,g=f.cache;b=f.setup(b,eb),d=b.success||t,h=b.error||t,c=g.find(b.data),c!==a?d(c):(b.success=function(a){g.add(b.data,a),d(a)},e.ajax(b))},update:function(a){return K(this.setup(a,bb))},destroy:function(a){return K(this.setup(a,db))},setup:function(a,e){a=a||{};var c,d=this,b=d.options[e],f=n(b.data)?b.data(a.data):b.data;return a=g(!0,{},b,a),c=g(!0,{},f,a.data),a.data=d.parameterMap(c,e),n(a.url)&&(a.url=a.url(c)),a}}),x=C.extend({init:function(){this._store={}},add:function(b,c){b!==a&&(this._store[I(b)]=c)},find:function(a){return this._store[I(a)]},clear:function(){this._store={}},remove:function(a){delete this._store[I(a)]}}),x.create=function(a){var b={inmemory:function(){return new x}};return r(a)&&n(a.find)?a:a===!0?new x:b[a]()},P=C.extend({init:function(d){var o,j,c,q,n,p,s,f,i,k,e,g,b,a=this;d=d||{};for(o in d)j=d[o],a[o]=typeof j===h?E(j):j;q=d.modelBase||m,r(a.model)&&(a.model=c=q.define(a.model)),a.model&&(n=l(a.data,a),p=l(a.groups,a),s=l(a.serialize,a),f={},i={},k={},e={},g=!1,c=a.model,c.fields&&(M(c.fields,function(a,c){var d;b=a,r(c)&&c.field?(b=c.field):typeof c===h&&(b=c),r(c)&&c.from&&(d=c.from),g=g||d&&d!==a||b!==a,i[a]=E(d||b),k[a]=E(a),f[d||b]=a,e[a]=d||b}),!d.serialize&&g&&(a.serialize=Z(s,c,Ub,k,f,e))),a._dataAccessFunction=n,a.data=Z(n,c,wb,i,f,e),a.groups=Z(p,c,yb,i,f,e))},errors:function(a){return a?a.errors:null},parse:z,data:z,total:function(a){return a.length},groups:z,aggregates:function(){return{}},serialize:function(a){return a}}),j=u.extend({init:function(e){var h,f,d=this;e&&(f=e.data),e=d.options=g({},d.options,e),d._map={},d._prefetch={},d._data=[],d._pristineData=[],d._ranges=[],d._view=[],d._pristineTotal=0,d._destroyed=[],d._pageSize=e.pageSize,d._page=e.page||(e.pageSize?1:a),d._sort=A(e.sort),d._filter=H(e.filter),d._group=B(e.group),d._aggregate=e.aggregate,d._total=e.total,u.fn.init.call(d),d.transport=S.create(e,f),n(d.transport.push)&&d.transport.push({pushCreate:l(d._pushCreate,d),pushUpdate:l(d._pushUpdate,d),pushDestroy:l(d._pushDestroy,d)}),d.reader=new b.data.readers[e.schema.type||"json"](e.schema),h=d.reader.model||{},d._data=d._observe(d._data),d.bind(["push",q,c,y,fb,v,N],e)},options:{data:[],schema:{modelBase:m},serverSorting:!1,serverPaging:!1,serverFiltering:!1,serverGrouping:!1,serverAggregates:!1,batch:!1},_isServerGrouped:function(){var a=this.group()||[];return this.options.serverGrouping&&a.length},_pushCreate:function(a){this._push(a,"pushCreate")},_pushUpdate:function(a){this._push(a,"pushUpdate")},_pushDestroy:function(a){this._push(a,"pushDestroy")},_push:function(b,c){var a=this._readData(b);a||(a=b),this[c](a)},_flatData:function(a){return this._isServerGrouped()?gb(a):a},parent:t,"get":function(d){var a,c,b=this._flatData(this._data);for(a=0,c=b.length;c>a;a++)if(b[a].id==d)return b[a]},getByUid:function(d){var a,c,b=this._flatData(this._data);if(b)for(a=0,c=b.length;c>a;a++)if(b[a].uid==d)return b[a]},indexOf:function(a){return hb(this._data,a)},at:function(a){return this._data[a]},data:function(c){var b=this;return c===a?b._data:(b._data=this._observe(c),b._pristineData=c.slice(0),b._ranges=[],b._addRange(b._data),b._total=b._data.length,b._pristineTotal=b._total,b._process(b._data),a)},view:function(){return this._view},add:function(a){return this.insert(this._data.length,a)},_createNewModel:function(a){return this.reader.model?new this.reader.model(a):new i(a)},insert:function(b,a){return a||(a=b,b=0),a instanceof m||(a=this._createNewModel(a)),this._isServerGrouped()?this._data.splice(b,0,T(this.group(),a)):this._data.splice(b,0,a),a},pushCreate:function(a){var d,b,f,e,c;for(k(a)||(a=[a]),d=[],b=0;a.length>b;b++)f=a[b],e=this.add(f),d.push(e),c=e.toJSON(),this._isServerGrouped()&&(c=T(this.group(),c)),this._pristineData.push(c);d.length&&this.trigger("push",{type:"create",items:d})},pushUpdate:function(b){var e,d,c,f,a;for(k(b)||(b=[b]),e=[],d=0;b.length>d;d++)c=b[d],f=this._createNewModel(c),a=this.get(f.id),a?(e.push(a),a.accept(c),a.trigger("change"),this._updatePristineForModel(a,c)):this.pushCreate(c);e.length&&this.trigger("push",{type:"update",items:e})},pushDestroy:function(a){var c,g,b,f,e,d;k(a)||(a=[a]),c=[],g=this.options.autoSync,this.options.autoSync=!1;try{for(b=0;a.length>b;b++)f=a[b],e=this._createNewModel(f),d=!1,this._eachItem(this._data,function(b){for(var a=0;b.length>a;a++)if(b[a].id===e.id){c.push(b[a]),b.splice(a,1),d=!0;break}}),d&&(this._removePristineForModel(e),this._destroyed.pop())}finally{this.options.autoSync=g}c.length&&this.trigger("push",{type:"destroy",items:c})},remove:function(c){var b,d=this,e=d._isServerGrouped();return this._eachItem(d._data,function(f){return b=xb(f,c),b&&e?(b.isNew&&b.isNew()||d._destroyed.push(b),!0):a}),this._removeModelFromRanges(c),this._updateRangesLength(),c},sync:function(){var b,g,c,a=this,h=[],f=[],i=a._destroyed,d=a._flatData(a._data);if(a.reader.model){for(b=0,g=d.length;g>b;b++)d[b].isNew()?h.push(d[b]):d[b].dirty&&f.push(d[b]);c=a._send("create",h),c.push.apply(c,a._send("update",f)),c.push.apply(c,a._send("destroy",i)),e.when.apply(null,c).then(function(){var b,c;for(b=0,c=arguments.length;c>b;b++)a._accept(arguments[b]);a._change({action:"sync"}),a.trigger(fb)})}},cancelChanges:function(c){var a=this;c instanceof b.data.Model?a._cancelModel(c):(a._destroyed=[],a._data=a._observe(a._pristineData),a.options.serverPaging&&(a._total=a._pristineTotal),a._change())},hasChanges:function(){var a,c,b=this._data;if(this._destroyed.length)return!0;for(a=0,c=b.length;c>a;a++)if(b[a].isNew()||b[a].dirty)return!0;return!1},_accept:function(g){var h,c=this,d=g.models,a=g.response,b=0,i=c._isServerGrouped(),j=c._pristineData,f=g.type;if(c.trigger(v,{response:a,type:f}),a&&!w(a)){if(a=c.reader.parse(a),c._handleCustomErrors(a))return;a=c.reader.data(a),k(a)||(a=[a])}else a=e.map(d,function(a){return a.toJSON()});for("destroy"===f&&(c._destroyed=[]),b=0,h=d.length;h>b;b++)"destroy"!==f?(d[b].accept(a[b]),"create"===f?j.push(i?T(c.group(),d[b]):a[b]):"update"===f&&c._updatePristineForModel(d[b],a[b])):c._removePristineForModel(d[b])},_updatePristineForModel:function(a,c){this._executeOnPristineForModel(a,function(a,d){b.deepExtend(d[a],c)})},_executeOnPristineForModel:function(b,c){this._eachPristineItem(function(e){var d=vb(e,b);return d>-1?(c(d,e),!0):a})},_removePristineForModel:function(a){this._executeOnPristineForModel(a,function(a,b){b.splice(a,1)})},_readData:function(a){var b=this._isServerGrouped()?this.reader.groups:this.reader.data;return b(a)},_eachPristineItem:function(a){this._eachItem(this._pristineData,a)},_eachItem:function(a,b){a&&a.length&&(this._isServerGrouped()?ob(a,b):b(a))},_pristineForModel:function(d){var c,b,e=function(e){return b=vb(e,d),b>-1?(c=e[b],!0):a};return this._eachPristineItem(e),c},_cancelModel:function(b){var a,c=this._pristineForModel(b);this._eachItem(this._data,function(d){a=hb(d,b),-1!=a&&(!b.isNew()&&c?d[a].accept(c):d.splice(a,1))})},_promise:function(f,d,b){var a=this,c=a.transport;return e.Deferred(function(e){a.trigger(y,{type:b}),c[b].call(c,g({success:function(a){e.resolve({response:a,models:d,type:b})},error:function(b,c,d){e.reject(b),a.error(b,c,d)}},f))}).promise()},_send:function(f,b){var a,g,c=this,d=[],e=c.reader.serialize(Vb(b));if(c.options.batch)b.length&&d.push(c._promise({data:{models:e}},b,f));else for(a=0,g=b.length;g>a;a++)d.push(c._promise({data:e[a]},[b[a]],f));return d},read:function(c){var a=this,b=a._params(c);a._queueRequest(b,function(){a.trigger(y,{type:"read"})?a._dequeueRequest():(a.trigger(N),a._ranges=[],a.transport.read({data:b,success:l(a.success,a),error:l(a.error,a)}))})},success:function(c){var b=this,d=b.options;return b.trigger(v,{response:c,type:"read"}),c=b.reader.parse(c),b._handleCustomErrors(c)?(b._dequeueRequest(),a):(b._total=b.reader.total(c),b._pristineTotal=b._total,b._aggregate&&d.serverAggregates&&(b._aggregateResult=b.reader.aggregates(c)),c=b._readData(c),b._pristineData=c.slice(0),b._data=b._observe(c),b._addRange(b._data),b._process(b._data),b._dequeueRequest(),a)},_addRange:function(b){var a=this,c=a._skip||0,d=c+a._flatData(b).length;a._ranges.push({start:c,end:d,data:b}),a._ranges.sort(function(a,b){return a.start-b.start})},error:function(a,c,b){this._dequeueRequest(),this.trigger(v,{}),this.trigger(q,{xhr:a,status:c,errorThrown:b})},_params:function(c){var b=this,a=g({take:b.take(),skip:b.skip(),page:b.page(),pageSize:b.pageSize(),sort:b._sort,filter:b._filter,group:b._group,aggregate:b._aggregate},c);return b.options.serverPaging||(delete a.take,delete a.skip,delete a.page,delete a.pageSize),b.options.serverGrouping?b.reader.model&&a.group&&(a.group=F(a.group,b.reader.model)):delete a.group,b.options.serverFiltering?b.reader.model&&a.filter&&(a.filter=ib(a.filter,b.reader.model)):delete a.filter,b.options.serverSorting?b.reader.model&&a.sort&&(a.sort=F(a.sort,b.reader.model)):delete a.sort,b.options.serverAggregates?b.reader.model&&a.aggregate&&(a.aggregate=F(a.aggregate,b.reader.model)):delete a.aggregate,a},_queueRequest:function(d,c){var b=this;b._requestInProgress?(b._pending={callback:l(c,b),options:d}):(b._requestInProgress=!0,b._pending=a,c())},_dequeueRequest:function(){var a=this;a._requestInProgress=!1,a._pending&&a._queueRequest(a._pending.options,a._pending.callback)},_handleCustomErrors:function(b){if(this.reader.errors){var a=this.reader.errors(b);if(a)return this.trigger(q,{xhr:null,status:"customerror",errorThrown:"custom error",errors:a}),!0}return!1},_observe:function(b){var a=this,d=a.reader.model,e=!1;return d&&b.length&&(e=!(b[0]instanceof d)),b instanceof p?e&&(b.type=a.reader.model,b.wrapAll(b,b)):(b=new p(b,a.reader.model),b.parent=function(){return a.parent()}),a._isServerGrouped()&&ub(b,d),a._changeHandler&&a._data&&a._data instanceof p?a._data.unbind(c,a._changeHandler):(a._changeHandler=l(a._change,a)),b.bind(c,a._changeHandler)},_change:function(b){var e,f,d,a=this,c=b?b.action:"";if("remove"===c)for(e=0,f=b.items.length;f>e;e++)b.items[e].isNew&&b.items[e].isNew()||a._destroyed.push(b.items[e]);!a.options.autoSync||"add"!==c&&"remove"!==c&&"itemchange"!==c?(d=parseInt(a._total||a._pristineTotal,10),"add"===c?(d+=b.items.length):"remove"===c?(d-=b.items.length):"itemchange"===c||"sync"===c||a.options.serverPaging?"sync"===c&&(d=a._pristineTotal=parseInt(a._total,10)):(d=a._pristineTotal),a._total=d,a._process(a._data,b)):a.sync()},_process:function(h,f){var g,b=this,e={};b.options.serverPaging!==!0&&(e.skip=b._skip,e.take=b._take||b._pageSize,e.skip===a&&b._page!==a&&b._pageSize!==a&&(e.skip=(b._page-1)*b._pageSize)),b.options.serverSorting!==!0&&(e.sort=b._sort),b.options.serverFiltering!==!0&&(e.filter=b._filter),b.options.serverGrouping!==!0&&(e.group=b._group),b.options.serverAggregates!==!0&&(e.aggregate=b._aggregate,b._aggregateResult=kb(h,e)),g=d.process(h,e),b._view=g.data,g.total===a||b.options.serverFiltering||(b._total=g.total),f=f||{},f.items=f.items||b._view,b.trigger(c,f)},_mergeState:function(b){var c=this;return b!==a&&(c._pageSize=b.pageSize,c._page=b.page,c._sort=b.sort,c._filter=b.filter,c._group=b.group,c._aggregate=b.aggregate,c._skip=b.skip,c._take=b.take,c._skip===a&&(c._skip=c.skip(),b.skip=c.skip()),c._take===a&&c._pageSize!==a&&(c._take=c._pageSize,b.take=c._take),b.sort&&(c._sort=b.sort=A(b.sort)),b.filter&&(c._filter=b.filter=H(b.filter)),b.group&&(c._group=b.group=B(b.group)),b.aggregate&&(c._aggregate=b.aggregate=Tb(b.aggregate))),b},query:function(f){var e,b=this,g=b.options.serverSorting||b.options.serverPaging||b.options.serverFiltering||b.options.serverGrouping||b.options.serverAggregates;g||(b._data===a||0===b._data.length)&&!b._destroyed.length?b.read(b._mergeState(f)):b.trigger(y,{type:"read"})||(b.trigger(N),e=d.process(b._data,b._mergeState(f)),b.options.serverFiltering||(b._total=e.total!==a?e.total:b._data.length),b._view=e.data,b._aggregateResult=kb(b._data,f),b.trigger(v,{}),b.trigger(c,{items:e.data}))},fetch:function(b){var a=this;return e.Deferred(function(d){var f=function(c){a.unbind(q,e),d.resolve(),b&&b.call(a,c)},e=function(a){d.reject(a)};a.one(c,f),a.one(q,e),a._query()}).promise()},_query:function(b){var a=this;a.query(g({},{page:a.page(),pageSize:a.pageSize(),sort:a.sort(),filter:a.filter(),group:a.group(),aggregate:a.aggregate()},b))},next:function(d){var b=this,c=b.page(),e=b.total();return d=d||{},!c||e&&c+1>b.totalPages()?a:(b._skip=c*b.take(),c+=1,d.page=c,b._query(d),c)},prev:function(d){var b=this,c=b.page();return d=d||{},c&&1!==c?(b._skip=b._skip-b.take(),c-=1,d.page=c,b._query(d),c):a},page:function(b){var d,c=this;return b!==a?(b=f.max(f.min(f.max(b,1),c.totalPages()),1),c._query({page:b}),a):(d=c.skip(),d!==a?f.round((d||0)/(c.take()||1))+1:a)},pageSize:function(b){var c=this;return b!==a?(c._query({pageSize:b,page:1}),a):c.take()},sort:function(b){var c=this;return b!==a?(c._query({sort:b}),a):c._sort},filter:function(b){var c=this;return b===a?c._filter:(c._query({filter:b,page:1}),a)},group:function(b){var c=this;return b!==a?(c._query({group:b}),a):c._group},total:function(){return parseInt(this._total||0,10)},aggregate:function(b){var c=this;return b!==a?(c._query({aggregate:b}),a):c._aggregate},aggregates:function(){return this._aggregateResult},totalPages:function(){var a=this,b=a.pageSize()||a.total();return f.ceil((a.total()||0)/b)},inRange:function(b,d){var a=this,c=f.min(b+d,a.total());return!a.options.serverPaging&&a._data.length>0?!0:a._findRange(b,c).length>0},lastRange:function(){var a=this._ranges;return a[a.length-1]||{start:0,end:0,data:[]}},firstItemUid:function(){var a=this._ranges;return a.length&&a[0].data.length&&a[0].data[0].uid},range:function(d,c){var b,g,e,h,i,l,k,j;if(d=f.min(d||0,this.total()),b=this,g=f.max(f.floor(d/c),0)*c,e=f.min(g+c,b.total()),h=b._findRange(d,f.min(d+c,b.total())),h.length){b._skip=d>b.skip()?f.min(e,(b.totalPages()-1)*b.take()):g,b._take=c,i=b.options.serverPaging,l=b.options.serverSorting,k=b.options.serverFiltering,j=b.options.serverAggregates;try{b.options.serverPaging=!0,b._isServerGrouped()||b.group()&&b.group().length||(b.options.serverSorting=!0),b.options.serverFiltering=!0,b.options.serverPaging=!0,b.options.serverAggregates=!0,i&&(b._data=h=b._observe(h)),b._process(h)}finally{b.options.serverPaging=i,b.options.serverSorting=l,b.options.serverFiltering=k,b.options.serverAggregates=j}}else c!==a&&(b._rangeExists(g,e)?d>g&&b.prefetch(e,c,function(){b.range(d,c)}):b.prefetch(g,c,function(){d>g&&e<b.total()&&!b._rangeExists(e,f.min(e+c,b.total()))?b.prefetch(e,c,function(){b.range(d,c)}):b.range(d,c)}))},_findRange:function(e,l){var b,h,n,o,k,p,i,j,m,f,q,t,c=this,s=c._ranges,r=[],g=c.options,u=g.serverSorting||g.serverPaging||g.serverFiltering||g.serverGrouping||g.serverAggregates;for(h=0,q=s.length;q>h;h++)if(b=s[h],e>=b.start&&b.end>=e){for(f=0,n=h;q>n;n++)if(b=s[n],m=c._flatData(b.data),m.length&&e+f>=b.start&&(p=b.data,i=b.end,u||(t=B(c.group()||[]).concat(A(c.sort()||[])),j=d.process(b.data,{sort:t,filter:c.filter()}),m=p=j.data,j.total!==a&&(i=j.total)),o=0,e+f>b.start&&(o=e+f-b.start),k=m.length,i>l&&(k-=i-l),f+=k-o,r=c._mergeGroups(r,p,o,k),b.end>=l&&f==l-e))return r;break}return[]},_mergeGroups:function(a,f,c,e){if(this._isServerGrouped()){var b,d=f.toJSON();return a.length&&(b=a[a.length-1]),Y(b,d,c,e),a.concat(d)}return a.concat(f.slice(c,e))},skip:function(){var b=this;return b._skip===a?b._page!==a?(b._page-1)*(b.take()||1):a:b._skip},take:function(){return this._take||this._pageSize},_prefetchSuccessHandler:function(b,e,d){var a=this;return function(g){var h,j,i,k=!1,f={start:b,end:e,data:[]};if(a._dequeueRequest(),a.trigger(v,{response:g,type:"read"}),g=a.reader.parse(g),i=a._readData(g),i.length){for(h=0,j=a._ranges.length;j>h;h++)if(a._ranges[h].start===b){k=!0,f=a._ranges[h];break}k||a._ranges.push(f)}f.data=a._observe(i),f.end=f.start+a._flatData(f.data).length,a._ranges.sort(function(a,b){return a.start-b.start}),a._total=a.reader.total(g),d&&i.length?d():a.trigger(c,{})}},prefetch:function(b,c,d){var a=this,e=f.min(b+c,a.total()),g={take:c,skip:b,page:b/c+1,pageSize:c,sort:a._sort,filter:a._filter,group:a._group,aggregate:a._aggregate};a._rangeExists(b,e)?d&&d():(clearTimeout(a._timeout),a._timeout=setTimeout(function(){a._queueRequest(g,function(){a.trigger(y,{type:"read"})?a._dequeueRequest():a.transport.read({data:a._params(g),success:a._prefetchSuccessHandler(b,e,d)})})},100))},_rangeExists:function(d,f){var a,c,e=this,b=e._ranges;for(a=0,c=b.length;c>a;a++)if(d>=b[a].start&&b[a].end>=f)return!0;return!1},_removeModelFromRanges:function(f){var e,b,d,a,c;for(a=0,c=this._ranges.length;c>a&&(d=this._ranges[a],this._eachItem(d.data,function(a){e=xb(a,f),e&&(b=!0)}),!b);a++);},_updateRangesLength:function(){var a,c,b,e,d=0;for(b=0,e=this._ranges.length;e>b;b++)a=this._ranges[b],a.start=a.start-d,c=this._flatData(a.data).length,d=a.end-c,a.end=a.start+c}}),S={},S.create=function(c,e){var d,a=c.transport;return a?(a.read=typeof a.read===h?{url:a.read}:a.read,c.type&&(b.data.transports[c.type]&&!r(b.data.transports[c.type])?(d=new b.data.transports[c.type](g(a,{data:e}))):(a=g(!0,{},b.data.transports[c.type],a)),c.schema=g(!0,{},b.data.schemas[c.type],c.schema)),d||(d=n(a.read)?a:new Q(a))):(d=new W({data:c.data})),d},j.create=function(d){(k(d)||d instanceof p)&&(d={data:d});var h,m,f,a=d||{},e=a.data,c=a.fields,l=a.table,n=a.select,i={};if(e||!c||a.transport||(l?(e=Qb(l,c)):n&&(e=Ob(n,c))),b.data.Model&&c&&(!a.schema||!a.schema.model)){for(h=0,m=c.length;m>h;h++)f=c[h],f.type&&(i[f.field]=f);w(i)||(a.schema=g(!0,a.schema,{model:{fields:i}}))}return a.data=e,l=null,a.table=null,a instanceof j?a:new j(a)},X=m.define({init:function(d){var a=this,c=a.hasChildren||d&&d.hasChildren,f="items",e={};b.data.Model.fn.init.call(a,d),typeof a.children===h&&(f=a.children),e={schema:{data:f,model:{hasChildren:c,id:a.idField}}},typeof a.children!==h&&g(e,a.children),e.data=d,c||(c=e.schema.data),typeof c===h&&(c=b.getter(c)),n(c)&&(a.hasChildren=!!c.call(a,a)),a._childrenOptions=e,a.hasChildren&&a._initChildren(),a._loaded=!(!d||!d[f]&&!d._loaded)},_initChildren:function(){var b,e,d,a=this;a.children instanceof s||(b=a.children=new s(a._childrenOptions),e=b.transport,d=e.parameterMap,e.parameterMap=function(b,c){return b[a.idField||"id"]=a.id,d&&(b=d(b,c)),b},b.parent=function(){return a},b.bind(c,function(b){b.node=b.node||a,a.trigger(c,b)}),b.bind(q,function(b){var c=a.parent();c&&(b.node=b.node||a,c.trigger(q,b))}),a._updateChildrenField())},append:function(a){this._initChildren(),this.loaded(!0),this.children.add(a)},hasChildren:!1,level:function(){for(var a=this.parentNode(),b=0;a&&a.parentNode;)b++,a=a.parentNode?a.parentNode():null;return b},_updateChildrenField:function(){var a=this._childrenOptions.schema.data;this[a||"items"]=this.children.data()},_childrenLoaded:function(){this._loaded=!0,this._updateChildrenField()},load:function(){var b,d={},e="_query";this.hasChildren?(this._initChildren(),b=this.children,d[this.idField||"id"]=this.id,this._loaded||(b._data=a,e="read"),b.one(c,l(this._childrenLoaded,this)),b[e](d)):this.loaded(!0)},parentNode:function(){var a=this.parent();return a.parent()},loaded:function(b){return b===a?this._loaded:(this._loaded=b,a)},shouldSerialize:function(a){return m.fn.shouldSerialize.call(this,a)&&"children"!==a&&"_loaded"!==a&&"hasChildren"!==a&&"_childrenOptions"!==a}}),s=j.extend({init:function(a){var b=X.define({children:a});j.fn.init.call(this,g(!0,{},{schema:{modelBase:b,model:b}},a)),this._attachBubbleHandlers()},_attachBubbleHandlers:function(){var a=this;a._data.bind(q,function(b){a.trigger(q,b)})},remove:function(c){var d,a=c.parentNode(),b=this;return a&&a._initChildren&&(b=a.children),d=j.fn.remove.call(b,c),a&&!b.data().length&&(a.hasChildren=!1),d},success:pb("success"),data:pb("data"),insert:function(b,c){var a=this.parent();return a&&a._initChildren&&(a.hasChildren=!0,a._initChildren()),j.fn.insert.call(this,b,c)},_find:function(e,g){var b,f,a,d,c;if(a=j.fn[e].call(this,g))return a;if(d=this._flatData(this.data()))for(b=0,f=d.length;f>b;b++)if(c=d[b].children,c instanceof s&&(a=c[e](g)))return a},"get":function(a){return this._find("get",a)},getByUid:function(a){return this._find("getByUid",a)}}),s.create=function(b){b=b&&b.push?{data:b}:b;var a=b||{},c=a.data,e=a.fields,d=a.list;return c&&c._dataSource?c._dataSource:(c||!e||a.transport||d&&(c=jb(d,e)),a.data=c,a instanceof s?a:new s(a))},V=b.Observable.extend({init:function(a,e,c){b.Observable.fn.init.call(this),this._prefetching=!1,this.dataSource=a,this.prefetch=!c;var d=this;a.bind("change",function(){d._change()}),this._syncWithDataSource(),this.setViewSize(e)},setViewSize:function(a){this.viewSize=a,this._recalculate()},at:function(b){var d,e,c=this.pageSize;return b>=this.total()?(this.trigger("endreached",{index:b}),a):this.useRanges?(this.useRanges&&((this.dataOffset>b||b>this.skip+c)&&(e=Math.floor(b/c)*c,this.range(e)),b===this.prefetchThreshold&&this._prefetch(),b===this.midPageThreshold?this.range(this.nextMidRange):b===this.nextPageThreshold?this.range(this.nextFullRange):b===this.pullBackThreshold&&(this.offset===this.skip?this.range(this.previousMidRange):this.range(this.previousFullRange)),d=this.dataSource.at(b-this.dataOffset)),d===a&&this.trigger("endreached",{index:b}),d):this.dataSource.view()[b]},indexOf:function(a){return this.dataSource.data().indexOf(a)+this.dataOffset},total:function(){return parseInt(this.dataSource.total(),10)},next:function(){var a=this,b=a.pageSize,c=a.skip-a.viewSize,d=f.max(f.floor(c/b),0)*b+b;this.offset=c,this.dataSource.prefetch(d,b,function(){a._goToRange(c,!0)})},range:function(a){if(this.offset!==a){var e=this,b=this.pageSize,d=f.max(f.floor(a/b),0)*b+b,c=this.dataSource;this.offset=a,this._recalculate(),c.inRange(a,b)?this._goToRange(a):this.prefetch&&c.prefetch(d,b,function(){e._goToRange(a,!0)})}},syncDataSource:function(){var a=this.offset;this.offset=null,this.range(a)},destroy:function(){this.unbind()},_prefetch:function(){var c=this,a=this.pageSize,b=this.skip+a,d=this.dataSource;d.inRange(b,a)||this._prefetching||!this.prefetch||(this._prefetching=!0,this.trigger("prefetching",{skip:b,take:a}),d.prefetch(b,a,function(){c._prefetching=!1,c.trigger("prefetched",{skip:b,take:a})}))},_goToRange:function(a,b){this.offset===a&&(this.dataOffset=a,this._expanding=b,this.dataSource.range(a,this.pageSize))},_change:function(){var a=this.dataSource,b=a.firstItemUid();this.length=this.useRanges?a.lastRange().end:a.view().length,this._firstItemUid===b&&this.useRanges||(this._syncWithDataSource(),this._recalculate(),this.trigger("reset",{offset:this.offset})),this.trigger("resize"),this._expanding&&this.trigger("expand"),delete this._expanding},_syncWithDataSource:function(){var a=this.dataSource;this._firstItemUid=a.firstItemUid(),this.dataOffset=this.offset=a.skip()||0,this.pageSize=a.pageSize(),this.useRanges=a.options.serverPaging},_recalculate:function(){var b=this.pageSize,d=this.offset,c=this.viewSize,a=Math.ceil(d/b)*b;this.skip=a,this.midPageThreshold=a+b-1,this.nextPageThreshold=a+c-1,this.prefetchThreshold=a+Math.floor(b/3*2),this.pullBackThreshold=this.offset-1,this.nextMidRange=a+b-c,this.nextFullRange=a,this.previousMidRange=d-c,this.previousFullRange=a-b}}),zb=b.Observable.extend({init:function(c,d){var a=this;b.Observable.fn.init.call(a),this.dataSource=c,this.batchSize=d,this._total=0,this.buffer=new V(c,3*d),this.buffer.bind({endreached:function(b){a.trigger("endreached",{index:b.index})},prefetching:function(b){a.trigger("prefetching",{skip:b.skip,take:b.take})},prefetched:function(b){a.trigger("prefetched",{skip:b.skip,take:b.take})},reset:function(){a._total=0,a.trigger("reset")},resize:function(){a._total=Math.ceil(this.length/a.batchSize),a.trigger("resize",{total:a.total(),offset:this.offset})}})},syncDataSource:function(){this.buffer.syncDataSource()},at:function(h){var d,c,b=this.buffer,e=h*this.batchSize,g=this.batchSize,f=[];for(b.offset>e&&b.at(b.offset-1),c=0;g>c&&(d=b.at(e+c),d!==a);c++)f.push(d);return f},total:function(){return this._total},destroy:function(){this.buffer.destroy(),this.unbind()}}),g(!0,b.data,{readers:{json:P},Query:d,DataSource:j,HierarchicalDataSource:s,Node:X,ObservableObject:i,ObservableArray:p,LocalTransport:W,RemoteTransport:Q,Cache:x,DataReader:P,Model:m,Buffer:V,BatchBuffer:zb})})(window.kendo.jQuery);(function(){kendo.data.transports.signalr=kendo.data.RemoteTransport.extend({init:function(b){var a,d=b&&b.signalr?b.signalr:{},c=d.promise;if(!c)throw Error('The "promise" option must be set.');if("function"!=typeof c.done||"function"!=typeof c.fail)throw Error('The "promise" option must be a Promise.');if(this.promise=c,a=d.hub,!a)throw Error('The "hub" option must be set.');if("function"!=typeof a.on||"function"!=typeof a.invoke)throw Error('The "hub" option is not a valid SignalR hub proxy.');this.hub=a,kendo.data.RemoteTransport.fn.init.call(this,b)},push:function(b){var a=this.options.signalr.client||{};a.create&&this.hub.on(a.create,b.pushCreate),a.update&&this.hub.on(a.update,b.pushUpdate),a.destroy&&this.hub.on(a.destroy,b.pushDestroy)},_crud:function(b,a){var d,c,f=this.hub,e=this.options.signalr.server;if(!e||!e[a])throw Error(kendo.format('The "server.{0}" option must be set.',a));d=[e[a]],c=this.parameterMap(b.data,a),$.isEmptyObject(c)||d.push(c),this.promise.done(function(){f.invoke.apply(f,d).done(b.success).fail(b.error)})},read:function(a){this._crud(a,"read")},create:function(a){this._crud(a,"create")},update:function(a){this._crud(a,"update")},destroy:function(a){this._crud(a,"destroy")}})})();(function(d,j){function A(c){var a,f,e,d,b=[];for(a=0,f=c.length;f>a;a++)if(c[a].hasSubgroups)b=b.concat(A(c[a].items));else for(e=c[a].items,d=0;e.length>d;d++)b.push(e[d]);return b}function K(c,d){var b=a.initWidget(c,{},d);return b?new u(b):j}function D(i){var c,h,b,d,f,a,e,g={};for(e=i.match(H),c=0,h=e.length;h>c;c++)b=e[c],d=b.indexOf(":"),f=b.substring(0,d),a=b.substring(d+1),"{"==a.charAt(0)&&(a=D(a)),g[f]=a;return g}function p(b,e,d){var a,c={};for(a in b)c[a]=new d(e,b[a]);return c}function l(f,m,k,g){var d,c,h,r=f.getAttribute("data-"+a.ns+"role"),b=f.getAttribute("data-"+a.ns+"bind"),n=f.children,o=[],q=!0,j={};if(g=g||[m],(r||b)&&G(f),r&&(h=K(f,k)),b&&(b=D(b.replace(E,"")),h||(j=a.parseOptions(f,{textField:"",valueField:"",template:"",valueUpdate:e,valuePrimitive:!1,autoBind:!0}),j.roles=k,h=new t(f,j)),h.source=m,c=p(b,g,i),j.template&&(c.template=new z(g,"",j.template)),c.click&&(b.events=b.events||{},b.events.click=b.click,delete c.click),c.source&&(q=!1),b.attr&&(c.attr=p(b.attr,g,i)),b.style&&(c.style=p(b.style,g,i)),b.events&&(c.events=p(b.events,g,C)),h.bind(c)),h&&(f.kendoBindingTarget=h),q&&n){for(d=0;n.length>d;d++)o[d]=n[d];for(d=0;o.length>d;d++)l(o[d],m,k,g)}}function F(c,e){var b,g,f,h=a.rolesFromNamespaces([].slice.call(arguments,2));for(e=a.observable(e),c=d(c),b=0,g=c.length;g>b;b++)f=c[b],1===f.nodeType&&l(f,e,h)}function G(a){var b=a.kendoBindingTarget;b&&(b.destroy(),y?delete a.kendoBindingTarget:a.removeAttribute?a.removeAttribute("kendoBindingTarget"):(a.kendoBindingTarget=null))}function o(a){G(a),v(a)}function v(d){var a,c,b=d.children;if(b)for(a=0,c=b.length;c>a;a++)o(b[a])}function I(b){var a,c;for(b=d(b),a=0,c=b.length;c>a;a++)o(b[a])}function J(c,d){var b=c.element,a=b[0].kendoBindingTarget;a&&F(b,a.source,d)}var q,i,C,z,b,s,t,u,H,E,a=window.kendo,r=a.Observable,h=a.data.ObservableObject,g=a.data.ObservableArray,w={}.toString,c={},x=a.Class,k=d.proxy,f="value",n="source",B="events",m="checked",y=!0,e="change";!function(){var a=document.createElement("a");a.innerText!==j?(q="innerText"):a.textContent!==j&&(q="textContent");try{delete a.test}catch(b){y=!1}}(),i=r.extend({init:function(b,c){var a=this;r.fn.init.call(a),a.source=b[0],a.parents=b,a.path=c,a.dependencies={},a.dependencies[c]=!0,a.observable=a.source instanceof r,a._access=function(b){a.dependencies[b.field]=!0},a.observable&&(a._change=function(b){a.change(b)},a.source.bind(e,a._change))},_parents:function(){var c,a=this.parents,b=this.get();return b&&"function"==typeof b.parent&&(c=b.parent(),d.inArray(c,a)<0&&(a=[c].concat(a))),a},change:function(c){var d,a,f=c.field,b=this;if("this"===b.path)b.trigger(e,c);else for(d in b.dependencies)if(0===d.indexOf(f)&&(a=d.charAt(f.length),!a||"."===a||"["===a)){b.trigger(e,c);break}},start:function(a){a.bind("get",this._access)},stop:function(a){a.unbind("get",this._access)},"get":function(){var b=this,a=b.source,f=0,d=b.path,c=a;if(!b.observable)return c;for(b.start(b.source),c=a.get(d);c===j&&a;)a=b.parents[++f],a instanceof h&&(c=a.get(d));if(c===j)for(a=b.source;c===j&&a;)a=a.parent(),a instanceof h&&(c=a.get(d));return"function"==typeof c&&(f=d.lastIndexOf("."),f>0&&(a=a.get(d.substring(0,f))),b.start(a),c=a!==b.source?c.call(a,b.source):c.call(a),b.stop(a)),a&&a!==b.source&&(b.currentSource=a,a.unbind(e,b._change).bind(e,b._change)),b.stop(b.source),c},"set":function(c){var b=this.currentSource||this.source,d=a.getter(this.path)(b);"function"==typeof d?b!==this.source?d.call(b,this.source,c):d.call(b,c):b.set(this.path,c)},destroy:function(){this.observable&&this.source.unbind(e,this._change)}}),C=i.extend({"get":function(){for(var a=this.source,c=this.path,d=0,b=a.get(c);!b&&a;)a=this.parents[++d],a instanceof h&&(b=a.get(c));return k(b,a)}}),z=i.extend({init:function(b,d,c){var a=this;i.fn.init.call(a,b,d),a.template=c},render:function(c){var b;return this.start(this.source),b=a.render(this.template,c),this.stop(this.source),b}}),b=x.extend({init:function(a,c,b){this.element=a,this.bindings=c,this.options=b},bind:function(a,b){var c=this;a=b?a[b]:a,a.bind(e,function(a){c.refresh(b||a)}),c.refresh(b)},destroy:function(){}}),c.attr=b.extend({refresh:function(a){this.element.setAttribute(a,this.bindings.attr[a].get())}}),c.style=b.extend({refresh:function(a){this.element.style[a]=this.bindings.style[a].get()||""}}),c.enabled=b.extend({refresh:function(){this.bindings.enabled.get()?this.element.removeAttribute("disabled"):this.element.setAttribute("disabled","disabled")}}),c.readonly=b.extend({refresh:function(){this.bindings.readonly.get()?this.element.setAttribute("readonly","readonly"):this.element.removeAttribute("readonly")}}),c.disabled=b.extend({refresh:function(){this.bindings.disabled.get()?this.element.setAttribute("disabled","disabled"):this.element.removeAttribute("disabled")}}),c.events=b.extend({init:function(a,d,c){b.fn.init.call(this,a,d,c),this.handlers={}},refresh:function(a){var e=d(this.element),c=this.bindings.events[a],b=this.handlers[a];b&&e.off(a,b),b=this.handlers[a]=c.get(),e.on(a,c.source,b)},destroy:function(){var a,b=d(this.element);for(a in this.handlers)b.off(a,this.handlers[a])}}),c.text=b.extend({refresh:function(){var a=this.bindings.text.get();null==a&&(a=""),this.element[q]=a}}),c.visible=b.extend({refresh:function(){this.element.style.display=this.bindings.visible.get()?"":"none"}}),c.invisible=b.extend({refresh:function(){this.element.style.display=this.bindings.invisible.get()?"none":""}}),c.html=b.extend({refresh:function(){this.element.innerHTML=this.bindings.html.get()}}),c.value=b.extend({init:function(f,c,a){b.fn.init.call(this,f,c,a),this._change=k(this.change,this),this.eventName=a.valueUpdate||e,d(this.element).on(this.eventName,this._change),this._initChange=!1},change:function(){var b,c;this._initChange=this.eventName!=e,b=this.element.value,c=this.element.type,"date"==c?(b=a.parseDate(b,"yyyy-MM-dd")):"datetime-local"==c?(b=a.parseDate(b,["yyyy-MM-ddTHH:mm:ss","yyyy-MM-ddTHH:mm"])):"number"==c&&(b=a.parseFloat(b)),this.bindings[f].set(b),this._initChange=!1},refresh:function(){var b,c;this._initChange||(b=this.bindings[f].get(),null==b&&(b=""),c=this.element.type,"date"==c?(b=a.toString(b,"yyyy-MM-dd")):"datetime-local"==c&&(b=a.toString(b,"yyyy-MM-ddTHH:mm:ss")),this.element.value=b),this._initChange=!1},destroy:function(){d(this.element).off(this.eventName,this._change)}}),c.source=b.extend({init:function(e,f,d){b.fn.init.call(this,e,f,d);var c=this.bindings.source.get();c instanceof a.data.DataSource&&d.autoBind!==!1&&c.fetch()},refresh:function(b){var c=this,d=c.bindings.source.get();d instanceof g||d instanceof a.data.DataSource?(b=b||{},"add"==b.action?c.add(b.index,b.items):"remove"==b.action?c.remove(b.index,b.items):"itemchange"!=b.action&&c.render()):c.render()},container:function(){var a=this.element;return"table"==a.nodeName.toLowerCase()&&(a.tBodies[0]||a.appendChild(document.createElement("tbody")),a=a.tBodies[0]),a},template:function(){var b=this.options,c=b.template,d=this.container().nodeName.toLowerCase();return c||(c="select"==d?b.valueField||b.textField?a.format('<option value="#:{0}#">#:{1}#</option>',b.valueField||b.textField,b.textField||b.valueField):"<option>#:data#</option>":"tbody"==d?"<tr><td>#:data#</td></tr>":"ul"==d||"ol"==d?"<li>#:data#</li>":"#:data#",c=a.template(c)),c},add:function(k,c){var h,b,i,f,g=this.container(),e=g.cloneNode(!1),j=g.children[k];if(d(e).html(a.render(this.template(),c)),e.children.length)for(h=this.bindings.source._parents(),b=0,i=c.length;i>b;b++)f=e.children[0],g.insertBefore(f,j||null),l(f,c[b],this.options.roles,[c[b]].concat(h))},remove:function(d,e){for(var a,c=this.container(),b=0;e.length>b;b++)a=c.children[d],o(a),c.removeChild(a)},render:function(){var h,c,f,b=this.bindings.source.get(),e=this.container(),i=this.template();if(b instanceof a.data.DataSource&&(b=b.view()),b instanceof g||"[object Array]"===w.call(b)||(b=[b]),this.bindings.template){if(v(e),d(e).html(this.bindings.template.render(b)),e.children.length)for(h=this.bindings.source._parents(),c=0,f=b.length;f>c;c++)l(e.children[c],b[c],this.options.roles,[b[c]].concat(h))}else d(e).html(a.render(i,b))}}),c.input={checked:b.extend({init:function(e,c,a){b.fn.init.call(this,e,c,a),this._change=k(this.change,this),d(this.element).change(this._change)},change:function(){var b,c,d=this.element,a=this.value();"radio"==d.type?this.bindings[m].set(a):"checkbox"==d.type&&(b=this.bindings[m].get(),b instanceof g?(a=this.element.value,"on"!==a&&"off"!==a&&(c=b.indexOf(a),c>-1?b.splice(c,1):b.push(a))):this.bindings[m].set(a))},refresh:function(){var a=this.bindings[m].get(),c=a,b=this.element;"checkbox"==b.type?(c instanceof g&&(a=this.element.value,c.indexOf(a)>=0&&(a=!0)),b.checked=a===!0):"radio"==b.type&&null!=a&&b.value===""+a&&(b.checked=!0)},value:function(){var a=this.element,b=a.value;return"checkbox"==a.type&&(b=a.checked),b},destroy:function(){d(this.element).off(e,this._change)}})},c.select={value:b.extend({init:function(e,c,a){b.fn.init.call(this,e,c,a),this._change=k(this.change,this),d(this.element).change(this._change)},change:function(){var i,e,d,a,b,j,c=[],l=this.element,k=this.options.valueField||this.options.textField,m=this.options.valuePrimitive;for(b=0,j=l.options.length;j>b;b++)e=l.options[b],e.selected&&(a=e.attributes.value,a=a&&a.specified?e.value:e.text,c.push(a));if(k)for(i=this.bindings.source.get(),d=0;c.length>d;d++)for(b=0,j=i.length;j>b;b++)if(i[b].get(k)==c[d]){c[d]=i[b];break}a=this.bindings[f].get(),a instanceof g?a.splice.apply(a,[0,a.length].concat(c)):m||!(a instanceof h)&&k?this.bindings[f].set(c[0].get(k)):this.bindings[f].set(c[0])},refresh:function(){var b,i,d,j=this.element,e=j.options,a=this.bindings[f].get(),c=a,k=this.options.valueField||this.options.textField,l=!1;for(c instanceof g||(c=new g([a])),j.selectedIndex=-1,d=0;c.length>d;d++)for(a=c[d],k&&a instanceof h&&(a=a.get(k)),b=0;e.length>b;b++)i=e[b].value,""===i&&""!==a&&(i=e[b].text),i==a&&(e[b].selected=!0,l=!0)},destroy:function(){d(this.element).off(e,this._change)}})},c.widget={events:b.extend({init:function(a,d,c){b.fn.init.call(this,a.element[0],d,c),this.widget=a,this.handlers={}},refresh:function(a){var c=this.bindings.events[a],b=this.handlers[a];b&&this.widget.unbind(a,b),b=c.get(),this.handlers[a]=function(a){a.data=c.source,b(a),a.data===c.source&&delete a.data},this.widget.bind(a,this.handlers[a])},destroy:function(){var a;for(a in this.handlers)this.widget.unbind(a,this.handlers[a])}}),checked:b.extend({init:function(a,d,c){b.fn.init.call(this,a.element[0],d,c),this.widget=a,this._change=k(this.change,this),this.widget.bind(e,this._change)},change:function(){this.bindings[m].set(this.value())},refresh:function(){this.widget.check(this.bindings[m].get()===!0)},value:function(){var b=this.element,a=b.value;return("on"==a||"off"==a)&&(a=b.checked),a},destroy:function(){this.widget.unbind(e,this._change)}}),visible:b.extend({init:function(a,d,c){b.fn.init.call(this,a.element[0],d,c),this.widget=a},refresh:function(){var a=this.bindings.visible.get();this.widget.wrapper[0].style.display=a?"":"none"}}),invisible:b.extend({init:function(a,d,c){b.fn.init.call(this,a.element[0],d,c),this.widget=a},refresh:function(){var a=this.bindings.invisible.get();this.widget.wrapper[0].style.display=a?"none":""}}),enabled:b.extend({init:function(a,d,c){b.fn.init.call(this,a.element[0],d,c),this.widget=a},refresh:function(){this.widget.enable&&this.widget.enable(this.bindings.enabled.get())}}),disabled:b.extend({init:function(a,d,c){b.fn.init.call(this,a.element[0],d,c),this.widget=a},refresh:function(){this.widget.enable&&this.widget.enable(!this.bindings.disabled.get())}}),source:b.extend({init:function(c,e,d){var a=this;b.fn.init.call(a,c.element[0],e,d),a.widget=c,a._dataBinding=k(a.dataBinding,a),a._dataBound=k(a.dataBound,a),a._itemChange=k(a.itemChange,a)},itemChange:function(a){l(a.item[0],a.data,this._ns(a.ns),[a.data].concat(this.bindings.source._parents()))},dataBinding:function(){var a,c,d=this.widget,b=d.items();for(a=0,c=b.length;c>a;a++)o(b[a])},_ns:function(c){c=c||a.ui;var b=[a.ui,a.dataviz.ui,a.mobile.ui];return b.splice(d.inArray(c,b),1),b.unshift(c),a.rolesFromNamespaces(b)},dataBound:function(i){var a,c,e,d=this.widget,f=d.items(),g=d.dataSource,b=g.view(),h=g.group()||[];if(f.length)for(h.length&&(b=A(b)),e=this.bindings.source._parents(),a=0,c=b.length;c>a;a++)l(f[a],b[a],this._ns(i.ns),[b[a]].concat(e))},refresh:function(e){var c,d=this,b=d.widget;e=e||{},e.action||(d.destroy(),b.bind("dataBinding",d._dataBinding),b.bind("dataBound",d._dataBound),b.bind("itemChange",d._itemChange),c=d.bindings.source.get(),b.dataSource instanceof a.data.DataSource&&b.dataSource!=c&&(c instanceof a.data.DataSource?b.setDataSource(c):c&&c._dataSource?b.setDataSource(c._dataSource):b.dataSource.data(c)))},destroy:function(){var a=this.widget;a.unbind("dataBinding",this._dataBinding),a.unbind("dataBound",this._dataBound),a.unbind("itemChange",this._itemChange)}}),value:b.extend({init:function(f,i,c){b.fn.init.call(this,f.element[0],i,c),this.widget=f,this._change=d.proxy(this.change,this),this.widget.first(e,this._change);var a=this.bindings.value.get();this._valueIsObservableObject=!c.valuePrimitive&&(null==a||a instanceof h),this._valueIsObservableArray=a instanceof g,this._initChange=!1},change:function(){var e,m,g,f,h,l,d,b=this.widget.value(),i=this.options.dataValueField||this.options.dataTextField,k="[object Array]"===w.call(b),j=this._valueIsObservableObject,c=[];if(this._initChange=!0,i)if(this.bindings.source&&(d=this.bindings.source.get()),""===b&&(j||this.options.valuePrimitive))b=null;else{for((!d||d instanceof a.data.DataSource)&&(d=this.widget.dataSource.view()),k&&(m=b.length,c=b.slice(0)),h=0,l=d.length;l>h;h++)if(g=d[h],f=g.get(i),k){for(e=0;m>e;e++)if(f==c[e]){c[e]=g;break}}else if(f==b){b=j?g:f;break}c[0]&&(b=this._valueIsObservableArray?c:j||!i?c[0]:c[0].get(i))}this.bindings.value.set(b),this._initChange=!1},refresh:function(){if(!this._initChange){var d,c=this.options.dataValueField||this.options.dataTextField,a=this.bindings.value.get(),b=0,e=[];if(a===j&&(a=null),c)if(a instanceof g){for(d=a.length;d>b;b++)e[b]=a[b].get(c);a=e}else a instanceof h&&(a=a.get(c));this.widget.value(a)}this._initChange=!1},destroy:function(){this.widget.unbind(e,this._change)}}),multiselect:{value:b.extend({init:function(a,f,c){b.fn.init.call(this,a.element[0],f,c),this.widget=a,this._change=d.proxy(this.change,this),this.widget.first(e,this._change),this._initChange=!1},change:function(){var j,l,h,d,c,k,n,i,m,e=this,b=e.bindings[f].get(),o=e.options.valuePrimitive,a=o?e.widget.value():e.widget.dataItems(),p=this.options.dataValueField||this.options.dataTextField;if(a=a.slice(0),e._initChange=!0,b instanceof g){for(j=[],l=a.length,h=0,d=0,c=b[h],k=!1;c;){for(m=!1,d=0;l>d;d++)if(o?(k=a[d]==c):(i=a[d],i=i.get?i.get(p):i,k=i==(c.get?c.get(p):c)),k){a.splice(d,1),l-=1,m=!0;break}m?(h+=1):(j.push(c),s(b,h,1),n=h),c=b[h]}s(b,b.length,0,a),j.length&&b.trigger("change",{action:"remove",items:j,index:n}),a.length&&b.trigger("change",{action:"add",items:a,index:b.length-1})}else e.bindings[f].set(a);e._initChange=!1},refresh:function(){if(!this._initChange){var f,b,d=this.options.dataValueField||this.options.dataTextField,a=this.bindings.value.get(),c=0,e=[];if(a===j&&(a=null),d)if(a instanceof g){for(f=a.length;f>c;c++)b=a[c],e[c]=b.get?b.get(d):b;a=e}else a instanceof h&&(a=a.get(d));this.widget.value(a)}},destroy:function(){this.widget.unbind(e,this._change)}})}},s=function(b,a,c,g){var d,i,h,e,f;if(g=g||[],c=c||0,d=g.length,i=b.length,h=[].slice.call(b,a+c),e=h.length,d){for(d=a+d,f=0;d>a;a++)b[a]=g[f],f++;b.length=d}else if(c)for(b.length=a,c+=a;c>a;)delete b[--c];if(e){for(e=a+e,f=0;e>a;a++)b[a]=h[f],f++;b.length=e}for(a=b.length;i>a;)delete b[a],a++},t=x.extend({init:function(a,b){this.target=a,this.options=b,this.toDestroy=[]},bind:function(a){var b,g,e,h,i=this.target.nodeName.toLowerCase(),d=c[i]||{};for(b in a)b==f?(g=!0):b==n?(e=!0):b==B?(h=!0):this.applyBinding(b,a,d);e&&this.applyBinding(n,a,d),g&&this.applyBinding(f,a,d),h&&this.applyBinding(B,a,d)},applyBinding:function(d,g,h){var e,b=h[d]||c[d],f=this.toDestroy,a=g[d];if(b)if(b=new b(this.target,g,this.options),f.push(b),a instanceof i)b.bind(a),f.push(a);else for(e in a)b.bind(a,e),f.push(a[e]);else if("template"!==d)throw Error("The "+d+" binding is not supported by the "+this.target.nodeName.toLowerCase()+" element");},destroy:function(){var a,c,b=this.toDestroy;for(a=0,c=b.length;c>a;a++)b[a].destroy()}}),u=t.extend({bind:function(a){var d,b=this,g=!1,h=!1,e=c.widget[b.target.options.name.toLowerCase()]||{};for(d in a)d==f?(g=!0):d==n?(h=!0):b.applyBinding(d,a,e);h&&b.applyBinding(n,a,e),g&&b.applyBinding(f,a,e)},applyBinding:function(d,g,h){var e,b=h[d]||c.widget[d],f=this.toDestroy,a=g[d];if(!b)throw Error("The "+d+" binding is not supported by the "+this.target.options.name+" widget");if(b=new b(this.target,g,this.target.options),f.push(b),a instanceof i)b.bind(a),f.push(a);else for(e in a)b.bind(a,e),f.push(a[e])}}),H=/[A-Za-z0-9_\-]+:(\{([^}]*)\}|[^,}]+)/g,E=/\s/g,a.unbind=I,a.bind=F,a.data.binders=c,a.data.Binder=b,a.notify=J,a.observable=function(a){return a instanceof h||(a=new h(a)),a},a.observableHierarchy=function(d){function c(b){for(var d,a=0;b.length>a;a++)b[a]._initChildren(),d=b[a].children,d.fetch(),b[a].items=d.data(),c(b[a].items)}var b=a.data.HierarchicalDataSource.create(d);return b.fetch(),c(b.data()),b._data._dataSource=b,b._data}})(window.kendo.jQuery);(function(b){function y(f){var d,c=a.ui.validator.ruleResolvers||{},e={};for(d in c)b.extend(!0,e,c[d].resolve(f));return e}function u(a){return a.replace(/&amp/g,"&amp;").replace(/&quot;/g,'"').replace(/&#39;/g,"'").replace(/&lt;/g,"<").replace(/&gt;/g,">")}function t(a){return a=(a+"").split("."),a.length>1?a[1].length:0}function w(a){return b.parseHTML?b(b.parseHTML(a)):b(a)}function p(h,i){var d,g,c,f,e=b();for(c=0,f=h.length;f>c;c++)d=h[c],q.test(d.className)&&(g=d.getAttribute(a.attr("for")),g===i&&(e=e.add(d)));return e}var l,a=window.kendo,f=a.ui.Widget,c=".kendoValidator",e="k-invalid-msg",q=RegExp(e,"i"),r="k-invalid",s=/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i,v=/^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i,d=":input:not(:button,[type=submit],[type=reset],[disabled],[readonly])",k=":checkbox:not([disabled],[readonly])",g="[type=number],[type=range]",n="blur",o="name",h="form",m="novalidate",z=b.proxy,i=function(b,a){return"string"==typeof a&&(a=RegExp("^(?:"+a+")$")),a.test(b)},j=function(a,d,c){var b=a.val();return a.filter(d).length&&""!==b?i(b,c):!0},x=function(a,b){return a.length?null!=a[0].attributes[b]:!1};a.ui.validator||(a.ui.validator={rules:{},messages:{}}),l=f.extend({init:function(g,c){var d=this,e=y(g);c=c||{},c.rules=b.extend({},a.ui.validator.rules,e.rules,c.rules),c.messages=b.extend({},a.ui.validator.messages,e.messages,c.messages),f.fn.init.call(d,g,c),d._errorTemplate=a.template(d.options.errorTemplate),d.element.is(h)&&d.element.attr(m,m),d._errors={},d._attachEvents()},events:["validate"],options:{name:"Validator",errorTemplate:'<span class="k-widget k-tooltip k-tooltip-validation"><span class="k-icon k-warning"> </span> #=message#</span>',messages:{required:"{0} is required",pattern:"{0} is not valid",min:"{0} should be greater than or equal to {1}",max:"{0} should be smaller than or equal to {1}",step:"{0} is not valid",email:"{0} is not valid email",url:"{0} is not valid URL",date:"{0} is not valid date"},rules:{required:function(a){var c=a.filter("[type=checkbox]").length&&!a.is(":checked"),b=a.val();return!(x(a,"required")&&(""===b||!b||c))},pattern:function(a){return a.filter("[type=text],[type=email],[type=url],[type=tel],[type=search],[type=password]").filter("[pattern]").length&&""!==a.val()?i(a.val(),a.attr("pattern")):!0},min:function(b){if(b.filter(g+",["+a.attr("type")+"=number]").filter("[min]").length&&""!==b.val()){var d=parseFloat(b.attr("min"))||0,c=a.parseFloat(b.val());return c>=d}return!0},max:function(b){if(b.filter(g+",["+a.attr("type")+"=number]").filter("[max]").length&&""!==b.val()){var d=parseFloat(b.attr("max"))||0,c=a.parseFloat(b.val());return d>=c}return!0},step:function(b){if(b.filter(g+",["+a.attr("type")+"=number]").filter("[step]").length&&""!==b.val()){var e,h=parseFloat(b.attr("min"))||0,d=parseFloat(b.attr("step"))||1,f=parseFloat(b.val()),c=t(d);return c?(e=Math.pow(10,c),(f-h)*e%(d*e)/Math.pow(100,c)===0):(f-h)%d===0}return!0},email:function(b){return j(b,"[type=email],["+a.attr("type")+"=email]",s)},url:function(b){return j(b,"[type=url],["+a.attr("type")+"=url]",v)},date:function(b){return b.filter("[type^=date],["+a.attr("type")+"=date]").length&&""!==b.val()?null!==a.parseDate(b.val(),b.attr(a.attr("format"))):!0}},validateOnBlur:!0},destroy:function(){f.fn.destroy.call(this),this.element.off(c)},_submit:function(a){return this.validate()?!0:(a.stopPropagation(),a.stopImmediatePropagation(),a.preventDefault(),!1)},_attachEvents:function(){var a=this;a.element.is(h)&&a.element.on("submit"+c,z(a._submit,a)),a.options.validateOnBlur&&(a.element.is(d)?(a.element.on(n+c,function(){a.validateInput(a.element)}),a.element.is(k)&&a.element.on("click"+c,function(){a.validateInput(a.element)})):(a.element.on(n+c,d,function(){a.validateInput(b(this))}),a.element.on("click"+c,k,function(){a.validateInput(b(this))})))},validate:function(){var c,b,f,e,a=!1;if(this._errors={},this.element.is(d))a=this.validateInput(this.element);else{for(e=!1,c=this.element.find(d),b=0,f=c.length;f>b;b++)this.validateInput(c.eq(b))||(e=!0);a=!e}return this.trigger("validate",{valid:a}),a},validateInput:function(a){var c,j,i,g,l,f,k,h,d;return a=b(a),c=this,j=c._errorTemplate,i=c._checkValidity(a),g=i.valid,l="."+e,f=a.attr(o)||"",k=c._findMessageContainer(f).add(a.next(l)).hide(),a.removeAttr("aria-invalid"),g||(h=c._extractMessage(a,i.key),c._errors[f]=h,d=w(j({message:u(h)})),c._decorateMessageContainer(d,f),k.replaceWith(d).length||d.insertAfter(a),d.show(),a.attr("aria-invalid",!0)),a.toggleClass(r,!g),g},hideMessages:function(){var c=this,b="."+e,a=c.element;a.is(d)?a.next(b).hide():a.find(b).hide()},_findMessageContainer:function(h){var e,d,g,f=a.ui.validator.messageLocators,c=b();for(d=0,g=this.element.length;g>d;d++)c=c.add(p(this.element[d].getElementsByTagName("*"),h));for(e in f)c=c.add(f[e].locate(this.element,h));return c},_decorateMessageContainer:function(b,f){var d,c=a.ui.validator.messageLocators;b.addClass(e).attr(a.attr("for"),f||"");for(d in c)c[d].decorate(b,f);b.attr("role","alert")},_extractMessage:function(b,d){var e=this,c=e.options.messages[d],f=b.attr(o);return c=a.isFunction(c)?c(b):c,a.format(b.attr(a.attr(d+"-msg"))||b.attr("validationMessage")||b.attr("title")||c||"",f,b.attr(d))},_checkValidity:function(c){var a,b=this.options.rules;for(a in b)if(!b[a](c))return{valid:!1,key:a};return{valid:!0}},errors:function(){var a,c=[],b=this._errors;for(a in b)c.push(b[a]);return c}}),a.ui.plugin(l)})(window.kendo.jQuery);(function(){return function(e,d){function g(a,c){if(!c)return a;a+"/"===c&&(a=c);var d=RegExp("^"+c,"i");return d.test(a)||(a=c+"/"+a),b.protocol+"//"+(b.host+"/"+a).replace(/\/\/+/g,"/")}function q(a){return a?"#!":"#"}function o(c){var a=b.href;return"#!"===c&&a.indexOf("#")>-1&&a.indexOf("#!")<0?null:a.split(c)[1]||""}function h(b,a){return 0===a.indexOf(b)?a.substr(b.length).replace(/\/\//g,"/"):a}function s(a){return a.replace(/^(#)?/,"#")}function v(a){return a.replace(/^(#(!)?)?/,"#!")}var a=window.kendo,f="change",t="back",l="same",m=a.support,b=window.location,c=window.history,u=50,n=a.support.browser.msie,w=/^#*/,i=window.document,j=a.Class.extend({back:function(){n?setTimeout(function(){c.back()}):c.back()},forward:function(){n?setTimeout(function(){c.forward()}):c.forward()},length:function(){return c.length},replaceLocation:function(a){b.replace(a)}}),k=j.extend({init:function(a){this.root=a},navigate:function(a){c.pushState({},i.title,g(a,this.root))},replace:function(a){c.replaceState({},i.title,g(a,this.root))},normalize:function(a){return h(this.root,a)},current:function(){var a=b.pathname;return b.search&&(a+=b.search),h(this.root,a)},change:function(a){e(window).bind("popstate.kendo",a)},stop:function(){e(window).unbind("popstate.kendo")},normalizeCurrent:function(f){var d,a=f.root,e=b.pathname,h=o(q(f.hashBang));a===e+"/"&&(d=a),a===e&&h&&(d=g(h.replace(w,""),a)),d&&c.pushState({},i.title,d)}}),p=j.extend({init:function(b){this._id=a.guid(),this.prefix=q(b),this.fix=b?v:s},navigate:function(a){b.hash=this.fix(a)},replace:function(a){this.replaceLocation(this.fix(a))},normalize:function(a){return a.indexOf(this.prefix)<0?a:a.split(this.prefix)[1]},change:function(a){m.hashChange?e(window).on("hashchange."+this._id,a):(this._interval=setInterval(a,u))},stop:function(){e(window).off("hashchange."+this._id),clearInterval(this._interval)},current:function(){return o(this.prefix)},normalizeCurrent:function(c){var d=b.pathname,a=c.root;return c.pushState&&a!==d?(this.replaceLocation(a+this.prefix+h(a,d)),!0):!1}}),r=a.Observable.extend({start:function(a){if(a=a||{},this.bind([f,t,l],a),!this._started){this._started=!0,a.root=a.root||"/";var c,b=this.createAdapter(a);b.normalizeCurrent(a)||(c=b.current(),e.extend(this,{adapter:b,root:a.root,historyLength:b.length(),current:c,locations:[c]}),b.change(e.proxy(this,"_checkUrl")))}},createAdapter:function(a){return m.pushState&&a.pushState?new k(a.root):new p(a.hashBang)},stop:function(){this._started&&(this.adapter.stop(),this.unbind(f),this._started=!1)},change:function(a){this.bind(f,a)},replace:function(a,b){this._navigate(a,b,function(b){b.replace(a),this.locations[this.locations-1]=this.current})},navigate:function(a,b){return"#:back"===a?(this.adapter.back(),d):(this._navigate(a,b,function(b){b.navigate(a),this.locations.push(this.current)}),d)},_navigate:function(a,c,e){var b=this.adapter;return a=b.normalize(a),this.current===a||this.current===decodeURIComponent(a)?(this.trigger(l),d):((c||!this.trigger(f,{url:a}))&&(this.current=a,e.call(this,b),this.historyLength=b.length()),d)},_checkUrl:function(){var b=this.adapter,a=b.current(),g=b.length(),h=this.historyLength===g,e=a===this.locations[this.locations.length-2]&&h,c=this.current;return null===a||this.current===a||this.current===decodeURIComponent(a)?!0:(this.historyLength=g,this.current=a,e&&this.trigger("back",{url:c,to:a})?(b.forward(),this.current=c,d):this.trigger(f,{url:a})?(e?b.forward():(b.back(),this.historyLength--),this.current=c,d):(e?this.locations.pop():this.locations.push(a),d))}});a.History=r,a.History.HistoryAdapter=j,a.History.HashAdapter=p,a.History.PushStateAdapter=k,a.absoluteURL=g,a.history=new r}(window.kendo.jQuery),function(){function k(a,b){return b?a:"([^/]+)"}function q(a){return RegExp("^"+a.replace(p,"\\$&").replace(j,"(?:$1)?").replace(l,k).replace(n,"(.*?)")+"$")}function g(a){return a.replace(/(\?.*)|(#.*)/g,"")}var a=window.kendo,b=a.history,h=a.Observable,e="init",i="routeMissing",d="change",f="back",c="same",j=/\((.*?)\)/g,l=/(\(\?)?:\w+/g,n=/\*\w+/g,p=/[\-{}\[\]+?.,\\\^$|#\s]/g,o=a.Class.extend({init:function(a,b){a instanceof RegExp||(a=q(a)),this.route=a,this._callback=b},callback:function(d){var b,e,c=0,f=a.parseQueryStringParams(d);for(d=g(d),b=this.route.exec(d).slice(1),e=b.length;e>c;c++)void 0!==b[c]&&(b[c]=decodeURIComponent(b[c]));b.push(f),this._callback.apply(null,b)},worksWith:function(a){return this.route.test(g(a))?(this.callback(a),!0):!1}}),m=h.extend({init:function(a){a||(a={}),h.fn.init.call(this),this.routes=[],this.pushState=a.pushState,this.hashBang=a.hashBang,this.root=a.root,this.bind([e,i,d,c],a)},destroy:function(){b.unbind(d,this._urlChangedProxy),b.unbind(c,this._sameProxy),b.unbind(f,this._backProxy),this.unbind()},start:function(){var c,a=this,g=function(){a._same()},f=function(b){a._back(b)},d=function(b){a._urlChanged(b)};b.start({same:g,change:d,back:f,pushState:a.pushState,hashBang:a.hashBang,root:a.root}),c={url:b.current||"/",preventDefault:$.noop},a.trigger(e,c)||a._urlChanged(c),this._urlChangedProxy=d,this._backProxy=f},route:function(a,b){this.routes.push(new o(a,b))},navigate:function(b,c){a.history.navigate(b,c)},replace:function(b,c){a.history.replace(b,c)},_back:function(a){this.trigger(f,{url:a.url,to:a.to})&&a.preventDefault()},_same:function(){this.trigger(c)},_urlChanged:function(c){var e,f,g,h,b=c.url;if(b||(b="/"),this.trigger(d,{url:c.url,params:a.parseQueryStringParams(c.url)}))return c.preventDefault(),void 0;for(e=0,f=this.routes,h=f.length;h>e;e++)if(g=f[e],g.worksWith(b))return;this.trigger(i,{url:b,params:a.parseQueryStringParams(b)})&&c.preventDefault()}});a.Router=m}(),window.kendo})();(function(b){function p(b){if(!b)return{};var a=b.match(k)||[];return{type:a[1],direction:a[3],reverse:"reverse"===a[5]}}var a=window.kendo,d=a.Observable,o="SCRIPT",h="init",e="show",i="hide",q="attach",m="detach",j=/unrecognized expression/,c=d.extend({init:function(f,c){var b=this;c=c||{},d.fn.init.call(b),b.content=f,b.id=a.guid(),b.tagName=c.tagName||"div",b.model=c.model,b._wrap=c.wrap!==!1,b._fragments={},b.bind([h,e,i],c)},render:function(d){var c=this,f=!c.element;return f&&(c.element=c._createElement()),d&&b(d).append(c.element),f&&(a.bind(c.element,c.model),c.trigger(h)),d&&(c._eachFragment(q),c.trigger(e)),c.element},clone:function(){return new f(this)},triggerBeforeShow:function(){return!0},showStart:function(){this.element.css("display","")},showEnd:function(){},hideStart:function(){},hideEnd:function(){this.hide()},hide:function(){this._eachFragment(m),this.element.detach(),this.trigger(i)},destroy:function(){var b=this.element;b&&(a.unbind(b),a.destroy(b),b.remove())},fragments:function(a){b.extend(this._fragments,a)},_eachFragment:function(b){for(var a in this._fragments)this._fragments[a][b](this,a)},_createElement:function(){var d,c,e=this,f=e._wrap,g="<"+e.tagName+" />";try{c=b(document.getElementById(e.content)||e.content),c[0].tagName===o&&(c=c.html())}catch(h){j.test(h.message)&&(c=e.content)}return"string"==typeof c?(d=b(g).append(c),a.stripWhitespace(d[0]),f||(d=d.contents())):(d=c,f&&(d=d.wrapAll(g).parent())),d}}),f=a.Class.extend({init:function(a){b.extend(this,{element:a.element.clone(!0),transition:a.transition,id:a.id}),a.element.parent().append(this.element)},hideStart:b.noop,hideEnd:function(){this.element.remove()}}),l=c.extend({init:function(a,b){c.fn.init.call(this,a,b),this.containers={}},container:function(b){var a=this.containers[b];return a||(a=this._createContainer(b),this.containers[b]=a),a},showIn:function(a,c,b){this.container(a).show(c,b)},_createContainer:function(a){var d,c=this.render(),b=c.find(a);if(!b.length&&c.is(a)){if(!c.is(a))throw Error("can't find a container with the specified "+a+" selector");b=c}return d=new g(b),d.bind("accepted",function(a){a.view.render(b)}),d}}),n=c.extend({attach:function(a,b){a.element.find(b).replaceWith(this.render())},detach:function(){console.log("detach",arguments)}}),k=/^(\w+)(:(\w+))?( (\w+))?$/,g=d.extend({init:function(a){d.fn.init.call(this),this.container=a,this.history=[],this.view=null,this.running=!1},after:function(){this.running=!1,this.trigger("complete",{view:this.view}),this.trigger("after")},end:function(){this.view.showEnd(),this.previous.hideEnd(),this.after()},show:function(c,j,h){if(!c.triggerBeforeShow())return this.trigger("after"),!1;h=h||c.id;var b=this,g=c===b.view?c.clone():b.view,d=b.history,k=d[d.length-2]||{},i=k.id===h,e=j||(i?d[d.length-1].transition:c.transition),f=p(e);return b.running&&b.effect.stop(),"none"===e&&(e=null),b.trigger("accepted",{view:c}),b.view=c,b.previous=g,b.running=!0,i?d.pop():d.push({id:h,transition:e}),g?(g.hideStart(),c.showStart(),e&&a.effects.enabled?(i&&!j&&(f.reverse=!f.reverse),b.effect=a.fx(c.element).replace(g.element,f.type).direction(f.direction).setReverse(f.reverse),b.effect.run().then(function(){b.end()})):b.end(),!0):(c.showStart(),c.showEnd(),b.after(),!0)}});a.ViewContainer=g,a.Fragment=n,a.Layout=l,a.View=c,a.ViewClone=f})(window.kendo.jQuery);(function(a){function u(b,h){var d=b.x.location,c=b.y.location,f=h.x.location,e=h.y.location,a=d-f,g=c-e;return{center:{x:(d+f)/2,y:(c+e)/2},distance:Math.sqrt(a*a+g*g)}}function i(a){var i,f,d,b=[],e=a.originalEvent,g=a.currentTarget,h=0;if(a.api)b.push({id:2,event:a,target:a.target,currentTarget:a.target,location:a});else if(a.type.match(/touch/))for(f=e?e.changedTouches:[],i=f.length;i>h;h++)d=f[h],b.push({location:d,event:a,target:d.target,currentTarget:g,id:d.identifier});else c.pointers||c.msPointers?b.push({location:e,event:a,target:a.target,currentTarget:g,id:e.pointerId}):b.push({id:1,event:a,target:a.target,currentTarget:g,location:a});return b}function C(d){var c,b,e;d.preventDefault(),c=a(d.data.root),b=c.closest(".k-widget").parent(),b[0]||(b=c.parent()),e=a.extend(!0,{},d,{target:c[0]}),b.trigger(a.Event(d.type,e))}function s(d){for(var c=b.eventMap.up.split(" "),a=0,e=c.length;e>a;a++)d(c[a])}var b=window.kendo,c=b.support,E=window.document,v=b.Class,o=b.Observable,e=a.now,g=a.extend,m=c.mobileOS,B=m&&m.android,r=800,A=c.browser.msie?5:0,q="press",w="hold",y="select",z="start",f="move",j="end",x="cancel",h="tap",n="release",D="gesturestart",p="gesturechange",t="gestureend",k="gesturetap",l=v.extend({init:function(b,c){var a=this;a.axis=b,a._updateLocationData(c),a.startLocation=a.location,a.velocity=a.delta=0,a.timeStamp=e()},move:function(c){var a=this,b=c["page"+a.axis],d=e(),f=d-a.timeStamp||1;(b||!B)&&(a.delta=b-a.location,a._updateLocationData(c),a.initialDelta=b-a.startLocation,a.velocity=a.delta/f,a.timeStamp=d)},_updateLocationData:function(b){var a=this,c=a.axis;a.location=b["page"+c],a.client=b["client"+c],a.screen=b["screen"+c]}}),F=v.extend({init:function(b,c,a){g(this,{x:new l("X",a.location),y:new l("Y",a.location),userEvents:b,target:c,currentTarget:a.currentTarget,initialTouch:a.target,id:a.id,pressEvent:a,_moved:!1,_finished:!1})},press:function(){this._holdTimeout=setTimeout(a.proxy(this,"_hold"),this.userEvents.minHold),this._trigger(q,this.pressEvent)},_hold:function(){this._trigger(w,this.pressEvent)},move:function(b){var a=this;if(!a._finished){if(a.x.move(b.location),a.y.move(b.location),!a._moved){if(a._withinIgnoreThreshold())return;if(d.current&&d.current!==a.userEvents)return a.dispose();a._start(b)}a._finished||a._trigger(f,b)}},end:function(b){var a=this;a.endTime=e(),a._finished||(a._finished=!0,a._moved?a._trigger(j,b):a._trigger(h,b),clearTimeout(a._holdTimeout),a._trigger(n,b),a.dispose())},dispose:function(){var c=this.userEvents,b=c.touches;this._finished=!0,this.pressEvent=null,clearTimeout(this._holdTimeout),b.splice(a.inArray(this,b),1)},skip:function(){this.dispose()},cancel:function(){this.dispose()},isMoved:function(){return this._moved},_start:function(a){clearTimeout(this._holdTimeout),this.startTime=e(),this._moved=!0,this._trigger(z,a)},_trigger:function(c,e){var a=this,b=e.event,d={touch:a,x:a.x,y:a.y,target:a.target,event:b};a.userEvents.notify(c,d)&&b.preventDefault()},_withinIgnoreThreshold:function(){var a=this.x.initialDelta,b=this.y.initialDelta;return Math.sqrt(a*a+b*b)<=this.userEvents.threshold}}),d=o.extend({init:function(i,e){var m,v,u,d=this,l=b.guid();e=e||{},m=d.filter=e.filter,d.threshold=e.threshold||A,d.minHold=e.minHold||r,d.touches=[],d._maxTouches=e.multiTouch?2:1,d.allowSelection=e.allowSelection,d.captureUpIfMoved=e.captureUpIfMoved,d.eventNS=l,i=a(i).handler(d),o.fn.init.call(d),g(d,{element:i,surface:e.global?a(E.documentElement):a(e.surface||i),stopPropagation:e.stopPropagation,pressed:!1}),d.surface.handler(d).on(b.applyEventMap("move",l),"_move").on(b.applyEventMap("up cancel",l),"_end"),i.on(b.applyEventMap("down",l),m,"_start"),(c.pointers||c.msPointers)&&i.css("-ms-touch-action","pinch-zoom double-tap-zoom"),e.preventDragEvent&&i.on(b.applyEventMap("dragstart",l),b.preventDefault),i.on(b.applyEventMap("mousedown selectstart",l),m,{root:i},"_select"),d.captureUpIfMoved&&c.eventCapture&&(v=d.surface[0],u=a.proxy(d.preventIfMoving,d),s(function(a){v.addEventListener(a,u,!0)})),d.bind([q,w,h,z,f,j,n,x,D,p,t,k,y],e)},preventIfMoving:function(a){this._isMoved()&&a.preventDefault()},destroy:function(){var b,a=this;a._destroyed||(a._destroyed=!0,a.captureUpIfMoved&&c.eventCapture&&(b=a.surface[0],s(function(c){b.removeEventListener(c,a.preventIfMoving)})),a.element.kendoDestroy(a.eventNS),a.surface.kendoDestroy(a.eventNS),a.element.removeData("handler"),a.surface.removeData("handler"),a._disposeAll(),a.unbind(),delete a.surface,delete a.element,delete a.currentTarget)},capture:function(){d.current=this},cancel:function(){this._disposeAll(),this.trigger(x)},notify:function(a,c){var d=this,b=d.touches;if(this._isMultiTouch()){switch(a){case f:a=p;break;case j:a=t;break;case h:a=k}g(c,{touches:b},u(b[0],b[1]))}return this.trigger(a,c)},press:function(a,c,b){this._apiCall("_start",a,c,b)},move:function(a,b){this._apiCall("_move",a,b)},end:function(a,b){this._apiCall("_end",a,b)},_isMultiTouch:function(){return this.touches.length>1},_maxTouchesReached:function(){return this.touches.length>=this._maxTouches},_disposeAll:function(){for(var a=this.touches;a.length>0;)a.pop().dispose()},_isMoved:function(){return a.grep(this.touches,function(a){return a.isMoved()}).length},_select:function(a){(!this.allowSelection||this.trigger(y,{event:a}))&&C(a)},_start:function(e){var g,c,b=this,f=0,l=b.filter,j=i(e),k=j.length,h=e.which;if(!(h&&h>1||b._maxTouchesReached()))for(d.current=null,b.currentTarget=e.currentTarget,b.stopPropagation&&e.stopPropagation();k>f&&!b._maxTouchesReached();f++)c=j[f],g=l?a(c.currentTarget):b.element,g.length&&(c=new F(b,g,c),b.touches.push(c),c.press(),b._isMultiTouch()&&b.notify("gesturestart",{}))},_move:function(a){this._eachTouch("move",a)},_end:function(a){this._eachTouch("end",a)},_eachTouch:function(h,k){for(var d,c,b,j=this,g={},f=i(k),e=j.touches,a=0;e.length>a;a++)d=e[a],g[d.id]=d;for(a=0;f.length>a;a++)c=f[a],b=g[c.id],b&&b[h](c)},_apiCall:function(e,c,b,d){this[e]({api:!0,pageX:c,pageY:b,clientX:c,clientY:b,target:a(d||this.element)[0],stopPropagation:a.noop,preventDefault:a.noop})}});d.minHold=function(a){r=a},b.getTouches=i,b.touchDelta=u,b.UserEvents=d})(window.kendo.jQuery);(function(b,l){function M(c,a){try{return b.contains(c,a)||c==a}catch(d){return!1}}function n(a,b){return parseInt(a.css(b),10)||0}function G(b,a){return Math.min(Math.max(b,a.min),a.max)}function v(a,e){var c=u(a),d=c.left+n(a,"borderLeftWidth")+n(a,"paddingLeft"),b=c.top+n(a,"borderTopWidth")+n(a,"paddingTop"),f=d+a.width()-e.outerWidth(!0),g=b+a.height()-e.outerHeight(!0);return{x:{min:d,max:f},y:{min:b,max:g}}}function N(a,e,d){for(var f,c,b=0,g=e&&e.length,h=d&&d.length;a&&a.parentNode;){for(b=0;g>b;b++)if(f=e[b],f.element[0]===a)return{target:f,targetElement:a};for(b=0;h>b;b++)if(c=d[b],r.matchesSelector.call(a,c.options.filter))return{target:c,targetElement:a};a=a.parentNode}return l}var e,t,E,j,y,A,a=window.kendo,r=a.support,p=window.document,L=a.Class,h=a.ui.Widget,f=a.Observable,Q=a.UserEvents,d=b.proxy,c=b.extend,u=a.getOffset,m={},g={},i={},q=a.elementUnderCursor,H="keyup",k="change",x="dragstart",z="hold",I="drag",F="dragend",w="dragcancel",s="dragenter",o="dragleave",B="drop",P=f.extend({init:function(h,g){var c=this,e=h[0];c.capture=!1,e.addEventListener?(b.each(a.eventMap.down.split(" "),function(){e.addEventListener(this,d(c._press,c),!0)}),b.each(a.eventMap.up.split(" "),function(){e.addEventListener(this,d(c._release,c),!0)})):(b.each(a.eventMap.down.split(" "),function(){e.attachEvent(this,d(c._press,c))}),b.each(a.eventMap.up.split(" "),function(){e.attachEvent(this,d(c._release,c))})),f.fn.init.call(c),c.bind(["press","release"],g||{})},captureNext:function(){this.capture=!0},cancelCapture:function(){this.capture=!1},_press:function(b){var a=this;a.trigger("press"),a.capture&&b.preventDefault()},_release:function(b){var a=this;a.trigger("release"),a.capture&&(b.preventDefault(),a.cancelCapture())}}),C=f.extend({init:function(c){var a=this;f.fn.init.call(a),a.forcedEnabled=!1,b.extend(a,c),a.scale=1,a.horizontal?(a.measure="offsetWidth",a.scrollSize="scrollWidth",a.axis="x"):(a.measure="offsetHeight",a.scrollSize="scrollHeight",a.axis="y")},makeVirtual:function(){b.extend(this,{virtual:!0,forcedEnabled:!0,_virtualMin:0,_virtualMax:0})},virtualSize:function(a,b){(this._virtualMin!==a||this._virtualMax!==b)&&(this._virtualMin=a,this._virtualMax=b,this.update())},outOfBounds:function(a){return a>this.max||this.min>a},forceEnabled:function(){this.forcedEnabled=!0},getSize:function(){return this.container[0][this.measure]},getTotal:function(){return this.element[0][this.scrollSize]},rescale:function(a){this.scale=a},update:function(e){var a=this,d=a.virtual?a._virtualMax:a.getTotal(),c=d*a.scale,b=a.getSize();a.max=a.virtual?-a._virtualMin:0,a.size=b,a.total=c,a.min=Math.min(a.max,b-c),a.minScale=b/d,a.centerOffset=(c-b)/2,a.enabled=a.forcedEnabled||c>b,e||a.trigger(k,a)}}),J=f.extend({init:function(b){var a=this;f.fn.init.call(a),a.x=new C(c({horizontal:!0},b)),a.y=new C(c({horizontal:!1},b)),a.container=b.container,a.forcedMinScale=b.minScale,a.maxScale=b.maxScale||100,a.bind(k,b)},rescale:function(a){this.x.rescale(a),this.y.rescale(a),this.refresh()},centerCoordinates:function(){return{x:Math.min(0,-this.x.centerOffset),y:Math.min(0,-this.y.centerOffset)}},refresh:function(){var a=this;a.x.update(),a.y.update(),a.enabled=a.x.enabled||a.y.enabled,a.minScale=a.forcedMinScale||Math.min(a.x.minScale,a.y.minScale),a.fitScale=Math.max(a.x.minScale,a.y.minScale),a.trigger(k)}}),D=f.extend({init:function(b){var a=this;c(a,b),f.fn.init.call(a)},outOfBounds:function(){return this.dimension.outOfBounds(this.movable[this.axis])},dragMove:function(b){var a=this,c=a.dimension,e=a.axis,f=a.movable,d=f[e]+b;c.enabled&&((c.min>d&&0>b||d>c.max&&b>0)&&(b*=a.resistance),f.translateAxis(e,b),a.trigger(k,a))}}),O=L.extend({init:function(g){var e,d,f,b,a=this;c(a,{elastic:!0},g),f=a.elastic?.5:0,b=a.movable,a.x=e=new D({axis:"x",dimension:a.dimensions.x,resistance:f,movable:b}),a.y=d=new D({axis:"y",dimension:a.dimensions.y,resistance:f,movable:b}),a.userEvents.bind(["move","end","gesturestart","gesturechange"],{gesturestart:function(b){a.gesture=b,a.offset=a.dimensions.container.offset()},gesturechange:function(f){var g,h,i,m=a.gesture,l=m.center,j=f.center,c=f.distance/m.distance,n=a.dimensions.minScale,k=a.dimensions.maxScale;n>=b.scale&&1>c&&(c+=.8*(1-c)),b.scale*c>=k&&(c=k/b.scale),h=b.x+a.offset.left,i=b.y+a.offset.top,g={x:(h-l.x)*c+j.x-h,y:(i-l.y)*c+j.y-i},b.scaleWith(c),e.dragMove(g.x),d.dragMove(g.y),a.dimensions.rescale(b.scale),a.gesture=f,f.preventDefault()},move:function(a){a.event.target.tagName.match(/textarea|input/i)||(e.dimension.enabled||d.dimension.enabled?(e.dragMove(a.x.delta),d.dragMove(a.y.delta),a.preventDefault()):a.touch.skip())},end:function(a){a.preventDefault()}})}}),K=r.transitions.prefix+"Transform";t=r.hasHW3D?function(a,c,b){return"translate3d("+a+"px,"+c+"px,0) scale("+b+")"}:function(a,c,b){return"translate("+a+"px,"+c+"px) scale("+b+")"},E=f.extend({init:function(c){var a=this;f.fn.init.call(a),a.element=b(c),a.element[0].style.webkitTransformOrigin="left top",a.x=0,a.y=0,a.scale=1,a._saveCoordinates(t(a.x,a.y,a.scale))},translateAxis:function(a,b){this[a]+=b,this.refresh()},scaleTo:function(a){this.scale=a,this.refresh()},scaleWith:function(a){this.scale*=a,this.refresh()},translate:function(a){this.x+=a.x,this.y+=a.y,this.refresh()},moveAxis:function(a,b){this[a]=b,this.refresh()},moveTo:function(a){c(this,a),this.refresh()},refresh:function(){var c,b=this,e=b.x,d=b.y;b.round&&(e=Math.round(e),d=Math.round(d)),c=t(e,d,b.scale),c!=b.coordinates&&(a.support.browser.msie&&10>a.support.browser.version?(b.element[0].style.position="absolute",b.element[0].style.left=b.x+"px",b.element[0].style.top=b.y+"px"):(b.element[0].style[K]=c),b._saveCoordinates(c),b.trigger(k))},_saveCoordinates:function(a){this.coordinates=a}}),j=h.extend({init:function(c,d){var b,a=this;h.fn.init.call(a,c,d),b=a.options.group,b in g?g[b].push(a):(g[b]=[a])},events:[s,o,B],options:{name:"DropTarget",group:"default"},destroy:function(){var a,c=this.options.group,b=g[c]||i[c];if(b.length>1){for(h.fn.destroy.call(this),a=0;b.length>a;a++)if(b[a]==this){b.splice(a,1);break}}else j.destroyGroup(c)},_trigger:function(e,b){var a=this,d=m[a.options.group];return d?a.trigger(e,c({},b.event,{draggable:d,dropTarget:b.dropTarget})):l},_over:function(a){this._trigger(s,a)},_out:function(a){this._trigger(o,a)},_drop:function(c){var b=this,a=m[b.options.group];a&&(a.dropped=!b._trigger(B,c))}}),j.destroyGroup=function(a){var c,b=g[a]||i[a];if(b){for(c=0;b.length>c;c++)h.fn.destroy.call(b[c]);b.length=0,delete g[a],delete i[a]}},j._cache=g,y=j.extend({init:function(c,d){var b,a=this;h.fn.init.call(a,c,d),b=a.options.group,b in i?i[b].push(a):(i[b]=[a])},options:{name:"DropTargetArea",group:"default",filter:null}}),A=h.extend({init:function(c,e){var b=this;h.fn.init.call(b,c,e),b._activated=!1,b.userEvents=new Q(b.element,{global:!0,allowSelection:!0,stopPropagation:!0,filter:b.options.filter,threshold:b.options.distance,start:d(b._start,b),hold:d(b._hold,b),move:d(b._drag,b),end:d(b._end,b),cancel:d(b._cancel,b),select:d(b._select,b)}),b._afterEndHandler=d(b._afterEnd,b),b.captureEscape=function(c){c.keyCode===a.keys.ESC&&(b._trigger(w,{event:c}),b.userEvents.cancel())}},events:[z,x,I,F,w],options:{name:"Draggable",distance:5,group:"default",cursorOffset:null,axis:null,container:null,filter:null,ignore:null,holdToDrag:!1,dropped:!1},cancelHold:function(){this._activated=!1},_updateHint:function(d){var a,c=this,h=c.options,e=c.boundaries,g=h.axis,f=c.options.cursorOffset;f?(a={left:d.x.location+f.left,top:d.y.location+f.top}):(c.hintOffset.left+=d.x.delta,c.hintOffset.top+=d.y.delta,a=b.extend({},c.hintOffset)),e&&(a.top=G(a.top,e.y),a.left=G(a.left,e.x)),"x"===g?delete a.top:"y"===g&&delete a.left,c.hint.css(a)},_shouldIgnoreTarget:function(c){var a=this.options.ignore;return a&&b(c).is(a)},_select:function(a){this._shouldIgnoreTarget(a.event.target)||a.preventDefault()},_start:function(g){var d,c=this,e=c.options,h=e.container,f=e.hint;return this._shouldIgnoreTarget(g.touch.initialTouch)||e.holdToDrag&&!c._activated?(c.userEvents.cancel(),l):(c.currentTarget=g.target,c.currentTargetOffset=u(c.currentTarget),f&&(c.hint&&c.hint.stop(!0,!0).remove(),c.hint=a.isFunction(f)?b(f.call(c,c.currentTarget)):f,d=u(c.currentTarget),c.hintOffset=d,c.hint.css({position:"absolute",zIndex:2e4,left:d.left,top:d.top}).appendTo(p.body)),m[e.group]=c,c.dropped=!1,h&&(c.boundaries=v(h,c.hint)),c._trigger(x,g)&&(c.userEvents.cancel(),c._afterEnd()),b(p).on(H,c.captureEscape),l)},_hold:function(a){this.currentTarget=a.target,this._trigger(z,a)?this.userEvents.cancel():(this._activated=!0)},_drag:function(a){var d=this;a.preventDefault(),d._withDropTarget(a,function(d,f){if(!d)return e&&(e._trigger(o,c(a,{dropTarget:b(e.targetElement)})),e=null),l;if(e){if(f===e.targetElement)return;e._trigger(o,c(a,{dropTarget:b(e.targetElement)}))}d._trigger(s,c(a,{dropTarget:b(f)})),e=c(d,{targetElement:f})}),d._trigger(I,a),d.hint&&d._updateHint(a)},_end:function(a){var d=this;d._withDropTarget(a,function(d,f){d&&(d._drop(c({},a,{dropTarget:b(f)})),e=null)}),d._trigger(F,a),d._cancel(a.event)},_cancel:function(){var a=this;a._activated=!1,a.hint&&!a.dropped?setTimeout(function(){a.hint.stop(!0,!0).animate(a.currentTargetOffset,"fast",a._afterEndHandler)},0):a._afterEnd()},_trigger:function(d,a){var b=this;return b.trigger(d,c({},a.event,{x:a.x,y:a.y,currentTarget:b.currentTarget,dropTarget:a.dropTarget}))},_withDropTarget:function(d,j){var a,c,b=this,h=b.options,f=g[h.group],e=i[h.group];(f&&f.length||e&&e.length)&&(a=q(d),b.hint&&M(b.hint[0],a)&&(b.hint.hide(),a=q(d),a||(a=q(d)),b.hint.show()),c=N(a,f,e),c?j(c.target,c.targetElement):j())},destroy:function(){var a=this;h.fn.destroy.call(a),a._afterEnd(),a.userEvents.destroy(),a.currentTarget=null},_afterEnd:function(){var a=this;a.hint&&a.hint.remove(),delete m[a.options.group],a.trigger("destroy"),b(p).off(H,a.captureEscape)}}),a.ui.plugin(j),a.ui.plugin(y),a.ui.plugin(A),a.TapCapture=P,a.containerBoundaries=v,c(a.ui,{Pane:O,PaneDimensions:J,Movable:E})})(window.kendo.jQuery);(function(a){function m(c,b){return c===b||a.contains(c,b)}var b=window.kendo,D=b.ui,v=D.Widget,g=b.support,d=b.getOffset,L=b._activeElement,t="open",w="close",A="deactivate",z="activate",e="center",C="left",k="right",s="top",j="bottom",x="absolute",G="hidden",n="body",p="location",h="position",F="visible",E="effects",y="k-state-active",l="k-state-border",K=/k-state-border-(\w+)/,B=".k-picker-wrap, .k-dropdown-wrap, .k-link",i="down",q=a(window),r=a(document.documentElement),o="resize scroll",J=g.transitions.css,H=J+"transform",c=a.extend,f=".kendoPopup",u=["font-size","font-family","font-stretch","font-style","font-weight","line-height"],I=v.extend({init:function(g,e){var h,d=this;e=e||{},e.isRtl&&(e.origin=e.origin||j+" "+k,e.position=e.position||s+" "+k),v.fn.init.call(d,g,e),g=d.element,e=d.options,d.collisions=e.collision?e.collision.split(" "):[],1===d.collisions.length&&d.collisions.push(d.collisions[0]),h=a(d.options.anchor).closest(".k-popup,.k-group").filter(":not([class^=km-])"),e.appendTo=a(a(e.appendTo)[0]||h[0]||n),d.element.hide().addClass("k-popup k-group k-reset").toggleClass("k-rtl",!!e.isRtl).css({position:x}).appendTo(e.appendTo).on("mouseenter"+f,function(){d._hovered=!0}).on("mouseleave"+f,function(){d._hovered=!1}),d.wrapper=a(),e.animation===!1&&(e.animation={open:{effects:{}},close:{hide:!0,effects:{}}}),c(e.animation.open,{complete:function(){d.wrapper.css({overflow:F}),d.trigger(z)}}),c(e.animation.close,{complete:function(){d.wrapper.hide();var f,c,i=d.wrapper.data(p),h=a(e.anchor);i&&d.wrapper.css(i),e.anchor!=n&&(f=(h[0].className.match(K)||["","down"])[1],c=l+"-"+f,h.removeClass(c).children(B).removeClass(y).removeClass(c),g.removeClass(l+"-"+b.directions[f].reverse)),d._closing=!1,d.trigger(A)}}),d._mousedownProxy=function(a){d._mousedown(a)},d._resizeProxy=function(a){d._resize(a)},e.toggleTarget&&a(e.toggleTarget).on(e.toggleEvent+f,a.proxy(d.toggle,d))},events:[t,z,w,A],options:{name:"Popup",toggleEvent:"click",origin:j+" "+C,position:s+" "+C,anchor:n,collision:"flip fit",viewport:window,copyAnchorStyles:!0,autosize:!1,modal:!1,animation:{open:{effects:"slideIn:down",transition:!0,duration:200},close:{duration:100,hide:!0}}},destroy:function(){var g,c=this,d=c.options,e=c.element.off(f);v.fn.destroy.call(c),d.toggleTarget&&a(d.toggleTarget).off(f),d.modal||(r.unbind(i,c._mousedownProxy),q.unbind(o,c._resizeProxy)),b.destroy(c.element.children()),e.removeData(),d.appendTo[0]===document.body&&(g=e.parent(".k-animation-container"),g[0]?g.remove():e.remove())},open:function(C,w){var j,k,v,d=this,z={isFixed:!isNaN(parseInt(w,10)),x:C,y:w},e=d.element,f=d.options,m="down",p=a(f.anchor),A=e[0]&&e.hasClass("km-widget");if(!d.visible()){if(f.copyAnchorStyles&&(A&&"font-size"==u[0]&&u.shift(),e.css(b.getComputedStyles(p[0],u))),e.data("animating")||d.trigger(t))return;f.modal||(r.unbind(i,d._mousedownProxy).bind(i,d._mousedownProxy),g.mobileOS.ios||g.mobileOS.android||q.unbind(o,d._resizeProxy).bind(o,d._resizeProxy)),d.wrapper=k=b.wrap(e,f.autosize).css({overflow:G,display:"block",position:x}),g.mobileOS.android&&k.add(p).css(H,"translatez(0)"),k.css(h),a(f.appendTo)[0]==document.body&&k.css(s,"-10000px"),j=c(!0,{},f.animation.open),d.flipped=d._position(z),j.effects=b.parseEffects(j.effects,d.flipped),m=j.effects.slideIn?j.effects.slideIn.direction:m,f.anchor!=n&&(v=l+"-"+m,e.addClass(l+"-"+b.directions[m].reverse),p.addClass(v).children(B).addClass(y).addClass(v)),e.data(E,j.effects).kendoStop(!0).kendoAnimate(j)}},toggle:function(){var a=this;a[a.visible()?w:t]()},visible:function(){return this.element.is(":"+F)},close:function(){var h,e,f,g,d=this,j=d.options;if(d.visible()){if(h=d.wrapper[0]?d.wrapper:b.wrap(d.element).hide(),d._closing||d.trigger(w))return;d.element.find(".k-popup").each(function(){var c=a(this),b=c.data("kendoPopup");b&&b.close()}),r.unbind(i,d._mousedownProxy),q.unbind(o,d._resizeProxy),e=c(!0,{},j.animation.close),f=d.element.data(E),g=e.effects,!g&&!b.size(g)&&f&&b.size(f)&&(e.effects=f,e.reverse=!0),d._closing=!0,d.element.kendoStop(!0),h.css({overflow:G}),d.element.kendoAnimate(e)}},_resize:function(b){var a=this;"resize"===b.type?(clearTimeout(a._resizeTimeout),a._resizeTimeout=setTimeout(function(){a._position(),a._resizeTimeout=null},50)):a._hovered||m(a.element[0],L())||a.close()},_mousedown:function(g){var d=this,j=d.element[0],f=d.options,k=a(f.anchor)[0],h=f.toggleTarget,e=b.eventTarget(g),c=a(e).closest(".k-popup"),i=c.parent().parent(".km-shim").length;c=c[0],(i||!c||c===d.element[0])&&"popover"!==a(g.target).closest("a").data("rel")&&(m(j,e)||m(k,e)||h&&m(a(h)[0],e)||d.close())},_fit:function(a,d,c){var b=0;return a+d>c&&(b=c-(a+d)),0>a&&(b=-a),b},_flip:function(d,c,f,i,h,g,b){var a=0;return b=b||c,g!==h&&g!==e&&h!==e&&(d+b>i&&(a+=-(f+c)),0>d+a&&(a+=f+c)),a},_position:function(t){var z,j,E,r,i,F,l,f,w,e=this,B=e.element.css(h,""),b=e.wrapper,q=e.options,n=a(q.viewport),G=n.offset(),k=a(q.anchor),y=q.origin.toLowerCase().split(" "),u=q.position.toLowerCase().split(" "),s=e.collisions,m=g.zoomLevel(),o=10002,D=!!(n[0]==window&&window.innerWidth&&1.02>=m),v=0,C=D?window.innerWidth:n.width(),H=D?window.innerHeight:n.height(),A=k.parents().filter(b.siblings());if(A[0])if(j=+a(A).css("zIndex"))o=j+1;else for(z=k.parentsUntil(A),E=z.length;E>v;v++)j=+a(z[v]).css("zIndex"),j&&j>o&&(o=j+1);return b.css("zIndex",o),t&&t.isFixed?b.css({left:t.x,top:t.y}):b.css(e._align(y,u)),r=d(b,h,k[0]===b.offsetParent()[0]),i=d(b),F=k.offsetParent().parent(".k-animation-container,.k-popup,.k-group"),F.length&&(r=d(b,h,!0),i=d(b)),n[0]===window?(i.top-=window.pageYOffset||document.documentElement.scrollTop||0,i.left-=window.pageXOffset||document.documentElement.scrollLeft||0):(i.top-=G.top,i.left-=G.left),e.wrapper.data(p)||b.data(p,c({},r)),l=c({},i),f=c({},r),"fit"===s[0]&&(f.top+=e._fit(l.top,b.outerHeight(),H/m)),"fit"===s[1]&&(f.left+=e._fit(l.left,b.outerWidth(),C/m)),w=c({},f),"flip"===s[0]&&(f.top+=e._flip(l.top,B.outerHeight(),k.outerHeight(),H/m,y[0],u[0],b.outerHeight())),"flip"===s[1]&&(f.left+=e._flip(l.left,B.outerWidth(),k.outerWidth(),C/m,y[1],u[1],b.outerWidth())),B.css(h,x),b.css(f),f.left!=w.left||f.top!=w.top},_align:function(t,r){var g,h=this,s=h.wrapper,i=a(h.options.anchor),q=t[0],u=t[1],n=r[0],m=r[1],p=d(i),o=a(h.options.appendTo),v=s.outerWidth(),x=s.outerHeight(),w=i.outerWidth(),l=i.outerHeight(),b=p.top,c=p.left,f=Math.round;return o[0]!=document.body&&(g=d(o),b-=g.top,c-=g.left),q===j&&(b+=l),q===e&&(b+=f(l/2)),n===j&&(b-=x),n===e&&(b-=f(x/2)),u===k&&(c+=w),u===e&&(c+=f(w/2)),m===k&&(c-=v),m===e&&(c-=f(v/2)),{top:b,left:c}}});D.plugin(I)})(window.kendo.jQuery);(function(d){var b=window.kendo,e=b.ui.Widget,c=d.proxy,a=Math.abs,g=20,f=b.Class.extend({init:function(f,e,c){c=d.extend({minXDelta:30,maxYDelta:20,maxDuration:1e3},c),new b.UserEvents(f,{surface:c.surface,allowSelection:!0,start:function(b){2*a(b.x.velocity)>=a(b.y.velocity)&&b.sender.capture()},move:function(d){var b=d.touch,g=d.event.timeStamp-b.startTime,f=b.x.initialDelta>0?"right":"left";a(b.x.initialDelta)>=c.minXDelta&&a(b.y.initialDelta)<c.maxYDelta&&c.maxDuration>g&&(e({direction:f,touch:b,target:b.target}),b.cancel())}})}}),h=e.extend({init:function(h,d){function f(b){return function(c){a._triggerTouch(b,c)}}function g(b){return function(c){a.trigger(b,{touches:c.touches,distance:c.distance,center:c.center,event:c.event})}}var a=this;e.fn.init.call(a,h,d),d=a.options,h=a.element,a.events=new b.UserEvents(h,{filter:d.filter,surface:d.surface,minHold:d.minHold,multiTouch:d.multiTouch,allowSelection:!0,press:f("touchstart"),hold:f("hold"),tap:c(a,"_tap"),gesturestart:g("gesturestart"),gesturechange:g("gesturechange"),gestureend:g("gestureend")}),d.enableSwipe?(a.events.bind("start",c(a,"_swipestart")),a.events.bind("move",c(a,"_swipemove"))):(a.events.bind("start",c(a,"_dragstart")),a.events.bind("move",f("drag")),a.events.bind("end",f("dragend"))),b.notify(a)},events:["touchstart","dragstart","drag","dragend","tap","doubletap","hold","swipe","gesturestart","gesturechange","gestureend"],options:{name:"Touch",surface:null,global:!1,multiTouch:!1,enableSwipe:!1,minXDelta:30,maxYDelta:20,maxDuration:1e3,minHold:800,doubleTapTimeout:800},cancel:function(){this.events.cancel()},_triggerTouch:function(b,a){this.trigger(b,{touch:a.touch,event:a.event})&&a.preventDefault()},_tap:function(c){var a=this,d=a.lastTap,e=c.touch;d&&a.options.doubleTapTimeout>e.endTime-d.endTime&&b.touchDelta(e,d).distance<g?(a._triggerTouch("doubletap",c),a.lastTap=null):(a._triggerTouch("tap",c),a.lastTap=e)},_dragstart:function(a){this._triggerTouch("dragstart",a)},_swipestart:function(b){2*a(b.x.velocity)>=a(b.y.velocity)&&b.sender.capture()},_swipemove:function(c){var e=this,d=e.options,b=c.touch,g=c.event.timeStamp-b.startTime,f=b.x.initialDelta>0?"right":"left";a(b.x.initialDelta)>=d.minXDelta&&a(b.y.initialDelta)<d.maxYDelta&&d.maxDuration>g&&(e.trigger("swipe",{direction:f,touch:c.touch}),b.cancel())}});window.jQuery.fn.kendoMobileSwipe=function(a,b){this.each(function(){new f(this,a,b)})},b.ui.plugin(h)})(window.kendo.jQuery);(function(f,q){var a=window.kendo,y=a.mobile,p=a.effects,o=y.ui,b=f.proxy,e=f.extend,g=o.Widget,z=a.Class,m=a.ui.Movable,v=a.ui.Pane,x=a.ui.PaneDimensions,n=p.Transition,d=p.Animation,c=Math.abs,B=500,F=.7,G=.96,I=10,k=55,r=.5,s=5,h="km-scroller-release",i="km-scroller-refresh",E="pull",j="change",A="resize",l="scroll",u=2,w=d.extend({init:function(c){var a=this;d.fn.init.call(a),e(a,c),a.userEvents.bind("gestureend",b(a.start,a)),a.tapCapture.bind("press",b(a.cancel,a))},enabled:function(){return this.dimensions.minScale>this.movable.scale},done:function(){return.01>this.dimensions.minScale-this.movable.scale},tick:function(){var a=this.movable;a.scaleWith(1.1),this.dimensions.rescale(a.scale)},onEnd:function(){var a=this.movable;a.scaleTo(this.dimensions.minScale),this.dimensions.rescale(a.scale)}}),H=d.extend({init:function(c){var a=this;d.fn.init.call(a),e(a,c,{transition:new n({axis:c.axis,movable:c.movable,onEnd:function(){a._end()}})}),a.tapCapture.bind("press",function(){a.cancel()}),a.userEvents.bind("end",b(a.start,a)),a.userEvents.bind("gestureend",b(a.start,a)),a.userEvents.bind("tap",b(a.onEnd,a))},onCancel:function(){this.transition.cancel()},freeze:function(b){var a=this;a.cancel(),a._moveTo(b)},onEnd:function(){var a=this;a.paneAxis.outOfBounds()?a._snapBack():a._end()},done:function(){return c(this.velocity)<1},start:function(b){var c,a=this;a.dimension.enabled&&(a.paneAxis.outOfBounds()?a._snapBack():(c=b.touch.id===u?0:b.touch[a.axis].velocity,a.velocity=Math.max(Math.min(c*a.velocityMultiplier,k),-k),a.tapCapture.captureNext(),d.fn.start.call(a)))},tick:function(){var a=this,c=a.dimension,e=a.paneAxis.outOfBounds()?r:a.friction,d=a.velocity*=e,b=a.movable[a.axis]+d;!a.elastic&&c.outOfBounds(b)&&(b=Math.max(Math.min(b,c.max),c.min),a.velocity=0),a.movable.moveAxis(a.axis,b)},_end:function(){this.tapCapture.cancelCapture(),this.end()},_snapBack:function(){var a=this,b=a.dimension,c=a.movable[a.axis]>b.max?b.max:b.min;a._moveTo(c)},_moveTo:function(a){this.transition.moveTo({location:a,duration:B,ease:n.easeOutExpo})}}),C=d.extend({init:function(b){var c=this;a.effects.Animation.fn.init.call(this),e(c,b,{origin:{},destination:{},offset:{}})},tick:function(){this._updateCoordinates(),this.moveTo(this.origin)},done:function(){return c(this.offset.y)<s&&c(this.offset.x)<s},onEnd:function(){this.moveTo(this.destination)},setCoordinates:function(a,b){this.offset={},this.origin=a,this.destination=b},_updateCoordinates:function(){this.offset={x:(this.destination.x-this.origin.x)/4,y:(this.destination.y-this.origin.y)/4},this.origin={y:this.origin.y+this.offset.y,x:this.origin.x+this.offset.x}}}),t=z.extend({init:function(d){var a=this,g="x"===d.axis,c=f('<div class="km-touch-scrollbar km-'+(g?"horizontal":"vertical")+'-scrollbar" />');e(a,d,{element:c,elementSize:0,movable:new m(c),scrollMovable:d.movable,size:g?"width":"height"}),a.scrollMovable.bind(j,b(a._move,a)),a.container.append(c)},_move:function(){var a=this,g=a.axis,e=a.dimension,d=e.size,h=a.scrollMovable,f=d/e.total,b=Math.round(-h[g]*f),c=Math.round(d*f);b+c>d?(c=d-b):0>b&&(c+=b,b=0),a.elementSize!=c&&(a.element.css(a.size,c+"px"),a.elementSize=c),a.movable.moveAxis(g,b)},show:function(){this.element.css({opacity:F,visibility:"visible"})},hide:function(){this.element.css({opacity:0})}}),D=g.extend({init:function(h,y){var o,p,k,i,s,n,t,u,r,d=this;return g.fn.init.call(d,h,y),h=d.element,(d._native=d.options.useNative&&a.support.hasNativeScrolling)?(h.addClass("km-native-scroller").prepend('<div class="km-scroll-header"/>'),e(d,{scrollElement:h,fixedContainer:h.children().first()}),q):(h.css("overflow","hidden").addClass("km-scroll-wrapper").wrapInner('<div class="km-scroll-container"/>').prepend('<div class="km-scroll-header"/>'),o=h.children().eq(1),p=new a.TapCapture(h),k=new m(o),i=new x({element:o,container:h,forcedEnabled:d.options.zoom}),s=this.options.avoidScrolling,n=new a.UserEvents(h,{allowSelection:!0,preventDragEvent:!0,captureUpIfMoved:!0,multiTouch:d.options.zoom,start:function(a){i.refresh();var e=c(a.x.velocity),b=c(a.y.velocity),j=2*e>=b,h=f.contains(d.fixedContainer[0],a.event.target),g=2*b>=e;!h&&!s(a)&&d.enabled&&(i.x.enabled&&j||i.y.enabled&&g)?n.capture():n.cancel()}}),t=new v({movable:k,dimensions:i,userEvents:n,elastic:d.options.elastic}),u=new w({movable:k,dimensions:i,userEvents:n,tapCapture:p}),r=new C({moveTo:function(a){d.scrollTo(a.x,a.y)}}),k.bind(j,function(){d.scrollTop=-k.y,d.scrollLeft=-k.x,d.trigger(l,{scrollTop:d.scrollTop,scrollLeft:d.scrollLeft})}),d.options.mousewheelScrolling&&h.on("DOMMouseScroll mousewheel",b(this,"_wheelScroll")),e(d,{movable:k,dimensions:i,zoomSnapBack:u,animatedScroller:r,userEvents:n,pane:t,tapCapture:p,pulled:!1,enabled:!0,scrollElement:o,scrollTop:0,scrollLeft:0,fixedContainer:h.children().first()}),d._initAxis("x"),d._initAxis("y"),d._wheelEnd=function(){d._wheel=!1,d.userEvents.end(0,d._wheelY)},i.refresh(),d.options.pullToRefresh&&d._initPullToRefresh(),q)},_wheelScroll:function(b){this._wheel||(this._wheel=!0,this._wheelY=0,this.userEvents.press(0,this._wheelY)),clearTimeout(this._wheelTimeout),this._wheelTimeout=setTimeout(this._wheelEnd,50);var c=a.wheelDeltaY(b);c&&(this._wheelY+=c,this.userEvents.move(0,this._wheelY)),b.preventDefault()},makeVirtual:function(){this.dimensions.y.makeVirtual()},virtualSize:function(a,b){this.dimensions.y.virtualSize(a,b)},height:function(){return this.dimensions.y.size},scrollHeight:function(){return this.scrollElement[0].scrollHeight},scrollWidth:function(){return this.scrollElement[0].scrollWidth},options:{name:"Scroller",zoom:!1,pullOffset:140,elastic:!0,useNative:!1,mousewheelScrolling:!0,avoidScrolling:function(){return!1},pullToRefresh:!1,pullTemplate:"Pull to refresh",releaseTemplate:"Release to refresh",refreshTemplate:"Refreshing"},events:[E,l,A],_resize:function(){this._native||this.contentResized()},setOptions:function(a){var b=this;g.fn.setOptions.call(b,a),a.pullToRefresh&&b._initPullToRefresh()},reset:function(){this._native?this.scrollElement.scrollTop(0):(this.movable.moveTo({x:0,y:0}),this._scale(1))},contentResized:function(){this.dimensions.refresh(),this.pane.x.outOfBounds()&&this.movable.moveAxis("x",this.dimensions.x.min),this.pane.y.outOfBounds()&&this.movable.moveAxis("y",this.dimensions.y.min)},zoomOut:function(){var a=this.dimensions;a.refresh(),this._scale(a.fitScale),this.movable.moveTo(a.centerCoordinates())},enable:function(){this.enabled=!0},disable:function(){this.enabled=!1},scrollTo:function(a,b){this._native?(this.scrollElement.scrollLeft(c(a)),this.scrollElement.scrollTop(c(b))):(this.dimensions.refresh(),this.movable.moveTo({x:a,y:b}))},animatedScrollTo:function(a,d){var c,b;this._native?this.scrollTo(a,d):(c={x:this.movable.x,y:this.movable.y},b={x:a,y:d},this.animatedScroller.setCoordinates(c,b),this.animatedScroller.start())},pullHandled:function(){var a=this;a.refreshHint.removeClass(i),a.hintContainer.html(a.pullTemplate({})),a.yinertia.onEnd(),a.xinertia.onEnd(),a.userEvents.cancel()},destroy:function(){g.fn.destroy.call(this),this.userEvents&&this.userEvents.destroy()},_scale:function(a){this.dimensions.rescale(a),this.movable.scaleTo(a)},_initPullToRefresh:function(){var c=this;c.dimensions.y.forceEnabled(),c.pullTemplate=a.template(c.options.pullTemplate),c.releaseTemplate=a.template(c.options.releaseTemplate),c.refreshTemplate=a.template(c.options.refreshTemplate),c.scrollElement.prepend('<span class="km-scroller-pull"><span class="km-icon"></span><span class="km-loading-left"></span><span class="km-loading-right"></span><span class="km-template">'+c.pullTemplate({})+"</span></span>"),c.refreshHint=c.scrollElement.children().first(),c.hintContainer=c.refreshHint.children(".km-template"),c.pane.y.bind("change",b(c._paneChange,c)),c.userEvents.bind("end",b(c._dragEnd,c))},_dragEnd:function(){var a=this;a.pulled&&(a.pulled=!1,a.refreshHint.removeClass(h).addClass(i),a.hintContainer.html(a.refreshTemplate({})),a.yinertia.freeze(a.options.pullOffset/2),a.trigger("pull"))},_paneChange:function(){var a=this;a.movable.y/r>a.options.pullOffset?a.pulled||(a.pulled=!0,a.refreshHint.removeClass(i).addClass(h),a.hintContainer.html(a.releaseTemplate({}))):a.pulled&&(a.pulled=!1,a.refreshHint.removeClass(h),a.hintContainer.html(a.pullTemplate({})))},_initAxis:function(b){var a=this,e=a.movable,d=a.dimensions[b],g=a.tapCapture,f=a.pane[b],c=new t({axis:b,movable:e,dimension:d,container:a.element});f.bind(j,function(){c.show()}),a[b+"inertia"]=new H({axis:b,paneAxis:f,movable:e,tapCapture:g,userEvents:a.userEvents,dimension:d,elastic:a.options.elastic,friction:a.options.friction||G,velocityMultiplier:a.options.velocityMultiplier||I,end:function(){c.hide(),a.trigger("scrollEnd",{axis:b,scrollTop:a.scrollTop,scrollLeft:a.scrollLeft})}})}});o.plugin(D)})(window.kendo.jQuery);(function(c,f){function q(g){var c,f,e=g.find(b("popover")),h=d.roles;for(c=0,f=e.length;f>c;c++)a.initWidget(e[c],{},h)}function n(b){a.triggeredByInput(b)||b.preventDefault()}function s(b){b.each(function(){a.initWidget(c(this),{},d.roles)})}var a=window.kendo,e=a.mobile,d=e.ui,l=a.attr,g=d.Widget,x=a.ViewClone,h="init",A='<div style="height: 100%; width: 100%; position: absolute; top: 0; left: 0; z-index: 20000; display: none" />',r="beforeShow",i="show",m="afterShow",p="beforeHide",j="hide",v="destroy",k=a.attrValue,b=a.roleSelector,C=g.extend({init:function(b,a){g.fn.init.call(this,b,a),this.params={},c.extend(this,a),this.transition=this.transition||this.defaultTransition,this._id(),this._layout(),this._overlay(),this._scroller(),this._model()},events:[h,r,i,m,p,j,v],options:{name:"View",title:"",reload:!1,transition:"",defaultTransition:"",useNativeScrolling:!1,stretch:!1,zoom:!1,model:null,initWidgets:!0},enable:function(a){f===a&&(a=!0),a?this.overlay.hide():this.overlay.show()},destroy:function(){this.layout&&this.layout.detach(this),this.trigger(v),g.fn.destroy.call(this),this.scroller&&this.scroller.destroy(),a.destroy(this.element)},purge:function(){this.destroy(),this.element.remove()},triggerBeforeShow:function(){return this.trigger(r,{view:this})?!1:!0},showStart:function(){var b=this;b.element.css("display",""),b.inited||(b.inited=!0,b.trigger(h,{view:b})),b.layout&&b.layout.attach(b),b._padIfNativeScrolling(),b.trigger(i,{view:b}),a.resize(b.element)},showEnd:function(){this.trigger(m,{view:this}),this._padIfNativeScrolling()},hideStart:function(){this.trigger(p,{view:this})},hideEnd:function(){var a=this;a.element.hide(),a.trigger(j,{view:a})},_padIfNativeScrolling:function(){if(e.appLevelNativeScrolling()){var b=a.support.mobileOS&&a.support.mobileOS.android,d=b?"footer":"header",c=b?"header":"footer";this.content.css({paddingTop:this[d].height(),paddingBottom:this[c].height()})}},contentElement:function(){var a=this;return a.options.stretch?a.content:a.scrollerContent},clone:function(){return new x(this)},_scroller:function(){var b=this;e.appLevelNativeScrolling()||(b.options.stretch?b.content.addClass("km-stretched-view"):(b.content.kendoMobileScroller({zoom:b.options.zoom,useNative:b.options.useNativeScrolling}),b.scroller=b.content.data("kendoMobileScroller"),b.scrollerContent=b.scroller.scrollElement),a.support.kineticScrollNeeded&&(c(b.element).on("touchmove",".km-header",n),b.options.useNativeScrolling||c(b.element).on("touchmove",".km-content",n)))},_model:function(){var b=this,f=b.element,c=b.options.model;"string"==typeof c&&(c=a.getter(c)(window)),b.model=c,q(f),b.element.css("display",""),b.options.initWidgets&&(c?a.bind(f.children(),c,d,a.ui,a.dataviz.ui):e.init(f.children())),b.element.css("display","none")},_id:function(){var b=this.element,c=b.attr("id")||"";this.id=k(b,"url")||"#"+c,"#"==this.id&&(this.id=a.guid(),b.attr("id",this.id))},_layout:function(){var a=this,d=b("content"),c=a.element;c.data("kendoView",a).addClass("km-view"),a.header=c.children(b("header")).addClass("km-header"),a.footer=c.children(b("footer")).addClass("km-footer"),c.children(d)[0]||c.wrapInner("<div "+l("role")+'="content"></div>'),a.content=c.children(b("content")).addClass("km-content"),a.element.prepend(a.header).append(a.footer),a.layout&&a.layout.setup(a)},_overlay:function(){this.overlay=c(A).appendTo(this.element)}}),E=g.extend({init:function(d,e){var c=this;g.fn.init.call(c,d,e),d=c.element,c.header=d.children(b("header")).addClass("km-header"),c.footer=d.children(b("footer")).addClass("km-footer"),c.elements=c.header.add(c.footer),q(d),a.mobile.init(c.element.children()),c.element.detach(),c.trigger(h,{layout:c})},options:{name:"Layout"},events:[h,i,j],setup:function(a){a.header[0]||(a.header=this.header),a.footer[0]||(a.footer=this.footer)},detach:function(b){var a=this;b.header===a.header&&a.header[0]&&b.element.prepend(a.header.detach()[0].cloneNode(!0)),b.footer===a.footer&&a.footer.length&&b.element.append(a.footer.detach()[0].cloneNode(!0)),a.trigger(j,{layout:a,view:b})},attach:function(c){var a=this,d=a.currentView;d&&a.detach(d),c.header===a.header&&(a.header.detach(),c.element.children(b("header")).remove(),c.element.prepend(a.header)),c.footer===a.footer&&(a.footer.detach(),c.element.children(b("footer")).remove(),c.element.append(a.footer)),a.trigger(i,{layout:a,view:c}),a.currentView=c}}),u=a.Observable,w=/<body[^>]*>(([\u000a\u000d\u2028\u2029]|.)*)<\/body>/i,D="loadStart",t="loadComplete",B="showStart",y="sameViewRequested",z="viewShow",o="after",F=u.extend({init:function(h){var g,f,e,d=this;if(u.fn.init.call(d),c.extend(d,h),d.sandbox=c("<div />"),e=d.container,g=d._hideViews(e),d.rootView=g.first(),!d.rootView[0]&&h.rootNeeded)throw f=e[0]==a.mobile.application.element[0]?'Your kendo mobile application element does not contain any direct child elements with data-role="view" attribute set. Make sure that you instantiate the mobile application using the correct container.':'Your pane element does not contain any direct child elements with data-role="view" attribute set.',Error(f);d.layouts={},d.viewContainer=new a.ViewContainer(d.container),d.viewContainer.bind("accepted",function(a){a.view.params=d.params}),d.viewContainer.bind("complete",function(a){d.trigger(z,{view:a.view})}),d.viewContainer.bind(o,function(){d.trigger(o)}),d._setupLayouts(e),s(e.children(b("modalview drawer")))},destroy:function(){a.destroy(this.container);for(var b in this.layouts)this.layouts[b].destroy()},view:function(){return this.viewContainer.view},showView:function(a,g,f){if(a=a.replace(RegExp("^"+this.remoteViewURLPrefix),""),""===a&&this.remoteViewURLPrefix&&(a="/"),a===this.url)return this.trigger(y),!1;this.trigger(B);var c=this,e=function(b){return c.viewContainer.show(b,g,a)},d=c._findViewElement(a),b=d.data("kendoView");return c.url=a,c.params=f,b&&b.reload&&(b.purge(),d=[]),this.trigger("viewTypeDetermined",{remote:0===d.length,url:a}),d[0]?(b||(b=c._createView(d)),e(b)):(c._loadView(a,e),!0)},append:function(f,j){var e,h,d,c=this,a=c.sandbox,i=(j||"").split("?")[0],g=c.container;return w.test(f)&&(f=RegExp.$1),a[0].innerHTML=f,g.append(a.children("script, style")),e=c._hideViews(a),d=e.first(),d.length||(e=d=a.wrapInner("<div data-role=view />").children()),i&&d.hide().attr(l("url"),i),c._setupLayouts(a),h=a.children(b("modalview drawer")),g.append(a.children(b("layout modalview drawer")).add(e)),s(h),c._createView(d)},_findViewElement:function(c){var b,a=c.split("?")[0];return a?(b=this.container.children("["+l("url")+"='"+a+"']"),b[0]||-1!==a.indexOf("/")||(b=this.container.children("#"===a.charAt(0)?a:"#"+a)),b):this.rootView},_createView:function(e){var g,c=this,b=k(e,"layout");return f===b&&(b=c.layout),b&&(b=c.layouts[b]),g={defaultTransition:c.transition,loader:c.loader,container:c.container,layout:b,reload:k(e,"reload")},a.initWidget(e,g,d.roles)},_loadView:function(d,e){var b=this;return this.serverNavigation?(location.href=d,f):(b._xhr&&b._xhr.abort(),b.trigger(D),b._xhr=c.get(a.absoluteURL(d,b.remoteViewURLPrefix),function(a){b.trigger(t),e(b.append(a,d))},"html").fail(function(a){b.trigger(t),0===a.status&&a.responseText&&e(b.append(a.responseText,d))}),f)},_hideViews:function(a){return a.children(b("view splitview")).hide()},_setupLayouts:function(h){var g=this;h.children(b("layout")).each(function(){var b=c(this),h=k(b,"platform");(h===f||h===e.application.os.name)&&(g.layouts[a.attrValue(b,"id")]=a.initWidget(b,{},d.roles))})}});a.mobile.ViewEngine=F,d.plugin(C),d.plugin(E)})(window.kendo.jQuery);(function(a){var e=window.kendo,c=e.mobile.ui,b=c.Widget,d=a.map(e.eventMap,function(a){return a}).join(" ").split(" "),f=b.extend({init:function(e,f){var c=this,d=a('<div class="km-loader"><span class="km-loading km-spin"></span><span class="km-loading-left"></span><span class="km-loading-right"></span></div>');b.fn.init.call(c,d,f),c.container=e,c.captureEvents=!1,c._attachCapture(),d.append(c.options.loading).hide().appendTo(e)},options:{name:"Loader",loading:"<h1>Loading...</h1>",timeout:100},show:function(){var a=this;clearTimeout(a._loading),a.options.loading!==!1&&(a.captureEvents=!0,a._loading=setTimeout(function(){a.element.show()},a.options.timeout))},hide:function(){this.captureEvents=!1,clearTimeout(this._loading),this.element.hide()},changeMessage:function(a){this.options.loading=a,this.element.find(">h1").html(a)},transition:function(){this.captureEvents=!0,this.container.css("pointer-events","none")},transitionDone:function(){this.captureEvents=!1,this.container.css("pointer-events","")},_attachCapture:function(){function c(a){b.captureEvents&&a.preventDefault()}var a,b=this;for(b.captureEvents=!1,a=0;d.length>a;a++)b.container[0].addEventListener(d[a],c,!0)}});c.plugin(f)})(window.kendo.jQuery);(function(b,t){var a=window.kendo,f=a.mobile,d=a.roleSelector,c=f.ui,h=c.Widget,s=f.ViewEngine,o=c.View,u=f.ui.Loader,l="external",j="href",m="#!",p="navigate",k="viewShow",e="sameViewRequested",r=/popover|actionsheet|modalview|drawer/,v="#:back",g=a.attrValue,q="button backbutton detailbutton listview-link",n="tab",i=h.extend({init:function(d,c){var b=this;h.fn.init.call(b,d,c),c=b.options,d=b.element,d.addClass("km-pane"),b.options.collapsible&&d.addClass("km-collapsible-pane"),this.history=[],this.historyCallback=function(a,d){var c=b.transition;return b.transition=null,b.viewEngine.showView(a,c,d)},this._historyNavigate=function(c){if(c===v){if(1===b.history.length)return;b.history.pop(),c=b.history[b.history.length-1]}else b.history.push(c);b.historyCallback(c,a.parseQueryStringParams(c))},this._historyReplace=function(c){var d=a.parseQueryStringParams(c);b.history[b.history.length-1]=c,b.historyCallback(c,d)},b.loader=new u(d,{loading:b.options.loading}),b.viewEngine=new s({container:d,transition:c.transition,rootNeeded:!c.initial,serverNavigation:c.serverNavigation,remoteViewURLPrefix:c.root||"",layout:c.layout,loader:b.loader}),b.viewEngine.bind("showStart",function(){b.loader.transition(),b.closeActiveDialogs()}),b.viewEngine.bind("after",function(){b.loader.transitionDone()}),b.viewEngine.bind(k,function(a){b.trigger(k,a)}),b.viewEngine.bind("loadStart",function(){b.loader.show()}),b.viewEngine.bind("loadComplete",function(){b.loader.hide()}),b.viewEngine.bind(e,function(){b.trigger(e)}),b.viewEngine.bind("viewTypeDetermined",function(a){a.remote&&b.options.serverNavigation||b.trigger(p,{url:a.url})}),this._setPortraitWidth(),a.onResize(function(){b._setPortraitWidth()}),b._setupAppLinks()},closeActiveDialogs:function(){var e=this.element.find(d("actionsheet popover modalview")).filter(":visible");e.each(function(){a.widgetInstance(b(this),c).close()})},navigateToInitial:function(){var a=this.options.initial;a&&this.navigate(a)},options:{name:"Pane",portraitWidth:"",transition:"",layout:"",collapsible:!1,initial:null,loading:"<h1>Loading...</h1>"},events:[p,k,e],append:function(a){return this.viewEngine.append(a)},destroy:function(){h.fn.destroy.call(this),this.viewEngine.destroy()},navigate:function(a,b){a instanceof o&&(a=a.id),this.transition=b,this._historyNavigate(a)},replace:function(a,b){a instanceof o&&(a=a.id),this.transition=b,this._historyReplace(a)},bindToRouter:function(b){var c=this,f=c.options,d=f.initial,g=this.viewEngine;b.bind("init",function(e){var c=e.url,f=b.pushState?c:"/";g.rootView.attr(a.attr("url"),f),"/"===c&&d&&(b.navigate(d,!0),e.preventDefault())}),b.bind("routeMissing",function(a){c.historyCallback(a.url,a.params)||a.preventDefault()}),b.bind("same",function(){c.trigger(e)}),c._historyNavigate=function(a){b.navigate(a)},c._historyReplace=function(a){b.replace(a)}},hideLoading:function(){this.loader.hide()},showLoading:function(){this.loader.show()},changeLoadingMessage:function(a){this.loader.changeMessage(a)},view:function(){return this.viewEngine.view()},_setPortraitWidth:function(){var b,c=this.options.portraitWidth;c&&(b=a.mobile.application.element.is(".km-vertical")?c:"auto",this.element.css("width",b))},_setupAppLinks:function(){this.element.handler(this).on("down",d(n),"_mouseup").on("up",d(q),"_mouseup").on("click",d(n+" "+q),"_appLinkClick")},_appLinkClick:function(a){var c=b(a.currentTarget).attr("href"),d=c&&"#"!==c[0]&&this.options.serverNavigation;d||g(b(a.currentTarget),"rel")==l||a.preventDefault()},_mouseup:function(h){if(!(h.which>1||h.isDefaultPrevented())){var k=this,e=b(h.currentTarget),p=g(e,"transition"),i=g(e,"rel")||"",n=g(e,"target"),d=e.attr(j),o=d&&"#"!==d[0]&&this.options.serverNavigation;o||i===l||t===d||d===m||(e.attr(j,m),setTimeout(function(){e.attr(j,d)}),i.match(r)?(a.widgetInstance(b(d),c).openFor(e),("actionsheet"===i||"drawer"===i)&&h.stopPropagation()):("_top"===n?(k=f.application.pane):n&&(k=b("#"+n).data("kendoMobilePane")),k.navigate(d,p)),h.preventDefault())}}});i.wrap=function(b){b.is(d("view"))||(b=b.wrap("<div data-"+a.ns+'role="view" data-stretch="true"></div>').parent());var e=b.wrap('<div class="km-pane-wrapper"><div></div></div>').parent(),c=new i(e);return c.navigate(""),c},c.plugin(i)})(window.kendo.jQuery);(function(a){var d=window.kendo,q=d.mobile,c=q.ui,g="show",f="hide",e="open",h="close",o='<div class="km-popup-wrapper" />',r='<div class="km-popup-arrow" />',j='<div class="km-popup-overlay" />',k="km-up km-down km-left km-right",b=c.Widget,l={down:{origin:"bottom center",position:"top center"},up:{origin:"top center",position:"bottom center"},left:{origin:"center left",position:"center right",collision:"fit flip"},right:{origin:"center right",position:"center left",collision:"fit flip"}},n={animation:{open:{effects:"fade:in",duration:0},close:{effects:"fade:out",duration:400}}},m={horizontal:{offset:"top",size:"height"},vertical:{offset:"left",size:"width"}},p={up:"down",down:"up",left:"right",right:"left"},i=b.extend({init:function(g,e){var i,p,c=this,q=g.closest(".km-modalview-wrapper"),h=g.closest(".km-root").children(".km-pane").first(),k=q[0]?q:h;e.viewport?(h=e.viewport):h[0]||(h=window),e.container?(k=e.container):k[0]||(k=document.body),i={viewport:h,open:function(){c.overlay.show()},activate:a.proxy(c._activate,c),deactivate:function(){c.overlay.hide(),c.trigger(f)}},b.fn.init.call(c,g,e),g=c.element,e=c.options,g.wrap(o).addClass("km-popup").show(),p=c.options.direction.match(/left|right/)?"horizontal":"vertical",c.dimensions=m[p],c.wrapper=g.parent().css({width:e.width,height:e.height}).addClass("km-popup-wrapper km-"+e.direction).hide(),c.arrow=a(r).prependTo(c.wrapper).hide(),c.overlay=a(j).appendTo(k).hide(),i.appendTo=c.overlay,i.copyAnchorStyles=!1,i.autosize=!0,e.className&&c.overlay.addClass(e.className),c.popup=new d.ui.Popup(c.wrapper,a.extend(!0,i,n,l[e.direction]))},options:{name:"Popup",width:240,height:"",direction:"down",container:null,viewport:null},events:[g,f],show:function(d){var c=this,b=c.popup;b.options.anchor=a(d),b.open()},target:function(){return this.popup.options.anchor},hide:function(){this.popup.close()},destroy:function(){b.fn.destroy.call(this),this.popup.destroy(),this.overlay.remove()},_activate:function(){var b=this,i=b.options.direction,d=b.dimensions,f=d.offset,e=b.popup,l=e.options.anchor,o=a(l).offset(),q=a(e.element).offset(),m=e.flipped?p[i]:i,j=2*b.arrow[d.size](),h=b.element[d.size]()-b.arrow[d.size](),n=a(l)[d.size](),c=o[f]-q[f]+n/2;j>c&&(c=j),c>h&&(c=h),b.wrapper.removeClass(k).addClass("km-"+m),b.arrow.css(f,c).show(),b.trigger(g)}}),s=b.extend({init:function(j,k){var g,f=this;f.initialOpen=!1,b.fn.init.call(f,j,k),g=a.extend({className:"km-popover-root",show:function(){f.trigger(e,{target:f.popup.target()})},hide:function(){f.trigger(h)}},this.options.popup),f.popup=new i(f.element,g),f.popup.overlay.on("move",!1),f.pane=new c.Pane(f.element,this.options.pane),f.pane.navigateToInitial(),d.notify(f,c)},options:{name:"PopOver",popup:{},pane:{}},events:[e,h],open:function(a){this.openFor(a)},openFor:function(a){this.popup.show(a),this.initialOpen||(this.pane.navigate(""),this.popup.popup._position(),this.initialOpen=!0)},close:function(){this.popup.hide()},destroy:function(){b.fn.destroy.call(this),this.pane.destroy(),this.popup.destroy(),d.destroy(this.element)}});c.plugin(i),c.plugin(s)})(window.kendo.jQuery);(function(b,d){var a=window.kendo,f=a.mobile.ui,i=a.ui.Popup,h='<div class="km-shim"/>',e="show",g="hide",c=f.Widget,j=c.extend({init:function(m,e){var d=this,g=a.mobile.application,n=a.support.mobileOS,j=g?g.os.name:n?n.name:"ios",k="ios"===j||"wp"===j||(g?g.os.skin:!1),l="blackberry"===j,o=e.align||(k?"bottom center":l?"center right":"center center"),q=e.position||(k?"bottom center":l?"center right":"center center"),p=e.effect||(k?"slideIn:up":l?"slideIn:left":"fade:in"),f=b(h).handler(d).hide();c.fn.init.call(d,m,e),d.shim=f,m=d.element,e=d.options,e.className&&d.shim.addClass(e.className),e.modal||d.shim.on("up","hide"),(g?g.element:b(document.body)).append(f),d.popup=new i(d.element,{anchor:f,modal:!0,appendTo:f,origin:o,position:q,animation:{open:{effects:p,duration:e.duration},close:{duration:e.duration}},deactivate:function(){f.hide()},open:function(){f.show()}}),a.notify(d)},events:[e,g],options:{name:"Shim",modal:!1,align:d,position:d,effect:d,duration:200},show:function(){this.popup.open(),this.trigger(e)},hide:function(a){a&&b.contains(this.shim.children().children(".k-popup")[0],a.target)||(this.popup.close(),this.trigger(g))},destroy:function(){c.fn.destroy.call(this),this.shim.kendoDestroy(),this.popup.destroy(),this.shim.remove()}});f.plugin(j)})(window.kendo.jQuery);(function(h){var f=window.kendo,a=f.mobile.ui,i=a.Shim,c=a.Widget,d="open",b="close",e="init",j='<div class="km-modalview-wrapper" />',g=a.View.extend({init:function(d,g){var k,h,a=this;c.fn.init.call(a,d,g),d=a.element,g=a.options,k=d[0].style.width||"auto",h=d[0].style.height||"auto",d.addClass("km-modalview").wrap(j),a.wrapper=d.parent().css({width:g.width||k||300,height:g.height||h||300}).addClass("auto"==h?" km-auto-height":""),d.css({width:"",height:""}),a.shim=new i(a.wrapper,{modal:g.modal,position:"center center",align:"center center",effect:"fade:in",className:"km-modalview-root",hide:function(){a.trigger(b)}}),f.support.mobileOS.wp&&a.shim.shim.on("click",!1),a._layout(),a._scroller(),a._model(),a.element.css("display",""),a.trigger(e)},events:[e,d,b],options:{name:"ModalView",modal:!0,width:null,height:null},destroy:function(){c.fn.destroy.call(this),this.shim.destroy()},open:function(b){var a=this;a.target=h(b),a.shim.show(),a.trigger("show",{view:a})},openFor:function(a){this.open(a),this.trigger(d,{target:a})},close:function(){this.shim.hide(),this.trigger(b)}});a.plugin(g)})(window.kendo.jQuery);(function(e,c){var b=window.kendo,k=b.mobile,g=b.support.mobileOS,m=b.effects.Transition,h=b.roleSelector,d="x",f=k.ui,o=!(g.ios&&7==g.majorVersion&&!g.appMode),i="beforeShow",n="init",j="show",l="hide",p={enable:e.noop},a=f.View.extend({init:function(i,j){var g,c,d,a,f;if(e(i).parent().prepend(i),k.ui.Widget.fn.init.call(this,i,j),this._layout(),this._scroller(),this._model(),g=this.element.closest(h("pane")).data("kendoMobilePane"))this.pane=g,this.pane.bind("viewShow",function(b){a._viewShow(b)}),this.pane.bind("sameViewRequested",function(){a.hide()}),c=this.userEvents=new b.UserEvents(g.element,{filter:h("view splitview"),allowSelection:!0});else{if(this.currentView=p,d=e(this.options.container),!d)throw Error("The drawer needs a container configuration option set.");c=this.userEvents=new b.UserEvents(d,{allowSelection:!0}),this._attachTransition(d)}a=this,f=function(b){a.visible&&(a.hide(),b.preventDefault())},this.options.swipeToOpen&&o?(c.bind("press",function(){a.transition.cancel()}),c.bind("start",function(b){a._start(b)}),c.bind("move",function(b){a._update(b)}),c.bind("end",function(b){a._end(b)}),c.bind("tap",f)):c.bind("press",f),this.leftPositioned="left"===this.options.position,this.visible=!1,this.element.hide().addClass("km-drawer").addClass(this.leftPositioned?"km-left-drawer":"km-right-drawer"),this.trigger(n)},options:{name:"Drawer",position:"left",views:[],swipeToOpen:!0,title:"",container:null},events:[i,l,n,j],show:function(){this._activate()&&this._show()},hide:function(){this.currentView&&(this.currentView.enable(),a.current=null,this._moveViewTo(0),this.trigger(l,{view:this}))},openFor:function(){this.visible?this.hide():this.show()},destroy:function(){f.View.fn.destroy.call(this),this.userEvents.destroy()},_activate:function(){if(this.visible)return!0;var a,b=!0,c=this.options.views;return this.pane&&c.length&&(a=this.pane.view(),b=this._viewsInclude(a.id.replace("#",""))||this._viewsInclude(a.element.attr("id"))),this.trigger(i,{view:this})||!b?!1:(this._setAsCurrent(),this.element.show(),this.trigger(j,{view:this}),!0)},_viewsInclude:function(a){return this.options.views.indexOf(a)>-1},_show:function(){this.currentView.enable(!1),this.visible=!0;var a=this.element.width();this.leftPositioned||(a=-a),this._moveViewTo(a)},_setAsCurrent:function(){a.last!==this&&(a.last&&a.last.element.hide(),this.element.show()),a.last=this,a.current=this},_moveViewTo:function(a){this.userEvents.cancel(),this.transition.moveTo({location:a,duration:400,ease:m.easeOutExpo})},_viewShow:function(a){return this.currentView&&this.currentView.enable(),this.currentView===a.view?(this.hide(),c):(this.currentView=a.view,this._attachTransition(a.view.element),c)},_attachTransition:function(a){var e=this,c=this.movable,f=c&&c.x;this.transition&&(this.transition.cancel(),this.movable.moveAxis("x",0)),c=this.movable=new b.ui.Movable(a),this.transition=new m({axis:d,movable:this.movable,onEnd:function(){0===c[d]&&(a[0].style.cssText="",e.element.hide(),e.visible=!1)}}),f&&(a.addClass("k-fx-hidden"),b.animationFrame(function(){a.removeClass("k-fx-hidden"),e.movable.moveAxis(d,f),e.hide()}))},_start:function(d){var e,g,j,i,h,f=d.sender;return Math.abs(d.x.velocity)<Math.abs(d.y.velocity)||b.triggeredByInput(d.event)?(f.cancel(),c):(e=this.leftPositioned,g=this.visible,j=e&&g||!e&&!a.current,i=!e&&g||e&&!a.current,h=0>d.x.velocity,(j&&h||i&&!h)&&this._activate()?(f.capture(),c):(f.cancel(),c))},_update:function(a){var c,e=this.movable,b=e.x+a.x.delta;c=this.leftPositioned?Math.min(Math.max(0,b),this.element.width()):Math.max(Math.min(0,b),-this.element.width()),this.movable.moveAxis(d,c),a.event.preventDefault(),a.event.stopPropagation()},_end:function(e){var d,a=e.x.velocity,c=Math.abs(this.movable.x)>this.element.width()/2,b=.8;d=this.leftPositioned?a>-b&&(a>b||c):b>a&&(-b>a||c),d?this._show():this.hide()}});f.plugin(a)})(window.kendo.jQuery);(function(b){var a=window.kendo,c=a.mobile.ui,e=c.Widget,f="<div class='km-expanded-pane-shim' />",g=c.View,d=g.extend({init:function(g,h){var i,d=this;e.fn.init.call(d,g,h),g=d.element,b.extend(d,h),d._id(),d._layout(),d._overlay(),d._style(),a.mobile.init(g.children(a.roleSelector("modalview"))),d.panes=[],d._paramsHistory=[],d.element.children(a.roleSelector("pane")).each(function(){i=a.initWidget(this,{},c.roles),d.panes.push(i)}),d.expandedPaneShim=b(f).appendTo(d.element),d._shimUserEvents=new a.UserEvents(d.expandedPaneShim,{tap:function(){d.collapsePanes()}})},options:{name:"SplitView",style:"horizontal"},expandPanes:function(){this.element.addClass("km-expanded-splitview")},collapsePanes:function(){this.element.removeClass("km-expanded-splitview")},_layout:function(){var c=this,d=c.element;d.data("kendoView",c).addClass("km-view km-splitview"),c.transition=a.attrValue(d,"transition"),b.extend(c,{header:[],footer:[],content:d})},_style:function(){var c,a=this.options.style,d=this.element;a&&(c=a.split(" "),b.each(c,function(){d.addClass("km-split-"+this)}))},showStart:function(){var a=this;a.element.css("display",""),a.inited||(a.inited=!0,b.each(a.panes,function(){this.options.initial?this.navigateToInitial():this.navigate("")}),a.trigger("init",{view:a})),a.trigger("show",{view:a})}});c.plugin(d)})(window.kendo.jQuery);(function(b,H){function D(b,d){var c=[];return a&&c.push("km-on-"+a.name),b.skin?c.push("km-"+b.skin):"ios"==b.name&&b.majorVersion>6?c.push("km-ios7"):c.push("km-"+b.name),("ios"==b.name&&7>b.majorVersion||"ios"!=b.name)&&c.push("km-"+b.name+b.majorVersion),c.push("km-"+b.majorVersion),c.push("km-m"+(b.minorVersion?b.minorVersion[0]:0)),b.variant&&c.push("km-"+(b.skin?b.skin:b.name)+"-"+b.variant),b.cordova&&c.push("km-cordova"),b.appMode?c.push("km-app"):c.push("km-web"),d&&d.statusBarStyle&&c.push("km-"+d.statusBarStyle+"-status-bar"),c.join(" ")}function B(){return 0===parseInt(b("<div style='background: Background' />").css("background-color").split(",")[1],10)?"dark":"light"}function k(b){return a.wp?"-kendo-landscape"==b.css("animation-name"):Math.abs(window.orientation)/90==1}function p(a){return k(a)?L:y}function l(a){a.parent().addBack().css("min-height",window.innerHeight)}function q(){b("meta[name=viewport]").remove(),f.append(m({height:", width=device-width"+(k()?", height="+window.innerHeight+"px":e.mobileOS.flatVersion>=600&&700>e.mobileOS.flatVersion?", height="+window.innerWidth+"px":", height=device-height")}))}var c=window.kendo,n=c.mobile,e=c.support,J=n.ui.Pane,z="ios7",a=e.mobileOS,i="blackberry"==a.device&&a.flatVersion>=600&&1e3>a.flatVersion&&a.appMode,y="km-vertical",j="chrome"===a.browser,r=a.ios&&a.flatVersion>=700&&(a.appMode||j),h=Math.abs(window.orientation)/90==1,L="km-horizontal",s={ios7:{ios:!0,browser:"default",device:"iphone",flatVersion:"700",majorVersion:"7",minorVersion:"0.0",name:"ios",tablet:!1},ios:{ios:!0,browser:"default",device:"iphone",flatVersion:"612",majorVersion:"6",minorVersion:"1.2",name:"ios",tablet:!1},android:{android:!0,browser:"default",device:"android",flatVersion:"442",majorVersion:"4",minorVersion:"4.2",name:"android",tablet:!1},blackberry:{blackberry:!0,browser:"default",device:"blackberry",flatVersion:"710",majorVersion:"7",minorVersion:"1.0",name:"blackberry",tablet:!1},meego:{meego:!0,browser:"default",device:"meego",flatVersion:"850",majorVersion:"8",minorVersion:"5.0",name:"meego",tablet:!1},wp:{wp:!0,browser:"default",device:"wp",flatVersion:"800",majorVersion:"8",minorVersion:"0.0",name:"wp",tablet:!1}},m=c.template('<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no#=data.height#" name="viewport" />',{usedWithBlock:!1}),C=c.template('<meta name="apple-mobile-web-app-capable" content="#= data.webAppCapable === false ? \'no\' : \'yes\' #" /> <meta name="apple-mobile-web-app-status-bar-style" content="#=data.statusBarStyle#" /> <meta name="msapplication-tap-highlight" content="no" /> ',{usedWithBlock:!1}),K=c.template("<style>.km-view { clip: rect(0 #= data.width #px #= data.height #px 0); }</style>",{usedWithBlock:!1}),v=a.android&&"chrome"!=a.browser||a.blackberry,I=m({height:""}),G=c.template('<link rel="apple-touch-icon'+(a.android?"-precomposed":"")+'" # if(data.size) { # sizes="#=data.size#" #}# href="#=data.icon#" />',{usedWithBlock:!1}),w=("iphone"==a.device||"ipod"==a.device)&&7>a.majorVersion,t=("iphone"==a.device||"ipod"==a.device)&&a.majorVersion>=7,x=t?"none":null,E="mobilesafari"==a.browser?60:0,g=20,A=b(window),d=window.screen,f=b("head"),o="init",u=b.proxy,F=c.Observable.extend({init:function(d,e){var a=this;n.application=a,a.options=b.extend({hideAddressBar:!0,useNativeScrolling:!1,statusBarStyle:"black",transition:"",historyTransition:x,updateDocumentTitle:!0},e),c.Observable.fn.init.call(a,a.options),a.bind(a.events,a.options),b(function(){d=b(d),a.element=d[0]?d:b(document.body),a._setupPlatform(),a._attachMeta(),a._setupElementClass(),a._attachHideBarHandlers(),a.pane=new J(a.element,a.options),a.pane.navigateToInitial(),a.options.updateDocumentTitle&&a._setupDocumentTitle(),a._startHistory(),a.trigger(o)})},events:[o],navigate:function(a,b){this.pane.navigate(a,b)},replace:function(a,b){this.pane.replace(a,b)},scroller:function(){return this.view().scroller},hideLoading:function(){if(!this.pane)throw Error("The mobile application instance is not fully instantiated. Please consider activating loading in the application init event handler.");this.pane.hideLoading()},showLoading:function(){if(!this.pane)throw Error("The mobile application instance is not fully instantiated. Please consider activating loading in the application init event handler.");this.pane.showLoading()},changeLoadingMessage:function(a){if(!this.pane)throw Error("The mobile application instance is not fully instantiated. Please consider changing the message in the application init event handler.");this.pane.changeLoadingMessage(a)},view:function(){return this.pane.view()},skin:function(b){var a=this;return arguments.length?(a.options.skin=b||"",a.element[0].className="km-pane",a._setupPlatform(),a._setupElementClass(),a.options.skin):a.options.skin},destroy:function(){this.pane.destroy(),this.router.destroy()},_setupPlatform:function(){var e,d=this,g=d.options.platform,h=d.options.skin,f=[],c=a||s[z];g&&("string"==typeof g?(f=g.split("-"),c=b.extend({variant:f[1]},c,s[f[0]])):(c=g)),h&&(f=h.split("-"),c=b.extend({},c,{skin:f[0],variant:f[1]})),d.os=c,d.osCssClass=D(d.os,d.options),c.wp&&(e&&(b(window).off("focusin",e),document.removeEventListener("resume",e)),c.skin||(d.element.parent().css("overflow","hidden"),e=function(){d.element.removeClass("km-wp-dark km-wp-light").addClass("km-wp-"+B())},b(window).on("focusin",e),document.addEventListener("resume",e),e()))},_startHistory:function(){this.router=new c.Router({pushState:this.options.pushState,root:this.options.root,hashBang:this.options.hashBang}),this.pane.bindToRouter(this.router),this.router.start()},_resizeToScreenHeight:function(){var e,c=b("meta[name=apple-mobile-web-app-status-bar-style]").attr("content").match(/black-translucent|hidden/),a=this.element;e=j?window.innerHeight:k(a)?c?h?d.availWidth+g:d.availWidth:h?d.availWidth:d.availWidth-g:c?h?d.availHeight:d.availHeight+g:h?d.availHeight-g:d.availHeight,a.height(e)},_setupElementClass:function(){var g,d=this,a=d.element;a.parent().addClass("km-root km-"+(d.os.tablet?"tablet":"phone")),a.addClass(d.osCssClass+" "+p(a)),this.options.useNativeScrolling&&a.parent().addClass("km-native-scrolling"),j&&a.addClass("km-ios-chrome"),e.wpDevicePixelRatio&&a.parent().css("font-size",e.wpDevicePixelRatio+"em"),i&&q(),d.options.useNativeScrolling?a.parent().addClass("km-native-scrolling"):v&&(g=(screen.availWidth>screen.availHeight?screen.availWidth:screen.availHeight)+200,b(K({width:g,height:g})).appendTo(f)),r&&d._resizeToScreenHeight(),c.onResize(function(){a.removeClass("km-horizontal km-vertical").addClass(p(a)),d.options.useNativeScrolling&&l(a),r&&d._resizeToScreenHeight(),i&&q(),c.resize(a)})},_clearExistingMeta:function(){f.find("meta").filter("[name|='apple-mobile-web-app'],[name|='msapplication-tap'],[name='viewport']").remove()},_attachMeta:function(){var b,c=this.options,a=c.icon;if(this._clearExistingMeta(),i||f.prepend(I),f.prepend(C(c)),a){"string"==typeof a&&(a={"":a});for(b in a)f.prepend(G({icon:a[b],size:b}))}c.useNativeScrolling&&l(this.element)},_attachHideBarHandlers:function(){var a=this,b=u(a,"_hideBar");!e.mobileOS.appMode&&a.options.hideAddressBar&&w&&!a.options.useNativeScrolling&&(a._initialHeight={},A.on("load",b),c.onResize(function(){setTimeout(window.scrollTo,0,0,1)}))},_setupDocumentTitle:function(){var a=this,b=document.title;a.pane.bind("viewShow",function(c){var a=c.view.title;document.title=a!==H?a:b})},_hideBar:function(){var d=this,a=d.element;a.height(c.support.transforms.css+"calc(100% + "+E+"px)"),b(window).trigger(c.support.resize)}});c.mobile.Application=F})(window.kendo.jQuery);(function(c){var a=window.kendo,l=a.support,b=a.mobile.ui,o=b.Shim,m=b.Popup,d=b.Widget,h="open",g="close",i="command",e="li>a",j="actionsheetContext",n='<div class="km-actionsheet-wrapper" />',f=a.template('<li class="km-actionsheet-cancel"><a href="\\#">#:cancel#</a></li>'),k=d.extend({init:function(k,i){var q,p,h,g=this,j=l.mobileOS;d.fn.init.call(g,k,i),i=g.options,h=i.type,p="auto"===h?j&&j.tablet:"tablet"===h,q=p?m:o,k=g.element,i.cancelTemplate&&(f=a.template(i.cancelTemplate)),k.addClass("km-actionsheet").append(f({cancel:g.options.cancel})).wrap(n).on("up",e,"_click").on("click",e,a.preventDefault),g.view().bind("destroy",function(){g.destroy()}),g.wrapper=k.parent().addClass(h?" km-actionsheet-"+h:""),g.shim=new q(g.wrapper,c.extend({modal:j.ios&&7>j.majorVersion,className:"km-actionsheet-root"},g.options.popup)),a.notify(g,b),p&&a.onResize(c.proxy(this,"_resize"))},events:[h,g,i],options:{name:"ActionSheet",cancel:"Cancel",type:"auto",popup:{height:"auto"}},open:function(b,d){var a=this;a.target=c(b),a.context=d,a.shim.show(b)},close:function(){this.context=this.target=null,this.shim.hide()},openFor:function(a){var c=this,b=a.data(j);c.open(a,b),c.trigger(h,{target:a,context:b})},destroy:function(){d.fn.destroy.call(this),this.shim.destroy()},_click:function(d){var b,e;d.isDefaultPrevented()||(b=c(d.currentTarget),e=b.data("action"),e&&a.getter(e)(window)({target:this.target,context:this.context}),this.trigger(i,{target:this.target,context:this.context,currentTarget:b}),d.preventDefault(),this.close(),this.trigger(g))},_resize:function(){this.shim.hide()}});b.plugin(k)})(window.kendo.jQuery);(function(a,f){function g(b,d,c){a(d.target).closest(".km-button,.km-detail").toggleClass("km-state-active",c),i&&b.deactivateTimeoutID&&(clearTimeout(b.deactivateTimeoutID),b.deactivateTimeoutID=0)}function k(b){return a('<span class="km-badge">'+b+"</span>")}var e=window.kendo,q=e.mobile,c=q.ui,h=c.Widget,n=e.support,j=n.mobileOS,i=j.android&&j.flatVersion>=300,l="click",d="disabled",p="km-state-disabled",b=h.extend({init:function(b,c){var a=this;h.fn.init.call(a,b,c),a._wrap(),a._style(),a.options.enable=a.options.enable&&!a.element.attr(d),a.enable(a.options.enable),a._userEvents=new e.UserEvents(a.element,{press:function(b){a._activate(b)},tap:function(b){a._release(b)},release:function(b){g(a,b,!1)},end:function(b){(e.mobile.appLevelNativeScrolling()||a.viewHasNativeScrolling())&&b.preventDefault()}}),i&&a.element.on("move",function(b){a._timeoutDeactivate(b)})},destroy:function(){h.fn.destroy.call(this),this._userEvents.destroy()},events:[l],options:{name:"Button",icon:"",style:"",badge:"",enable:!0},badge:function(a){var b=this.badgeElement=this.badgeElement||k(a).appendTo(this.element);return a||0===a?(b.html(a),this):a===!1?(b.empty().remove(),this.badgeElement=!1,this):b.html()},enable:function(a){var b=this.element;f===a&&(a=!0),this.options.enable=a,a?b.removeAttr(d):b.attr(d,d),b.toggleClass(p,!a)},_timeoutDeactivate:function(a){this.deactivateTimeoutID||(this.deactivateTimeoutID=setTimeout(g,500,this,a,!1))},_activate:function(c){var a=document.activeElement,b=a?a.nodeName:"";this.options.enable&&(g(this,c,!0),("INPUT"==b||"TEXTAREA"==b)&&a.blur())},_release:function(b){var c=this;if(!(b.which>1))return c.options.enable?(c.trigger(l,{target:a(b.target),button:c.element})&&b.preventDefault(),f):(b.preventDefault(),f)},_style:function(){var c,b=this.options.style,d=this.element;b&&(c=b.split(" "),a.each(c,function(){d.addClass("km-"+this)}))},_wrap:function(){var c=this,f=c.options.icon,d=c.options.badge,g='<span class="km-icon km-'+f,b=c.element.addClass("km-button"),e=b.children("span:not(.km-icon)").addClass("km-text"),h=b.find("img").addClass("km-image");!e[0]&&b.html()&&(e=b.wrapInner('<span class="km-text" />').children("span.km-text")),!h[0]&&f&&(e[0]||(g+=" km-notext"),c.iconElement=b.prepend(a(g+'" />'))),(d||0===d)&&(c.badgeElement=k(d).appendTo(b))}}),m=b.extend({options:{name:"BackButton",style:"back"},init:function(c,d){var a=this;b.fn.init.call(a,c,d),f===a.element.attr("href")&&a.element.attr("href","#:back")}}),o=b.extend({options:{name:"DetailButton",style:""},init:function(a,c){b.fn.init.call(this,a,c)},_style:function(){var c,b=this.options.style+" detail",d=this.element;b&&(c=b.split(" "),a.each(c,function(){d.addClass("km-"+this)}))},_wrap:function(){var e=this,d=e.options.icon,c='<span class="km-icon km-'+d,b=e.element,g=b.children("span"),f=b.find("img").addClass("km-image");!f[0]&&d&&(g[0]||(c+=" km-notext"),b.prepend(a(c+'" />')))}});c.plugin(b),c.plugin(m),c.plugin(o)})(window.kendo.jQuery);(function(a,j){function d(b){return a('<span class="km-badge">'+b+"</span>")}var c=window.kendo,e=c.mobile.ui,f=e.Widget,b="km-state-active",g="select",i="li:not(."+b+")",h=f.extend({init:function(b,c){var a=this;f.fn.init.call(a,b,c),a.element.addClass("km-buttongroup").find("li").each(a._button),a.element.on(a.options.selectOn,i,"_select"),a.select(a.options.index)},events:[g],options:{name:"ButtonGroup",selectOn:"down",index:-1},current:function(){return this.element.find("."+b)},select:function(c){var d=this,e=-1;c!==j&&-1!==c&&(d.current().removeClass(b),"number"==typeof c?(e=c,c=a(d.element[0].children[c])):c.nodeType&&(c=a(c),e=c.index()),c.addClass(b),d.selectedIndex=e)},badge:function(b,c){var e,f=this.element;return isNaN(b)||(b=f.children().get(b)),b=f.find(b),e=a(b.children(".km-badge")[0]||d(c).appendTo(b)),c||0===c?(e.html(c),this):c===!1?(e.empty().remove(),this):e.html()},_button:function(){var b=a(this).addClass("km-button"),g=c.attrValue(b,"icon"),f=c.attrValue(b,"badge"),e=b.children("span"),h=b.find("img").addClass("km-image");e[0]||(e=b.wrapInner("<span/>").children("span")),e.addClass("km-text"),!h[0]&&g&&b.prepend(a('<span class="km-icon km-'+g+'"/>')),(f||0===f)&&d(f).appendTo(b)},_select:function(a){a.which>1||a.isDefaultPrevented()||(this.select(a.currentTarget),this.trigger(g,{index:this.selectedIndex}))}});e.plugin(h)})(window.kendo.jQuery);(function(b,z){function M(){return this.nodeType===S.TEXT_NODE&&this.nodeValue.match(R)}function f(a,b){b&&!a[0].querySelector(".km-icon")&&a.prepend('<span class="km-icon km-'+b+'"/>')}function N(a){f(a,d(a,"icon")),f(a,d(a.children(G),"icon"))}function Q(b){var c=b.parent(),e=b.add(c.children(a.roleSelector("detailbutton"))),g=c.contents().not(e).not(M);g.length||(b.addClass("km-listview-link").attr(a.attr("role"),"listview-link"),f(b,d(c,"icon")),f(b,d(b,"icon")))}function E(a){if(a[0].querySelector("input[type=checkbox],input[type=radio]")){var b=a.parent();b.contents().not(a).not(function(){return 3==this.nodeType})[0]||(a.addClass("km-listview-label"),a.children("[type=checkbox],[type=radio]").addClass("km-widget km-icon km-check"))}}function o(c,a){b(c).css("transform","translate3d(0px, "+a+"px, 0px)")}var v,k,j,A,u,y,s,t,a=window.kendo,S=window.Node,p=a.mobile,c=p.ui,T=a.data.DataSource,i=c.Widget,L=".km-list > li, > li:not(.km-group-container)",C=".km-listview-link, .km-listview-label",G="["+a.attr("icon")+"]",h=b.proxy,d=a.attrValue,e="km-group-title",F="km-state-active",H='<div class="'+e+'"><div class="km-text"></div></div>',O=a.template('<li><div class="'+e+'"><div class="km-text">#= this.headerTemplate(data) #</div></div><ul>#= kendo.render(this.template, data.items)#</ul></li>'),J='<div class="km-listview-wrapper" />',V=a.template('<form class="km-filter-form"><div class="km-filter-wrap"><input type="search" placeholder="#=placeholder#"/><a href="\\#" class="km-filter-reset" title="Clear"><span class="km-icon km-clear"></span><span class="km-text">Clear</span></a></div></form>'),g=".kendoMobileListView",r="styled",m="dataBound",q="dataBinding",n="itemChange",l="click",x="change",w="progress",B="function",R=/^\s+$/,U=/button/,K=a.Class.extend({init:function(a){var c,b,d=a.scroller();d&&(this.options=a.options,this.element=a.element,this.scroller=a.scroller(),this._shouldFixHeaders(),c=this,b=function(){c._cacheHeaders()},a.bind("resize",b),a.bind(r,b),a.bind(m,b),d.bind("scroll",function(a){c._fixHeader(a)}))},_fixHeader:function(i){if(this.fixedHeaders){var a,e,c,d=0,f=this.scroller,h=this.headers,g=i.scrollTop;do{if(a=h[d++],!a){c=b("<div />");break}e=a.offset,c=a.header}while(e+1>g);this.currentHeader!=d&&(f.fixedContainer.html(c.clone()),this.currentHeader=d)}},_shouldFixHeaders:function(){this.fixedHeaders="group"===this.options.type&&this.options.fixedHeaders},_cacheHeaders:function(){if(this._shouldFixHeaders(),this.fixedHeaders){var c=[],a=this.scroller.scrollTop;this.element.find("."+e).each(function(e,d){d=b(d),c.unshift({offset:d.position().top+a,header:d})}),this.headers=c,this._fixHeader({scrollTop:a})}}}),P=function(){return{page:1}},I=a.Class.extend({init:function(a){var c=this,b=a.options,d=a.scroller(),e=b.pullParameters||P;this.listView=a,this.scroller=d,a.bind("_dataSource",function(a){c.setDataSource(a.dataSource)}),d.setOptions({pullToRefresh:!0,pull:function(){c._pulled=!0,c.dataSource.read(e.call(a,c._first))},pullTemplate:b.pullTemplate,releaseTemplate:b.releaseTemplate,refreshTemplate:b.refreshTemplate})},setDataSource:function(a){var b=this;this._first=a.view()[0],this.dataSource=a,a.bind("change",function(){b._change()})},_change:function(){var a,c=this.scroller,b=this.dataSource;this._pulled&&c.pullHandled(),(this._pulled||!this._first)&&(a=b.view(),a[0]&&(this._first=a[0])),this._pulled=!1}}),D=a.Observable.extend({init:function(c){var b=this;a.Observable.fn.init.call(b),b.buffer=c.buffer,b.height=c.height,b.item=c.item,b.items=[],b.footer=c.footer,b.buffer.bind("reset",function(){b.refresh()})},refresh:function(){for(var f,g,c,b,d=this.buffer,a=this.items,e=!1;a.length;)a.pop().destroy();for(this.offset=d.offset,f=this.item,b=0;d.viewSize>b;b++){if(b===d.total()){e=!0;break}c=f(this.content(this.offset+a.length)),c.below(g),g=c,a.push(c)}this.itemCount=a.length,this.trigger("reset"),this._resize(),e&&this.trigger("endReached")},totalHeight:function(){if(!this.items[0])return 0;var a=this,b=a.items,d=b[0].top,c=b[b.length-1].bottom,e=(c-d)/a.itemCount,f=a.buffer.length-a.offset-a.itemCount;return(this.footer?this.footer.height:0)+c+f*e},batchUpdate:function(d){var b,c,e=this.height(),a=this.items,f=this.offset;if(a[0]){if(this.lastDirection)for(;a[a.length-1].bottom>d+2*e&&0!==this.offset;)this.offset--,b=a.pop(),b.update(this.content(this.offset)),b.above(a[0]),a.unshift(b);else for(;d-e>a[0].top;){if(c=this.offset+this.itemCount,c===this.buffer.total()){this.trigger("endReached");break}if(c===this.buffer.length)break;b=a.shift(),b.update(this.content(this.offset+this.itemCount)),b.below(a[a.length-1]),a.push(b),this.offset++}f!==this.offset&&this._resize()}},update:function(d){var b,j,i,e,f=this,a=this.items,k=this.height(),m=this.itemCount,c=k/2,g=(this.lastTop||0)>d,l=d-c,h=d+k+c;a[0]&&(this.lastTop=d,this.lastDirection=g,g?a[0].top>l&&a[a.length-1].bottom>h+c&&this.offset>0&&(this.offset--,b=a.pop(),j=a[0],b.update(this.content(this.offset)),a.unshift(b),b.above(j),f._resize()):h>a[a.length-1].bottom&&l-c>a[0].top&&(e=this.offset+m,e===this.buffer.total()?this.trigger("endReached"):e!==this.buffer.length&&(b=a.shift(),i=a[a.length-1],a.push(b),b.update(this.content(this.offset+this.itemCount)),f.offset++,b.below(i),f._resize())))},content:function(a){return this.buffer.at(a)},destroy:function(){this.unbind()},_resize:function(){var a=this.items,e=0,c=0,b=a[0],d=a[a.length-1];b&&(e=b.top,c=d.bottom),this.trigger("resize",{top:e,bottom:c}),this.footer&&this.footer.below(d)}});a.mobile.ui.VirtualList=D,v=a.Class.extend({init:function(d,e){var a=d.append([e])[0],c=a.offsetHeight;b.extend(this,{top:0,element:a,listView:d,height:c,bottom:c})},update:function(a){this.element=this.listView.setDataItem(this.element,a)},above:function(a){a&&(this.height=this.element.offsetHeight,this.top=a.top-this.height,this.bottom=a.top,o(this.element,this.top))},below:function(a){a&&(this.height=this.element.offsetHeight,this.top=a.bottom,this.bottom=this.top+this.height,o(this.element,this.top))},destroy:function(){a.destroy(this.element),b(this.element).remove()}}),k='<div><span class="km-icon"></span><span class="km-loading-left"></span><span class="km-loading-right"></span></div>',j=a.Class.extend({init:function(a){this.element=b('<li class="km-load-more km-scroller-refresh" style="display: none"></li>').appendTo(a.element),this._loadIcon=b(k).appendTo(this.element)},enable:function(){this.element.show(),this.height=this.element.outerHeight(!0)},disable:function(){this.element.hide(),this.height=0},below:function(a){a&&(this.top=a.bottom,this.bottom=this.height+this.top,o(this.element,this.top))}}),A=j.extend({init:function(d,c){this._loadIcon=b(k).hide(),this._loadButton=b('<a class="km-load">'+d.options.loadMoreText+"</a>").hide(),this.element=b('<li class="km-load-more" style="display: none"></li>').append(this._loadIcon).append(this._loadButton).appendTo(d.element);var a=this;this._loadButton.kendoMobileButton().data("kendoMobileButton").bind("click",function(){a._hideShowButton(),c.next()}),c.bind("resize",function(){a._showLoadButton()}),this.height=this.element.outerHeight(!0),this.disable()},_hideShowButton:function(){this._loadButton.hide(),this.element.addClass("km-scroller-refresh"),this._loadIcon.css("display","block")},_showLoadButton:function(){this._loadButton.show(),this.element.removeClass("km-scroller-refresh"),this._loadIcon.hide()}}),u=a.Class.extend({init:function(a){var b=this;this.chromeHeight=a.wrapper.children().not(a.element).outerHeight()||0,this.listView=a,this.scroller=a.scroller(),this.options=a.options,a.bind("_dataSource",function(a){b.setDataSource(a.dataSource,a.empty)}),a.bind("resize",function(){b.list.items.length&&(b.scroller.reset(),b.buffer.range(0),b.list.refresh())}),this.scroller.makeVirtual(),this.scroller.bind("scroll",function(a){b.list.update(a.scrollTop)}),this.scroller.bind("scrollEnd",function(a){b.list.batchUpdate(a.scrollTop)})},destroy:function(){this.list.unbind(),this.buffer.unbind()},setDataSource:function(k,n){var g,d,f,c,i=this,l=this.options,e=this.listView,h=e.scroller(),m=l.loadMore;if(this.dataSource=k,g=k.pageSize()||l.virtualViewSize,!g&&!n)throw Error("the DataSource does not have page size configured. Page Size setting is mandatory for the mobile listview virtual scrolling to work as expected.");this.buffer&&this.buffer.destroy(),d=new a.data.Buffer(k,Math.floor(g/2),m),f=m?new A(e,d):new j(e),this.list&&this.list.destroy(),c=new D({buffer:d,footer:f,item:function(a){return new v(e,a)},height:function(){return h.height()}}),c.bind("resize",function(){i.updateScrollerSize()}),c.bind("reset",function(){i.footer.enable()}),c.bind("endReached",function(){f.disable(),i.updateScrollerSize()}),d.bind("expand",function(){c.lastDirection=!1,c.batchUpdate(h.scrollTop)}),b.extend(this,{buffer:d,scroller:h,list:c,footer:f})},updateScrollerSize:function(){this.scroller.virtualSize(0,this.list.totalHeight()+this.chromeHeight)},refresh:function(){this.list.refresh()},reset:function(){this.buffer.range(0),this.list.refresh()}}),y=a.Class.extend({init:function(a){var b,c=this;this.listView=a,this.options=a.options,b=this,this._refreshHandler=function(a){b.refresh(a)},this._progressHandler=function(){a.showLoading()},a.bind("_dataSource",function(a){c.setDataSource(a.dataSource)})},destroy:function(){this._unbindDataSource()},reset:function(){},refresh:function(e){var g,f,h=e&&e.action,b=e&&e.items,a=this.listView,k=this.dataSource,l=this.options.appendOnRefresh,d=k.view(),j=k.group(),i=j&&j[0];return"itemchange"===h?(g=a.findByDataItem(b)[0],g&&a.setDataItem(g,b[0]),z):(a.trigger(q),"add"!==h||i?"remove"!==h||i?i?a.replaceGrouped(d):l&&!a._filter?a.prepend(d):a.replace(d):a.remove(b):(f=d.indexOf(b[0]),f>-1&&a.insertAt(b,f)),this._shouldShowLoading()&&a.hideLoading(),a.trigger(m,{ns:c}),z)},setDataSource:function(a){this.dataSource&&this._unbindDataSource(),this.dataSource=a,a.bind(x,this._refreshHandler),this._shouldShowLoading()&&this.dataSource.bind(w,this._progressHandler)},_unbindDataSource:function(){this.dataSource.unbind(x,this._refreshHandler).unbind(w,this._progressHandler)},_shouldShowLoading:function(){var a=this.options;return!a.pullToRefresh&&!a.loadMore&&!a.endlessScroll}}),s=a.Class.extend({init:function(a){var d=this,b=a.options.filterable,c="change paste";this.listView=a,this.options=b,a.element.before(V({placeholder:b.placeholder||"Search..."})),b.autoFilter!==!1&&(c+=" keyup"),this.element=a.wrapper.find(".km-search-form"),this.searchInput=a.wrapper.find("input[type=search]").closest("form").on("submit"+g,function(a){a.preventDefault()}).end().on("focus"+g,function(){d._oldFilter=d.searchInput.val()}).on(c.split(" ").join(g+" ")+g,h(this._filterChange,this)),this.clearButton=a.wrapper.find(".km-filter-reset").on(l,h(this,"_clearFilter")).hide()},_search:function(a){this._filter=!0,this.clearButton[a?"show":"hide"](),this.listView.dataSource.filter(a)},_filterChange:function(a){var b=this;"paste"==a.type&&this.options.autoFilter!==!1?setTimeout(function(){b._applyFilter()},1):this._applyFilter()},_applyFilter:function(){var b=this.options,a=this.searchInput.val(),c=a.length?{field:b.field,operator:b.operator||"startsWith",ignoreCase:b.ignoreCase,value:a}:null;a!==this._oldFilter&&(this._oldFilter=a,this._search(c))},_clearFilter:function(a){this.searchInput.val(""),this._search(null),a.preventDefault()}}),t=i.extend({init:function(d,b){var e=this;i.fn.init.call(this,d,b),d=this.element,b=this.options,b.scrollTreshold&&(b.scrollThreshold=b.scrollTreshold),d.on("down",C,"_highlight").on("move up cancel",C,"_dim"),this._userEvents=new a.UserEvents(d,{filter:L,allowSelection:!0,tap:function(a){e._click(a)},end:function(b){(a.mobile.appLevelNativeScrolling()||e.viewHasNativeScrolling())&&b.preventDefault()}}),d.css("-ms-touch-action","auto"),d.wrap(J),this.wrapper=this.element.parent(),this._headerFixer=new K(this),this._itemsCache={},this._templates(),this.virtual=b.endlessScroll||b.loadMore,this._style(),this.options.pullToRefresh&&(this._pullToRefreshHandler=new I(this)),this.options.filterable&&(this._filter=new s(this)),this._itemBinder=this.virtual?new u(this):new y(this),this.setDataSource(b.dataSource),this._enhanceItems(this.items()),a.notify(this,c)},events:[l,q,m,n],options:{name:"ListView",style:"",type:"flat",autoBind:!0,fixedHeaders:!1,template:"#:data#",headerTemplate:'<span class="km-text">#:value#</span>',appendOnRefresh:!1,loadMore:!1,loadMoreText:"Press to load more",endlessScroll:!1,scrollThreshold:30,pullToRefresh:!1,pullTemplate:"Pull to refresh",releaseTemplate:"Release to refresh",refreshTemplate:"Refreshing",pullOffset:140,filterable:!1,virtualViewSize:null},refresh:function(){this._itemBinder.refresh()},reset:function(){this._itemBinder.reset()},setDataSource:function(a){var b=!a;this.dataSource=T.create(a),this.trigger("_dataSource",{dataSource:this.dataSource,empty:b}),this.options.autoBind&&!b&&(this.items().remove(),this.dataSource.fetch())},destroy:function(){i.fn.destroy.call(this),a.destroy(this.element),this._userEvents.destroy(),this._itemBinder&&this._itemBinder.destroy(),this.element.unwrap(),delete this.element,delete this.wrapper,delete this._userEvents},items:function(){return"group"===this.options.type?this.element.find(".km-list").children():this.element.children().not(".km-load-more")},scroller:function(){return this._scrollerInstance||(this._scrollerInstance=this.element.closest(".km-scroll-wrapper").data("kendoMobileScroller")),this._scrollerInstance},showLoading:function(){var a=this.view();a&&a.loader&&a.loader.show()},hideLoading:function(){var a=this.view();a&&a.loader&&a.loader.hide()},insertAt:function(d,b){var a=this;return this._renderItems(d,function(e){0===b?a.element.prepend(e):-1===b?a.element.append(e):a.items().eq(b-1).after(e);for(var f=0;e.length>f;f++)a.trigger(n,{item:[e[f]],data:d[f],ns:c})})},append:function(a){return this.insertAt(a,-1)},prepend:function(a){return this.insertAt(a,0)},replace:function(a){return this.options.type="flat",this.element.empty(),this._style(),this.insertAt(a,0)},replaceGrouped:function(d){this.options.type="group",this.element.empty();var c=b(a.render(this.groupTemplate,d));this._enhanceItems(c.children("ul").children("li")),this.element.append(c),p.init(c),this._style()},remove:function(c){var b=this.findByDataItem(c);a.destroy(b),b.remove()},findByDataItem:function(c){var b,e,d=[];for(b=0,e=c.length;e>b;b++)d[b]="[data-"+a.ns+"uid="+c[b].uid+"]";return this.element.find(d.join(","))},setDataItem:function(f,a){var d=this,e=function(g){var e=b(g[0]);b(f).replaceWith(e),d.trigger(n,{item:e,data:a,ns:c})};return this._renderItems([a],e)[0]},_renderItems:function(e,d){var c=b(a.render(this.template,e));return d(c),p.init(c),this._enhanceItems(c),c},_dim:function(a){this._toggle(a,!1)},_highlight:function(a){this._toggle(a,!0)},_toggle:function(a,f){if(!(a.which>1)){var c=b(a.currentTarget),g=c.parent(),h=d(c,"role")||"",e=!h.match(U),i=a.isDefaultPrevented();e&&g.toggleClass(F,f&&!i)}},_templates:function(){var b=this.options.template,d=this.options.headerTemplate,f=' data-uid="#=arguments[0].uid || ""#"',e={},c={};typeof b===B&&(e.template=b,b="#=this.template(data)#"),this.template=h(a.template("<li"+f+">"+b+"</li>"),e),c.template=this.template,typeof d===B&&(c._headerTemplate=d,d="#=this._headerTemplate(data)#"),c.headerTemplate=a.template(d),this.groupTemplate=h(O,c)},_click:function(d){if(!(d.event.which>1||d.event.isDefaultPrevented())){var f,e=d.target,g=b(d.event.target),j=g.closest(a.roleSelector("button","detailbutton","backbutton")),i=a.widgetInstance(j,c),h=e.attr(a.attr("uid"));h&&(f=this.dataSource.getByUid(h)),this.trigger(l,{target:g,item:e,dataItem:f,button:i})&&d.preventDefault()}},_styleGroups:function(){var a=this.element.children();a.children("ul").addClass("km-list"),a.each(function(){var c=b(this),a=c.contents().first();c.addClass("km-group-container"),a.is("ul")||a.is("div."+e)||a.wrap(H)})},_style:function(){var d=this.options,a="group"===d.type,c=this.element,b="inset"===d.style;c.addClass("km-listview").toggleClass("km-list",!a).toggleClass("km-virtual-list",this.virtual).toggleClass("km-listinset",!a&&b).toggleClass("km-listgroup",a&&!b).toggleClass("km-listgroupinset",a&&b),c.parents(".km-listview")[0]||c.closest(".km-content").toggleClass("km-insetcontent",b),a&&this._styleGroups(),this.trigger(r)},_enhanceItems:function(a){a.each(function(){var a,d=b(this),c=!1;d.children().each(function(){a=b(this),a.is("a")?(Q(a),c=!0):a.is("label")&&(E(a),c=!0)}),c||N(d)})}}),c.plugin(t)})(window.kendo.jQuery);(function(a,j){function f(d,c){var e=c.find("["+b.attr("align")+"="+d+"]");return e[0]?a('<div class="km-'+d+'item" />').append(e).prependTo(c):j}function e(b){var d=b.siblings(),c=!!b.children("ul")[0],e=!!d[0]&&""===a.trim(b.text());b.prevAll().toggleClass("km-absolute",c),b.toggleClass("km-show-title",e),b.toggleClass("km-fill-title",e&&!a.trim(b.html())),b.toggleClass("km-no-title",c),b.toggleClass("km-hide-title","hidden"==b.css("visibility")&&!d.children().is(":visible"))}var b=window.kendo,h=b.mobile,d=h.ui,i=b.roleSelector,c=d.Widget,g=c.extend({init:function(d,e){var b=this;c.fn.init.call(b,d,e),d=b.element,b.container().bind("show",a.proxy(this,"refresh")),d.addClass("km-navbar").wrapInner(a('<div class="km-view-title km-show-title" />')),b.leftElement=f("left",d),b.rightElement=f("right",d),b.centerElement=d.find(".km-view-title")},options:{name:"NavBar"},title:function(a){this.element.find(i("view-title")).text(a),e(this.centerElement)},refresh:function(b){var a=b.view;a.options.title?this.title(a.options.title):e(this.centerElement)},destroy:function(){c.fn.destroy.call(this),b.destroy(this.element)}});d.plugin(g)})(window.kendo.jQuery);(function(e,l){var r,p,s,t,n,k,q,z,a=window.kendo,O=a.mobile,A=O.ui,b=e.proxy,h=a.effects.Transition,G=a.ui.Pane,J=a.ui.PaneDimensions,u=A.Widget,E=a.data.DataSource,Q=a.data.Buffer,P=a.data.BatchBuffer,d=Math,i=d.abs,v=d.ceil,x=d.round,N=d.max,M=d.min,y=d.floor,c="change",g="changing",f="refresh",w="km-current-page",B="km-virtual-page",C="function",o="itemChange",I=3,F=-1,L=0,H=1,j=-1,K=0,m=1,D=a.Class.extend({init:function(a){var d=this,g=e("<ol class='km-pages'/>");a.element.append(g),this._changeProxy=b(d,"_change"),this._refreshProxy=b(d,"_refresh"),a.bind(c,this._changeProxy),a.bind(f,this._refreshProxy),e.extend(d,{element:g,scrollView:a})},items:function(){return this.element.children()},_refresh:function(a){for(var b="",c=0;a.pageCount>c;c++)b+="<li/>";this.element.html(b),this.items().eq(a.page).addClass(w)},_change:function(a){this.items().removeClass(w).eq(a.page).addClass(w)},destroy:function(){this.scrollView.unbind(c,this._changeProxy),this.scrollView.unbind(f,this._refreshProxy),this.element.remove()}});a.mobile.ui.ScrollViewPager=D,r="transitionEnd",p="dragStart",s="dragEnd",t=a.Observable.extend({init:function(l,j){var f,k,d,g,m,n,b=this;a.Observable.fn.init.call(this),this.element=l,this.container=l.parent(),f=new a.ui.Movable(b.element),k=new h({axis:"x",movable:f,onEnd:function(){b.trigger(r)}}),d=new a.UserEvents(l,{start:function(a){2*i(a.x.velocity)>=i(a.y.velocity)?d.capture():d.cancel(),b.trigger(p,a),k.cancel()},allowSelection:!0,end:function(a){b.trigger(s,a)}}),g=new J({element:b.element,container:b.container}),m=g.x,m.bind(c,function(){b.trigger(c)}),n=new G({dimensions:g,userEvents:d,movable:f,elastic:!0}),e.extend(b,{duration:j&&j.duration||1,movable:f,transition:k,userEvents:d,dimensions:g,dimension:m,pane:n}),this.bind([r,p,s,c],j)},size:function(){return{width:this.dimensions.x.getSize(),height:this.dimensions.y.getSize()}},total:function(){return this.dimension.getTotal()},offset:function(){return-this.movable.x},updateDimension:function(){this.dimension.update(!0)},refresh:function(){this.dimensions.refresh()},moveTo:function(a){this.movable.moveAxis("x",-a)},transitionTo:function(a,c,b){b?this.moveTo(-a):this.transition.moveTo({location:a,duration:this.duration,ease:c})}}),a.mobile.ui.ScrollViewElasticPane=t,n=a.Observable.extend({init:function(d,e,c){var b=this;a.Observable.fn.init.call(this),b.element=d,b.pane=e,b._getPages(),this.page=0,this.pageSize=c.pageSize||1,this.contentHeight=c.contentHeight,this.enablePager=c.enablePager},scrollTo:function(a,b){this.page=a,this.pane.transitionTo(-a*this.pane.size().width,h.easeOutExpo,b)},paneMoved:function(f,n,i,l){var e,b,a=this,c=a.pane,g=c.size().width*a.pageSize,d=x,k=n?h.easeOutBack:h.easeOutExpo;f===j?(d=v):f===m&&(d=y),b=d(c.offset()/g),e=N(a.minSnap,M(-b*g,a.maxSnap)),b!=a.page&&i&&i({currentPage:a.page,nextPage:b})&&(e=-a.page*c.size().width),c.transitionTo(e,k,l)},updatePage:function(){var a=this.pane,b=x(a.offset()/a.size().width);return b!=this.page?(this.page=b,!0):!1},forcePageUpdate:function(){return this.updatePage()},resizeTo:function(e){var b,d,c=this.pane,a=e.width;this.pageElements.width(a),"100%"===this.contentHeight&&(b=this.element.parent().height(),this.enablePager===!0&&(d=this.element.parent().find("ol.km-pages"),d.length&&(b-=d.outerHeight(!0))),this.element.css("height",b),this.pageElements.css("height",b)),c.updateDimension(),this._paged||(this.page=y(c.offset()/a)),this.scrollTo(this.page,!0),this.pageCount=v(c.total()/a),this.minSnap=-(this.pageCount-1)*a,this.maxSnap=0},_getPages:function(){this.pageElements=this.element.find("[data-role=page]"),this._paged=this.pageElements.length>0}}),a.mobile.ui.ScrollViewContent=n,k=a.Observable.extend({init:function(d,e,c){var b=this;a.Observable.fn.init.call(this),b.element=d,b.pane=e,b.options=c,b._templates(),b.page=c.page||0,b.pages=[],b._initPages(),b.resizeTo(b.pane.size()),b.pane.dimension.forceEnabled()},setDataSource:function(a){this.dataSource=E.create(a),this._buffer(),this._pendingPageRefresh=!1,this._pendingWidgetRefresh=!1},_viewShow:function(){var a=this;a._pendingWidgetRefresh&&(setTimeout(function(){a._resetPages()},0),a._pendingWidgetRefresh=!1)},_buffer:function(){var a=this.options.itemsPerPage;this.buffer&&this.buffer.destroy(),this.buffer=a>1?new P(this.dataSource,a):new Q(this.dataSource,3*a),this._resizeProxy=b(this,"_onResize"),this._resetProxy=b(this,"_onReset"),this._endReachedProxy=b(this,"_onEndReached"),this.buffer.bind({resize:this._resizeProxy,reset:this._resetProxy,endreached:this._endReachedProxy})},_templates:function(){var c=this.options.template,d=this.options.emptyTemplate,f={},e={};typeof c===C&&(f.template=c,c="#=this.template(data)#"),this.template=b(a.template(c),f),typeof d===C&&(e.emptyTemplate=d,d="#=this.emptyTemplate(data)#"),this.emptyTemplate=b(a.template(d),e)},_initPages:function(){for(var a,d=this.pages,c=this.element,b=0;I>b;b++)a=new q(c),d.push(a);this.pane.updateDimension()},resizeTo:function(e){for(var a,c,b=this.pages,f=this.pane,d=0;b.length>d;d++)b[d].setWidth(e.width);"auto"===this.options.contentHeight?this.element.css("height",this.pages[1].element.height()):"100%"===this.options.contentHeight&&(a=this.element.parent().height(),this.options.enablePager===!0&&(c=this.element.parent().find("ol.km-pages"),c.length&&(a-=c.outerHeight(!0))),this.element.css("height",a),b[0].element.css("height",a),b[1].element.css("height",a),b[2].element.css("height",a)),f.updateDimension(),this._repositionPages(),this.width=e.width},scrollTo:function(a){var c,b=this.buffer;b.syncDataSource(),c=b.at(a),c&&(this._updatePagesContent(a),this.page=a)},paneMoved:function(i,o,l,e){var d,c=this,g=c.pane,n=g.size().width,f=g.offset(),h=Math.abs(f)>=n/3,p=o?a.effects.Transition.easeOutBack:a.effects.Transition.easeOutExpo,k=c.page+2>c.buffer.total(),b=0;i===m?0!==c.page&&(b=-1):i!==j||k?f>0&&h&&!k?(b=1):0>f&&h&&0!==c.page&&(b=-1):(b=1),d=c.page,b&&(d=b>0?d+1:d-1),l&&l({currentPage:c.page,nextPage:d})&&(b=0),0===b?c._cancelMove(p,e):-1===b?c._moveBackward(e):1===b&&c._moveForward(e)},updatePage:function(){var a=this.pages;return 0===this.pane.offset()?!1:(this.pane.offset()>0?(a.push(this.pages.shift()),this.page++,this.setPageContent(a[2],this.page+1)):(a.unshift(this.pages.pop()),this.page--,this.setPageContent(a[0],this.page-1)),this._repositionPages(),this._resetMovable(),!0)},forcePageUpdate:function(){var a=this.pane.offset(),b=3*this.pane.size().width/4;return i(a)>b?this.updatePage():!1},_resetMovable:function(){this.pane.moveTo(0)},_moveForward:function(b){this.pane.transitionTo(-this.width,a.effects.Transition.easeOutExpo,b)},_moveBackward:function(b){this.pane.transitionTo(this.width,a.effects.Transition.easeOutExpo,b)},_cancelMove:function(a,b){this.pane.transitionTo(0,a,b)},_resetPages:function(){this.page=this.options.page||0,this._updatePagesContent(this.page),this._repositionPages(),this.trigger("reset")},_onResize:function(){var a=this.pages[2],b=this.page+1;this._pendingPageRefresh&&(this.setPageContent(a,b),this._pendingPageRefresh=!1)},_onReset:function(){this.pageCount=v(this.dataSource.total()/this.options.itemsPerPage),this.element.is(":visible")?this._resetPages():(this._widgetNeedsRefresh=!0)},_onEndReached:function(){this._pendingPageRefresh=!0},_repositionPages:function(){var a=this.pages;a[0].position(F),a[1].position(L),a[2].position(H)},_updatePagesContent:function(c){var b=this.pages,a=c||0;this.setPageContent(b[0],a-1),this.setPageContent(b[1],a),this.setPageContent(b[2],a+1)},setPageContent:function(c,d){var f=this.buffer,g=this.template,h=this.emptyTemplate,b=null;d>=0&&(b=f.at(d),e.isArray(b)&&!b.length&&(b=null)),b?c.content(g(b)):c.content(h({})),a.mobile.init(c.element),this.trigger(o,{item:c.element,data:b,ns:a.mobile.ui})}}),a.mobile.ui.VirtualScrollViewContent=k,q=a.Class.extend({init:function(a){this.element=e("<div class='"+B+"'></div>"),this.width=a.width(),this.element.width(this.width),a.append(this.element)},content:function(a){this.element.html(a)},position:function(a){this.element.css("transform","translate3d("+this.width*a+"px, 0, 0)")},setWidth:function(a){this.width=a,this.element.width(a)}}),a.mobile.ui.VirtualPage=q,z=u.extend({init:function(e,d){var h,g,c=this;u.fn.init.call(c,e,d),d=c.options,e=c.element,a.stripWhitespace(e[0]),e.wrapInner("<div/>").addClass("km-scrollview"),this.options.enablePager&&(this.pager=new D(this)),c.inner=e.children().first(),c.page=0,c.inner.css("height",d.contentHeight),c.pane=new t(c.inner,{duration:this.options.duration,transitionEnd:b(this,"_transitionEnd"),dragStart:b(this,"_dragStart"),dragEnd:b(this,"_dragEnd"),change:b(this,f)}),c.bind("resize",function(){c.pane.refresh()}),c.page=d.page,h=0===this.inner.children().length,c._content=h?new k(c.inner,c.pane,d):new n(c.inner,c.pane,d),c._content.page=c.page,c._content.bind("reset",function(){var a=c._content;c._syncWithContent(),c.trigger(f,{pageCount:a.pageCount,page:a.page})}),c._content.bind(o,function(a){c.trigger(o,a)}),c.setDataSource(d.dataSource),g=c.container(),g.nullObject?(c.viewInit(),c.viewShow()):g.bind("show",b(this,"viewShow")).bind("init",b(this,"viewInit"))},options:{name:"ScrollView",page:0,duration:400,velocityThreshold:.8,contentHeight:"auto",pageSize:1,itemsPerPage:1,bounceVelocityThreshold:1.6,enablePager:!0,autoBind:!0,template:"",emptyTemplate:""},events:[g,c,f],destroy:function(){u.fn.destroy.call(this),a.destroy(this.element)},viewInit:function(){this.options.autoBind&&this._content.scrollTo(this._content.page,!0)},viewShow:function(){this.pane.refresh()},refresh:function(){var a=this._content;a.resizeTo(this.pane.size()),this.page=a.page,this.trigger(f,{pageCount:a.pageCount,page:a.page})},content:function(a){this.element.children().first().html(a),this._content._getPages(),this.pane.refresh()},scrollTo:function(a,b){this._content.scrollTo(a,b),this._syncWithContent()},prev:function(){var a=this;a._content.paneMoved(m,l,function(b){return a.trigger(g,b)})},next:function(){var a=this;a._content.paneMoved(j,l,function(b){return a.trigger(g,b)})},setDataSource:function(a){if(this._content instanceof k){var b=!a;this.dataSource=E.create(a),this._content.setDataSource(this.dataSource),this.options.autoBind&&!b&&this.dataSource.fetch()}},items:function(){return this.element.find("."+B)},_syncWithContent:function(){var a,d,b=this._content.pages,e=this._content.buffer;this.page=this._content.page,a=e?e.at(this.page):l,a instanceof Array||(a=[a]),d=b?b[1].element:l,this.trigger(c,{page:this.page,element:d,data:a})},_dragStart:function(){this._content.forcePageUpdate()&&this._syncWithContent()},_dragEnd:function(d){var f=this,a=d.x.velocity,c=this.options.velocityThreshold,b=K,e=i(a)>this.options.bounceVelocityThreshold;a>c?(b=m):-c>a&&(b=j),this._content.paneMoved(b,e,function(a){return f.trigger(g,a)})},_transitionEnd:function(){this._content.updatePage()&&this._syncWithContent()}}),A.plugin(z)})(window.kendo.jQuery);(function(h,f){function q(a,c,b){return Math.max(c,Math.min(b,a))}var a=window.kendo,j=a.mobile.ui,e=j.Widget,m=a.support,k="change",i="km-switch-on",g="km-switch-off",c="margin-left",l="km-state-active",o="km-state-disabled",b="disabled",n=m.transitions.css+"transform",d=h.proxy,p='<span class="km-switch km-widget">        <span class="km-switch-wrapper"><span class="km-switch-background"></span></span>         <span class="km-switch-container"><span class="km-switch-handle" >             <span class="km-switch-label-on">{0}</span>             <span class="km-switch-label-off">{1}</span>         </span>     </span>',r=e.extend({init:function(i,f){var g,d=this;e.fn.init.call(d,i,f),f=d.options,d.wrapper=h(a.format(p,f.onLabel,f.offLabel)),d.handle=d.wrapper.find(".km-switch-handle"),d.background=d.wrapper.find(".km-switch-background"),d.wrapper.insertBefore(d.element).prepend(d.element),d._drag(),d.origin=parseInt(d.background.css(c),10),d.constrain=0,d.snapPoint=0,i=d.element[0],i.type="checkbox",d._animateBackground=!0,g=d.options.checked,null===g&&(g=i.checked),d.check(g),d.options.enable=d.options.enable&&!d.element.attr(b),d.enable(d.options.enable),d.refresh(),a.notify(d,a.mobile.ui)},refresh:function(){var a=this,b=a.handle.outerWidth(!0);a.width=a.wrapper.width(),a.constrain=a.width-b,a.snapPoint=a.constrain/2,"number"!=typeof a.origin&&(a.origin=parseInt(a.background.css(c),10)),a.background.data("origin",a.origin),a.check(a.element[0].checked)},events:[k],options:{name:"Switch",onLabel:"on",offLabel:"off",checked:null,enable:!0},check:function(a){var b=this,c=b.element[0];return a===f?c.checked:(b._position(a?b.constrain:0),c.checked=a,b.wrapper.toggleClass(i,a).toggleClass(g,!a),f)},destroy:function(){e.fn.destroy.call(this),this.userEvents.destroy()},toggle:function(){var a=this;a.check(!a.element[0].checked)},enable:function(a){var c=this.element,d=this.wrapper;f===a&&(a=!0),this.options.enable=a,a?c.removeAttr(b):c.attr(b,b),d.toggleClass(o,!a)},_resize:function(){this.refresh()},_move:function(b){var a=this;b.preventDefault(),a._position(q(a.position+b.x.delta,0,a.width-a.handle.outerWidth(!0)))},_position:function(b){var a=this;a.position=b,a.handle.css(n,"translatex("+b+"px)"),a._animateBackground&&a.background.css(c,a.origin+b)},_start:function(){this.options.enable?(this.userEvents.capture(),this.handle.addClass(l)):this.userEvents.cancel()},_stop:function(){var a=this;a.handle.removeClass(l),a._toggle(a.position>a.snapPoint)},_toggle:function(c){var d,b=this,j=b.handle,f=b.element[0],h=f.checked,e=a.mobile.application&&a.mobile.application.os.wp?100:200;b.wrapper.toggleClass(i,c).toggleClass(g,!c),b.position=d=c*b.constrain,b._animateBackground&&b.background.kendoStop(!0,!0).kendoAnimate({effects:"slideMargin",offset:d,reset:!0,reverse:!c,axis:"left",duration:e}),j.kendoStop(!0,!0).kendoAnimate({effects:"slideTo",duration:e,offset:d+"px,0",reset:!0,complete:function(){h!==c&&(f.checked=c,b.trigger(k,{checked:c}))}})},_drag:function(){var b=this;b.userEvents=new a.UserEvents(b.wrapper,{tap:function(){b.options.enable&&b._toggle(!b.element[0].checked)},start:d(b._start,b),move:d(b._move,b),end:d(b._stop,b)})}});j.plugin(r)})(window.kendo.jQuery);(function(a){function g(b){return a('<span class="km-badge">'+b+"</span>")}var b=window.kendo,e=b.mobile.ui,f=e.Widget,c="km-state-active",d="select",h=f.extend({init:function(e,d){var b=this;f.fn.init.call(b,e,d),b.container().bind("show",a.proxy(this,"refresh")),b.element.addClass("km-tabstrip").find("a").each(b._buildButton).eq(b.options.selectedIndex).addClass(c),b.element.on("down","a","_release")},events:[d],switchTo:function(b){var d,c,e=this.element.find("a"),f=0,g=e.length;if(!isNaN(b))return this._setActiveItem(e.eq(b)),!0;for(;g>f;f++)if(d=e[f],c=d.href.replace(/(\#.+)(\?.+)$/,"$1"),-1!==c.indexOf(b,c.length-b.length))return this._setActiveItem(a(d)),!0;return!1},switchByFullUrl:function(b){var a;a=this.element.find("a[href$='"+b+"']"),this._setActiveItem(a)},clear:function(){this.currentItem().removeClass(c)},currentItem:function(){return this.element.children("."+c)},badge:function(b,c){var d,e=this.element;return isNaN(b)||(b=e.children().get(b)),b=e.find(b),d=a(b.find(".km-badge")[0]||g(c).insertAfter(b.children(".km-icon"))),c||0===c?(d.html(c),this):c===!1?(d.empty().remove(),this):d.html()},_release:function(e){if(!(e.which>1)){var c=this,b=a(e.currentTarget);b[0]!==c.currentItem()[0]&&(c.trigger(d,{item:b})?e.preventDefault():c._setActiveItem(b))}},_setActiveItem:function(a){a[0]&&(this.clear(),a.addClass(c))},_buildButton:function(){var c=a(this),h=b.attrValue(c,"icon"),e=b.attrValue(c,"badge"),d=c.find("img"),f=a('<span class="km-icon"/>');c.addClass("km-button").attr(b.attr("role"),"tab").contents().not(d).wrapAll('<span class="km-text"/>'),d[0]?d.addClass("km-image").prependTo(c):(c.prepend(f),h&&(f.addClass("km-"+h),(e||0===e)&&g(e).insertAfter(f)))},refresh:function(a){var c=a.view.element.attr(b.attr("url"));!this.switchTo(a.view.id)&&c&&this.switchTo(c)},options:{name:"TabStrip",selectedIndex:0,enable:!0}});e.plugin(h)})(window.kendo.jQuery);return window.kendo},typeof define=="function"&&define.amd?define:function(b,a){a()});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\base\js\lib\json2.js */ 
if(window.Prototype){delete Object.prototype.toJSON;delete Array.prototype.toJSON;delete Hash.prototype.toJSON;delete String.prototype.toJSON}if(!this.JSON)this.JSON={};(function(){function c(a){return a<10?"0"+a:a}if(typeof Date.prototype.toJSON!=="function"){Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+c(this.getUTCMonth()+1)+"-"+c(this.getUTCDate())+"T"+c(this.getUTCHours())+":"+c(this.getUTCMinutes())+":"+c(this.getUTCSeconds())+"Z":null};String.prototype.toJSON=Number.prototype.toJSON=Boolean.prototype.toJSON=function(){return this.valueOf()}}var h=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,f=/[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,a,d,i={"\b":"\\b","\t":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},b;function g(a){f.lastIndex=0;return f.test(a)?'"'+a.replace(f,function(a){var b=i[a];return typeof b==="string"?b:"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+a+'"'}function e(n,m){var i,j,h,k,l=a,f,c=m[n];if(typeof b==="function")c=b.call(m,n,c);switch(typeof c){case"string":return g(c);case"number":return isFinite(c)?String(c):"null";case"boolean":case"null":return String(c);case"object":if(!c)return"null";a+=d;f=[];if(Object.prototype.toString.apply(c)==="[object Array]"){k=c.length;for(i=0;i<k;i+=1)f[i]=e(i,c)||"null";h=f.length===0?"[]":a?"[\n"+a+f.join(",\n"+a)+"\n"+l+"]":"["+f.join(",")+"]";a=l;return h}if(b&&typeof b==="object"){k=b.length;for(i=0;i<k;i+=1){j=b[i];if(typeof j==="string"){h=e(j,c);h&&f.push(g(j)+(a?": ":":")+h)}}}else for(j in c)if(Object.hasOwnProperty.call(c,j)){h=e(j,c);h&&f.push(g(j)+(a?": ":":")+h)}h=f.length===0?"{}":a?"{\n"+a+f.join(",\n"+a)+"\n"+l+"}":"{"+f.join(",")+"}";a=l;return h}}if(typeof JSON.encode!=="function")JSON.encode=function(h,c,f){var g;a="";d="";if(typeof f==="number")for(g=0;g<f;g+=1)d+=" ";else if(typeof f==="string")d=f;b=c;if(c&&typeof c!=="function"&&(typeof c!=="object"||typeof c.length!=="number"))throw new Error("JSON.encode");return e("",{"":h})};if(typeof JSON.parse!=="function")JSON.parse=function(a,c){var b;function d(f,g){var b,e,a=f[g];if(a&&typeof a==="object")for(b in a)if(Object.hasOwnProperty.call(a,b)){e=d(a,b);if(e!==undefined)a[b]=e;else delete a[b]}return c.call(f,g,a)}a=String(a);h.lastIndex=0;if(h.test(a))a=a.replace(h,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)});if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,""))){b=eval("("+a+")");return typeof c==="function"?d({"":b},""):b}throw new SyntaxError("JSON.parse");}})();
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\base\js\lib\jquery.class.js */ 
(function(b){var a={undHash:/_|-/,colons:/::/,words:/([A-Z]+)([A-Z][a-z])/g,lowUp:/([a-z\d])([A-Z])/g,dash:/([a-z\d])([A-Z])/g,replacer:/\{([^\}]+)\}/g,dot:/\./},f=function(b,a,c){return b[a]||c&&(b[a]={})},d=function(b){var a=typeof b;return a&&(a=="function"||a=="object")},e=function(k,h,i){var g=k?k.split(a.dot):[],m=g.length,l=b.isArray(h)?h:[h||window],c,j,e,n=0,o;if(m==0)return l[0];while(c=l[n++]){for(e=0;e<m-1&&d(c);e++)c=f(c,g[e],i);if(d(c)){j=f(c,g[e],i);if(j!==undefined){if(i===false)delete c[g[e]];return j}}}},c=b.String=b.extend(b.String||{},{getObject:e,capitalize:function(a){return a.charAt(0).toUpperCase()+a.substr(1)},camelize:function(a){a=c.classize(a);return a.charAt(0).toLowerCase()+a.substr(1)},classize:function(f,e){for(var b=f.split(a.undHash),d=0;d<b.length;d++)b[d]=c.capitalize(b[d]);return b.join(e||"")},niceName:function(){c.classize(parts[i]," ")},underscore:function(b){return b.replace(a.colons,"/").replace(a.words,"$1_$2").replace(a.lowUp,"$1_$2").replace(a.dash,"_").toLowerCase()},sub:function(f,d,c){var b=[];b.push(f.replace(a.replacer,function(h,g){var f=e(g,d,typeof c=="boolean"?!c:c),a=typeof f;if((a==="object"||a==="function")&&a!==null){b.push(f);return""}else return""+f}));return b.length<=1?b[0]:b}})})(jQuery);(function(b){var c=false,i=b.makeArray,h=b.isFunction,d=b.isArray,e=b.extend,g=function(b,a){return b.concat(i(a))},j=/xyz/.test(function(){xyz})?/\b_super\b/:/.*/,f=function(b,d,c){c=c||b;for(var a in b)c[a]=h(b[a])&&h(d[a])&&j.test(b[a])?function(a,b){return function(){var e=this._super,c;this._super=d[a];c=b.apply(this,arguments);this._super=e;return c}}(a,b[a]):b[a]},a=b.Class=function(){arguments.length&&a.extend.apply(a,arguments)};e(a,{callback:function(a){var c=i(arguments),b;a=c.shift();if(!d(a))a=[a];b=this;return function(){for(var e=g(c,arguments),i,j=a.length,h=0,f;h<j;h++){f=a[h];if(!f)continue;i=typeof f=="string";if(i&&b._set_called)b.called=f;e=(i?b[f]:f).apply(b,e||[]);if(h<j-1)e=!d(e)||e._use_call?[e]:e}return e}},getObject:b.String.getObject,newInstance:function(){var a=this.rawInstance(),b;if(a.setup)b=a.setup.apply(a,arguments);a.init&&a.init.apply(a,d(b)?b:arguments);return a},setup:function(a){this.defaults=e(true,{},a.defaults,this.defaults);return arguments},rawInstance:function(){c=true;var a=new this;c=false;return a},extend:function(d,i,h){if(typeof d!="string"){h=i;i=d;d=null}if(!h){h=i;i=null}h=h||{};var o=this,q=this.prototype,j,l,p,k;c=true;k=new this;c=false;f(h,q,k);function b(){return c?void 0:this.constructor!==b&&arguments.length?arguments.callee.extend.apply(arguments.callee,arguments):this.Class.newInstance.apply(this.Class,arguments)}for(j in this)if(this.hasOwnProperty(j))b[j]=this[j];f(i,this,b);if(d){var n=d.split(/\./),l=n.pop(),m=a.getObject(n.join("."),window,true),p=m;m[l]=b}e(b,{prototype:k,"namespace":p,shortName:l,constructor:b,fullName:d});b.prototype.Class=b.prototype.constructor=b;var r=b.setup.apply(b,g([o],arguments));b.init&&b.init.apply(b,r||[]);return b}});a.prototype.callback=a.callback})(jQuery);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\jquery.ui\js\tablet\jquery.ui.js */ 
/* jQuery UI - v1.10.2 - 2013-03-14
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.draggable.js, jquery.ui.droppable.js, jquery.ui.resizable.js, jquery.ui.selectable.js, jquery.ui.sortable.js, jquery.ui.effect.js, jquery.ui.accordion.js, jquery.ui.autocomplete.js, jquery.ui.button.js, jquery.ui.datepicker.js, jquery.ui.dialog.js, jquery.ui.effect-blind.js, jquery.ui.effect-bounce.js, jquery.ui.effect-clip.js, jquery.ui.effect-drop.js, jquery.ui.effect-explode.js, jquery.ui.effect-fade.js, jquery.ui.effect-fold.js, jquery.ui.effect-highlight.js, jquery.ui.effect-pulsate.js, jquery.ui.effect-scale.js, jquery.ui.effect-shake.js, jquery.ui.effect-slide.js, jquery.ui.effect-transfer.js, jquery.ui.menu.js, jquery.ui.position.js, jquery.ui.progressbar.js, jquery.ui.slider.js, jquery.ui.spinner.js, jquery.ui.tabs.js, jquery.ui.tooltip.js
* Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */
(function(a,c){var f=0,e=/^ui-id-\d+$/;a.ui=a.ui||{};a.extend(a.ui,{version:"1.10.2",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}});a.fn.extend({focus:function(b){return function(c,d){return typeof c==="number"?this.each(function(){var b=this;setTimeout(function(){a(b).focus();d&&d.call(b)},c)}):b.apply(this,arguments)}}(a.fn.focus),scrollParent:function(){var b;if(a.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position")))b=this.parents().filter(function(){return/(relative|absolute|fixed)/.test(a.css(this,"position"))&&/(auto|scroll)/.test(a.css(this,"overflow")+a.css(this,"overflow-y")+a.css(this,"overflow-x"))}).eq(0);else b=this.parents().filter(function(){return/(auto|scroll)/.test(a.css(this,"overflow")+a.css(this,"overflow-y")+a.css(this,"overflow-x"))}).eq(0);return/fixed/.test(this.css("position"))||!b.length?a(document):b},zIndex:function(f){if(f!==c)return this.css("zIndex",f);if(this.length){var b=a(this[0]),d,e;while(b.length&&b[0]!==document){d=b.css("position");if(d==="absolute"||d==="relative"||d==="fixed"){e=parseInt(b.css("zIndex"),10);if(!isNaN(e)&&e!==0)return e}b=b.parent()}}return 0},uniqueId:function(){return this.each(function(){if(!this.id)this.id="ui-id-"+ ++f})},removeUniqueId:function(){return this.each(function(){e.test(this.id)&&a(this).removeAttr("id")})}});function b(b,h){var g,e,f,c=b.nodeName.toLowerCase();if("area"===c){g=b.parentNode;e=g.name;if(!b.href||!e||g.nodeName.toLowerCase()!=="map")return false;f=a("img[usemap=#"+e+"]")[0];return!!f&&d(f)}return(/input|select|textarea|button|object/.test(c)?!b.disabled:"a"===c?b.href||h:h)&&d(b)}function d(b){return a.expr.filters.visible(b)&&!a(b).parents().addBack().filter(function(){return a.css(this,"visibility")==="hidden"}).length}a.extend(a.expr[":"],{data:a.expr.createPseudo?a.expr.createPseudo(function(b){return function(c){return!!a.data(c,b)}}):function(c,d,b){return!!a.data(c,b[3])},focusable:function(c){return b(c,!isNaN(a.attr(c,"tabindex")))},tabbable:function(e){var d=a.attr(e,"tabindex"),c=isNaN(d);return(c||d>=0)&&b(e,!c)}});!a("<a>").outerWidth(1).jquery&&a.each(["Width","Height"],function(h,b){var g=b==="Width"?["Left","Right"]:["Top","Bottom"],f=b.toLowerCase(),e={innerWidth:a.fn.innerWidth,innerHeight:a.fn.innerHeight,outerWidth:a.fn.outerWidth,outerHeight:a.fn.outerHeight};function d(c,b,d,e){a.each(g,function(){b-=parseFloat(a.css(c,"padding"+this))||0;if(d)b-=parseFloat(a.css(c,"border"+this+"Width"))||0;if(e)b-=parseFloat(a.css(c,"margin"+this))||0});return b}a.fn["inner"+b]=function(g){return g===c?e["inner"+b].call(this):this.each(function(){a(this).css(f,d(this,g)+"px")})};a.fn["outer"+b]=function(c,g){return typeof c!=="number"?e["outer"+b].call(this,c):this.each(function(){a(this).css(f,d(this,c,true,g)+"px")})}});if(!a.fn.addBack)a.fn.addBack=function(a){return this.add(a==null?this.prevObject:this.prevObject.filter(a))};if(a("<a>").data("a-b","a").removeData("a-b").data("a-b"))a.fn.removeData=function(b){return function(c){return arguments.length?b.call(this,a.camelCase(c)):b.call(this)}}(a.fn.removeData);a.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());a.support.selectstart="onselectstart"in document.createElement("div");a.fn.extend({disableSelection:function(){return this.bind((a.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(a){a.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}});a.extend(a.ui,{plugin:{add:function(e,f,d){var b,c=a.ui[e].prototype;for(b in d){c.plugins[b]=c.plugins[b]||[];c.plugins[b].push([f,d[b]])}},call:function(a,e,d){var b,c=a.plugins[e];if(!c||!a.element[0].parentNode||a.element[0].parentNode.nodeType===11)return;for(b=0;b<c.length;b++)a.options[c[b][0]]&&c[b][1].apply(a.element,d)}},hasScroll:function(b,e){if(a(b).css("overflow")==="hidden")return false;var c=e&&e==="left"?"scrollLeft":"scrollTop",d=false;if(b[c]>0)return true;b[c]=1;d=b[c]>0;b[c]=0;return d}})})(jQuery);(function(a,b){var e=0,c=Array.prototype.slice,d=a.cleanData;a.cleanData=function(b){for(var e=0,c;(c=b[e])!=null;e++)try{a(c).triggerHandler("remove")}catch(f){}d(b)};a.widget=function(b,f,h){var i,d,c,g,j={},e=b.split(".")[0];b=b.split(".")[1];i=e+"-"+b;if(!h){h=f;f=a.Widget}a.expr[":"][i.toLowerCase()]=function(b){return!!a.data(b,i)};a[e]=a[e]||{};d=a[e][b];c=a[e][b]=function(b,a){if(!this._createWidget)return new c(b,a);arguments.length&&this._createWidget(b,a)};a.extend(c,d,{version:h.version,_proto:a.extend({},h),_childConstructors:[]});g=new f;g.options=a.widget.extend({},g.options);a.each(h,function(b,c){if(!a.isFunction(c)){j[b]=c;return}j[b]=function(){var d=function(){return f.prototype[b].apply(this,arguments)},a=function(a){return f.prototype[b].apply(this,a)};return function(){var f=this._super,e=this._superApply,b;this._super=d;this._superApply=a;b=c.apply(this,arguments);this._super=f;this._superApply=e;return b}}()});c.prototype=a.widget.extend(g,{widgetEventPrefix:d?g.widgetEventPrefix:b},j,{constructor:c,"namespace":e,widgetName:b,widgetFullName:i});if(d){a.each(d._childConstructors,function(e,d){var b=d.prototype;a.widget(b.namespace+"."+b.widgetName,c,d._proto)});delete d._childConstructors}else f._childConstructors.push(c);a.widget.bridge(b,c)};a.widget.extend=function(g){for(var h=c.call(arguments,1),f=0,i=h.length,d,e;f<i;f++)for(d in h[f]){e=h[f][d];if(h[f].hasOwnProperty(d)&&e!==b)if(a.isPlainObject(e))g[d]=a.isPlainObject(g[d])?a.widget.extend({},g[d],e):a.widget.extend({},e);else g[d]=e}return g};a.widget.bridge=function(d,f){var e=f.prototype.widgetFullName||d;a.fn[d]=function(g){var j=typeof g==="string",i=c.call(arguments,1),h=this;g=!j&&i.length?a.widget.extend.apply(null,[g].concat(i)):g;if(j)this.each(function(){var c,f=a.data(this,e);if(!f)return a.error("cannot call methods on "+d+" prior to initialization; attempted to call method '"+g+"'");if(!a.isFunction(f[g])||g.charAt(0)==="_")return a.error("no such method '"+g+"' for "+d+" widget instance");c=f[g].apply(f,i);if(c!==f&&c!==b){h=c&&c.jquery?h.pushStack(c.get()):c;return false}});else this.each(function(){var b=a.data(this,e);if(b)b.option(g||{})._init();else a.data(this,e,new f(g,this))});return h}};a.Widget=function(){};a.Widget._childConstructors=[];a.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:false,create:null},_createWidget:function(c,b){b=a(b||this.defaultElement||this)[0];this.element=a(b);this.uuid=e++;this.eventNamespace="."+this.widgetName+this.uuid;this.options=a.widget.extend({},this.options,this._getCreateOptions(),c);this.bindings=a();this.hoverable=a();this.focusable=a();if(b!==this){a.data(b,this.widgetFullName,this);this._on(true,this.element,{remove:function(a){a.target===b&&this.destroy()}});this.document=a(b.style?b.ownerDocument:b.document||b);this.window=a(this.document[0].defaultView||this.document[0].parentWindow)}this._create();this._trigger("create",null,this._getCreateEventData());this._init()},_getCreateOptions:a.noop,_getCreateEventData:a.noop,_create:a.noop,_init:a.noop,destroy:function(){this._destroy();this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(a.camelCase(this.widgetFullName));this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled ui-state-disabled");this.bindings.unbind(this.eventNamespace);this.hoverable.removeClass("ui-state-hover");this.focusable.removeClass("ui-state-focus")},_destroy:a.noop,widget:function(){return this.element},option:function(c,h){var g=c,e,d,f;if(arguments.length===0)return a.widget.extend({},this.options);if(typeof c==="string"){g={};e=c.split(".");c=e.shift();if(e.length){d=g[c]=a.widget.extend({},this.options[c]);for(f=0;f<e.length-1;f++){d[e[f]]=d[e[f]]||{};d=d[e[f]]}c=e.pop();if(h===b)return d[c]===b?null:d[c];d[c]=h}else{if(h===b)return this.options[c]===b?null:this.options[c];g[c]=h}}this._setOptions(g);return this},_setOptions:function(b){var a;for(a in b)this._setOption(a,b[a]);return this},_setOption:function(b,a){this.options[b]=a;if(b==="disabled"){this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!a).attr("aria-disabled",a);this.hoverable.removeClass("ui-state-hover");this.focusable.removeClass("ui-state-focus")}return this},enable:function(){return this._setOption("disabled",false)},disable:function(){return this._setOption("disabled",true)},_on:function(c,b,d){var f,e=this;if(typeof c!=="boolean"){d=b;b=c;c=false}if(!d){d=b;b=this.element;f=this.widget()}else{b=f=a(b);this.bindings=this.bindings.add(b)}a.each(d,function(k,d){function g(){return!c&&(e.options.disabled===true||a(this).hasClass("ui-state-disabled"))?void 0:(typeof d==="string"?e[d]:d).apply(e,arguments)}if(typeof d!=="string")g.guid=d.guid=d.guid||g.guid||a.guid++;var j=k.match(/^(\w+)\s*(.*)$/),h=j[1]+e.eventNamespace,i=j[2];if(i)f.delegate(i,h,g);else b.bind(h,g)})},_off:function(b,a){a=(a||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace;b.unbind(a).undelegate(a)},_delay:function(a,d){function c(){return(typeof a==="string"?b[a]:a).apply(b,arguments)}var b=this;return setTimeout(c,d||0)},_hoverable:function(b){this.hoverable=this.hoverable.add(b);this._on(b,{mouseenter:function(b){a(b.currentTarget).addClass("ui-state-hover")},mouseleave:function(b){a(b.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(b){this.focusable=this.focusable.add(b);this._on(b,{focusin:function(b){a(b.currentTarget).addClass("ui-state-focus")},focusout:function(b){a(b.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(f,b,c){var e,d,g=this.options[f];c=c||{};b=a.Event(b);b.type=(f===this.widgetEventPrefix?f:this.widgetEventPrefix+f).toLowerCase();b.target=this.element[0];d=b.originalEvent;if(d)for(e in d)if(!(e in b))b[e]=d[e];this.element.trigger(b,c);return!(a.isFunction(g)&&g.apply(this.element[0],[b].concat(c))===false||b.isDefaultPrevented())}};a.each({show:"fadeIn",hide:"fadeOut"},function(b,c){a.Widget.prototype["_"+b]=function(e,d,g){if(typeof d==="string")d={effect:d};var h,f=!d?b:d===true||typeof d==="number"?c:d.effect||c;d=d||{};if(typeof d==="number")d={duration:d};h=!a.isEmptyObject(d);d.complete=g;d.delay&&e.delay(d.delay);if(h&&a.effects&&a.effects.effect[f])e[b](d);else if(f!==b&&e[f])e[f](d.duration,d.easing,g);else e.queue(function(c){a(this)[b]();g&&g.call(e[0]);c()})}})})(jQuery);(function(a){var b=false;a(document).mouseup(function(){b=false});a.widget("ui.mouse",{version:"1.10.2",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var b=this;this.element.bind("mousedown."+this.widgetName,function(a){return b._mouseDown(a)}).bind("click."+this.widgetName,function(c){if(true===a.data(c.target,b.widgetName+".preventClickEvent")){a.removeData(c.target,b.widgetName+".preventClickEvent");c.stopImmediatePropagation();return false}});this.started=false},_mouseDestroy:function(){this.element.unbind("."+this.widgetName);this._mouseMoveDelegate&&a(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(c){if(b)return;this._mouseStarted&&this._mouseUp(c);this._mouseDownEvent=c;var d=this,f=c.which===1,e=typeof this.options.cancel==="string"&&c.target.nodeName?a(c.target).closest(this.options.cancel).length:false;if(!f||e||!this._mouseCapture(c))return true;this.mouseDelayMet=!this.options.delay;if(!this.mouseDelayMet)this._mouseDelayTimer=setTimeout(function(){d.mouseDelayMet=true},this.options.delay);if(this._mouseDistanceMet(c)&&this._mouseDelayMet(c)){this._mouseStarted=this._mouseStart(c)!==false;if(!this._mouseStarted){c.preventDefault();return true}}true===a.data(c.target,this.widgetName+".preventClickEvent")&&a.removeData(c.target,this.widgetName+".preventClickEvent");this._mouseMoveDelegate=function(a){return d._mouseMove(a)};this._mouseUpDelegate=function(a){return d._mouseUp(a)};a(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate);c.preventDefault();b=true;return true},_mouseMove:function(b){if(a.ui.ie&&(!document.documentMode||document.documentMode<9)&&!b.button)return this._mouseUp(b);if(this._mouseStarted){this._mouseDrag(b);return b.preventDefault()}if(this._mouseDistanceMet(b)&&this._mouseDelayMet(b)){this._mouseStarted=this._mouseStart(this._mouseDownEvent,b)!==false;this._mouseStarted?this._mouseDrag(b):this._mouseUp(b)}return!this._mouseStarted},_mouseUp:function(b){a(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate);if(this._mouseStarted){this._mouseStarted=false;b.target===this._mouseDownEvent.target&&a.data(b.target,this.widgetName+".preventClickEvent",true);this._mouseStop(b)}return false},_mouseDistanceMet:function(a){return Math.max(Math.abs(this._mouseDownEvent.pageX-a.pageX),Math.abs(this._mouseDownEvent.pageY-a.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return true}})})(jQuery);(function(a){a.widget("ui.draggable",a.ui.mouse,{version:"1.10.2",widgetEventPrefix:"drag",options:{addClasses:true,appendTo:"parent",axis:false,connectToSortable:false,containment:false,cursor:"auto",cursorAt:false,grid:false,handle:false,helper:"original",iframeFix:false,opacity:false,refreshPositions:false,revert:false,revertDuration:500,scope:"default",scroll:true,scrollSensitivity:20,scrollSpeed:20,snap:false,snapMode:"both",snapTolerance:20,stack:false,zIndex:false,drag:null,start:null,stop:null},_create:function(){if(this.options.helper==="original"&&!/^(?:r|a|f)/.test(this.element.css("position")))this.element[0].style.position="relative";this.options.addClasses&&this.element.addClass("ui-draggable");this.options.disabled&&this.element.addClass("ui-draggable-disabled");this._mouseInit()},_destroy:function(){this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled");this._mouseDestroy()},_mouseCapture:function(c){var b=this.options;if(this.helper||b.disabled||a(c.target).closest(".ui-resizable-handle").length>0)return false;this.handle=this._getHandle(c);if(!this.handle)return false;a(b.iframeFix===true?"iframe":b.iframeFix).each(function(){a("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"0.001",zIndex:1e3}).css(a(this).offset()).appendTo("body")});return true},_mouseStart:function(b){var c=this.options;this.helper=this._createHelper(b);this.helper.addClass("ui-draggable-dragging");this._cacheHelperProportions();if(a.ui.ddmanager)a.ui.ddmanager.current=this;this._cacheMargins();this.cssPosition=this.helper.css("position");this.scrollParent=this.helper.scrollParent();this.offset=this.positionAbs=this.element.offset();this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left};a.extend(this.offset,{click:{left:b.pageX-this.offset.left,top:b.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});this.originalPosition=this.position=this._generatePosition(b);this.originalPageX=b.pageX;this.originalPageY=b.pageY;c.cursorAt&&this._adjustOffsetFromHelper(c.cursorAt);c.containment&&this._setContainment();if(this._trigger("start",b)===false){this._clear();return false}this._cacheHelperProportions();a.ui.ddmanager&&!c.dropBehaviour&&a.ui.ddmanager.prepareOffsets(this,b);this._mouseDrag(b,true);a.ui.ddmanager&&a.ui.ddmanager.dragStart(this,b);return true},_mouseDrag:function(b,d){this.position=this._generatePosition(b);this.positionAbs=this._convertPositionTo("absolute");if(!d){var c=this._uiHash();if(this._trigger("drag",b,c)===false){this._mouseUp({});return false}this.position=c.position}if(!this.options.axis||this.options.axis!=="y")this.helper[0].style.left=this.position.left+"px";if(!this.options.axis||this.options.axis!=="x")this.helper[0].style.top=this.position.top+"px";a.ui.ddmanager&&a.ui.ddmanager.drag(this,b);return false},_mouseStop:function(d){var c,f=this,e=false,b=false;if(a.ui.ddmanager&&!this.options.dropBehaviour)b=a.ui.ddmanager.drop(this,d);if(this.dropped){b=this.dropped;this.dropped=false}c=this.element[0];while(c&&(c=c.parentNode))if(c===document)e=true;if(!e&&this.options.helper==="original")return false;if(this.options.revert==="invalid"&&!b||this.options.revert==="valid"&&b||this.options.revert===true||a.isFunction(this.options.revert)&&this.options.revert.call(this.element,b))a(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){f._trigger("stop",d)!==false&&f._clear()});else this._trigger("stop",d)!==false&&this._clear();return false},_mouseUp:function(b){a("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)});a.ui.ddmanager&&a.ui.ddmanager.dragStop(this,b);return a.ui.mouse.prototype._mouseUp.call(this,b)},cancel:function(){if(this.helper.is(".ui-draggable-dragging"))this._mouseUp({});else this._clear();return this},_getHandle:function(b){return this.options.handle?!!a(b.target).closest(this.element.find(this.options.handle)).length:true},_createHelper:function(d){var c=this.options,b=a.isFunction(c.helper)?a(c.helper.apply(this.element[0],[d])):c.helper==="clone"?this.element.clone().removeAttr("id"):this.element;!b.parents("body").length&&b.appendTo(c.appendTo==="parent"?this.element[0].parentNode:c.appendTo);b[0]!==this.element[0]&&!/(fixed|absolute)/.test(b.css("position"))&&b.css("position","absolute");return b},_adjustOffsetFromHelper:function(b){if(typeof b==="string")b=b.split(" ");if(a.isArray(b))b={left:+b[0],top:+b[1]||0};if("left"in b)this.offset.click.left=b.left+this.margins.left;if("right"in b)this.offset.click.left=this.helperProportions.width-b.right+this.margins.left;if("top"in b)this.offset.click.top=b.top+this.margins.top;if("bottom"in b)this.offset.click.top=this.helperProportions.height-b.bottom+this.margins.top},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var b=this.offsetParent.offset();if(this.cssPosition==="absolute"&&this.scrollParent[0]!==document&&a.contains(this.scrollParent[0],this.offsetParent[0])){b.left+=this.scrollParent.scrollLeft();b.top+=this.scrollParent.scrollTop()}if(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()==="html"&&a.ui.ie)b={top:0,left:0};return{top:b.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:b.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if(this.cssPosition==="relative"){var a=this.element.position();return{top:a.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:a.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}else return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var d,e,b,c=this.options;if(c.containment==="parent")c.containment=this.helper[0].parentNode;if(c.containment==="document"||c.containment==="window")this.containment=[c.containment==="document"?0:a(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,c.containment==="document"?0:a(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,(c.containment==="document"?0:a(window).scrollLeft())+a(c.containment==="document"?document:window).width()-this.helperProportions.width-this.margins.left,(c.containment==="document"?0:a(window).scrollTop())+(a(c.containment==="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];if(!/^(document|window|parent)$/.test(c.containment)&&c.containment.constructor!==Array){e=a(c.containment);b=e[0];if(!b)return;d=a(b).css("overflow")!=="hidden";this.containment=[(parseInt(a(b).css("borderLeftWidth"),10)||0)+(parseInt(a(b).css("paddingLeft"),10)||0),(parseInt(a(b).css("borderTopWidth"),10)||0)+(parseInt(a(b).css("paddingTop"),10)||0),(d?Math.max(b.scrollWidth,b.offsetWidth):b.offsetWidth)-(parseInt(a(b).css("borderRightWidth"),10)||0)-(parseInt(a(b).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(d?Math.max(b.scrollHeight,b.offsetHeight):b.offsetHeight)-(parseInt(a(b).css("borderBottomWidth"),10)||0)-(parseInt(a(b).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom];this.relative_container=e}else if(c.containment.constructor===Array)this.containment=c.containment},_convertPositionTo:function(f,c){if(!c)c=this.position;var b=f==="absolute"?1:-1,d=this.cssPosition==="absolute"&&!(this.scrollParent[0]!==document&&a.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,e=/(html|body)/i.test(d[0].tagName);return{top:c.top+this.offset.relative.top*b+this.offset.parent.top*b-(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():e?0:d.scrollTop())*b,left:c.left+this.offset.relative.left*b+this.offset.parent.left*b-(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():e?0:d.scrollLeft())*b}},_generatePosition:function(f){var b,i,e,d,c=this.options,j=this.cssPosition==="absolute"&&!(this.scrollParent[0]!==document&&a.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,k=/(html|body)/i.test(j[0].tagName),g=f.pageX,h=f.pageY;if(this.originalPosition){if(this.containment){if(this.relative_container){i=this.relative_container.offset();b=[this.containment[0]+i.left,this.containment[1]+i.top,this.containment[2]+i.left,this.containment[3]+i.top]}else b=this.containment;if(f.pageX-this.offset.click.left<b[0])g=b[0]+this.offset.click.left;if(f.pageY-this.offset.click.top<b[1])h=b[1]+this.offset.click.top;if(f.pageX-this.offset.click.left>b[2])g=b[2]+this.offset.click.left;if(f.pageY-this.offset.click.top>b[3])h=b[3]+this.offset.click.top}if(c.grid){e=c.grid[1]?this.originalPageY+Math.round((h-this.originalPageY)/c.grid[1])*c.grid[1]:this.originalPageY;h=b?e-this.offset.click.top>=b[1]||e-this.offset.click.top>b[3]?e:e-this.offset.click.top>=b[1]?e-c.grid[1]:e+c.grid[1]:e;d=c.grid[0]?this.originalPageX+Math.round((g-this.originalPageX)/c.grid[0])*c.grid[0]:this.originalPageX;g=b?d-this.offset.click.left>=b[0]||d-this.offset.click.left>b[2]?d:d-this.offset.click.left>=b[0]?d-c.grid[0]:d+c.grid[0]:d}}return{top:h-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():k?0:j.scrollTop()),left:g-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():k?0:j.scrollLeft())}},_clear:function(){this.helper.removeClass("ui-draggable-dragging");this.helper[0]!==this.element[0]&&!this.cancelHelperRemoval&&this.helper.remove();this.helper=null;this.cancelHelperRemoval=false},_trigger:function(c,d,b){b=b||this._uiHash();a.ui.plugin.call(this,c,[d,b]);if(c==="drag")this.positionAbs=this._convertPositionTo("absolute");return a.Widget.prototype._trigger.call(this,c,d,b)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}});a.ui.plugin.add("draggable","connectToSortable",{start:function(d,e){var b=a(this).data("ui-draggable"),f=b.options,c=a.extend({},e,{item:b.element});b.sortables=[];a(f.connectToSortable).each(function(){var e=a.data(this,"ui-sortable");if(e&&!e.options.disabled){b.sortables.push({instance:e,shouldRevert:e.options.revert});e.refreshPositions();e._trigger("activate",d,c)}})},stop:function(c,e){var b=a(this).data("ui-draggable"),d=a.extend({},e,{item:b.element});a.each(b.sortables,function(){if(this.instance.isOver){this.instance.isOver=0;b.cancelHelperRemoval=true;this.instance.cancelHelperRemoval=false;if(this.shouldRevert)this.instance.options.revert=this.shouldRevert;this.instance._mouseStop(c);this.instance.options.helper=this.instance.options._helper;b.options.helper==="original"&&this.instance.currentItem.css({top:"auto",left:"auto"})}else{this.instance.cancelHelperRemoval=false;this.instance._trigger("deactivate",c,d)}})},drag:function(c,e){var b=a(this).data("ui-draggable"),d=this;a.each(b.sortables,function(){var f=false,g=this;this.instance.positionAbs=b.positionAbs;this.instance.helperProportions=b.helperProportions;this.instance.offset.click=b.offset.click;if(this.instance._intersectsWith(this.instance.containerCache)){f=true;a.each(b.sortables,function(){this.instance.positionAbs=b.positionAbs;this.instance.helperProportions=b.helperProportions;this.instance.offset.click=b.offset.click;if(this!==g&&this.instance._intersectsWith(this.instance.containerCache)&&a.contains(g.instance.element[0],this.instance.element[0]))f=false;return f})}if(f){if(!this.instance.isOver){this.instance.isOver=1;this.instance.currentItem=a(d).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item",true);this.instance.options._helper=this.instance.options.helper;this.instance.options.helper=function(){return e.helper[0]};c.target=this.instance.currentItem[0];this.instance._mouseCapture(c,true);this.instance._mouseStart(c,true,true);this.instance.offset.click.top=b.offset.click.top;this.instance.offset.click.left=b.offset.click.left;this.instance.offset.parent.left-=b.offset.parent.left-this.instance.offset.parent.left;this.instance.offset.parent.top-=b.offset.parent.top-this.instance.offset.parent.top;b._trigger("toSortable",c);b.dropped=this.instance.element;b.currentItem=b.element;this.instance.fromOutside=b}this.instance.currentItem&&this.instance._mouseDrag(c)}else if(this.instance.isOver){this.instance.isOver=0;this.instance.cancelHelperRemoval=true;this.instance.options.revert=false;this.instance._trigger("out",c,this.instance._uiHash(this.instance));this.instance._mouseStop(c,true);this.instance.options.helper=this.instance.options._helper;this.instance.currentItem.remove();this.instance.placeholder&&this.instance.placeholder.remove();b._trigger("fromSortable",c);b.dropped=false}})}});a.ui.plugin.add("draggable","cursor",{start:function(){var b=a("body"),c=a(this).data("ui-draggable").options;if(b.css("cursor"))c._cursor=b.css("cursor");b.css("cursor",c.cursor)},stop:function(){var b=a(this).data("ui-draggable").options;b._cursor&&a("body").css("cursor",b._cursor)}});a.ui.plugin.add("draggable","opacity",{start:function(e,d){var b=a(d.helper),c=a(this).data("ui-draggable").options;if(b.css("opacity"))c._opacity=b.css("opacity");b.css("opacity",c.opacity)},stop:function(d,c){var b=a(this).data("ui-draggable").options;b._opacity&&a(c.helper).css("opacity",b._opacity)}});a.ui.plugin.add("draggable","scroll",{start:function(){var b=a(this).data("ui-draggable");if(b.scrollParent[0]!==document&&b.scrollParent[0].tagName!=="HTML")b.overflowOffset=b.scrollParent.offset()},drag:function(e){var c=a(this).data("ui-draggable"),b=c.options,d=false;if(c.scrollParent[0]!==document&&c.scrollParent[0].tagName!=="HTML"){if(!b.axis||b.axis!=="x")if(c.overflowOffset.top+c.scrollParent[0].offsetHeight-e.pageY<b.scrollSensitivity)c.scrollParent[0].scrollTop=d=c.scrollParent[0].scrollTop+b.scrollSpeed;else if(e.pageY-c.overflowOffset.top<b.scrollSensitivity)c.scrollParent[0].scrollTop=d=c.scrollParent[0].scrollTop-b.scrollSpeed;if(!b.axis||b.axis!=="y")if(c.overflowOffset.left+c.scrollParent[0].offsetWidth-e.pageX<b.scrollSensitivity)c.scrollParent[0].scrollLeft=d=c.scrollParent[0].scrollLeft+b.scrollSpeed;else if(e.pageX-c.overflowOffset.left<b.scrollSensitivity)c.scrollParent[0].scrollLeft=d=c.scrollParent[0].scrollLeft-b.scrollSpeed}else{if(!b.axis||b.axis!=="x")if(e.pageY-a(document).scrollTop()<b.scrollSensitivity)d=a(document).scrollTop(a(document).scrollTop()-b.scrollSpeed);else if(a(window).height()-(e.pageY-a(document).scrollTop())<b.scrollSensitivity)d=a(document).scrollTop(a(document).scrollTop()+b.scrollSpeed);if(!b.axis||b.axis!=="y")if(e.pageX-a(document).scrollLeft()<b.scrollSensitivity)d=a(document).scrollLeft(a(document).scrollLeft()-b.scrollSpeed);else if(a(window).width()-(e.pageX-a(document).scrollLeft())<b.scrollSensitivity)d=a(document).scrollLeft(a(document).scrollLeft()+b.scrollSpeed)}d!==false&&a.ui.ddmanager&&!b.dropBehaviour&&a.ui.ddmanager.prepareOffsets(c,e)}});a.ui.plugin.add("draggable","snap",{start:function(){var b=a(this).data("ui-draggable"),c=b.options;b.snapElements=[];a(c.snap.constructor!==String?c.snap.items||":data(ui-draggable)":c.snap).each(function(){var c=a(this),d=c.offset();this!==b.element[0]&&b.snapElements.push({item:this,width:c.outerWidth(),height:c.outerHeight(),top:d.top,left:d.left})})},drag:function(t,e){for(var m,j,k,l,f,i,g,h,r,b=a(this).data("ui-draggable"),s=b.options,c=s.snapTolerance,n=e.offset.left,p=n+b.helperProportions.width,o=e.offset.top,q=o+b.helperProportions.height,d=b.snapElements.length-1;d>=0;d--){f=b.snapElements[d].left;i=f+b.snapElements[d].width;g=b.snapElements[d].top;h=g+b.snapElements[d].height;if(!(f-c<n&&n<i+c&&g-c<o&&o<h+c||f-c<n&&n<i+c&&g-c<q&&q<h+c||f-c<p&&p<i+c&&g-c<o&&o<h+c||f-c<p&&p<i+c&&g-c<q&&q<h+c)){if(b.snapElements[d].snapping)b.options.snap.release&&b.options.snap.release.call(b.element,t,a.extend(b._uiHash(),{snapItem:b.snapElements[d].item}));b.snapElements[d].snapping=false;continue}if(s.snapMode!=="inner"){m=Math.abs(g-q)<=c;j=Math.abs(h-o)<=c;k=Math.abs(f-p)<=c;l=Math.abs(i-n)<=c;if(m)e.position.top=b._convertPositionTo("relative",{top:g-b.helperProportions.height,left:0}).top-b.margins.top;if(j)e.position.top=b._convertPositionTo("relative",{top:h,left:0}).top-b.margins.top;if(k)e.position.left=b._convertPositionTo("relative",{top:0,left:f-b.helperProportions.width}).left-b.margins.left;if(l)e.position.left=b._convertPositionTo("relative",{top:0,left:i}).left-b.margins.left}r=m||j||k||l;if(s.snapMode!=="outer"){m=Math.abs(g-o)<=c;j=Math.abs(h-q)<=c;k=Math.abs(f-n)<=c;l=Math.abs(i-p)<=c;if(m)e.position.top=b._convertPositionTo("relative",{top:g,left:0}).top-b.margins.top;if(j)e.position.top=b._convertPositionTo("relative",{top:h-b.helperProportions.height,left:0}).top-b.margins.top;if(k)e.position.left=b._convertPositionTo("relative",{top:0,left:f}).left-b.margins.left;if(l)e.position.left=b._convertPositionTo("relative",{top:0,left:i-b.helperProportions.width}).left-b.margins.left}if(!b.snapElements[d].snapping&&(m||j||k||l||r))b.options.snap.snap&&b.options.snap.snap.call(b.element,t,a.extend(b._uiHash(),{snapItem:b.snapElements[d].item}));b.snapElements[d].snapping=m||j||k||l||r}}});a.ui.plugin.add("draggable","stack",{start:function(){var c,d=this.data("ui-draggable").options,b=a.makeArray(a(d.stack)).sort(function(b,c){return(parseInt(a(b).css("zIndex"),10)||0)-(parseInt(a(c).css("zIndex"),10)||0)});if(!b.length)return;c=parseInt(a(b[0]).css("zIndex"),10)||0;a(b).each(function(b){a(this).css("zIndex",c+b)});this.css("zIndex",c+b.length)}});a.ui.plugin.add("draggable","zIndex",{start:function(e,d){var b=a(d.helper),c=a(this).data("ui-draggable").options;if(b.css("zIndex"))c._zIndex=b.css("zIndex");b.css("zIndex",c.zIndex)},stop:function(d,c){var b=a(this).data("ui-draggable").options;b._zIndex&&a(c.helper).css("zIndex",b._zIndex)}})})(jQuery);(function(a){function b(b,a,c){return b>a&&b<a+c}a.widget("ui.droppable",{version:"1.10.2",widgetEventPrefix:"drop",options:{accept:"*",activeClass:false,addClasses:true,greedy:false,hoverClass:false,scope:"default",tolerance:"intersect",activate:null,deactivate:null,drop:null,out:null,over:null},_create:function(){var b=this.options,c=b.accept;this.isover=false;this.isout=true;this.accept=a.isFunction(c)?c:function(a){return a.is(c)};this.proportions={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight};a.ui.ddmanager.droppables[b.scope]=a.ui.ddmanager.droppables[b.scope]||[];a.ui.ddmanager.droppables[b.scope].push(this);b.addClasses&&this.element.addClass("ui-droppable")},_destroy:function(){for(var b=0,c=a.ui.ddmanager.droppables[this.options.scope];b<c.length;b++)c[b]===this&&c.splice(b,1);this.element.removeClass("ui-droppable ui-droppable-disabled")},_setOption:function(c,b){if(c==="accept")this.accept=a.isFunction(b)?b:function(a){return a.is(b)};a.Widget.prototype._setOption.apply(this,arguments)},_activate:function(c){var b=a.ui.ddmanager.current;this.options.activeClass&&this.element.addClass(this.options.activeClass);b&&this._trigger("activate",c,this.ui(b))},_deactivate:function(c){var b=a.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass);b&&this._trigger("deactivate",c,this.ui(b))},_over:function(c){var b=a.ui.ddmanager.current;if(!b||(b.currentItem||b.element)[0]===this.element[0])return;if(this.accept.call(this.element[0],b.currentItem||b.element)){this.options.hoverClass&&this.element.addClass(this.options.hoverClass);this._trigger("over",c,this.ui(b))}},_out:function(c){var b=a.ui.ddmanager.current;if(!b||(b.currentItem||b.element)[0]===this.element[0])return;if(this.accept.call(this.element[0],b.currentItem||b.element)){this.options.hoverClass&&this.element.removeClass(this.options.hoverClass);this._trigger("out",c,this.ui(b))}},_drop:function(e,d){var b=d||a.ui.ddmanager.current,c=false;if(!b||(b.currentItem||b.element)[0]===this.element[0])return false;this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function(){var d=a.data(this,"ui-droppable");if(d.options.greedy&&!d.options.disabled&&d.options.scope===b.options.scope&&d.accept.call(d.element[0],b.currentItem||b.element)&&a.ui.intersect(b,a.extend(d,{offset:d.element.offset()}),d.options.tolerance)){c=true;return false}});if(c)return false;if(this.accept.call(this.element[0],b.currentItem||b.element)){this.options.activeClass&&this.element.removeClass(this.options.activeClass);this.options.hoverClass&&this.element.removeClass(this.options.hoverClass);this._trigger("drop",e,this.ui(b));return this.element}return false},ui:function(a){return{draggable:a.currentItem||a.element,helper:a.helper,position:a.position,offset:a.positionAbs}}});a.ui.intersect=function(a,c,n){if(!c.offset)return false;var l,m,f=(a.positionAbs||a.position.absolute).left,h=f+a.helperProportions.width,g=(a.positionAbs||a.position.absolute).top,i=g+a.helperProportions.height,d=c.offset.left,k=d+c.proportions.width,e=c.offset.top,j=e+c.proportions.height;switch(n){case"fit":return d<=f&&h<=k&&e<=g&&i<=j;case"intersect":return d<f+a.helperProportions.width/2&&h-a.helperProportions.width/2<k&&e<g+a.helperProportions.height/2&&i-a.helperProportions.height/2<j;case"pointer":l=(a.positionAbs||a.position.absolute).left+(a.clickOffset||a.offset.click).left;m=(a.positionAbs||a.position.absolute).top+(a.clickOffset||a.offset.click).top;return b(m,e,c.proportions.height)&&b(l,d,c.proportions.width);case"touch":return(g>=e&&g<=j||i>=e&&i<=j||g<e&&i>j)&&(f>=d&&f<=k||h>=d&&h<=k||f<d&&h>k);default:return false}};a.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(d,f){var b,e,c=a.ui.ddmanager.droppables[d.options.scope]||[],h=f?f.type:null,g=(d.currentItem||d.element).find(":data(ui-droppable)").addBack();a:for(b=0;b<c.length;b++){if(c[b].options.disabled||d&&!c[b].accept.call(c[b].element[0],d.currentItem||d.element))continue;for(e=0;e<g.length;e++)if(g[e]===c[b].element[0]){c[b].proportions.height=0;continue a}c[b].visible=c[b].element.css("display")!=="none";if(!c[b].visible)continue;h==="mousedown"&&c[b]._activate.call(c[b],f);c[b].offset=c[b].element.offset();c[b].proportions={width:c[b].element[0].offsetWidth,height:c[b].element[0].offsetHeight}}},drop:function(b,d){var c=false;a.each((a.ui.ddmanager.droppables[b.options.scope]||[]).slice(),function(){if(!this.options)return;if(!this.options.disabled&&this.visible&&a.ui.intersect(b,this,this.options.tolerance))c=this._drop.call(this,d)||c;if(!this.options.disabled&&this.visible&&this.accept.call(this.element[0],b.currentItem||b.element)){this.isout=true;this.isover=false;this._deactivate.call(this,d)}});return c},dragStart:function(b,c){b.element.parentsUntil("body").bind("scroll.droppable",function(){!b.options.refreshPositions&&a.ui.ddmanager.prepareOffsets(b,c)})},drag:function(b,c){b.options.refreshPositions&&a.ui.ddmanager.prepareOffsets(b,c);a.each(a.ui.ddmanager.droppables[b.options.scope]||[],function(){if(this.options.disabled||this.greedyChild||!this.visible)return;var d,h,f,g=a.ui.intersect(b,this,this.options.tolerance),e=!g&&this.isover?"isout":g&&!this.isover?"isover":null;if(!e)return;if(this.options.greedy){h=this.options.scope;f=this.element.parents(":data(ui-droppable)").filter(function(){return a.data(this,"ui-droppable").options.scope===h});if(f.length){d=a.data(f[0],"ui-droppable");d.greedyChild=e==="isover"}}if(d&&e==="isover"){d.isover=false;d.isout=true;d._out.call(d,c)}this[e]=true;this[e==="isout"?"isover":"isout"]=false;this[e==="isover"?"_over":"_out"].call(this,c);if(d&&e==="isout"){d.isout=false;d.isover=true;d._over.call(d,c)}})},dragStop:function(b,c){b.element.parentsUntil("body").unbind("scroll.droppable");!b.options.refreshPositions&&a.ui.ddmanager.prepareOffsets(b,c)}}})(jQuery);(function(a){function c(a){return parseInt(a,10)||0}function b(a){return!isNaN(parseInt(a,10))}a.widget("ui.resizable",a.ui.mouse,{version:"1.10.2",widgetEventPrefix:"resize",options:{alsoResize:false,animate:false,animateDuration:"slow",animateEasing:"swing",aspectRatio:false,autoHide:false,containment:false,ghost:false,grid:false,handles:"e,s,se",helper:false,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:90,resize:null,start:null,stop:null},_create:function(){var g,f,d,c,h,e=this,b=this.options;this.element.addClass("ui-resizable");a.extend(this,{_aspectRatio:!!b.aspectRatio,aspectRatio:b.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:b.helper||b.ghost||b.animate?b.helper||"ui-resizable-helper":null});if(this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)){this.element.wrap(a("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")}));this.element=this.element.parent().data("ui-resizable",this.element.data("ui-resizable"));this.elementIsWrapper=true;this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")});this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0});this.originalResizeStyle=this.originalElement.css("resize");this.originalElement.css("resize","none");this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"}));this.originalElement.css({margin:this.originalElement.css("margin")});this._proportionallyResize()}this.handles=b.handles||(!a(".ui-resizable-handle",this.element).length?"e,s,se":{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"});if(this.handles.constructor===String){if(this.handles==="all")this.handles="n,e,s,w,se,sw,ne,nw";g=this.handles.split(",");this.handles={};for(f=0;f<g.length;f++){d=a.trim(g[f]);h="ui-resizable-"+d;c=a("<div class='ui-resizable-handle "+h+"'></div>");c.css({zIndex:b.zIndex});"se"===d&&c.addClass("ui-icon ui-icon-gripsmall-diagonal-se");this.handles[d]=".ui-resizable-"+d;this.element.append(c)}}this._renderAxis=function(c){var b,d,f,e;c=c||this.element;for(b in this.handles){if(this.handles[b].constructor===String)this.handles[b]=a(this.handles[b],this.element).show();if(this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)){d=a(this.handles[b],this.element);e=/sw|ne|nw|se|n|s/.test(b)?d.outerHeight():d.outerWidth();f=["padding",/ne|nw|n/.test(b)?"Top":/se|sw|s/.test(b)?"Bottom":/^e$/.test(b)?"Right":"Left"].join("");c.css(f,e);this._proportionallyResize()}if(!a(this.handles[b]).length)continue}};this._renderAxis(this.element);this._handles=a(".ui-resizable-handle",this.element).disableSelection();this._handles.mouseover(function(){if(!e.resizing){if(this.className)c=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i);e.axis=c&&c[1]?c[1]:"se"}});if(b.autoHide){this._handles.hide();a(this.element).addClass("ui-resizable-autohide").mouseenter(function(){if(b.disabled)return;a(this).removeClass("ui-resizable-autohide");e._handles.show()}).mouseleave(function(){if(b.disabled)return;if(!e.resizing){a(this).addClass("ui-resizable-autohide");e._handles.hide()}})}this._mouseInit()},_destroy:function(){this._mouseDestroy();var b,c=function(b){a(b).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()};if(this.elementIsWrapper){c(this.element);b=this.element;this.originalElement.css({position:b.css("position"),width:b.outerWidth(),height:b.outerHeight(),top:b.css("top"),left:b.css("left")}).insertAfter(b);b.remove()}this.originalElement.css("resize",this.originalResizeStyle);c(this.originalElement);return this},_mouseCapture:function(d){var e,b,c=false;for(e in this.handles){b=a(this.handles[e])[0];if(b===d.target||a.contains(b,d.target))c=true}return!this.options.disabled&&c},_mouseStart:function(h){var e,f,g,d=this.options,i=this.element.position(),b=this.element;this.resizing=true;if(/absolute/.test(b.css("position")))b.css({position:"absolute",top:b.css("top"),left:b.css("left")});else b.is(".ui-draggable")&&b.css({position:"absolute",top:i.top,left:i.left});this._renderProxy();e=c(this.helper.css("left"));f=c(this.helper.css("top"));if(d.containment){e+=a(d.containment).scrollLeft()||0;f+=a(d.containment).scrollTop()||0}this.offset=this.helper.offset();this.position={left:e,top:f};this.size=this._helper?{width:b.outerWidth(),height:b.outerHeight()}:{width:b.width(),height:b.height()};this.originalSize=this._helper?{width:b.outerWidth(),height:b.outerHeight()}:{width:b.width(),height:b.height()};this.originalPosition={left:e,top:f};this.sizeDiff={width:b.outerWidth()-b.width(),height:b.outerHeight()-b.height()};this.originalMousePosition={left:h.pageX,top:h.pageY};this.aspectRatio=typeof d.aspectRatio==="number"?d.aspectRatio:this.originalSize.width/this.originalSize.height||1;g=a(".ui-resizable-"+this.axis).css("cursor");a("body").css("cursor",g==="auto"?this.axis+"-resize":g);b.addClass("ui-resizable-resizing");this._propagate("start",h);return true},_mouseDrag:function(b){var d,m=this.helper,c={},f=this.originalMousePosition,n=this.axis,j=this.position.top,i=this.position.left,h=this.size.width,g=this.size.height,k=b.pageX-f.left||0,l=b.pageY-f.top||0,e=this._change[n];if(!e)return false;d=e.apply(this,[b,k,l]);this._updateVirtualBoundaries(b.shiftKey);if(this._aspectRatio||b.shiftKey)d=this._updateRatio(d,b);d=this._respectSize(d,b);this._updateCache(d);this._propagate("resize",b);if(this.position.top!==j)c.top=this.position.top+"px";if(this.position.left!==i)c.left=this.position.left+"px";if(this.size.width!==h)c.width=this.size.width+"px";if(this.size.height!==g)c.height=this.size.height+"px";m.css(c);!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize();!a.isEmptyObject(c)&&this._trigger("resize",b,this.ui());return false},_mouseStop:function(k){this.resizing=false;var c,d,e,f,j,g,h,i=this.options,b=this;if(this._helper){c=this._proportionallyResizeElements;d=c.length&&/textarea/i.test(c[0].nodeName);e=d&&a.ui.hasScroll(c[0],"left")?0:b.sizeDiff.height;f=d?0:b.sizeDiff.width;j={width:b.helper.width()-f,height:b.helper.height()-e};g=parseInt(b.element.css("left"),10)+(b.position.left-b.originalPosition.left)||null;h=parseInt(b.element.css("top"),10)+(b.position.top-b.originalPosition.top)||null;!i.animate&&this.element.css(a.extend(j,{top:h,left:g}));b.helper.height(b.size.height);b.helper.width(b.size.width);this._helper&&!i.animate&&this._proportionallyResize()}a("body").css("cursor","auto");this.element.removeClass("ui-resizable-resizing");this._propagate("stop",k);this._helper&&this.helper.remove();return false},_updateVirtualBoundaries:function(h){var g,f,e,d,a,c=this.options;a={minWidth:b(c.minWidth)?c.minWidth:0,maxWidth:b(c.maxWidth)?c.maxWidth:Infinity,minHeight:b(c.minHeight)?c.minHeight:0,maxHeight:b(c.maxHeight)?c.maxHeight:Infinity};if(this._aspectRatio||h){g=a.minHeight*this.aspectRatio;e=a.minWidth/this.aspectRatio;f=a.maxHeight*this.aspectRatio;d=a.maxWidth/this.aspectRatio;if(g>a.minWidth)a.minWidth=g;if(e>a.minHeight)a.minHeight=e;if(f<a.maxWidth)a.maxWidth=f;if(d<a.maxHeight)a.maxHeight=d}this._vBoundaries=a},_updateCache:function(a){this.offset=this.helper.offset();if(b(a.left))this.position.left=a.left;if(b(a.top))this.position.top=a.top;if(b(a.height))this.size.height=a.height;if(b(a.width))this.size.width=a.width},_updateRatio:function(a){var d=this.position,c=this.size,e=this.axis;if(b(a.height))a.width=a.height*this.aspectRatio;else if(b(a.width))a.height=a.width/this.aspectRatio;if(e==="sw"){a.left=d.left+(c.width-a.width);a.top=null}if(e==="nw"){a.top=d.top+(c.height-a.height);a.left=d.left+(c.width-a.width)}return a},_respectSize:function(a){var c=this._vBoundaries,l=this.axis,e=b(a.width)&&c.maxWidth&&c.maxWidth<a.width,d=b(a.height)&&c.maxHeight&&c.maxHeight<a.height,g=b(a.width)&&c.minWidth&&c.minWidth>a.width,f=b(a.height)&&c.minHeight&&c.minHeight>a.height,k=this.originalPosition.left+this.originalSize.width,j=this.position.top+this.size.height,i=/sw|nw|w/.test(l),h=/nw|ne|n/.test(l);if(g)a.width=c.minWidth;if(f)a.height=c.minHeight;if(e)a.width=c.maxWidth;if(d)a.height=c.maxHeight;if(g&&i)a.left=k-c.minWidth;if(e&&i)a.left=k-c.maxWidth;if(f&&h)a.top=j-c.minHeight;if(d&&h)a.top=j-c.maxHeight;if(!a.width&&!a.height&&!a.left&&a.top)a.top=null;else if(!a.width&&!a.height&&!a.top&&a.left)a.left=null;return a},_proportionallyResize:function(){if(!this._proportionallyResizeElements.length)return;for(var b,c,e,a,f=this.helper||this.element,d=0;d<this._proportionallyResizeElements.length;d++){a=this._proportionallyResizeElements[d];if(!this.borderDif){this.borderDif=[];c=[a.css("borderTopWidth"),a.css("borderRightWidth"),a.css("borderBottomWidth"),a.css("borderLeftWidth")];e=[a.css("paddingTop"),a.css("paddingRight"),a.css("paddingBottom"),a.css("paddingLeft")];for(b=0;b<c.length;b++)this.borderDif[b]=(parseInt(c[b],10)||0)+(parseInt(e[b],10)||0)}a.css({height:f.height()-this.borderDif[0]-this.borderDif[2]||0,width:f.width()-this.borderDif[1]-this.borderDif[3]||0})}},_renderProxy:function(){var b=this.element,c=this.options;this.elementOffset=b.offset();if(this._helper){this.helper=this.helper||a("<div style='overflow:hidden;'></div>");this.helper.addClass(this._helper).css({width:this.element.outerWidth()-1,height:this.element.outerHeight()-1,position:"absolute",left:this.elementOffset.left+"px",top:this.elementOffset.top+"px",zIndex:++c.zIndex});this.helper.appendTo("body").disableSelection()}else this.helper=this.element},_change:{e:function(b,a){return{width:this.originalSize.width+a}},w:function(d,a){var b=this.originalSize,c=this.originalPosition;return{left:c.left+a,width:b.width-a}},n:function(d,e,a){var b=this.originalSize,c=this.originalPosition;return{top:c.top+a,height:b.height-a}},s:function(b,c,a){return{height:this.originalSize.height+a}},se:function(b,c,d){return a.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[b,c,d]))},sw:function(b,c,d){return a.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[b,c,d]))},ne:function(b,c,d){return a.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[b,c,d]))},nw:function(b,c,d){return a.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[b,c,d]))}},_propagate:function(b,c){a.ui.plugin.call(this,b,[c,this.ui()]);b!=="resize"&&this._trigger(b,c,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}}});a.ui.plugin.add("resizable","animate",{stop:function(j){var b=a(this).data("ui-resizable"),g=b.options,c=b._proportionallyResizeElements,d=c.length&&/textarea/i.test(c[0].nodeName),h=d&&a.ui.hasScroll(c[0],"left")?0:b.sizeDiff.height,i=d?0:b.sizeDiff.width,k={width:b.size.width-i,height:b.size.height-h},e=parseInt(b.element.css("left"),10)+(b.position.left-b.originalPosition.left)||null,f=parseInt(b.element.css("top"),10)+(b.position.top-b.originalPosition.top)||null;b.element.animate(a.extend(k,f&&e?{top:f,left:e}:{}),{duration:g.animateDuration,easing:g.animateEasing,step:function(){var d={width:parseInt(b.element.css("width"),10),height:parseInt(b.element.css("height"),10),top:parseInt(b.element.css("top"),10),left:parseInt(b.element.css("left"),10)};c&&c.length&&a(c[0]).css({width:d.width,height:d.height});b._updateCache(d);b._propagate("resize",j)}})}});a.ui.plugin.add("resizable","containment",{start:function(){var e,g,h,k,l,j,i,b=a(this).data("ui-resizable"),n=b.options,m=b.element,f=n.containment,d=f instanceof a?f.get(0):/parent/.test(f)?m.parent().get(0):f;if(!d)return;b.containerElement=a(d);if(/document/.test(f)||f===document){b.containerOffset={left:0,top:0};b.containerPosition={left:0,top:0};b.parentData={element:a(document),left:0,top:0,width:a(document).width(),height:a(document).height()||document.body.parentNode.scrollHeight}}else{e=a(d);g=[];a(["Top","Right","Left","Bottom"]).each(function(b,a){g[b]=c(e.css("padding"+a))});b.containerOffset=e.offset();b.containerPosition=e.position();b.containerSize={height:e.innerHeight()-g[3],width:e.innerWidth()-g[1]};h=b.containerOffset;k=b.containerSize.height;l=b.containerSize.width;j=a.ui.hasScroll(d,"left")?d.scrollWidth:l;i=a.ui.hasScroll(d)?d.scrollHeight:k;b.parentData={element:d,left:h.left,top:h.top,width:j,height:i}}},resize:function(l){var f,g,i,h,b=a(this).data("ui-resizable"),m=b.options,c=b.containerOffset,k=b.position,e=b._aspectRatio||l.shiftKey,d={top:0,left:0},j=b.containerElement;if(j[0]!==document&&/static/.test(j.css("position")))d=c;if(k.left<(b._helper?c.left:0)){b.size.width=b.size.width+(b._helper?b.position.left-c.left:b.position.left-d.left);if(e)b.size.height=b.size.width/b.aspectRatio;b.position.left=m.helper?c.left:0}if(k.top<(b._helper?c.top:0)){b.size.height=b.size.height+(b._helper?b.position.top-c.top:b.position.top);if(e)b.size.width=b.size.height*b.aspectRatio;b.position.top=b._helper?c.top:0}b.offset.left=b.parentData.left+b.position.left;b.offset.top=b.parentData.top+b.position.top;f=Math.abs((b._helper?b.offset.left-d.left:b.offset.left-d.left)+b.sizeDiff.width);g=Math.abs((b._helper?b.offset.top-d.top:b.offset.top-c.top)+b.sizeDiff.height);i=b.containerElement.get(0)===b.element.parent().get(0);h=/relative|absolute/.test(b.containerElement.css("position"));if(i&&h)f-=b.parentData.left;if(f+b.size.width>=b.parentData.width){b.size.width=b.parentData.width-f;if(e)b.size.height=b.size.width/b.aspectRatio}if(g+b.size.height>=b.parentData.height){b.size.height=b.parentData.height-g;if(e)b.size.width=b.size.height*b.aspectRatio}},stop:function(){var b=a(this).data("ui-resizable"),i=b.options,f=b.containerOffset,d=b.containerPosition,e=b.containerElement,c=a(b.helper),g=c.offset(),j=c.outerWidth()-b.sizeDiff.width,h=c.outerHeight()-b.sizeDiff.height;b._helper&&!i.animate&&/relative/.test(e.css("position"))&&a(this).css({left:g.left-d.left-f.left,width:j,height:h});b._helper&&!i.animate&&/static/.test(e.css("position"))&&a(this).css({left:g.left-d.left-f.left,width:j,height:h})}});a.ui.plugin.add("resizable","alsoResize",{start:function(){var d=a(this).data("ui-resizable"),b=d.options,c=function(b){a(b).each(function(){var b=a(this);b.data("ui-resizable-alsoresize",{width:parseInt(b.width(),10),height:parseInt(b.height(),10),left:parseInt(b.css("left"),10),top:parseInt(b.css("top"),10)})})};if(typeof b.alsoResize==="object"&&!b.alsoResize.parentNode)if(b.alsoResize.length){b.alsoResize=b.alsoResize[0];c(b.alsoResize)}else a.each(b.alsoResize,function(a){c(a)});else c(b.alsoResize)},resize:function(i,h){var b=a(this).data("ui-resizable"),c=b.options,f=b.originalSize,e=b.originalPosition,g={height:b.size.height-f.height||0,width:b.size.width-f.width||0,top:b.position.top-e.top||0,left:b.position.left-e.left||0},d=function(c,b){a(c).each(function(){var d=a(this),e=a(this).data("ui-resizable-alsoresize"),c={},f=b&&b.length?b:d.parents(h.originalElement[0]).length?["width","height"]:["width","height","top","left"];a.each(f,function(d,a){var b=(e[a]||0)+(g[a]||0);if(b&&b>=0)c[a]=b||null});d.css(c)})};if(typeof c.alsoResize==="object"&&!c.alsoResize.nodeType)a.each(c.alsoResize,function(a,b){d(a,b)});else d(c.alsoResize)},stop:function(){a(this).removeData("resizable-alsoresize")}});a.ui.plugin.add("resizable","ghost",{start:function(){var b=a(this).data("ui-resizable"),d=b.options,c=b.size;b.ghost=b.originalElement.clone();b.ghost.css({opacity:.25,display:"block",position:"relative",height:c.height,width:c.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass(typeof d.ghost==="string"?d.ghost:"");b.ghost.appendTo(b.helper)},resize:function(){var b=a(this).data("ui-resizable");b.ghost&&b.ghost.css({position:"relative",height:b.size.height,width:b.size.width})},stop:function(){var b=a(this).data("ui-resizable");b.ghost&&b.helper&&b.helper.get(0).removeChild(b.ghost.get(0))}});a.ui.plugin.add("resizable","grid",{resize:function(){var b=a(this).data("ui-resizable"),c=b.options,n=b.size,i=b.originalSize,h=b.originalPosition,m=b.axis,j=typeof c.grid==="number"?[c.grid,c.grid]:c.grid,f=j[0]||1,g=j[1]||1,k=Math.round((n.width-i.width)/f)*f,l=Math.round((n.height-i.height)/g)*g,e=i.width+k,d=i.height+l,q=c.maxWidth&&c.maxWidth<e,o=c.maxHeight&&c.maxHeight<d,r=c.minWidth&&c.minWidth>e,p=c.minHeight&&c.minHeight>d;c.grid=j;if(r)e=e+f;if(p)d=d+g;if(q)e=e-f;if(o)d=d-g;if(/^(se|s|e)$/.test(m)){b.size.width=e;b.size.height=d}else if(/^(ne)$/.test(m)){b.size.width=e;b.size.height=d;b.position.top=h.top-l}else if(/^(sw)$/.test(m)){b.size.width=e;b.size.height=d;b.position.left=h.left-k}else{b.size.width=e;b.size.height=d;b.position.top=h.top-l;b.position.left=h.left-k}}})})(jQuery);(function(a){a.widget("ui.selectable",a.ui.mouse,{version:"1.10.2",options:{appendTo:"body",autoRefresh:true,distance:0,filter:"*",tolerance:"touch",selected:null,selecting:null,start:null,stop:null,unselected:null,unselecting:null},_create:function(){var b,c=this;this.element.addClass("ui-selectable");this.dragged=false;this.refresh=function(){b=a(c.options.filter,c.element[0]);b.addClass("ui-selectee");b.each(function(){var b=a(this),c=b.offset();a.data(this,"selectable-item",{element:this,$element:b,left:c.left,top:c.top,right:c.left+b.outerWidth(),bottom:c.top+b.outerHeight(),startselected:false,selected:b.hasClass("ui-selected"),selecting:b.hasClass("ui-selecting"),unselecting:b.hasClass("ui-unselecting")})})};this.refresh();this.selectees=b.addClass("ui-selectee");this._mouseInit();this.helper=a("<div class='ui-selectable-helper'></div>")},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item");this.element.removeClass("ui-selectable ui-selectable-disabled");this._mouseDestroy()},_mouseStart:function(b){var d=this,c=this.options;this.opos=[b.pageX,b.pageY];if(this.options.disabled)return;this.selectees=a(c.filter,this.element[0]);this._trigger("start",b);a(c.appendTo).append(this.helper);this.helper.css({left:b.pageX,top:b.pageY,width:0,height:0});c.autoRefresh&&this.refresh();this.selectees.filter(".ui-selected").each(function(){var c=a.data(this,"selectable-item");c.startselected=true;if(!b.metaKey&&!b.ctrlKey){c.$element.removeClass("ui-selected");c.selected=false;c.$element.addClass("ui-unselecting");c.unselecting=true;d._trigger("unselecting",b,{unselecting:c.element})}});a(b.target).parents().addBack().each(function(){var e,c=a.data(this,"selectable-item");if(c){e=!b.metaKey&&!b.ctrlKey||!c.$element.hasClass("ui-selected");c.$element.removeClass(e?"ui-unselecting":"ui-selected").addClass(e?"ui-selecting":"ui-unselecting");c.unselecting=!e;c.selecting=e;c.selected=e;if(e)d._trigger("selecting",b,{selecting:c.element});else d._trigger("unselecting",b,{unselecting:c.element});return false}})},_mouseDrag:function(b){this.dragged=true;if(this.options.disabled)return;var h,g=this,i=this.options,c=this.opos[0],d=this.opos[1],e=b.pageX,f=b.pageY;if(c>e){h=e;e=c;c=h}if(d>f){h=f;f=d;d=h}this.helper.css({left:c,top:d,width:e-c,height:f-d});this.selectees.each(function(){var h=a.data(this,"selectable-item"),j=false;if(!h||h.element===g.element[0])return;if(i.tolerance==="touch")j=!(h.left>e||h.right<c||h.top>f||h.bottom<d);else if(i.tolerance==="fit")j=h.left>c&&h.right<e&&h.top>d&&h.bottom<f;if(j){if(h.selected){h.$element.removeClass("ui-selected");h.selected=false}if(h.unselecting){h.$element.removeClass("ui-unselecting");h.unselecting=false}if(!h.selecting){h.$element.addClass("ui-selecting");h.selecting=true;g._trigger("selecting",b,{selecting:h.element})}}else{if(h.selecting)if((b.metaKey||b.ctrlKey)&&h.startselected){h.$element.removeClass("ui-selecting");h.selecting=false;h.$element.addClass("ui-selected");h.selected=true}else{h.$element.removeClass("ui-selecting");h.selecting=false;if(h.startselected){h.$element.addClass("ui-unselecting");h.unselecting=true}g._trigger("unselecting",b,{unselecting:h.element})}if(h.selected)if(!b.metaKey&&!b.ctrlKey&&!h.startselected){h.$element.removeClass("ui-selected");h.selected=false;h.$element.addClass("ui-unselecting");h.unselecting=true;g._trigger("unselecting",b,{unselecting:h.element})}}});return false},_mouseStop:function(b){var c=this;this.dragged=false;a(".ui-unselecting",this.element[0]).each(function(){var d=a.data(this,"selectable-item");d.$element.removeClass("ui-unselecting");d.unselecting=false;d.startselected=false;c._trigger("unselected",b,{unselected:d.element})});a(".ui-selecting",this.element[0]).each(function(){var d=a.data(this,"selectable-item");d.$element.removeClass("ui-selecting").addClass("ui-selected");d.selecting=false;d.selected=true;d.startselected=true;c._trigger("selected",b,{selected:d.element})});this._trigger("stop",b);this.helper.remove();return false}})})(jQuery);(function(a){function b(b,a,c){return b>a&&b<a+c}function c(a){return/left|right/.test(a.css("float"))||/inline|table-cell/.test(a.css("display"))}a.widget("ui.sortable",a.ui.mouse,{version:"1.10.2",widgetEventPrefix:"sort",ready:false,options:{appendTo:"parent",axis:false,connectWith:false,containment:false,cursor:"auto",cursorAt:false,dropOnEmpty:true,forcePlaceholderSize:false,forceHelperSize:false,grid:false,handle:false,helper:"original",items:"> *",opacity:false,placeholder:false,revert:false,scroll:true,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_create:function(){var a=this.options;this.containerCache={};this.element.addClass("ui-sortable");this.refresh();this.floating=this.items.length?a.axis==="x"||c(this.items[0].item):false;this.offset=this.element.offset();this._mouseInit();this.ready=true},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled");this._mouseDestroy();for(var a=this.items.length-1;a>=0;a--)this.items[a].item.removeData(this.widgetName+"-item");return this},_setOption:function(c,b){if(c==="disabled"){this.options[c]=b;this.widget().toggleClass("ui-sortable-disabled",!!b)}else a.Widget.prototype._setOption.apply(this,arguments)},_mouseCapture:function(c,f){var b=null,e=false,d=this;if(this.reverting)return false;if(this.options.disabled||this.options.type==="static")return false;this._refreshItems(c);a(c.target).parents().each(function(){if(a.data(this,d.widgetName+"-item")===d){b=a(this);return false}});if(a.data(c.target,d.widgetName+"-item")===d)b=a(c.target);if(!b)return false;if(this.options.handle&&!f){a(this.options.handle,b).find("*").addBack().each(function(){if(this===c.target)e=true});if(!e)return false}this.currentItem=b;this._removeCurrentsFromItems();return true},_mouseStart:function(c,g,f){var e,d,b=this.options;this.currentContainer=this;this.refreshPositions();this.helper=this._createHelper(c);this._cacheHelperProportions();this._cacheMargins();this.scrollParent=this.helper.scrollParent();this.offset=this.currentItem.offset();this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left};a.extend(this.offset,{click:{left:c.pageX-this.offset.left,top:c.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()});this.helper.css("position","absolute");this.cssPosition=this.helper.css("position");this.originalPosition=this._generatePosition(c);this.originalPageX=c.pageX;this.originalPageY=c.pageY;b.cursorAt&&this._adjustOffsetFromHelper(b.cursorAt);this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]};this.helper[0]!==this.currentItem[0]&&this.currentItem.hide();this._createPlaceholder();b.containment&&this._setContainment();if(b.cursor&&b.cursor!=="auto"){d=this.document.find("body");this.storedCursor=d.css("cursor");d.css("cursor",b.cursor);this.storedStylesheet=a("<style>*{ cursor: "+b.cursor+" !important; }</style>").appendTo(d)}if(b.opacity){if(this.helper.css("opacity"))this._storedOpacity=this.helper.css("opacity");this.helper.css("opacity",b.opacity)}if(b.zIndex){if(this.helper.css("zIndex"))this._storedZIndex=this.helper.css("zIndex");this.helper.css("zIndex",b.zIndex)}if(this.scrollParent[0]!==document&&this.scrollParent[0].tagName!=="HTML")this.overflowOffset=this.scrollParent.offset();this._trigger("start",c,this._uiHash());!this._preserveHelperProportions&&this._cacheHelperProportions();if(!f)for(e=this.containers.length-1;e>=0;e--)this.containers[e]._trigger("activate",c,this._uiHash(this));if(a.ui.ddmanager)a.ui.ddmanager.current=this;a.ui.ddmanager&&!b.dropBehaviour&&a.ui.ddmanager.prepareOffsets(this,c);this.dragging=true;this.helper.addClass("ui-sortable-helper");this._mouseDrag(c);return true},_mouseDrag:function(c){var h,e,f,g,b=this.options,d=false;this.position=this._generatePosition(c);this.positionAbs=this._convertPositionTo("absolute");if(!this.lastPositionAbs)this.lastPositionAbs=this.positionAbs;if(this.options.scroll){if(this.scrollParent[0]!==document&&this.scrollParent[0].tagName!=="HTML"){if(this.overflowOffset.top+this.scrollParent[0].offsetHeight-c.pageY<b.scrollSensitivity)this.scrollParent[0].scrollTop=d=this.scrollParent[0].scrollTop+b.scrollSpeed;else if(c.pageY-this.overflowOffset.top<b.scrollSensitivity)this.scrollParent[0].scrollTop=d=this.scrollParent[0].scrollTop-b.scrollSpeed;if(this.overflowOffset.left+this.scrollParent[0].offsetWidth-c.pageX<b.scrollSensitivity)this.scrollParent[0].scrollLeft=d=this.scrollParent[0].scrollLeft+b.scrollSpeed;else if(c.pageX-this.overflowOffset.left<b.scrollSensitivity)this.scrollParent[0].scrollLeft=d=this.scrollParent[0].scrollLeft-b.scrollSpeed}else{if(c.pageY-a(document).scrollTop()<b.scrollSensitivity)d=a(document).scrollTop(a(document).scrollTop()-b.scrollSpeed);else if(a(window).height()-(c.pageY-a(document).scrollTop())<b.scrollSensitivity)d=a(document).scrollTop(a(document).scrollTop()+b.scrollSpeed);if(c.pageX-a(document).scrollLeft()<b.scrollSensitivity)d=a(document).scrollLeft(a(document).scrollLeft()-b.scrollSpeed);else if(a(window).width()-(c.pageX-a(document).scrollLeft())<b.scrollSensitivity)d=a(document).scrollLeft(a(document).scrollLeft()+b.scrollSpeed)}d!==false&&a.ui.ddmanager&&!b.dropBehaviour&&a.ui.ddmanager.prepareOffsets(this,c)}this.positionAbs=this._convertPositionTo("absolute");if(!this.options.axis||this.options.axis!=="y")this.helper[0].style.left=this.position.left+"px";if(!this.options.axis||this.options.axis!=="x")this.helper[0].style.top=this.position.top+"px";for(h=this.items.length-1;h>=0;h--){e=this.items[h];f=e.item[0];g=this._intersectsWithPointer(e);if(!g)continue;if(e.instance!==this.currentContainer)continue;if(f!==this.currentItem[0]&&this.placeholder[g===1?"next":"prev"]()[0]!==f&&!a.contains(this.placeholder[0],f)&&(this.options.type==="semi-dynamic"?!a.contains(this.element[0],f):true)){this.direction=g===1?"down":"up";if(this.options.tolerance==="pointer"||this._intersectsWithSides(e))this._rearrange(c,e);else break;this._trigger("change",c,this._uiHash());break}}this._contactContainers(c);a.ui.ddmanager&&a.ui.ddmanager.drag(this,c);this._trigger("sort",c,this._uiHash());this.lastPositionAbs=this.positionAbs;return false},_mouseStop:function(b,f){if(!b)return;a.ui.ddmanager&&!this.options.dropBehaviour&&a.ui.ddmanager.drop(this,b);if(this.options.revert){var g=this,e=this.placeholder.offset(),c=this.options.axis,d={};if(!c||c==="x")d.left=e.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollLeft);if(!c||c==="y")d.top=e.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollTop);this.reverting=true;a(this.helper).animate(d,parseInt(this.options.revert,10)||500,function(){g._clear(b)})}else this._clear(b,f);return false},cancel:function(){if(this.dragging){this._mouseUp({target:null});if(this.options.helper==="original")this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper");else this.currentItem.show();for(var b=this.containers.length-1;b>=0;b--){this.containers[b]._trigger("deactivate",null,this._uiHash(this));if(this.containers[b].containerCache.over){this.containers[b]._trigger("out",null,this._uiHash(this));this.containers[b].containerCache.over=0}}}if(this.placeholder){this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]);this.options.helper!=="original"&&this.helper&&this.helper[0].parentNode&&this.helper.remove();a.extend(this,{helper:null,dragging:false,reverting:false,_noFinalSort:null});if(this.domPosition.prev)a(this.domPosition.prev).after(this.currentItem);else a(this.domPosition.parent).prepend(this.currentItem)}return this},serialize:function(b){var d=this._getItemsAsjQuery(b&&b.connected),c=[];b=b||{};a(d).each(function(){var d=(a(b.item||this).attr(b.attribute||"id")||"").match(b.expression||/(.+)[\-=_](.+)/);d&&c.push((b.key||d[1]+"[]")+"="+(b.key&&b.expression?d[1]:d[2]))});!c.length&&b.key&&c.push(b.key+"=");return c.join("&")},toArray:function(b){var d=this._getItemsAsjQuery(b&&b.connected),c=[];b=b||{};d.each(function(){c.push(a(b.item||this).attr(b.attribute||"id")||"")});return c},_intersectsWith:function(a){var b=this.positionAbs.left,k=b+this.helperProportions.width,c=this.positionAbs.top,l=c+this.helperProportions.height,d=a.left,i=d+a.width,e=a.top,h=e+a.height,g=this.offset.click.top,f=this.offset.click.left,j=c+g>e&&c+g<h&&b+f>d&&b+f<i;return this.options.tolerance==="pointer"||this.options.forcePointerForContainers||this.options.tolerance!=="pointer"&&this.helperProportions[this.floating?"width":"height"]>a[this.floating?"width":"height"]?j:d<b+this.helperProportions.width/2&&k-this.helperProportions.width/2<i&&e<c+this.helperProportions.height/2&&l-this.helperProportions.height/2<h},_intersectsWithPointer:function(a){var e=this.options.axis==="x"||b(this.positionAbs.top+this.offset.click.top,a.top,a.height),f=this.options.axis==="y"||b(this.positionAbs.left+this.offset.click.left,a.left,a.width),g=e&&f,c=this._getDragVerticalDirection(),d=this._getDragHorizontalDirection();return!g?false:this.floating?d&&d==="right"||c==="down"?2:1:c&&(c==="down"?2:1)},_intersectsWithSides:function(a){var e=b(this.positionAbs.top+this.offset.click.top,a.top+a.height/2,a.height),f=b(this.positionAbs.left+this.offset.click.left,a.left+a.width/2,a.width),d=this._getDragVerticalDirection(),c=this._getDragHorizontalDirection();return this.floating&&c?c==="right"&&f||c==="left"&&!f:d&&(d==="down"&&e||d==="up"&&!e)},_getDragVerticalDirection:function(){var a=this.positionAbs.top-this.lastPositionAbs.top;return a!==0&&(a>0?"down":"up")},_getDragHorizontalDirection:function(){var a=this.positionAbs.left-this.lastPositionAbs.left;return a!==0&&(a>0?"right":"left")},refresh:function(a){this._refreshItems(a);this.refreshPositions();return this},_connectWith:function(){var a=this.options;return a.connectWith.constructor===String?[a.connectWith]:a.connectWith},_getItemsAsjQuery:function(i){var c,e,g,b,h=[],d=[],f=this._connectWith();if(f&&i)for(c=f.length-1;c>=0;c--){g=a(f[c]);for(e=g.length-1;e>=0;e--){b=a.data(g[e],this.widgetFullName);b&&b!==this&&!b.options.disabled&&d.push([a.isFunction(b.options.items)?b.options.items.call(b.element):a(b.options.items,b.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),b])}}d.push([a.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):a(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]);for(c=d.length-1;c>=0;c--)d[c][0].each(function(){h.push(this)});return a(h)},_removeCurrentsFromItems:function(){var b=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=a.grep(this.items,function(c){for(var a=0;a<b.length;a++)if(b[a]===c.item[0])return false;return true})},_refreshItems:function(l){this.items=[];this.containers=[this];var c,d,j,b,g,h,i,k,m=this.items,e=[[a.isFunction(this.options.items)?this.options.items.call(this.element[0],l,{item:this.currentItem}):a(this.options.items,this.element),this]],f=this._connectWith();if(f&&this.ready)for(c=f.length-1;c>=0;c--){j=a(f[c]);for(d=j.length-1;d>=0;d--){b=a.data(j[d],this.widgetFullName);if(b&&b!==this&&!b.options.disabled){e.push([a.isFunction(b.options.items)?b.options.items.call(b.element[0],l,{item:this.currentItem}):a(b.options.items,b.element),b]);this.containers.push(b)}}}for(c=e.length-1;c>=0;c--){g=e[c][1];h=e[c][0];for(d=0,k=h.length;d<k;d++){i=a(h[d]);i.data(this.widgetName+"-item",g);m.push({item:i,instance:g,width:0,height:0,left:0,top:0})}}},refreshPositions:function(f){if(this.offsetParent&&this.helper)this.offset.parent=this._getParentOffset();for(var c,e,d,b=this.items.length-1;b>=0;b--){c=this.items[b];if(c.instance!==this.currentContainer&&this.currentContainer&&c.item[0]!==this.currentItem[0])continue;e=this.options.toleranceElement?a(this.options.toleranceElement,c.item):c.item;if(!f){c.width=e.outerWidth();c.height=e.outerHeight()}d=e.offset();c.left=d.left;c.top=d.top}if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(b=this.containers.length-1;b>=0;b--){d=this.containers[b].element.offset();this.containers[b].containerCache.left=d.left;this.containers[b].containerCache.top=d.top;this.containers[b].containerCache.width=this.containers[b].element.outerWidth();this.containers[b].containerCache.height=this.containers[b].element.outerHeight()}return this},_createPlaceholder:function(b){b=b||this;var d,c=b.options;if(!c.placeholder||c.placeholder.constructor===String){d=c.placeholder;c.placeholder={element:function(){var e=b.currentItem[0].nodeName.toLowerCase(),c=a(b.document[0].createElement(e)).addClass(d||b.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper");if(e==="tr")c.append("<td colspan='99'>&#160;</td>");else e==="img"&&c.attr("src",b.currentItem.attr("src"));!d&&c.css("visibility","hidden");return c},update:function(e,a){if(d&&!c.forcePlaceholderSize)return;!a.height()&&a.height(b.currentItem.innerHeight()-parseInt(b.currentItem.css("paddingTop")||0,10)-parseInt(b.currentItem.css("paddingBottom")||0,10));!a.width()&&a.width(b.currentItem.innerWidth()-parseInt(b.currentItem.css("paddingLeft")||0,10)-parseInt(b.currentItem.css("paddingRight")||0,10))}}}b.placeholder=a(c.placeholder.element.call(b.element,b.currentItem));b.currentItem.after(b.placeholder);c.placeholder.update(b,b.placeholder)},_contactContainers:function(g){for(var e,p,i,l,n,k,h,o,m,j=null,d=null,f=this.containers.length-1;f>=0;f--){if(a.contains(this.currentItem[0],this.containers[f].element[0]))continue;if(this._intersectsWith(this.containers[f].containerCache)){if(j&&a.contains(this.containers[f].element[0],j.element[0]))continue;j=this.containers[f];d=f}else if(this.containers[f].containerCache.over){this.containers[f]._trigger("out",g,this._uiHash(this));this.containers[f].containerCache.over=0}}if(!j)return;if(this.containers.length===1){if(!this.containers[d].containerCache.over){this.containers[d]._trigger("over",g,this._uiHash(this));this.containers[d].containerCache.over=1}}else{p=1e4;i=null;m=j.floating||c(this.currentItem);l=m?"left":"top";n=m?"width":"height";k=this.positionAbs[l]+this.offset.click[l];for(e=this.items.length-1;e>=0;e--){if(!a.contains(this.containers[d].element[0],this.items[e].item[0]))continue;if(this.items[e].item[0]===this.currentItem[0])continue;if(m&&!b(this.positionAbs.top+this.offset.click.top,this.items[e].top,this.items[e].height))continue;h=this.items[e].item.offset()[l];o=false;if(Math.abs(h-k)>Math.abs(h+this.items[e][n]-k)){o=true;h+=this.items[e][n]}if(Math.abs(h-k)<p){p=Math.abs(h-k);i=this.items[e];this.direction=o?"up":"down"}}if(!i&&!this.options.dropOnEmpty)return;if(this.currentContainer===this.containers[d])return;i?this._rearrange(g,i,null,true):this._rearrange(g,null,this.containers[d].element,true);this._trigger("change",g,this._uiHash());this.containers[d]._trigger("change",g,this._uiHash(this));this.currentContainer=this.containers[d];this.options.placeholder.update(this.currentContainer,this.placeholder);this.containers[d]._trigger("over",g,this._uiHash(this));this.containers[d].containerCache.over=1}},_createHelper:function(d){var c=this.options,b=a.isFunction(c.helper)?a(c.helper.apply(this.element[0],[d,this.currentItem])):c.helper==="clone"?this.currentItem.clone():this.currentItem;!b.parents("body").length&&a(c.appendTo!=="parent"?c.appendTo:this.currentItem[0].parentNode)[0].appendChild(b[0]);if(b[0]===this.currentItem[0])this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")};(!b[0].style.width||c.forceHelperSize)&&b.width(this.currentItem.width());(!b[0].style.height||c.forceHelperSize)&&b.height(this.currentItem.height());return b},_adjustOffsetFromHelper:function(b){if(typeof b==="string")b=b.split(" ");if(a.isArray(b))b={left:+b[0],top:+b[1]||0};if("left"in b)this.offset.click.left=b.left+this.margins.left;if("right"in b)this.offset.click.left=this.helperProportions.width-b.right+this.margins.left;if("top"in b)this.offset.click.top=b.top+this.margins.top;if("bottom"in b)this.offset.click.top=this.helperProportions.height-b.bottom+this.margins.top},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var b=this.offsetParent.offset();if(this.cssPosition==="absolute"&&this.scrollParent[0]!==document&&a.contains(this.scrollParent[0],this.offsetParent[0])){b.left+=this.scrollParent.scrollLeft();b.top+=this.scrollParent.scrollTop()}if(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()==="html"&&a.ui.ie)b={top:0,left:0};return{top:b.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:b.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if(this.cssPosition==="relative"){var a=this.currentItem.position();return{top:a.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:a.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}else return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var b,d,e,c=this.options;if(c.containment==="parent")c.containment=this.helper[0].parentNode;if(c.containment==="document"||c.containment==="window")this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,a(c.containment==="document"?document:window).width()-this.helperProportions.width-this.margins.left,(a(c.containment==="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];if(!/^(document|window|parent)$/.test(c.containment)){b=a(c.containment)[0];d=a(c.containment).offset();e=a(b).css("overflow")!=="hidden";this.containment=[d.left+(parseInt(a(b).css("borderLeftWidth"),10)||0)+(parseInt(a(b).css("paddingLeft"),10)||0)-this.margins.left,d.top+(parseInt(a(b).css("borderTopWidth"),10)||0)+(parseInt(a(b).css("paddingTop"),10)||0)-this.margins.top,d.left+(e?Math.max(b.scrollWidth,b.offsetWidth):b.offsetWidth)-(parseInt(a(b).css("borderLeftWidth"),10)||0)-(parseInt(a(b).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,d.top+(e?Math.max(b.scrollHeight,b.offsetHeight):b.offsetHeight)-(parseInt(a(b).css("borderTopWidth"),10)||0)-(parseInt(a(b).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top]}},_convertPositionTo:function(f,c){if(!c)c=this.position;var b=f==="absolute"?1:-1,d=this.cssPosition==="absolute"&&!(this.scrollParent[0]!==document&&a.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,e=/(html|body)/i.test(d[0].tagName);return{top:c.top+this.offset.relative.top*b+this.offset.parent.top*b-(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():e?0:d.scrollTop())*b,left:c.left+this.offset.relative.left*b+this.offset.parent.left*b-(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():e?0:d.scrollLeft())*b}},_generatePosition:function(e){var d,c,b=this.options,f=e.pageX,g=e.pageY,h=this.cssPosition==="absolute"&&!(this.scrollParent[0]!==document&&a.contains(this.scrollParent[0],this.offsetParent[0]))?this.offsetParent:this.scrollParent,i=/(html|body)/i.test(h[0].tagName);if(this.cssPosition==="relative"&&!(this.scrollParent[0]!==document&&this.scrollParent[0]!==this.offsetParent[0]))this.offset.relative=this._getRelativeOffset();if(this.originalPosition){if(this.containment){if(e.pageX-this.offset.click.left<this.containment[0])f=this.containment[0]+this.offset.click.left;if(e.pageY-this.offset.click.top<this.containment[1])g=this.containment[1]+this.offset.click.top;if(e.pageX-this.offset.click.left>this.containment[2])f=this.containment[2]+this.offset.click.left;if(e.pageY-this.offset.click.top>this.containment[3])g=this.containment[3]+this.offset.click.top}if(b.grid){d=this.originalPageY+Math.round((g-this.originalPageY)/b.grid[1])*b.grid[1];g=this.containment?d-this.offset.click.top>=this.containment[1]&&d-this.offset.click.top<=this.containment[3]?d:d-this.offset.click.top>=this.containment[1]?d-b.grid[1]:d+b.grid[1]:d;c=this.originalPageX+Math.round((f-this.originalPageX)/b.grid[0])*b.grid[0];f=this.containment?c-this.offset.click.left>=this.containment[0]&&c-this.offset.click.left<=this.containment[2]?c:c-this.offset.click.left>=this.containment[0]?c-b.grid[0]:c+b.grid[0]:c}}return{top:g-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():i?0:h.scrollTop()),left:f-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():i?0:h.scrollLeft())}},_rearrange:function(e,a,b,c){b?b[0].appendChild(this.placeholder[0]):a.item[0].parentNode.insertBefore(this.placeholder[0],this.direction==="down"?a.item[0]:a.item[0].nextSibling);this.counter=this.counter?++this.counter:1;var d=this.counter;this._delay(function(){d===this.counter&&this.refreshPositions(!c)})},_clear:function(d,c){this.reverting=false;var a,b=[];!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem);this._noFinalSort=null;if(this.helper[0]===this.currentItem[0]){for(a in this._storedCSS)if(this._storedCSS[a]==="auto"||this._storedCSS[a]==="static")this._storedCSS[a]="";this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();this.fromOutside&&!c&&b.push(function(a){this._trigger("receive",a,this._uiHash(this.fromOutside))});(this.fromOutside||this.domPosition.prev!==this.currentItem.prev().not(".ui-sortable-helper")[0]||this.domPosition.parent!==this.currentItem.parent()[0])&&!c&&b.push(function(a){this._trigger("update",a,this._uiHash())});if(this!==this.currentContainer)if(!c){b.push(function(a){this._trigger("remove",a,this._uiHash())});b.push(function(a){return function(b){a._trigger("receive",b,this._uiHash(this))}}.call(this,this.currentContainer));b.push(function(a){return function(b){a._trigger("update",b,this._uiHash(this))}}.call(this,this.currentContainer))}for(a=this.containers.length-1;a>=0;a--){!c&&b.push(function(a){return function(b){a._trigger("deactivate",b,this._uiHash(this))}}.call(this,this.containers[a]));if(this.containers[a].containerCache.over){b.push(function(a){return function(b){a._trigger("out",b,this._uiHash(this))}}.call(this,this.containers[a]));this.containers[a].containerCache.over=0}}if(this.storedCursor){this.document.find("body").css("cursor",this.storedCursor);this.storedStylesheet.remove()}this._storedOpacity&&this.helper.css("opacity",this._storedOpacity);this._storedZIndex&&this.helper.css("zIndex",this._storedZIndex==="auto"?"":this._storedZIndex);this.dragging=false;if(this.cancelHelperRemoval){if(!c){this._trigger("beforeStop",d,this._uiHash());for(a=0;a<b.length;a++)b[a].call(this,d);this._trigger("stop",d,this._uiHash())}this.fromOutside=false;return false}!c&&this._trigger("beforeStop",d,this._uiHash());this.placeholder[0].parentNode.removeChild(this.placeholder[0]);this.helper[0]!==this.currentItem[0]&&this.helper.remove();this.helper=null;if(!c){for(a=0;a<b.length;a++)b[a].call(this,d);this._trigger("stop",d,this._uiHash())}this.fromOutside=false;return true},_trigger:function(){a.Widget.prototype._trigger.apply(this,arguments)===false&&this.cancel()},_uiHash:function(c){var b=c||this;return{helper:b.helper,placeholder:b.placeholder||a([]),position:b.position,originalPosition:b.originalPosition,offset:b.positionAbs,item:b.currentItem,sender:c?c.element:null}}})})(jQuery);(function(a,c){var b="ui-effects-";a.effects={effect:{}};
/*
 * jQuery Color Animations v2.1.2
 * https://github.com/jquery/jquery-color
 *
 * Copyright 2013 jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 *
 * Date: Wed Jan 16 08:47:09 2013 -0600
 */
(function(a,e){var o="backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",n=/^([\-+])=\s*(\d+\.?\d*)/,m=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(a){return[a[1],a[2],a[3],a[4]]}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(a){return[a[1]*2.55,a[2]*2.55,a[3]*2.55,a[4]]}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(a){return[parseInt(a[1],16),parseInt(a[2],16),parseInt(a[3],16)]}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(a){return[parseInt(a[1]+a[1],16),parseInt(a[2]+a[2],16),parseInt(a[3]+a[3],16)]}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(a){return[a[1],a[2]/100,a[3]/100,a[4]]}}],b=a.Color=function(c,d,e,b){return new a.Color.fn.parse(c,d,e,b)},d={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},k={"byte":{floor:true,max:255},percent:{max:1},degrees:{mod:360,floor:true}},l=b.support={},j=a("<p>")[0],f,c=a.each;j.style.cssText="background-color:rgba(1,1,1,.5)";l.rgba=j.style.backgroundColor.indexOf("rgba")>-1;c(d,function(b,a){a.cache="_"+b;a.props.alpha={idx:3,type:"percent",def:1}});function g(a,c,d){var b=k[c.type]||{};if(a==null)return d||!c.def?null:c.def;a=b.floor?~~a:parseFloat(a);return isNaN(a)?c.def:b.mod?(a+b.mod)%b.mod:0>a?0:b.max<a?b.max:a}function i(g){var e=b(),h=e._rgba=[];g=g.toLowerCase();c(m,function(j,c){var b,i=c.re.exec(g),f=i&&c.parse(i),a=c.space||"rgba";if(f){b=e[a](f);e[d[a].cache]=b[d[a].cache];h=e._rgba=b._rgba;return false}});if(h.length){h.join()==="0,0,0,0"&&a.extend(h,f.transparent);return e}return f[g]}b.fn=a.extend(b.prototype,{parse:function(h,k,n,m){if(h===e){this._rgba=[null,null,null,null];return this}if(h.jquery||h.nodeType){h=a(h).css(k);k=e}var j=this,l=a.type(h),o=this._rgba=[];if(k!==e){h=[h,k,n,m];l="array"}if(l==="string")return this.parse(i(h)||f._default);if(l==="array"){c(d.rgba.props,function(b,a){o[a.idx]=g(h[a.idx],a)});return this}if(l==="object"){if(h instanceof b)c(d,function(b,a){if(h[a.cache])j[a.cache]=h[a.cache].slice()});else c(d,function(e,d){var b=d.cache;c(d.props,function(a,c){if(!j[b]&&d.to){if(a==="alpha"||h[a]==null)return;j[b]=d.to(j._rgba)}j[b][c.idx]=g(h[a],c,true)});if(j[b]&&a.inArray(null,j[b].slice(0,3))<0){j[b][3]=1;if(d.from)j._rgba=d.from(j[b])}});return this}},is:function(f){var g=b(f),a=true,e=this;c(d,function(h,b){var f,d=g[b.cache];if(d){f=e[b.cache]||b.to&&b.to(e._rgba)||[];c(b.props,function(c,b){if(d[b.idx]!=null){a=d[b.idx]===f[b.idx];return a}})}return a});return a},_space:function(){var a=[],b=this;c(d,function(c,d){b[d.cache]&&a.push(c)});return a.pop()},transition:function(m,l){var e=b(m),i=e._space(),a=d[i],h=this.alpha()===0?b("transparent"):this,j=h[a.cache]||a.to(h._rgba),f=j.slice();e=e[a.cache];c(a.props,function(i,h){var d=h.idx,a=j[d],b=e[d],c=k[h.type]||{};if(b===null)return;if(a===null)f[d]=b;else{if(c.mod)if(b-a>c.mod/2)a+=c.mod;else if(a-b>c.mod/2)a-=c.mod;f[d]=g((b-a)*l+a,h)}});return this[i](f)},blend:function(e){if(this._rgba[3]===1)return this;var c=this._rgba.slice(),d=c.pop(),f=b(e)._rgba;return b(a.map(c,function(b,a){return(1-d)*f[a]+d*b}))},toRgbaString:function(){var c="rgba(",b=a.map(this._rgba,function(a,b){return a==null?b>2?1:0:a});if(b[3]===1){b.pop();c="rgb("}return c+b.join()+")"},toHslaString:function(){var c="hsla(",b=a.map(this.hsla(),function(a,b){if(a==null)a=b>2?1:0;if(b&&b<3)a=Math.round(a*100)+"%";return a});if(b[3]===1){b.pop();c="hsl("}return c+b.join()+")"},toHexString:function(c){var b=this._rgba.slice(),d=b.pop();c&&b.push(~~(d*255));return"#"+a.map(b,function(a){a=(a||0).toString(16);return a.length===1?"0"+a:a}).join("")},toString:function(){return this._rgba[3]===0?"transparent":this.toRgbaString()}});b.fn.parse.prototype=b.fn;function h(b,c,a){a=(a+1)%1;return a*6<1?b+(c-b)*a*6:a*2<1?c:a*3<2?b+(c-b)*(2/3-a)*6:b}d.hsla.to=function(a){if(a[0]==null||a[1]==null||a[2]==null)return[null,null,null,a[3]];var f=a[0]/255,d=a[1]/255,g=a[2]/255,k=a[3],c=Math.max(f,d,g),j=Math.min(f,d,g),b=c-j,i=c+j,l=i*.5,e,h;if(j===c)e=0;else if(f===c)e=60*(d-g)/b+360;else if(d===c)e=60*(g-f)/b+120;else e=60*(f-d)/b+240;if(b===0)h=0;else if(l<=.5)h=b/i;else h=b/(2-i);return[Math.round(e)%360,h,l,k==null?1:k]};d.hsla.from=function(a){if(a[0]==null||a[1]==null||a[2]==null)return[null,null,null,a[3]];var d=a[0]/360,f=a[1],b=a[2],g=a[3],c=b<=.5?b*(1+f):b+f-b*f,e=2*b-c;return[Math.round(h(e,c,d+1/3)*255),Math.round(h(e,c,d)*255),Math.round(h(e,c,d-1/3)*255),g]};c(d,function(h,f){var i=f.props,d=f.cache,k=f.to,j=f.from;b.fn[h]=function(h){if(k&&!this[d])this[d]=k(this._rgba);if(h===e)return this[d].slice();var m,l=a.type(h),n=l==="array"||l==="object"?h:arguments,f=this[d].slice();c(i,function(c,a){var b=n[l==="object"?c:a.idx];if(b==null)b=f[a.idx];f[a.idx]=g(b,a)});if(j){m=b(j(f));m[d]=f;return m}else return b(f)};c(i,function(d,c){if(b.fn[d])return;b.fn[d]=function(b){var f=a.type(b),j=d==="alpha"?this._hsla?"hsla":"rgba":h,g=this[j](),i=g[c.idx],e;if(f==="undefined")return i;if(f==="function"){b=b.call(this,i);f=a.type(b)}if(b==null&&c.empty)return this;if(f==="string"){e=n.exec(b);if(e)b=i+parseFloat(e[2])*(e[1]==="+"?1:-1)}g[c.idx]=b;return this[j](g)}})});b.hook=function(e){var d=e.split(" ");c(d,function(d,c){a.cssHooks[c]={"set":function(g,d){var h,f,e="";if(d!=="transparent"&&(a.type(d)!=="string"||(h=i(d)))){d=b(h||d);if(!l.rgba&&d._rgba[3]!==1){f=c==="backgroundColor"?g.parentNode:g;while((e===""||e==="transparent")&&f&&f.style)try{e=a.css(f,"backgroundColor");f=f.parentNode}catch(j){}d=d.blend(e&&e!=="transparent"?e:"_default")}d=d.toRgbaString()}try{g.style[c]=d}catch(j){}}};a.fx.step[c]=function(d){if(!d.colorInit){d.start=b(d.elem,c);d.end=b(d.end);d.colorInit=true}a.cssHooks[c].set(d.elem,d.start.transition(d.end,d.pos))}})};b.hook(o);a.cssHooks.borderColor={expand:function(b){var a={};c(["Top","Right","Bottom","Left"],function(d,c){a["border"+c+"Color"]=b});return a}};f=a.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"}})(jQuery);(function(){var d=["add","remove","toggle"],e={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};a.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(c,b){a.fx.step[b]=function(a){if(a.end!=="none"&&!a.setAttr||a.pos===1&&!a.setAttr){jQuery.style(a.elem,b,a.end);a.setAttr=true}}});function b(d){var c,f,b=d.ownerDocument.defaultView?d.ownerDocument.defaultView.getComputedStyle(d,null):d.currentStyle,e={};if(b&&b.length&&b[0]&&b[b[0]]){f=b.length;while(f--){c=b[f];if(typeof b[c]==="string")e[a.camelCase(c)]=b[c]}}else for(c in b)if(typeof b[c]==="string")e[c]=b[c];return e}function f(g,d){var f={},b,c;for(b in d){c=d[b];if(g[b]!==c)if(!e[b])if(a.fx.step[b]||!isNaN(parseFloat(c)))f[b]=c}return f}if(!a.fn.addBack)a.fn.addBack=function(a){return this.add(a==null?this.prevObject:this.prevObject.filter(a))};a.effects.animateClass=function(e,h,i,g){var c=a.speed(h,i,g);return this.queue(function(){var h=a(this),j=h.attr("class")||"",i,g=c.children?h.find("*").addBack():h;g=g.map(function(){var c=a(this);return{el:c,start:b(this)}});i=function(){a.each(d,function(b,a){e[a]&&h[a+"Class"](e[a])})};i();g=g.map(function(){this.end=b(this.el[0]);this.diff=f(this.start,this.end);return this});h.attr("class",j);g=g.map(function(){var d=this,b=a.Deferred(),e=a.extend({},c,{queue:false,complete:function(){b.resolve(d)}});this.el.animate(this.diff,e);return b.promise()});a.when.apply(a,g.get()).done(function(){i();a.each(arguments,function(){var b=this.el;a.each(this.diff,function(a){b.css(a,"")})});c.complete.call(h[0])})})};a.fn.extend({addClass:function(b){return function(d,c,f,e){return c?a.effects.animateClass.call(this,{add:d},c,f,e):b.apply(this,arguments)}}(a.fn.addClass),removeClass:function(b){return function(c,f,e,d){return arguments.length>1?a.effects.animateClass.call(this,{remove:c},f,e,d):b.apply(this,arguments)}}(a.fn.removeClass),toggleClass:function(b){return function(e,d,f,g,h){return typeof d==="boolean"||d===c?!f?b.apply(this,arguments):a.effects.animateClass.call(this,d?{add:e}:{remove:e},f,g,h):a.effects.animateClass.call(this,{toggle:e},d,f,g)}}(a.fn.toggleClass),switchClass:function(d,f,e,c,b){return a.effects.animateClass.call(this,{add:f,remove:d},e,c,b)}})})();(function(){a.extend(a.effects,{version:"1.10.2",save:function(d,c){for(var a=0;a<c.length;a++)c[a]!==null&&d.data(b+c[a],d[0].style[c[a]])},restore:function(f,d){for(var e,a=0;a<d.length;a++)if(d[a]!==null){e=f.data(b+d[a]);if(e===c)e="";f.css(d[a],e)}},setMode:function(b,a){if(a==="toggle")a=b.is(":hidden")?"show":"hide";return a},getBaseline:function(c,d){var b,a;switch(c[0]){case"top":b=0;break;case"middle":b=.5;break;case"bottom":b=1;break;default:b=c[0]/d.height}switch(c[1]){case"left":a=0;break;case"center":a=.5;break;case"right":a=1;break;default:a=c[1]/d.width}return{x:a,y:b}},createWrapper:function(b){if(b.parent().is(".ui-effects-wrapper"))return b.parent();var d={width:b.outerWidth(true),height:b.outerHeight(true),"float":b.css("float")},e=a("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),f={width:b.width(),height:b.height()},c=document.activeElement;try{c.id}catch(g){c=document.body}b.wrap(e);(b[0]===c||a.contains(b[0],c))&&a(c).focus();e=b.parent();if(b.css("position")==="static"){e.css({position:"relative"});b.css({position:"relative"})}else{a.extend(d,{position:b.css("position"),zIndex:b.css("z-index")});a.each(["top","left","bottom","right"],function(c,a){d[a]=b.css(a);if(isNaN(parseInt(d[a],10)))d[a]="auto"});b.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})}b.css(f);return e.css(d).show()},removeWrapper:function(b){var c=document.activeElement;if(b.parent().is(".ui-effects-wrapper")){b.parent().replaceWith(b);(b[0]===c||a.contains(b[0],c))&&a(c).focus()}return b},setTransition:function(c,e,d,b){b=b||{};a.each(e,function(f,e){var a=c.cssUnit(e);if(a[0]>0)b[e]=a[0]*d+a[1]});return b}});function d(d,b,c,e){if(a.isPlainObject(d)){b=d;d=d.effect}d={effect:d};if(b==null)b={};if(a.isFunction(b)){e=b;c=null;b={}}if(typeof b==="number"||a.fx.speeds[b]){e=c;c=b;b={}}if(a.isFunction(c)){e=c;c=null}b&&a.extend(d,b);c=c||b.duration;d.duration=a.fx.off?0:typeof c==="number"?c:c in a.fx.speeds?a.fx.speeds[c]:a.fx.speeds._default;d.complete=e||b.complete;return d}function e(b){return!b||typeof b==="number"||a.fx.speeds[b]?true:typeof b==="string"&&!a.effects.effect[b]?true:a.isFunction(b)?true:typeof b==="object"&&!b.effect?true:false}a.fn.extend({effect:function(){var b=d.apply(this,arguments),f=b.mode,e=b.queue,c=a.effects.effect[b.effect];if(a.fx.off||!c)return f?this[f](b.duration,b.complete):this.each(function(){b.complete&&b.complete.call(this)});function g(h){var d=a(this),f=b.complete,e=b.mode;function g(){a.isFunction(f)&&f.call(d[0]);a.isFunction(h)&&h()}if(d.is(":hidden")?e==="hide":e==="show"){d[e]();g()}else c.call(d[0],b,g)}return e===false?this.each(g):this.queue(e||"fx",g)},show:function(a){return function(c){if(e(c))return a.apply(this,arguments);else{var b=d.apply(this,arguments);b.mode="show";return this.effect.call(this,b)}}}(a.fn.show),hide:function(a){return function(c){if(e(c))return a.apply(this,arguments);else{var b=d.apply(this,arguments);b.mode="hide";return this.effect.call(this,b)}}}(a.fn.hide),toggle:function(a){return function(b){if(e(b)||typeof b==="boolean")return a.apply(this,arguments);else{var c=d.apply(this,arguments);c.mode="toggle";return this.effect.call(this,c)}}}(a.fn.toggle),cssUnit:function(d){var b=this.css(d),c=[];a.each(["em","px","%","pt"],function(d,a){if(b.indexOf(a)>0)c=[parseFloat(b),a]});return c}})})();(function(){var b={};a.each(["Quad","Cubic","Quart","Quint","Expo"],function(c,a){b[a]=function(a){return Math.pow(a,c+2)}});a.extend(b,{Sine:function(a){return 1-Math.cos(a*Math.PI/2)},Circ:function(a){return 1-Math.sqrt(1-a*a)},Elastic:function(a){return a===0||a===1?a:-Math.pow(2,8*(a-1))*Math.sin(((a-1)*80-7.5)*Math.PI/15)},Back:function(a){return a*a*(3*a-2)},Bounce:function(c){var b,a=4;while(c<((b=Math.pow(2,--a))-1)/11);return 1/Math.pow(4,3-a)-7.5625*Math.pow((b*3-2)/22-c,2)}});a.each(b,function(c,b){a.easing["easeIn"+c]=b;a.easing["easeOut"+c]=function(a){return 1-b(1-a)};a.easing["easeInOut"+c]=function(a){return a<.5?b(a*2)/2:1-b(a*-2+2)/2}})})()})(jQuery);(function(a){var d=0,b={},c={};b.height=b.paddingTop=b.paddingBottom=b.borderTopWidth=b.borderBottomWidth="hide";c.height=c.paddingTop=c.paddingBottom=c.borderTopWidth=c.borderBottomWidth="show";a.widget("ui.accordion",{version:"1.10.2",options:{active:0,animate:{},collapsible:false,event:"click",header:"> li > :first-child,> :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},_create:function(){var b=this.options;this.prevShow=this.prevHide=a();this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role","tablist");if(!b.collapsible&&(b.active===false||b.active==null))b.active=0;this._processPanels();if(b.active<0)b.active+=this.headers.length;this._refresh()},_getCreateEventData:function(){return{header:this.active,panel:!this.active.length?a():this.active.next(),content:!this.active.length?a():this.active.next()}},_createIcons:function(){var b=this.options.icons;if(b){a("<span>").addClass("ui-accordion-header-icon ui-icon "+b.header).prependTo(this.headers);this.active.children(".ui-accordion-header-icon").removeClass(b.header).addClass(b.activeHeader);this.headers.addClass("ui-accordion-icons")}},_destroyIcons:function(){this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()},_destroy:function(){var a;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role");this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function(){/^ui-accordion/.test(this.id)&&this.removeAttribute("id")});this._destroyIcons();a=this.headers.next().css("display","").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function(){/^ui-accordion/.test(this.id)&&this.removeAttribute("id")});this.options.heightStyle!=="content"&&a.css("height","")},_setOption:function(b,a){if(b==="active"){this._activate(a);return}if(b==="event"){this.options.event&&this._off(this.headers,this.options.event);this._setupEvents(a)}this._super(b,a);b==="collapsible"&&!a&&this.options.active===false&&this._activate(0);if(b==="icons"){this._destroyIcons();a&&this._createIcons()}b==="disabled"&&this.headers.add(this.headers.next()).toggleClass("ui-state-disabled",!!a)},_keydown:function(d){if(d.altKey||d.ctrlKey)return;var b=a.ui.keyCode,e=this.headers.length,f=this.headers.index(d.target),c=false;switch(d.keyCode){case b.RIGHT:case b.DOWN:c=this.headers[(f+1)%e];break;case b.LEFT:case b.UP:c=this.headers[(f-1+e)%e];break;case b.SPACE:case b.ENTER:this._eventHandler(d);break;case b.HOME:c=this.headers[0];break;case b.END:c=this.headers[e-1]}if(c){a(d.target).attr("tabIndex",-1);a(c).attr("tabIndex",0);c.focus();d.preventDefault()}},_panelKeyDown:function(b){b.keyCode===a.ui.keyCode.UP&&b.ctrlKey&&a(b.currentTarget).prev().focus()},refresh:function(){var b=this.options;this._processPanels();if(b.active===false&&b.collapsible===true||!this.headers.length){b.active=false;this.active=a()}if(b.active===false)this._activate(0);else if(this.active.length&&!a.contains(this.element[0],this.active[0]))if(this.headers.length===this.headers.find(".ui-state-disabled").length){b.active=false;this.active=a()}else this._activate(Math.max(0,b.active-1));else b.active=this.headers.index(this.active);this._destroyIcons();this._refresh()},_processPanels:function(){this.headers=this.element.find(this.options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all");this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()},_refresh:function(){var b,c=this.options,f=c.heightStyle,g=this.element.parent(),e=this.accordionId="ui-accordion-"+(this.element.attr("id")||++d);this.active=this._findActive(c.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all");this.active.next().addClass("ui-accordion-content-active").show();this.headers.attr("role","tab").each(function(g){var d=a(this),b=d.attr("id"),f=d.next(),c=f.attr("id");if(!b){b=e+"-header-"+g;d.attr("id",b)}if(!c){c=e+"-panel-"+g;f.attr("id",c)}d.attr("aria-controls",c);f.attr("aria-labelledby",b)}).next().attr("role","tabpanel");this.headers.not(this.active).attr({"aria-selected":"false",tabIndex:-1}).next().attr({"aria-expanded":"false","aria-hidden":"true"}).hide();if(!this.active.length)this.headers.eq(0).attr("tabIndex",0);else this.active.attr({"aria-selected":"true",tabIndex:0}).next().attr({"aria-expanded":"true","aria-hidden":"false"});this._createIcons();this._setupEvents(c.event);if(f==="fill"){b=g.height();this.element.siblings(":visible").each(function(){var d=a(this),c=d.css("position");if(c==="absolute"||c==="fixed")return;b-=d.outerHeight(true)});this.headers.each(function(){b-=a(this).outerHeight(true)});this.headers.next().each(function(){a(this).height(Math.max(0,b-a(this).innerHeight()+a(this).height()))}).css("overflow","auto")}else if(f==="auto"){b=0;this.headers.next().each(function(){b=Math.max(b,a(this).css("height","").height())}).height(b)}},_activate:function(c){var b=this._findActive(c)[0];if(b===this.active[0])return;b=b||this.active[0];this._eventHandler({target:b,currentTarget:b,preventDefault:a.noop})},_findActive:function(b){return typeof b==="number"?this.headers.eq(b):a()},_setupEvents:function(c){var b={keydown:"_keydown"};c&&a.each(c.split(" "),function(c,a){b[a]="_eventHandler"});this._off(this.headers.add(this.headers.next()));this._on(this.headers,b);this._on(this.headers.next(),{keydown:"_panelKeyDown"});this._hoverable(this.headers);this._focusable(this.headers)},_eventHandler:function(g){var b=this.options,d=this.active,c=a(g.currentTarget),e=c[0]===d[0],f=e&&b.collapsible,j=f?a():c.next(),i=d.next(),h={oldHeader:d,oldPanel:i,newHeader:f?a():c,newPanel:j};g.preventDefault();if(e&&!b.collapsible||this._trigger("beforeActivate",g,h)===false)return;b.active=f?false:this.headers.index(c);this.active=e?a():c;this._toggle(h);d.removeClass("ui-accordion-header-active ui-state-active");b.icons&&d.children(".ui-accordion-header-icon").removeClass(b.icons.activeHeader).addClass(b.icons.header);if(!e){c.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top");b.icons&&c.children(".ui-accordion-header-icon").removeClass(b.icons.header).addClass(b.icons.activeHeader);c.next().addClass("ui-accordion-content-active")}},_toggle:function(d){var c=d.newPanel,b=this.prevShow.length?this.prevShow:d.oldPanel;this.prevShow.add(this.prevHide).stop(true,true);this.prevShow=c;this.prevHide=b;if(this.options.animate)this._animate(c,b,d);else{b.hide();c.show();this._toggleComplete(d)}b.attr({"aria-expanded":"false","aria-hidden":"true"});b.prev().attr("aria-selected","false");if(c.length&&b.length)b.prev().attr("tabIndex",-1);else c.length&&this.headers.filter(function(){return a(this).attr("tabIndex")===0}).attr("tabIndex",-1);c.attr({"aria-expanded":"true","aria-hidden":"false"}).prev().attr({"aria-selected":"true",tabIndex:0})},_animate:function(g,f,m){var k,d,a,l=this,j=0,n=g.length&&(!f.length||g.index()<f.index()),h=this.options.animate||{},e=n&&h.down||h,i=function(){l._toggleComplete(m)};if(typeof e==="number")a=e;if(typeof e==="string")d=e;d=d||e.easing||h.easing;a=a||e.duration||h.duration;if(!f.length)return g.animate(c,a,d,i);if(!g.length)return f.animate(b,a,d,i);k=g.show().outerHeight();f.animate(b,{duration:a,easing:d,step:function(a,b){b.now=Math.round(a)}});g.hide().animate(c,{duration:a,easing:d,complete:i,step:function(b,a){a.now=Math.round(b);if(a.prop!=="height")j+=a.now;else if(l.options.heightStyle!=="content"){a.now=Math.round(k-f.outerHeight()-j);j=0}}})},_toggleComplete:function(b){var a=b.oldPanel;a.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all");if(a.length)a.parent()[0].className=a.parent()[0].className;this._trigger("activate",null,b)}})})(jQuery);(function(a){var b=0;a.widget("ui.autocomplete",{version:"1.10.2",defaultElement:"<input>",options:{appendTo:null,autoFocus:false,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},pending:0,_create:function(){var b,c,d,f=this.element[0].nodeName.toLowerCase(),e=f==="textarea",g=f==="input";this.isMultiLine=e?true:g?false:this.element.prop("isContentEditable");this.valueMethod=this.element[e||g?"val":"text"];this.isNewMenu=true;this.element.addClass("ui-autocomplete-input").attr("autocomplete","off");this._on(this.element,{keydown:function(e){if(this.element.prop("readOnly")){b=true;d=true;c=true;return}b=false;d=false;c=false;var f=a.ui.keyCode;switch(e.keyCode){case f.PAGE_UP:b=true;this._move("previousPage",e);break;case f.PAGE_DOWN:b=true;this._move("nextPage",e);break;case f.UP:b=true;this._keyEvent("previous",e);break;case f.DOWN:b=true;this._keyEvent("next",e);break;case f.ENTER:case f.NUMPAD_ENTER:if(this.menu.active){b=true;e.preventDefault();this.menu.select(e)}break;case f.TAB:this.menu.active&&this.menu.select(e);break;case f.ESCAPE:if(this.menu.element.is(":visible")){this._value(this.term);this.close(e);e.preventDefault()}break;default:c=true;this._searchTimeout(e)}},keypress:function(d){if(b){b=false;d.preventDefault();return}if(c)return;var e=a.ui.keyCode;switch(d.keyCode){case e.PAGE_UP:this._move("previousPage",d);break;case e.PAGE_DOWN:this._move("nextPage",d);break;case e.UP:this._keyEvent("previous",d);break;case e.DOWN:this._keyEvent("next",d)}},input:function(a){if(d){d=false;a.preventDefault();return}this._searchTimeout(a)},focus:function(){this.selectedItem=null;this.previous=this._value()},blur:function(a){if(this.cancelBlur){delete this.cancelBlur;return}clearTimeout(this.searching);this.close(a);this._change(a)}});this._initSource();this.menu=a("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({input:a(),role:null}).hide().data("ui-menu");this._on(this.menu.element,{mousedown:function(c){c.preventDefault();this.cancelBlur=true;this._delay(function(){delete this.cancelBlur});var b=this.menu.element[0];!a(c.target).closest(".ui-menu-item").length&&this._delay(function(){var c=this;this.document.one("mousedown",function(d){d.target!==c.element[0]&&d.target!==b&&!a.contains(b,d.target)&&c.close()})})},menufocus:function(b,d){if(this.isNewMenu){this.isNewMenu=false;if(b.originalEvent&&/^mouse/.test(b.originalEvent.type)){this.menu.blur();this.document.one("mousemove",function(){a(b.target).trigger(b.originalEvent)});return}}var c=d.item.data("ui-autocomplete-item");if(false!==this._trigger("focus",b,{item:c}))b.originalEvent&&/^key/.test(b.originalEvent.type)&&this._value(c.value);else this.liveRegion.text(c.value)},menuselect:function(c,d){var a=d.item.data("ui-autocomplete-item"),b=this.previous;if(this.element[0]!==this.document[0].activeElement){this.element.focus();this.previous=b;this._delay(function(){this.previous=b;this.selectedItem=a})}false!==this._trigger("select",c,{item:a})&&this._value(a.value);this.term=this._value();this.close(c);this.selectedItem=a}});this.liveRegion=a("<span>",{role:"status","aria-live":"polite"}).addClass("ui-helper-hidden-accessible").insertAfter(this.element);this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching);this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete");this.menu.element.remove();this.liveRegion.remove()},_setOption:function(a,b){this._super(a,b);a==="source"&&this._initSource();a==="appendTo"&&this.menu.element.appendTo(this._appendTo());a==="disabled"&&b&&this.xhr&&this.xhr.abort()},_appendTo:function(){var b=this.options.appendTo;if(b)b=b.jquery||b.nodeType?a(b):this.document.find(b).eq(0);if(!b)b=this.element.closest(".ui-front");if(!b.length)b=this.document[0].body;return b},_initSource:function(){var c,d,b=this;if(a.isArray(this.options.source)){c=this.options.source;this.source=function(d,b){b(a.ui.autocomplete.filter(c,d.term))}}else if(typeof this.options.source==="string"){d=this.options.source;this.source=function(e,c){b.xhr&&b.xhr.abort();b.xhr=a.ajax({url:d,data:e,dataType:"json",success:function(a){c(a)},error:function(){c([])}})}}else this.source=this.options.source},_searchTimeout:function(a){clearTimeout(this.searching);this.searching=this._delay(function(){if(this.term!==this._value()){this.selectedItem=null;this.search(null,a)}},this.options.delay)},search:function(a,b){a=a!=null?a:this._value();this.term=this._value();return a.length<this.options.minLength?this.close(b):this._trigger("search",b)===false?void 0:this._search(a)},_search:function(a){this.pending++;this.element.addClass("ui-autocomplete-loading");this.cancelSearch=false;this.source({term:a},this._response())},_response:function(){var a=this,c=++b;return function(d){c===b&&a.__response(d);a.pending--;!a.pending&&a.element.removeClass("ui-autocomplete-loading")}},__response:function(a){if(a)a=this._normalize(a);this._trigger("response",null,{content:a});if(!this.options.disabled&&a&&a.length&&!this.cancelSearch){this._suggest(a);this._trigger("open")}else this._close()},close:function(a){this.cancelSearch=true;this._close(a)},_close:function(a){if(this.menu.element.is(":visible")){this.menu.element.hide();this.menu.blur();this.isNewMenu=true;this._trigger("close",a)}},_change:function(a){this.previous!==this._value()&&this._trigger("change",a,{item:this.selectedItem})},_normalize:function(b){return b.length&&b[0].label&&b[0].value?b:a.map(b,function(b){return typeof b==="string"?{label:b,value:b}:a.extend({label:b.label||b.value,value:b.value||b.label},b)})},_suggest:function(c){var b=this.menu.element.empty();this._renderMenu(b,c);this.isNewMenu=true;this.menu.refresh();b.show();this._resizeMenu();b.position(a.extend({of:this.element},this.options.position));this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var a=this.menu.element;a.outerWidth(Math.max(a.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(d,b){var c=this;a.each(b,function(b,a){c._renderItemData(d,a)})},_renderItemData:function(b,a){return this._renderItem(b,a).data("ui-autocomplete-item",a)},_renderItem:function(c,b){return a("<li>").append(a("<a>").text(b.label)).appendTo(c)},_move:function(a,b){if(!this.menu.element.is(":visible")){this.search(null,b);return}if(this.menu.isFirstItem()&&/^previous/.test(a)||this.menu.isLastItem()&&/^next/.test(a)){this._value(this.term);this.menu.blur();return}this.menu[a](b)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(b,a){if(!this.isMultiLine||this.menu.element.is(":visible")){this._move(b,a);a.preventDefault()}}});a.extend(a.ui.autocomplete,{escapeRegex:function(a){return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(c,d){var b=new RegExp(a.ui.autocomplete.escapeRegex(d),"i");return a.grep(c,function(a){return b.test(a.label||a.value||a)})}});a.widget("ui.autocomplete",a.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(a){return a+(a>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(a){var b;this._superApply(arguments);if(this.options.disabled||this.cancelSearch)return;if(a&&a.length)b=this.options.messages.results(a.length);else b=this.options.messages.noResults;this.liveRegion.text(b)}})})(jQuery);(function(a){var c,g,h,b,d="ui-button ui-widget ui-state-default ui-corner-all",j="ui-state-hover ui-state-active ",e="ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",i=function(){var b=a(this).find(":ui-button");setTimeout(function(){b.button("refresh")},1)},f=function(d){var b=d.name,e=d.form,c=a([]);if(b){b=b.replace(/'/g,"\\'");if(e)c=a(e).find("[name='"+b+"']");else c=a("[name='"+b+"']",d.ownerDocument).filter(function(){return!this.form})}return c};a.widget("ui.button",{version:"1.10.2",defaultElement:"<button>",options:{disabled:null,text:true,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset"+this.eventNamespace).bind("reset"+this.eventNamespace,i);if(typeof this.options.disabled!=="boolean")this.options.disabled=!!this.element.prop("disabled");else this.element.prop("disabled",this.options.disabled);this._determineButtonType();this.hasTitle=!!this.buttonElement.attr("title");var j=this,e=this.options,k=this.type==="checkbox"||this.type==="radio",m=!k?"ui-state-active":"",l="ui-state-focus";if(e.label===null)e.label=this.type==="input"?this.buttonElement.val():this.buttonElement.html();this._hoverable(this.buttonElement);this.buttonElement.addClass(d).attr("role","button").bind("mouseenter"+this.eventNamespace,function(){if(e.disabled)return;this===c&&a(this).addClass("ui-state-active")}).bind("mouseleave"+this.eventNamespace,function(){if(e.disabled)return;a(this).removeClass(m)}).bind("click"+this.eventNamespace,function(a){if(e.disabled){a.preventDefault();a.stopImmediatePropagation()}});this.element.bind("focus"+this.eventNamespace,function(){j.buttonElement.addClass(l)}).bind("blur"+this.eventNamespace,function(){j.buttonElement.removeClass(l)});if(k){this.element.bind("change"+this.eventNamespace,function(){if(b)return;j.refresh()});this.buttonElement.bind("mousedown"+this.eventNamespace,function(a){if(e.disabled)return;b=false;g=a.pageX;h=a.pageY}).bind("mouseup"+this.eventNamespace,function(a){if(e.disabled)return;if(g!==a.pageX||h!==a.pageY)b=true})}if(this.type==="checkbox")this.buttonElement.bind("click"+this.eventNamespace,function(){if(e.disabled||b)return false});else if(this.type==="radio")this.buttonElement.bind("click"+this.eventNamespace,function(){if(e.disabled||b)return false;a(this).addClass("ui-state-active");j.buttonElement.attr("aria-pressed","true");var c=j.element[0];f(c).not(c).map(function(){return a(this).button("widget")[0]}).removeClass("ui-state-active").attr("aria-pressed","false")});else{this.buttonElement.bind("mousedown"+this.eventNamespace,function(){if(e.disabled)return false;a(this).addClass("ui-state-active");c=this;j.document.one("mouseup",function(){c=null})}).bind("mouseup"+this.eventNamespace,function(){if(e.disabled)return false;a(this).removeClass("ui-state-active")}).bind("keydown"+this.eventNamespace,function(b){if(e.disabled)return false;(b.keyCode===a.ui.keyCode.SPACE||b.keyCode===a.ui.keyCode.ENTER)&&a(this).addClass("ui-state-active")}).bind("keyup"+this.eventNamespace+" blur"+this.eventNamespace,function(){a(this).removeClass("ui-state-active")});this.buttonElement.is("a")&&this.buttonElement.keyup(function(b){b.keyCode===a.ui.keyCode.SPACE&&a(this).click()})}this._setOption("disabled",e.disabled);this._resetButton()},_determineButtonType:function(){var a,b,c;if(this.element.is("[type=checkbox]"))this.type="checkbox";else if(this.element.is("[type=radio]"))this.type="radio";else if(this.element.is("input"))this.type="input";else this.type="button";if(this.type==="checkbox"||this.type==="radio"){a=this.element.parents().last();b="label[for='"+this.element.attr("id")+"']";this.buttonElement=a.find(b);if(!this.buttonElement.length){a=a.length?a.siblings():this.element.siblings();this.buttonElement=a.filter(b);if(!this.buttonElement.length)this.buttonElement=a.find(b)}this.element.addClass("ui-helper-hidden-accessible");c=this.element.is(":checked");c&&this.buttonElement.addClass("ui-state-active");this.buttonElement.prop("aria-pressed",c)}else this.buttonElement=this.element},widget:function(){return this.buttonElement},_destroy:function(){this.element.removeClass("ui-helper-hidden-accessible");this.buttonElement.removeClass(d+" "+j+" "+e).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html());!this.hasTitle&&this.buttonElement.removeAttr("title")},_setOption:function(b,a){this._super(b,a);if(b==="disabled"){if(a)this.element.prop("disabled",true);else this.element.prop("disabled",false);return}this._resetButton()},refresh:function(){var b=this.element.is("input, button")?this.element.is(":disabled"):this.element.hasClass("ui-button-disabled");b!==this.options.disabled&&this._setOption("disabled",b);if(this.type==="radio")f(this.element[0]).each(function(){if(a(this).is(":checked"))a(this).button("widget").addClass("ui-state-active").attr("aria-pressed","true");else a(this).button("widget").removeClass("ui-state-active").attr("aria-pressed","false")});else if(this.type==="checkbox")if(this.element.is(":checked"))this.buttonElement.addClass("ui-state-active").attr("aria-pressed","true");else this.buttonElement.removeClass("ui-state-active").attr("aria-pressed","false")},_resetButton:function(){if(this.type==="input"){this.options.label&&this.element.val(this.options.label);return}var c=this.buttonElement.removeClass(e),g=a("<span></span>",this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(c.empty()).text(),b=this.options.icons,f=b.primary&&b.secondary,d=[];if(b.primary||b.secondary){this.options.text&&d.push("ui-button-text-icon"+(f?"s":b.primary?"-primary":"-secondary"));b.primary&&c.prepend("<span class='ui-button-icon-primary ui-icon "+b.primary+"'></span>");b.secondary&&c.append("<span class='ui-button-icon-secondary ui-icon "+b.secondary+"'></span>");if(!this.options.text){d.push(f?"ui-button-icons-only":"ui-button-icon-only");!this.hasTitle&&c.attr("title",a.trim(g))}}else d.push("ui-button-text-only");c.addClass(d.join(" "))}});a.widget("ui.buttonset",{version:"1.10.2",options:{items:"button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"},_create:function(){this.element.addClass("ui-buttonset")},_init:function(){this.refresh()},_setOption:function(a,b){a==="disabled"&&this.buttons.button("option",a,b);this._super(a,b)},refresh:function(){var b=this.element.css("direction")==="rtl";this.buttons=this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function(){return a(this).button("widget")[0]}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(b?"ui-corner-right":"ui-corner-left").end().filter(":last").addClass(b?"ui-corner-left":"ui-corner-right").end().end()},_destroy:function(){this.element.removeClass("ui-buttonset");this.buttons.map(function(){return a(this).button("widget")[0]}).removeClass("ui-corner-left ui-corner-right").end().button("destroy")}})})(jQuery);(function(a,d){a.extend(a.ui,{datepicker:{version:"1.10.2"}});var b="datepicker",c=(new Date).getTime(),f;function g(){this._curInst=null;this._keyEvent=false;this._disabledInputs=[];this._datepickerShowing=false;this._inDialog=false;this._mainDivId="ui-datepicker-div";this._inlineClass="ui-datepicker-inline";this._appendClass="ui-datepicker-append";this._triggerClass="ui-datepicker-trigger";this._dialogClass="ui-datepicker-dialog";this._disableClass="ui-datepicker-disabled";this._unselectableClass="ui-datepicker-unselectable";this._currentClass="ui-datepicker-current-day";this._dayOverClass="ui-datepicker-days-cell-over";this.regional=[];this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:false,showMonthAfterYear:false,yearSuffix:""};this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:false,hideIfNoPrevNext:false,navigationAsDateFormat:false,gotoCurrent:false,changeMonth:false,changeYear:false,yearRange:"c-10:c+10",showOtherMonths:false,selectOtherMonths:false,showWeek:false,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:true,showButtonPanel:false,autoSize:false,disabled:false};a.extend(this._defaults,this.regional[""]);this.dpDiv=h(a("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}a.extend(g.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(a){e(this._defaults,a||{});return this},_attachDatepicker:function(b,f){var c,e,d;c=b.nodeName.toLowerCase();e=c==="div"||c==="span";if(!b.id){this.uuid+=1;b.id="dp"+this.uuid}d=this._newInst(a(b),e);d.settings=a.extend({},f||{});if(c==="input")this._connectDatepicker(b,d);else e&&this._inlineDatepicker(b,d)},_newInst:function(c,b){var d=c[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:d,input:c,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:b,dpDiv:!b?this.dpDiv:h(a("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}},_connectDatepicker:function(d,c){var e=a(d);c.append=a([]);c.trigger=a([]);if(e.hasClass(this.markerClassName))return;this._attachments(e,c);e.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp);this._autoSize(c);a.data(d,b,c);c.settings.disabled&&this._disableDatepicker(d)},_attachments:function(c,b){var e,d,f,g=this._get(b,"appendText"),h=this._get(b,"isRTL");b.append&&b.append.remove();if(g){b.append=a("<span class='"+this._appendClass+"'>"+g+"</span>");c[h?"before":"after"](b.append)}c.unbind("focus",this._showDatepicker);b.trigger&&b.trigger.remove();e=this._get(b,"showOn");(e==="focus"||e==="both")&&c.focus(this._showDatepicker);if(e==="button"||e==="both"){d=this._get(b,"buttonText");f=this._get(b,"buttonImage");b.trigger=a(this._get(b,"buttonImageOnly")?a("<img/>").addClass(this._triggerClass).attr({src:f,alt:d,title:d}):a("<button type='button'></button>").addClass(this._triggerClass).html(!f?d:a("<img/>").attr({src:f,alt:d,title:d})));c[h?"before":"after"](b.trigger);b.trigger.click(function(){if(a.datepicker._datepickerShowing&&a.datepicker._lastInput===c[0])a.datepicker._hideDatepicker();else if(a.datepicker._datepickerShowing&&a.datepicker._lastInput!==c[0]){a.datepicker._hideDatepicker();a.datepicker._showDatepicker(c[0])}else a.datepicker._showDatepicker(c[0]);return false})}},_autoSize:function(a){if(this._get(a,"autoSize")&&!a.inline){var e,g,f,b,c=new Date(2009,12-1,20),d=this._get(a,"dateFormat");if(d.match(/[DM]/)){e=function(a){g=0;f=0;for(b=0;b<a.length;b++)if(a[b].length>g){g=a[b].length;f=b}return f};c.setMonth(e(this._get(a,d.match(/MM/)?"monthNames":"monthNamesShort")));c.setDate(e(this._get(a,d.match(/DD/)?"dayNames":"dayNamesShort"))+20-c.getDay())}a.input.attr("size",this._formatDate(a,c).length)}},_inlineDatepicker:function(d,c){var e=a(d);if(e.hasClass(this.markerClassName))return;e.addClass(this.markerClassName).append(c.dpDiv);a.data(d,b,c);this._setDate(c,this._getDefaultDate(c),true);this._updateDatepicker(c);this._updateAlternate(c);c.settings.disabled&&this._disableDatepicker(d);c.dpDiv.css("display","block")},_dialogDatepicker:function(n,d,l,m,f){var k,h,g,i,j,c=this._dialogInst;if(!c){this.uuid+=1;k="dp"+this.uuid;this._dialogInput=a("<input type='text' id='"+k+"' style='position: absolute; top: -100px; width: 0px;'/>");this._dialogInput.keydown(this._doKeyDown);a("body").append(this._dialogInput);c=this._dialogInst=this._newInst(this._dialogInput,false);c.settings={};a.data(this._dialogInput[0],b,c)}e(c.settings,m||{});d=d&&d.constructor===Date?this._formatDate(c,d):d;this._dialogInput.val(d);this._pos=f?f.length?f:[f.pageX,f.pageY]:null;if(!this._pos){h=document.documentElement.clientWidth;g=document.documentElement.clientHeight;i=document.documentElement.scrollLeft||document.body.scrollLeft;j=document.documentElement.scrollTop||document.body.scrollTop;this._pos=[h/2-100+i,g/2-150+j]}this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px");c.settings.onSelect=l;this._inDialog=true;this.dpDiv.addClass(this._dialogClass);this._showDatepicker(this._dialogInput[0]);a.blockUI&&a.blockUI(this.dpDiv);a.data(this._dialogInput[0],b,c);return this},_destroyDatepicker:function(d){var c,e=a(d),f=a.data(d,b);if(!e.hasClass(this.markerClassName))return;c=d.nodeName.toLowerCase();a.removeData(d,b);if(c==="input"){f.append.remove();f.trigger.remove();e.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)}else(c==="div"||c==="span")&&e.removeClass(this.markerClassName).empty()},_enableDatepicker:function(c){var d,e,f=a(c),g=a.data(c,b);if(!f.hasClass(this.markerClassName))return;d=c.nodeName.toLowerCase();if(d==="input"){c.disabled=false;g.trigger.filter("button").each(function(){this.disabled=false}).end().filter("img").css({opacity:"1.0",cursor:""})}else if(d==="div"||d==="span"){e=f.children("."+this._inlineClass);e.children().removeClass("ui-state-disabled");e.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",false)}this._disabledInputs=a.map(this._disabledInputs,function(a){return a===c?null:a})},_disableDatepicker:function(c){var d,e,f=a(c),g=a.data(c,b);if(!f.hasClass(this.markerClassName))return;d=c.nodeName.toLowerCase();if(d==="input"){c.disabled=true;g.trigger.filter("button").each(function(){this.disabled=true}).end().filter("img").css({opacity:"0.5",cursor:"default"})}else if(d==="div"||d==="span"){e=f.children("."+this._inlineClass);e.children().addClass("ui-state-disabled");e.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",true)}this._disabledInputs=a.map(this._disabledInputs,function(a){return a===c?null:a});this._disabledInputs[this._disabledInputs.length]=c},_isDisabledDatepicker:function(b){if(!b)return false;for(var a=0;a<this._disabledInputs.length;a++)if(this._disabledInputs[a]===b)return true;return false},_getInst:function(c){try{return a.data(c,b)}catch(d){throw"Missing instance data for this datepicker";}},_optionDatepicker:function(g,f,k){var c,j,i,h,b=this._getInst(g);if(arguments.length===2&&typeof f==="string")return f==="defaults"?a.extend({},a.datepicker._defaults):b?f==="all"?a.extend({},b.settings):this._get(b,f):null;c=f||{};if(typeof f==="string"){c={};c[f]=k}if(b){this._curInst===b&&this._hideDatepicker();j=this._getDateDatepicker(g,true);i=this._getMinMaxDate(b,"min");h=this._getMinMaxDate(b,"max");e(b.settings,c);if(i!==null&&c.dateFormat!==d&&c.minDate===d)b.settings.minDate=this._formatDate(b,i);if(h!==null&&c.dateFormat!==d&&c.maxDate===d)b.settings.maxDate=this._formatDate(b,h);if("disabled"in c)if(c.disabled)this._disableDatepicker(g);else this._enableDatepicker(g);this._attachments(a(g),b);this._autoSize(b);this._setDate(b,j);this._updateAlternate(b);this._updateDatepicker(b)}},_changeDatepicker:function(a,c,b){this._optionDatepicker(a,c,b)},_refreshDatepicker:function(b){var a=this._getInst(b);a&&this._updateDatepicker(a)},_setDateDatepicker:function(b,c){var a=this._getInst(b);if(a){this._setDate(a,c);this._updateDatepicker(a);this._updateAlternate(a)}},_getDateDatepicker:function(c,b){var a=this._getInst(c);a&&!a.inline&&this._setDateFromField(a,b);return a?this._getDate(a):null},_doKeyDown:function(b){var e,g,f,c=a.datepicker._getInst(b.target),d=true,h=c.dpDiv.is(".ui-datepicker-rtl");c._keyEvent=true;if(a.datepicker._datepickerShowing)switch(b.keyCode){case 9:a.datepicker._hideDatepicker();d=false;break;case 13:f=a("td."+a.datepicker._dayOverClass+":not(."+a.datepicker._currentClass+")",c.dpDiv);f[0]&&a.datepicker._selectDay(b.target,c.selectedMonth,c.selectedYear,f[0]);e=a.datepicker._get(c,"onSelect");if(e){g=a.datepicker._formatDate(c);e.apply(c.input?c.input[0]:null,[g,c])}else a.datepicker._hideDatepicker();return false;case 27:a.datepicker._hideDatepicker();break;case 33:a.datepicker._adjustDate(b.target,b.ctrlKey?-a.datepicker._get(c,"stepBigMonths"):-a.datepicker._get(c,"stepMonths"),"M");break;case 34:a.datepicker._adjustDate(b.target,b.ctrlKey?+a.datepicker._get(c,"stepBigMonths"):+a.datepicker._get(c,"stepMonths"),"M");break;case 35:(b.ctrlKey||b.metaKey)&&a.datepicker._clearDate(b.target);d=b.ctrlKey||b.metaKey;break;case 36:(b.ctrlKey||b.metaKey)&&a.datepicker._gotoToday(b.target);d=b.ctrlKey||b.metaKey;break;case 37:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,h?+1:-1,"D");d=b.ctrlKey||b.metaKey;b.originalEvent.altKey&&a.datepicker._adjustDate(b.target,b.ctrlKey?-a.datepicker._get(c,"stepBigMonths"):-a.datepicker._get(c,"stepMonths"),"M");break;case 38:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,-7,"D");d=b.ctrlKey||b.metaKey;break;case 39:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,h?-1:+1,"D");d=b.ctrlKey||b.metaKey;b.originalEvent.altKey&&a.datepicker._adjustDate(b.target,b.ctrlKey?+a.datepicker._get(c,"stepBigMonths"):+a.datepicker._get(c,"stepMonths"),"M");break;case 40:(b.ctrlKey||b.metaKey)&&a.datepicker._adjustDate(b.target,+7,"D");d=b.ctrlKey||b.metaKey;break;default:d=false}else if(b.keyCode===36&&b.ctrlKey)a.datepicker._showDatepicker(this);else d=false;if(d){b.preventDefault();b.stopPropagation()}},_doKeyPress:function(b){var c,d,e=a.datepicker._getInst(b.target);if(a.datepicker._get(e,"constrainInput")){c=a.datepicker._possibleChars(a.datepicker._get(e,"dateFormat"));d=String.fromCharCode(b.charCode==null?b.keyCode:b.charCode);return b.ctrlKey||b.metaKey||(d<" "||!c||c.indexOf(d)>-1)}},_doKeyUp:function(d){var c,b=a.datepicker._getInst(d.target);if(b.input.val()!==b.lastVal)try{c=a.datepicker.parseDate(a.datepicker._get(b,"dateFormat"),b.input?b.input.val():null,a.datepicker._getFormatConfig(b));if(c){a.datepicker._setDateFromField(b);a.datepicker._updateAlternate(b);a.datepicker._updateDatepicker(b)}}catch(e){}return true},_showDatepicker:function(c){c=c.target||c;if(c.nodeName.toLowerCase()!=="input")c=a("input",c.parentNode)[0];if(a.datepicker._isDisabledDatepicker(c)||a.datepicker._lastInput===c)return;var b,i,h,f,g,d,j;b=a.datepicker._getInst(c);if(a.datepicker._curInst&&a.datepicker._curInst!==b){a.datepicker._curInst.dpDiv.stop(true,true);b&&a.datepicker._datepickerShowing&&a.datepicker._hideDatepicker(a.datepicker._curInst.input[0])}i=a.datepicker._get(b,"beforeShow");h=i?i.apply(c,[c,b]):{};if(h===false)return;e(b.settings,h);b.lastVal=null;a.datepicker._lastInput=c;a.datepicker._setDateFromField(b);if(a.datepicker._inDialog)c.value="";if(!a.datepicker._pos){a.datepicker._pos=a.datepicker._findPos(c);a.datepicker._pos[1]+=c.offsetHeight}f=false;a(c).parents().each(function(){f|=a(this).css("position")==="fixed";return!f});g={left:a.datepicker._pos[0],top:a.datepicker._pos[1]};a.datepicker._pos=null;b.dpDiv.empty();b.dpDiv.css({position:"absolute",display:"block",top:"-1000px"});a.datepicker._updateDatepicker(b);g=a.datepicker._checkOffset(b,g,f);b.dpDiv.css({position:a.datepicker._inDialog&&a.blockUI?"static":f?"fixed":"absolute",display:"none",left:g.left+"px",top:g.top+"px"});if(!b.inline){d=a.datepicker._get(b,"showAnim");j=a.datepicker._get(b,"duration");b.dpDiv.zIndex(a(c).zIndex()+1);a.datepicker._datepickerShowing=true;if(a.effects&&a.effects.effect[d])b.dpDiv.show(d,a.datepicker._get(b,"showOptions"),j);else b.dpDiv[d||"show"](d?j:null);b.input.is(":visible")&&!b.input.is(":disabled")&&b.input.focus();a.datepicker._curInst=b}},_updateDatepicker:function(b){this.maxRows=4;f=b;b.dpDiv.empty().append(this._generateHTML(b));this._attachHandlers(b);b.dpDiv.find("."+this._dayOverClass+" a").mouseover();var c,d=this._getNumberOfMonths(b),e=d[1],g=17;b.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width("");e>1&&b.dpDiv.addClass("ui-datepicker-multi-"+e).css("width",g*e+"em");b.dpDiv[(d[0]!==1||d[1]!==1?"add":"remove")+"Class"]("ui-datepicker-multi");b.dpDiv[(this._get(b,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl");b===a.datepicker._curInst&&a.datepicker._datepickerShowing&&b.input&&b.input.is(":visible")&&!b.input.is(":disabled")&&b.input[0]!==document.activeElement&&b.input.focus();if(b.yearshtml){c=b.yearshtml;setTimeout(function(){c===b.yearshtml&&b.yearshtml&&b.dpDiv.find("select.ui-datepicker-year:first").replaceWith(b.yearshtml);c=b.yearshtml=null},0)}},_getBorders:function(b){var a=function(a){return({thin:1,medium:2,thick:3})[a]||a};return[parseFloat(a(b.css("border-left-width"))),parseFloat(a(b.css("border-top-width")))]},_checkOffset:function(c,b,e){var d=c.dpDiv.outerWidth(),g=c.dpDiv.outerHeight(),j=c.input?c.input.outerWidth():0,h=c.input?c.input.outerHeight():0,f=document.documentElement.clientWidth+(e?0:a(document).scrollLeft()),i=document.documentElement.clientHeight+(e?0:a(document).scrollTop());b.left-=this._get(c,"isRTL")?d-j:0;b.left-=e&&b.left===c.input.offset().left?a(document).scrollLeft():0;b.top-=e&&b.top===c.input.offset().top+h?a(document).scrollTop():0;b.left-=Math.min(b.left,b.left+d>f&&f>d?Math.abs(b.left+d-f):0);b.top-=Math.min(b.top,b.top+g>i&&i>g?Math.abs(g+h):0);return b},_findPos:function(b){var c,e=this._getInst(b),d=this._get(e,"isRTL");while(b&&(b.type==="hidden"||b.nodeType!==1||a.expr.filters.hidden(b)))b=b[d?"previousSibling":"nextSibling"];c=a(b).offset();return[c.left,c.top]},_hideDatepicker:function(h){var d,f,e,g,c=this._curInst;if(!c||h&&c!==a.data(h,b))return;if(this._datepickerShowing){d=this._get(c,"showAnim");f=this._get(c,"duration");e=function(){a.datepicker._tidyDialog(c)};if(a.effects&&(a.effects.effect[d]||a.effects[d]))c.dpDiv.hide(d,a.datepicker._get(c,"showOptions"),f,e);else c.dpDiv[d==="slideDown"?"slideUp":d==="fadeIn"?"fadeOut":"hide"](d?f:null,e);!d&&e();this._datepickerShowing=false;g=this._get(c,"onClose");g&&g.apply(c.input?c.input[0]:null,[c.input?c.input.val():"",c]);this._lastInput=null;if(this._inDialog){this._dialogInput.css({position:"absolute",left:"0",top:"-100px"});if(a.blockUI){a.unblockUI();a("body").append(this.dpDiv)}}this._inDialog=false}},_tidyDialog:function(a){a.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(c){if(!a.datepicker._curInst)return;var b=a(c.target),d=a.datepicker._getInst(b[0]);(b[0].id!==a.datepicker._mainDivId&&b.parents("#"+a.datepicker._mainDivId).length===0&&!b.hasClass(a.datepicker.markerClassName)&&!b.closest("."+a.datepicker._triggerClass).length&&a.datepicker._datepickerShowing&&!(a.datepicker._inDialog&&a.blockUI)||b.hasClass(a.datepicker.markerClassName)&&a.datepicker._curInst!==d)&&a.datepicker._hideDatepicker()},_adjustDate:function(f,e,c){var d=a(f),b=this._getInst(d[0]);if(this._isDisabledDatepicker(d[0]))return;this._adjustInstDate(b,e+(c==="M"?this._get(b,"showCurrentAtPos"):0),c);this._updateDatepicker(b)},_gotoToday:function(e){var c,d=a(e),b=this._getInst(d[0]);if(this._get(b,"gotoCurrent")&&b.currentDay){b.selectedDay=b.currentDay;b.drawMonth=b.selectedMonth=b.currentMonth;b.drawYear=b.selectedYear=b.currentYear}else{c=new Date;b.selectedDay=c.getDate();b.drawMonth=b.selectedMonth=c.getMonth();b.drawYear=b.selectedYear=c.getFullYear()}this._notifyChange(b);this._adjustDate(d)},_selectMonthYear:function(f,d,c){var e=a(f),b=this._getInst(e[0]);b["selected"+(c==="M"?"Month":"Year")]=b["draw"+(c==="M"?"Month":"Year")]=parseInt(d.options[d.selectedIndex].value,10);this._notifyChange(b);this._adjustDate(e)},_selectDay:function(d,f,g,e){var b,c=a(d);if(a(e).hasClass(this._unselectableClass)||this._isDisabledDatepicker(c[0]))return;b=this._getInst(c[0]);b.selectedDay=b.currentDay=a("a",e).html();b.selectedMonth=b.currentMonth=f;b.selectedYear=b.currentYear=g;this._selectDate(d,this._formatDate(b,b.currentDay,b.currentMonth,b.currentYear))},_clearDate:function(c){var b=a(c);this._selectDate(b,"")},_selectDate:function(f,c){var d,e=a(f),b=this._getInst(e[0]);c=c!=null?c:this._formatDate(b);b.input&&b.input.val(c);this._updateAlternate(b);d=this._get(b,"onSelect");if(d)d.apply(b.input?b.input[0]:null,[c,b]);else b.input&&b.input.trigger("change");if(b.inline)this._updateDatepicker(b);else{this._hideDatepicker();this._lastInput=b.input[0];typeof b.input[0]!=="object"&&b.input.focus();this._lastInput=null}},_updateAlternate:function(b){var c,f,e,d=this._get(b,"altField");if(d){c=this._get(b,"altFormat")||this._get(b,"dateFormat");f=this._getDate(b);e=this.formatDate(c,f,this._getFormatConfig(b));a(d).each(function(){a(this).val(e)})}},noWeekends:function(b){var a=b.getDay();return[a>0&&a<6,""]},iso8601Week:function(c){var b,a=new Date(c.getTime());a.setDate(a.getDate()+4-(a.getDay()||7));b=a.getTime();a.setMonth(0);a.setDate(1);return Math.floor(Math.round((b-a)/8.64e7)/7)+1},parseDate:function(j,d,f){if(j==null||d==null)throw"Invalid arguments";d=typeof d==="object"?d.toString():d+"";if(d==="")return null;for(var q,p,c=0,m=(f?f.shortYearCutoff:null)||this._defaults.shortYearCutoff,u=typeof m!=="string"?m:(new Date).getFullYear()%100+parseInt(m,10),v=(f?f.dayNamesShort:null)||this._defaults.dayNamesShort,x=(f?f.dayNames:null)||this._defaults.dayNames,t=(f?f.monthNamesShort:null)||this._defaults.monthNamesShort,w=(f?f.monthNames:null)||this._defaults.monthNames,e=-1,g=-1,i=-1,r=-1,o=false,b,l=function(b){var a=h+1<j.length&&j.charAt(h+1)===b;if(a)h++;return a},k=function(a){var e=l(a),g=a==="@"?14:a==="!"?20:a==="y"&&e?4:a==="o"?3:2,f=new RegExp("^\\d{1,"+g+"}"),b=d.substring(c).match(f);if(!b)throw"Missing number at position "+c;c+=b[0].length;return parseInt(b[0],10)},s=function(g,e,f){var b=-1,h=a.map(l(g)?f:e,function(b,a){return[[a,b]]}).sort(function(a,b){return-(a[1].length-b[1].length)});a.each(h,function(f,e){var a=e[1];if(d.substr(c,a.length).toLowerCase()===a.toLowerCase()){b=e[0];c+=a.length;return false}});if(b!==-1)return b+1;else throw"Unknown name at position "+c;},n=function(){if(d.charAt(c)!==j.charAt(h))throw"Unexpected literal at position "+c;c++},h=0;h<j.length;h++)if(o)if(j.charAt(h)==="'"&&!l("'"))o=false;else n();else switch(j.charAt(h)){case"d":i=k("d");break;case"D":s("D",v,x);break;case"o":r=k("o");break;case"m":g=k("m");break;case"M":g=s("M",t,w);break;case"y":e=k("y");break;case"@":b=new Date(k("@"));e=b.getFullYear();g=b.getMonth()+1;i=b.getDate();break;case"!":b=new Date((k("!")-this._ticksTo1970)/1e4);e=b.getFullYear();g=b.getMonth()+1;i=b.getDate();break;case"'":if(l("'"))n();else o=true;break;default:n()}if(c<d.length){p=d.substr(c);if(!/^\s+/.test(p))throw"Extra/unparsed characters found in date: "+p;}if(e===-1)e=(new Date).getFullYear();else if(e<100)e+=(new Date).getFullYear()-(new Date).getFullYear()%100+(e<=u?0:-100);if(r>-1){g=1;i=r;do{q=this._getDaysInMonth(e,g-1);if(i<=q)break;g++;i-=q}while(true)}b=this._daylightSavingAdjust(new Date(e,g-1,i));if(b.getFullYear()!==e||b.getMonth()+1!==g||b.getDate()!==i)throw"Invalid date";return b},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:((1970-1)*365+Math.floor(1970/4)-Math.floor(1970/100)+Math.floor(1970/400))*24*60*60*1e7,formatDate:function(e,a,d){if(!a)return"";var c,k=(d?d.dayNamesShort:null)||this._defaults.dayNamesShort,m=(d?d.dayNames:null)||this._defaults.dayNames,j=(d?d.monthNamesShort:null)||this._defaults.monthNamesShort,l=(d?d.monthNames:null)||this._defaults.monthNames,f=function(b){var a=c+1<e.length&&e.charAt(c+1)===b;if(a)c++;return a},g=function(b,c,d){var a=""+c;if(f(b))while(a.length<d)a="0"+a;return a},i=function(d,a,b,c){return f(d)?c[a]:b[a]},b="",h=false;if(a)for(c=0;c<e.length;c++)if(h)if(e.charAt(c)==="'"&&!f("'"))h=false;else b+=e.charAt(c);else switch(e.charAt(c)){case"d":b+=g("d",a.getDate(),2);break;case"D":b+=i("D",a.getDay(),k,m);break;case"o":b+=g("o",Math.round(((new Date(a.getFullYear(),a.getMonth(),a.getDate())).getTime()-(new Date(a.getFullYear(),0,0)).getTime())/8.64e7),3);break;case"m":b+=g("m",a.getMonth()+1,2);break;case"M":b+=i("M",a.getMonth(),j,l);break;case"y":b+=f("y")?a.getFullYear():(a.getYear()%100<10?"0":"")+a.getYear()%100;break;case"@":b+=a.getTime();break;case"!":b+=a.getTime()*1e4+this._ticksTo1970;break;case"'":if(f("'"))b+="'";else h=true;break;default:b+=e.charAt(c)}return b},_possibleChars:function(b){for(var c="",d=false,e=function(d){var c=a+1<b.length&&b.charAt(a+1)===d;if(c)a++;return c},a=0;a<b.length;a++)if(d)if(b.charAt(a)==="'"&&!e("'"))d=false;else c+=b.charAt(a);else switch(b.charAt(a)){case"d":case"m":case"y":case"@":c+="0123456789";break;case"D":case"M":return null;case"'":if(e("'"))c+="'";else d=true;break;default:c+=b.charAt(a)}return c},_get:function(b,a){return b.settings[a]!==d?b.settings[a]:this._defaults[a]},_setDateFromField:function(a,f){if(a.input.val()===a.lastVal)return;var e=this._get(a,"dateFormat"),c=a.lastVal=a.input?a.input.val():null,d=this._getDefaultDate(a),b=d,g=this._getFormatConfig(a);try{b=this.parseDate(e,c,g)||d}catch(h){c=f?"":c}a.selectedDay=b.getDate();a.drawMonth=a.selectedMonth=b.getMonth();a.drawYear=a.selectedYear=b.getFullYear();a.currentDay=c?b.getDate():0;a.currentMonth=c?b.getMonth():0;a.currentYear=c?b.getFullYear():0;this._adjustInstDate(a)},_getDefaultDate:function(a){return this._restrictMinMax(a,this._determineDate(a,this._get(a,"defaultDate"),new Date))},_determineDate:function(e,c,d){var f=function(b){var a=new Date;a.setDate(a.getDate()+b);return a},g=function(d){try{return a.datepicker.parseDate(a.datepicker._get(e,"dateFormat"),d,a.datepicker._getFormatConfig(e))}catch(j){}var h=(d.toLowerCase().match(/^c/)?a.datepicker._getDate(e):null)||new Date,g=h.getFullYear(),f=h.getMonth(),c=h.getDate(),i=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,b=i.exec(d);while(b){switch(b[2]||"d"){case"d":case"D":c+=parseInt(b[1],10);break;case"w":case"W":c+=parseInt(b[1],10)*7;break;case"m":case"M":f+=parseInt(b[1],10);c=Math.min(c,a.datepicker._getDaysInMonth(g,f));break;case"y":case"Y":g+=parseInt(b[1],10);c=Math.min(c,a.datepicker._getDaysInMonth(g,f))}b=i.exec(d)}return new Date(g,f,c)},b=c==null||c===""?d:typeof c==="string"?g(c):typeof c==="number"?isNaN(c)?d:f(c):new Date(c.getTime());b=b&&b.toString()==="Invalid Date"?d:b;if(b){b.setHours(0);b.setMinutes(0);b.setSeconds(0);b.setMilliseconds(0)}return this._daylightSavingAdjust(b)},_daylightSavingAdjust:function(a){if(!a)return null;a.setHours(a.getHours()>12?a.getHours()+2:0);return a},_setDate:function(a,c,e){var g=!c,d=a.selectedMonth,f=a.selectedYear,b=this._restrictMinMax(a,this._determineDate(a,c,new Date));a.selectedDay=a.currentDay=b.getDate();a.drawMonth=a.selectedMonth=a.currentMonth=b.getMonth();a.drawYear=a.selectedYear=a.currentYear=b.getFullYear();(d!==a.selectedMonth||f!==a.selectedYear)&&!e&&this._notifyChange(a);this._adjustInstDate(a);a.input&&a.input.val(g?"":this._formatDate(a))},_getDate:function(a){return!a.currentYear||a.input&&a.input.val()===""?null:this._daylightSavingAdjust(new Date(a.currentYear,a.currentMonth,a.currentDay))},_attachHandlers:function(d){var e=this._get(d,"stepMonths"),b="#"+d.id.replace(/\\\\/g,"\\");d.dpDiv.find("[data-handler]").map(function(){var d={prev:function(){window["DP_jQuery_"+c].datepicker._adjustDate(b,-e,"M")},next:function(){window["DP_jQuery_"+c].datepicker._adjustDate(b,+e,"M")},hide:function(){window["DP_jQuery_"+c].datepicker._hideDatepicker()},today:function(){window["DP_jQuery_"+c].datepicker._gotoToday(b)},selectDay:function(){window["DP_jQuery_"+c].datepicker._selectDay(b,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this);return false},selectMonth:function(){window["DP_jQuery_"+c].datepicker._selectMonthYear(b,this,"M");return false},selectYear:function(){window["DP_jQuery_"+c].datepicker._selectMonthYear(b,this,"Y");return false}};a(this).bind(this.getAttribute("data-event"),d[this.getAttribute("data-handler")])})},_generateHTML:function(a){var t,k,R,j,Q,r,G,F,W,i,I,ab,X,Y,V,z,u,T,E,x,h,q,L,p,B,l,e,N,S,D,H,w,K,c,P,M,s,m,v,J=new Date,O=this._daylightSavingAdjust(new Date(J.getFullYear(),J.getMonth(),J.getDate())),g=this._get(a,"isRTL"),cb=this._get(a,"showButtonPanel"),U=this._get(a,"hideIfNoPrevNext"),y=this._get(a,"navigationAsDateFormat"),f=this._getNumberOfMonths(a),bb=this._get(a,"showCurrentAtPos"),Z=this._get(a,"stepMonths"),A=f[0]!==1||f[1]!==1,C=this._daylightSavingAdjust(!a.currentDay?new Date(9999,9,9):new Date(a.currentYear,a.currentMonth,a.currentDay)),o=this._getMinMaxDate(a,"min"),n=this._getMinMaxDate(a,"max"),b=a.drawMonth-bb,d=a.drawYear;if(b<0){b+=12;d--}if(n){t=this._daylightSavingAdjust(new Date(n.getFullYear(),n.getMonth()-f[0]*f[1]+1,n.getDate()));t=o&&t<o?o:t;while(this._daylightSavingAdjust(new Date(d,b,1))>t){b--;if(b<0){b=11;d--}}}a.drawMonth=b;a.drawYear=d;k=this._get(a,"prevText");k=!y?k:this.formatDate(k,this._daylightSavingAdjust(new Date(d,b-Z,1)),this._getFormatConfig(a));R=this._canAdjustMonth(a,-1,d,b)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+k+"'><span class='ui-icon ui-icon-circle-triangle-"+(g?"e":"w")+"'>"+k+"</span></a>":U?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+k+"'><span class='ui-icon ui-icon-circle-triangle-"+(g?"e":"w")+"'>"+k+"</span></a>";j=this._get(a,"nextText");j=!y?j:this.formatDate(j,this._daylightSavingAdjust(new Date(d,b+Z,1)),this._getFormatConfig(a));Q=this._canAdjustMonth(a,+1,d,b)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+j+"'><span class='ui-icon ui-icon-circle-triangle-"+(g?"w":"e")+"'>"+j+"</span></a>":U?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+j+"'><span class='ui-icon ui-icon-circle-triangle-"+(g?"w":"e")+"'>"+j+"</span></a>";r=this._get(a,"currentText");G=this._get(a,"gotoCurrent")&&a.currentDay?C:O;r=!y?r:this.formatDate(r,G,this._getFormatConfig(a));F=!a.inline?"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(a,"closeText")+"</button>":"";W=cb?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(g?F:"")+(this._isInRange(a,G)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+r+"</button>":"")+(g?"":F)+"</div>":"";i=parseInt(this._get(a,"firstDay"),10);i=isNaN(i)?0:i;I=this._get(a,"showWeek");ab=this._get(a,"dayNames");X=this._get(a,"dayNamesMin");Y=this._get(a,"monthNames");V=this._get(a,"monthNamesShort");z=this._get(a,"beforeShowDay");u=this._get(a,"showOtherMonths");T=this._get(a,"selectOtherMonths");E=this._getDefaultDate(a);x="";h;for(q=0;q<f[0];q++){L="";this.maxRows=4;for(p=0;p<f[1];p++){B=this._daylightSavingAdjust(new Date(d,b,a.selectedDay));l=" ui-corner-all";e="";if(A){e+="<div class='ui-datepicker-group";if(f[1]>1)switch(p){case 0:e+=" ui-datepicker-group-first";l=" ui-corner-"+(g?"right":"left");break;case f[1]-1:e+=" ui-datepicker-group-last";l=" ui-corner-"+(g?"left":"right");break;default:e+=" ui-datepicker-group-middle";l=""}e+="'>"}e+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+l+"'>"+(/all|left/.test(l)&&q===0?g?Q:R:"")+(/all|right/.test(l)&&q===0?g?R:Q:"")+this._generateMonthYearHeader(a,b,d,o,n,q>0||p>0,Y,V)+"</div><table class='ui-datepicker-calendar'><thead><tr>";N=I?"<th class='ui-datepicker-week-col'>"+this._get(a,"weekHeader")+"</th>":"";for(h=0;h<7;h++){S=(h+i)%7;N+="<th"+((h+i+6)%7>=5?" class='ui-datepicker-week-end'":"")+"><span title='"+ab[S]+"'>"+X[S]+"</span></th>"}e+=N+"</tr></thead><tbody>";D=this._getDaysInMonth(d,b);if(d===a.selectedYear&&b===a.selectedMonth)a.selectedDay=Math.min(a.selectedDay,D);H=(this._getFirstDayOfMonth(d,b)-i+7)%7;w=Math.ceil((H+D)/7);K=A?this.maxRows>w?this.maxRows:w:w;this.maxRows=K;c=this._daylightSavingAdjust(new Date(d,b,1-H));for(P=0;P<K;P++){e+="<tr>";M=!I?"":"<td class='ui-datepicker-week-col'>"+this._get(a,"calculateWeek")(c)+"</td>";for(h=0;h<7;h++){s=z?z.apply(a.input?a.input[0]:null,[c]):[true,""];m=c.getMonth()!==b;v=m&&!T||!s[0]||o&&c<o||n&&c>n;M+="<td class='"+((h+i+6)%7>=5?" ui-datepicker-week-end":"")+(m?" ui-datepicker-other-month":"")+(c.getTime()===B.getTime()&&b===a.selectedMonth&&a._keyEvent||E.getTime()===c.getTime()&&E.getTime()===B.getTime()?" "+this._dayOverClass:"")+(v?" "+this._unselectableClass+" ui-state-disabled":"")+(m&&!u?"":" "+s[1]+(c.getTime()===C.getTime()?" "+this._currentClass:"")+(c.getTime()===O.getTime()?" ui-datepicker-today":""))+"'"+((!m||u)&&s[2]?" title='"+s[2].replace(/'/g,"&#39;")+"'":"")+(v?"":" data-handler='selectDay' data-event='click' data-month='"+c.getMonth()+"' data-year='"+c.getFullYear()+"'")+">"+(m&&!u?"&#xa0;":v?"<span class='ui-state-default'>"+c.getDate()+"</span>":"<a class='ui-state-default"+(c.getTime()===O.getTime()?" ui-state-highlight":"")+(c.getTime()===C.getTime()?" ui-state-active":"")+(m?" ui-priority-secondary":"")+"' href='#'>"+c.getDate()+"</a>")+"</td>";c.setDate(c.getDate()+1);c=this._daylightSavingAdjust(c)}e+=M+"</tr>"}b++;if(b>11){b=0;d++}e+="</tbody></table>"+(A?"</div>"+(f[0]>0&&p===f[1]-1?"<div class='ui-datepicker-row-break'></div>":""):"");L+=e}x+=L}x+=W;a._keyEvent=false;return x},_generateMonthYearHeader:function(a,q,f,i,h,j,u,t){var s,r,c,o,n,k,b,g,l=this._get(a,"changeMonth"),m=this._get(a,"changeYear"),p=this._get(a,"showMonthAfterYear"),d="<div class='ui-datepicker-title'>",e="";if(j||!l)e+="<span class='ui-datepicker-month'>"+u[q]+"</span>";else{s=i&&i.getFullYear()===f;r=h&&h.getFullYear()===f;e+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";for(c=0;c<12;c++)if((!s||c>=i.getMonth())&&(!r||c<=h.getMonth()))e+="<option value='"+c+"'"+(c===q?" selected='selected'":"")+">"+t[c]+"</option>";e+="</select>"}if(!p)d+=e+(j||!(l&&m)?"&#xa0;":"");if(!a.yearshtml){a.yearshtml="";if(j||!m)d+="<span class='ui-datepicker-year'>"+f+"</span>";else{o=this._get(a,"yearRange").split(":");n=(new Date).getFullYear();k=function(a){var b=a.match(/c[+\-].*/)?f+parseInt(a.substring(1),10):a.match(/[+\-].*/)?n+parseInt(a,10):parseInt(a,10);return isNaN(b)?n:b};b=k(o[0]);g=Math.max(b,k(o[1]||""));b=i?Math.max(b,i.getFullYear()):b;g=h?Math.min(g,h.getFullYear()):g;a.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";for(;b<=g;b++)a.yearshtml+="<option value='"+b+"'"+(b===f?" selected='selected'":"")+">"+b+"</option>";a.yearshtml+="</select>";d+=a.yearshtml;a.yearshtml=null}}d+=this._get(a,"yearSuffix");if(p)d+=(j||!(l&&m)?"&#xa0;":"")+e;d+="</div>";return d},_adjustInstDate:function(a,c,b){var f=a.drawYear+(b==="Y"?c:0),e=a.drawMonth+(b==="M"?c:0),g=Math.min(a.selectedDay,this._getDaysInMonth(f,e))+(b==="D"?c:0),d=this._restrictMinMax(a,this._daylightSavingAdjust(new Date(f,e,g)));a.selectedDay=d.getDate();a.drawMonth=a.selectedMonth=d.getMonth();a.drawYear=a.selectedYear=d.getFullYear();(b==="M"||b==="Y")&&this._notifyChange(a)},_restrictMinMax:function(e,d){var b=this._getMinMaxDate(e,"min"),a=this._getMinMaxDate(e,"max"),c=b&&d<b?b:d;return a&&c>a?a:c},_notifyChange:function(a){var b=this._get(a,"onChangeMonthYear");b&&b.apply(a.input?a.input[0]:null,[a.selectedYear,a.selectedMonth+1,a])},_getNumberOfMonths:function(b){var a=this._get(b,"numberOfMonths");return a==null?[1,1]:typeof a==="number"?[1,a]:a},_getMinMaxDate:function(a,b){return this._determineDate(a,this._get(a,b+"Date"),null)},_getDaysInMonth:function(b,a){return 32-this._daylightSavingAdjust(new Date(b,a,32)).getDate()},_getFirstDayOfMonth:function(b,a){return(new Date(b,a,1)).getDay()},_canAdjustMonth:function(d,b,f,e){var c=this._getNumberOfMonths(d),a=this._daylightSavingAdjust(new Date(f,e+(b<0?b:c[0]*c[1]),1));b<0&&a.setDate(this._getDaysInMonth(a.getFullYear(),a.getMonth()));return this._isInRange(d,a)},_isInRange:function(f,d){var a,e,h=this._getMinMaxDate(f,"min"),g=this._getMinMaxDate(f,"max"),c=null,b=null,i=this._get(f,"yearRange");if(i){a=i.split(":");e=(new Date).getFullYear();c=parseInt(a[0],10);b=parseInt(a[1],10);if(a[0].match(/[+\-].*/))c+=e;if(a[1].match(/[+\-].*/))b+=e}return(!h||d.getTime()>=h.getTime())&&(!g||d.getTime()<=g.getTime())&&(!c||d.getFullYear()>=c)&&(!b||d.getFullYear()<=b)},_getFormatConfig:function(b){var a=this._get(b,"shortYearCutoff");a=typeof a!=="string"?a:(new Date).getFullYear()%100+parseInt(a,10);return{shortYearCutoff:a,dayNamesShort:this._get(b,"dayNamesShort"),dayNames:this._get(b,"dayNames"),monthNamesShort:this._get(b,"monthNamesShort"),monthNames:this._get(b,"monthNames")}},_formatDate:function(a,b,c,e){if(!b){a.currentDay=a.selectedDay;a.currentMonth=a.selectedMonth;a.currentYear=a.selectedYear}var d=b?typeof b==="object"?b:this._daylightSavingAdjust(new Date(e,c,b)):this._daylightSavingAdjust(new Date(a.currentYear,a.currentMonth,a.currentDay));return this.formatDate(this._get(a,"dateFormat"),d,this._getFormatConfig(a))}});function h(c){var b="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return c.delegate(b,"mouseout",function(){a(this).removeClass("ui-state-hover");this.className.indexOf("ui-datepicker-prev")!==-1&&a(this).removeClass("ui-datepicker-prev-hover");this.className.indexOf("ui-datepicker-next")!==-1&&a(this).removeClass("ui-datepicker-next-hover")}).delegate(b,"mouseover",function(){if(!a.datepicker._isDisabledDatepicker(f.inline?c.parent()[0]:f.input[0])){a(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover");a(this).addClass("ui-state-hover");this.className.indexOf("ui-datepicker-prev")!==-1&&a(this).addClass("ui-datepicker-prev-hover");this.className.indexOf("ui-datepicker-next")!==-1&&a(this).addClass("ui-datepicker-next-hover")}})}function e(c,b){a.extend(c,b);for(var d in b)if(b[d]==null)c[d]=b[d];return c}a.fn.datepicker=function(b){if(!this.length)return this;if(!a.datepicker.initialized){a(document).mousedown(a.datepicker._checkExternalClick);a.datepicker.initialized=true}a("#"+a.datepicker._mainDivId).length===0&&a("body").append(a.datepicker.dpDiv);var c=Array.prototype.slice.call(arguments,1);return typeof b==="string"&&(b==="isDisabled"||b==="getDate"||b==="widget")?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this[0]].concat(c)):b==="option"&&arguments.length===2&&typeof arguments[1]==="string"?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this[0]].concat(c)):this.each(function(){typeof b==="string"?a.datepicker["_"+b+"Datepicker"].apply(a.datepicker,[this].concat(c)):a.datepicker._attachDatepicker(this,b)})};a.datepicker=new g;a.datepicker.initialized=false;a.datepicker.uuid=(new Date).getTime();a.datepicker.version="1.10.2";window["DP_jQuery_"+c]=a})(jQuery);(function(a){var c={buttons:true,height:true,maxHeight:true,maxWidth:true,minHeight:true,minWidth:true,width:true},b={maxHeight:true,maxWidth:true,minHeight:true,minWidth:true};a.widget("ui.dialog",{version:"1.10.2",options:{appendTo:"body",autoOpen:true,buttons:[],closeOnEscape:true,closeText:"close",dialogClass:"",draggable:true,hide:null,height:"auto",maxHeight:null,maxWidth:null,minHeight:150,minWidth:150,modal:false,position:{my:"center",at:"center",of:window,collision:"fit",using:function(c){var b=a(this).css(c).offset().top;b<0&&a(this).css("top",c.top-b)}},resizable:true,show:null,title:null,width:300,beforeClose:null,close:null,drag:null,dragStart:null,dragStop:null,focus:null,open:null,resize:null,resizeStart:null,resizeStop:null},_create:function(){this.originalCss={display:this.element[0].style.display,width:this.element[0].style.width,minHeight:this.element[0].style.minHeight,maxHeight:this.element[0].style.maxHeight,height:this.element[0].style.height};this.originalPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)};this.originalTitle=this.element.attr("title");this.options.title=this.options.title||this.originalTitle;this._createWrapper();this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog);this._createTitlebar();this._createButtonPane();this.options.draggable&&a.fn.draggable&&this._makeDraggable();this.options.resizable&&a.fn.resizable&&this._makeResizable();this._isOpen=false},_init:function(){this.options.autoOpen&&this.open()},_appendTo:function(){var b=this.options.appendTo;return b&&(b.jquery||b.nodeType)?a(b):this.document.find(b||"body").eq(0)},_destroy:function(){var a,b=this.originalPosition;this._destroyOverlay();this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach();this.uiDialog.stop(true,true).remove();this.originalTitle&&this.element.attr("title",this.originalTitle);a=b.parent.children().eq(b.index);if(a.length&&a[0]!==this.element[0])a.before(this.element);else b.parent.append(this.element)},widget:function(){return this.uiDialog},disable:a.noop,enable:a.noop,close:function(b){var c=this;if(!this._isOpen||this._trigger("beforeClose",b)===false)return;this._isOpen=false;this._destroyOverlay();!this.opener.filter(":focusable").focus().length&&a(this.document[0].activeElement).blur();this._hide(this.uiDialog,this.options.hide,function(){c._trigger("close",b)})},isOpen:function(){return this._isOpen},moveToTop:function(){this._moveToTop()},_moveToTop:function(c,b){var a=!!this.uiDialog.nextAll(":visible").insertBefore(this.uiDialog).length;a&&!b&&this._trigger("focus",c);return a},open:function(){var b=this;if(this._isOpen){this._moveToTop()&&this._focusTabbable();return}this._isOpen=true;this.opener=a(this.document[0].activeElement);this._size();this._position();this._createOverlay();this._moveToTop(null,true);this._show(this.uiDialog,this.options.show,function(){b._focusTabbable();b._trigger("focus")});this._trigger("open")},_focusTabbable:function(){var a=this.element.find("[autofocus]");if(!a.length)a=this.element.find(":tabbable");if(!a.length)a=this.uiDialogButtonPane.find(":tabbable");if(!a.length)a=this.uiDialogTitlebarClose.filter(":tabbable");if(!a.length)a=this.uiDialog;a.eq(0).focus()},_keepFocus:function(c){function b(){var b=this.document[0].activeElement,c=this.uiDialog[0]===b||a.contains(this.uiDialog[0],b);!c&&this._focusTabbable()}c.preventDefault();b.call(this);this._delay(b)},_createWrapper:function(){this.uiDialog=a("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front "+this.options.dialogClass).hide().attr({tabIndex:-1,role:"dialog"}).appendTo(this._appendTo());this._on(this.uiDialog,{keydown:function(b){if(this.options.closeOnEscape&&!b.isDefaultPrevented()&&b.keyCode&&b.keyCode===a.ui.keyCode.ESCAPE){b.preventDefault();this.close(b);return}if(b.keyCode!==a.ui.keyCode.TAB)return;var c=this.uiDialog.find(":tabbable"),d=c.filter(":first"),e=c.filter(":last");if((b.target===e[0]||b.target===this.uiDialog[0])&&!b.shiftKey){d.focus(1);b.preventDefault()}else if((b.target===d[0]||b.target===this.uiDialog[0])&&b.shiftKey){e.focus(1);b.preventDefault()}},mousedown:function(a){this._moveToTop(a)&&this._focusTabbable()}});!this.element.find("[aria-describedby]").length&&this.uiDialog.attr({"aria-describedby":this.element.uniqueId().attr("id")})},_createTitlebar:function(){var b;this.uiDialogTitlebar=a("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog);this._on(this.uiDialogTitlebar,{mousedown:function(b){!a(b.target).closest(".ui-dialog-titlebar-close")&&this.uiDialog.focus()}});this.uiDialogTitlebarClose=a("<button></button>").button({label:this.options.closeText,icons:{primary:"ui-icon-closethick"},text:false}).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar);this._on(this.uiDialogTitlebarClose,{click:function(a){a.preventDefault();this.close(a)}});b=a("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar);this._title(b);this.uiDialog.attr({"aria-labelledby":b.attr("id")})},_title:function(a){!this.options.title&&a.html("&#160;");a.text(this.options.title)},_createButtonPane:function(){this.uiDialogButtonPane=a("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix");this.uiButtonSet=a("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane);this._createButtons()},_createButtons:function(){var c=this,b=this.options.buttons;this.uiDialogButtonPane.remove();this.uiButtonSet.empty();if(a.isEmptyObject(b)||a.isArray(b)&&!b.length){this.uiDialog.removeClass("ui-dialog-buttons");return}a.each(b,function(f,b){var e,d;b=a.isFunction(b)?{click:b,text:f}:b;b=a.extend({type:"button"},b);e=b.click;b.click=function(){e.apply(c.element[0],arguments)};d={icons:b.icons,text:b.showText};delete b.icons;delete b.showText;a("<button></button>",b).button(d).appendTo(c.uiButtonSet)});this.uiDialog.addClass("ui-dialog-buttons");this.uiDialogButtonPane.appendTo(this.uiDialog)},_makeDraggable:function(){var b=this,d=this.options;function c(a){return{position:a.position,offset:a.offset}}this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(d,e){a(this).addClass("ui-dialog-dragging");b._blockFrames();b._trigger("dragStart",d,c(e))},drag:function(a,d){b._trigger("drag",a,c(d))},stop:function(f,e){d.position=[e.position.left-b.document.scrollLeft(),e.position.top-b.document.scrollTop()];a(this).removeClass("ui-dialog-dragging");b._unblockFrames();b._trigger("dragStop",f,c(e))}})},_makeResizable:function(){var c=this,b=this.options,e=b.resizable,g=this.uiDialog.css("position"),f=typeof e==="string"?e:"n,e,s,w,se,sw,ne,nw";function d(a){return{originalPosition:a.originalPosition,originalSize:a.originalSize,position:a.position,size:a.size}}this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:b.maxWidth,maxHeight:b.maxHeight,minWidth:b.minWidth,minHeight:this._minHeight(),handles:f,start:function(b,e){a(this).addClass("ui-dialog-resizing");c._blockFrames();c._trigger("resizeStart",b,d(e))},resize:function(a,b){c._trigger("resize",a,d(b))},stop:function(e,f){b.height=a(this).height();b.width=a(this).width();a(this).removeClass("ui-dialog-resizing");c._unblockFrames();c._trigger("resizeStop",e,d(f))}}).css("position",g)},_minHeight:function(){var a=this.options;return a.height==="auto"?a.minHeight:Math.min(a.minHeight,a.height)},_position:function(){var a=this.uiDialog.is(":visible");!a&&this.uiDialog.show();this.uiDialog.position(this.options.position);!a&&this.uiDialog.hide()},_setOptions:function(f){var g=this,e=false,d={};a.each(f,function(a,f){g._setOption(a,f);if(a in c)e=true;if(a in b)d[a]=f});if(e){this._size();this._position()}this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option",d)},_setOption:function(a,b){var e,d,c=this.uiDialog;a==="dialogClass"&&c.removeClass(this.options.dialogClass).addClass(b);if(a==="disabled")return;this._super(a,b);a==="appendTo"&&this.uiDialog.appendTo(this._appendTo());a==="buttons"&&this._createButtons();a==="closeText"&&this.uiDialogTitlebarClose.button({label:""+b});if(a==="draggable"){e=c.is(":data(ui-draggable)");e&&!b&&c.draggable("destroy");!e&&b&&this._makeDraggable()}a==="position"&&this._position();if(a==="resizable"){d=c.is(":data(ui-resizable)");d&&!b&&c.resizable("destroy");d&&typeof b==="string"&&c.resizable("option","handles",b);!d&&b!==false&&this._makeResizable()}a==="title"&&this._title(this.uiDialogTitlebar.find(".ui-dialog-title"))},_size:function(){var b,d,c,a=this.options;this.element.show().css({width:"auto",minHeight:0,maxHeight:"none",height:0});if(a.minWidth>a.width)a.width=a.minWidth;b=this.uiDialog.css({height:"auto",width:a.width}).outerHeight();d=Math.max(0,a.minHeight-b);c=typeof a.maxHeight==="number"?Math.max(0,a.maxHeight-b):"none";if(a.height==="auto")this.element.css({minHeight:d,maxHeight:c,height:"auto"});else this.element.height(Math.max(0,a.height-b));this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())},_blockFrames:function(){this.iframeBlocks=this.document.find("iframe").map(function(){var b=a(this);return a("<div>").css({position:"absolute",width:b.outerWidth(),height:b.outerHeight()}).appendTo(b.parent()).offset(b.offset())[0]})},_unblockFrames:function(){if(this.iframeBlocks){this.iframeBlocks.remove();delete this.iframeBlocks}},_allowInteraction:function(b){return a(b.target).closest(".ui-dialog").length?true:!!a(b.target).closest(".ui-datepicker").length},_createOverlay:function(){if(!this.options.modal)return;var c=this,b=this.widgetFullName;!a.ui.dialog.overlayInstances&&this._delay(function(){a.ui.dialog.overlayInstances&&this.document.bind("focusin.dialog",function(d){if(!c._allowInteraction(d)){d.preventDefault();a(".ui-dialog:visible:last .ui-dialog-content").data(b)._focusTabbable()}})});this.overlay=a("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo());this._on(this.overlay,{mousedown:"_keepFocus"});a.ui.dialog.overlayInstances++},_destroyOverlay:function(){if(!this.options.modal)return;if(this.overlay){a.ui.dialog.overlayInstances--;!a.ui.dialog.overlayInstances&&this.document.unbind("focusin.dialog");this.overlay.remove();this.overlay=null}}});a.ui.dialog.overlayInstances=0;a.uiBackCompat!==false&&a.widget("ui.dialog",a.ui.dialog,{_position:function(){var b=this.options.position,c=[],d=[0,0],e;if(b){if(typeof b==="string"||typeof b==="object"&&"0"in b){c=b.split?b.split(" "):[b[0],b[1]];if(c.length===1)c[1]=c[0];a.each(["left","top"],function(a,b){if(+c[a]===c[a]){d[a]=c[a];c[a]=b}});b={my:c[0]+(d[0]<0?d[0]:"+"+d[0])+" "+c[1]+(d[1]<0?d[1]:"+"+d[1]),at:c.join(" ")}}b=a.extend({},a.ui.dialog.prototype.options.position,b)}else b=a.ui.dialog.prototype.options.position;e=this.uiDialog.is(":visible");!e&&this.uiDialog.show();this.uiDialog.position(b);!e&&this.uiDialog.hide()}})})(jQuery);(function(a){var c=/up|down|vertical/,b=/up|left|vertical|horizontal/;a.effects.effect.blind=function(i,r){var d=a(this),k=["position","top","bottom","left","right","height","width"],q=a.effects.setMode(d,i.mode||"hide"),o=i.direction||"up",g=c.test(o),n=g?"height":"width",l=g?"top":"left",p=b.test(o),j={},m=q==="show",e,f,h;if(d.parent().is(".ui-effects-wrapper"))a.effects.save(d.parent(),k);else a.effects.save(d,k);d.show();e=a.effects.createWrapper(d).css({overflow:"hidden"});f=e[n]();h=parseFloat(e.css(l))||0;j[n]=m?f:0;if(!p){d.css(g?"bottom":"right",0).css(g?"top":"left","auto").css({position:"absolute"});j[l]=m?h:f+h}if(m){e.css(n,0);!p&&e.css(l,h+f)}e.animate(j,{duration:i.duration,easing:i.easing,queue:false,complete:function(){q==="hide"&&d.hide();a.effects.restore(d,k);a.effects.removeWrapper(d);r()}})}})(jQuery);(function(a){a.effects.effect.bounce=function(h,u){var b=a(this),n=["position","top","bottom","left","right","height","width"],t=a.effects.setMode(b,h.mode||"effect"),f=t==="hide",p=t==="show",i=h.direction||"up",c=h.distance,o=h.times||5,s=o*2+(p||f?1:0),l=h.duration/s,j=h.easing,g=i==="up"||i==="down"?"top":"left",m=i==="up"||i==="left",q,e,d,k=b.queue(),r=k.length;(p||f)&&n.push("opacity");a.effects.save(b,n);b.show();a.effects.createWrapper(b);if(!c)c=b[g==="top"?"outerHeight":"outerWidth"]()/3;if(p){d={opacity:1};d[g]=0;b.css("opacity",0).css(g,m?-c*2:c*2).animate(d,l,j)}if(f)c=c/Math.pow(2,o-1);d={};d[g]=0;for(q=0;q<o;q++){e={};e[g]=(m?"-=":"+=")+c;b.animate(e,l,j).animate(d,l,j);c=f?c*2:c/2}if(f){e={opacity:0};e[g]=(m?"-=":"+=")+c;b.animate(e,l,j)}b.queue(function(){f&&b.hide();a.effects.restore(b,n);a.effects.removeWrapper(b);u()});r>1&&k.splice.apply(k,[1,0].concat(k.splice(r,s+1)));b.dequeue()}})(jQuery);(function(a){a.effects.effect.clip=function(f,n){var b=a(this),k=["position","top","bottom","left","right","height","width"],o=a.effects.setMode(b,f.mode||"hide"),e=o==="show",m=f.direction||"vertical",l=m==="vertical",h=l?"height":"width",i=l?"top":"left",g={},j,c,d;a.effects.save(b,k);b.show();j=a.effects.createWrapper(b).css({overflow:"hidden"});c=b[0].tagName==="IMG"?j:b;d=c[h]();if(e){c.css(h,0);c.css(i,d/2)}g[h]=e?d:0;g[i]=e?0:d/2;c.animate(g,{queue:false,duration:f.duration,easing:f.easing,complete:function(){!e&&b.hide();a.effects.restore(b,k);a.effects.removeWrapper(b);n()}})}})(jQuery);(function(a){a.effects.effect.drop=function(c,l){var b=a(this),j=["position","top","bottom","left","right","opacity","height","width"],k=a.effects.setMode(b,c.mode||"hide"),g=k==="show",d=c.direction||"left",h=d==="up"||d==="down"?"top":"left",f=d==="up"||d==="left"?"pos":"neg",i={opacity:g?1:0},e;a.effects.save(b,j);b.show();a.effects.createWrapper(b);e=c.distance||b[h==="top"?"outerHeight":"outerWidth"](true)/2;g&&b.css("opacity",0).css(h,f==="pos"?-e:e);i[h]=(g?f==="pos"?"+=":"-=":f==="pos"?"-=":"+=")+e;b.animate(i,{queue:false,duration:c.duration,easing:c.easing,complete:function(){k==="hide"&&b.hide();a.effects.restore(b,j);a.effects.removeWrapper(b);l()}})}})(jQuery);(function(a){a.effects.effect.explode=function(i,s){var h=i.pieces?Math.round(Math.sqrt(i.pieces)):3,j=h,c=a(this),t=a.effects.setMode(c,i.mode||"hide"),b=t==="show",p=c.show().css("visibility","hidden").offset(),g=Math.ceil(c.outerWidth()/j),f=Math.ceil(c.outerHeight()/h),k=[],d,e,l,m,n,o;function q(){k.push(this);k.length===h*j&&r()}for(d=0;d<h;d++){m=p.top+d*f;o=d-(h-1)/2;for(e=0;e<j;e++){l=p.left+e*g;n=e-(j-1)/2;c.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-e*g,top:-d*f}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:g,height:f,left:l+(b?n*g:0),top:m+(b?o*f:0),opacity:b?0:1}).animate({left:l+(b?0:n*g),top:m+(b?0:o*f),opacity:b?1:0},i.duration||500,i.easing,q)}}function r(){c.css({visibility:"visible"});a(k).remove();!b&&c.hide();s()}}})(jQuery);(function(a){a.effects.effect.fade=function(b,d){var c=a(this),e=a.effects.setMode(c,b.mode||"toggle");c.animate({opacity:e},{queue:false,duration:b.duration,easing:b.easing,complete:d})}})(jQuery);(function(a){a.effects.effect.fold=function(d,r){var c=a(this),n=["position","top","bottom","left","right","height","width"],p=a.effects.setMode(c,d.mode||"hide"),g=p==="show",o=p==="hide",e=d.size||15,m=/([0-9]+)%/.exec(e),j=!!d.horizFirst,k=g!==j,q=k?["width","height"]:["height","width"],l=d.duration/2,b,f,h={},i={};a.effects.save(c,n);c.show();b=a.effects.createWrapper(c).css({overflow:"hidden"});f=k?[b.width(),b.height()]:[b.height(),b.width()];if(m)e=parseInt(m[1],10)/100*f[o?0:1];g&&b.css(j?{height:0,width:e}:{height:e,width:0});h[q[0]]=g?f[0]:e;i[q[1]]=g?f[1]:0;b.animate(h,l,d.easing).animate(i,l,d.easing,function(){o&&c.hide();a.effects.restore(c,n);a.effects.removeWrapper(c);r()})}})(jQuery);(function(a){a.effects.effect.highlight=function(c,g){var b=a(this),e=["backgroundImage","backgroundColor","opacity"],f=a.effects.setMode(b,c.mode||"show"),d={backgroundColor:b.css("backgroundColor")};if(f==="hide")d.opacity=0;a.effects.save(b,e);b.show().css({backgroundImage:"none",backgroundColor:c.color||"#ffff99"}).animate(d,{queue:false,duration:c.duration,easing:c.easing,complete:function(){f==="hide"&&b.hide();a.effects.restore(b,e);g()}})}})(jQuery);(function(a){a.effects.effect.pulsate=function(d,m){var b=a(this),g=a.effects.setMode(b,d.mode||"show"),k=g==="show",n=g==="hide",l=k||g==="hide",f=(d.times||5)*2+(l?1:0),i=d.duration/f,c=0,e=b.queue(),j=e.length,h;if(k||!b.is(":visible")){b.css("opacity",0).show();c=1}for(h=1;h<f;h++){b.animate({opacity:c},i,d.easing);c=1-c}b.animate({opacity:c},i,d.easing);b.queue(function(){n&&b.hide();m()});j>1&&e.splice.apply(e,[1,0].concat(e.splice(j,f+1)));b.dequeue()}})(jQuery);(function(a){a.effects.effect.puff=function(e,i){var b=a(this),h=a.effects.setMode(b,e.mode||"hide"),g=h==="hide",f=parseInt(e.percent,10)||150,d=f/100,c={height:b.height(),width:b.width(),outerHeight:b.outerHeight(),outerWidth:b.outerWidth()};a.extend(e,{effect:"scale",queue:false,fade:true,mode:h,complete:i,percent:g?f:100,from:g?c:{height:c.height*d,width:c.width*d,outerHeight:c.outerHeight*d,outerWidth:c.outerWidth*d}});b.effect(e)};a.effects.effect.scale=function(c,k){var d=a(this),b=a.extend(true,{},c),f=a.effects.setMode(d,c.mode||"effect"),i=parseInt(c.percent,10)||(parseInt(c.percent,10)===0?0:f==="hide"?0:100),h=c.direction||"both",j=c.origin,e={height:d.height(),width:d.width(),outerHeight:d.outerHeight(),outerWidth:d.outerWidth()},g={y:h!=="horizontal"?i/100:1,x:h!=="vertical"?i/100:1};b.effect="size";b.queue=false;b.complete=k;if(f!=="effect"){b.origin=j||["middle","center"];b.restore=true}b.from=c.from||(f==="show"?{height:0,width:0,outerHeight:0,outerWidth:0}:e);b.to={height:e.height*g.y,width:e.width*g.x,outerHeight:e.outerHeight*g.y,outerWidth:e.outerWidth*g.x};if(b.fade){if(f==="show"){b.from.opacity=0;b.to.opacity=1}if(f==="hide"){b.from.opacity=1;b.to.opacity=0}}d.effect(b)};a.effects.effect.size=function(e,t){var d,k,c,b=a(this),q=["position","top","bottom","left","right","width","height","overflow","opacity"],s=["position","top","bottom","left","right","overflow","opacity"],n=["width","height","overflow"],m=["fontSize"],g=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],f=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],j=a.effects.setMode(b,e.mode||"effect"),l=e.restore||j!=="effect",i=e.scale||"both",p=e.origin||["middle","center"],r=b.css("position"),h=l?q:s,o={height:0,width:0,outerHeight:0,outerWidth:0};j==="show"&&b.show();d={height:b.height(),width:b.width(),outerHeight:b.outerHeight(),outerWidth:b.outerWidth()};if(e.mode==="toggle"&&j==="show"){b.from=e.to||o;b.to=e.from||d}else{b.from=e.from||(j==="show"?o:d);b.to=e.to||(j==="hide"?o:d)}c={from:{y:b.from.height/d.height,x:b.from.width/d.width},to:{y:b.to.height/d.height,x:b.to.width/d.width}};if(i==="box"||i==="both"){if(c.from.y!==c.to.y){h=h.concat(g);b.from=a.effects.setTransition(b,g,c.from.y,b.from);b.to=a.effects.setTransition(b,g,c.to.y,b.to)}if(c.from.x!==c.to.x){h=h.concat(f);b.from=a.effects.setTransition(b,f,c.from.x,b.from);b.to=a.effects.setTransition(b,f,c.to.x,b.to)}}if(i==="content"||i==="both")if(c.from.y!==c.to.y){h=h.concat(m).concat(n);b.from=a.effects.setTransition(b,m,c.from.y,b.from);b.to=a.effects.setTransition(b,m,c.to.y,b.to)}a.effects.save(b,h);b.show();a.effects.createWrapper(b);b.css("overflow","hidden").css(b.from);if(p){k=a.effects.getBaseline(p,d);b.from.top=(d.outerHeight-b.outerHeight())*k.y;b.from.left=(d.outerWidth-b.outerWidth())*k.x;b.to.top=(d.outerHeight-b.to.outerHeight)*k.y;b.to.left=(d.outerWidth-b.to.outerWidth)*k.x}b.css(b.from);if(i==="content"||i==="both"){g=g.concat(["marginTop","marginBottom"]).concat(m);f=f.concat(["marginLeft","marginRight"]);n=q.concat(g).concat(f);b.find("*[width]").each(function(){var b=a(this),d={height:b.height(),width:b.width(),outerHeight:b.outerHeight(),outerWidth:b.outerWidth()};l&&a.effects.save(b,n);b.from={height:d.height*c.from.y,width:d.width*c.from.x,outerHeight:d.outerHeight*c.from.y,outerWidth:d.outerWidth*c.from.x};b.to={height:d.height*c.to.y,width:d.width*c.to.x,outerHeight:d.height*c.to.y,outerWidth:d.width*c.to.x};if(c.from.y!==c.to.y){b.from=a.effects.setTransition(b,g,c.from.y,b.from);b.to=a.effects.setTransition(b,g,c.to.y,b.to)}if(c.from.x!==c.to.x){b.from=a.effects.setTransition(b,f,c.from.x,b.from);b.to=a.effects.setTransition(b,f,c.to.x,b.to)}b.css(b.from);b.animate(b.to,e.duration,e.easing,function(){l&&a.effects.restore(b,n)})})}b.animate(b.to,{queue:false,duration:e.duration,easing:e.easing,complete:function(){b.to.opacity===0&&b.css("opacity",b.from.opacity);j==="hide"&&b.hide();a.effects.restore(b,h);if(!l)if(r==="static")b.css({position:"relative",top:b.to.top,left:b.to.left});else a.each(["top","left"],function(a,c){b.css(c,function(f,d){var e=parseInt(d,10),c=a?b.to.left:b.to.top;return d==="auto"?c+"px":e+c+"px"})});a.effects.removeWrapper(b);t()}})}})(jQuery);(function(a){a.effects.effect.shake=function(c,r){var b=a(this),p=["position","top","bottom","left","right","height","width"],s=a.effects.setMode(b,c.mode||"effect"),e=c.direction||"left",j=c.distance||20,q=c.times||3,o=q*2+1,d=Math.round(c.duration/o),k=e==="up"||e==="down"?"top":"left",g=e==="up"||e==="left",i={},h={},m={},l,f=b.queue(),n=f.length;a.effects.save(b,p);b.show();a.effects.createWrapper(b);i[k]=(g?"-=":"+=")+j;h[k]=(g?"+=":"-=")+j*2;m[k]=(g?"-=":"+=")+j*2;b.animate(i,d,c.easing);for(l=1;l<q;l++)b.animate(h,d,c.easing).animate(m,d,c.easing);b.animate(h,d,c.easing).animate(i,d/2,c.easing).queue(function(){s==="hide"&&b.hide();a.effects.restore(b,p);a.effects.removeWrapper(b);r()});n>1&&f.splice.apply(f,[1,0].concat(f.splice(n,o+1)));b.dequeue()}})(jQuery);(function(a){a.effects.effect.slide=function(d,l){var b=a(this),i=["position","top","bottom","left","right","width","height"],j=a.effects.setMode(b,d.mode||"show"),k=j==="show",e=d.direction||"left",g=e==="up"||e==="down"?"top":"left",f=e==="up"||e==="left",c,h={};a.effects.save(b,i);b.show();c=d.distance||b[g==="top"?"outerHeight":"outerWidth"](true);a.effects.createWrapper(b).css({overflow:"hidden"});k&&b.css(g,f?isNaN(c)?"-"+c:-c:c);h[g]=(k?f?"+=":"-=":f?"-=":"+=")+c;b.animate(h,{queue:false,duration:d.duration,easing:d.easing,complete:function(){j==="hide"&&b.hide();a.effects.restore(b,i);a.effects.removeWrapper(b);l()}})}})(jQuery);(function(a){a.effects.effect.transfer=function(c,m){var e=a(this),b=a(c.to),d=b.css("position")==="fixed",j=a("body"),i=d?j.scrollTop():0,h=d?j.scrollLeft():0,g=b.offset(),k={top:g.top-i,left:g.left-h,height:b.innerHeight(),width:b.innerWidth()},f=e.offset(),l=a("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(c.className).css({top:f.top-i,left:f.left-h,height:e.innerHeight(),width:e.innerWidth(),position:d?"fixed":"absolute"}).animate(k,c.duration,c.easing,function(){l.remove();m()})}})(jQuery);(function(a){a.widget("ui.menu",{version:"1.10.2",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element;this.mouseHandled=false;this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}).bind("click"+this.eventNamespace,a.proxy(function(a){this.options.disabled&&a.preventDefault()},this));this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true");this._on({"mousedown .ui-menu-item > a":function(a){a.preventDefault()},"click .ui-state-disabled > a":function(a){a.preventDefault()},"click .ui-menu-item:has(a)":function(b){var c=a(b.target).closest(".ui-menu-item");if(!this.mouseHandled&&c.not(".ui-state-disabled").length){this.mouseHandled=true;this.select(b);if(c.has(".ui-menu").length)this.expand(b);else if(!this.element.is(":focus")){this.element.trigger("focus",[true]);this.active&&this.active.parents(".ui-menu").length===1&&clearTimeout(this.timer)}}},"mouseenter .ui-menu-item":function(c){var b=a(c.currentTarget);b.siblings().children(".ui-state-active").removeClass("ui-state-active");this.focus(c,b)},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(b,a){var c=this.active||this.element.children(".ui-menu-item").eq(0);!a&&this.focus(b,c)},blur:function(b){this._delay(function(){!a.contains(this.element[0],this.document[0].activeElement)&&this.collapseAll(b)})},keydown:"_keydown"});this.refresh();this._on(this.document,{click:function(b){!a(b.target).closest(".ui-menu").length&&this.collapseAll(b);this.mouseHandled=false}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show();this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var b=a(this);b.data("ui-menu-submenu-carat")&&b.remove()});this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(b){var c,f,d,g,e,h=true;function i(a){return a.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}switch(b.keyCode){case a.ui.keyCode.PAGE_UP:this.previousPage(b);break;case a.ui.keyCode.PAGE_DOWN:this.nextPage(b);break;case a.ui.keyCode.HOME:this._move("first","first",b);break;case a.ui.keyCode.END:this._move("last","last",b);break;case a.ui.keyCode.UP:this.previous(b);break;case a.ui.keyCode.DOWN:this.next(b);break;case a.ui.keyCode.LEFT:this.collapse(b);break;case a.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(b);break;case a.ui.keyCode.ENTER:case a.ui.keyCode.SPACE:this._activate(b);break;case a.ui.keyCode.ESCAPE:this.collapse(b);break;default:h=false;f=this.previousFilter||"";d=String.fromCharCode(b.keyCode);g=false;clearTimeout(this.filterTimer);if(d===f)g=true;else d=f+d;e=new RegExp("^"+i(d),"i");c=this.activeMenu.children(".ui-menu-item").filter(function(){return e.test(a(this).children("a").text())});c=g&&c.index(this.active.next())!==-1?this.active.nextAll(".ui-menu-item"):c;if(!c.length){d=String.fromCharCode(b.keyCode);e=new RegExp("^"+i(d),"i");c=this.activeMenu.children(".ui-menu-item").filter(function(){return e.test(a(this).children("a").text())})}if(c.length){this.focus(b,c);if(c.length>1){this.previousFilter=d;this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)}else delete this.previousFilter}else delete this.previousFilter}h&&b.preventDefault()},_activate:function(a){if(!this.active.is(".ui-state-disabled"))if(this.active.children("a[aria-haspopup='true']").length)this.expand(a);else this.select(a)},refresh:function(){var b,d=this.options.icons.submenu,c=this.element.find(this.options.menus);c.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var c=a(this),b=c.prev("a"),e=a("<span>").addClass("ui-menu-icon ui-icon "+d).data("ui-menu-submenu-carat",true);b.attr("aria-haspopup","true").prepend(e);c.attr("aria-labelledby",b.attr("id"))});b=c.add(this.element);b.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role","presentation").children("a").uniqueId().addClass("ui-corner-all").attr({tabIndex:-1,role:this._itemRole()});b.children(":not(.ui-menu-item)").each(function(){var b=a(this);!/[^\-\u2014\u2013\s]/.test(b.text())&&b.addClass("ui-widget-content ui-menu-divider")});b.children(".ui-state-disabled").attr("aria-disabled","true");this.active&&!a.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return({menu:"menuitem",listbox:"option"})[this.options.role]},_setOption:function(b,a){b==="icons"&&this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(a.submenu);this._super(b,a)},focus:function(a,b){var c,d;this.blur(a,a&&a.type==="focus");this._scrollIntoView(b);this.active=b.first();d=this.active.children("a").addClass("ui-state-focus");this.options.role&&this.element.attr("aria-activedescendant",d.attr("id"));this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active");if(a&&a.type==="keydown")this._close();else this.timer=this._delay(function(){this._close()},this.delay);c=b.children(".ui-menu");c.length&&/^mouse/.test(a.type)&&this._startOpening(c);this.activeMenu=b.parent();this._trigger("focus",a,{item:b})},_scrollIntoView:function(h){var g,f,b,e,c,d;if(this._hasScroll()){g=parseFloat(a.css(this.activeMenu[0],"borderTopWidth"))||0;f=parseFloat(a.css(this.activeMenu[0],"paddingTop"))||0;b=h.offset().top-this.activeMenu.offset().top-g-f;e=this.activeMenu.scrollTop();c=this.activeMenu.height();d=h.height();if(b<0)this.activeMenu.scrollTop(e+b);else b+d>c&&this.activeMenu.scrollTop(e+b-c+d)}},blur:function(b,a){!a&&clearTimeout(this.timer);if(!this.active)return;this.active.children("a").removeClass("ui-state-focus");this.active=null;this._trigger("blur",b,{item:this.active})},_startOpening:function(a){clearTimeout(this.timer);if(a.attr("aria-hidden")!=="true")return;this.timer=this._delay(function(){this._close();this._open(a)},this.delay)},_open:function(b){var c=a.extend({of:this.active},this.options.position);clearTimeout(this.timer);this.element.find(".ui-menu").not(b.parents(".ui-menu")).hide().attr("aria-hidden","true");b.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(c)},collapseAll:function(b,c){clearTimeout(this.timer);this.timer=this._delay(function(){var d=c?this.element:a(b&&b.target).closest(this.element.find(".ui-menu"));if(!d.length)d=this.element;this._close(d);this.blur(b);this.activeMenu=d},this.delay)},_close:function(a){if(!a)a=this.active?this.active.parent():this.element;a.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find("a.ui-state-active").removeClass("ui-state-active")},collapse:function(b){var a=this.active&&this.active.parent().closest(".ui-menu-item",this.element);if(a&&a.length){this._close();this.focus(b,a)}},expand:function(b){var a=this.active&&this.active.children(".ui-menu ").children(".ui-menu-item").first();if(a&&a.length){this._open(a.parent());this._delay(function(){this.focus(b,a)})}},next:function(a){this._move("next","first",a)},previous:function(a){this._move("prev","last",a)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(b,c,d){var a;if(this.active)if(b==="first"||b==="last")a=this.active[b==="first"?"prevAll":"nextAll"](".ui-menu-item").eq(-1);else a=this.active[b+"All"](".ui-menu-item").eq(0);if(!a||!a.length||!this.active)a=this.activeMenu.children(".ui-menu-item")[c]();this.focus(d,a)},nextPage:function(b){var c,e,d;if(!this.active){this.next(b);return}if(this.isLastItem())return;if(this._hasScroll()){e=this.active.offset().top;d=this.element.height();this.active.nextAll(".ui-menu-item").each(function(){c=a(this);return c.offset().top-e-d<0});this.focus(b,c)}else this.focus(b,this.activeMenu.children(".ui-menu-item")[!this.active?"first":"last"]())},previousPage:function(b){var c,e,d;if(!this.active){this.next(b);return}if(this.isFirstItem())return;if(this._hasScroll()){e=this.active.offset().top;d=this.element.height();this.active.prevAll(".ui-menu-item").each(function(){c=a(this);return c.offset().top-e+d>0});this.focus(b,c)}else this.focus(b,this.activeMenu.children(".ui-menu-item").first())},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(b){this.active=this.active||a(b.target).closest(".ui-menu-item");var c={item:this.active};!this.active.has(".ui-menu").length&&this.collapseAll(b,true);this._trigger("select",b,c)}})})(jQuery);(function(a,n){a.ui=a.ui||{};var e,d=Math.max,b=Math.abs,l=Math.round,f=/left|center|right/,i=/top|center|bottom/,k=/[\+\-]\d+(\.[\d]+)?%?/,h=/^\w+/,j=/%$/,o=a.fn.position;function g(a,c,b){return[parseFloat(a[0])*(j.test(a[0])?c/100:1),parseFloat(a[1])*(j.test(a[1])?b/100:1)]}function c(c,b){return parseInt(a.css(c,b),10)||0}function m(b){var c=b[0];return c.nodeType===9?{width:b.width(),height:b.height(),offset:{top:0,left:0}}:a.isWindow(c)?{width:b.width(),height:b.height(),offset:{top:b.scrollTop(),left:b.scrollLeft()}}:c.preventDefault?{width:0,height:0,offset:{top:c.pageY,left:c.pageX}}:{width:b.outerWidth(),height:b.outerHeight(),offset:b.offset()}}a.position={scrollbarWidth:function(){if(e!==n)return e;var d,c,b=a("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),f=b.children()[0];a("body").append(b);d=f.offsetWidth;b.css("overflow","scroll");c=f.offsetWidth;if(d===c)c=b[0].clientWidth;b.remove();return e=d-c},getScrollInfo:function(b){var c=b.isWindow?"":b.element.css("overflow-x"),d=b.isWindow?"":b.element.css("overflow-y"),e=c==="scroll"||c==="auto"&&b.width<b.element[0].scrollWidth,f=d==="scroll"||d==="auto"&&b.height<b.element[0].scrollHeight;return{width:f?a.position.scrollbarWidth():0,height:e?a.position.scrollbarWidth():0}},getWithinInfo:function(d){var b=a(d||window),c=a.isWindow(b[0]);return{element:b,isWindow:c,offset:b.offset()||{left:0,top:0},scrollLeft:b.scrollLeft(),scrollTop:b.scrollTop(),width:c?b.width():b.outerWidth(),height:c?b.height():b.outerHeight()}}};a.fn.position=function(e){if(!e||!e.of)return o.apply(this,arguments);e=a.extend({},e);var s,n,j,q,p,t,v=a(e.of),x=a.position.getWithinInfo(e.within),w=a.position.getScrollInfo(x),r=(e.collision||"flip").split(" "),u={};t=m(v);if(v[0].preventDefault)e.at="left top";n=t.width;j=t.height;q=t.offset;p=a.extend({},q);a.each(["my","at"],function(){var a=(e[this]||"").split(" "),b,c;if(a.length===1)a=f.test(a[0])?a.concat(["center"]):i.test(a[0])?["center"].concat(a):["center","center"];a[0]=f.test(a[0])?a[0]:"center";a[1]=i.test(a[1])?a[1]:"center";b=k.exec(a[0]);c=k.exec(a[1]);u[this]=[b?b[0]:0,c?c[0]:0];e[this]=[h.exec(a[0])[0],h.exec(a[1])[0]]});if(r.length===1)r[1]=r[0];if(e.at[0]==="right")p.left+=n;else if(e.at[0]==="center")p.left+=n/2;if(e.at[1]==="bottom")p.top+=j;else if(e.at[1]==="center")p.top+=j/2;s=g(u.at,n,j);p.left+=s[0];p.top+=s[1];return this.each(function(){var o,z,k=a(this),i=k.outerWidth(),h=k.outerHeight(),t=c(this,"marginLeft"),y=c(this,"marginTop"),B=i+t+c(this,"marginRight")+w.width,A=h+y+c(this,"marginBottom")+w.height,f=a.extend({},p),m=g(u.my,k.outerWidth(),k.outerHeight());if(e.my[0]==="right")f.left-=i;else if(e.my[0]==="center")f.left-=i/2;if(e.my[1]==="bottom")f.top-=h;else if(e.my[1]==="center")f.top-=h/2;f.left+=m[0];f.top+=m[1];if(!a.support.offsetFractions){f.left=l(f.left);f.top=l(f.top)}o={marginLeft:t,marginTop:y};a.each(["left","top"],function(b,c){a.ui.position[r[b]]&&a.ui.position[r[b]][c](f,{targetWidth:n,targetHeight:j,elemWidth:i,elemHeight:h,collisionPosition:o,collisionWidth:B,collisionHeight:A,offset:[s[0]+m[0],s[1]+m[1]],my:e.my,at:e.at,within:x,elem:k})});if(e.using)z=function(o){var c=q.left-f.left,m=c+n-i,g=q.top-f.top,l=g+j-h,a={target:{element:v,left:q.left,top:q.top,width:n,height:j},element:{element:k,left:f.left,top:f.top,width:i,height:h},horizontal:m<0?"left":c>0?"right":"center",vertical:l<0?"top":g>0?"bottom":"middle"};if(n<i&&b(c+m)<n)a.horizontal="center";if(j<h&&b(g+l)<j)a.vertical="middle";if(d(b(c),b(m))>d(b(g),b(l)))a.important="horizontal";else a.important="vertical";e.using.call(this,o,a)};k.offset(a.extend(f,{using:z}))})};a.ui.position={fit:{left:function(a,e){var h=e.within,c=h.isWindow?h.scrollLeft:h.offset.left,g=h.width,i=a.left-e.collisionPosition.marginLeft,b=c-i,f=i+e.collisionWidth-g-c,j;if(e.collisionWidth>g)if(b>0&&f<=0){j=a.left+b+e.collisionWidth-g-c;a.left+=b-j}else if(f>0&&b<=0)a.left=c;else if(b>f)a.left=c+g-e.collisionWidth;else a.left=c;else if(b>0)a.left+=b;else if(f>0)a.left-=f;else a.left=d(a.left-i,a.left)},top:function(a,c){var i=c.within,e=i.isWindow?i.scrollTop:i.offset.top,g=c.within.height,h=a.top-c.collisionPosition.marginTop,b=e-h,f=h+c.collisionHeight-g-e,j;if(c.collisionHeight>g)if(b>0&&f<=0){j=a.top+b+c.collisionHeight-g-e;a.top+=b-j}else if(f>0&&b<=0)a.top=e;else if(b>f)a.top=e+g-c.collisionHeight;else a.top=e;else if(b>0)a.top+=b;else if(f>0)a.top-=f;else a.top=d(a.top-h,a.top)}},flip:{left:function(d,a){var c=a.within,o=c.offset.left+c.scrollLeft,l=c.width,j=c.isWindow?c.scrollLeft:c.offset.left,k=d.left-a.collisionPosition.marginLeft,n=k-j,m=k+a.collisionWidth-l-j,f=a.my[0]==="left"?-a.elemWidth:a.my[0]==="right"?a.elemWidth:0,e=a.at[0]==="left"?a.targetWidth:a.at[0]==="right"?-a.targetWidth:0,g=-2*a.offset[0],h,i;if(n<0){h=d.left+f+e+g+a.collisionWidth-l-o;if(h<0||h<b(n))d.left+=f+e+g}else if(m>0){i=d.left-a.collisionPosition.marginLeft+f+e+g-j;if(i>0||b(i)<m)d.left+=f+e+g}},top:function(c,a){var g=a.within,o=g.offset.top+g.scrollTop,n=g.height,k=g.isWindow?g.scrollTop:g.offset.top,m=c.top-a.collisionPosition.marginTop,l=m-k,j=m+a.collisionHeight-n-k,p=a.my[1]==="top",e=p?-a.elemHeight:a.my[1]==="bottom"?a.elemHeight:0,d=a.at[1]==="top"?a.targetHeight:a.at[1]==="bottom"?-a.targetHeight:0,f=-2*a.offset[1],i,h;if(l<0){h=c.top+e+d+f+a.collisionHeight-n-o;if(c.top+e+d+f>l&&(h<0||h<b(l)))c.top+=e+d+f}else if(j>0){i=c.top-a.collisionPosition.marginTop+e+d+f-k;if(c.top+e+d+f>j&&(i>0||b(i)<j))c.top+=e+d+f}}},flipfit:{left:function(){a.ui.position.flip.left.apply(this,arguments);a.ui.position.fit.left.apply(this,arguments)},top:function(){a.ui.position.flip.top.apply(this,arguments);a.ui.position.fit.top.apply(this,arguments)}}};(function(){var b,c,d,e,h,f=document.getElementsByTagName("body")[0],g=document.createElement("div");b=document.createElement(f?"div":"body");d={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"};f&&a.extend(d,{position:"absolute",left:"-1000px",top:"-1000px"});for(h in d)b.style[h]=d[h];b.appendChild(g);c=f||document.documentElement;c.insertBefore(b,c.firstChild);g.style.cssText="position: absolute; left: 10.7432222px;";e=a(g).offset().left;a.support.offsetFractions=e>10&&e<11;b.innerHTML="";c.removeChild(b)})()})(jQuery);(function(a,b){a.widget("ui.progressbar",{version:"1.10.2",options:{max:100,value:0,change:null,complete:null},min:0,_create:function(){this.oldValue=this.options.value=this._constrainedValue();this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min});this.valueDiv=a("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element);this._refreshValue()},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");this.valueDiv.remove()},value:function(a){if(a===b)return this.options.value;this.options.value=this._constrainedValue(a);this._refreshValue()},_constrainedValue:function(a){if(a===b)a=this.options.value;this.indeterminate=a===false;if(typeof a!=="number")a=0;return this.indeterminate?false:Math.min(this.options.max,Math.max(this.min,a))},_setOptions:function(a){var b=a.value;delete a.value;this._super(a);this.options.value=this._constrainedValue(b);this._refreshValue()},_setOption:function(b,a){if(b==="max")a=Math.max(this.min,a);this._super(b,a)},_percentage:function(){return this.indeterminate?100:100*(this.options.value-this.min)/(this.options.max-this.min)},_refreshValue:function(){var b=this.options.value,c=this._percentage();this.valueDiv.toggle(this.indeterminate||b>this.min).toggleClass("ui-corner-right",b===this.options.max).width(c.toFixed(0)+"%");this.element.toggleClass("ui-progressbar-indeterminate",this.indeterminate);if(this.indeterminate){this.element.removeAttr("aria-valuenow");if(!this.overlayDiv)this.overlayDiv=a("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv)}else{this.element.attr({"aria-valuemax":this.options.max,"aria-valuenow":b});if(this.overlayDiv){this.overlayDiv.remove();this.overlayDiv=null}}if(this.oldValue!==b){this.oldValue=b;this._trigger("change")}b===this.options.max&&this._trigger("complete")}})})(jQuery);(function(a){var b=5;a.widget("ui.slider",a.ui.mouse,{version:"1.10.2",widgetEventPrefix:"slide",options:{animate:false,distance:0,max:100,min:0,orientation:"horizontal",range:false,step:1,value:0,values:null,change:null,slide:null,start:null,stop:null},_create:function(){this._keySliding=false;this._mouseSliding=false;this._animateOff=true;this._handleIndex=null;this._detectOrientation();this._mouseInit();this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget ui-widget-content ui-corner-all");this._refresh();this._setOption("disabled",this.options.disabled);this._animateOff=false},_refresh:function(){this._createRange();this._createHandles();this._setupEvents();this._refreshValue()},_createHandles:function(){var d,c,f=this.options,b=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),g="<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",e=[];c=f.values&&f.values.length||1;if(b.length>c){b.slice(c).remove();b=b.slice(0,c)}for(d=b.length;d<c;d++)e.push(g);this.handles=b.add(a(e.join("")).appendTo(this.element));this.handle=this.handles.eq(0);this.handles.each(function(b){a(this).data("ui-slider-handle-index",b)})},_createRange:function(){var b=this.options,c="";if(b.range){if(b.range===true)if(!b.values)b.values=[this._valueMin(),this._valueMin()];else if(b.values.length&&b.values.length!==2)b.values=[b.values[0],b.values[0]];else if(a.isArray(b.values))b.values=b.values.slice(0);if(!this.range||!this.range.length){this.range=a("<div></div>").appendTo(this.element);c="ui-slider-range ui-widget-header ui-corner-all"}else this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({left:"",bottom:""});this.range.addClass(c+(b.range==="min"||b.range==="max"?" ui-slider-range-"+b.range:""))}else this.range=a([])},_setupEvents:function(){var a=this.handles.add(this.range).filter("a");this._off(a);this._on(a,this._handleEvents);this._hoverable(a);this._focusable(a)},_destroy:function(){this.handles.remove();this.range.remove();this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all");this._mouseDestroy()},_mouseCapture:function(c){var j,f,d,b,e,k,g,i,h=this,l=this.options;if(l.disabled)return false;this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()};this.elementOffset=this.element.offset();j={x:c.pageX,y:c.pageY};f=this._normValueFromMouse(j);d=this._valueMax()-this._valueMin()+1;this.handles.each(function(c){var g=Math.abs(f-h.values(c));if(d>g||d===g&&(c===h._lastChangedValue||h.values(c)===l.min)){d=g;b=a(this);e=c}});k=this._start(c,e);if(k===false)return false;this._mouseSliding=true;this._handleIndex=e;b.addClass("ui-state-active").focus();g=b.offset();i=!a(c.target).parents().addBack().is(".ui-slider-handle");this._clickOffset=i?{left:0,top:0}:{left:c.pageX-g.left-b.width()/2,top:c.pageY-g.top-b.height()/2-(parseInt(b.css("borderTopWidth"),10)||0)-(parseInt(b.css("borderBottomWidth"),10)||0)+(parseInt(b.css("marginTop"),10)||0)};!this.handles.hasClass("ui-state-hover")&&this._slide(c,e,f);this._animateOff=true;return true},_mouseStart:function(){return true},_mouseDrag:function(a){var c={x:a.pageX,y:a.pageY},b=this._normValueFromMouse(c);this._slide(a,this._handleIndex,b);return false},_mouseStop:function(a){this.handles.removeClass("ui-state-active");this._mouseSliding=false;this._stop(a,this._handleIndex);this._change(a,this._handleIndex);this._handleIndex=null;this._clickOffset=null;this._animateOff=false;return false},_detectOrientation:function(){this.orientation=this.options.orientation==="vertical"?"vertical":"horizontal"},_normValueFromMouse:function(f){var c,b,a,e,d;if(this.orientation==="horizontal"){c=this.elementSize.width;b=f.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)}else{c=this.elementSize.height;b=f.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)}a=b/c;if(a>1)a=1;if(a<0)a=0;if(this.orientation==="vertical")a=1-a;e=this._valueMax()-this._valueMin();d=this._valueMin()+a*e;return this._trimAlignValue(d)},_start:function(c,b){var a={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){a.value=this.values(b);a.values=this.values()}return this._trigger("start",c,a)},_slide:function(f,b,a){var c,e,d;if(this.options.values&&this.options.values.length){c=this.values(b?0:1);if(this.options.values.length===2&&this.options.range===true&&(b===0&&a>c||b===1&&a<c))a=c;if(a!==this.values(b)){e=this.values();e[b]=a;d=this._trigger("slide",f,{handle:this.handles[b],value:a,values:e});c=this.values(b?0:1);d!==false&&this.values(b,a,true)}}else if(a!==this.value()){d=this._trigger("slide",f,{handle:this.handles[b],value:a});d!==false&&this.value(a)}},_stop:function(c,b){var a={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){a.value=this.values(b);a.values=this.values()}this._trigger("stop",c,a)},_change:function(c,b){if(!this._keySliding&&!this._mouseSliding){var a={handle:this.handles[b],value:this.value()};if(this.options.values&&this.options.values.length){a.value=this.values(b);a.values=this.values()}this._lastChangedValue=b;this._trigger("change",c,a)}},value:function(a){if(arguments.length){this.options.value=this._trimAlignValue(a);this._refreshValue();this._change(null,0);return}return this._value()},values:function(c,f){var d,e,b;if(arguments.length>1){this.options.values[c]=this._trimAlignValue(f);this._refreshValue();this._change(null,c);return}if(arguments.length)if(a.isArray(arguments[0])){d=this.options.values;e=arguments[0];for(b=0;b<d.length;b+=1){d[b]=this._trimAlignValue(e[b]);this._change(null,b)}this._refreshValue()}else return this.options.values&&this.options.values.length?this._values(c):this.value();else return this._values()},_setOption:function(e,d){var b,c=0;if(e==="range"&&this.options.range===true)if(d==="min"){this.options.value=this._values(0);this.options.values=null}else if(d==="max"){this.options.value=this._values(this.options.values.length-1);this.options.values=null}if(a.isArray(this.options.values))c=this.options.values.length;a.Widget.prototype._setOption.apply(this,arguments);switch(e){case"orientation":this._detectOrientation();this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation);this._refreshValue();break;case"value":this._animateOff=true;this._refreshValue();this._change(null,0);this._animateOff=false;break;case"values":this._animateOff=true;this._refreshValue();for(b=0;b<c;b+=1)this._change(null,b);this._animateOff=false;break;case"min":case"max":this._animateOff=true;this._refreshValue();this._animateOff=false;break;case"range":this._animateOff=true;this._refresh();this._animateOff=false}},_value:function(){var a=this.options.value;a=this._trimAlignValue(a);return a},_values:function(d){var c,a,b;if(arguments.length){c=this.options.values[d];c=this._trimAlignValue(c);return c}else if(this.options.values&&this.options.values.length){a=this.options.values.slice();for(b=0;b<a.length;b+=1)a[b]=this._trimAlignValue(a[b]);return a}else return[]},_trimAlignValue:function(b){if(b<=this._valueMin())return this._valueMin();if(b>=this._valueMax())return this._valueMax();var a=this.options.step>0?this.options.step:1,c=(b-this._valueMin())%a,d=b-c;if(Math.abs(c)*2>=a)d+=c>0?a:-a;return parseFloat(d.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var i,b,k,f,j,g=this.options.range,d=this.options,c=this,e=!this._animateOff?d.animate:false,h={};if(this.options.values&&this.options.values.length)this.handles.each(function(f){b=(c.values(f)-c._valueMin())/(c._valueMax()-c._valueMin())*100;h[c.orientation==="horizontal"?"left":"bottom"]=b+"%";a(this).stop(1,1)[e?"animate":"css"](h,d.animate);if(c.options.range===true)if(c.orientation==="horizontal"){f===0&&c.range.stop(1,1)[e?"animate":"css"]({left:b+"%"},d.animate);f===1&&c.range[e?"animate":"css"]({width:b-i+"%"},{queue:false,duration:d.animate})}else{f===0&&c.range.stop(1,1)[e?"animate":"css"]({bottom:b+"%"},d.animate);f===1&&c.range[e?"animate":"css"]({height:b-i+"%"},{queue:false,duration:d.animate})}i=b});else{k=this.value();f=this._valueMin();j=this._valueMax();b=j!==f?(k-f)/(j-f)*100:0;h[this.orientation==="horizontal"?"left":"bottom"]=b+"%";this.handle.stop(1,1)[e?"animate":"css"](h,d.animate);g==="min"&&this.orientation==="horizontal"&&this.range.stop(1,1)[e?"animate":"css"]({width:b+"%"},d.animate);g==="max"&&this.orientation==="horizontal"&&this.range[e?"animate":"css"]({width:100-b+"%"},{queue:false,duration:d.animate});g==="min"&&this.orientation==="vertical"&&this.range.stop(1,1)[e?"animate":"css"]({height:b+"%"},d.animate);g==="max"&&this.orientation==="vertical"&&this.range[e?"animate":"css"]({height:100-b+"%"},{queue:false,duration:d.animate})}},_handleEvents:{keydown:function(e){var h,d,c,g,f=a(e.target).data("ui-slider-handle-index");switch(e.keyCode){case a.ui.keyCode.HOME:case a.ui.keyCode.END:case a.ui.keyCode.PAGE_UP:case a.ui.keyCode.PAGE_DOWN:case a.ui.keyCode.UP:case a.ui.keyCode.RIGHT:case a.ui.keyCode.DOWN:case a.ui.keyCode.LEFT:e.preventDefault();if(!this._keySliding){this._keySliding=true;a(e.target).addClass("ui-state-active");h=this._start(e,f);if(h===false)return}}g=this.options.step;if(this.options.values&&this.options.values.length)d=c=this.values(f);else d=c=this.value();switch(e.keyCode){case a.ui.keyCode.HOME:c=this._valueMin();break;case a.ui.keyCode.END:c=this._valueMax();break;case a.ui.keyCode.PAGE_UP:c=this._trimAlignValue(d+(this._valueMax()-this._valueMin())/b);break;case a.ui.keyCode.PAGE_DOWN:c=this._trimAlignValue(d-(this._valueMax()-this._valueMin())/b);break;case a.ui.keyCode.UP:case a.ui.keyCode.RIGHT:if(d===this._valueMax())return;c=this._trimAlignValue(d+g);break;case a.ui.keyCode.DOWN:case a.ui.keyCode.LEFT:if(d===this._valueMin())return;c=this._trimAlignValue(d-g)}this._slide(e,f,c)},click:function(a){a.preventDefault()},keyup:function(b){var c=a(b.target).data("ui-slider-handle-index");if(this._keySliding){this._keySliding=false;this._stop(b,c);this._change(b,c);a(b.target).removeClass("ui-state-active")}}}})})(jQuery);(function(a){function b(a){return function(){var b=this.element.val();a.apply(this,arguments);this._refresh();b!==this.element.val()&&this._trigger("change")}}a.widget("ui.spinner",{version:"1.10.2",defaultElement:"<input>",widgetEventPrefix:"spin",options:{culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:true,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max);this._setOption("min",this.options.min);this._setOption("step",this.options.step);this._value(this.element.val(),true);this._draw();this._on(this._events);this._refresh();this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_getCreateOptions:function(){var b={},c=this.element;a.each(["min","max","step"],function(e,d){var a=c.attr(d);if(a!==undefined&&a.length)b[d]=a});return b},_events:{keydown:function(a){this._start(a)&&this._keydown(a)&&a.preventDefault()},keyup:"_stop",focus:function(){this.previous=this.element.val()},blur:function(a){if(this.cancelBlur){delete this.cancelBlur;return}this._stop();this._refresh();this.previous!==this.element.val()&&this._trigger("change",a)},mousewheel:function(a,b){if(!b)return;if(!this.spinning&&!this._start(a))return false;this._spin((b>0?1:-1)*this.options.step,a);clearTimeout(this.mousewheelTimer);this.mousewheelTimer=this._delay(function(){this.spinning&&this._stop(a)},100);a.preventDefault()},"mousedown .ui-spinner-button":function(b){var c;c=this.element[0]===this.document[0].activeElement?this.previous:this.element.val();function d(){var a=this.element[0]===this.document[0].activeElement;if(!a){this.element.focus();this.previous=c;this._delay(function(){this.previous=c})}}b.preventDefault();d.call(this);this.cancelBlur=true;this._delay(function(){delete this.cancelBlur;d.call(this)});if(this._start(b)===false)return;this._repeat(null,a(b.currentTarget).hasClass("ui-spinner-up")?1:-1,b)},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(b){if(!a(b.currentTarget).hasClass("ui-state-active"))return;if(this._start(b)===false)return false;this._repeat(null,a(b.currentTarget).hasClass("ui-spinner-up")?1:-1,b)},"mouseleave .ui-spinner-button":"_stop"},_draw:function(){var a=this.uiSpinner=this.element.addClass("ui-spinner-input").attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());this.element.attr("role","spinbutton");this.buttons=a.find(".ui-spinner-button").attr("tabIndex",-1).button().removeClass("ui-corner-all");this.buttons.height()>Math.ceil(a.height()*.5)&&a.height()>0&&a.height(a.height());this.options.disabled&&this.disable()},_keydown:function(b){var d=this.options,c=a.ui.keyCode;switch(b.keyCode){case c.UP:this._repeat(null,1,b);return true;case c.DOWN:this._repeat(null,-1,b);return true;case c.PAGE_UP:this._repeat(null,d.page,b);return true;case c.PAGE_DOWN:this._repeat(null,-d.page,b);return true}return false},_uiSpinnerHtml:function(){return"<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"},_buttonHtml:function(){return"<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon "+this.options.icons.up+"'>&#9650;</span></a><a class='ui-spinner-button ui-spinner-down ui-corner-br'><span class='ui-icon "+this.options.icons.down+"'>&#9660;</span></a>"},_start:function(a){if(!this.spinning&&this._trigger("start",a)===false)return false;if(!this.counter)this.counter=1;this.spinning=true;return true},_repeat:function(a,c,b){a=a||500;clearTimeout(this.timer);this.timer=this._delay(function(){this._repeat(40,c,b)},a);this._spin(c*this.options.step,b)},_spin:function(c,b){var a=this.value()||0;if(!this.counter)this.counter=1;a=this._adjustValue(a+c*this._increment(this.counter));if(!this.spinning||this._trigger("spin",b,{value:a})!==false){this._value(a);this.counter++}},_increment:function(b){var c=this.options.incremental;return c?a.isFunction(c)?c(b):Math.floor(b*b*b/5e4-b*b/500+17*b/200+1):1},_precision:function(){var a=this._precisionOf(this.options.step);if(this.options.min!==null)a=Math.max(a,this._precisionOf(this.options.min));return a},_precisionOf:function(c){var b=c.toString(),a=b.indexOf(".");return a===-1?0:b.length-a-1},_adjustValue:function(b){var d,c,a=this.options;d=a.min!==null?a.min:0;c=b-d;c=Math.round(c/a.step)*a.step;b=d+c;b=parseFloat(b.toFixed(this._precision()));return a.max!==null&&b>a.max?a.max:a.min!==null&&b<a.min?a.min:b},_stop:function(a){if(!this.spinning)return;clearTimeout(this.timer);clearTimeout(this.mousewheelTimer);this.counter=0;this.spinning=false;this._trigger("stop",a)},_setOption:function(a,b){if(a==="culture"||a==="numberFormat"){var c=this._parse(this.element.val());this.options[a]=b;this.element.val(this._format(c));return}if(a==="max"||a==="min"||a==="step")if(typeof b==="string")b=this._parse(b);if(a==="icons"){this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(b.up);this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(b.down)}this._super(a,b);if(a==="disabled")if(b){this.element.prop("disabled",true);this.buttons.button("disable")}else{this.element.prop("disabled",false);this.buttons.button("enable")}},_setOptions:b(function(a){this._super(a);this._value(this.element.val())}),_parse:function(a){if(typeof a==="string"&&a!=="")a=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(a,10,this.options.culture):+a;return a===""||isNaN(a)?null:a},_format:function(a){return a===""?"":window.Globalize&&this.options.numberFormat?Globalize.format(a,this.options.numberFormat,this.options.culture):a},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())})},_value:function(b,c){var a;if(b!==""){a=this._parse(b);if(a!==null){if(!c)a=this._adjustValue(a);b=this._format(a)}}this.element.val(b);this._refresh()},_destroy:function(){this.element.removeClass("ui-spinner-input").prop("disabled",false).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow");this.uiSpinner.replaceWith(this.element)},stepUp:b(function(a){this._stepUp(a)}),_stepUp:function(a){if(this._start()){this._spin((a||1)*this.options.step);this._stop()}},stepDown:b(function(a){this._stepDown(a)}),_stepDown:function(a){if(this._start()){this._spin((a||1)*-this.options.step);this._stop()}},pageUp:b(function(a){this._stepUp((a||1)*this.options.page)}),pageDown:b(function(a){this._stepDown((a||1)*this.options.page)}),value:function(a){if(!arguments.length)return this._parse(this.element.val());b(this._value).call(this,a)},widget:function(){return this.uiSpinner}})})(jQuery);(function(a,b){var f=0,d=/#.*$/;function e(){return++f}function c(a){return a.hash.length>1&&decodeURIComponent(a.href.replace(d,""))===decodeURIComponent(location.href.replace(d,""))}a.widget("ui.tabs",{version:"1.10.2",delay:300,options:{active:null,collapsible:false,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_create:function(){var c=this,b=this.options;this.running=false;this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible",b.collapsible).delegate(".ui-tabs-nav > li","mousedown"+this.eventNamespace,function(b){a(this).is(".ui-state-disabled")&&b.preventDefault()}).delegate(".ui-tabs-anchor","focus"+this.eventNamespace,function(){a(this).closest("li").is(".ui-state-disabled")&&this.blur()});this._processTabs();b.active=this._initialActive();if(a.isArray(b.disabled))b.disabled=a.unique(b.disabled.concat(a.map(this.tabs.filter(".ui-state-disabled"),function(a){return c.tabs.index(a)}))).sort();if(this.options.active!==false&&this.anchors.length)this.active=this._findActive(b.active);else this.active=a();this._refresh();this.active.length&&this.load(b.active)},_initialActive:function(){var b=this.options.active,d=this.options.collapsible,c=location.hash.substring(1);if(b===null){c&&this.tabs.each(function(e,d){if(a(d).attr("aria-controls")===c){b=e;return false}});if(b===null)b=this.tabs.index(this.tabs.filter(".ui-tabs-active"));if(b===null||b===-1)b=this.tabs.length?0:false}if(b!==false){b=this.tabs.index(this.tabs.eq(b));if(b===-1)b=d?false:0}if(!d&&b===false&&this.anchors.length)b=0;return b},_getCreateEventData:function(){return{tab:this.active,panel:!this.active.length?a():this._getPanelForTab(this.active)}},_tabKeydown:function(c){var e=a(this.document[0].activeElement).closest("li"),b=this.tabs.index(e),d=true;if(this._handlePageNav(c))return;switch(c.keyCode){case a.ui.keyCode.RIGHT:case a.ui.keyCode.DOWN:b++;break;case a.ui.keyCode.UP:case a.ui.keyCode.LEFT:d=false;b--;break;case a.ui.keyCode.END:b=this.anchors.length-1;break;case a.ui.keyCode.HOME:b=0;break;case a.ui.keyCode.SPACE:c.preventDefault();clearTimeout(this.activating);this._activate(b);return;case a.ui.keyCode.ENTER:c.preventDefault();clearTimeout(this.activating);this._activate(b===this.options.active?false:b);return;default:return}c.preventDefault();clearTimeout(this.activating);b=this._focusNextTab(b,d);if(!c.ctrlKey){e.attr("aria-selected","false");this.tabs.eq(b).attr("aria-selected","true");this.activating=this._delay(function(){this.option("active",b)},this.delay)}},_panelKeydown:function(b){if(this._handlePageNav(b))return;if(b.ctrlKey&&b.keyCode===a.ui.keyCode.UP){b.preventDefault();this.active.focus()}},_handlePageNav:function(b){if(b.altKey&&b.keyCode===a.ui.keyCode.PAGE_UP){this._activate(this._focusNextTab(this.options.active-1,false));return true}if(b.altKey&&b.keyCode===a.ui.keyCode.PAGE_DOWN){this._activate(this._focusNextTab(this.options.active+1,true));return true}},_findNextTab:function(b,d){var c=this.tabs.length-1;function e(){if(b>c)b=0;if(b<0)b=c;return b}while(a.inArray(e(),this.options.disabled)!==-1)b=d?b+1:b-1;return b},_focusNextTab:function(a,b){a=this._findNextTab(a,b);this.tabs.eq(a).focus();return a},_setOption:function(b,a){if(b==="active"){this._activate(a);return}if(b==="disabled"){this._setupDisabled(a);return}this._super(b,a);if(b==="collapsible"){this.element.toggleClass("ui-tabs-collapsible",a);!a&&this.options.active===false&&this._activate(0)}b==="event"&&this._setupEvents(a);b==="heightStyle"&&this._setupHeightStyle(a)},_tabId:function(a){return a.attr("aria-controls")||"ui-tabs-"+e()},_sanitizeSelector:function(a){return a?a.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):""},refresh:function(){var b=this.options,c=this.tablist.children(":has(a[href])");b.disabled=a.map(c.filter(".ui-state-disabled"),function(a){return c.index(a)});this._processTabs();if(b.active===false||!this.anchors.length){b.active=false;this.active=a()}else if(this.active.length&&!a.contains(this.tablist[0],this.active[0]))if(this.tabs.length===b.disabled.length){b.active=false;this.active=a()}else this._activate(this._findNextTab(Math.max(0,b.active-1),false));else b.active=this.tabs.index(this.active);this._refresh()},_refresh:function(){this._setupDisabled(this.options.disabled);this._setupEvents(this.options.event);this._setupHeightStyle(this.options.heightStyle);this.tabs.not(this.active).attr({"aria-selected":"false",tabIndex:-1});this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-expanded":"false","aria-hidden":"true"});if(!this.active.length)this.tabs.eq(0).attr("tabIndex",0);else{this.active.addClass("ui-tabs-active ui-state-active").attr({"aria-selected":"true",tabIndex:0});this._getPanelForTab(this.active).show().attr({"aria-expanded":"true","aria-hidden":"false"})}},_processTabs:function(){var b=this;this.tablist=this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role","tablist");this.tabs=this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({role:"tab",tabIndex:-1});this.anchors=this.tabs.map(function(){return a("a",this)[0]}).addClass("ui-tabs-anchor").attr({role:"presentation",tabIndex:-1});this.panels=a();this.anchors.each(function(k,f){var e,d,h,j=a(f).uniqueId().attr("id"),g=a(f).closest("li"),i=g.attr("aria-controls");if(c(f)){e=f.hash;d=b.element.find(b._sanitizeSelector(e))}else{h=b._tabId(g);e="#"+h;d=b.element.find(e);if(!d.length){d=b._createPanel(h);d.insertAfter(b.panels[k-1]||b.tablist)}d.attr("aria-live","polite")}if(d.length)b.panels=b.panels.add(d);i&&g.data("ui-tabs-aria-controls",i);g.attr({"aria-controls":e.substring(1),"aria-labelledby":j});d.attr("aria-labelledby",j)});this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role","tabpanel")},_getList:function(){return this.element.find("ol,ul").eq(0)},_createPanel:function(b){return a("<div>").attr("id",b).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",true)},_setupDisabled:function(b){if(a.isArray(b))if(!b.length)b=false;else if(b.length===this.anchors.length)b=true;for(var d=0,c;c=this.tabs[d];d++)if(b===true||a.inArray(d,b)!==-1)a(c).addClass("ui-state-disabled").attr("aria-disabled","true");else a(c).removeClass("ui-state-disabled").removeAttr("aria-disabled");this.options.disabled=b},_setupEvents:function(c){var b={click:function(a){a.preventDefault()}};c&&a.each(c.split(" "),function(c,a){b[a]="_eventHandler"});this._off(this.anchors.add(this.tabs).add(this.panels));this._on(this.anchors,b);this._on(this.tabs,{keydown:"_tabKeydown"});this._on(this.panels,{keydown:"_panelKeydown"});this._focusable(this.tabs);this._hoverable(this.tabs)},_setupHeightStyle:function(c){var b,d=this.element.parent();if(c==="fill"){b=d.height();b-=this.element.outerHeight()-this.element.height();this.element.siblings(":visible").each(function(){var d=a(this),c=d.css("position");if(c==="absolute"||c==="fixed")return;b-=d.outerHeight(true)});this.element.children().not(this.panels).each(function(){b-=a(this).outerHeight(true)});this.panels.each(function(){a(this).height(Math.max(0,b-a(this).innerHeight()+a(this).height()))}).css("overflow","auto")}else if(c==="auto"){b=0;this.panels.each(function(){b=Math.max(b,a(this).height("").height())}).height(b)}},_eventHandler:function(c){var g=this.options,d=this.active,k=a(c.currentTarget),b=k.closest("li"),e=b[0]===d[0],f=e&&g.collapsible,h=f?a():this._getPanelForTab(b),j=!d.length?a():this._getPanelForTab(d),i={oldTab:d,oldPanel:j,newTab:f?a():b,newPanel:h};c.preventDefault();if(b.hasClass("ui-state-disabled")||b.hasClass("ui-tabs-loading")||this.running||e&&!g.collapsible||this._trigger("beforeActivate",c,i)===false)return;g.active=f?false:this.tabs.index(b);this.active=e?a():b;this.xhr&&this.xhr.abort();!j.length&&!h.length&&a.error("jQuery UI Tabs: Mismatching fragment identifier.");h.length&&this.load(this.tabs.index(b),c);this._toggle(c,i)},_toggle:function(h,b){var e=this,c=b.newPanel,d=b.oldPanel;this.running=true;function f(){e.running=false;e._trigger("activate",h,b)}function g(){b.newTab.closest("li").addClass("ui-tabs-active ui-state-active");if(c.length&&e.options.show)e._show(c,e.options.show,f);else{c.show();f()}}if(d.length&&this.options.hide)this._hide(d,this.options.hide,function(){b.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");g()});else{b.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active");d.hide();g()}d.attr({"aria-expanded":"false","aria-hidden":"true"});b.oldTab.attr("aria-selected","false");if(c.length&&d.length)b.oldTab.attr("tabIndex",-1);else c.length&&this.tabs.filter(function(){return a(this).attr("tabIndex")===0}).attr("tabIndex",-1);c.attr({"aria-expanded":"true","aria-hidden":"false"});b.newTab.attr({"aria-selected":"true",tabIndex:0})},_activate:function(d){var c,b=this._findActive(d);if(b[0]===this.active[0])return;if(!b.length)b=this.active;c=b.find(".ui-tabs-anchor")[0];this._eventHandler({target:c,currentTarget:c,preventDefault:a.noop})},_findActive:function(b){return b===false?a():this.tabs.eq(b)},_getIndex:function(a){if(typeof a==="string")a=this.anchors.index(this.anchors.filter("[href$='"+a+"']"));return a},_destroy:function(){this.xhr&&this.xhr.abort();this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible");this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role");this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId();this.tabs.add(this.panels).each(function(){if(a.data(this,"ui-tabs-destroy"))a(this).remove();else a(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")});this.tabs.each(function(){var b=a(this),c=b.data("ui-tabs-aria-controls");if(c)b.attr("aria-controls",c).removeData("ui-tabs-aria-controls");else b.removeAttr("aria-controls")});this.panels.show();this.options.heightStyle!=="content"&&this.panels.css("height","")},enable:function(d){var c=this.options.disabled;if(c===false)return;if(d===b)c=false;else{d=this._getIndex(d);if(a.isArray(c))c=a.map(c,function(a){return a!==d?a:null});else c=a.map(this.tabs,function(b,a){return a!==d?a:null})}this._setupDisabled(c)},disable:function(d){var c=this.options.disabled;if(c===true)return;if(d===b)c=true;else{d=this._getIndex(d);if(a.inArray(d,c)!==-1)return;if(a.isArray(c))c=a.merge([d],c).sort();else c=[d]}this._setupDisabled(c)},load:function(f,i){f=this._getIndex(f);var e=this,b=this.tabs.eq(f),h=b.find(".ui-tabs-anchor"),d=this._getPanelForTab(b),g={tab:b,panel:d};if(c(h[0]))return;this.xhr=a.ajax(this._ajaxSettings(h,i,g));if(this.xhr&&this.xhr.statusText!=="canceled"){b.addClass("ui-tabs-loading");d.attr("aria-busy","true");this.xhr.success(function(a){setTimeout(function(){d.html(a);e._trigger("load",i,g)},1)}).complete(function(c,a){setTimeout(function(){a==="abort"&&e.panels.stop(false,true);b.removeClass("ui-tabs-loading");d.removeAttr("aria-busy");if(c===e.xhr)delete e.xhr},1)})}},_ajaxSettings:function(c,d,b){var e=this;return{url:c.attr("href"),beforeSend:function(f,c){return e._trigger("beforeLoad",d,a.extend({jqXHR:f,ajaxSettings:c},b))}}},_getPanelForTab:function(b){var c=a(b).attr("aria-controls");return this.element.find(this._sanitizeSelector("#"+c))}})})(jQuery);(function(a){var d=0;function c(c,d){var b=(c.attr("aria-describedby")||"").split(/\s+/);b.push(d);c.data("ui-tooltip-id",d).attr("aria-describedby",a.trim(b.join(" ")))}function b(c){var e=c.data("ui-tooltip-id"),b=(c.attr("aria-describedby")||"").split(/\s+/),d=a.inArray(e,b);d!==-1&&b.splice(d,1);c.removeData("ui-tooltip-id");b=a.trim(b.join(" "));if(b)c.attr("aria-describedby",b);else c.removeAttr("aria-describedby")}a.widget("ui.tooltip",{version:"1.10.2",options:{content:function(){var b=a(this).attr("title")||"";return a("<a>").text(b).html()},hide:true,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flip"},show:true,tooltipClass:null,track:false,close:null,open:null},_create:function(){this._on({mouseover:"open",focusin:"open"});this.tooltips={};this.parents={};this.options.disabled&&this._disable()},_setOption:function(b,c){var d=this;if(b==="disabled"){this[c?"_disable":"_enable"]();this.options[b]=c;return}this._super(b,c);b==="content"&&a.each(this.tooltips,function(b,a){d._updateContent(a)})},_disable:function(){var b=this;a.each(this.tooltips,function(e,d){var c=a.Event("blur");c.target=c.currentTarget=d[0];b.close(c,true)});this.element.find(this.options.items).addBack().each(function(){var b=a(this);b.is("[title]")&&b.data("ui-tooltip-title",b.attr("title")).attr("title","")})},_enable:function(){this.element.find(this.options.items).addBack().each(function(){var b=a(this);b.data("ui-tooltip-title")&&b.attr("title",b.data("ui-tooltip-title"))})},open:function(c){var d=this,b=a(c?c.target:this.element).closest(this.options.items);if(!b.length||b.data("ui-tooltip-id"))return;b.attr("title")&&b.data("ui-tooltip-title",b.attr("title"));b.data("ui-tooltip-open",true);c&&c.type==="mouseover"&&b.parents().each(function(){var b=a(this),c;if(b.data("ui-tooltip-open")){c=a.Event("blur");c.target=c.currentTarget=this;d.close(c,true)}if(b.attr("title")){b.uniqueId();d.parents[this.id]={element:this,title:b.attr("title")};b.attr("title","")}});this._updateContent(b,c)},_updateContent:function(b,a){var d,c=this.options.content,f=this,e=a?a.type:null;if(typeof c==="string")return this._open(a,b,c);d=c.call(b[0],function(c){if(!b.data("ui-tooltip-open"))return;f._delay(function(){if(a)a.type=e;this._open(a,b,c)})});d&&this._open(a,b,d)},_open:function(d,e,i){var b,f,j,g=a.extend({},this.options.position);if(!i)return;b=this._find(e);if(b.length){b.find(".ui-tooltip-content").html(i);return}if(e.is("[title]"))if(d&&d.type==="mouseover")e.attr("title","");else e.removeAttr("title");b=this._tooltip(e);c(e,b.attr("id"));b.find(".ui-tooltip-content").html(i);function h(a){g.of=a;if(b.is(":hidden"))return;b.position(g)}if(this.options.track&&d&&/^mouse/.test(d.type)){this._on(this.document,{mousemove:h});h(d)}else b.position(a.extend({of:e},this.options.position));b.hide();this._show(b,this.options.show);if(this.options.show&&this.options.show.delay)j=this.delayedShow=setInterval(function(){if(b.is(":visible")){h(g.of);clearInterval(j)}},a.fx.interval);this._trigger("open",d,{tooltip:b});f={keyup:function(c){if(c.keyCode===a.ui.keyCode.ESCAPE){var b=a.Event(c);b.currentTarget=e[0];this.close(b,true)}},remove:function(){this._removeTooltip(b)}};if(!d||d.type==="mouseover")f.mouseleave="close";if(!d||d.type==="focusin")f.focusout="close";this._on(true,e,f)},close:function(d){var f=this,c=a(d?d.currentTarget:this.element),e=this._find(c);if(this.closing)return;clearInterval(this.delayedShow);c.data("ui-tooltip-title")&&c.attr("title",c.data("ui-tooltip-title"));b(c);e.stop(true);this._hide(e,this.options.hide,function(){f._removeTooltip(a(this))});c.removeData("ui-tooltip-open");this._off(c,"mouseleave focusout keyup");c[0]!==this.element[0]&&this._off(c,"remove");this._off(this.document,"mousemove");d&&d.type==="mouseleave"&&a.each(this.parents,function(c,b){a(b.element).attr("title",b.title);delete f.parents[c]});this.closing=true;this._trigger("close",d,{tooltip:e});this.closing=false},_tooltip:function(e){var c="ui-tooltip-"+d++,b=a("<div>").attr({id:c,role:"tooltip"}).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content "+(this.options.tooltipClass||""));a("<div>").addClass("ui-tooltip-content").appendTo(b);b.appendTo(this.document[0].body);this.tooltips[c]=e;return b},_find:function(c){var b=c.data("ui-tooltip-id");return b?a("#"+b):a()},_removeTooltip:function(a){a.remove();delete this.tooltips[a.attr("id")]},_destroy:function(){var b=this;a.each(this.tooltips,function(e,c){var d=a.Event("blur");d.target=d.currentTarget=c[0];b.close(d,true);a("#"+e).remove();if(c.data("ui-tooltip-title")){c.attr("title",c.data("ui-tooltip-title"));c.removeData("ui-tooltip-title")}})}})})(jQuery);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi.actionSheet\bizagi.actionSheet.js */ 
(function(a){a.fn.actionSheet=function(f){f=f||{};var c=this;c.guid=(b()+b()+"-"+b()+"-4"+b().substr(0,3)+"-"+b()+"-"+b()+b()+b()).toLowerCase();c.template='<ul id="actionsheet-#=data.id#" class="bz-actionsheet">#for(var i = 0; i < data.actions.length; i++){#<li class="bz-render-as-option #if(i === data.actions.length-1){#bz-render-last-child#}#"><a #if(data.actions[i].guid !== "back"){#data-bz-ordinal="#= data.actions[i].guid #"#}else{#data-bz-ordinal="-1"#}#>#= data.actions[i].displayName #</a></li>#}#</ul>';var g={setDataToShow:function(){},actionClicked:function(){},actions:[]},e=a.extend(g,f);function b(){return((1+Math.random())*65536|0).toString(16).substring(1)}var d={init:function(){d.bindElements()},bindElements:function(){c.on("click",d.showActionSheet)},showActionSheet:function(){a.when(e.setDataToShow()).done(function(b){b=typeof b!=="undefined"?b:e.actions;if(b.length>0){a.each(a(".bz-actionsheet","body"),function(c,b){a(b).data("kendoMobileActionSheet").destroy()});var f=kendo.template(c.template,{useWithBlock:false});a("body").append(f({id:c.guid,actions:b}));var d=a("#actionsheet-"+c.guid).kendoMobileActionSheet({type:"phone",destroy:function(){this.element.remove()}});a("a[data-bz-ordinal]",d).kendoMobileButton({click:function(c){var a=b.find(function(a){return a.guid==c.button.data("bz-ordinal")});e.actionClicked(a)}});a(d).data("kendoMobileActionSheet").open()}})}};d.init();return d}})(jQuery);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi\js\services\bizagi.services.ajax.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.services=typeof bizagi.services!=="undefined"?bizagi.services:{};bizagi.services.ajax=typeof bizagi.services.ajax!=="undefined"?bizagi.services.ajax:{};bizagi.log=bizagi.log||function(){};bizagi.assert=bizagi.assert||function(){};bizagi.services.ajax.useAbsolutePath=typeof BIZAGI_USE_ABSOLUTE_PATH!=="undefined"?BIZAGI_USE_ABSOLUTE_PATH:false;bizagi.services.ajax.pathToBase=bizagi.services.ajax.useAbsolutePath?bizagi.loader.getLocationPrefix():typeof BIZAGI_PATH_TO_BASE!=="undefined"?BIZAGI_PATH_TO_BASE:"";bizagi.services.ajax.loginPage=bizagi.services.ajax.pathToBase||"default.htm";bizagi.services.ajax.errorPage=bizagi.services.ajax.pathToBase+"error.html";bizagi.services.ajax.rpcEnabled=false;bizagi.services.ajax.rpcHandler="RestServices/JsonRpcHandler.ashx";bizagi.services.ajax.logEnabled=true;bizagi.services.ajax.logSuccess=true;bizagi.services.ajax.logSubmits=true;bizagi.services.ajax.logFail=true;bizagi.services.ajax.batch={};bizagi.services.ajax.batchCount=0;bizagi.services.ajax.batchTimer=100;bizagi.services.ajax.batchTimeoutInstance=null;bizagi.services.ajax.xhrPool=[];$.ajaxPrefilter("json",function(a,e,b){bizagi.services.ajax.xhrPool.push(b);var d=a.rpcEnabled!==undefined?a.rpcEnabled:bizagi.services.ajax.rpcEnabled;if(a.triggerBatch)return;if(d){if(bizagi.services.ajax.batchTimeoutInstance==null)bizagi.services.ajax.batchTimeoutInstance=setTimeout(function(){bizagi.services.ajax.fireBatch()},bizagi.services.ajax.batchTimer);var c=jQuery.Deferred();c.promise(b);bizagi.services.ajax.addToBatch(a,c,b);a.dataType="jsonrpc";a.dataTypes=[];a.dataTypes.push(a.dataType)}});$.ajaxPrefilter("*",function(a,e,d){var c=a.rpcEnabled!==undefined?a.rpcEnabled:bizagi.services.ajax.rpcEnabled;if(a.triggerBatch||!c){var b=a.url.indexOf("http")==0?a.url:bizagi.services.ajax.pathToBase+a.url;a.url=b;if(a.dataType=="json"&&!a.cache)a.cache=false;if(bizagi.services.ajax.logEnabled){bizagi.services.ajax.logSubmits&&b.indexOf("tmpl")==-1&&b.indexOf("resources.json")==-1&&bizagi.log("Sending data to "+b,a.data,"success");if(a.crossDomain==true)a.xhrFields={withCredentials:true};d.done(function(a){bizagi.services.ajax.logSuccess&&b.indexOf("tmpl")==-1&&b.indexOf("resources.json")==-1&&bizagi.log(b+" succeded",a,"success")}).fail(function(e,d,c){bizagi.log("Sent data to"+b,a.data,"error");if(bizagi.services.ajax.logFail){if(c.name=="SyntaxError")bizagi.log(b+" failed - syntax error",arguments[0].responseText,"error");else bizagi.log(b+" failed",c,"error");if((arguments[0]&&arguments[0].status)==404&&bizagi.detectDevice()!="desktop"){bizagi.showMessageBox(bizagi.localization.getResource("workportal-general-error-connection"),"Error");typeof bizagi.kendoMobileApplication!=="undefined"&&bizagi.util.mobility.stopLoading()}}})}}return});$.ajaxTransport("jsonrpc",function(){return{send:function(){},abort:function(){}}});bizagi.services.ajax.parseJSON=function(b){var a={};b=b.replace(/(\r\n|\n|\r)/gm,"");try{a=$.parseJSON(b)}catch(e){try{a=$.parseJSON(b.replace(/\x1d/g,"."))}catch(d){a.type="error";a.code=e.errorCode;a.message=b}}true;if(a.type&&a.type=="error"||a.status&&a.status=="error"){if(a.code=="AUTH_ERROR"||a.code=="AUTHENTICATION_ERROR")if(typeof bizagi.services.ajax.loginPage==="string"){sessionStorage.removeItem("bizagiAuthentication");if(localStorage.getItem("clientLog")!="true"){window.location=bizagi.services.ajax.loginPage;if(!bizagi.util.isIE()&&bizagi.detectDevice()==="desktop")window.location.href=location.origin+location.pathname}}else bizagi.services.ajax.loginPage();else if(a.code=="FED_AUTHENTICATION_ERROR")if(typeof bizagi.services.ajax.loginPage==="string"){sessionStorage.setItem("FED_AUTHENTICATION_ERROR",true);window.location=a.message}else bizagi.services.ajax.loginPage();if(a.code=="licenseError")window.location=bizagi.services.ajax.errorPage+"?message="+a.message+"&type="+a.type;var c=(a.code?a.code:"")+(a.message?""+a.message:"");jQuery.error(c)}localStorage.setItem("clientLog","false");return a};bizagi.services.ajax.xhrPool.abortAll=function(){$(this).each(function(b,a){a.abort()});bizagi.services.ajax.xhrPool.splice(0,bizagi.services.ajax.xhrPool.length)};$.ajaxSetup({converters:{"text json":bizagi.services.ajax.parseJSON},complete:function(b){var a=bizagi.services.ajax.xhrPool.indexOf(b);a>-1&&bizagi.services.ajax.xhrPool.splice(a,1)}});bizagi.services.ajax.enableBatch=function(a){bizagi.services.ajax.rpcEnabled&&!a&&bizagi.services.ajax.fireBatch();bizagi.services.ajax.rpcEnabled=a};bizagi.services.ajax.addToBatch=function(c,a,d){a=a||$.Deferred();var b=bizagi.services.ajax.batchCount;bizagi.services.ajax.batch[b]={deferred:a,jqXHR:d,options:c,id:b};bizagi.services.ajax.batchCount++;return a.promise()};bizagi.services.ajax.fireBatch=function(){var b=[];$.each(bizagi.services.ajax.batch,function(c,a){b.push(d(a.options,a.id))});var a=bizagi.services.ajax.batch;bizagi.services.ajax.batch={};bizagi.services.ajax.batchCount=0;var c=$.ajax({url:bizagi.services.ajax.pathToBase+bizagi.services.ajax.rpcHandler,type:"POST",dataType:"json",triggerBatch:true,data:JSON.encode(b)});$.when(c).done(function(b){$.each(b,function(d,b){var c=a[b.id].deferred;if(b.result)c.resolve($.parseJSON(b.result));else if(b.error){bizagi.log("Sent data to"+a[b.id].options.url,a[b.id].options.data,"error");bizagi.services.ajax.logFail&&bizagi.log(a[b.id].options.url+" failed",b.error,"error");c.reject(null,"error",b.error)}})});bizagi.services.ajax.batchTimeoutInstance=null;function d(a,b){return{jsonrpc:"2.0",method:a.url,params:a.data,id:b}}return c};if(typeof jQuery.browser!="undefined"&&jQuery.browser.msie&&window.XDomainRequest&&(document.documentMode==7||document.documentMode==8||document.documentMode==9))jQuery.support.cors=true;
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi\js\services\bizagi.services.service.js */ 
$.Class.extend("bizagi.services.service",{},{init:function(){},getServerResourceDictionary:function(){var a="";if(typeof bizagiConfig!=="undefined"&&bizagiConfig.proxyPrefix)a=bizagiConfig.proxyPrefix;else if(typeof BIZAGI_PROXY_PREFIX!=="undefined")a=BIZAGI_PROXY_PREFIX;var b=typeof BIZAGI_LANGUAGE==="undefined"?bizagi.language:BIZAGI_LANGUAGE;return $.ajax({url:a+"Rest/Multilanguage/Client?cultureName="+b,rpcEnabled:false,dataType:"json",async:true})},getResourceDictionary:function(a){return $.ajax({url:a,rpcEnabled:false,dataType:"json",async:true})}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi\js\services\bizagi.kendo.template.services.service.js */ 
/*
*   Name: BizAgi Template Services
*   Author: oo
*   Comments:
*   -   This class will provide a facade to access to all templates available on bizagi
*/

$.Class.extend('bizagi.templates.services.service', {
    /*  Static templates*/
    cachedTemplates: {},
    customTemplates: {},
    releaseIdCachedTemplate: {}
}, {
    /* 
    *   Constructor
    */
    init: function (localization) {
        this.localization = localization;
        this.cssRegex = /{{css '(.+)'}}/g;
        this.loadingFileDeferred = {};
    },

    /*
    *   Load custom templates info in order to override framework stuff
    */
    loadCustomTemplates: function () {
        var self = this;
        var defer = new $.Deferred();

        $.when(this.getTemplateFile(this.Class.customTemplateFile))
        .done(function (response) {
            var temp = $('<div />').html(response);
            var responseParts = $('script', temp);
            $.each(responseParts, function (i, responsePart) {
                responsePart = $(responsePart);
                self.Class.customTemplates[responsePart.attr('id')] = responsePart.html();
            });


            // Resolve deferred
            defer.resolve();
        });

        return defer.promise();
    },

    /*
    *   Returns the template from cache or from ajax
    *   Must return a deferred
    */
    getTemplate: function (template, id) {

        var self = this;
        var isCustomTemplate = false;

        // If the template contains double hash, it's because its in release mode, then we got to ignore the first hash
        template = template.replace(/([^#]*#)([^#]*#)([^#]*)/g, '$1$3');

        // If the template contains the hash before the question mark, we need to exchange them
        template = template.replace(/([a-zA-Z_\/.-=]*)#([a-zA-Z_\/.-]*)\?(build=[a-zA-Z0-9.]*)/g, '$1?$3#$2');

        if (bizagi.enableCustomizations) {
            var hash = self.getHash(template);

            // Check if a custom template has been given in order to override it
            if (self.Class.customTemplates[hash]) {
                isCustomTemplate = true;
            }
        }

        if (isCustomTemplate || (bizagi.loader.environment !== 'release')) {
            return this.internalGetTemplate(template, id)
            .pipe(function (resolvedTemplate) {
                // Creates a in-memory element to hold the template
                return resolvedTemplate; //$.template('#' + template, resolvedTemplate);
            });
        }
        else {
            return this.getTemplateForRelease(template, id).pipe(function (resolvedTemplate) {
                return resolvedTemplate;
            });
        }
    },
    /*
    *   Returns the template from cache or from ajax
    *   Must return a deferred
    */
    getTemplateWebpart: function (template, id) {

        // If the template contains double hash, it's because its in release mode, then we got to ignore the first hash
        template = template.replace(/([^#]*#)([^#]*#)([^#]*)/g, '$1$3');

        // If the template contains the hash before the question mark, we need to exchange them
        template = template.replace(/([a-zA-Z_\/.-=]*)#([a-zA-Z_\/.-]*)\?(build=[a-zA-Z0-9.]*)/g, '$1?$3#$2');
        return this.internalGetTemplate(template, id)
	    .pipe(function (resolvedTemplate) {
	        // Creates a in-memory element to hold the template
	        return resolvedTemplate; //$.template('#' + template, resolvedTemplate);
	    });
    },

    /*
    * Load the production template and return requested template
    */
    getTemplateForRelease: function (template, id) {
        var self = this, defer = $.Deferred(), url, hash, cachedTemplate;

        // extract url
        url = self.getUrlWithoutHashes(template);
        hash = self.getHash(template);
        self.Class.releaseIdCachedTemplate[hash] = id;

        cachedTemplate = self.Class.cachedTemplates[url];

        if (!cachedTemplate) {
            $.when(self.loadTemplateFileForRelease(url))
            .done(function () {
                defer.resolve(self.processAndReturnTemplate(url, id));
            });

        } else {
            defer.resolve(self.processAndReturnTemplate(url, id));
        }

        return defer.promise();
    },

    loadTemplateFileForRelease: function (url) {
        var self = this;
        var defer = $.Deferred();

        // Check in cache
        if (self.Class.cachedTemplates[url]) { return self.Class.cachedTemplates[url]; }

        // Check if it is already being loaded
        if (self.loadingFileDeferred[url]) {
            $.when(self.loadingFileDeferred[url])
            .done(function () {
                // Resolve with cached content
                defer.resolve(self.Class.cachedTemplates[url]);
            });
            return defer.promise();
        }

        // Load file via ajax
        self.loadingFileDeferred[url] = $.Deferred();
        $.when($.ajax({ url: url, dataType: 'text' }),
                self.localization.ready()
        ).done(function (response) {
            if (!response) {
                defer.reject('');
            }
            // response is an object
            if (typeof (response) != 'string') {
                response = JSON.parse(response[0]);
            }

            self.Class.cachedTemplates[url] = response;

            // process templates for translate, remove CSS and generate global function template
            var index, valueTemplate, translatedTemplate;
            for (index in response) {
                if (response.hasOwnProperty(index)) {
                    valueTemplate = response[index];
                    translatedTemplate = self.localization.translate(valueTemplate);

                    // Remove css tags
                    translatedTemplate = translatedTemplate.replace(self.cssRegex, '');

                    var id = self.Class.releaseIdCachedTemplate[index];

                    self.Class.cachedTemplates[url][id] = self.Class.cachedTemplates[id] = translatedTemplate;
                }
            }

            self.loadingFileDeferred[url].resolve();
            defer.resolve(response);
        });

        return defer.promise();
    },

    /*
    * Return hashed template
    */
    processAndReturnTemplate: function (url, id) {
        var self = this;
        var template = self.Class.cachedTemplates[url][id];

        if (!template) {
            throw 'template ' + template + 'doesn\'t exist';
        }

        return template;
    },

    /*
    *   Returns the template from cache or from ajax
    *   Must return a deferred
    */
    internalGetTemplate: function (template, id) {
        var self = this;
        var defer = new $.Deferred();
        var cachedTemplate = this.Class.cachedTemplates[id];
        if (!cachedTemplate) {

            // Check if the template has a hash
            if (self.containsHash(template)) {
                var url = self.getUrlWithoutHashes(template);
                var hash = self.getHash(template);

                // Check if a custom template has been given in order to override it
                if (self.Class.customTemplates[hash]) {
                    // Resolve deferred
                    defer.resolve(self.Class.customTemplates[hash]);
                    return defer.promise();
                }

                $.when(self.getTemplatePart(url, hash, id))
                .done(function (response) {
                    // Set in cache
                    self.Class.cachedTemplates[id] = response;

                    // Resolve deferred
                    defer.resolve(response);
                });

            } else {

                // Gets the full template
                $.when(self.getTemplateFile(template, id))
                .done(function (response) {

                    // Resolve deferred
                    defer.resolve(response);
                });
            }
        }
        else {

            defer.resolve(cachedTemplate);
        }

        return defer.promise();
    },

    /*
    *   Gets a template part from a composite template file
    *   Just load one template at a time to avoid multiple loadings of the same file
    */
    getTemplatePart: function (templateUrl, hash, id) {
        var self = this;
        var defer = new $.Deferred();

        // If it is already loading a template set a timeout to check later
        $.when(self.loadingTemplatePartExecution)
        .done(function () {
            // Set a async flag to synchronize this method
            self.loadingTemplatePart = true;
            self.loadingTemplatePartExecution = new $.Deferred();

            // Load template file
            $.when(self.getTemplateFile(templateUrl, id))
            .done(function (response) {
                var temp = $('<div />').html(response);
                var responsePart = $('#' + hash, temp);

                // Assertion to check that hash exists inside the main template
                bizagi.assert(responsePart.length > 0, 'No template part (' + hash + ') found in content for:' + templateUrl);

                // Set response
                response = responsePart.html();

                // Free async flag
                self.loadingTemplatePartExecution.resolve();
                self.loadingTemplatePart = true;

                // Resolve main deferred
                defer.resolve(response);
            });
        });

        return defer.promise();
    },

    /*
    *   Loads a full template file and keeps it on cache
    */
    getTemplateFile: function (template, id) {
        var self = this;
        var cachedTemplate = this.Class.cachedTemplates[id];

        if (cachedTemplate) {
            return cachedTemplate;
        }

        // Not found in cache, fetch it with ajax
        return $.when(
                    $.ajax({ url: template, dataType: 'text' }),
                    self.localization.ready()
                ).pipe(function (response) {
                    // if no response has been found in the source file return an empty template
                    if (!response) { return ''; }

                    if (typeof (response) != 'string') {
                        response = response[0];
                    }

                    // Translate template
                    var translatedTemplate = self.localization.translate(response);

                    // Find css and import them
                    var matches = translatedTemplate.match(self.cssRegex);
                    if (matches) {
                        $.each(matches, function (i, match) {
                            var cssFile = match.replace(self.cssRegex, '$1');
                            bizagi.loader.loadFile({
                                src: cssFile,
                                type: 'css',
                                environment: 'debug'
                            });
                        });
                    }
                    // Remove css tags
                    translatedTemplate = translatedTemplate.replace(self.cssRegex, '');

                    // Cache all sub-templates
                    var temp = $('<div />').html(translatedTemplate);
                    $('script', temp).each(function (i, subTemplate) {
                        var id = $(subTemplate).attr('id');
                        //alert('review')
                        // $.template('#' + id, $(subTemplate).html());
                    });

                    // Set in cache
                    self.Class.cachedTemplates[id] = typeof (translatedTemplate) === 'string' ? translatedTemplate : translatedTemplate[0].outerHTML;

                    return translatedTemplate;
                });
    },

    /*
    *   Check if a url contains hashes
    */
    containsHash: function (url) {
        return url.indexOf('#') != -1;
    },

    /*
    *   Get url hash
    */
    getHash: function (url) {
        return url.substring(url.indexOf('#') + 1, url.length);
    },

    /*
    *   Get url without hashes
    */
    getUrlWithoutHashes: function (url) {
        return url.substring(0, url.indexOf('#'));
    }
});

/**
 *   Overrides detach method to apply custom stuff
 */
jQuery.fn.fastEmpty = function () {
    // Call original jquery method
    this.children().detach();

    // Check if it has loading property to apply plugin
    var element = $(this);
    if (element.hasClass('ui-bizagi-component-loading')) {
        element.loadingMessage();
    }
};

/**
 *   JQuery tmpl extension
 */
jQuery.fasttmpl = function(tmpl, data) {
    if (typeof tmpl === 'undefined') {
        return '';
    }

    var tempTmpl = kendo.template(tmpl,{ useWithBlock: false });
    // Return original response
    var html = tempTmpl(data)
    html = html.replace(/(\n*)|(\r*)|(\t*)/g, '');
    html = html.replace(/>\s*</g, '><').trim();
    return html;
};


jQuery.tmpl = function(tmpl, data) {
    return $(jQuery.fasttmpl(tmpl, data));
};

jQuery.cacheTemplate = function(tmpl, data){
    return jQuery.fasttmpl(bizagi.templates.services.service.cachedTemplates[tmpl], data);
};

/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi\js\bizagi.l10n.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.util=typeof bizagi.util!=="undefined"?bizagi.util:{};$.Class.extend("bizagi.l10n",{templateRegex:/%{([\w-{}$.]+)}/,templateSubExpressionRegex:/[$]{([\w.]+)}/g},{init:function(a){this.resourceLocationDefinition=a;this.service=new bizagi.services.service;this.dictionaries={};this.dictionary={};this.readyDeferred=null;this.enableCustomizations=typeof bizagi.enableCustomizations!=="undefined"?bizagi.enableCustomizations:typeof BIZAGI_ENABLE_CUSTOMIZATIONS!=="undefined"?BIZAGI_ENABLE_CUSTOMIZATIONS:true;this.defaultDatetimeInfo=typeof BIZAGI_DEFAULT_DATETIME_INFO!="undefined"?BIZAGI_DEFAULT_DATETIME_INFO:false},setLanguage:function(b){var a=this;a.language=b;a.readyDeferred=new $.Deferred;$.when(a._getDictionary(b)).done(function(c){a.dictionary=c;if(a.defaultDatetimeInfo){a.defaultDatetimeInfo.shortDateFormat&&a.overrideSpecificResource("dateFormat",a.defaultDatetimeInfo.shortDateFormat);a.defaultDatetimeInfo.timeFormat&&a.overrideSpecificResource("timeFormat",a.defaultDatetimeInfo.timeFormat)}if(a.enableCustomizations){if(a.resourceLocationDefinition.custom){var b=typeof BIZAGI_LOCAL_RESOURCES!=="undefined"&&BIZAGI_LOCAL_RESOURCES?a.service.getResourceDictionary:a.service.getServerResourceDictionary;b(a.resourceLocationDefinition.custom).done(function(b){a.enableCustomizations&&a.overrideResources(b);a.readyDeferred.resolve()})}}else a.readyDeferred.resolve()})},resolvei18n:function(a){if(!a)return"";if(typeof a=="string")return a;if(this.language&&this.language!="default"){var b=a.i18n["default"].languages[this.language];if(b)return b}return a.i18n["default"]},getResource:function(a){var b=this;return b.dictionary[a]==null?a:b.dictionary[a]},translate:function(b){var e=this,a;do{a=this.Class.templateRegex.exec(b);if(a){var c=a[1];if(c.indexOf("$")==-1)b=b.replaceAll(a[0],e.getResource(c));else{var d=this.Class.templateSubExpressionRegex.exec(c);b=b.replaceAll(a[0],"{{resource "+d[1]+"}}")}}}while(a!=null);return b},overrideResources:function(e){var a=this,d=a._getUserLanguage(a.language),b=e[d];for(var c in b)a.dictionary[c]=b[c]},overrideSpecificResource:function(b,a){var c=this;b=b||"nodefined";a=a||"";c.dictionary[b]=a},_getDictionary:function(a){var b=this,c=new $.Deferred;a=a.toLowerCase();if(b.dictionaries[a]!=null)return b.dictionaries[a];var e=b._getUserLanguage(a),d=typeof BIZAGI_LOCAL_RESOURCES!=="undefined"&&BIZAGI_LOCAL_RESOURCES?b.service.getResourceDictionary:b.service.getServerResourceDictionary;d(b.resourceLocationDefinition[e]).done(function(d){b.dictionaries[a]=d;c.resolve(d)}).fail(function(d,c,b){if(typeof Windows!="undefined")bizagi.log("Can't load localization file for language: "+a+", "+c+" - "+b);else alert("Can't load localization file for language: "+a+", "+c+" - "+b)});return c.promise()},_getUserLanguage:function(e){var d=this,a=e.toLowerCase();if(d.resourceLocationDefinition[a]==null&&a.length>0)for(var c=a.split("-"),b=c.length-1;b>=0;b--)if(d.resourceLocationDefinition[c[b]]!=null){a=c[b];break}if(a.length==0||d.resourceLocationDefinition[a]==null){bizagi.log("No resource definition location has been given for language :"+e+", loading default language");a="default"}return a},ready:function(){var a=this;return a.readyDeferred.promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi.notifications\js\bizagi.notifications.flat.js */ 
(function(a){a.widget("ui.bizagi_notifications",{options:{containerAdditionalClass:"",headerAdditionalClass:"",contentAdditionalClass:"",itemAdditionalClass:"",itemIcon:"ui-icon-alert",title:"Notifications",minimized:true,clearEnabled:true,sendEnabled:false,location:"bottom",device:"desktop"},_init:function(){var c=this,b=c.options,e=c.element;c.counter=0;e.addClass(b.containerAdditionalClass).addClass("ui-bizagi-notifications-container");e.addClass(b.minimized?"ui-bizagi-state-collapsed":"ui-bizagi-state-expanded");var f=a("<div></div>").addClass("ui-bizagi-notifications-content").addClass(b.contentAdditionalClass).addClass("ui-widget-content"),d=a("<div></div>").addClass("ui-bizagi-notifications-header").addClass(b.headerAdditionalClass).addClass("ui-widget-header").addClass("ui-state-active");if(b.device!=="desktop")d.data("expanded",!b.minimized).html("<label>"+b.title+"</label>");else d.data("expanded",!b.minimized).text(b.title);d.mouseenter(function(){e.addClass("ui-bizagi-state-hover")});d.mouseleave(function(){e.removeClass("ui-bizagi-state-hover")});c.expandedIcon=b.location==="bottom"?"bz-icon-expanded":"bz-icon-collapsed";c.collapsedIcon=b.location==="bottom"?"bz-icon-collapsed":"bz-icon-expanded";if(b.device==="desktop")c.headerButton=a("<span />").addClass("ui-bizagi-notifications-box-button").addClass("ui-icon").addClass(b.minimized?c.collapsedIcon:c.expandedIcon).appendTo(d).click(function(){c.onHeaderClick()});else{d.click(function(){c.onHeaderClick()});c.headerButton=a("<span />").addClass("ui-bizagi-notifications-box-button").addClass("ui-icon").addClass(b.minimized?c.collapsedIcon:c.expandedIcon).appendTo(d)}var h=a("<span />").addClass("ui-bizagi-notifications-close-button").appendTo(d);a("<span />").addClass("bz-icon").addClass("bz-cancel").appendTo(h);if(b.clearEnabled){var g=a("<span />").addClass("ui-bizagi-notifications-clear-button").appendTo(d).click(function(){c.clearAll(false)});a("<span />").addClass("bz-icon").addClass("bz-trash-icon").appendTo(g)}if(b.sendEnabled){var i=a("<span />").addClass("ui-bizagi-notifications-send-button").appendTo(d).click(function(){c.sendMail()});a("<span />").addClass("bz-icon").addClass("bz-send").appendTo(i)}e.append(d).append(f);b.location=="bottom"&&e.addClass("ui-bizagi-notifications-container-bottom");b.minimized&&f.hide();c.content=f;c.header=d},addNotification:function(b,h,f,e,g){if(b)if(b.replace){var c=this,d=c.options,k=c.element,l=c.batchMode?c.batchContent:c.content,j=c.content.closest(".ui-bizagi-log-container").length===0;if(j&&(d.device==="smartphone_ios"||d.device==="smartphone_android")&&c.counter>=1)return;f=f||d.itemIcon;e=e||d.itemAdditionalClass;if(typeof b!="string")b=b.toString();b=bizagi.util.encodeHtml(b.replace(/<(\/?(?:STRONG|FONT))>/gi,"#$1!#"));b=b.replace(/#(\/?(?:STRONG|FONT))!#/gi,"<$1>");b=b.replace(/\n|\r/g,"<br/>");var i=a("<div class='ui-bizagi-notification-item "+e+" ui-widget-content'><span class='bz-icon "+f+"' /><label>"+b+"</label></div>").appendTo(l);!c.batchMode&&c._hasScrollBar()&&i[0].scrollIntoView();i.bind("click",function(){k.triggerHandler("itemClick",{data:h});g&&g({data:h})});!c.batchMode&&c.show();c.counter++}},clearAll:function(a){var b=this,c=b.content;a=a===undefined?true:a;bizagi.util.removeItemLocalStorage("bizagi.mobility.trace");c.empty();b.counter=0;if(a)b.hide();else b.show()},sendMail:function(){var a=bizagi.util.getItemLocalStorage("bizagi.mobility.trace");if(!a){bizagi.showMessageBox(bizagi.localization.getResource("bz-rp-nodata"),"Error");return}if(typeof cordova==="undefined")return;window.plugin.email.isAvailable(function(c){if(!c){bizagi.showMessageBox("The e-mail service is currently not available.","Error");return}var b="base64:bizagi-mobile-log.txt//"+bizagi.util.b64EncodeUnicode(a),d="<p>A log file has been attached</p><br /><p> Powered by Bizagi Mobile Application</p><br />";cordova.plugins.email.open({subject:"Bizagi Mobile - Log Report",body:d,isHtml:true,attachments:[b]})})},show:function(){var a=this,b=a.element;!a.isContentVisible()&&b.fadeIn()},hide:function(){var a=this,b=a.element;a.isContentVisible()&&b.hide()},count:function(){var b=this,a=b.content;return a.children().size()},_hasScrollBar:function(){var a=this,b=a.content;return!a.isContentVisible()?false:bizagi.util.hasScroll(b,"vertical")},onHeaderClick:function(){var a=this;if(a.isExpanded())a.collapse();else a.expand()},isExpanded:function(){var b=this,a=b.header;return a.data("expanded")==true},collapse:function(){var a=this,e=a.options,c=a.element,b=a.headerButton,d=a.header;b.removeClass(a.expandedIcon);b.addClass(a.collapsedIcon);d.data("expanded",false);c.removeClass("ui-bizagi-state-expanded");c.addClass("ui-bizagi-state-collapsed");a.content.hide()},expand:function(){var a=this,e=a.options,c=a.element,b=a.headerButton,d=a.header;b.removeClass(a.collapsedIcon);b.addClass(a.expandedIcon);d.data("expanded",true);c.removeClass("ui-bizagi-state-collapsed");c.addClass("ui-bizagi-state-expanded");a.content.show()},autoFocusFirstError:function(){var c=this,b=c.content;window.setTimeout(function(){a("div:first",b).click()},9e3)},initBatch:function(){var b=this;b.batchMode=true;b.batchContent=a("<div></div>")},endBatch:function(){var a=this;a.batchMode=false;a.batchContent.children().appendTo(a.content);a.batchContent.detach();a.batchContent=null;a.content.children().length>0&&a.show()},isContentVisible:function(){var c=this,a=c.content,b=c.element;return b.length>0&&b.css("display")!="none"&&a.length>0&&a.css("display")!="none"}})})(jQuery);
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\bizagi.workportal.util.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.util=typeof bizagi.util!=="undefined"?bizagi.util:{};bizagi.context=typeof bizagi.context!=="undefined"?bizagi.context:{};bizagi.util.smartphone=typeof bizagi.util.smartphone!=="undefined"?bizagi.util.smartphone:{};bizagi.util.tablet=typeof bizagi.util.tablet!=="undefined"?bizagi.util.tablet:{};bizagi.util.mobility=typeof bizagi.util.mobility!=="undefined"?bizagi.util.mobility:{};bizagi.util.isIPad=function(){return navigator.platform.indexOf("iPad")!=-1};bizagi.util.isIPhone=function(){return navigator.userAgent.toLowerCase().indexOf("iphone")>-1};bizagi.util.isIPod=function(){return navigator.userAgent.toLowerCase().indexOf("ipod")>-1};bizagi.util.isIE7=function(){return bizagi.util.isIE()&&document.documentMode==7};bizagi.util.isIE10=function(){return bizagi.util.isIE()&&document.documentMode==10};bizagi.util.isIE8=function(){return bizagi.util.isIE()&&document.documentMode==8};bizagi.util.isIE9=function(){return bizagi.util.isIE()&&document.documentMode==9};bizagi.util.isIE11=function(){return!!navigator.userAgent.match(/Trident\/7.0/)&&!navigator.userAgent.match(/MSIE/i)};bizagi.util.isIE=function(){return navigator.appName.indexOf("Internet Explorer")>0};bizagi.util.isAndroid=function(){return navigator.userAgent.toLowerCase().indexOf("android")>-1};bizagi.util.isIphoneHigherIOS5=function(){return this.value!=undefined?this.value:(this.value=RegExp("OS\\s*(5|6|7|8)_*\\d").test(navigator.userAgent)&&RegExp(" AppleWebKit/").test(navigator.userAgent))};bizagi.util.isLessThanIOS5=function(){return navigator.userAgent.match(new RegExp(/CPU OS (1|2|3|4)/i))?true:false};bizagi.util.isIphoneAndLessIOS6=function(){return this.value!=undefined?this.value:(this.value=RegExp("OS\\s*(4|5|6)_*\\d").test(navigator.userAgent)&&RegExp(" AppleWebKit/").test(navigator.userAgent))};bizagi.util.getInternetExplorerVersion=function(){return!bizagi.util.isIE()?-1:Number(document.documentMode)};bizagi.util.detectDevice=function(){return bizagi.detectDevice()};bizagi.util.isTablet=function(){return bizagi.util.isIPad()};bizagi.util.isNumeric=function(a){return!isNaN(parseFloat(a))&&isFinite(a)};bizagi.util.isEmpty=function(a){return a===0?false:a===undefined||a===null||a===""?true:typeof a==="Array"?a.length==0:Object.prototype.toString.apply(a)==="[object Array]"?a.length===0:Object.prototype.toString.apply(a)==="[object Object]"?$.isEmptyObject(a):false};bizagi.util.decodeURI=function(d){d=d||"";var c=false,a=d,b;while(!c)try{b=decodeURI(a);if(b==a)c=true;else a=b}catch(e){c=true}return a};bizagi.loader.initWebpartOld=bizagi.loader.initWebpart;bizagi.loader.initWebpart=function(a,c,b){var d=this;if(a!=null)if(!a.initialized)if(a.devices)if(typeof a.devices[c]!=="undefined")bizagi.loader.initWebpartOld($.extend(a,a.devices[c]),b);else console.log("No existe la definicion del device "+c+" Para el webpart "+a.name);else bizagi.loader.initWebpartOld(a,b);else steal.then(function(){b&&b()});return this};bizagi.util.initWebpart=function(a,c){var b=new $.Deferred;if(a.initialized)b.resolve();else if(a.initializing)$.when(a.loadingDeferred.promise()).done(function(){b.resolve()});else{a.loadingDeferred=new $.Deferred;a.initializing=true;bizagi.loader.initWebpart(a,c,function(){a.loadingDeferred.resolve();a.initializing=false;b.resolve()})}return b.promise()};bizagi.cookie=function(d,b,a){if(arguments.length>1&&String(b)!=="[object Object]"){a=$.extend({},a);if(b===null||b===undefined)a.expires=-1;if(typeof a.expires==="number"){var g=a.expires,e=a.expires=new Date;e.setDate(e.getDate()+g)}b=String(b);return document.cookie=[encodeURIComponent(d),"=",a.raw?b:encodeURIComponent(b),a.expires?"; expires="+a.expires.toUTCString():"",a.path?"; path="+a.path:"",a.domain?"; domain="+a.domain:"",a.secure?"; secure":""].join("")}a=b||{};var c,f=a.raw?function(a){return a}:decodeURIComponent;return(c=(new RegExp("(?:^|; )"+encodeURIComponent(d)+"=([^;]*)")).exec(document.cookie))?f(c[1]):null};bizagi.util.formatInvariantDate=function(a,b){a=a||"";b=b||"MM/dd/yyyy hh:mm:ss";$(".formatDate",a).not(".formated").each(function(f,a){var c=$(a).html();$(a).addClass("formated");try{var e=new Date(c),d=bizagi.util.dateFormatter.formatDate(e,b);$(a).html(d)}catch(g){$(a).html(c)}})};bizagi.util.monthNames=["January","February","March","April","May","June","July","August","September","October","November","December","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];bizagi.util.dayNames=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sun","Mon","Tue","Wed","Thu","Fri","Sat"];bizagi.util.dateFormatter=new function(){this.LZ=function(a){return(a<0||a>9?"":"0")+a};this.isDate=function(c,a){var b=this.getDateFromFormat(c,a);return b==0?false:true};this.compareDates=function(e,c,f,d){var a=getDateFromFormat(e,c),b=getDateFromFormat(f,d);return a==0||b==0?-1:a>b?1:0};this.formatDate=function(d,c){var l=bizagi.util.monthNames,j=bizagi.util.dayNames;c=c+"";var g="",f=0,m,h,e=d.getYear()+"",i=d.getMonth()+1,n=d.getDate(),k=d.getDay(),b=d.getHours(),o=d.getMinutes(),p=d.getSeconds(),a={};if(e.length<4)e=""+(e-0+1900);a.y=""+e;a.yyyy=e;a.yy=e.substring(2,4);a.M=i;a.MM=this.LZ(i);a.MMM=l[i+11];a.MMMM=l[i-1];a.NNN=l[i+11];a.d=n;a.dd=this.LZ(n);a.ddd=j[k+7];a.dddd=j[k];a.E=j[k+7];a.EE=j[k];a.H=b;a.HH=this.LZ(b);a.tt=b<12?"am":"pm";a.TT=b<12?"AM":"PM";if(b==0)a.h=12;else if(b>12)a.h=b-12;else a.h=b;a.hh=this.LZ(a.h);if(b>11)a.K=b-12;else a.K=b;a.k=b+1;a.KK=this.LZ(a.K);a.kk=this.LZ(a.k);if(b>11)a.a="PM";else a.a="AM";a.m=o;a.mm=this.LZ(o);a.s=p;a.ss=this.LZ(p);while(f<c.length){m=c.charAt(f);h="";while(c.charAt(f)==m&&f<c.length)h+=c.charAt(f++);if(a[h]!=null)g=g+a[h];else g=g+h}return g};this.getDateFromFormat=function(e,i){var u=bizagi.util.monthNames,v=bizagi.util.dayNames;e=e+"";i=i+"";var b=0,l=0,w,a,p=0,q=0,m=new Date,f=m.getYear(),d=m.getMonth()+1,h=1,c=m.getHours(),j=m.getMinutes(),k=m.getSeconds(),o="";while(l<i.length){w=i.charAt(l);a="";while(i.charAt(l)==w&&l<i.length)a+=i.charAt(l++);if(a=="yyyy"||a=="yy"||a=="y"){if(a=="yyyy"){p=4;q=4}if(a=="yy"){p=2;q=2}if(a=="y"){p=2;q=4}f=g(e,b,p,q);if(f==null)return 0;b+=f.length;if(f.length==2)if(f>70)f=1900+(f-0);else f=2e3+(f-0)}else if(a=="MMMM"||a=="MMM"||a=="NNN"){d=0;for(var n=0;n<u.length;n++){var r=u[n];if(e.substring(b,b+r.length).toLowerCase()==r.toLowerCase())if(a=="MMMM"||a=="MMM"||a=="NNN"&&n>11){d=n+1;if(d>12)d-=12;b+=r.length;break}}if(d<1||d>12)return 0}else if(a=="dddd"||a=="EE"||a=="E")for(var t=0;t<v.length;t++){var s=v[t];if(e.substring(b,b+s.length).toLowerCase()==s.toLowerCase()){b+=s.length;break}}else if(a=="MM"||a=="M"){d=g(e,b,1,2);if(d==null||d<1||d>12)return 0;b+=d.length}else if(a=="dd"||a=="d"){h=g(e,b,1,2);if(h==null||h<1||h>31)return 0;b+=h.length}else if(a=="hh"||a=="h"){c=g(e,b,a.length,2);if(c==null||c<1||c>12)return 0;b+=c.length}else if(a=="HH"||a=="H"){c=g(e,b,a.length,2);if(c==null||c<0||c>23)return 0;b+=c.length}else if(a=="KK"||a=="K"){c=g(e,b,a.length,2);if(c==null||c<0||c>11)return 0;b+=c.length}else if(a=="kk"||a=="k"){c=g(e,b,a.length,2);if(c==null||c<1||c>24)return 0;b+=c.length;c--}else if(a=="mm"||a=="m"){j=g(e,b,a.length,2);if(j==null||j<0||j>59)return 0;b+=j.length}else if(a=="ss"||a=="s"){k=g(e,b,a.length,2);if(k==null||k<0||k>59)return 0;b+=k.length}else if(a=="a"){if(e.substring(b,b+2).toLowerCase()=="am")o="AM";else if(e.substring(b,b+2).toLowerCase()=="pm")o="PM";else return 0;b+=2}else if(e.substring(b,b+a.length)!=a)return 0;else b+=a.length}if(d==2)if(f%4==0&&f%100!=0||f%400==0){if(h>29)return 0}else if(h>28)return 0;if(d==4||d==6||d==9||d==11)if(h>30)return 0;if(c<12&&o=="PM")c=c-0+12;else if(c>11&&o=="AM")c-=12;var y=new Date(f,d-1,h,c,j,k);return y;function x(b){for(var c="1234567890",a=0;a<b.length;a++)if(c.indexOf(b.charAt(a))==-1)return false;return true}function g(f,d,c,e){for(var b=e;b>=c;b--){var a=f.substring(d,d+b);if(a.length<c)return null;if(x(a))return a}return null}};this.parseDate=function(j){for(var e=arguments.length==2?arguments[1]:false,i=["y-M-d","MMM d, y","MMM d,y","y-MMM-d","d-MMM-y","MMM d"],d=["M/d/y","M-d-y","M.d.y","MMM-d","M/d","M-d"],g=["d/M/y","d-M-y","d.M.y","d-MMM","d/M","d-M"],f=[i,e?g:d,e?d:g],a,b=0;b<f.length;b++)for(var h=window[f[b]],c=0;c<h.length;c++){a=getDateFromFormat(j,h[c]);if(a!=0)return new Date(a)}return null};this.analyzeTimeFormat=function(c){var e=0,f,d={show24Hours:false,showSeconds:false,separator:":"},a,b="";while(e<c.length){f=c.charAt(e);a="";while(c.charAt(e)==f&&e<c.length)a+=c.charAt(e++);if(a=="hh"||a=="h"){b=a;d.show24Hours=false}else if(a=="HH"||a=="H"){b=a;d.show24Hours=true}else if(a=="mm"||a=="m")b=a;else if(a=="ss"||a=="s"){b=a;d.showSeconds=true}else if(a=="a")b=a;else if(b.toUpperCase()=="H"||b.toUpperCase()=="HH")d.separator=a}return d};this.getDateFromInvariant=function(a,c){a=a==null?"":a;a=typeof a!="string"?a.toString():a;var b="MM/dd/yyyy"+(c?" H:mm:ss":"");if(c)if(a&&(a.toLowerCase().indexOf("am")>0||a.toLowerCase().indexOf("pm")>0))b="MM/dd/yyyy h:mm:ss a";var d=bizagi.util.dateFormatter.getDateFromFormat(a,b);if(d==0&&!c){b="MM/dd/yyyy H:mm:ss";d=bizagi.util.dateFormatter.getDateFromFormat(a,b)}return d};this.formatInvariant=function(d,c){var a="MM/dd/yyyy"+(c?" HH:mm:ss":""),b=bizagi.util.dateFormatter.formatDate(d,a);return b};this.getDateFromISO=function(d,c){var a="yyyy-MM-dd"+(c?" HH:mm":""),b=bizagi.util.dateFormatter.getDateFromFormat(d,a);if(b==0&&c){a="yyyy-MM-dd"+(c?" HH:mm:ss":"");b=bizagi.util.dateFormatter.getDateFromFormat(d,a)}if(b==0&&!c){a="yyyy-MM-dd HH:mm";b=bizagi.util.dateFormatter.getDateFromFormat(d,a);if(b==0){a="yyyy-MM-dd HH:mm:ss";b=bizagi.util.dateFormatter.getDateFromFormat(d,a)}}return b};this.formatISO=function(d,c){var b="yyyy-MM-dd"+(c?" HH:mm":""),a=bizagi.util.dateFormatter.formatDate(d,b);return a};this.sleep=function(a){var b=(new Date).getTime();while((new Date).getTime()<b+a);}};bizagi.util.isEventSupported=function(b,a){var d=!("onblur"in document.documentElement),c;if(!b)return false;if(!a||typeof a==="string")a=document.createElement(a||"div");b="on"+b;c=b in a;if(!c&&d){if(!a.setAttribute)a=document.createElement("div");if(a.setAttribute&&a.removeAttribute){a.setAttribute(b,"");c=typeof a[b]==="function";if(a[b]!==undefined)a[b]=undefined;a.removeAttribute(b)}}return c};bizagi.circularDependencies=function(){var c=new Function,a=c.prototype;a.list=[];a.lastNodeAdded;a.addNode=function(c,i){var d=JSON.encode(i),b=g(c,d);if(b){var h=a.lastNodeAdded||a.list[a.list.length-1];h.nodeObj.addChild(b.nodeObj);a.lastNodeAdded=b}else{var f=new e(c,d);a.list.push({name:c,obj:d,nodeObj:f,timeStamp:(new Date).getTime()});return f}};a.resolve=function(c,e,g){c=c||b()||a.list[0].nodeObj;var d=new f(c,e,g);return d};var d=function(){a.list=[]},b=function(){for(var b=0;b<a.list.length;b++)if(a.list[b].nodeObj.childs.length>0)return a.list[b].nodeObj;return a.list.length>0?a.list[0].nodeObj:{}},g=function(d,f){for(var b=0;b<a.list.length;b++)if(a.list[b].name==d){var e=(new Date).getTime(),c=2e3;if(a.list[b].timeStamp+c<e)a.list.splice(b,1);else if(a.list[b].obj==f)return a.list[b]}return false},e=function(a,c){a=a||"";c=c||{};var b=function(a,b){this.name=a;this.obj=b;this.childs=[]},d=b.prototype;d.addChild=function(a){this.childs.push(a)};return b}(),f=function(){var b=function(g,f,h){this.node=g||{};this.node.childs=this.node.childs||[];this.resolved=f||[];this.seen=h||[];this.seen.push(this.node);for(var c=0;c<this.node.childs.length;c++)if(!a.hasObject(this.node.childs[c],this.resolved))if(a.hasObject(this.node.childs[c],this.seen)){d();var e={dependencyFrom:this.node.name,dependencyFromObj:this.node.obj,dependencyTo:this.node.childs[c].name,dependencyToObj:this.node.childs[c].obj,path:this.seen};return{error:$.extend({standardError:{status:"error",responseText:JSON.encode({message:a.makeErrorMessage(e),type:"alert"})},multiactionError:{message:a.makeErrorMessage(e),type:"alert"}},e)}}else b(this.node.childs[c],this.resolved,this.seen);this.resolved&&this.resolved.push(this.node)},a=b.prototype;a.hasObject=function(c,b){for(var a=0;a<b.length;a++)if(b[a].name==c.name)return true;return false};a.makeErrorMessage=function(a){var b=bizagi.localization.getResource("render-actions-loop-validation");return printf(b,a.dependencyFrom,a.dependencyTo);return{responseText:parsedMessage,message:parsedMessage,extraInfo:a}};return b}();return a};bizagi.util.autoSave=function(){var a=$.Deferred();if($(document).data("auto-save"))$(document).trigger("save-form",[a]);else a.resolve();return a.promise()};bizagi.util.isDate=function(a){a=a||"";var b=false,c=typeof a=="string"?a.split("/"):"";try{if(c.length==3){var d=new Date(a);if(d.getYear()>0)b=true}}catch(e){b=false}return b};bizagi.util.formatBoolean=function(a){var b=this;return bizagi.util.parseBoolean(a)==true?bizagi.localization.getResource("render-boolean-yes"):bizagi.util.parseBoolean(a)==false?bizagi.localization.getResource("render-boolean-no"):""};bizagi.util.replaceLineBreak=function(a){return typeof a==="string"?a.replace(/\r\n|\r|\n|<br>|<br\/>/g,"<br/>"):a};bizagi.util.replaceAll=function(c,a,b){return bizagi.replaceAll(c,a,b)};String.prototype.replaceAll=function(a,b){return bizagi.util.replaceAll(this,a,b)};bizagi.util.trim=function(a){return typeof a==="undefined"||a==null?a:a.replace(/^\s\s*/,"").replace(/\s\s*$/,"")};bizagi.util.percent2Number=function(a){return Number(String(a).replace("%",""))};bizagi.util.isObjectEmpty=function(a){return a.length>0?false:true};bizagi.util.parseBoolean=function(a){return a===undefined?null:a===null?null:a===""?null:a===true||a===1||a.toString()==="true"?true:a.toString().toLowerCase()==="true"?true:a!==null&&a===false||a===0||a.toString()==="false"?false:a.toString().toLowerCase()==="false"?false:null};bizagi.util.setContext=function(a,b){a=a||{};b=b||false;if(b)bizagi.context={};$.each(a,function(b,a){bizagi.context[b]=a})};bizagi.util.decodeURI=function(d){d=d||"";var c=false,a=d,b;while(!c)try{b=decodeURI(a);if(b==a)c=true;else a=b}catch(e){c=true}return a};(function(){jQuery.br2nl=function(a){return a.replace(/\r\n|\r|\n|<br>|<br\/>/g,"\r")}})(jQuery);(function(){jQuery.nl2br=function(a){return a.replace(/(\r\n|\n\r|\r|\n|\\r\\n|\\n\\r|\\r|\\n)/g,"<br>")}})(jQuery);bizagi.util.isNumeric=function(a){return!isNaN(parseFloat(a))&&isFinite(a)};bizagi.util.getStandardNotation=function(a,f){var i="+",h="-",f=f||".",g="e",c="",e=false,d=false;a=(a.length>0?a.toString().toLowerCase():"").replace(/ /g,"");for(var b=0;b<a.length;b++)if((a[b]===i||a[b]===h)&&(b===0||a[b-1]==g))c+=a[b];else if(!isNaN(Number(a[b])))c+=a[b];else if(a[b]===f&&!e){c+=a[b];e=true}else if(a[b]===g&&!d){c+=a[b];d=true}return c};bizagi.util.scientificNotationFormat=function(a,e,g,m,l){e=e||".";a=bizagi.util.getStandardNotation(a,e);g=g>0?g:38;m=m>0?m:-125;l=l>0?l:125;var o="e",v=new RegExp("[0-9]+("+e+"[0-9]+)?(e[+-]?[0-9]+)?"),i=a.match(v),c=0,q=a[0]==="-"?a[0]:"",h=0;a=a.replace(q,"");if(i&&i[0]&&i[2]){var d=i[0]?i[0].replace(i[2],"").split(e):"",b="",p="";d[0]=d[0].replace(/^0+/g,"");if(d[0].length>1){b=d[0].slice(1);h=b.length;p=d[0][0]||"";b=b.concat(i[1]||"")}else if(d[0].length===1){p=d[0][0]||"";b=b.concat(d[1]||"")}else{h=d[1].length;d[1]=d[1].replace(/^0+/g,"");h=d[1].length-1-h;p=d[1][0];b=d[1].slice(1)}b=b.replace(e,"").replace(/0+$/g,"");if(b.length>=g)b=b.slice(0,g-1);c=Number(i[2].replace(o,""))+h;c=c>l?l:c<m?m:c;return q+p+(b?e+b:"")+o+(c>0?"+"+c:c)}else if(a){h=a.indexOf(e);var f=0,k=a.length-1;a=a.replace(e,"");var r=f;while(!n)if(a[r]!=="0"&&a[r]!==e){n=true;f=r}else r++;var n=false,s=k;while(!n)if(a[s]!=="0"&&a[s]!==e&&"-"){n=true;k=s+1}else s--;k=k>g+f?g+f:k;if(h!==-1){var n=false,b=a.slice(f,k),u=f+1;c=h-u;return b.length>0&&!isNaN(Number(b[0]))?q+b[0]+(b.length>1?e+b.slice(1):"")+o+(c>0?"+"+c:c):"0"}else{var t=a.slice(f,f+1),j=a.slice(f+1,a.length);c=j.length;j=j.replace(/0+$/g,"");if(j.length>g)j=a.slice(f+1,g+f);return Number(t)?q+t+(j.length>0?e+j:"")+o+(c>0?"+"+c:c):"0"}}else return""};if(![].find)Array.prototype.find=function(d){if(this==null)throw new TypeError("Array.prototype.find called on null or undefined");if(typeof d!=="function")throw new TypeError("predicate must be a function");for(var c=Object(this),f=c.length>>>0,e=arguments[1],b,a=0;a<f;a++){b=c[a];if(d.call(e,b,a,c))return b}return undefined};bizagi.util.browserSupportLocalStorage=function(){try{return"localStorage"in window&&window.localStorage!==null}catch(a){return false}};bizagi.util.setItemLocalStorage=function(b,a){if(bizagi.util.browserSupportLocalStorage()){localStorage.setItem(b,a);return true}else return false};bizagi.util.getItemLocalStorage=function(a){return localStorage.getItem(a)};bizagi.util.removeItemLocalStorage=function(a){localStorage.removeItem(a)};bizagi.util.clearLocalStorage=function(){bizagi.util.browserSupportLocalStorage()&&localStorage.clear()};function printf(c){var b=Array.prototype.slice.call(arguments,1),a;return c.replace(/(%[disv])/g,function(d,c){a=b.shift();if(a!==undefined)switch(c.charCodeAt(1)){case 100:return+a;case 105:return Math.round(+a);case 115:return String(a);case 118:return a}return c})}bizagi.util.md5=function(b){var a=a||function(c,k){var e={},d=e.lib={},i=function(){},b=d.Base={extend:function(b){i.prototype=this;var a=new i;b&&a.mixIn(b);a.hasOwnProperty("init")||(a.init=function(){a.$super.init.apply(this,arguments)});a.init.prototype=a;a.$super=this;return a},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var b in a)a.hasOwnProperty(b)&&(this[b]=a[b]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},a=d.WordArray=b.extend({init:function(a,b){a=this.words=a||[];this.sigBytes=b!=k?b:4*a.length},toString:function(a){return(a||l).stringify(this)},concat:function(b){var d=this.words,c=b.words,e=this.sigBytes;b=b.sigBytes;this.clamp();if(e%4)for(var a=0;a<b;a++)d[e+a>>>2]|=(c[a>>>2]>>>24-8*(a%4)&255)<<24-8*((e+a)%4);else if(65535<c.length)for(a=0;a<b;a+=4)d[e+a>>>2]=c[a>>>2];else d.push.apply(d,c);this.sigBytes+=b;return this},clamp:function(){var b=this.words,a=this.sigBytes;b[a>>>2]&=4294967295<<32-8*(a%4);b.length=c.ceil(a/4)},clone:function(){var a=b.clone.call(this);a.words=this.words.slice(0);return a},random:function(d){for(var e=[],b=0;b<d;b+=4)e.push(4294967296*c.random()|0);return new a.init(e,d)}}),f=e.enc={},l=f.Hex={stringify:function(a){var e=a.words;a=a.sigBytes;for(var c=[],b=0;b<a;b++){var d=e[b>>>2]>>>24-8*(b%4)&255;c.push((d>>>4).toString(16));c.push((d&15).toString(16))}return c.join("")},parse:function(d){for(var c=d.length,e=[],b=0;b<c;b+=2)e[b>>>3]|=parseInt(d.substr(b,2),16)<<24-4*(b%8);return new a.init(e,c/2)}},g=f.Latin1={stringify:function(a){var d=a.words;a=a.sigBytes;for(var c=[],b=0;b<a;b++)c.push(String.fromCharCode(d[b>>>2]>>>24-8*(b%4)&255));return c.join("")},parse:function(d){for(var c=d.length,e=[],b=0;b<c;b++)e[b>>>2]|=(d.charCodeAt(b)&255)<<24-8*(b%4);return new a.init(e,c)}},m=f.Utf8={stringify:function(a){try{return decodeURIComponent(escape(g.stringify(a)))}catch(b){throw Error("Malformed UTF-8 data");}},parse:function(a){return g.parse(unescape(encodeURIComponent(a)))}},h=d.BufferedBlockAlgorithm=b.extend({reset:function(){this._data=new a.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=m.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(b){var f=this._data,i=f.words,d=f.sigBytes,g=this.blockSize,h=d/(4*g),h=b?c.ceil(h):c.max((h|0)-this._minBufferSize,0);b=h*g;d=c.min(4*b,d);if(b){for(var e=0;e<b;e+=g)this._doProcessBlock(i,e);e=i.splice(0,b);f.sigBytes-=d}return new a.init(e,d)},clone:function(){var a=b.clone.call(this);a._data=this._data.clone();return a},_minBufferSize:0});d.Hasher=h.extend({cfg:b.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){h.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(b,c){return(new a.init(c)).finalize(b)}},_createHmacHelper:function(a){return function(b,c){return(new j.HMAC.init(a,c)).finalize(b)}}});var j=e.algo={};return e}(Math);(function(k){function f(a,b,d,e,f,c,g){a=a+(b&d|~b&e)+f+g;return(a<<c|a>>>32-c)+b}function d(a,d,e,b,f,c,g){a=a+(d&b|e&~b)+f+g;return(a<<c|a>>>32-c)+d}function c(a,c,d,e,f,b,g){a=a+(c^d^e)+f+g;return(a<<b|a>>>32-b)+c}function e(a,c,d,e,f,b,g){a=a+(d^(c|~e))+f+g;return(a<<b|a>>>32-b)+c}for(var h=a,g=h.lib,l=g.WordArray,i=g.Hasher,g=h.algo,b=[],j=0;64>j;j++)b[j]=4294967296*k.abs(k.sin(j+1))|0;g=g.MD5=i.extend({_doReset:function(){this._hash=new l.init([1732584193,4023233417,2562383102,271733878])},_doProcessBlock:function(j,k){for(var l=0;16>l;l++){var n=k+l,m=j[n];j[n]=(m<<8|m>>>24)&16711935|(m<<24|m>>>8)&4278255360}var l=this._hash.words,n=j[k+0],m=j[k+1],s=j[k+2],t=j[k+3],u=j[k+4],v=j[k+5],w=j[k+6],x=j[k+7],y=j[k+8],z=j[k+9],A=j[k+10],B=j[k+11],o=j[k+12],p=j[k+13],q=j[k+14],r=j[k+15],a=l[0],g=l[1],h=l[2],i=l[3],a=f(a,g,h,i,n,7,b[0]),i=f(i,a,g,h,m,12,b[1]),h=f(h,i,a,g,s,17,b[2]),g=f(g,h,i,a,t,22,b[3]),a=f(a,g,h,i,u,7,b[4]),i=f(i,a,g,h,v,12,b[5]),h=f(h,i,a,g,w,17,b[6]),g=f(g,h,i,a,x,22,b[7]),a=f(a,g,h,i,y,7,b[8]),i=f(i,a,g,h,z,12,b[9]),h=f(h,i,a,g,A,17,b[10]),g=f(g,h,i,a,B,22,b[11]),a=f(a,g,h,i,o,7,b[12]),i=f(i,a,g,h,p,12,b[13]),h=f(h,i,a,g,q,17,b[14]),g=f(g,h,i,a,r,22,b[15]),a=d(a,g,h,i,m,5,b[16]),i=d(i,a,g,h,w,9,b[17]),h=d(h,i,a,g,B,14,b[18]),g=d(g,h,i,a,n,20,b[19]),a=d(a,g,h,i,v,5,b[20]),i=d(i,a,g,h,A,9,b[21]),h=d(h,i,a,g,r,14,b[22]),g=d(g,h,i,a,u,20,b[23]),a=d(a,g,h,i,z,5,b[24]),i=d(i,a,g,h,q,9,b[25]),h=d(h,i,a,g,t,14,b[26]),g=d(g,h,i,a,y,20,b[27]),a=d(a,g,h,i,p,5,b[28]),i=d(i,a,g,h,s,9,b[29]),h=d(h,i,a,g,x,14,b[30]),g=d(g,h,i,a,o,20,b[31]),a=c(a,g,h,i,v,4,b[32]),i=c(i,a,g,h,y,11,b[33]),h=c(h,i,a,g,B,16,b[34]),g=c(g,h,i,a,q,23,b[35]),a=c(a,g,h,i,m,4,b[36]),i=c(i,a,g,h,u,11,b[37]),h=c(h,i,a,g,x,16,b[38]),g=c(g,h,i,a,A,23,b[39]),a=c(a,g,h,i,p,4,b[40]),i=c(i,a,g,h,n,11,b[41]),h=c(h,i,a,g,t,16,b[42]),g=c(g,h,i,a,w,23,b[43]),a=c(a,g,h,i,z,4,b[44]),i=c(i,a,g,h,o,11,b[45]),h=c(h,i,a,g,r,16,b[46]),g=c(g,h,i,a,s,23,b[47]),a=e(a,g,h,i,n,6,b[48]),i=e(i,a,g,h,x,10,b[49]),h=e(h,i,a,g,q,15,b[50]),g=e(g,h,i,a,v,21,b[51]),a=e(a,g,h,i,o,6,b[52]),i=e(i,a,g,h,t,10,b[53]),h=e(h,i,a,g,A,15,b[54]),g=e(g,h,i,a,m,21,b[55]),a=e(a,g,h,i,y,6,b[56]),i=e(i,a,g,h,r,10,b[57]),h=e(h,i,a,g,w,15,b[58]),g=e(g,h,i,a,p,21,b[59]),a=e(a,g,h,i,u,6,b[60]),i=e(i,a,g,h,B,10,b[61]),h=e(h,i,a,g,s,15,b[62]),g=e(g,h,i,a,z,21,b[63]);l[0]=l[0]+a|0;l[1]=l[1]+g|0;l[2]=l[2]+h|0;l[3]=l[3]+i|0},_doFinalize:function(){var d=this._data,c=d.words,a=8*this._nDataBytes,b=8*d.sigBytes;c[b>>>5]|=128<<24-b%32;var e=k.floor(a/4294967296);c[(b+64>>>9<<4)+15]=(e<<8|e>>>24)&16711935|(e<<24|e>>>8)&4278255360;c[(b+64>>>9<<4)+14]=(a<<8|a>>>24)&16711935|(a<<24|a>>>8)&4278255360;d.sigBytes=4*(c.length+1);this._process();d=this._hash;c=d.words;for(a=0;4>a;a++)b=c[a],c[a]=(b<<8|b>>>24)&16711935|(b<<24|b>>>8)&4278255360;return d},clone:function(){var a=i.clone.call(this);a._hash=this._hash.clone();return a}});h.MD5=i._createHelper(g);h.HmacMD5=i._createHmacHelper(g)})(Math);return a.MD5(b).toString()};bizagi.util.stripslashes=function(a){return a.replace(/\\'/g,"'").replace(/\"/g,'"').replace(/\\\\/g,"\\").replace(/\\0/g,"\0")};bizagi.getTimeAgo=function(a){return $.timeago(new Date(a))};bizagi.util.replaceAllGlobalRegExp=function(d,b,c){var a=d,e=new RegExp(b,"g");a=a.replace(e,c);return a.toString()};bizagi.util.dispose=function(a){for(key in a){if(typeof a[key]=="object")delete a[key];if(typeof a[key]=="function"){a[key]=null;delete a[key]}}};bizagi.getGroupedData=function(d){var a={},c=new Date,h=c.getFullYear(),g=c.getMonth(),e=c.getDate();d=bizagi.util.isDate(d)?d:c;var b=new Date(d),j=b.getMonth(),k=b.getFullYear(),f=b.getDate(),i=new Date(k,j,f,0,0,0)-new Date(h,g,e,0,0,0);if(i<0){a.date=bizagi.util.dateFormatter.formatDate(new Date(b),"dd MMM");a.type=bizagi.localization.getResource("workportal-taskfeed-overdue");a.status="red"}else if(e==f){a.date=bizagi.util.dateFormatter.formatDate(new Date(b),"hh:mm");a.type=bizagi.localization.getResource("workportal-taskfeed-today");a.status="yellow"}else if(e+1==f){a.date=bizagi.util.dateFormatter.formatDate(new Date(b),"hh:mm");a.type=bizagi.localization.getResource("workportal-taskfeed-tomorrow");a.status="yellow"}else{a.date=bizagi.util.dateFormatter.formatDate(new Date(b),"dd MMM");a.type=bizagi.localization.getResource("workportal-taskfeed-upcomming");a.status="green"}return a};bizagi.util.getTaskStatus=function(k){var c=new Date,b=c.getMonth(),d=c.getDate(),h=c.getFullYear(),a=k||"";if(!bizagi.util.isDate(a))return"--";var f=new Date(a),g=f.getDate(),e=f.getMonth(),j=f.getFullYear(),i=new Date(j,e,g,0,0,0)-new Date(h,b,d,0,0,0);return i<0?bizagi.util.dateFormatter.formatDate(new Date(a),"dd MMM"):b===e&&d===g?bizagi.util.dateFormatter.formatDate(new Date(a),"hh:mm"):b===e&&d+1===g?bizagi.util.dateFormatter.formatDate(new Date(a),"hh:mm"):bizagi.util.dateFormatter.formatDate(new Date(a),"dd MMM")};bizagi.util.unicode2htmlencode=function(a){var f="charCodeAt";a=a.replace(/\r\n/g,"\n");for(var e,d,g=a.length,c="",b=0;b<g;b++){e=a[f](b);d=e.toString(16);c+="&#x"+d+";"}return c};bizagi.util.getHeadElement=function(){var a=document.getElementsByTagName("head")[0],b=document&&document.documentElement;if(!a){a=document.createElement("head");b.insertBefore(a,b.firstChild)}bizagi.util.getHeadElement=function(){return a};return a};bizagi.util.loadStyle=function(b){if(b){var a=document.createElement("style");a.type="text/css";if(a.styleSheet)a.styleSheet.cssText=b;else(function(b){if(a.childNodes.length)a.firstChild.nodeValue!==b.nodeValue&&a.replaceChild(b,a.firstChild);else a.appendChild(b)})(document.createTextNode(b));bizagi.util.getHeadElement().appendChild(a)}};bizagi.util.mobility.startLoading=function(){bizagi.util.mobility.showLoading()};bizagi.util.mobility.stopLoading=function(){bizagi.util.mobility.hideLoading()};bizagi.util.mobility.showLoading=function(b){var a=this;a.loaderInstanceVisible=false;a.loaderStopped=false;a.errorMessageShown=false;b=b||{};$(".km-loader").css("display","none");(bizagi.kendoMobileApplication||b.context)&&kendo.ui.progress($(".km-flat"),true);for(var c=0,e=$(".k-loading-mask").length;c<e;c++){var d=$(".k-loading-mask")[c];if($(d).css("display")!=="none"){a.loaderInstanceVisible=true;break}}!a.loaderInstanceVisible&&$($(".k-loading-mask")[0]).css("display","block");setTimeout(function(){if(!a.loaderStopped&&!a.errorMessageShown){bizagi.util.mobility.hideLoading();console.log(bizagi.localization.getResource("workportal-general-error-connection"));a.errorMessageShown=true}},2e4)};bizagi.util.mobility.hideLoading=function(a){var b=this;a=a||{};b.loaderStopped=true;if(bizagi.kendoMobileApplication||a.context){$(".km-loader").css("display","none");kendo.ui.progress($(".km-flat"),false)}};bizagi.util.showNotification=function(b){b=b||{};var a=$('<div id="sync-notification" class="bz-cm-icon"></div>').text(b.text||"");a.appendTo($("body"));typeof bizagi.kendoMobileApplication!=="undefined"&&bizagi.kendoMobileApplication.hideLoading();a.fadeIn(1500,function(){a.stop()});a.fadeOut(2e3,function(){a.remove()})};bizagi.util.isModeOnline=function(){return typeof bizagi.context.isOfflineForm!=="undefined"&&!bizagi.context.isOfflineForm};bizagi.util.hasOfflineForm=function(){return typeof BIZAGI_ENABLE_OFFLINE_FORMS!="undefined"&&BIZAGI_ENABLE_OFFLINE_FORMS==true};bizagi.util.formatDataType=function(c,a){var b="";if(a=="NText"||a=="VarChar")b="<div>"+bizagi.util.replaceLineBreak(c)+"</div>";else if(a=="Boolean")b=bizagi.util.formatBoolean(c);else if(a=="Int"||a=="BigInt"||a=="SmallInt"||a=="TinyInt")b=c;else if(a=="Money")b=bizagi.util.formatMonetaryCell(c);else if(a=="Float"||a=="Real")b=bizagi.util.formatDecimalCell(c);else if(a=="DateTime")b=bizagi.util.dateFormatter.formatDate(new Date(c),bizagi.localization.getResource("dateFormat")+" "+bizagi.localization.getResource("timeFormat"));else b=c;return b};bizagi.util.formatDateTime=function(b){b=b==null?"":b;b=typeof b!="string"?b.toString():b;if(Date.parse(b)){var a=new Date(b),c=(a.getMonth()+1<10?"0"+(a.getMonth()+1):a.getMonth()+1)+"/"+(a.getDate()<10?"0"+a.getDate():a.getDate())+"/"+a.getFullYear(),d=(a.getHours()<12&&b.toLowerCase().indexOf("pm")>0?a.getHours()+12:a.getHours())+":"+(a.getMinutes()<10?"0"+a.getMinutes():a.getMinutes());return c+" "+d}else return b};bizagi.util.formatMonetaryCell=function(a){return bizagi.util.formatMoney(a,BIZAGI_DEFAULT_CURRENCY_INFO)};bizagi.util.formatDecimalCell=function(d){var a,b={decimalSymbol:BIZAGI_DEFAULT_CURRENCY_INFO.decimalSeparator,digitGroupSymbol:BIZAGI_DEFAULT_CURRENCY_INFO.groupSeparator,roundToDecimalPlace:BIZAGI_DEFAULT_CURRENCY_INFO.decimalDigits},c={colorize:false,decimalSymbol:".",digitGroupSymbol:",",groupDigits:true,negativeFormat:"(%s%n)",positiveFormat:"%s%n",roundToDecimalPlace:"2",symbol:""};a=$.extend(c,b);return bizagi.util.formatDecimal(d,a)};bizagi.util.formatMoney=function(a,b){var d="",f="",e="",h="",j="";if(a!=undefined)a=a.toString();else a="0";if(a.length>=1){var g=a.indexOf(".");if(g>0){a=parseFloat(a).toFixed(b.decimalDigits);e=a.substring(0,g)}else{for(var k="0",c=0;c<b.decimalDigits;c++)j+=k;e=a.replace(/\D/g,"")}a=a.replace(/\D/g,"");if(b.decimalDigits>0)h=g>0?b.decimalSeparator+a.substring(a.length-b.decimalDigits,a.length):b.decimalSeparator+j;else h="";if(e.length>3){for(var i=0,c=e.length-1;c>=0;c--){if(i==3){f+=".";i=0}f+=e[c];i+=1}for(var c=f.length-1;c>=0;c--)d+=f[c]}else d=e;d=d.replaceAll(".",b.groupSeparator);d=b.symbol+d+h;return d}else return a};bizagi.util.formatDecimal=function(c,b){var a=$("<i/>").text(c);a.formatCurrency(b);return a.text()};bizagi.util.isTabletDevice=function(){var a=bizagi.util.detectDevice();return a=="tablet"||a=="tablet_ios"||a=="tablet_android"};bizagi.util.arrayEach=function(a,c){if(!!a)if(Object.prototype.toString.apply(a)==="[object Array]"&&Object.prototype.toString.apply(c)==="[object Function]"){var b,d;for(b in a)if(a.hasOwnProperty(b)){d=c.call(a[b],b,a[b]);if(d===false)return}}else $.each(a,c)};(function(a){a.fn.callInside=function(c,b){if(a(this)[0].tagName.toLowerCase()!="iframe")return null;if(a(this)[0].contentWindow==null)return null;try{a(this)[0].contentWindow.args=b}catch(d){return null}return a(this)[0].contentWindow.eval("args = window.args;insideFunction = "+c.toString()+"; insideFunction(args)")}})(jQuery);(function(a){a.fn.iframeAutoHeight=function(b){var b=a.extend({heightOffset:50},b);a(this).each(function(){if(a.browser&&(a.browser.safari||a.browser.opera)){var e=this,d=function(){c(e)};setTimeout(d,0);var f=a(this).attr("src");a(this).attr("src","");a(this).attr("src",f)}else c(this);function c(a){var c=a.offsetHeight+b.heightOffset;a.style.height=c+"px"}})}})(jQuery);bizagi.util.inputTray=function(b){var a="inbox";if(bizagi.util.isTabletDevice()&&bizagi.util.hasOfflineForm()){a=bizagi.util.getItemLocalStorage("inputtray");return a==null?b?"inbox":"true":!b&&a=="inbox"?"true":a}return a};bizagi.util.encodeHtml=function(a){return a===undefined||a===null?"":typeof a=="string"?a.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;"):a};bizagi.util.hasScroll=function(a,b){if(a.length)a=a[0];b=b==="vertical"?"scrollTop":"scrollLeft";var c=!!a[b];if(!c){a[b]=1;c=!!a[b];a[b]=0}return c};bizagi.util.syntaxHighlight=function(a){if(typeof a=="undefined"||a==null)return"";a=a.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");return a.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,function(a){var b="number";if(/^"/.test(a))if(/:$/.test(a))b="key";else b="string";else if(/true|false/.test(a))b="boolean";else if(/null/.test(a))b="null";return"<span class='"+b+"'>"+a+"</span>"})};bizagi.util.b64EncodeUnicode=function(a){return btoa(encodeURIComponent(a).replace(/%([0-9A-F]{2})/g,function(b,a){return String.fromCharCode("0x"+a)}))};
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\tablet\js\bizagi.workportal.util.tablet.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.util=typeof bizagi.util!=="undefined"?bizagi.util:{};bizagi.util.tablet=typeof bizagi.util.tablet!=="undefined"?bizagi.util.tablet:{};bizagi.util.mobility=typeof bizagi.util.mobility!=="undefined"?bizagi.util.mobility:{};bizagi.environment=typeof BIZAGI_ENVIRONMENT!=="undefined"?BIZAGI_ENVIRONMENT:queryString.environment||"debug";bizagi.util.tablet.startLoading=function(){bizagi.util.mobility.showLoading()};bizagi.util.tablet.stopLoading=function(){bizagi.util.mobility.hideLoading()};bizagi.util.tablet.startkendo=function(){if(typeof bizagi.kendoMobileApplication==="undefined"){bizagi.kendoMobileApplication=new kendo.mobile.Application($("body"),{transition:"slide",skin:"flat",initial:"initKendo",init:function(){bizagi.util.mobility.showLoading({context:this})}});bizagi.kendoMobileApplication.bizagiNavigate=function(a,b){this.navigate(a,b)}}};
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi\js\bizagi.logging.mobile.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.chrono=typeof bizagi.chrono!=="undefined"?bizagi.chrono:{};bizagi.enableTrace=true;bizagi.enableDebug=false;bizagi.enableChrono=false;bizagi.log=function(c,a,h){var d=window.document;if(!bizagi.enableTrace)return;if(bizagi.logContainer===undefined){bizagi.logContainer=$(".ui-bizagi-log-container",d);if(bizagi.logContainer.length===0){bizagi.logContainer=f();bizagi.util.removeItemLocalStorage("bizagi.mobility.trace");bizagi.util.setItemLocalStorage("bizagi.mobility.trace",[])}}if(a)try{a=JSON.parse(JSON.encode(a))}catch(i){a={}}c=c||"";var b=c.replace(BIZAGI_PATH_TO_BASE,"");bizagi.logContainer.bizagi_notifications("addNotification",b,a,g(h));if(b.indexOf("Rest/Multilanguage/Client")===-1){var e=bizagi.util.getItemLocalStorage("bizagi.mobility.trace")?JSON.parse(bizagi.util.getItemLocalStorage("bizagi.mobility.trace")):[];e.push([b,a]);bizagi.util.setItemLocalStorage("bizagi.mobility.trace",JSON.stringify(e))}function f(){var a=$("<div />").appendTo($("body",d)),b=bizagi.util.detectDevice();a.bizagi_notifications({headerAdditionalClass:"ui-bizagi-log-container-header",containerAdditionalClass:"ui-bizagi-notifications-container ui-bizagi-log-container",itemIcon:"ui-icon-info",title:"Debugging Log",location:"top",device:b,sendEnabled:typeof cordova!=="undefined"});a.bind("itemClick",function(f,a){$(".bz-render-popup-log").remove();if(!a.data||bizagi.util.isEmpty(a.data))return;var c=$("<div class='bz-render-popup-log' />").appendTo($("body"),d),b=$("<pre class='bz-render-popup-json' readonly=true />");b.height("90%");b.width("100%");var e=bizagi.util.syntaxHighlight(typeof a.data=="string"?a.data:JSON.stringify(a.data,null,2));b.html(e);c.html(b);c.dialog({width:640,height:480,modal:true,title:"Object Hierarchy",close:function(){c.dialog("destroy");c.detach()}})});return a}function g(a){a=a||"bz-workonit default";return a==="info"?"bz-information-icon "+a:a==="warning"?"bz-warning-icon "+a:a==="error"?"bz-minus "+a:a==="timer"?"bz-counterclock-input "+a:a==="success"?"bz-check-symbol "+a:a}};bizagi.assert=function(a,b,c){!a&&bizagi.log(b,c,"error")};bizagi.logError=function(a,b){bizagi.log(a,b,"error")};bizagi.debug=function(a,b){bizagi.enableDebug&&bizagi.log(a,b,"info")};bizagi.getLogInstance=function(){return bizagi.logContainer};bizagi.chrono.init=function(a){if(!a)bizagi.chronos={};else bizagi.chronos[a]={millis:0,timestamp:0}};bizagi.chrono.start=function(a,b){if(!bizagi.enableChrono)return;b=b!==undefined?b:false;bizagi.chronos[a]=bizagi.chronos[a]||{millis:0,timestamp:0};if(bizagi.chronos[a].running)return;bizagi.chronos[a].timestamp=(new Date).getTime();bizagi.chronos[a].profiling=bizagi.enableProfiler&&b;bizagi.chronos[a].running=true;if(bizagi.enableProfiler&&b){console.time(a);console.timeStamp(a);console.profile(a)}};bizagi.chrono.initAndStart=function(a){bizagi.chrono.init(a);bizagi.chrono.start(a)};bizagi.chrono.stop=function(a){if(!bizagi.enableChrono)return;bizagi.chronos[a]=bizagi.chronos[a]||{millis:0,timestamp:0};if(!bizagi.chronos[a].running)return;var b=(new Date).getTime()-bizagi.chronos[a].timestamp;bizagi.chronos[a].millis=bizagi.chronos[a].millis+b;bizagi.chronos[a].running=false;if(bizagi.chronos[a].profiling){console.timeEnd(a);console.timeStamp(a+"end");console.profileEnd(a)}};bizagi.chrono.stopAndLog=function(a){bizagi.chronos[a]&&bizagi.chronos[a].running&&bizagi.chrono.stop(a);bizagi.chrono.log(a)};bizagi.chrono.log=function(a){if(!bizagi.chronos[a])return;if(!bizagi.enableChrono)return;console.log("Timer "+a+": "+bizagi.chronos[a].millis+"ms")};bizagi.chrono.logTimers=function(){if(!bizagi.enableChrono)return;for(key in bizagi.chronos)bizagi.chrono.log(key)};bizagi.chrono.init();
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\common\bizagi\js\bizagi.js */ 
if(typeof Windows!="undefined"){jQuery.fn.nonSecureAppend=jQuery.fn.append;jQuery.fn.append=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.fn.nonSecureAppend.apply(c,a)});return b};jQuery.fn.nonSecureBefore=jQuery.fn.before;jQuery.fn.before=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.fn.nonSecureBefore.apply(c,a)});return b};jQuery.fn.nonSecureAfter=jQuery.fn.after;jQuery.fn.after=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.fn.nonSecureAfter.apply(c,a)});return b};jQuery.nonSecureClean=jQuery.clean;jQuery.clean=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.nonSecureClean.apply(c,a)});return b};jQuery.nonSecurereplaceWith=jQuery.fn.replaceWith;jQuery.fn.replaceWith=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.nonSecurereplaceWith.apply(c,a)});return b};jQuery.nonSecureinsertBefore=jQuery.fn.insertBefore;jQuery.fn.insertBefore=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.nonSecureinsertBefore.apply(c,a)});return b};jQuery.nonSecurebuildFragment=jQuery.buildFragment;jQuery.buildFragment=function(){var a=arguments,c=this,b=MSApp.execUnsafeLocalFunction(function(){return jQuery.nonSecurebuildFragment.apply(c,a)});return b}}$.expr.cacheLength=1;function initBizagi(){if(bizagi.loader.initialized==false){setTimeout(initBizagi,100);return}bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.enableCustomizations=typeof BIZAGI_ENABLE_CUSTOMIZATIONS!=="undefined"?BIZAGI_ENABLE_CUSTOMIZATIONS:true;bizagi.enableTrace=typeof BIZAGI_ENABLE_LOG!=="undefined"?BIZAGI_ENABLE_LOG:true;bizagi.enableDebug=typeof BIZAGI_ENABLE_LOG!=="undefined"?BIZAGI_ENABLE_LOG:false;bizagi.enableChrono=typeof BIZAGI_ENABLE_LOG!=="undefined"?BIZAGI_ENABLE_LOG:true;bizagi.enableProfiler=typeof BIZAGI_ENABLE_PROFILER!=="undefined"?BIZAGI_ENABLE_PROFILER:bizagi.readQueryString().enableProfiler==="true"?true:false;bizagi.services.ajax.logSuccess=typeof BIZAGI_ENABLE_LOG!=="undefined"?BIZAGI_ENABLE_LOG:true;bizagi.services.ajax.logSubmits=typeof BIZAGI_ENABLE_LOG!=="undefined"?BIZAGI_ENABLE_LOG:true;bizagi.proxyPrefix=typeof BIZAGI_PROXY_PREFIX!=="undefined"?BIZAGI_PROXY_PREFIX:"";bizagi.UserPreferencesUrl=typeof BIZAGI_USER_PREFERENCES_PAGE!=="undefined"?BIZAGI_USER_PREFERENCES_PAGE:"";BIZAGI_PATH_TO_BASE=typeof BIZAGI_PATH_TO_BASE!=="undefined"?BIZAGI_PATH_TO_BASE:"";bizagi.resourceDefinitionLocation={custom:bizagi.loader.getResource("l10n","bizagi.custom.resources"),"default":bizagi.loader.getResource("l10n","bizagi.resources.default"),"en-us":bizagi.loader.getResource("l10n","bizagi.resources.en-us"),es:bizagi.loader.getResource("l10n","bizagi.resources.es"),"es-es":bizagi.loader.getResource("l10n","bizagi.resources.es-es"),pt:bizagi.loader.getResource("l10n","bizagi.resources.pt"),"pt-br":bizagi.loader.getResource("l10n","bizagi.resources.pt-br"),de:bizagi.loader.getResource("l10n","bizagi.resources.de"),fr:bizagi.loader.getResource("l10n","bizagi.resources.fr"),ru:bizagi.loader.getResource("l10n","bizagi.resources.ru"),nl:bizagi.loader.getResource("l10n","bizagi.resources.nl"),pl:bizagi.loader.getResource("l10n","bizagi.resources.pl"),it:bizagi.loader.getResource("l10n","bizagi.resources.it"),ja:bizagi.loader.getResource("l10n","bizagi.resources.ja"),zh:bizagi.loader.getResource("l10n","bizagi.resources.zh"),"cs-cz":bizagi.loader.getResource("l10n","bizagi.resources.cs-cz"),cs:bizagi.loader.getResource("l10n","bizagi.resources.cs-cz")};var a=bizagi.readQueryString().language;a=a||(typeof BIZAGI_LANGUAGE!=="undefined"?BIZAGI_LANGUAGE:null);a=a||"default";bizagi.language=a;bizagi.localization=new bizagi.l10n(bizagi.resourceDefinitionLocation);bizagi.localization.setLanguage(bizagi.language);bizagi.templateService=new bizagi.templates.services.service(bizagi.localization);bizagi.util.isIE&&bizagi.util.isIE()&&bizagi.loader.loadFile({src:bizagi.getStyleSheet("common.ie7"),type:"css"});window.onerror=function(){bizagi.logError("Uncaught error",{message:arguments[0],file:arguments[1],line:arguments[2]})}}initBizagi();if(bizagi.util.isIE8&&bizagi.util.isIE8()){if(!Array.isArray)Array.isArray=function(a){return Object.prototype.toString.call(a)==="[object Array]"||a instanceof Array};if(!Array.prototype.forEach)Array.prototype.forEach=function(c,b){for(var d=this.length>>>0,a=0;a<d;a++)a in this&&c.call(b,this[a],a,this)};if(!Array.prototype.map)Array.prototype.map=function(e){for(var b=this.length>>>0,c=new Array(b),d=arguments[1],a=0;a<b;a++)if(a in this)c[a]=e.call(d,this[a],a,this);return c};if(!Array.prototype.filter)Array.prototype.filter=function(c){for(var b=[],d=arguments[1],a=0;a<this.length;a++)c.call(d,this[a])&&b.push(this[a]);return b};if(!Array.prototype.reduce)Array.prototype.reduce=function(d){var c=this.length>>>0,a=0;if(c===0&&arguments.length===1)throw new TypeError;if(arguments.length>=2)var b=arguments[1];else do{if(a in this){b=this[a++];break}if(++a>=c)throw new TypeError;}while(true);for(;a<c;a++)if(a in this)b=d.call(null,b,this[a],a,this);return b};if(!Array.prototype.indexOf)Array.prototype.indexOf=function(c){var b=this.length,a=arguments[1]||0;if(!b)return-1;if(a>=b)return-1;if(a<0)a+=b;for(;a<b;a++){if(!Object.prototype.hasOwnProperty.call(this,a))continue;if(c===this[a])return a}return-1};if(!Object.keys)Object.keys=function(a){var b=[];for(var c in a)Object.prototype.hasOwnProperty.call(a,c)&&b.push(c);return b}};
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\bizagi.workportal.webpart.controllers.controller.js */ 
$.Class.extend("bizagi.workportal.controllers.controller",{},{init:function(a,b,c){this.workportalFacade=a;this.dataService=b;this.resources=bizagi.localization;this.container=c.container||document;this.componentContainers={};bizagi.workportal.controllers.instances={};bizagi.workportal.controllers.instances.controller=this},getWorkportal:function(){return this.workportalFacade.workportal},publish:function(a,b){return $(this.container).triggerHandler(a,b)},subscribe:function(a,b){$(this.container).bind(a,b)},subscribeEvent:function(a,b){$(document).bind(a,b)},subscribeOneTime:function(a,b){$(this.container).one(a,b)},unsubscribe:function(a,b){if(b)$(this.container).unbind(a,b);else $(this.container).unbind(a)},render:function(){alert("controllers render");var a=this,b=new $.Deferred;$.when(a.renderContent()).done(function(){a.postRender();b.resolve(a.getContent())});return b.promise()},getContent:function(){return this.content},getResource:function(a){return this.resources.getResource(a)},renderContent:function(){return""},postRender:function(){}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\bizagi.workportal.services.context.js */ 
$.Class.extend("bizagi.workportal.services.context",{},{init:function(a){this.context=a.context;this.endPoints=bizagi.workportal.services.getEndPoints(a);this.proxyPrefix=!bizagi.util.isEmpty(a.proxyPrefix)?a.proxyPrefix:""},getUrl:function(a){typeof this.endPoints[a]==="undefined"&&bizagi.log("Url not found","No endpoint defined for: "+a);return this.endPoints[a]}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\bizagi.workportal.services.endPoints.js */ 
bizagi.workportal=typeof bizagi.workportal!=="undefined"?bizagi.workportal:{};bizagi.workportal.services=typeof bizagi.workportal.services!=="undefined"?bizagi.workportal.services:{};bizagi.workportal.services.endPoints=[];bizagi.workportal.services.getEndPoints=function(a){return a.context=="workportal"?{"user-handler":a.proxyPrefix+"RestServices/UserHandler.ashx","process-handler":a.proxyPrefix+"RestServices/ProcessHandler.ashx","query-handler":a.proxyPrefix+"Rest/Handlers/Query","case-handler":a.proxyPrefix+"RestServices/CaseHandler.ashx","query-form":a.proxyPrefix+"App/ListaDetalle/QueryForm.aspx","query-form-edit":a.proxyPrefix+"App/ListaDetalle/SaveQuery.aspx","query-form-delete":a.proxyPrefix+"App/ListaDetalle/QueryForm.aspx","query-form-delete-cube":a.proxyPrefix+"Rest/Queries/Cubes/{idCube}","favorites-handler":a.proxyPrefix+"RestServices/FavoritesHandler.ashx","file-handler":a.proxyPrefix+"RestServices/EntityHandler.ashx","login-handler":a.proxyPrefix+"Rest/Users/UserAuthentication","login-handlerv10":a.proxyPrefix+"RestServices/UserHandler.ashx?action=authenticateUser&userName={0}&password={1}&domain={2}","logoff-handler":a.proxyPrefix+"RestServices/UserHandler.ashx?action=logOff","logoff-handlerv1":a.proxyPrefix+"Rest/Users/LogOff","authorization-handler-getMenuAuthorization":a.proxyPrefix+"Rest/Authorization/MenuAuthorization","authorization-handler-isCaseCreationAuthorized":a.proxyPrefix+"Rest/Authorization/Processes/{0}/IsCaseCreationAuthorized","case-handler-getCaseSummary":a.proxyPrefix+"Rest/Cases/{idCase}/Summary","case-handler-releaseActivity":a.proxyPrefix+"Rest/Cases/{idCase}/ReleaseActivity","case-handler-getCasesList":a.proxyPrefix+"Rest/Processes/SearchCases","offline-getProcessTree":a.proxyPrefix+"Rest/Processes/OfflineProcessTree","offline-getForms":a.proxyPrefix+"Rest/RenderForm/offlineForms","offline-sendForm":a.proxyPrefix+"Rest/Cases/SaveAsynchWorkItemOffLine","case-handler-getCaseTasks":a.proxyPrefix+"Rest/Cases/{idCase}/Tasks","case-handler-getCaseEvents":a.proxyPrefix+"Rest/Cases/{idCase}/Events","case-handler-getCaseSubprocesses":a.proxyPrefix+"Rest/Cases/{idCase}/Subprocesses","case-handler-getTaskAssignees":a.proxyPrefix+"Rest/Cases/{idCase}/Tasks/{idTask}/Assignees","case-handler-getCaseAssignees":a.proxyPrefix+"Rest/Cases/{idCase}/Assignees","case-handler-getWorkItems":a.proxyPrefix+"Rest/Cases/{idCase}/WorkItems","case-handler-getAsynchExecutionState":a.proxyPrefix+"Rest/Cases/{idCase}/AsynchExecutionState","case-handler-addNewCase":a.proxyPrefix+"Rest/Cases","case-handler-supportedLogTypes":a.proxyPrefix+"Rest/Cases/SupportedLogsTypes","case-handler-getActivityLog":a.proxyPrefix+"Rest/Cases/{idCase}/ActivityLog","case-handler-getActivityDetailLog":a.proxyPrefix+"Rest/Cases/{idCase}/{idWorkItemFrom}/ActivityDetailLog","case-handler-getEntityLog":a.proxyPrefix+"Rest/Cases/{idCase}/EntityLog","case-handler-getEntityDetailLog":a.proxyPrefix+"Rest/Cases/{idCase}/EntityDetailLog","case-handler-getUserLog":a.proxyPrefix+"Rest/Cases/{idCase}/UserLog","case-handler-getUserDetailLog":a.proxyPrefix+"Rest/Cases/{idCase}/{idUser}/UserDetailLog","case-handler-getAdminLog":a.proxyPrefix+"Rest/Cases/{idCase}/AdminLog","case-handler-getCaseFormsRenderVersion":a.proxyPrefix+"Rest/Cases/{idCase}/FormsRenderVersion","favorites-handler-saveFavorite":a.proxyPrefix+"Rest/Favorites","favorites-handler-deleteFavorite":a.proxyPrefix+"Rest/Favorites/{guidFavorite}","bamAnalytics-handler-getAnalisysQueries":a.proxyPrefix+"Rest/BAMAnalytics/AnalisysQueries","bamAnalytics-handler-updateQuery":a.proxyPrefix+"Rest/BAMAnalytics/Reports/Ids/{idQuery}","inbox-handler-getInboxSummary":a.proxyPrefix+"Rest/Inbox/Summary","MessageHandler-NewComment":a.proxyPrefix+"Rest/Cases/Comments","MessageHandler-GetComments":a.proxyPrefix+"Rest/Cases/Comments","MessageHandler-SetCategoryToComment":a.proxyPrefix+"Rest/Cases/Comments/{idComment}","MessageHandler-RemoveComment":a.proxyPrefix+"Rest/Cases/Comments/{idComment}","MessageHandler-ReplyComment":a.proxyPrefix+"Rest/Cases/Comments/{idComment}/Replies","MessageHandler-RemoveReply":a.proxyPrefix+"Rest/Cases/Comments/{idComment}/Replies/{idReply}","MessageHandler-GetCategoryColors":a.proxyPrefix+"Rest/Cases/Comments/CategoryColors","MessageHandler-RenameCategoryColor":a.proxyPrefix+"Rest/Cases/Comments/CategoryColors/{idColorCategory}","MessageHandler-CountNewComments":a.proxyPrefix+"Rest/Cases/Comments/{idComment}/NewComments/Count","process-handler-getAllProcesses":a.proxyPrefix+"Rest/Processes","process-handler-getCustomizedColumnsData":a.proxyPrefix+"Rest/Processes/CustomizedColumnsData","process-handler-getCategory":a.proxyPrefix+"Rest/Processes/Categories","process-handler-getRecentProcesses":a.proxyPrefix+"Rest/Processes/RecentProcesses","process-handler-getCustomizedColumnsDataInfo":a.proxyPrefix+"Rest/Processes/CustomizedColumnsDataInfo","process-handler-getOrganizations":a.proxyPrefix+"Rest/Profile/Organizations","query-handler-GETQUERIES":a.proxyPrefix+"Rest/Queries","user-handler-getCurrentUser":a.proxyPrefix+"Rest/Users/CurrentUser","old-render":a.proxyPrefix+"App/ListaDetalle/Detalle.aspx",Reports:a.proxyPrefix+"RestServices/BAMAnalyticsHandler.ashx","reports-handler-deleteQueries":a.proxyPrefix+"Rest/BAMAnalytics/Reports/{QueryId}","folders-handler-getUserQueries":a.proxyPrefix+"Rest/SmartFolders","folders-associate-deleteSmartFolder":a.proxyPrefix+"Rest/SmartFolders/{idSmartFolder}","folders-handler":a.proxyPrefix+"RestServices/SmartFoldersHandler.ashx","folders-associate":a.proxyPrefix+"App/Ajax/AJAXGateway.aspx","smartfolders-integration":a.proxyPrefix+"App/WorkPortal/ConfigureFilteredFolder.aspx",AlarmAdmin:a.proxyPrefix+"App/Admin/AlarmsAdmin.aspx",AnalyticsProcess:a.proxyPrefix+"App/Cockpit/AnalyticsProcess.aspx",AnalyticsSensor:a.proxyPrefix+"App/Cockpit/AnalyticsSensor.aspx",AnalyticsTask:a.proxyPrefix+"App/Cockpit/AnalyticsTask.aspx",AsynchronousWorkitemRetries:a.proxyPrefix+"App/Admin/AsynchDisabledWorkitems.aspx",AuthenticationLogQuery:a.proxyPrefix+"App/Admin/AuthLogQuery.aspx",BAMProcess:a.proxyPrefix+"App/Cockpit/BAMProcess.aspx",BAMTask:a.proxyPrefix+"App/Cockpit/BAMTask.aspx",BusinessPolicies:a.proxyPrefix+"App/Admin/BusinessPolicies/BusinessPoliciesSelector.aspx",CaseAdmin:a.proxyPrefix+"App/Admin/CaseSearch.aspx",adminReassignCases:"CaseAdmin",asyncECMUpload:"asyncECMUpload",CasesMonitor:a.proxyPrefix+"App/Admin/CasesMonitor.aspx",Closed:a.proxyPrefix+"App/ListaDetalle/listaitems.aspx?h_Location=Cerrados&I_ProcessState=Completed",CurrentUser:a.proxyPrefix+"App/Admin/CurrentUser.aspx",EncryptionAdmin:a.proxyPrefix+"App/Admin/Encrypt.aspx",MobileUpdatesAdmin:a.proxyPrefix+"App/MobileUpdates/default.aspx",EntityAdmin:a.proxyPrefix+"App/Admin/Entity.aspx",LocationResources:a.proxyPrefix+"App/Admin/AdminLocResources.aspx",NewCase:a.proxyPrefix+"App/Radicar/application.aspx",Pending:a.proxyPrefix+"App/ListaDetalle/listaitems.aspx?h_Location=Pendientes&I_processState=Running",Profiles:a.proxyPrefix+"App/Admin/ProfilesAdminSearch.aspx",Search:a.proxyPrefix+"App/ListaDetalle/Search.aspx",UserAdmin:a.proxyPrefix+"App/Admin/ListUsers.aspx",UserDefaultAssignation:a.proxyPrefix+"App/Admin/DefaultAssignationUser.aspx?h_AdminDefaultAssign=1",UserPendingRequests:a.proxyPrefix+"App/Admin/UserPendingRequests.aspx",ListPreferences:bizagi.UserPreferencesUrl!==""?a.proxyPrefix+bizagi.UserPreferencesUrl:a.proxyPrefix+"App/Admin/CurrentUser.aspx",GRDimensionAdmin:a.proxyPrefix+"App/Cockpit/DimensionEdit.aspx",Licenses:a.proxyPrefix+"App/Admin/Licenses.aspx",AnalysisQueries:a.proxyPrefix+"App/Inicio/WPAnalysisQuery.aspx",DocumentTemplates:a.proxyPrefix+"App/Admin/AdminDocumentTemplates.aspx",ProcessAdmin:a.proxyPrefix+"App/Admin/AdminProcess.aspx",ResourceBAM:a.proxyPrefix+"App/Cockpit/BAMResourceMonitor.aspx",WorkPortalVersion:a.proxyPrefix+"Rest/Util/Version","entities-administration":"RestServices/EntityHandler.ashx","theme-handler-getLogoImagePath":a.proxyPrefix+"Rest/Theme/LogoImage",logout:a.proxyPrefix+"Rest/Authentication/logout",logoutDotNet:a.proxyPrefix+"App/Inicio/LogOff.aspx","massive-activity-assignments-getOrganizationInfo":a.proxyPrefix+"Rest/Users/OrganizationInfo","massive-activity-assignments-getCasesByOrganization":a.proxyPrefix+"Rest/Queries/GetCasesByOrganization","massive-activity-assignments-reassignCases":a.proxyPrefix+"Rest/Cases/ReassignCases","massive-activity-assignments-searchUsers":a.proxyPrefix+"Rest/Users/","async-ecm-upload-baseService":a.proxyPrefix+"Rest/Handlers/Metadata",domains:a.proxyPrefix+"Rest/Authentication/Domains",getCurrentTheme:a.proxyPrefix+"Rest/Theme/Current","admin-getAuthenticationLog":a.proxyPrefix+"Rest/Users/AuthLog","admin-getAuthenticationDomains":a.proxyPrefix+"Rest/Users/Domains","admin-getAuthenticationEventsTypes":a.proxyPrefix+"Rest/Users/EventTypes","admin-getAuthenticationEventSubTypes":a.proxyPrefix+"Rest/Users/EventSubTypes","admin-EncryptString":a.proxyPrefix+"Rest/Util/EncryptString","admin-UserPendingRequests":a.proxyPrefix+"Rest/Users/UserPendingRequests","admin-UserAuthenticationInfo":a.proxyPrefix+"Rest/Users/UserAuthenticationInfo","admin-updateUserAuthenticationInfo":a.proxyPrefix+"Rest/Users/UpdateUserAuthenticationInfo","admin-generateRandomPassword":a.proxyPrefix+"Rest/Util/GenerateRandomPassword","admin-GenerateDataToSendByEmail":a.proxyPrefix+"Rest/Users/GenerateDataToSendByEmail","admin-sendEmail":a.proxyPrefix+"Rest/Util/SendEmail/","admin-getApplicationList":a.proxyPrefix+"Rest/Application","admin-getApplicationProcesses":a.proxyPrefix+"Rest/Application/{idApp}/Process","admin-getProcessVersion":a.proxyPrefix+"Rest/Processes/Version","admin-getProcessTasks":a.proxyPrefix+"Rest/Processes/Version/{version}/Tasks","admin-getTaskAlarms":a.proxyPrefix+"Rest/Alarm/TaskAlarms","admin-getLapseMode":a.proxyPrefix+"Rest/Alarm/LapseMode","admin-getRecurrMode":a.proxyPrefix+"Rest/Alarm/RecurrMode","admin-getScheduleType":a.proxyPrefix+"Rest/Alarm/ScheduleType","admin-getBossList":a.proxyPrefix+"Rest/Alarm/Boss","admin-addAlarm":a.proxyPrefix+"Rest/Alarm","admin-editAlarm":a.proxyPrefix+"Rest/Alarm/{idAlarm}","admin-deleteAlarm":a.proxyPrefix+"Rest/Alarm","admin-alarmRecipients":a.proxyPrefix+"Rest/Alarm/{idAlarm}/Recipient","admin-deleteAlarmRecipients":a.proxyPrefix+"Rest/Alarm/Recipient","admin-recipientToAlarm":a.proxyPrefix+"Rest/Alarm/{idAlarm}/Recipient/{idRecipient}","admin-enableAlarm":a.proxyPrefix+"Rest/Alarm/Task/{idTask}/ToggleAlarm","admin-getCategoriesList":a.proxyPrefix+"Rest/Application/Category","admin-getCasesList":a.proxyPrefix+"Rest/Queries/SearchCases","admin-abortItems":a.proxyPrefix+"Rest/Cases/Abort","admin-reassignItems":a.proxyPrefix+"Rest/Cases/Reassign","admin-getDefaultAssignationUserToAllProcess":a.proxyPrefix+"Rest/Users/{serviceAction}/","admin-getDefaultAssignationUserToProcess":a.proxyPrefix+"Rest/Users/Process/{process}/AssignationUser","admin-setDefaultAssignationUserToProcess":a.proxyPrefix+"Rest/Users/Process/{process}/AssignationUser","admin-getProfilesTypes":a.proxyPrefix+"Rest/Profile/Types","admin-searchProfiles":a.proxyPrefix+"Rest/Profile","admin-getUsersByProfile":a.proxyPrefix+"Rest/Users/Profile/{type}/{id}","admin-removeUserFromProfile":a.proxyPrefix+"Rest/Profile/{type}/{id}/User/{idUser}/","admin-addUserToProfile":a.proxyPrefix+"Rest/Profile/{type}/{id}/User/{idUser}/","admin-async-activities-get-activities":a.proxyPrefix+"Rest/Cases/Asynchronous/Activities","admin-async-activities-get-retry-now":a.proxyPrefix+"Rest/Cases/{idCase}/Workitem/{idworkItem}/RetryNow","admin-async-activities-get-activities-by-task":a.proxyPrefix+"Rest/Cases/Asynchronous/Activities/Task","admin-async-activities-enable-execution":a.proxyPrefix+"Rest/Cases/Asynchronous/Enable","admin-async-activities-enable-multiple":a.proxyPrefix+"Rest/Cases/Asynchronous/EnableMultiple","admin-async-activities-async-execution":a.proxyPrefix+"Rest/Cases/{idCase}/Workitem/{idWorkitem}/AsynchExecution","admin-async-activities-async-execution-log":a.proxyPrefix+"Rest/Cases/{idCase}/Workitem/{idworkItem}/AsynchExecutionLog","admin-Licenses":a.proxyPrefix+"Rest/Licenses","admin-GetDimensions":a.proxyPrefix+"Rest/Dimensions","admin-EditDimension":a.proxyPrefix+"Rest/Dimensions/{id}","admin-CreateAdministrableDimension":a.proxyPrefix+"Rest/Dimensions/Administrable","admin-DeleteDimension":a.proxyPrefix+"Rest/Dimensions/{id}?isAdministrable={administrable}","admin-EntityPathChildNodesAction":a.proxyPrefix+"Rest/Dimensions/EntityChildNodes","admin-GetActiveWFClasses":a.proxyPrefix+"Rest/Dimensions/ActiveProcess","admin-document-templates-storeDocumentTemplates":a.proxyPrefix+"Rest/DocumentTemplates/StoreDocumentTemplates","admin-document-templates-restoreDocumentTemplates":a.proxyPrefix+"Rest/DocumentTemplates/RestoreDocumentTemplate","admin-document-templates-uploadDocumentTemplate":a.proxyPrefix+"Rest/DocumentTemplates/UploadDocumentTemplate","admin-processes-workflowClasses":a.proxyPrefix+"Rest/Processes/WorkflowClasses","admin-processes-tasksByWorkflow":a.proxyPrefix+"Rest/Processes/TasksByWorkflow","admin-processes-modifyProcessDuration":a.proxyPrefix+"Rest/Processes/{idWorkflow}/Duration/","admin-processes-modifyTaskDuration":a.proxyPrefix+"Rest/Processes/Task/{idTask}/Duration/","admin-language-resource":a.proxyPrefix+"Rest/Multilanguage/Resource","admin-language-languages":a.proxyPrefix+"Rest/Multilanguage/Languages","bam-resourcemonitor-myteam":a.proxyPrefix+"Rest/Reports/BAM/Resources/MyTeam","reports-analysisquery":a.proxyPrefix+"Rest/Reports/AnalysisQueries","reports-analysisquery-update":a.proxyPrefix+"Rest/Reports/AnalysisQueries","reports-analysisquery-delete":a.proxyPrefix+"Rest/Reports/AnalysisQueries","processviewer-processdefinition":a.proxyPrefix+"Rest/Reports/Components/ProcessDefinition","processviewer-processgraphicinfo":a.proxyPrefix+"Rest/Reports/Components/ProcessGraphicInfo","mobile-getLastUpdate":a.proxyPrefix+"Rest/Util/mobileUpdates"}:{}};
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\bizagi.workportal.services.service.js */ 
$.Class.extend("bizagi.workportal.services.service",{BA_CONTEXT_PARAMETER_PREFIX:"h_",ENTITIES_QUERY_PAGE_SIZE:10},{init:function(a){a=typeof a=="object"?a:{};a.context=!bizagi.util.isEmpty(a.context)?a.context:"workportal";a.proxyPrefix=!bizagi.util.isEmpty(a.proxyPrefix)?a.proxyPrefix:"";a.proxyPrefix=this.normalizeUrl(a.proxyPrefix);bizagi.RPproxyPrefix=a.proxyPrefix;a.sharepointProxyPrefix=!bizagi.util.isEmpty(a.sharepointProxyPrefix)?a.sharepointProxyPrefix:"";a.sharepointProxyPrefix=this.normalizeUrl(a.sharepointProxyPrefix);this.serviceLocator=new bizagi.workportal.services.context(a);this.pageSize=10;if(typeof BIZAGI_INBOX_ROWS_PER_PAGE!=="undefined")this.pageSize=$.isNumeric(BIZAGI_INBOX_ROWS_PER_PAGE)?BIZAGI_INBOX_ROWS_PER_PAGE:this.pageSize},getUrl:function(a){var b=this,c=b.serviceLocator.getUrl(a.endPoint);return c},normalizeUrl:function(a){if(a!==""){if(a[a.length-1]!="/")a=a.concat("/");if(a.indexOf("http://")==-1&&a.indexOf("https://")==-1)a="http://"+a}return a},getWPLogo:function(){var a=this;return $.ajax({url:a.serviceLocator.getUrl("theme-handler-getLogoImagePath"),dataType:"json",type:"GET"})},getCurrentTheme:function(){var a=this,b=a.serviceLocator.getUrl("getCurrentTheme");return $.read(b)},authenticatedUser:function(){var b=this,a=b.serviceLocator.getUrl("login-handler");return $.read({url:a,data:{userName:BIZAGI_USER,password:BIZAGI_PASSWORD,domain:BIZAGI_DOMAIN}}).pipe(function(a){return a},function(){a=b.serviceLocator.getUrl("login-handlerv10");a=a.replace("{0}",BIZAGI_USER||"");a=a.replace("{1}",BIZAGI_PASSWORD||"");a=a.replace("{2}",BIZAGI_DOMAIN||"");return $.ajax({url:a,type:"GET",dataType:"json"})})},logoutUser:function(){var b=this,a=b.serviceLocator.getUrl("logoff-handlerv1");return $.read({url:a})},logOffUser:function(){var b=this,a=b.serviceLocator.getUrl("logoff-handler");return $.read({url:a})},getCurrentUser:function(){var a=this;return $.read(a.serviceLocator.getUrl("user-handler-getCurrentUser"))},getInboxSummary:function(){var a=this;return $.read(a.serviceLocator.getUrl("inbox-handler-getInboxSummary")).pipe(function(a){return a.inboxSummary})},getAllProcesses:function(a){var d=this;a=a||{};var b={},c=d.serviceLocator.getUrl("process-handler-getAllProcesses");if(a.taskState!=undefined)b.taskState=a.taskState;if(a.onlyFavorites!=undefined&&a.onlyFavorites!="")b.onlyFavorites=a.onlyFavorites;if(a.smartfoldersParameters)c=c+"?"+a.smartfoldersParameters;if(bizagi.isMobile())b.mobileDevice=true;return $.read(c,b).pipe(function(e){if(!!a.skipAggrupate)return e;var d={},b={},c=bizagi.localization.getResource("workportal-widget-inbox-all-processes"),f={name:bizagi.localization.getResource("workportal-widget-inbox-all-cases"),path:"",category:c,isFavorite:a.onlyFavorites||false,guidFavorite:"",idworkflow:"",guidWFClass:"",count:0};b[c]=[];b[c].workflows=[];b[c].workflows.push(f);e.workflows&&$.each(e.workflows.workflow,function(c,a){if(!b[a.category]){b[a.category]=[];b[a.category].workflows=[]}b[a.category].workflows.push(a);f.count+=Number(a.count)});d.categories=[];for(key in b)d.categories.push({name:key,workflows:b[key].workflows});return d})},getCasesList:function(a){var c=this;a=a||{};var b={},d=c.serviceLocator.getUrl("case-handler-getCasesList");b.pageSize=a.pageSize||10;b.page=a.page||1;b.numberOfFields=a.numberOfFields||"-1";b.idWfClass=a.idWfClass||"-1";return $.read(d,b).pipe(function(c){for(var b={overdue:[],today:[],tomorrow:[],upcoming:[],page:c.page,totalPages:c.totalPages},d=new Date,i=d.getMonth(),e=d.getDate(),j=d.getYear(),k=typeof c.elements!=="undefined"?c.elements.length:0,f=0;k>f;f++){var a=c.elements[f],g=new Date(a[3]),h=g.getDate(),l=g.getMonth(),m=g.getYear();if(e>h||(i>l||j>m)){a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"dd MMM");b.overdue.push(a)}else if(e==h){a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"hh:mm");b.today.push(a)}else if(e+1==h){a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"hh:mm");b.tomorrow.push(a)}else{a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"dd MMM");b.upcoming.push(a)}}return b})},getCasesListBeta:function(a){var c=this;a=a||{};var b={},d=c.serviceLocator.getUrl("case-handler-getCasesList");if(typeof a.radNumber!=="undefined")b.radNumber=a.radNumber;b.pageSize=a.pageSize||10;b.page=a.page||1;b.numberOfFields=a.numberOfFields||"-1";b.idWfClass=a.idWfClass||"-1";return $.read(d,b).pipe(function(b){var c={elements:[],page:b.page,totalPages:b.totalPages},i=new Date,d=i.getMonth(),e=i.getDate(),r=i.getFullYear(),l=b.customTaskState||false;if(l)var m="1|"+(b.taskState.red[0]||bizagi.localization.getResource("workportal-taskfeed-overdue")),p="2|"+(b.taskState.yellow[0]||bizagi.localization.getResource("workportal-taskfeed-today")),q="3|"+(b.taskState.green[0]||bizagi.localization.getResource("workportal-taskfeed-upcomming"));for(var j=0;b.elements.length>j;j++){var a=b.elements[j];if(a[3]!=""){var k=new Date(a[3]),g=k.getDate(),f=k.getMonth(),s=k.getFullYear(),n=new Date(s,f,g,0,0,0)-new Date(r,d,e,0,0,0);if(n<0)a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"dd MMM");else if(d===f&&e==g)a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"hh:mm");else if(d===f&&e+1==g)a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"hh:mm");else a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"dd MMM")}else a[3]="";if(l){var o=a[7],h;switch(o){case"red":h=m;break;case"yellow":h=p;break;case"green":h=q;break;default:h=m}c.elements.push($.extend({},a,{group:h,state:o}))}else if(n<0)c.elements.push($.extend({},a,{group:"1|"+bizagi.localization.getResource("workportal-taskfeed-overdue"),state:"red"}));else if(d===f&&e==g)c.elements.push($.extend({},a,{group:"2|"+bizagi.localization.getResource("workportal-taskfeed-today"),state:"yellow"}));else if(d===f&&e+1==g)c.elements.push($.extend({},a,{group:"3|"+bizagi.localization.getResource("workportal-taskfeed-tomorrow"),state:"yellow"}));else c.elements.push($.extend({},a,{group:"4|"+bizagi.localization.getResource("workportal-taskfeed-upcomming"),state:"green"}))}return c})},getAllAdministrableEntities:function(b){var c=this;b=b||{};var a={};a.action="entitiesList";a.kind="administrable";return $.ajax({url:c.serviceLocator.getUrl("entities-administration"),data:a,type:"POST",dataType:"json"}).pipe(function(b){var a={entities:[]};$.each(b,function(c,b){a.entities.push(b)});return a})},getEntitiesList:function(a){var b=this;a=a||{};a.action="entitiesList";a.kind="entitiesData";return $.ajax({url:b.serviceLocator.getUrl("entities-administration"),data:a,type:"POST",dataType:"json"})},getCasesByWorkflow:function(a){var c=this;a=a||{};var b={},e=c.serviceLocator.getUrl("process-handler-getCustomizedColumnsDataInfo");if(!bizagi.util.isEmpty(a.taskState))b.taskState=a.taskState;if(!bizagi.util.isEmpty(a.idWorkflow))b.idWorkflow=a.idWorkflow;if(a.onlyFavorites==true){b.onlyFavorites=true;b.taskState="all"}b.pageSize=a.pageSize||c.pageSize;b.page=a.page||1;if(bizagi.isMobile())b.mobileDevice=true;if(a.smartfoldersParameters){var d=a.smartfoldersParameters.split("&");$.each(d,function(d,c){var a=c.split("=");b[a[0]]=a[1]})}if(a.radnumber||a.radNumber)b.radNumber=a.radNumber?a.radNumber:a.radnumber;if((a.idcase||a.idCase)&&bizagi.util.isNumeric(a.idcase||a.idCase))b.idCase=a.idCase?a.idCase:a.idcase;return $.read(e,b).pipe(function(a){$.each(a.cases.rows,function(b,d){var c=[];a.cases.rows[b].radnumber=d.id;$.each(d.fields,function(e,d){if(d!=undefined)try{if(d.isRadNumber!=undefined&&d.isRadNumber=="true"){a.cases.rows[b].radnumber=d.Value;a.cases.rows[b].casenumberdisplayname=d.DisplayName}else if(d.workitems!=undefined)a.cases.rows[b].workitems=d.workitems;else c.push(d)}catch(f){}});a.cases.rows[b].fields=c});return a.cases})},getOrganizationsList:function(){var b=this,c=b.serviceLocator.getUrl("process-handler-getOrganizations"),a={};a.type=12;a.name="";return $.read(c,a)},getCustomizedColumnsData:function(a){var c=this;a=a||{};var b={},e=c.serviceLocator.getUrl("process-handler-getCustomizedColumnsData");b.pageSize=a.pageSize||c.pageSize;b.page=a.page||1;b.orderFieldName=a.orderFieldName||"";b.orderType=a.orderType||"0";b.order=a.order||"";if(bizagi.isMobile())b.mobileDevice=true;a.taskState=a.taskState||"all";if(a.taskState.toString().toLowerCase()==="")a.taskState="all";if(a.taskState.toString().toLowerCase()==="red")a.taskState="Red";if(a.taskState.toString().toLowerCase()==="yellow")a.taskState="Yellow";if(a.taskState.toString().toLowerCase()==="green")a.taskState="Green";b.taskState=a.taskState;if(!bizagi.util.isEmpty(a.idWorkflow))b.idWorkflow=a.idWorkflow;if(!bizagi.util.isEmpty(a.idTask))b.idTask=a.idTask;if(!bizagi.util.isEmpty(a.radNumber))b.radNumber=a.radNumber;if(a.onlyFavorites==true){b.onlyFavorites=true;b.taskState="all"}if(a.smartfoldersParameters){var d=a.smartfoldersParameters.split("&");$.each(d,function(d,c){var a=c.split("=");b[a[0]]=a[1]})}return $.read(e,b)},getCustomizedColumnsDataBeta:function(a){var c=this;a=a||{};var b={},e=c.serviceLocator.getUrl("process-handler-getCustomizedColumnsData");b.pageSize=a.pageSize||c.pageSize;b.page=a.page||1;b.orderFieldName=a.orderFieldName||"";b.orderType=a.orderType||"0";b.order=a.order||"";b.mobileDevice=bizagi.isMobile()?true:false;a.taskState=a.taskState||"all";if(a.taskState.toString().toLowerCase()==="")a.taskState="all";if(a.taskState.toString().toLowerCase()==="red")a.taskState="Red";if(a.taskState.toString().toLowerCase()==="yellow")a.taskState="Yellow";if(a.taskState.toString().toLowerCase()==="green")a.taskState="Green";b.taskState=a.taskState;if(!bizagi.util.isEmpty(a.idWorkflow))b.idWorkflow=a.idWorkflow;if(!bizagi.util.isEmpty(a.idTask))b.idTask=a.idTask;if(!bizagi.util.isEmpty(a.radNumber))b.radNumber=a.radNumber;if(a.onlyFavorites==true){b.onlyFavorites=true;b.taskState="all"}if(a.smartfoldersParameters){var d=a.smartfoldersParameters.split("&");$.each(d,function(d,c){var a=c.split("=");b[a[0]]=a[1]})}return $.read(e,b).pipe(function(c){var a={columnTitle:[],lstIdCases:[],elements:[],page:c.page,totalPages:c.totalPages},d=new Date,i=d.getMonth(),e=d.getDate(),j=d.getYear();a.columnTitle=c.cases.columnTitle;a.lstIdCases=c.cases.lstIdCases;a.page=c.cases.page;a.totalPages=c.cases.totalPages;for(var f=0,m=c.cases.rows.length;f<m;f++){var b=c.cases.rows[f],g=new Date(b.fields[3]),h=g.getDate(),k=g.getMonth(),l=g.getYear();if(e>h||(i>k||j>l)){b.fields[3]=bizagi.util.dateFormatter.formatDate(new Date(b.fields[3]),"dd MMM");a.elements.push($.extend({},b,{group:"overdue"}))}else if(e==h){b.fields[3]=bizagi.util.dateFormatter.formatDate(new Date(b.fields[3]),"hh:mm");a.elements.push($.extend({},b,{group:"today"}))}else if(e+1==h){b.fields[3]=bizagi.util.dateFormatter.formatDate(new Date(b.fields[3]),"hh:mm");a.elements.push($.extend({},b,{group:"tomorrow"}))}else{b.fields[3]=bizagi.util.dateFormatter.formatDate(new Date(b.fields[3]),"dd MMM");a.elements.push($.extend({},b,{group:"upcoming"}))}}(typeof a.elements==="undefined"||typeof a.elements!=="undefined"&&a.elements.length===0)&&a.elements.push({noResults:true,message:bizagi.localization.getResource("workportal-menu-search-found-no-cases")});return a})},getCategories:function(a){var c=this;a=a||{};var b={};if(a.idCategory)b.idCategory=a.idCategory;if(a.idApp)b.idApp=a.idApp;if(bizagi.isMobile())b.mobileDevice=true;b.groupByApp=a.groupByApp||false;return $.read(c.serviceLocator.getUrl("process-handler-getCategory"),b)},getRecentProcesses:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("process-handler-getRecentProcesses"),a)},getCaseSummary:function(a){var b=this;a=a||{};return $.read({url:b.serviceLocator.getUrl("case-handler-getCaseSummary"),data:{idCase:a.idCase,eventAsTasks:a.eventAsTasks||"false",onlyUserWorkItems:a.onlyUserWorkItems||"true",mobileDevice:bizagi.isMobile()||"false"},serviceType:"GETSUMMARY"}).pipe(function(a){return a})},getCaseAssignees:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getCaseAssignees"),{idCase:a.idCase}).pipe(function(a){return a})},getCaseTasks:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getCaseTasks"),{idCase:a.idCase})},getCaseEvents:function(a){var b=this;a=a||{};return $.read({url:b.serviceLocator.getUrl("case-handler-getCaseEvents"),data:{idCase:a.idCase},serviceType:"GETEVENTS"}).pipe(function(a){return a})},getCaseSubprocesses:function(a){var b=this;a=a||{};return $.read({url:b.serviceLocator.getUrl("case-handler-getCaseSubprocesses"),data:{idCase:a.idCase},serviceType:"GETSUBPROCESSES"}).pipe(function(a){return a})},getCaseFormsRenderVersion:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getCaseFormsRenderVersion"),{idCase:a.idCase})},getCaseFormsRenderVersionDataContent:function(b){var c=this,a=new $.Deferred;$.when(c.getCaseFormsRenderVersion(b)).done(function(b){a.resolve(b)});return a.promise()},getTaskAssignees:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getTaskAssignees"),{idCase:a.idCase,idTask:a.idTask})},getWorkitems:function(a){var c=this;a=a||{};var b={};b.idCase=a.idCase;b.fromTask=a.fromTask;b.mobileDevice=bizagi.isMobile();if(typeof a.onlyUserWorkItems!=="undefined")b.onlyUserWorkItems=a.onlyUserWorkItems;return $.read({url:c.serviceLocator.getUrl("case-handler-getWorkItems"),data:b,serviceType:"GETWORKITEMS"})},getQueries:function(b){var a=this;if(a.hashQueries&&b.idElement)return a.hashQueries[b.idElement];else{var c=new $.Deferred;$.read(a.serviceLocator.getUrl("query-handler-GETQUERIES")).done(function(b){a.hashQueries={};a.hashQueries[0]={};a.processQueries(b.query);c.resolve(a.hashQueries["-1"])});return c.promise()}},processQueries:function(b){var a=this;$.each(b,function(c,b){if(!a.hashQueries[b.idParent])a.hashQueries[b.idParent]=[];a.hashQueries[b.idParent].push(b);if(b.nodes)a.processQueries(b.nodes);else b.nodes={}})},createNewCase:function(a){var b=this;a=a||{};return $.create({url:b.serviceLocator.getUrl("case-handler-addNewCase"),data:{idWfClass:a.idWfClass,caseData:a.caseData?JSON.stringify(a.caseData):null,idOrganization:a.idOrganization?a.idOrganization:null},serviceType:"NEWCASE"})},searchCases:function(b){var c=this;b=b||{};var a={};a.h_action="SEARCHCASES";a.onlyUserCases=false;a.active=false;a.page=b.page||1;if(b.radNumber)a.radNumber=$.trim(b.radNumber);a.pageSize=b.pageSize||c.pageSize;return $.ajax({url:c.serviceLocator.getUrl("query-handler"),data:a,type:"POST",dataType:"json"})},queryCases:function(a){var c=this;a=a||{};var b={};b.h_action="QUERYCASES";b.onlyUserCases=a.onlyUserCases||false;b.active=a.active&&true;b.page=a.page||1;if(a.radNumber)b.radNumber=$.trim(a.radNumber);b.pageSize=a.pageSize||c.pageSize;b.filter=a.filter;b.outputxpath=a.outputxpath;b.orderFieldName=a.orderFieldName||"";b.orderType=a.orderType=="asc"||a.orderType==1?1:0;b.order=a.order||"";return $.ajax({url:c.serviceLocator.getUrl("query-handler"),data:b,type:"POST",dataType:"json"})},queryEntities:function(b){var c=this;b=b||{};var a={};a.h_action="QUERYENTITIES";a.page=b.page||1;a.pageSize=c.Class.ENTITIES_QUERY_PAGE_SIZE;a[c.Class.BA_CONTEXT_PARAMETER_PREFIX+"idEnt"]=b.idEntity;a.filter=b.filter;a.outputxpath=b.outputxpath;a.orderFieldName=b.orderFieldName||"";a.orderType=b.orderType||"";a.order=b.order||"";return $.ajax({url:c.serviceLocator.getUrl("query-handler"),data:a,type:"POST",dataType:"json"})},getMenuAuthorization:function(){var a=this;return $.read(a.serviceLocator.getUrl("authorization-handler-getMenuAuthorization"))},isCaseCreationAuthorized:function(b){var c=this,a=c.serviceLocator.getUrl("authorization-handler-isCaseCreationAuthorized");a=a.replace("{0}",b.idWfClass||"");return $.read({url:a})},getAsynchExecutionState:function(a){var b=this;a=a||{};return $.read({url:b.serviceLocator.getUrl("case-handler-getAsynchExecutionState"),data:{idCase:a.idCase},serviceType:"ASYNCHEXECUTION"})},supportedLogTypes:function(){var a=this;return $.read(a.serviceLocator.getUrl("case-handler-supportedLogTypes"))},getActivityLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getActivityLog"),{idCase:a.idCase,orden:a.sort||0,idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},getActivityDetailLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getActivityDetailLog"),{idCase:a.idCase,idWorkItemFrom:a.idWorkItemFrom,idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},getEntityLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getEntityLog"),{idCase:a.idCase,orden:a.sort||0,idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},getEntityDetailLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getEntityDetailLog"),{idCase:a.idCase,idEntity:a.idEntity||-1,userFullName:a.userFullName||"",attribDisplayName:a.attribDisplayName||"",idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},getUserLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getUserLog"),{idCase:a.idCase,orden:a.sort||0,idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},getUserDetailLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getUserDetailLog"),{idCase:a.idCase,idUser:a.idUser,entDisplayName:a.entDisplayName||"",attribDisplayName:a.attribDisplayName||"",idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},getAdminLog:function(a){var b=this;a=a||{};return $.read(b.serviceLocator.getUrl("case-handler-getAdminLog"),{idCase:a.idCase,orden:a.sort||0,idActualPage:a.idActualPage||1,pageSize:a.pageSize||10}).pipe(function(a){return a})},addFavorite:function(a){var b=this;a=a||{};return $.create(b.serviceLocator.getUrl("favorites-handler-saveFavorite"),{idObject:a.idObject,favoriteType:a.favoriteType}).pipe(function(a){return a})},delFavorite:function(a){var b=this;a=a||{};return $.destroy(b.serviceLocator.getUrl("favorites-handler-deleteFavorite"),{guidFavorite:a.idObject}).pipe(function(a){return a})},summaryAssigness:function(b){var d=this,a=$.Deferred(),c=b.idCase||"";$.when(d.getCaseAssignees({idCase:c})).done(function(g){var c=g.assignees,b={};b.showAssignees=c.length>=1?true:false;b.assignees={};b.events="";b.activities="";for(var f="",e="",d=0;d<c.length;d++)if(c[d].isEvent==="true")f+=f.indexOf(c[d].Name)==-1?c[d].Name+", ":"";else e+=e.indexOf(c[d].Name)==-1?c[d].Name+", ":"";b.events=f.substring(0,f.length-2);b.activities=e.substring(0,e.length-2);a.resolve(b)});return a.promise()},summarySubProcess:function(b){var d=this,a=$.Deferred(),c=b.idCase||"";$.when(d.getCaseSubprocesses({idCase:c})).done(function(b){b.showSubProcess=b.subProcesses.length>=1?true:false;b.showSubProcesColumns=b.CustFields[0]!==undefined&&b.CustFields[0][0].length>=1?true:false;if(b.showSubProcesColumns){b.subProcPersonalized={};for(var e,c=0;c<b.CustFields.length;c++){e=0;for(var d=0;d<b.subProcesses.length;d++)if(b.subProcesses[d].idCustData==c){b.subProcPersonalized[c]=b.subProcPersonalized[c]||{};b.subProcPersonalized[c].subProcesses=b.subProcPersonalized[c].subProcesses||{};b.subProcPersonalized[c].subProcesses[e++]=b.subProcesses[d]}b.subProcPersonalized[c].CustFields=b.CustFields[c];b.subProcPersonalized[c].idCase=b.idCase}}a.resolve(b)});return a.promise()},summaryCaseEvents:function(b){var d=this,a=$.Deferred(),c=b.idCase||"";$.when(d.getCaseEvents({idCase:c})).done(function(b){b.showEvents=b.events.length>=1?true:false;a.resolve(b)});return a.promise()},summaryCaseDetails:function(b){var d=this,c=$.Deferred(),a=b.idWorkitem||0;$.when(d.getCaseSummaryDataContent(b)).done(function(b){b.showEvents=b.countEvents>=1?true:false;b.showParentProcess=b.idParentCase>=1?true:false;b.parentProcess={displayName:b.parentDisplayName,idCase:b.idParentCase};b.isClosed=b.isOpen=="true"?false:true;b.showAssignees=b.countAssigness>=1?true:false;b.showSubProcess=b.countSubProcesses>=1?true:false;b.showForm=b.hasGlobalForm=="true"?true:false;b.allowsReassign="false";for(i=0;i<b.currentState.length;i++)if(b.currentState[i].idWorkItem==a)b.allowsReassign=b.currentState[i].allowsReassign;var d=[],e=0;for(i=0;i<b.currentState.length;i++)if(b.currentState[i].isEvent=="false"&&b.currentState[i].assignToCurrentUser=="true"&&b.currentState[i].idWorkItem!=a)d[e++]=b.currentState[i];b.currentStateTypes=d;b.showActivities=d.length>=1?true:false;c.resolve(b)});return c.promise()},releaseActivity:function(a){var d=this,c=d.serviceLocator.getUrl("case-handler-releaseActivity");c=c.replace("{idCase}",a.idCase);var b={};if(a){b.idCase=a.idCase;b.idWorkItem=a.idWorkItem}return $.ajax({url:c,data:b,type:"POST",dataType:"json"})},summaryActivities:function(d){var a={},b=d.data||{},c=c||d.idWorkitem||0;try{a.showActivities=b.currentStateTypes.length>=1?true:false;a.currentState=b.currentStateTypes;a.globalIdWorkitem=c;a.creationDate=b.creationDate}catch(e){}return a},getCaseSummaryDataContent:function(c){var b=this,a=new $.Deferred;$.when(b.getCaseSummary(c)).done(function(c){c.taskState=b.icoTaskState,c.createdByName=c.createdBy.Name;c.createdByUserName=c.createdBy.userName;c.caseDescription=c.caseDescription==""?"":c.caseDescription;c.processPath=c.processPath.replace(/\//g," > ")+c.process;c.showWorkOnIt=true;a.resolve(c)});return a.promise()},getSmartFolders:function(a){var c=this,b={};b.idFolder=a||"";return $.read(c.serviceLocator.getUrl("folders-handler-getUserQueries"),b).pipe(function(b){return a==-1||a==""?b:c.searchFolders(a,b)||[]})},deleteSmartFolder:function(a){var c=this,b={};b.action="6";a=a||{};b.idSmartFolder=a.idSmartFolder||"";b.idUser=a.idUser||"";return $.destroy(c.serviceLocator.getUrl("folders-associate-deleteSmartFolder"),b)},getFolders:function(a){var c=this,b={};b.action="getUserFolder";b.idFolder=a||"";return $.ajax({url:c.serviceLocator.getUrl("folders-handler"),type:"POST",data:b,dataType:"json"}).pipe(function(b){return a==""?{folders:[{name:bizagi.localization.getResource("workportal-widget-folders"),id:"-1",idParent:0,childs:b}]}:a==-1?b:c.searchFolders(a,b)||[]})},searchFolders:function(c,b){for(var a=0;a<b.folders.length;a++){var d=b.folders[a];if(d.id==c){return b.folders[a].childs;break}}return undefined},getCasesByFolder:function(a){a=a||"";return $.ajax({cache:true,url:"RestServices/"+a,type:"GET",dataType:"json"})},makeFolder:function(a){var c=this,b={};b.action="CreateUpdateFolder";b.folderName=a.folderName||"No Name";if(a.idParentFolder!=undefined&&a.idParentFolder>1)b.idParentFolder=a.idParentFolder;return $.ajax({url:c.serviceLocator.getUrl("folders-handler"),data:b,type:"POST",dataType:"json"})},updateFolder:function(a){var c=this,b={};a=a||{};b.action="CreateUpdateFolder";b.idFolder=a.idFolder||"";if(a.folderName)b.folderName=a.folderName;if(a.idParentFolder)b.idParentFolder=a.idParentFolder;return $.ajax({url:c.serviceLocator.getUrl("folders-handler"),data:b,type:"POST",dataType:"json"})},associateCaseToFolder:function(a){var c=this,b={};b.action="4";a=a||{};b.idCase=a.idCase||"";b.idCustomFolder=a.idCustomFolder;return $.ajax({url:c.serviceLocator.getUrl("folders-associate"),data:b,type:"POST",dataType:"json"})},dissasociateCaseFromFolder:function(a){var c=this,b={};b.action="DeleteCaseFromFolder";a=a||{};b.idFolder=a.idFolder;b.idCase=a.idCase;return $.ajax({url:c.serviceLocator.getUrl("folders-handler"),data:b,type:"POST",dataType:"json"})},deleteFolder:function(a){var c=this,b={};b.action="5";a=a||{};b.idCustomFolder=a.idCustomFolder||"";return $.ajax({url:c.serviceLocator.getUrl("folders-associate"),data:b,type:"POST",dataType:"json"})},getFile:function(a){var d=this,b={},c=this.serviceLocator.getUrl("file-handler");a=a||{};b.action="getFileContent";b.uploadId=a.uploadId||"";b.entityId=a.entityId||"";b.entityKey=a.entityKey||"";return bizagi.services.ajax.pathToBase+c+"?"+jQuery.param(b)},getComments:function(a){var b=this;a=a||{};return $.read(encodeURI(b.serviceLocator.getUrl("MessageHandler-GetComments")),{idCase:a.idCase||"",idColorCategory:typeof a.idColorCategory=="number"?a.idColorCategory:"",pag:a.pag||1,pagSize:a.pagSize||10}).pipe(function(a){var d=new $.Deferred,c="iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABJwSURBVHhe7Vt5UJPnuj+ddvSqdcpotQougCyGRfY9QCQQwhKIrAqurUtbbbVV1KO2UrXH46lL1aOOdWvrcl3QuosIBUSGongA5bLXqLjCVbarrX/97vO8SSAieLRVJ86czPwmX0L8vve3PM/7vp/JX/7yn8d/FHhVCkykC+144403NL16vQ1TU1MtBpqiR48eoL8VE9YQnF/VgF7VdVLffPPNRkdHJ0SER0CpDIdCEYqQkBAtgvlZgdDQUHh5ecHMzIzFyCbIXtUAX9Z12MliN1c3JCcnIzkpGWp1jCBrSD6YBAgODoZczpALBMmCYGFhwULsIJi8rAG+zPM6s+vseGLiaMTFxSM2JlaQl8lkAoGBgQgMCERAQAD8/f0hlfrDz88Pfr5+8PX1FXByctKXxmslgiAfEqxAdFQ0VKooqCJVwnVvb1+Ct4i6l6cXPDw8CR5wd3OHm5sbXF1d4ezMcIYTlQwLYGNjoxfhZRr2ws7NTmm8vLwRSnWuIMelflK46kkRISbl6OAIB4K9vT3s7ewhkdgRJLC1sRWEra2tYWVlhWHDhonnPn36sAjcII3+kWplZQ0Pd08RYXt7Bx0pW0FKT8xqmBUsLYfB0sISFkMtMHToUAwZPASDBw8WTdDU1AwDBw4UGDBggAClikUwN2YFTLp37y5izvFlId7t+y4G9B8gSGmJaae9gQO0xPr3649+/fqJz/UlsNN9TPrAxMRE4J133hGf43RwEnRN0Wg1mMVR5q5vQc72pLl99sfj0b3bW3j77beJYF8t+vRtI9qjR0+80/sdIqoly8e9e/d+DJaWlrC1tRXlwn8z5lmh2MvTm2rYFt26dcOSeTNQnn8U2Ue2w8pyCHp076EjTwIIl/vgrbfeYkKCGAvQ+ykCcAq4VOjzE40xAiYm5CIvdvpRrHv26I6a8+nQXMxAbdFpFGcfxJTx8YIsk9fHnB3n9xicho7uc3K4D3ACGNxDjLUZyrh+hwwZSvXcD3J/T1wrPYNrJRkC10uzUPFLOnZvWQ13Zwf06tVL29yoPzBJqZc7fD2cRaN7913qG/Q3fuY+wKLwbMACGKwSjS4EsoHU3MzMBoOTwLVfV5pJOCNw41I2bpadxbVLubiUn46VyxZhYP9+QghujBx/pVyKr+d/goH9+gpRWAR2n4Xg1wwWmfcSRsee65IF4O7OdTxn+gTcvJyFm5cyBW6V5eB2xTncrvoFd2qKoLmcj5L8DHy37hsE+HmR828xMVgOMcWejcsw98MkSN3tRY/gBOgF4EToSsbo+oBMTG0UaRZg/fL5uFOWTcgSuFuRi7uV+aivKUTDlYtouFqKe9fLUVddgrLCLGQc/hGLPp+GQF83mPZ/F9tXzkPegfU4vHkJUmeOx9houSCuE4B3jWpjS4GMu3p/FuDtXqgsOIr6ihzUl2cLNFSeRUN1Ae7VXsD9qyVovH4JTTcq0XyrBo115bhbfQG1FzJQmrUfpWd24VbREdwpOoTrBftQkfk9StO3GApglCtCE65nruUFMz8gwrm4V0WozCHkorEmH02a82i5XoLWG5fx8E4VHt6txcP6q3hwuwotdaVovHIe96vO4l7ZGTSUHkd98WHUXzxEOIj6ojT4eziAF1rkfKqxuS/G06vHfzXy9HZo+2oikovmmjy01BKunEPr1UI8uF6EhzdK8dvtcvx2txq/37uKR/ev47f6WvHew7pitF4pQFNlFu5dOoF7JUcIh3Gv+BDukQiBXo7o2bMnkiOkvEU2rsepTYsmTklQigXQ0R/XoonIN1ZRCVzOQF3RUdy4eAI3S07jFs0It2hGaL5eSgJo8KhRJ8CtMjRQn7hRdBx1hRT9/L24lrcL1wm3C/eRAGmQ+7qKafIfcyfjxIZFxtUDTmxamHpwy3JRp0d/WIP6skxcPX8ENXn7tTiXhqqzB1CZewDl2VTXOQdwX1NCCbhGCagRopRn7iLsRGXWTlRR3ddkfY/abC2uZG+Hr6stYkOlyNi9Fic3LphlVBFgRzL3rINc6o7D36+ixpeJO5cycLs0nVxnZBDJLJoOc2k9kIfawpO4XpzZJoCm8DiuFaWjrphw4QjqqPnV5e/GDcJNwp3CvVi9YDLy9qxC5u7VOLVpocyoBDi1abH52X1ryZ3VKCMn66mRNVVlaxtaWQYayrNoVshFfRVNhdWFtCYowNWiDDy6RwmgfnC7/BzulucK0e4UH8edC2m4c36fQP2FfWgsPoCajC24eHQT8ug6P61ZbGJUAvBgzmxdqrlB7p3bv0643lKdQ6B6r8pCU3Uu9QWaCa4UounqRTReLUZFbppohCwAv3e/Oh/3K7Jx/3K6aH73i9MEGhkXqXRObsSVvL04tflLXgcY34PLoOjwP5Gz8x+4UXyCunkmWog8o5Vmg1bNLzQTXMRvNBM0XrlgIECVeO/htfNorTmLlvIMNJUeQWPJwTbyjRf345e934DPb3QN0NAKHtzhdXM1tef2o+HyaTSTCM1VmYJYq6YAD64VCbL1tCoUCWjQUAJ0AtBU2UpJaSk/TQIcfox8w/k9OLFxocaoyeuFOLouRV14aANulxwnAc4IEVprcmmOzycBLtB8X4o62iZX5OgEuFOpTYCGBchBc5lOAIo9O8+ozviOGt8io1v/d1mHxzYs1Fw7fwjNFRlCBCbWWnsOD66eJwFKUJ13SEyFIgFCgBI8oBJpocbZXJaOppJDbeRZgPzdf2/8aYcRNr6uFDi4NiW1NH0bLWlPCBFaqRlqBShEMy159WuB3xuuCAFYlAdUIkKAyydJAKp/nfvXcraT+18a5fq/ywTwNJW+dUljXeFBNFNTa9U1wgcU84fXi2lKzKOF0SFKAAlwu0IrAC2DW2gZ3HyZGqiBAOQ+Tu1Ybm58bf/fjIhTUHhwPRrJ0RbRB86KmD/kmeDmJXK+Cr/XswC0DyBRHlCP6CgAu396y1fGufl5FkfSVqdoNDR3t3AfoEbILrcLUEkC6DZCJEpHAbjzZ21bguzXqfY7inJk/bzU01uXUC84KRohk3yomwq59oUAtBFiUToKUHKI1vybv+D9xev7OLBqdmpe2gZcOLIJTRVZbQJot8UsQE2nAlRnbEPe7hU4s33p6y3AwbVzf6osPIHMXatQcnIbWmib/OAqrQWEABWdClCbsxOZ25eh/DSt/SkFp79b7PzaRuDExkXZdbQLrCg4JkS4cGwrmmupD9BiqDMBanL3kOvLaNOzgTZB+3Hp2Aac2rLYuHZ+z+PGqiUp2Q/qLtJuLwdVdJ/wwvFtyPh+BYpP78QN+r8C3gXepnsB1WfTkLNrJU5Sv/jl0HrcKNgjFkNHvluKxTMnvz4rQBYnOjraPDIycs1IpVrj6S9HQcY+tNISuJlmgJt0X6A0ay/O7F2Hw0Ru/7qF+OGbFPy4ai69XoJ8mjYr6WYI3wW6SSKMT1TDL3oilFHxGpVKlcrnfh4DXulnaYATw1SjsmWxk+GS/CXsx6+Ac9znUCpCsHPzSnFfkPcDfHOk/Ox+nDv8HY5uX4E9//wSaXT7+wwl4PyRjUIAvgs8LoHIK+NgN26FOBefk8/N1+BrvVJyXV2MHWFnFOrRjd6jZ8Nx3NdisIbwSPgMsuBQxKhV2LlpBUqz9wsBCqgfnPxxVZsAx7b9DdtWzkfK9IniuwU+ihjYJS974nx8Db5WaFR8oy4VJq9cDF3Md4SMGgv3pAVPDLKjCE5jl0AaMxUjQ1Xi+0BMcNzoWIyJi0K8OgKhIXLx/SD+3pBvRBJcEufDTgj5d9iPM8Ryek0Yuxx2BLfR8xE8KhlUcjteSXnQRUxY9Wcl3lEIJuSS9AU8ElPgE/shvEZNE3CLm0mk5wlSdmP/pkUy42sDLINdkh5L6ZgwZikkBNeEFMjVSS83EeHh4c4RqlGaJx0nh/6NU12SaiPExLSE9KQkY5YQOcJoxlftSEyFpA2LIUnQYjjBPXYm94hiHusLLQs+oUKd2OiYTAPUxe+FkUokck8hNTzhSwyPZ3zxOOIWYXgbFmJ4rBZ2sfMQokpofGEicOzDyXmHMam6CHblkoFTTyVFZDojFUcEuyBlG7sAAjGMvz6OUfNhKzBPC/U8SNRzKAnqF3PzlOd01/g5HVziGLZHTxB6UaRGEcFOSDExG/VcLaIZKe2IouOoOTrMpufZcFVNo+8oqv7cVppjFKQeB4lBBG05ap04xe936VQbocedsnkqKT0hfiZSqnZYqz6HdaQen9GxFlbhn8I6YlYbRqpG40/NDqRgtnM8RSquPX7Dwj/RRvAFkrImcl2RMiRkHTET1uHtYMJWYYxPBCxDpmFY6HRYKWfAOmwGHCI+FlPkH2qIERERsqBRE+AQvwCSmPlt8bMImdohfp07JQh15lTEZ1i3Ox38SJ6/ntzqQKgDKUFOyZhhACLJRHUYFvoxhik+hqV8MsxHTqLjj2BN7w0PnwGZaswfSwG5/5M71b5j7HxqKlRjOpcGS8c8GT+D2GkdezqpdbtOagWYu7ZTUoKQjhSTaUMIHYd82I5gcrwNU2FB5If4jSEhpsCa0jBc+RGcwqc9fwq4buRU+y7x8+EYkwJJ1OewIWIWwVMwyDtO1FrH+GmdMnSJjzu4pCO1dudxIUDSnNXwSpiDUdO/hrN6lpaYASnL4KkQkDOmtCOIjoMm6/ABLEdqMVSaDDP3aDp+H9aUBoliGhzCPoI8MuH5UsCd3yd2BlxiU+Co/hwSFblKNTVEmgQzrxhRaxy/rp0ycKkTUmt/OCoE+HHPfvz888/49ddf8ejRI6Sl58M5imo5qJ0UE7MgQhYyPSbRsQ6BE2HRhgkY4pOIAU6hsAycAOugSZCETIaDciq8wyfwjDDrmXtBuCpW4xGfApcY2uBEzYIk4hNRU4O8YmHmqYYFDdAweu0uGTj1mEvsVjupb78/IgRYunIjZi5eg282/zf+936TeC8j71+PkbIImAA9zAPGw9xfj3F0rIN0LMwJg73i8J6DHOa+o2EtmwCJfBIcFZPhpJwCZYT62b5ep5/6PGJnw4Vi6Rj5CSRhH8Oa4jTQOUxEjFXXRtDQKXKoM6cec4kdm4Bvd/wkyI7+dDn0pEaEfYjm1gfifWn8Z4JQO5Jh7qdHEob6EXwZY9rhMwaDPWLwnn0QhnrGwDpgLCQjJ8AxZBJclJMRGJaAZ1od8uJBGvMhPGJmwSXqUzjSVCIJnQZrqn8+uambSijfHj0iZeCSBbvUmVMGhNZsOyiITv3rah0xLamCf5WL9xOnL9ESI1JDfUa3wzsRQ9uQgCFeesTTcTwGuUXjPYkMg8goG/8xkMjGwlE+AS6KSfBUJD9TGXQjlU66Rc2Ah/pTuKimw5GcsVNQVw16H/1s/WDqEikGK+L3mEvkWJtL7JbepY5OjcZX3/4giBZc/J82QmHj5rYlwFc9XRAS8NQjjo518IjFkDbEYLC7FoNcVHhveCAGjQiBjV8iJAFJGBE0Fi4hE+AUNI6325upDwwjvNlVP3DkD/lEvg/P6OlwjfgII6iJ2AW/Dyv/JPRnAUhd4cpj8TNwif9m4NJQ7yedciBX6m7VCxH4mYVobvk/8XrrnmNErp3UYPdRRI7gxlAbIBqDXQ0RBTOncAwY7o+BBBvfOEj8EzFCRvcX5OMgVSbS75KkK4m4G0HSlQBu9J1+mUKhyB6pSoZb+GSMUHwAuyCqee8Y9LfxhamTUufO4/HTOmXgEh8buNSRlI9qKgqKLgvS/GABtu4++gSpQS5R5CxDpYUzI7IdThEYpIOZoxIDbKUYaOMDGxqvnV88nAMTEUi31pgTc9MJwCJ0+uiv/wD9tmeqUqnMDoqIp/qhxY831Red2NRRIZoNE3p+p6KIoJ6QISkDQkzOgNQgcpVhNoJBTZjhqIeSjnVwCIWZfQgG2PhhkK0PPGVqyJVq+p2iMpu5GBBn8n2fNiX21tUJf1AkwsfHJ5V+23csSBF5y1sWCSf/aEh89PH7g6QEoY6kDAgxMSbVBgURVMBUIKQddiGwdQuFs68SvrJwjJSH3eKx8pg7OM58zAndnnU9wI2CleJ/5KhXkE/KinKv4AvJFcoiaVDErYDgSHgERMKdYO8dgeGe4QJWbkRS79JzkDK1C4aVi0KQs3VTwM4jFO7SMAH/oHBIRypvyYNDi+iHlnt4LDymTgjzuHn8zKPLxvesgrByJgRTXUJsOsRKpIUHwmAHeGCG4DqkSBY9DSxqx39Hvy2crT9vJyRFUgk8Hu7wPD4e5zM7/awCdPU5VpbLhsE9hAdgKBIPTA/njqJ18Zo/Z/jvDMnxufk6+mv+qfH/Pz9r9qvYcZe2AAAAAElFTkSuQmCC";if(!a.comments)a.comments=[];else $.each(a.comments,function(b){if(!a.comments[b].hasOwnProperty("CategoryColor"))a.comments[b].CategoryColor=""});if(!a.users)$.when(b.getCurrentUser()).done(function(b){a.users=[];a.users.push({Id:b.idUser,Name:b.user,DisplayName:b.userName,Picture:c})});else $.each(a.users,function(b){a.users[b].Picture=a.users[b].Picture===""?c:a.users[b].Picture});d.resolve(a);return d.promise()})},makeNewComment:function(a){var c=this,b="iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABJwSURBVHhe7Vt5UJPnuj+ddvSqdcpotQougCyGRfY9QCQQwhKIrAqurUtbbbVV1KO2UrXH46lL1aOOdWvrcl3QuosIBUSGongA5bLXqLjCVbarrX/97vO8SSAieLRVJ86czPwmX0L8vve3PM/7vp/JX/7yn8d/FHhVCkykC+144403NL16vQ1TU1MtBpqiR48eoL8VE9YQnF/VgF7VdVLffPPNRkdHJ0SER0CpDIdCEYqQkBAtgvlZgdDQUHh5ecHMzIzFyCbIXtUAX9Z12MliN1c3JCcnIzkpGWp1jCBrSD6YBAgODoZczpALBMmCYGFhwULsIJi8rAG+zPM6s+vseGLiaMTFxSM2JlaQl8lkAoGBgQgMCERAQAD8/f0hlfrDz88Pfr5+8PX1FXByctKXxmslgiAfEqxAdFQ0VKooqCJVwnVvb1+Ct4i6l6cXPDw8CR5wd3OHm5sbXF1d4ezMcIYTlQwLYGNjoxfhZRr2ws7NTmm8vLwRSnWuIMelflK46kkRISbl6OAIB4K9vT3s7ewhkdgRJLC1sRWEra2tYWVlhWHDhonnPn36sAjcII3+kWplZQ0Pd08RYXt7Bx0pW0FKT8xqmBUsLYfB0sISFkMtMHToUAwZPASDBw8WTdDU1AwDBw4UGDBggAClikUwN2YFTLp37y5izvFlId7t+y4G9B8gSGmJaae9gQO0xPr3649+/fqJz/UlsNN9TPrAxMRE4J133hGf43RwEnRN0Wg1mMVR5q5vQc72pLl99sfj0b3bW3j77beJYF8t+vRtI9qjR0+80/sdIqoly8e9e/d+DJaWlrC1tRXlwn8z5lmh2MvTm2rYFt26dcOSeTNQnn8U2Ue2w8pyCHp076EjTwIIl/vgrbfeYkKCGAvQ+ykCcAq4VOjzE40xAiYm5CIvdvpRrHv26I6a8+nQXMxAbdFpFGcfxJTx8YIsk9fHnB3n9xicho7uc3K4D3ACGNxDjLUZyrh+hwwZSvXcD3J/T1wrPYNrJRkC10uzUPFLOnZvWQ13Zwf06tVL29yoPzBJqZc7fD2cRaN7913qG/Q3fuY+wKLwbMACGKwSjS4EsoHU3MzMBoOTwLVfV5pJOCNw41I2bpadxbVLubiUn46VyxZhYP9+QghujBx/pVyKr+d/goH9+gpRWAR2n4Xg1wwWmfcSRsee65IF4O7OdTxn+gTcvJyFm5cyBW6V5eB2xTncrvoFd2qKoLmcj5L8DHy37hsE+HmR828xMVgOMcWejcsw98MkSN3tRY/gBOgF4EToSsbo+oBMTG0UaRZg/fL5uFOWTcgSuFuRi7uV+aivKUTDlYtouFqKe9fLUVddgrLCLGQc/hGLPp+GQF83mPZ/F9tXzkPegfU4vHkJUmeOx9houSCuE4B3jWpjS4GMu3p/FuDtXqgsOIr6ihzUl2cLNFSeRUN1Ae7VXsD9qyVovH4JTTcq0XyrBo115bhbfQG1FzJQmrUfpWd24VbREdwpOoTrBftQkfk9StO3GApglCtCE65nruUFMz8gwrm4V0WozCHkorEmH02a82i5XoLWG5fx8E4VHt6txcP6q3hwuwotdaVovHIe96vO4l7ZGTSUHkd98WHUXzxEOIj6ojT4eziAF1rkfKqxuS/G06vHfzXy9HZo+2oikovmmjy01BKunEPr1UI8uF6EhzdK8dvtcvx2txq/37uKR/ev47f6WvHew7pitF4pQFNlFu5dOoF7JUcIh3Gv+BDukQiBXo7o2bMnkiOkvEU2rsepTYsmTklQigXQ0R/XoonIN1ZRCVzOQF3RUdy4eAI3S07jFs0It2hGaL5eSgJo8KhRJ8CtMjRQn7hRdBx1hRT9/L24lrcL1wm3C/eRAGmQ+7qKafIfcyfjxIZFxtUDTmxamHpwy3JRp0d/WIP6skxcPX8ENXn7tTiXhqqzB1CZewDl2VTXOQdwX1NCCbhGCagRopRn7iLsRGXWTlRR3ddkfY/abC2uZG+Hr6stYkOlyNi9Fic3LphlVBFgRzL3rINc6o7D36+ixpeJO5cycLs0nVxnZBDJLJoOc2k9kIfawpO4XpzZJoCm8DiuFaWjrphw4QjqqPnV5e/GDcJNwp3CvVi9YDLy9qxC5u7VOLVpocyoBDi1abH52X1ryZ3VKCMn66mRNVVlaxtaWQYayrNoVshFfRVNhdWFtCYowNWiDDy6RwmgfnC7/BzulucK0e4UH8edC2m4c36fQP2FfWgsPoCajC24eHQT8ug6P61ZbGJUAvBgzmxdqrlB7p3bv0643lKdQ6B6r8pCU3Uu9QWaCa4UounqRTReLUZFbppohCwAv3e/Oh/3K7Jx/3K6aH73i9MEGhkXqXRObsSVvL04tflLXgcY34PLoOjwP5Gz8x+4UXyCunkmWog8o5Vmg1bNLzQTXMRvNBM0XrlgIECVeO/htfNorTmLlvIMNJUeQWPJwTbyjRf345e934DPb3QN0NAKHtzhdXM1tef2o+HyaTSTCM1VmYJYq6YAD64VCbL1tCoUCWjQUAJ0AtBU2UpJaSk/TQIcfox8w/k9OLFxocaoyeuFOLouRV14aANulxwnAc4IEVprcmmOzycBLtB8X4o62iZX5OgEuFOpTYCGBchBc5lOAIo9O8+ozviOGt8io1v/d1mHxzYs1Fw7fwjNFRlCBCbWWnsOD66eJwFKUJ13SEyFIgFCgBI8oBJpocbZXJaOppJDbeRZgPzdf2/8aYcRNr6uFDi4NiW1NH0bLWlPCBFaqRlqBShEMy159WuB3xuuCAFYlAdUIkKAyydJAKp/nfvXcraT+18a5fq/ywTwNJW+dUljXeFBNFNTa9U1wgcU84fXi2lKzKOF0SFKAAlwu0IrAC2DW2gZ3HyZGqiBAOQ+Tu1Ybm58bf/fjIhTUHhwPRrJ0RbRB86KmD/kmeDmJXK+Cr/XswC0DyBRHlCP6CgAu396y1fGufl5FkfSVqdoNDR3t3AfoEbILrcLUEkC6DZCJEpHAbjzZ21bguzXqfY7inJk/bzU01uXUC84KRohk3yomwq59oUAtBFiUToKUHKI1vybv+D9xev7OLBqdmpe2gZcOLIJTRVZbQJot8UsQE2nAlRnbEPe7hU4s33p6y3AwbVzf6osPIHMXatQcnIbWmib/OAqrQWEABWdClCbsxOZ25eh/DSt/SkFp79b7PzaRuDExkXZdbQLrCg4JkS4cGwrmmupD9BiqDMBanL3kOvLaNOzgTZB+3Hp2Aac2rLYuHZ+z+PGqiUp2Q/qLtJuLwdVdJ/wwvFtyPh+BYpP78QN+r8C3gXepnsB1WfTkLNrJU5Sv/jl0HrcKNgjFkNHvluKxTMnvz4rQBYnOjraPDIycs1IpVrj6S9HQcY+tNISuJlmgJt0X6A0ay/O7F2Hw0Ru/7qF+OGbFPy4ai69XoJ8mjYr6WYI3wW6SSKMT1TDL3oilFHxGpVKlcrnfh4DXulnaYATw1SjsmWxk+GS/CXsx6+Ac9znUCpCsHPzSnFfkPcDfHOk/Ox+nDv8HY5uX4E9//wSaXT7+wwl4PyRjUIAvgs8LoHIK+NgN26FOBefk8/N1+BrvVJyXV2MHWFnFOrRjd6jZ8Nx3NdisIbwSPgMsuBQxKhV2LlpBUqz9wsBCqgfnPxxVZsAx7b9DdtWzkfK9IniuwU+ihjYJS974nx8Db5WaFR8oy4VJq9cDF3Md4SMGgv3pAVPDLKjCE5jl0AaMxUjQ1Xi+0BMcNzoWIyJi0K8OgKhIXLx/SD+3pBvRBJcEufDTgj5d9iPM8Ryek0Yuxx2BLfR8xE8KhlUcjteSXnQRUxY9Wcl3lEIJuSS9AU8ElPgE/shvEZNE3CLm0mk5wlSdmP/pkUy42sDLINdkh5L6ZgwZikkBNeEFMjVSS83EeHh4c4RqlGaJx0nh/6NU12SaiPExLSE9KQkY5YQOcJoxlftSEyFpA2LIUnQYjjBPXYm94hiHusLLQs+oUKd2OiYTAPUxe+FkUokck8hNTzhSwyPZ3zxOOIWYXgbFmJ4rBZ2sfMQokpofGEicOzDyXmHMam6CHblkoFTTyVFZDojFUcEuyBlG7sAAjGMvz6OUfNhKzBPC/U8SNRzKAnqF3PzlOd01/g5HVziGLZHTxB6UaRGEcFOSDExG/VcLaIZKe2IouOoOTrMpufZcFVNo+8oqv7cVppjFKQeB4lBBG05ap04xe936VQbocedsnkqKT0hfiZSqnZYqz6HdaQen9GxFlbhn8I6YlYbRqpG40/NDqRgtnM8RSquPX7Dwj/RRvAFkrImcl2RMiRkHTET1uHtYMJWYYxPBCxDpmFY6HRYKWfAOmwGHCI+FlPkH2qIERERsqBRE+AQvwCSmPlt8bMImdohfp07JQh15lTEZ1i3Ox38SJ6/ntzqQKgDKUFOyZhhACLJRHUYFvoxhik+hqV8MsxHTqLjj2BN7w0PnwGZaswfSwG5/5M71b5j7HxqKlRjOpcGS8c8GT+D2GkdezqpdbtOagWYu7ZTUoKQjhSTaUMIHYd82I5gcrwNU2FB5If4jSEhpsCa0jBc+RGcwqc9fwq4buRU+y7x8+EYkwJJ1OewIWIWwVMwyDtO1FrH+GmdMnSJjzu4pCO1dudxIUDSnNXwSpiDUdO/hrN6lpaYASnL4KkQkDOmtCOIjoMm6/ABLEdqMVSaDDP3aDp+H9aUBoliGhzCPoI8MuH5UsCd3yd2BlxiU+Co/hwSFblKNTVEmgQzrxhRaxy/rp0ycKkTUmt/OCoE+HHPfvz888/49ddf8ejRI6Sl58M5imo5qJ0UE7MgQhYyPSbRsQ6BE2HRhgkY4pOIAU6hsAycAOugSZCETIaDciq8wyfwjDDrmXtBuCpW4xGfApcY2uBEzYIk4hNRU4O8YmHmqYYFDdAweu0uGTj1mEvsVjupb78/IgRYunIjZi5eg282/zf+936TeC8j71+PkbIImAA9zAPGw9xfj3F0rIN0LMwJg73i8J6DHOa+o2EtmwCJfBIcFZPhpJwCZYT62b5ep5/6PGJnw4Vi6Rj5CSRhH8Oa4jTQOUxEjFXXRtDQKXKoM6cec4kdm4Bvd/wkyI7+dDn0pEaEfYjm1gfifWn8Z4JQO5Jh7qdHEob6EXwZY9rhMwaDPWLwnn0QhnrGwDpgLCQjJ8AxZBJclJMRGJaAZ1od8uJBGvMhPGJmwSXqUzjSVCIJnQZrqn8+uambSijfHj0iZeCSBbvUmVMGhNZsOyiITv3rah0xLamCf5WL9xOnL9ESI1JDfUa3wzsRQ9uQgCFeesTTcTwGuUXjPYkMg8goG/8xkMjGwlE+AS6KSfBUJD9TGXQjlU66Rc2Ah/pTuKimw5GcsVNQVw16H/1s/WDqEikGK+L3mEvkWJtL7JbepY5OjcZX3/4giBZc/J82QmHj5rYlwFc9XRAS8NQjjo518IjFkDbEYLC7FoNcVHhveCAGjQiBjV8iJAFJGBE0Fi4hE+AUNI6325upDwwjvNlVP3DkD/lEvg/P6OlwjfgII6iJ2AW/Dyv/JPRnAUhd4cpj8TNwif9m4NJQ7yedciBX6m7VCxH4mYVobvk/8XrrnmNErp3UYPdRRI7gxlAbIBqDXQ0RBTOncAwY7o+BBBvfOEj8EzFCRvcX5OMgVSbS75KkK4m4G0HSlQBu9J1+mUKhyB6pSoZb+GSMUHwAuyCqee8Y9LfxhamTUufO4/HTOmXgEh8buNSRlI9qKgqKLgvS/GABtu4++gSpQS5R5CxDpYUzI7IdThEYpIOZoxIDbKUYaOMDGxqvnV88nAMTEUi31pgTc9MJwCJ0+uiv/wD9tmeqUqnMDoqIp/qhxY831Red2NRRIZoNE3p+p6KIoJ6QISkDQkzOgNQgcpVhNoJBTZjhqIeSjnVwCIWZfQgG2PhhkK0PPGVqyJVq+p2iMpu5GBBn8n2fNiX21tUJf1AkwsfHJ5V+23csSBF5y1sWCSf/aEh89PH7g6QEoY6kDAgxMSbVBgURVMBUIKQddiGwdQuFs68SvrJwjJSH3eKx8pg7OM58zAndnnU9wI2CleJ/5KhXkE/KinKv4AvJFcoiaVDErYDgSHgERMKdYO8dgeGe4QJWbkRS79JzkDK1C4aVi0KQs3VTwM4jFO7SMAH/oHBIRypvyYNDi+iHlnt4LDymTgjzuHn8zKPLxvesgrByJgRTXUJsOsRKpIUHwmAHeGCG4DqkSBY9DSxqx39Hvy2crT9vJyRFUgk8Hu7wPD4e5zM7/awCdPU5VpbLhsE9hAdgKBIPTA/njqJ18Zo/Z/jvDMnxufk6+mv+qfH/Pz9r9qvYcZe2AAAAAElFTkSuQmCC";return $.create(c.serviceLocator.getUrl("MessageHandler-NewComment"),{idCase:a.idCase||"",comment:a.comment.replace(/\n/g,"<br>")||""}).pipe(function(a){if(a.comments){var d=[a.comments];$.each(d,function(a){d[a].CategoryColor=""});a.comments=d}if(a.users){var c=[a.users];$.each(c,function(a){c[a].Picture=c[a].Picture==""?b:c[a].Picture});a.users=c}return a})},makeNewReply:function(a){var c=this,b="iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABJwSURBVHhe7Vt5UJPnuj+ddvSqdcpotQougCyGRfY9QCQQwhKIrAqurUtbbbVV1KO2UrXH46lL1aOOdWvrcl3QuosIBUSGongA5bLXqLjCVbarrX/97vO8SSAieLRVJ86czPwmX0L8vve3PM/7vp/JX/7yn8d/FHhVCkykC+144403NL16vQ1TU1MtBpqiR48eoL8VE9YQnF/VgF7VdVLffPPNRkdHJ0SER0CpDIdCEYqQkBAtgvlZgdDQUHh5ecHMzIzFyCbIXtUAX9Z12MliN1c3JCcnIzkpGWp1jCBrSD6YBAgODoZczpALBMmCYGFhwULsIJi8rAG+zPM6s+vseGLiaMTFxSM2JlaQl8lkAoGBgQgMCERAQAD8/f0hlfrDz88Pfr5+8PX1FXByctKXxmslgiAfEqxAdFQ0VKooqCJVwnVvb1+Ct4i6l6cXPDw8CR5wd3OHm5sbXF1d4ezMcIYTlQwLYGNjoxfhZRr2ws7NTmm8vLwRSnWuIMelflK46kkRISbl6OAIB4K9vT3s7ewhkdgRJLC1sRWEra2tYWVlhWHDhonnPn36sAjcII3+kWplZQ0Pd08RYXt7Bx0pW0FKT8xqmBUsLYfB0sISFkMtMHToUAwZPASDBw8WTdDU1AwDBw4UGDBggAClikUwN2YFTLp37y5izvFlId7t+y4G9B8gSGmJaae9gQO0xPr3649+/fqJz/UlsNN9TPrAxMRE4J133hGf43RwEnRN0Wg1mMVR5q5vQc72pLl99sfj0b3bW3j77beJYF8t+vRtI9qjR0+80/sdIqoly8e9e/d+DJaWlrC1tRXlwn8z5lmh2MvTm2rYFt26dcOSeTNQnn8U2Ue2w8pyCHp076EjTwIIl/vgrbfeYkKCGAvQ+ykCcAq4VOjzE40xAiYm5CIvdvpRrHv26I6a8+nQXMxAbdFpFGcfxJTx8YIsk9fHnB3n9xicho7uc3K4D3ACGNxDjLUZyrh+hwwZSvXcD3J/T1wrPYNrJRkC10uzUPFLOnZvWQ13Zwf06tVL29yoPzBJqZc7fD2cRaN7913qG/Q3fuY+wKLwbMACGKwSjS4EsoHU3MzMBoOTwLVfV5pJOCNw41I2bpadxbVLubiUn46VyxZhYP9+QghujBx/pVyKr+d/goH9+gpRWAR2n4Xg1wwWmfcSRsee65IF4O7OdTxn+gTcvJyFm5cyBW6V5eB2xTncrvoFd2qKoLmcj5L8DHy37hsE+HmR828xMVgOMcWejcsw98MkSN3tRY/gBOgF4EToSsbo+oBMTG0UaRZg/fL5uFOWTcgSuFuRi7uV+aivKUTDlYtouFqKe9fLUVddgrLCLGQc/hGLPp+GQF83mPZ/F9tXzkPegfU4vHkJUmeOx9houSCuE4B3jWpjS4GMu3p/FuDtXqgsOIr6ihzUl2cLNFSeRUN1Ae7VXsD9qyVovH4JTTcq0XyrBo115bhbfQG1FzJQmrUfpWd24VbREdwpOoTrBftQkfk9StO3GApglCtCE65nruUFMz8gwrm4V0WozCHkorEmH02a82i5XoLWG5fx8E4VHt6txcP6q3hwuwotdaVovHIe96vO4l7ZGTSUHkd98WHUXzxEOIj6ojT4eziAF1rkfKqxuS/G06vHfzXy9HZo+2oikovmmjy01BKunEPr1UI8uF6EhzdK8dvtcvx2txq/37uKR/ev47f6WvHew7pitF4pQFNlFu5dOoF7JUcIh3Gv+BDukQiBXo7o2bMnkiOkvEU2rsepTYsmTklQigXQ0R/XoonIN1ZRCVzOQF3RUdy4eAI3S07jFs0It2hGaL5eSgJo8KhRJ8CtMjRQn7hRdBx1hRT9/L24lrcL1wm3C/eRAGmQ+7qKafIfcyfjxIZFxtUDTmxamHpwy3JRp0d/WIP6skxcPX8ENXn7tTiXhqqzB1CZewDl2VTXOQdwX1NCCbhGCagRopRn7iLsRGXWTlRR3ddkfY/abC2uZG+Hr6stYkOlyNi9Fic3LphlVBFgRzL3rINc6o7D36+ixpeJO5cycLs0nVxnZBDJLJoOc2k9kIfawpO4XpzZJoCm8DiuFaWjrphw4QjqqPnV5e/GDcJNwp3CvVi9YDLy9qxC5u7VOLVpocyoBDi1abH52X1ryZ3VKCMn66mRNVVlaxtaWQYayrNoVshFfRVNhdWFtCYowNWiDDy6RwmgfnC7/BzulucK0e4UH8edC2m4c36fQP2FfWgsPoCajC24eHQT8ug6P61ZbGJUAvBgzmxdqrlB7p3bv0643lKdQ6B6r8pCU3Uu9QWaCa4UounqRTReLUZFbppohCwAv3e/Oh/3K7Jx/3K6aH73i9MEGhkXqXRObsSVvL04tflLXgcY34PLoOjwP5Gz8x+4UXyCunkmWog8o5Vmg1bNLzQTXMRvNBM0XrlgIECVeO/htfNorTmLlvIMNJUeQWPJwTbyjRf345e934DPb3QN0NAKHtzhdXM1tef2o+HyaTSTCM1VmYJYq6YAD64VCbL1tCoUCWjQUAJ0AtBU2UpJaSk/TQIcfox8w/k9OLFxocaoyeuFOLouRV14aANulxwnAc4IEVprcmmOzycBLtB8X4o62iZX5OgEuFOpTYCGBchBc5lOAIo9O8+ozviOGt8io1v/d1mHxzYs1Fw7fwjNFRlCBCbWWnsOD66eJwFKUJ13SEyFIgFCgBI8oBJpocbZXJaOppJDbeRZgPzdf2/8aYcRNr6uFDi4NiW1NH0bLWlPCBFaqRlqBShEMy159WuB3xuuCAFYlAdUIkKAyydJAKp/nfvXcraT+18a5fq/ywTwNJW+dUljXeFBNFNTa9U1wgcU84fXi2lKzKOF0SFKAAlwu0IrAC2DW2gZ3HyZGqiBAOQ+Tu1Ybm58bf/fjIhTUHhwPRrJ0RbRB86KmD/kmeDmJXK+Cr/XswC0DyBRHlCP6CgAu396y1fGufl5FkfSVqdoNDR3t3AfoEbILrcLUEkC6DZCJEpHAbjzZ21bguzXqfY7inJk/bzU01uXUC84KRohk3yomwq59oUAtBFiUToKUHKI1vybv+D9xev7OLBqdmpe2gZcOLIJTRVZbQJot8UsQE2nAlRnbEPe7hU4s33p6y3AwbVzf6osPIHMXatQcnIbWmib/OAqrQWEABWdClCbsxOZ25eh/DSt/SkFp79b7PzaRuDExkXZdbQLrCg4JkS4cGwrmmupD9BiqDMBanL3kOvLaNOzgTZB+3Hp2Aac2rLYuHZ+z+PGqiUp2Q/qLtJuLwdVdJ/wwvFtyPh+BYpP78QN+r8C3gXepnsB1WfTkLNrJU5Sv/jl0HrcKNgjFkNHvluKxTMnvz4rQBYnOjraPDIycs1IpVrj6S9HQcY+tNISuJlmgJt0X6A0ay/O7F2Hw0Ru/7qF+OGbFPy4ai69XoJ8mjYr6WYI3wW6SSKMT1TDL3oilFHxGpVKlcrnfh4DXulnaYATw1SjsmWxk+GS/CXsx6+Ac9znUCpCsHPzSnFfkPcDfHOk/Ox+nDv8HY5uX4E9//wSaXT7+wwl4PyRjUIAvgs8LoHIK+NgN26FOBefk8/N1+BrvVJyXV2MHWFnFOrRjd6jZ8Nx3NdisIbwSPgMsuBQxKhV2LlpBUqz9wsBCqgfnPxxVZsAx7b9DdtWzkfK9IniuwU+ihjYJS974nx8Db5WaFR8oy4VJq9cDF3Md4SMGgv3pAVPDLKjCE5jl0AaMxUjQ1Xi+0BMcNzoWIyJi0K8OgKhIXLx/SD+3pBvRBJcEufDTgj5d9iPM8Ryek0Yuxx2BLfR8xE8KhlUcjteSXnQRUxY9Wcl3lEIJuSS9AU8ElPgE/shvEZNE3CLm0mk5wlSdmP/pkUy42sDLINdkh5L6ZgwZikkBNeEFMjVSS83EeHh4c4RqlGaJx0nh/6NU12SaiPExLSE9KQkY5YQOcJoxlftSEyFpA2LIUnQYjjBPXYm94hiHusLLQs+oUKd2OiYTAPUxe+FkUokck8hNTzhSwyPZ3zxOOIWYXgbFmJ4rBZ2sfMQokpofGEicOzDyXmHMam6CHblkoFTTyVFZDojFUcEuyBlG7sAAjGMvz6OUfNhKzBPC/U8SNRzKAnqF3PzlOd01/g5HVziGLZHTxB6UaRGEcFOSDExG/VcLaIZKe2IouOoOTrMpufZcFVNo+8oqv7cVppjFKQeB4lBBG05ap04xe936VQbocedsnkqKT0hfiZSqnZYqz6HdaQen9GxFlbhn8I6YlYbRqpG40/NDqRgtnM8RSquPX7Dwj/RRvAFkrImcl2RMiRkHTET1uHtYMJWYYxPBCxDpmFY6HRYKWfAOmwGHCI+FlPkH2qIERERsqBRE+AQvwCSmPlt8bMImdohfp07JQh15lTEZ1i3Ox38SJ6/ntzqQKgDKUFOyZhhACLJRHUYFvoxhik+hqV8MsxHTqLjj2BN7w0PnwGZaswfSwG5/5M71b5j7HxqKlRjOpcGS8c8GT+D2GkdezqpdbtOagWYu7ZTUoKQjhSTaUMIHYd82I5gcrwNU2FB5If4jSEhpsCa0jBc+RGcwqc9fwq4buRU+y7x8+EYkwJJ1OewIWIWwVMwyDtO1FrH+GmdMnSJjzu4pCO1dudxIUDSnNXwSpiDUdO/hrN6lpaYASnL4KkQkDOmtCOIjoMm6/ABLEdqMVSaDDP3aDp+H9aUBoliGhzCPoI8MuH5UsCd3yd2BlxiU+Co/hwSFblKNTVEmgQzrxhRaxy/rp0ycKkTUmt/OCoE+HHPfvz888/49ddf8ejRI6Sl58M5imo5qJ0UE7MgQhYyPSbRsQ6BE2HRhgkY4pOIAU6hsAycAOugSZCETIaDciq8wyfwjDDrmXtBuCpW4xGfApcY2uBEzYIk4hNRU4O8YmHmqYYFDdAweu0uGTj1mEvsVjupb78/IgRYunIjZi5eg282/zf+936TeC8j71+PkbIImAA9zAPGw9xfj3F0rIN0LMwJg73i8J6DHOa+o2EtmwCJfBIcFZPhpJwCZYT62b5ep5/6PGJnw4Vi6Rj5CSRhH8Oa4jTQOUxEjFXXRtDQKXKoM6cec4kdm4Bvd/wkyI7+dDn0pEaEfYjm1gfifWn8Z4JQO5Jh7qdHEob6EXwZY9rhMwaDPWLwnn0QhnrGwDpgLCQjJ8AxZBJclJMRGJaAZ1od8uJBGvMhPGJmwSXqUzjSVCIJnQZrqn8+uambSijfHj0iZeCSBbvUmVMGhNZsOyiITv3rah0xLamCf5WL9xOnL9ESI1JDfUa3wzsRQ9uQgCFeesTTcTwGuUXjPYkMg8goG/8xkMjGwlE+AS6KSfBUJD9TGXQjlU66Rc2Ah/pTuKimw5GcsVNQVw16H/1s/WDqEikGK+L3mEvkWJtL7JbepY5OjcZX3/4giBZc/J82QmHj5rYlwFc9XRAS8NQjjo518IjFkDbEYLC7FoNcVHhveCAGjQiBjV8iJAFJGBE0Fi4hE+AUNI6325upDwwjvNlVP3DkD/lEvg/P6OlwjfgII6iJ2AW/Dyv/JPRnAUhd4cpj8TNwif9m4NJQ7yedciBX6m7VCxH4mYVobvk/8XrrnmNErp3UYPdRRI7gxlAbIBqDXQ0RBTOncAwY7o+BBBvfOEj8EzFCRvcX5OMgVSbS75KkK4m4G0HSlQBu9J1+mUKhyB6pSoZb+GSMUHwAuyCqee8Y9LfxhamTUufO4/HTOmXgEh8buNSRlI9qKgqKLgvS/GABtu4++gSpQS5R5CxDpYUzI7IdThEYpIOZoxIDbKUYaOMDGxqvnV88nAMTEUi31pgTc9MJwCJ0+uiv/wD9tmeqUqnMDoqIp/qhxY831Red2NRRIZoNE3p+p6KIoJ6QISkDQkzOgNQgcpVhNoJBTZjhqIeSjnVwCIWZfQgG2PhhkK0PPGVqyJVq+p2iMpu5GBBn8n2fNiX21tUJf1AkwsfHJ5V+23csSBF5y1sWCSf/aEh89PH7g6QEoY6kDAgxMSbVBgURVMBUIKQddiGwdQuFs68SvrJwjJSH3eKx8pg7OM58zAndnnU9wI2CleJ/5KhXkE/KinKv4AvJFcoiaVDErYDgSHgERMKdYO8dgeGe4QJWbkRS79JzkDK1C4aVi0KQs3VTwM4jFO7SMAH/oHBIRypvyYNDi+iHlnt4LDymTgjzuHn8zKPLxvesgrByJgRTXUJsOsRKpIUHwmAHeGCG4DqkSBY9DSxqx39Hvy2crT9vJyRFUgk8Hu7wPD4e5zM7/awCdPU5VpbLhsE9hAdgKBIPTA/njqJ18Zo/Z/jvDMnxufk6+mv+qfH/Pz9r9qvYcZe2AAAAAElFTkSuQmCC";a=a||{};return $.create(c.serviceLocator.getUrl("MessageHandler-ReplyComment"),{idCase:a.idCase||"",idComment:a.idComment||"",comment:a.comment.replace(/\n/g,"<br>")||""}).pipe(function(a){if(a.comments)a.Replies=[a.comments];if(a.users){var c=[a.users];$.each(c,function(a){c[a].Picture=c[a].Picture==""?b:c[a].Picture});a.users=c}return a})},removeComment:function(a){var b=this;a=a||{};return $.destroy(b.serviceLocator.getUrl("MessageHandler-RemoveComment"),{idCase:a.idCase||"",idComment:a.idComment||""})},removeReply:function(a){var b=this;a=a||{};return $.destroy(b.serviceLocator.getUrl("MessageHandler-RemoveReply"),{idCase:a.idCase||"",idComment:a.idComment||"",idReply:a.idReply||""})},renameCommentCategory:function(a){var b=this;a=a||{};return $.update(b.serviceLocator.getUrl("MessageHandler-RenameCategoryColor"),{idColorCategory:a.idColorCategory>=0?a.idColorCategory:"",colorName:a.colorName||""})},setCommentCategory:function(a){var b=this;a=a||{};return $.update(b.serviceLocator.getUrl("MessageHandler-SetCategoryToComment"),{idCase:a.idCase||"",idComment:a.idComment||"",idColorCategory:typeof a.idColorCategory=="number"?a.idColorCategory:""})},getCommentsCategories:function(){var a=this;return $.read(a.serviceLocator.getUrl("MessageHandler-GetCategoryColors")).pipe(function(a){return a.length>1&&a[0].categories?a[0]:a})},getNewComments:function(a){var b=this;a=a||{};return $.read(encodeURI(b.serviceLocator.getUrl("MessageHandler-CountNewComments")),{idCase:a.idCase||0,idComment:a.idComment||0})},getAnalisysQueries:function(){var a=this;return $.read(a.serviceLocator.getUrl("bamAnalytics-handler-getAnalisysQueries"))},updateQueries:function(a){var b=this;return $.update(b.serviceLocator.getUrl("bamAnalytics-handler-updateQuery"),{idQuery:a.idQuery>=0?a.idQuery:"",queryName:a.queryName||"",queryDescription:a.queryDescription||""})},deleteQueries:function(b){var c=this,a={};a.action="1612";a.QueryId=b||"";return $.destroy(c.serviceLocator.getUrl("reports-handler-deleteQueries"),a)},logout:function(){var b=this;sessionStorage.removeItem("bizagiAuthentication");try{var a=$.create(b.serviceLocator.getUrl("logout"));$.when(a).done(function(){location.reload()}).fail(function(){bizagi.log("Error logging out")})}catch(c){}},logoutMobile:function(){var a=this;sessionStorage.removeItem("bizagiAuthentication");var b=a.serviceLocator.getUrl("logout");return $.read(b)},getOrganizationInfo:function(b){var c=this,a={};a.objectType=b;return $.ajax({url:c.serviceLocator.getUrl("massive-activity-assignments-getOrganizationInfo"),data:a,type:"GET",dataType:"json"})},getCasesByOrganization:function(a){var c=this,b={};b.roles=a.roles?"["+a.roles.toString()+"]":"[]";b.skills=a.skills?"["+a.skills.toString()+"]":"[]";b.locations=a.locations?"["+a.locations.toString()+"]":"[]";return $.ajax({url:c.serviceLocator.getUrl("massive-activity-assignments-getCasesByOrganization"),data:b,type:"GET",dataType:"json"})},reassignCases:function(a){var c=this,b={};b.users=a.user?"["+a.user.toString()+"]":"[]";b.roles=a.roles?"["+a.roles.toString()+"]":"[]";b.skills=a.skills?"["+a.skills.toString()+"]":"[]";b.locations=a.locations?"["+a.locations.toString()+"]":"[]";return $.ajax({url:c.serviceLocator.getUrl("massive-activity-assignments-reassignCases"),data:b,type:"POST",dataType:"json"})},searchUsers:function(b){var c=this,a={};a.userName=b.userName||"";a.fullName=b.fullName||"";a.domain=b.domain||"";return $.ajax({url:c.serviceLocator.getUrl("massive-activity-assignments-searchUsers"),data:a,type:"GET",dataType:"json"})},getDomainList:function(){var a=this,b=a.serviceLocator.getUrl("domains");return $.read(b)},getEcmAllScheduledJobs:function(){var b=this,a={};a.action="getEcmAllScheduledJobs";return $.read(b.serviceLocator.getUrl("async-ecm-upload-baseService"),a)},retryECMPendingScheduledJob:function(b){var c=this,a={};a.action="retryECMPendingScheduledJob";a.idJob=b;return $.read(c.serviceLocator.getUrl("async-ecm-upload-baseService"),a)},getWorkPortalVersion:function(){var a=this;return $.ajax({url:a.serviceLocator.getUrl("WorkPortalVersion"),type:"GET",dataType:"json"}).always(function(a){a=a||{};if(a.version=="")a.version=bizagi.loader.productBuild||"";return a})},getAuthenticationLogData:function(b){var d=this,c={},a="";if(b.dataType=="domains")a="admin-getAuthenticationDomains";else if(b.dataType=="events")a="admin-getAuthenticationEventsTypes";else a="admin-getAuthenticationEventSubTypes";return $.ajax({url:d.serviceLocator.getUrl(a),data:c,type:"GET",dataType:"json"})},getAuthenticationLogResult:function(b){var c=this,a={};a.action=b.action;a.domain=b.domain;a.userName=b.userName;a.dtFrom=b.dtFrom;a.dtTo=b.dtTo;a.eventSubType=b.eventSubType;a.eventType=b.eventType;a.pag=b.pag;a.pagSize=b.pagSize;return $.ajax({url:c.serviceLocator.getUrl("admin-getAuthenticationLog"),data:a,type:"GET",dataType:"json"})},encryptString:function(a){var c=this,b={};a=a||{};b.entry=a.entry;return $.ajax({url:c.serviceLocator.getUrl("admin-EncryptString"),data:b,type:"POST",dataType:"json"})},userPendingRequests:function(a){var b=this;return $.read(b.serviceLocator.getUrl("admin-UserPendingRequests"),{pag:a.pag,pagSize:a.pagSize})},userAuthenticationInfo:function(a){var b=this;return $.read(b.serviceLocator.getUrl("admin-UserAuthenticationInfo"),{idUser:a.idUser})},updateUserAuthenticationInfo:function(a){var b=this;return $.create(b.serviceLocator.getUrl("admin-updateUserAuthenticationInfo"),{idUser:a.idUser,password:a.password,enable:a.enable,expired:a.expired,locked:a.locked})},generateRandomPassword:function(a){var b=this;a=a||{};a.action="generateRandomPassword";return $.ajax({url:b.serviceLocator.getUrl("admin-generateRandomPassword"),data:a,type:"POST",dataType:"json"})},generateDataToSendByEmail:function(a){var c=this;a=a||{};var b={};b.action="GenerateDataToSendByEmail";b.idUser=a.idUser;b.password=a.password;return $.ajax({url:c.serviceLocator.getUrl("admin-GenerateDataToSendByEmail"),data:a,type:"POST",dataType:"json"})},sendEmail:function(a){var c=this;a=a||{};var b={};b.action="sendEmail";b.emailTo=a.emailTo;b.subject=a.subject;b.body=a.body;return $.ajax({url:c.serviceLocator.getUrl("admin-sendEmail"),data:a,type:"POST",dataType:"json"})},getApplicationCategoriesList:function(c){var d=this,a={},b="";if(c.action=="Applications"){b="admin-getApplicationList";return $.ajax({url:d.serviceLocator.getUrl(b),type:"GET",dataType:"json"})}else{b="admin-getCategoriesList";a.idApp=c.idApp;a.idCategory=c.idCategory;if(a.idCategory=="")a.idCategory=-1;return $.ajax({url:d.serviceLocator.getUrl(b),data:a,type:"GET",dataType:"json"})}},getAdminCasesList:function(a){var c=this,b=a||{};return $.ajax({cache:true,url:c.serviceLocator.getUrl("admin-getCasesList"),data:b,type:"GET",dataType:"json"})},getApplicationProcesses:function(a){var c=this,d=c.serviceLocator.getUrl("admin-getApplicationProcesses"),b={};b.idApp=a.idApp?a.idApp:-1;return $.read(d,b)},getProcessVersion:function(b){var c=this,d=c.serviceLocator.getUrl("admin-getProcessVersion"),a={};a.idWfClass=b.idWFClass;return $.read(d,a)},getProcessTasks:function(a){var c=this,d=c.serviceLocator.getUrl("admin-getProcessTasks"),b={};b.idWfClass=a.idWFClass;b.version=a.version?a.version:undefined;return $.read(d,b)},getTaskAlarms:function(b){var c=this,d=c.serviceLocator.getUrl("admin-getTaskAlarms"),a={};a.idTask=b.idTask;return $.read(d,a)},getLapseMode:function(){var a=this,b=a.serviceLocator.getUrl("admin-getLapseMode");return $.read(b)},getRecurrMode:function(){var a=this,b=a.serviceLocator.getUrl("admin-getRecurrMode");return $.read(b)},getScheduleType:function(){var a=this,b=a.serviceLocator.getUrl("admin-getScheduleType");return $.read(b)},getBossList:function(){var a=this,b=a.serviceLocator.getUrl("admin-getBossList");return $.read(b)},addAlarm:function(b){var c=this,d=c.serviceLocator.getUrl("admin-addAlarm"),a={};a.idTask=b.idTask;a.idRecurrMode=b.idRecurrMode;a.idLapseMode=b.idLapseMode;a.scheduleType=b.scheduleType;a.alarmTime=b.alarmTime;a.alarmRecurrTime=b.alarmRecurrTime;a.sendToCurrentAssignee=b.sendToCurrentAssignee;return $.update(d,a)},editAlarm:function(b){var c=this,d=c.serviceLocator.getUrl("admin-editAlarm"),a={};a.idTask=b.idTask;a.idAlarm=b.idAlarm;a.idRecurrMode=b.idRecurrMode;a.idLapseMode=b.idLapseMode;a.scheduleType=b.scheduleType;a.alarmTime=b.alarmTime;a.alarmRecurrTime=b.alarmRecurrTime;a.sendToCurrentAssignee=b.sendToCurrentAssignee;return $.update(d,a)},deleteAlarm:function(a){var c=this,d=c.serviceLocator.getUrl("admin-deleteAlarm"),b={};b.idAlarm=a.idAlarm;return $.destroy(d,a)},getAlarmRecipients:function(b){var c=this,d=c.serviceLocator.getUrl("admin-alarmRecipients"),a={};a.idAlarm=b.idAlarm;return $.read(d,a)},addRecipientToAlarm:function(b){var c=this,d=c.serviceLocator.getUrl("admin-recipientToAlarm"),a={};a.idAlarm=b.idAlarm;a.idRecipient=b.idRecipient;return $.update(d,a)},deleteRecipientsFromAlarm:function(b){var c=this,d=c.serviceLocator.getUrl("admin-deleteAlarmRecipients"),a={};a.idRecipients=b.idRecipients;return $.destroy(d,a)},enableAlarm:function(b){var c=this,d=c.serviceLocator.getUrl("admin-enableAlarm"),a={};a.idTask=b.idTask;return $.create(d,a)},abortReassignItems:function(a){var c=this,b=a||{},d=a.action=="abort"?"admin-abortItems":"admin-reassignItems";return $.ajax({url:c.serviceLocator.getUrl(d),data:b,type:"POST",dataType:"json"})},asyncActivitiesServices:function(b){var c=this,f=b,a="",d="GET";if(b.action=="getActivities")a=c.serviceLocator.getUrl("admin-async-activities-get-activities");else if(b.action=="retryNow"){d="POST";a=c.serviceLocator.getUrl("admin-async-activities-get-retry-now");a=a.replace("{idCase}",b.idCase);a=a.replace("{idworkItem}",b.idWorkitem)}else if(b.action=="getActivitiesByTask")a=c.serviceLocator.getUrl("admin-async-activities-get-activities-by-task");else if(b.action=="enableExecution"){d="POST";a=c.serviceLocator.getUrl("admin-async-activities-enable-execution")}else if(b.action=="enableMultiple"){a=c.serviceLocator.getUrl("admin-async-activities-enable-multiple");d="POST"}else if(b.action=="asyncExecution")a=c.serviceLocator.getUrl("admin-async-activities-async-execution");else if(b.action=="asyncExecutionLog"){a=c.serviceLocator.getUrl("admin-async-activities-async-execution-log");a=a.replace("{idCase}",b.idCase);a=a.replace("{idworkItem}",b.idWorkItem)}else if(b.action=="getAsyncExecution")a=c.serviceLocator.getUrl("admin-async-activities-async-get-current-execution-log");var e=$.ajax({url:a,data:f,type:d,dataType:"json"});return e},getDefaultAssignationUserToAllProcess:function(){var a=this;return $.read(a.serviceLocator.getUrl("admin-getDefaultAssignationUserToAllProcess"),{serviceAction:"getDefaultAssignationUserToAllProcess"})},getDefaultAssignationUserToProcess:function(b){var c=this,a;if(b.idWFClass=="")a=-1;else a=b.idWfClass;return $.read(c.serviceLocator.getUrl("admin-getDefaultAssignationUserToProcess"),{serviceAction:"getDefaultAssignationUserToProcess",process:a})},setDefaultAssignationUserToProcess:function(a){var b=this;if(a.idWFClass=="")process=-1;else process=a.idWfClass;return $.create(b.serviceLocator.getUrl("admin-setDefaultAssignationUserToProcess"),{serviceAction:"setDefaultAssignationUserToProcess",process:process,idUser:a.idUser})},getProfilesTypes:function(){var a=this;return $.read(a.serviceLocator.getUrl("admin-getProfilesTypes"))},searchProfiles:function(b){var c=this,d=c.serviceLocator.getUrl("admin-searchProfiles"),a={};a.type=b.profileType;a.name=b.profileName;return $.read(d,a)},getUsersByProfile:function(b){var c=this,d=c.serviceLocator.getUrl("admin-getUsersByProfile"),a={};a.type=b.profileType;a.id=b.idProfile;return $.read(d,a)},removeUserFromProfile:function(b){var c=this,d=c.serviceLocator.getUrl("admin-removeUserFromProfile"),a={};a.type=b.profileType;a.id=b.idProfile;a.idUser=b.idUser;return $.destroy(d,a)},addUserToProfile:function(b){var c=this,d=c.serviceLocator.getUrl("admin-addUserToProfile"),a={};a.type=b.profileType;a.id=b.idProfile;a.idUser=b.idUser;return $.update(d,a)},licenses:function(){var a=this,b=a.serviceLocator.getUrl("admin-Licenses");return $.read(b)},getDimensions:function(){var a=this,b=a.serviceLocator.getUrl("admin-GetDimensions");return $.read(b)},editDimension:function(a){var c=this;a=a||{};var b={};b.id=a.id;b.displayName=a.displayName;b.name=a.name;b.idWfClass=a.idWfClass;b.entityPath=a.entityPath;b.description=a.description;var d=c.serviceLocator.getUrl("admin-EditDimension");return $.create(d,b)},createAdministrableDimension:function(a){var c=this;a=a||{};var b={};b.displayName=a.displayName;b.name=a.name;b.idWfClass=a.idWfClass;b.entityPath=a.entityPath;b.Description=a.description;return $.ajax({url:c.serviceLocator.getUrl("admin-CreateAdministrableDimension"),data:a,type:"PUT",dataType:"json"})},deleteDimension:function(a){var c=this,d=c.serviceLocator.getUrl("admin-DeleteDimension");a=a||{};var b={};b.id=a.id;b.idDimension=a.id;b.administrable=a.administrable;return $.destroy(d,b)},entityPathChildNodesAction:function(a){var c=this;a=a||{};var b={};b.pathNodeType=a.nodeType;b.idNode=a.idNode;b.nodeDisplayPath=a.nodeDisplayPath;b.nodePath=a.nodePath;b.idWfClass=a.idWfClass;return $.ajax({url:c.serviceLocator.getUrl("admin-EntityPathChildNodesAction"),data:a,type:"GET",dataType:"json"})},getActiveWFClasses:function(a){var c=this,b=a||{};return $.ajax({cache:true,url:c.serviceLocator.getUrl("admin-GetActiveWFClasses"),data:b,type:"GET",dataType:"json"})},storeDocumentTemplates:function(){var a=this,b=a.serviceLocator.getUrl("admin-document-templates-storeDocumentTemplates");return $.read(b)},restoreDocumentTemplates:function(b){var c=this,a=c.serviceLocator.getUrl("admin-document-templates-restoreDocumentTemplates");a+="?Guid="+b.Guid;return $.read(a)},getWorkFlowClasses:function(){var a=this,b=a.serviceLocator.getUrl("admin-processes-workflowClasses");return $.read(b)},getTaskByWorkFlow:function(b){var c=this,d=c.serviceLocator.getUrl("admin-processes-tasksByWorkflow"),a={};a.idWorkflow=b.idWorkflow;return $.read(d,a)},modifyProcessDuration:function(b){var c=this,d=c.serviceLocator.getUrl("admin-processes-modifyProcessDuration"),a={};a.idWorkflow=b.idWorkflow;a.duration=b.duration;return $.create(d,a)},modifyTaskDuration:function(b){var c=this,d=c.serviceLocator.getUrl("admin-processes-modifyTaskDuration"),a={};a.idTask=b.idTask;a.duration=b.duration;return $.create(d,a)},processesHierarchy:function(){var b=this,a={};a.removeOnlineItems="true";return $.ajax({url:b.serviceLocator.getUrl("offline-getProcessTree"),data:a,dataType:"json",type:"GET"})},syncOfflineCases:function(a){var b=this;return $.ajax({url:b.serviceLocator.getUrl("offline-sendForm"),data:{idCase:a.idCase,idWFClass:a.idWfClass,awCaseCreationContext:JSON.stringify(a.objToSend),idWorkflow:a.idWfClass},type:"POST",dataType:"json",serviceType:"LOAD"})},processesHierarchyTofetchForms:function(b){var c=this,a={};a.idChangeSet=b.changeSet;return $.ajax({url:c.serviceLocator.getUrl("offline-getForms"),data:a,dataType:"json"})},getDataForMyTeam:function(){var a=this;return $.ajax({url:a.serviceLocator.getUrl("bam-resourcemonitor-myteam"),type:"GET",dataType:"json"})},getReporstAnalysisQuery:function(){var a=this;return $.read(a.serviceLocator.getUrl("reports-analysisquery"))},updateReportData:function(a){var b=this;return $.update(b.serviceLocator.getUrl("reports-analysisquery-update"),a)},deleteReportData:function(a){var b=this;return $.destroy(b.serviceLocator.getUrl("reports-analysisquery-delete")+"?"+a)},getStoreLanguageTemplates:function(){var b=this,c=b.serviceLocator.getUrl("admin-language-languages"),a={justActives:false};return $.read(c,a)},getLanguageTemplate:function(b){var c=this,a=$.Deferred(),d=c.serviceLocator.getUrl("admin-language-resource")+"?cultureName="+b.cultureName;a.resolve("OK");window.location.href=d;return a.promise()},processDefinition:function(a){var b=this;return $.read(b.serviceLocator.getUrl("processviewer-processdefinition"),a)},graphicInfo:function(a){var b=this;return $.read(b.serviceLocator.getUrl("processviewer-processgraphicinfo"),a)},getLastUpdateByMobile:function(){var a=this;return $.read({url:a.serviceLocator.getUrl("mobile-getLastUpdate")})}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\bizagi.workportal.services.routing.js */ 
$.Class.extend("bizagi.workportal.services.routing",{},{init:function(b){var a=this;b=b||{};a.params=b.params||{};a.dataService=b.dataService||{};a.modules={render:"activityform",oldRender:"oldrenderintegration",async:"async",activitySelector:"routing"};a.renderVersion=2;a.resetRouteInformation()},resetRouteInformation:function(){var a=this;a.route={module:"",moduleParams:{}}},setRenderParams:function(b){var a=this;a.params=a.getRenderParams(b)},getRenderParams:function(a){var d=this,c={idCase:parseInt(a.idCase||0),idWorkflow:parseInt(a.idWorkflow||0),idWorkitem:a.isOfflineForm===true?0:parseInt(a.idWorkitem||0),idTask:parseInt(a.idTask||0),eventAsTasks:a.eventAsTasks||false,onlyUserWorkItems:a.onlyUserWorkItems||"true",formsRenderVersion:a.formsRenderVersion||0,referrer:a.referrer||"",isComplex:a.isComplex!==undefined?true:false,formsRenderVersion:2,onClose:a.onClose||"",isOfflineForm:a.isOfflineForm||false,radNumber:a.radNumber||null},b=true;try{b=BIZAGI_SETTINGS.skipSubprocessGlobalForm||true}catch(e){}d.skipSubprocessGlobalForm=b==="true";return c},getRoute:function(c){var a=this,b=new $.Deferred;a.params=c?c:a.params;var e=c.fromTask||c.idTask||"",d=c.fromWorkItemId||c.idWorkItem||"";$.when(a.getRenderVersion(a.params)).done(function(c){a.renderVersion=c;a.route.moduleParams=a.getRenderParams(a.params);if(a.route.moduleParams.idWorkitem>0){a.setRenderModuleName(a.getRenderModuleName(c));if(a.route.moduleParams.idTask==0)a.route.moduleParams.idTask=a.searchIdTask(a.route.moduleParams.idWorkitem);b.resolve(a.route);a.resetRouteInformation()}else $.when(a.dataService.getWorkitems({idCase:a.params.idCase,onlyUserWorkItems:a.params.onlyUserWorkItems,isOfflineForm:a.params.isOfflineForm,fromTask:e,fromWorkItemId:d})).done(function(d){var g=a.checkAsyncWorkItems(d);if(d.workItems.length==0){a.route.moduleParams.idWorkflow=d.idWorkFlow;if(d.msgTask!=="")b.resolve(a.routeToMessageForm(d.msgTask));else if(bizagi.util.parseBoolean(d.hasGlobalForm)){a.route.moduleParams.displayName=null;b.resolve(a.routeToGlobalForm(d))}else b.resolve(a.routeToGlobalFormWithoutData())}else if(d.workItems.length==1||g.length>0){var e=0;if(g.length>0)for(var f=0,h=d.workItems.length;f<h;f++)if(d.workItems[f].isAsynch=="true"){e=f;break}a.route.moduleParams.idCase=d.workItems[e].idCase;a.route.moduleParams.idTask=d.workItems[e].idTask;a.route.moduleParams.fromTask=d.workItems[e].idTask;a.route.moduleParams.idWorkitem=d.workItems[e].idWorkItem;a.route.moduleParams.idWorkflow=d.workItems[e].idWorkFlow;a.route.moduleParams.displayName=d.workItems[e].displayName?d.workItems[0].displayName:"";a.route.moduleParams.radNumber=d.radNumber||"";if(g.length>0)a.setRenderModuleName(a.modules.async);else a.setRenderModuleName(a.getRenderModuleName(c));b.resolve(a.route);a.resetRouteInformation()}else{d.checkProcess=d.subProcesses.length>0&&a.skipSubprocessGlobalForm?true:false;d.checkWorkItems=d.workItems.length>1?true:false;d.fromSearchWidget=a.params.referrer=="search"?true:false;a.setRenderModuleName(a.modules.activitySelector);a.route.moduleParams.data=d;b.resolve(a.route);a.resetRouteInformation()}}).fail(function(){var c=bizagi.localization.getResource("workportal-menu-search-found-no-cases");b.resolve(a.routeToMessageForm(c))})});return b.promise()},setRenderModuleName:function(a){var b=this;b.route.module=a;b.route.moduleParams.widgetName=a},getRenderModuleName:function(b){var a=this;return b==1?a.modules.oldRender:a.modules.render},searchIdTask:function(a,d){var c=0;a=a||[];for(var b=0;b<a.length;b++)if(parseInt(a[b].idWorkItem)==parseInt(d)){c=parseInt(a[b].idTask);break}return c},checkAsyncWorkItems:function(a){var b=[];$.each(a.workItems,function(c){a.workItems[c].isAsynch=="true"&&b.push({idCase:a.idCase,idWorkitem:a.workItems[c].idWorkItem,idTask:a.workItems[c].idTask})});return b},routeToGlobalForm:function(b){var a=this;a.route.moduleParams.idCase=$.isNumeric(b.idCaseForGlobalForm)&&b.idCaseForGlobalForm>0?b.idCaseForGlobalForm:a.route.moduleParams.idCase;if(a.renderVersion==1){var c=a.dataService.serviceLocator.getUrl("old-render")+"?PostBack=1&idCase=%s&idWorkitem=%s&idTask=%s",e=a.route.moduleParams.idCase||"",d=a.route.moduleParams.idWorkitem||"",f=a.route.moduleParams.idTask||"",g=printf(c,e,d,f);a.route.moduleParams.onlyUserWorkItems=a.route.moduleParams.idCase||"";a.setRenderModuleName(a.modules.oldRender);a.route.moduleParams.url=g}else a.setRenderModuleName(a.modules.render);return a.route},routeToGlobalFormWithoutData:function(){var a=this;a.setRenderModuleName(a.modules.render);a.route.moduleParams.withOutGlobalForm=true;return a.route},routeToMessageForm:function(b){var a=this;b=b||"";a.setRenderModuleName(a.modules.render);a.route.moduleParams.withOutGlobalForm=true;a.route.moduleParams.messageForm=b;return a.route},getRenderVersion:function(c){var d=this,b=new $.Deferred,a=c.formsRenderVersion||0;if(a!=0)b.resolve(a);else $.when(d.dataService.getCaseFormsRenderVersion({idCase:c.idCase})).done(function(c){a=parseInt(c.formsRenderVersion);b.resolve(a)});return b.promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\bizagi.workportal.services.service.offline.js */ 
bizagi.workportal.services.service.extend("bizagi.workportal.services.service",{},{init:function(b){var a=this;a._super(b);a.database=typeof bizagi.workportal.services.db!="undefined"?new bizagi.workportal.services.db:null;a.enableOfflineEvents()},enableOfflineEvents:function(){var a=this;if(bizagi.util.isTabletDevice()){if(typeof navigator.connection!="undefined")if(typeof Connection!=="undefined"&&(navigator.connection.type==Connection.NONE||navigator.connection.type=="unknown"))a.online=false;else a.online=true;else{var b=bizagi.readQueryString(),c=b&&b.online?eval(b.online):true;a.online=c}$(document).off("online.services");$(document).on("online.services",function(){a.online=true;bizagi.log("online.services");a.returnOnlineStatus()});$(document).off("offline.services");$(document).on("offline.services",function(){bizagi.log("offline.services");a.online=false})}else if(typeof Windows!="undefined"){a.online=winBizagi.network.isOnline();document.addEventListener("networkStatusChange",function(b){a.online=b.detail.onLine;a.online&&a.returnOnlineStatus()})}$(document).off("tryPushData.offline");$(document).on("tryPushData.offline",function(){a.online&&a.pushOfflineData()})},returnOnlineStatus:function(){var a=this;$.when(a.logoutUser()).always(function(){$.when(a.authenticatedUser()).always(function(){$.when(a.pushOfflineData()).always(function(){a.fetchOfflineData()})})})},fetchOfflineData:function(){var b=this;bizagi.log("fetchOfflineData");var a=new bizagi.workportal.services.fetchWorker(this);$.when(a.fetch()).done(function(){bizagi.log("Sync Completed");var a=bizagi.util.getItemLocalStorage("formChangesetadmon-http://localhost/bizagiR100x-domain");(bizagi.util.isTabletDevice()||typeof Windows!=="undefined")&&bizagi.util.showNotification({text:"Successful Synchronization"})}).fail(function(a){bizagi.log("Sync Failed");if(bizagi.util.isTabletDevice()||typeof Windows!="undefined")a!==0&&bizagi.util.showNotification({text:"FAILED Synchronization"})})},pushOfflineData:function(){var b=this;bizagi.log("pushOfflineData");var a=new bizagi.workportal.services.pullWorker(this);return $.when(a.pull()).then(function(){bizagi.log("Offline cases syncronized, all data sended")},function(){bizagi.log("Error syncronizing cases")})},getCurrentUser:function(c){var b=this;if(b.online)return b._super(c);else{var a=new $.Deferred,d={idUser:1,user:typeof BIZAGI_USER!="undefined"?BIZAGI_USER:"admon",userName:typeof BIZAGI_USER!="undefined"?BIZAGI_USER:"admon",domain:typeof BIZAGI_DOMAIN!="undefined"?BIZAGI_DOMAIN:"domain",delegateUserName:"",idDelegateUser:-1,groupSeparator:",",language:"en-US",decimalSeparator:".",decimalDigits:"2",symbol:"$",ShortDateFormat:"M/d/yyyy",TimeFormat:"h:mm tt",LongDateFormat:"dddd, MMMM d, yyyy",twoDigitYearMax:2029,twoDigitYearMaxDelta:16,isMultiOrganization:"false"};a.resolve(d)}return a.promise()},getAllProcesses:function(a){var b=this;a.inputtray=a.inputtray||"inbox";return a.inputtray=="inbox"?b._super(a):$.when(b.database.getAllProcesses(a)).pipe(function(c){c=JSON.parse(c);if(!!a.skipAggrupate)return c;var e={},b={},d=bizagi.localization.getResource("workportal-widget-inbox-all-processes"),f={name:bizagi.localization.getResource("workportal-widget-inbox-all-cases"),path:"",category:d,isFavorite:a.onlyFavorites||false,guidFavorite:"",idworkflow:"",guidWFClass:"",count:0};b[d]=[];b[d].workflows=[];b[d].workflows.push(f);c.workflows&&$.each(c.workflows.workflow,function(c,a){if(!b[a.category]){b[a.category]=[];b[a.category].workflows=[]}b[a.category].workflows.push(a);f.count+=Number(a.count)});e.categories=[];for(key in b)e.categories.push({name:key,workflows:b[key].workflows});return e})},getCasesByWorkflow:function(a){var c=this;a.inputtray=a.inputtray||"inbox";if(a.inputtray=="inbox")return c._super(a);else{var b=new $.Deferred;$.when(c.database.getCustomizedColumnsData(a)).pipe(function(a){a=JSON.parse(a);$.each(a.cases.rows,function(b,d){var c=[];a.cases.rows[b].radnumber=d.id;$.each(d.fields,function(e,d){if(d!=undefined)try{if(d.isRadNumber!=undefined&&d.isRadNumber=="true")a.cases.rows[b].radnumber=d.Value;else if(d.workitems!=undefined)a.cases.rows[b].workitems=d.workitems;else c.push(d)}catch(f){}});a.cases.rows[b].fields=c});b.resolve(a.cases)});return b.promise()}},getMenuAuthorization:function(){var b=this;if(b.online)return b._super();else{var a=new $.Deferred;a.resolve(JSON.parse('{"permissions":[{"Cases":["NewCase","Pending","Closed","Search"]},{"AnalysisReports":["BAMProcess","BAMTask","AnalyticsProcess","AnalyticsTask","AnalyticsSensor","AnalysisQueries","BAMResourceMonitor"]},{"Admin":["UserAdmin","Licenses","EntityAdmin","CaseAdmin","AlarmAdmin","EncryptionAdmin","MobileUpdatesAdmin","AsynchronousWorkitemRetries","UserPendingRequests","AuthenticationLogQuery","BusinessPolicies","Profiles","UserDefaultAssignation","GRDimensionAdmin","ThemeBuilder"]},{"CurrentUserAdministration":["CurrentUser"]},{"LogOut":[]},{"Search":[]},{"SmartFolders":[]},{"CustomFolders":[]},{"BizagiQueries":[]},{"AnalysisQueries":[]},{"StateLog":[]},{"PrintableVersion":[]}]}'));return a.promise()}},getInboxSummary:function(a){var d=this,b=d.online;if(a&&(a.inputtray=="false"||a.inputtray=="true"))b=false;if(b)return d._super(a);else{var c=new $.Deferred;$.when('{"inboxSummary":{"Red":7,"Yellow":0,"Green":0,"Black":0,"All":7}}').pipe(function(a){var b=JSON.parse(a);c.resolve(b.inboxSummary)});return c.promise()}},getCategories:function(a){var c=this;if(c.online)return c._super(a);else{a=a||{};var b={};if(a.idCategory)b.idCategory=a.idCategory;if(a.idApp)b.idApp=a.idApp;b.groupByApp=a.groupByApp||false;return c.database.getCategories(b)}},createNewCase:function(a){var b=this;a=a||{};return!a.isOfflineForm?b._super(a):b.database.createNewCase(a)},getWorkitems:function(a){var b=this;a=a||{};return!a.isOfflineForm?b._super(a):b.database.getWorkitems(a)},getCaseSummary:function(a){var b=this;a=a||{};return!a.isOfflineForm?b._super(a):b.database.getSummary(a)},deleteCase:function(a){var b=this;a=a||{};return b.database.deleteCase(a)},getCaseSubprocesses:function(a){var c=this;a=a||{};if(!a.isOfflineForm)return c._super(a);else{var b=new $.Deferred;b.resolve({subProcesses:[]});return b.promise()}},getOverrides:function(){return{}},getCasesListBeta:function(a){var b=this;a=a||{};a.inputtray=a.inputtray||"inbox";if(a.inputtray=="inbox")return b._super(a);else{var c=new $.Deferred;$.when(b.database.getCasesList(a)).pipe(function(a){a=JSON.parse(a);var d=b.getTaskFeedData(a);c.resolve(d)});return c.promise()}},getTaskFeedData:function(g){for(var c={elements:[],page:g.page,totalPages:g.totalPages},h=new Date,b=h.getMonth(),d=h.getDate(),l=h.getFullYear(),i=0;g.elements.length>i;i++){var a=g.elements[i];if(a[3]!=""){var j=new Date(a[3]),f=j.getDate(),e=j.getMonth(),m=j.getFullYear(),k=new Date(m,e,f,0,0,0)-new Date(l,b,d,0,0,0);if(k<0)a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"dd MMM");else if(b===e&&d==f)a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"hh:mm");else if(b===e&&d+1==f)a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"hh:mm");else a[3]=bizagi.util.dateFormatter.formatDate(new Date(a[3]),"dd MMM")}else a[3]="";if(k<0)c.elements.push($.extend({},a,{group:"1|"+bizagi.localization.getResource("workportal-taskfeed-overdue"),state:"red"}));else if(b===e&&d==f)c.elements.push($.extend({},a,{group:"2|"+bizagi.localization.getResource("workportal-taskfeed-today"),state:"yellow"}));else if(b===e&&d+1==f)c.elements.push($.extend({},a,{group:"3|"+bizagi.localization.getResource("workportal-taskfeed-tomorrow"),state:"yellow"}));else c.elements.push($.extend({},a,{group:"4|"+bizagi.localization.getResource("workportal-taskfeed-upcomming"),state:"green"}))}return c},summaryCaseDetails:function(b){var d=this,a=b||{};a.inputtray=a.inputtray||"inbox";if(!a.isOfflineForm)return d._super(a);else{var e=$.Deferred(),c=b.idWorkitem||0;$.when(d.getCaseSummaryDataContent(b)).done(function(a){a.showEvents=a.countEvents>=1?true:false;a.showParentProcess=a.idParentCase>=1?true:false;a.parentProcess={displayName:a.parentDisplayName,idCase:a.idParentCase};a.isClosed=a.isOpen=="true"?false:true;a.showAssignees=a.countAssigness>=1?true:false;a.showSubProcess=a.countSubProcesses>=1?true:false;a.showForm=a.hasGlobalForm=="true"?true:false;a.allowsReassign="false";for(var d=0;d<a.currentState.length;d++)if(a.currentState[d].idWorkItem==c)a.allowsReassign=a.currentState[d].allowsReassign;for(var f=[],g=0,b=0;b<a.currentState.length;b++)if(a.currentState[b].isEvent=="false"&&a.currentState[b].assignToCurrentUser=="true"&&a.currentState[b].idWorkItem!=c)f[g++]=a.currentState[b];a.currentStateTypes=f;a.showActivities=f.length>=1?true:false;e.resolve(a)});return e.promise()}},getCaseSummaryDataContent:function(c){var b=this,a=new $.Deferred;$.when(b.getCaseSummary(c)).done(function(c){c.taskState=b.icoTaskState,c.createdByName=c.createdBy.Name;c.createdByUserName=c.createdBy.userName;c.caseDescription=c.caseDescription==""?"":c.caseDescription;c.processPath=c.processPath.replace(/\//g," > ")+c.process;c.showWorkOnIt=true;a.resolve(c)});return a.promise()},getCustomizedColumnsData:function(a){var c=this;a.inputtray=a.inputtray||"inbox";if(a.inputtray=="inbox")return c._super(a);else{var b=new $.Deferred;$.when(c.database.getCustomizedColumnsNewData(a)).pipe(function(a){b.resolve(JSON.parse(a))});return b.promise()}},getRecentProcesses:function(a){var b=this;a=a||{};return b.online?b._super(a):{processes:[]}}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\bizagi.workportal.services.routing.offline.js */ 
bizagi.workportal.services.routing.extend("bizagi.workportal.services.routing",{},{init:function(a){this._super(a)},getRoute:function(a){var b=this,d=new $.Deferred;b.params=a?a:b.params;if(typeof a.offlineAction!="undefined"&&a.offlineAction=="next"){var c=new $.Deferred;c.resolve(b.routeToGlobalFormWithoutData());return c.promise()}else return b._super(a)}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\database\bizagi.clientdb.js */ 
$.Class("bizagi.workportal.services.db",{},{init:function(){var a=this,b=bizagi.readQueryString();querydisable=b&&b.disabledb?eval(b.disabledb):false;a.disabledb=querydisable;if(!a.disabledb){a.dbProcessHierarchy=new PouchDB("DataProcessesHierarchy");a.dbForms=new PouchDB("DataRenderForms");a.dbCases=new PouchDB("DataRenderCases")}a.globalId=typeof BIZAGI_USER!="undefined"?BIZAGI_USER.toLowerCase()+"-"+BIZAGI_PROXY_PREFIX.toLowerCase()+"-"+BIZAGI_DOMAIN.toLowerCase():"admon-http://localhost/bizagiR100x-domain"},_destroy:function(a){var b=this;if(typeof a==="undefined")a=function(){};PouchDB.destroy("DataRenderForms",a);PouchDB.destroy("DataRenderCases",a);PouchDB.destroy("DataProcessesHierarchy",a)},getHierarchyObjects:function(a,d,e){var f=this,c=[];for(var b in a){if(!a.hasOwnProperty(b))continue;if(typeof a[b]=="object")c=c.concat(f.getHierarchyObjects(a[b],d,e));else if(b==d&&a[b]==e||b==d&&e=="")c.push(a);else if(a[b]==e&&d=="")c.lastIndexOf(a)==-1&&c.push(a)}return c},getAllOpenCases:function(){var b=this,a=new $.Deferred;function c(a){a.closed===false&&a.createdBy.userName==="admon"&&emit(a,null)}b.dbCases.query({map:c},{reduce:false},function(c,b){!c&&a.resolve(b)});return a.promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\database\bizagi.clientdb.cases.js */ 
bizagi.workportal.services.db.extend("bizagi.workportal.services.db",{},{createNewCase:function(b){var f=this,e=b.idWfClass,a=(new Date).toISOString(),c=new $.Deferred,d={_id:a,idWfClass:e.toString(),idWorkFlow:e.toString(),idCase:a,isFavorite:false,globalId:f.globalId,createdBy:{Name:typeof BIZAGI_USER!="undefined"?BIZAGI_USER:"admon",userName:typeof BIZAGI_USER!="undefined"?BIZAGI_USER:"admon"},creationDate:new Date,process:b.process||"Offline",radNumber:a,isOfflineForm:true,formsRenderVersion:2,syncronized:false,isOpen:true,workItems:[{idWorkItem:PouchDB.uuids(),idTask:PouchDB.uuids(),idCase:a,colorState:"Green",State:"Green",TaskName:b.process||"Offline"}]};f.dbCases.put(d,function(a){if(!a)c.resolve(d);else c.reject(a)});return c.promise()},deleteCase:function(c){var b=this,a=new $.Deferred;b.dbCases.get(c.idCase,function(c,d){if(!c)b.dbCases.remove(d,function(d,b){if(!d)a.resolve(b);else{bizagi.log("Error erasing case");a.reject(c)}});else{bizagi.log("Error, the case do not exist");a.reject(c)}});return a.promise()},getWorkitems:function(b){var c=this,a=new $.Deferred;c.dbCases.get(b.idCase,function(b,c){if(!b)a.resolve(c);else a.reject(b)});return a.promise()},getCategories:function(b){var c=this,e=new $.Deferred;if(!c.categories)c.dbProcessHierarchy.get(c.globalId,function(h,g){if(!h){c.categories=g.doc;if(b.idApp||b.idCategory){var f=d(c.categories,b);e.resolve({category:f,totalApps:b.idApp?f.length:0})}else{var a=c.categories;while(a.length==1&&(a[0].categories||a[0].subCategories))a=d(a,a[0].appId?{idApp:a[0].appId}:{idCategory:a[0].idCategory});e.resolve({category:a,totalApps:a[0]&&a[0].appId?a.length:0})}}else e.reject("could not extract this document")});else if(b.idApp||b.idCategory){var f=d(c.categories,b);e.resolve({category:f,totalApps:b.idApp?f.length:0})}else{var a=c.categories;while(a.length==1&&(a[0].categories||a[0].subCategories))a=d(a,a[0].appId?{idApp:a[0].appId}:{idCategory:a[0].idCategory});e.resolve({category:a,totalApps:a[0]&&a[0].appId?a.length:0})}return e.promise();function d(b,c){for(var a=0,f=b.length;a<f;a++){if(b[a].appId&&b[a].appId==c.idApp)return b[a].categories;if(b[a].idCategory&&b[a].idCategory==c.idCategory)return b[a].subCategories;else{if(b[a].categories){var e=d(b[a].categories,c);if(e)return e}if(b[a].subCategories){var e=d(b[a].subCategories,c);if(e)return e}}}}},getAllOpenCases:function(){var b=this,a=new $.Deferred;function c(a){a.closed===false&&a.createdBy.userName==="admon"&&emit(a,null)}b.dbCases.query({map:c},{reduce:false},function(c,b){!c&&a.resolve(b)});return a.promise()},getCase:function(b){var c=this,a=new $.Deferred;c.dbCases.get(b,function(b,c){if(!b)a.resolve(c);else a.reject(b)});return a.promise()},getCustomizedColumnsData:function(a){var d=this,c=new $.Deferred,b={cases:{rows:[],totalPages:1,page:1,lstIdCases:[]}},f=a.inputtray==="true";if(a.radnumber||a.radNumber)a.radNumber=a.radnumber?a.radnumber:a.radNumber;if(a.idcase||a.idCase)a.radNumber=a.idcase?a.idcase:a.idCase;var e={map:function(a){emit(a.globalId,a)}};d.dbCases.query(e,{reduce:false,key:d.globalId},function(h,e){if(!h){for(var d=0,i=e.total_rows;d<i;d++)if(f===e.rows[d].value.isOpen&&(typeof a.radNumber==="undefined"||a.radNumber.toString()===""||a.radNumber.toString()===e.rows[d].value.radNumber)){var g={id:e.rows[d].id,idWFClass:e.rows[d].idWFClass,idWorkFlow:e.rows[d].id,taskState:"",isFavorite:e.rows[d].value.isFavorite,guidFavorite:"",isOpen:e.rows[d].value.isOpen,isOfflineForm:true,fields:[{Value:e.rows[d].value.process,DisplayName:e.rows[d].value.process,DataType:"VarChar",isRadNumber:false},{Value:e.rows[d].value.creationDate,DisplayName:"Process creation date",DataType:"DateTime",isRadNumber:false}]};b.cases.rows.push(g);b.cases.lstIdCases.push(e.rows[d].id)}b.cases.totalPages=b.cases.page=b.cases.rows.length>0?1:0;c.resolve(JSON.stringify(b))}else{bizagi.log("Error loading cases from local data base");c.reject("Error loading cases from local data base")}});return c.promise()},getAllProcesses:function(b){var a=this,c=new $.Deferred;if(typeof b.inputtray==="undefined")b.inputtray=true;var d={workflows:{taskstate:"",workflow:[]}},e={map:function(a){a.idWfClass&&a.globalId&&emit({idWf:a.idWfClass,gId:a.globalId,isOpen:a.isOpen.toString()},1)},reduce:"_count"};a.dbProcessHierarchy.get(a.globalId,function(g,f){if(!g)a.dbCases.query(e,{reduce:true},function(i,g){if(!i){for(var e=0,j=g.total_rows;e<j;e++)if(g.rows[e].key.gId==a.globalId&&g.rows[e].key.isOpen==b.inputtray.toString()){var h=a.getHierarchyObjects(f,"idCategory",g.rows[e].key.idWf);d.isOpen=b.inputtray;d.workflows.workflow.push({category:"Offline Procesess",name:h[0]?h[0].categoryName:"",idworkflow:g.rows[e].key.idWf,count:g.rows[e].value,isFavorite:false})}c.resolve(JSON.stringify(d))}else c.reject("error")});else c.reject("error")});return c.promise()},getOutboxCases:function(){var b=this,a=new $.Deferred;queryFun={map:function(a){!a.isOpen&&emit(a.globalId,a)}};b.dbCases.query(queryFun,{reduce:false,key:b.globalId},function(b,c){if(!b)a.resolve(c);else{bizagi.log("Error loading data before syncronization");a.reject(b)}});return a.promise()},getCasesList:function(a){var d=this,b=new $.Deferred,c={page:1,totalPages:1,elements:[]},f=a.inputtray==="true",e={map:function(a){emit(a.globalId,a)}};d.dbCases.query(e,{reduce:false,key:d.globalId},function(h,e){if(!h){for(var d=0,i=e.total_rows;d<i;d++)if(f===e.rows[d].value.isOpen&&(typeof a.radNumber==="undefined"||a.radNumber.toString()===""||e.rows[d].value.radNumber.toString().indexOf(a.radNumber.toString())!=-1)){var g=[e.rows[d].value.idCase,e.rows[d].value.workItems[0].idWorkItem[0]||"",e.rows[d].value.workItems[0].idTask[0]||"",e.rows[d].value.creationDate,e.rows[d].value.process,e.rows[d].value.process,[["Case No",e.rows[d].value.idCase],["Process",e.rows[d].value.process],["Process creation date",bizagi.util.formatDateTime(e.rows[d].value.creationDate)]],e.rows[d].value.isOfflineForm,e.rows[d].value.isOpen];c.elements.push(g)}b.resolve(JSON.stringify(c))}else{bizagi.log("Error loading cases from local data base");b.reject("Error loading cases from local data base")}});return b.promise()},getCustomizedColumnsNewData:function(b){var d=this,c=new $.Deferred,a={cases:{rows:[],totalPages:1,page:1,lstIdCases:[]}},f=b.inputtray==="true",e={map:function(a){emit(a.globalId,a)}};d.dbCases.query(e,{reduce:false,key:d.globalId},function(h,e){if(!h){for(var d=0,i=e.total_rows;d<i;d++)if(f===e.rows[d].value.isOpen&&(typeof b.radNumber==="undefined"||b.radNumber.toString()===""||b.radNumber.toString()===e.rows[d].value.radNumber)){var g={id:e.rows[d].value.idCase,idWFClass:e.rows[d].value.idWfClass,idWorkFlow:e.rows[d].value.idWorkFlow,taskState:"",isFavorite:e.rows[d].value.isFavorite,guidFavorite:"",isOpen:e.rows[d].value.isOpen,isAborted:false,isOfflineForm:true,fields:[e.rows[d].value.idCase,e.rows[d].value.process,{workitems:e.rows[d].value.workItems},bizagi.util.formatDateTime(e.rows[d].value.creationDate),"",bizagi.util.formatDateTime(e.rows[d].value.creationDate)]};a.cases.rows.push(g);a.cases.lstIdCases.push(e.rows[d].id)}a.cases.totalPages=a.cases.page=a.cases.rows.length>0?1:0;c.resolve(JSON.stringify(a))}else{bizagi.log("Error loading cases from local data base");c.reject("Error loading cases from local data base")}});return c.promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\database\bizagi.clientdb.forms.js */ 
bizagi.workportal.services.db.extend("bizagi.workportal.services.db",{},{getFormData:function(b){var c=this,a=new $.Deferred;c.dbForms.get(b+"-"+b+"-"+c.globalId,function(c,b){if(!c)if(typeof b.form!=="undefined")a.resolve({form:b.form,type:"form"});else{var d='{ "message": "'+bizagi.localization.getResource("workportal-offline-error-nonexisting-form-message")+'", "errorType": "'+bizagi.localization.getResource("workportal-offline-error-nonexisting-form")+'" }';a.reject(d)}else a.reject(c)});return a.promise()},mergeForm:function(a,d){var c=this,b=new $.Deferred;$.when(d).done(function(d){if(!a.data)b.resolve(d);else{if(!a.isOpen)d.form.buttons=[];for(prop in a.data)e(d.form.elements,{key:prop,value:a.data[prop]});function e(b,d){if(b.length&&b.length>0)for(var f=0,g=b.length;f<g;f++)e(b[f],d);else if(b.container)e(b.container.elements,d);else{if(!a.isOpen)b.render.properties.editable=false;if(b.render.properties.xpath==d.key){b.render.properties.type==="grid"&&c.setRenderGridValues(b.render);c.setRenderValues(b.render.properties,d.value)}}}b.resolve(d)}}).fail(function(a){if(!a.responseText)a.responseText='{ "message": "'+bizagi.localization.getResource("workportal-offline-error-nonexisting-form-message")+'", "errorType": "'+bizagi.localization.getResource("workportal-offline-error-nonexisting-form")+'" }';b.reject(a)});return b.promise()},setRenderGridValues:function(a){a.properties.data.columns=[];for(var b=0,c=a.elements.length;b<c;b++)a.properties.data.columns.push(a.elements[b].render.properties.xpath)},setDataType:function(){},setRenderValues:function(a,b){var d=a.type;if(!d)d="label";var m=["text","label","number","money","date","boolean","geolocation","hidden","upload","image"],n=["combo","cascadingCombo","radio","list"];if(m.indexOf(d)!=-1){a.value=b.value;return}if(n.indexOf(d)!=-1){for(var i=0,r=a.data.length;i<r;i++)if(a.data[i].id==b.value)a.value=[{id:b.value,value:a.data[i].value}];return}if(d=="search"){for(var j=0,p=a.data.length;j<p;j++)if(a.data[j].id==b.value.id)a.value=b.value;return}if(d=="grid"){a.data.records=b.value.length;a.data.total=1;a.data.page=1;for(var o=a.data.columns.length,h=[],g=0,q=b.value.length;g<q;g++){var e=[],k=bizagi.util.randomNumber();e[e.length]=k;for(var f=[],c=0;c<o;c++){var l=typeof b.value[g][c]!=="undefined"?a.data.columns.indexOf(b.value[g][c].xpath):-1;if(l!=-1)f[l]=b.value[g][c];f[c]=typeof f[c]==="undefined"?{}:f[c];e[e.length]="true"}a.data.rows.push($.merge([k],f));h[h.length]=e}a.data.editable=h;a.data.visible=h;return}try{a.value=b.value}catch(s){}bizagi.log(d+" not supported in render factory",b,"error");return null},getSummary:function(b){var c=this,a=new $.Deferred;c.dbCases.get(b.idCase,function(c,b){if(!c)a.resolve($.extend(b,{processPath:"/",currentState:b.workItems,countAssigness:0,countEvents:0,isOpen:b.isOpen.toString(),countSubProcesses:0}));else a.reject(c)});return a.promise()},saveFormInfo:function(d,c){var a=this,b=new $.Deferred;a.dbCases.get(bizagi.context.idCase,function(e,d){if(!e)a.mergeDataDocument(c,d,a.getFormData(d.idWfClass.toString())).done(function(f){if(c.h_action=="next")d.isOpen=false;d.data=d.data||{};for(var e in f){var g=false;for(var h in d.data)if(e==h)if(bizagi.util.isEmpty(f[e])||bizagi.util.isEmpty(f[e].value)){delete d.data[h];g=true}else{d.data[h]=f[e];g=true}if(!g)d.data[e]=f[e]}a.dbCases.put(d,function(a,c){if(!a)b.resolve(c);else b.reject(a)})});else b.reject(e)});return b.promise()},mergeDataDocument:function(b,e,d){var c=new $.Deferred,a={};$.when(d).done(function(e){for(prop in b)if(prop!="h_action"){a[prop]={};a[prop].value=b[prop];d(e.form.elements,{key:prop,value:b[prop]})}function d(c,b){if(c.length&&c.length>0)for(var g=0,k=c.length;g<k;g++)d(c[g],b);else if(c.container)d(c.container.elements,b);else if(c.render.properties.xpath==b.key){a[b.key].DataType=c.render.properties.dataType||"";a[b.key].type=c.render.properties.type||"";if(!bizagi.util.isEmpty(a[b.key]))for(var e=0,j=a[b.key].value.length;e<j;e++)if(!bizagi.util.isEmpty(a[b.key].value[e])){for(var h=[],f=0,i=a[b.key].value[e].length;f<i;f++)!bizagi.util.isEmpty(a[b.key].value[e][f])&&h.push(a[b.key].value[e][f]);a[b.key].value[e]=h}}}c.resolve(a)});return c.promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\database\bizagi.clientdb.pullWorker.js */ 
$.Class.extend("bizagi.workportal.services.pullWorker",{},{init:function(c){var a=this,b=bizagi.readQueryString();a.services=c;a.database=c.database;querydisable=b&&b.disabledb?eval(b.disabledb):false;a.disabledb=querydisable;if(!a.disabledb)a.dbForms=new PouchDB("DataRenderForms"),a.dbCases=new PouchDB("DataRenderCases"),a.dbProcessHierarchy=new PouchDB("DataProcessesHierarchy");a.globalId=typeof BIZAGI_USER!="undefined"?BIZAGI_USER.toLowerCase()+"-"+BIZAGI_PROXY_PREFIX.toLowerCase()+"-"+BIZAGI_DOMAIN.toLowerCase():"admon-http://localhost/bizagiR100x-domain"},pull:function(){var a=this;return a.disabledb?false:$.when(a.syncOfflineCases()).done(function(){return true})},syncOfflineCases:function(){var a=this;if(typeof a.database!="undefined"){a.synchronizing=a.synchronizing||false;if(!a.synchronizing){a.synchronizing=true;return a.database.getOutboxCases().done(function(g){for(var e=[],d=0;d<g.total_rows;d++){var b=g.rows[d].value,f=[];for(var c in b.data)c!="h_action"&&f.push({domain:typeof BIZAGI_DOMAIN!="undefined"?BIZAGI_DOMAIN:"domain",user:typeof BIZAGI_USER!="undefined"?BIZAGI_USER:"admon",xpath:c,value:a.processDataValues(b.data[c].value),DataType:b.data[c].DataType,type:b.data[c].type});b=$.extend(b,{objToSend:f});e.push(a.services.syncOfflineCases(b).pipe(function(){for(var c=decodeURIComponent(this.data).split("&"),b=c.length-1;b>=0;b--){var d=c[b].split("=");if(d[0]=="idCase")return a.database.deleteCase({idCase:d[1]})}},function(a){bizagi.log(a)}))}return $.when.apply($,e).pipe(function(){a.synchronizing=false;return true})})}}},processDataValues:function(a){if($.type(a)=="object")if(a.hasOwnProperty("id"))a=a.id;return a}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\services\database\bizagi.clientdb.fetchWorker.js */ 
$.Class.extend("bizagi.workportal.services.fetchWorker",{},{init:function(c){var a=this;a.services=c;var b=bizagi.readQueryString();querydisable=b&&b.disabledb?eval(b.disabledb):false;a.disabledb=querydisable;if(!a.disabledb)a.dbForms=new PouchDB("DataRenderForms"),a.dbCases=new PouchDB("DataRenderCases"),a.dbProcessHierarchy=new PouchDB("DataProcessesHierarchy");a.globalId=typeof BIZAGI_USER!="undefined"?BIZAGI_USER.toLowerCase()+"-"+BIZAGI_PROXY_PREFIX.toLowerCase()+"-"+BIZAGI_DOMAIN.toLowerCase():"admon-http://localhost/bizagiR100x-domain"},fetch:function(){var b=this,a=new $.Deferred;if(b.disabledb){a.reject("Disable Sync");return a.promise()}$.when(b.services.processesHierarchy()).done(function(c){$.when(b.saveProcessesHierarchy(c),b.processesHierarchyTofetchForms()).done(function(){a.resolve("All data fecth")}).fail(function(b){if(b==="nothing")a.reject(0);else a.reject("error fectching data")})});return a.promise()},processesHierarchyTofetchForms:function(){var c=this,b=new $.Deferred,a=bizagi.util.getItemLocalStorage("formChangeset"+c.globalId);if(a===null||typeof a==="undefined")a=0;c.callSyncForms({changeSet:a}).done(function(a){console.log(a,"sync forms");b.resolve(a)}).fail(function(a){if(a==="nothing")console.log("nothing to sync");else console.log(a,"error sync forms");b.reject(a)});return b.promise()},callSyncForms:function(d){var b=this,a=new $.Deferred,c=d.changeSet;b.services.processesHierarchyTofetchForms(d).done(function(d){if(d.changeSet>c){var e=null;if(d.changeSetType===1)e=b.saveProcessesHierarchyTofetchForms(d.result);if(d.changeSetType===4)e=b.saveProcessInHierarchy(d.result);$.when(e).done(function(){b.callSyncForms({changeSet:d.changeSet}).done(function(b){a.resolve(b)}).fail(function(b){a.reject(b)})}).fail(function(b){a.reject(b)})}else{bizagi.util.setItemLocalStorage("formChangeset"+b.globalId,d.changeSet);c=parseInt(c);if(d.changeSet===0&&c===0)a.reject("nothing");else a.resolve("succesfully")}}).fail(function(b){a.reject(b)});return a.promise()},saveProcessInHierarchy:function(){var c=this,a=new $.Deferred;a.resolve("");return a.promise()},getHierarchyObjects:function(a,d,e){var f=this,c=[];for(var b in a){if(!a.hasOwnProperty(b))continue;if(typeof a[b]=="object")c=c.concat(f.getHierarchyObjects(a[b],d,e));else if(b==d&&a[b]==e||b==d&&e=="")c.push(a);else if(a[b]==e&&d=="")c.lastIndexOf(a)==-1&&c.push(a)}return c},saveProcessesHierarchyTofetchForms:function(a){var b=this,c=new $.Deferred,d={_id:a.wfClass.toString()+"-"+a.wfClass.toString()+"-"+b.globalId,globalid:b.globalId,idWfClass:a.wfClass,idWorkFlow:a.workFlow,form:a.data.form};b.dbForms.get(d._id,function(e,a){if(e)b.dbForms.put(d,function(b,a){if(!b)c.resolve(a);else c.reject("Could'not agregate the document")});else b.dbForms.put({_rev:a._rev,_id:a._id,form:d.form},function(b,a){if(!b)c.resolve(a);else c.reject("Could'not updated the document")})});return c.promise()},saveProcessesHierarchy:function(d){var b=this,a=new $.Deferred,c={_id:b.globalId,doc:d};b.dbProcessHierarchy.get(c._id,function(f,e){if(f)b.dbProcessHierarchy.put(c,function(c,b){if(!c)a.resolve(b);else a.reject("Could'not agregate the document")});else b.dbProcessHierarchy.put({_id:e._id,_rev:e._rev,doc:d},function(c,b){if(!c)a.resolve(b);else a.reject("Could'not updated the document")})});return a.promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\bizagi.workportal.device.factory.js */ 
$.Class.extend("bizagi.workportal.device.factory",{},{init:function(a){this.device=bizagi.util.detectDevice();this.workportal=a;this.cachedFacade=null},getWorkportalFacade:function(d,c){var a=this,b=new $.Deferred;c=typeof c!=="undefined"?c:true;if(a.cachedFacade!=null)b.resolve(a.cachedFacade);else if(this.device=="desktop"){a.cachedFacade=new bizagi.workportal.desktop.facade(this.workportal,d);$.when(a.cachedFacade.initAsyncStuff(c)).done(function(){b.resolve(a.cachedFacade)});return b.promise()}else if(this.device=="tablet"||this.device=="tablet_ios"||this.device=="tablet_android"){a.cachedFacade=new bizagi.workportal.tablet.facade(d,this.device);$.when(a.cachedFacade.initAsyncStuff()).done(function(){b.resolve(a.cachedFacade)});return b.promise()}else if(this.device=="smartphone_ios"||this.device=="smartphone_android"){a.cachedFacade=new bizagi.workportal.smartphone.facade(d,this.device);$.when(a.cachedFacade.initAsyncStuff()).done(function(){b.resolve(a.cachedFacade)});return b.promise()}else{alert("Not supported device: "+this.device);b.resolve("")}return b.promise()},getDataService:function(){return this.dataService}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\bizagi.workportal.webparts.facade.js */ 
bizagi.workportal=bizagi.workportal||{};bizagi.workportal.currentInboxView="inbox";bizagi.workportal.state=bizagi.workportal.state||{};bizagi.webparts=bizagi.webparts||{};bizagi.webparts.instances=bizagi.webparts.instances||[];$.Class.extend("bizagi.workportal.facade",{},{init:function(a){this.deviceFactory=new bizagi.workportal.device.factory(this);this.dataService=new bizagi.workportal.services.service(a);this.dataService.routing=new bizagi.workportal.services.routing({dataService:this.dataService});this.defaultParams=a||{}},createWebpart:function(a){return this.executeWebpart($.extend(a,{creating:true}))},executeWebpart:function(a){var d=this,e=this.ownerDocument,f=$("body",e),c=a.canvas,b=true;if(a.webpartConfiguration)b=d.testWebpartConfiguration(a);if(b)return d.processWebpart(a).done(function(a){a.name=a.webpart.Class.fullName;bizagi.webparts.instances.push(a);c.append(a.content);c.triggerHandler("ondomincluded")})},ready:function(){return this.executionDeferred.promise()},processWebpart:function(a){var e=this,c=new $.Deferred,b,d=this.deviceFactory.getWorkportalFacade(e.dataService,false);$.extend(a,this.defaultParams);$.when(d).pipe(function(b){var c=new $.Deferred;$.when(b.loadWebpart(a)).done(function(){c.resolve(b)});return c.promise()}).pipe(function(c){b=c.getWebpart(a.webpart,a);return b.render(a)}).done(function(a){c.resolve({webpart:b,content:a})});return c.promise()},getMainController:function(){return this.mainController},execute:function(){var a=this,b=this.deviceFactory.getWorkportalFacade(a.dataService);a.processOfflineData();return $.when(b).then(function(a){return a.executeWebparts()})},processOfflineData:function(){var a=this;typeof a.dataService.fetchOfflineData!="undefined"&&a.dataService.online==true&&$.when(a.dataService.pushOfflineData()).always(function(){a.dataService.fetchOfflineData()})}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\tablet\js\bizagi.workportal.webparts.tablet.facade.js */ 
$.Class.extend("bizagi.workportal.tablet.facade",{getWidgetImplementation:function(a){bizagi.log("getWidgetImplementation"+a)}},{init:function(a,b){this.templates={};this.dataService=a;this.setIPhoneMetaTags();this.modules={};this.device=b},setIPhoneMetaTags:function(){$("<meta>",{name:"apple-mobile-web-app-capable",content:"yes"}).appendTo("head");$("<meta>",{name:"viewport",content:"width=device-width, initial-scale=1.0, minimum-scale=1, maximum-scale=1, user-scalable=no"}).appendTo("head");$("<link>",{rel:"apple-touch-icon-precomposed",href:"jquery/common/base/css/tablet/images/BizAgi_logo.png"}).appendTo("head");$("<link>",{rel:"apple-touch-startup-image",href:"jquery/common/base/css/tablet/images/splash.png"}).appendTo("head")},initAsyncStuff:function(){var a=this;return $.when(a.loadTemplate("base",bizagi.getTemplate("base.workportal.tablet"))).done(function(a){$("body").append(a);bizagi.util.tablet.startkendo({init:function(){kendo.UserEvents.defaultThreshold(kendo.support.mobileOS.device==="android"?0:20)}})})},loadTemplate:function(a,b){var c=this;return bizagi.templateService.getTemplate(b,a).done(function(b){if(typeof b==="string")c.templates[a]=$.trim(b.replace(/\n/g,""))})},loadTemplateWebpart:function(a,b){var c=this;return bizagi.templateService.getTemplateWebpart(b,a).done(function(b){c.templates[a]=$.trim(b.replace(/\n/g,""))})},getTemplate:function(a){var b=this;return b.templates[a]},loadWebpart:function(b){var d=this,c=new $.Deferred,e=b.webpart,a=bizagi.getWebpart(e,b);!a&&bizagi.log("webpart not found");$.when(bizagi.util.initWebpart(a,d.device)).done(function(){$.when.apply(this,$.map(a.tmpl,function(a){return d.loadTemplateWebpart(a.originalAlias,bizagi.getTemplate(a.alias,true))})).done(function(){c.resolve(a)})});return c.promise()},getWebpart:function(b,c){try{if(b.indexOf("bizagi")=="-1")b="bizagi.workportal.webparts."+b;var a="var baDynamicFn = function(facade, dataService, params){ \r\n";a+="return new "+b+"(facade, dataService, params);\r\n";a+="}\r\n";a+="baDynamicFn";a=eval(a);return a(this,this.dataService,c)}catch(d){bizagi.log(d)}},executeWebpart:function(a){var c=this,b=new $.Deferred,d=this.ownerDocument;$.when(c.loadWebpart(a)).done(function(e){var d=e["class"];dynamic=c.getWebpart(d,a);$.when(dynamic.render({creating:false})).done(function(c){a.canvas.append(c);a.canvas.triggerHandler("ondomincluded");b.resolve(dynamic)})});return b.promise()},executeWebparts:function(){var a=this;a.loadModule("renderingflat");return $.when(a.dataService.getCurrentUser()).pipe(function(b){bizagi.currentUser=b;return $.when(a.executeWebpart({webpart:"homePortal",canvas:$("body")}),a.executeWebpart({webpart:"menu",canvas:$("body")}),a.executeWebpart({webpart:"newcase",canvas:$("body")}),a.executeWebpart({webpart:"render",canvas:$("body")})).then(function(){bizagi.debug("Finalizo ejecución de webparts workportal")})})},loadModule:function(a){var b=this;if(typeof a!=="string")return;if(typeof b.modules[a]!=="undefined")return b.modules[a];b.modules[a]=new $.Deferred;bizagi.loader.start(a).then(function(){b.modules[a].resolve()});return b.modules[a].promise()}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\webpart\bizagi.workportal.webpart.helper.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.webparts=typeof bizagi.webparts!=="undefined"?bizagi.webparts:{};$.Class.extend("bizagi.workportal.webpart.helper",{},{init:function(a){this.dataService=a.dataService;this.resources=a.resources;this.workportalFacade=a.workportalFacade;this.templates={}},getFileImage:function(a){var b=this;return!a||a.length==0?b.getEmptyFile():a},getFile:function(a){var b=this;return!a||a.length==0?b.getEmptyFile():b.dataService.getFile(a[0])},loadTemplate:function(b,a){var c=this;return bizagi.templateService.getTemplate(a).done(function(a){c.templates[b]=a})},isWebpartInIFrame:function(){try{return window.self!==window.top}catch(a){return true}},getFailServerErrorDeferred:function(){}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\webpart\bizagi.workportal.webparts.webpart.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.webpart=typeof bizagi.webpart!=="undefined"?bizagi.webpart:{};bizagi.webpart.subscribe=function(a,b){$(document).bind(a,b)};bizagi.webpart.publish=function(a,b){return $(document).triggerHandler(a,b)};bizagi.webpart.whenCreated=function(b,a){bizagi.webpart.subscribe("bizagi-webpart-created",function(c){if(c.webpart.Class.fullname=="bizagi.workportal.webparts."+b)a&&a(c)})};bizagi.webpart.whenExecuted=function(b,a){bizagi.webpart.subscribe("bizagi-webpart-executed",function(c){if(c.webpart.Class.fullname=="bizagi.workportal.webparts."+b)a&&a(c)})};bizagi.workportal.controllers.controller.extend("bizagi.workportal.webparts.webpart",{BIZAGI_WORKPORTAL_WEBPART_ROUTING:"routing",BIZAGI_WORKPORTAL_WEBPART_RENDER:"activityform",BIZAGI_WORKPORTAL_WEBPART_ASYNC:"async",BIZAGI_WORKPORTAL_WEBPART_OLDRENDERINTEGRATION:"oldrenderintegration"},{init:function(c,d,b){var a=this;this._super(c,d,b);this.helper=new bizagi.workportal.webpart.helper(this);this.canvas=b.canvas;this.project=b.project;this.sharepointProxyPrefix=b.sharepointProxyPrefix;this.namespace=b.namespace;this.isVisible=true;this.isWebpartInIFrame=typeof b.isWebpartInIFrame!="undefined"?b.isWebpartInIFrame:a.helper.isWebpartInIFrame();this.adjustButtonsToContent=b.adjustButtonsToContent;this.remoteServer=b.remoteServer;if(this.remoteServer){a.postmessageSocket=new bizagi.postmessage({remoteServer:this.remoteServer,destination:window.parent,origin:window});a.postmessageSocket.receive=function(c){var b=eval("("+c.data+")");if(b.parameters)if(b.parameters.iframeidentifier==BIZAGI_IFRAME_IDENTIFIER)return;if(b.postMessageEventName)switch(b.postMessageEventName){case"postmessage-bizagi-canCloseIFramePopUp":a.canHide&&a.Class.shortName==b.webPartDestine&&$.when(a.canHide()).done(function(){var b={};b.postMessageEventName="postmessage-bizagi-closeIFramePopUp";b.parameters={};b.parameters.iframeidentifier=BIZAGI_IFRAME_IDENTIFIER;a.postmessageSocket.send(JSON.stringify(b));var c={};c.postMessageEventName="ui-bizagi-reload-cases";a.postmessageSocket.send(JSON.stringify(c))});break;case"postmessage-bizagi-openParentPopUp":break;default:a.publish(b.postMessageEventName,b.parameters,true)}};a.sendDimensionsiFrame()}},sendDimensionsiFrame:function(b){var a=this;if(this.remoteServer)if(!!!a.adjustButtonsToContent&&(!!!a.namespace||b))a.isWebpartInIFrame&&window.setTimeout(function(){var b={};b.iframeName=BIZAGI_IFRAME_IDENTIFIER;b.iframeHeight=$("body").height();if(a.Class.shortName=="newcaselist")b.iframeWidth=$(document).width();else b.iframeWidth=undefined;a.postmessageSocket.send(JSON.stringify(b))},50)},publish:function(a,b,e){var d=this;a=this.namespace?this.namespace+"."+a:a;if(!!!e&&a.indexOf("bizagi-webpart-created")==-1&&a.indexOf("bizagi-webpart-executed")==-1&&d.postmessageSocket){var c={};c.postMessageEventName=a;if(b){b.canvas=null;b.workportal=null}c.parameters=$.extend(b,{project:this.project,iframeidentifier:BIZAGI_IFRAME_IDENTIFIER});d.postmessageSocket.send(JSON.stringify(c))}return this._super(a,$.extend(b,{project:this.project}))},subscribe:function(a,c){var b=this;a=this.namespace?this.namespace+"."+a:a;this._super(a,function(d,a){if(typeof a==="undefined")a={};if(b.project==a.project&&b.isVisible)return c(d,a)})},unsubscribe:function(a,b){var c=this;a=this.namespace?this.namespace+"."+a:a;this._super(a,b)},refresh:function(c){var a=this,b;if(a.canvas.data("role")=="view")b=a.canvas.data("kendoMobileView").contentElement();else b=a.canvas;b.empty();return a.render(c).then(function(c){b.append(c);a.canvas.triggerHandler("ondomincluded");return"included"})},render:function(b){var a=this;a.params=b;if(b.creating){a.content=a.empty();a.publish("bizagi-webpart-created",{webpart:a});return a.content}a.publish("bizagi-webpart-created",{webpart:a});return $.when(a.renderContent(b)).pipe(function(c){if(typeof c=="string")c=$(c);a.content=c;return $.when(a.postRender(b)).then(function(){a.publish("bizagi-webpart-executed",{webpart:a});a.sendDimensionsiFrame();return a.getContent()})})},renderContent:function(){},postRender:function(){},getContent:function(){return this.content},empty:function(){return $("<div/>")},getTemplate:function(a){return this.workportalFacade.getTemplate(a)},createWebpart:function(a){return this.workportalFacade.workportal.createWebpart(a)},executeWebpart:function(a){return this.workportalFacade.workportal.executeWebpart(a)},manageError:function(e,d,b){var a=this,c="";if(e.responseText)c=e.responseText;else c=e;if(c.indexOf("AUTHENTICATION_ERROR")==-1){a.helper.getFailServerErrorDeferred({message:c,sharepointProxyPrefix:a.sharepointProxyPrefix},d);a.endWaiting();b&&d.done(function(c){b.append(c);a.endWaiting()})}else{var f=a.content="<div class='ui-bizagi-webpart-error-message'>The current user does not exist as a registered user in the bizagi project</div>";b&&b.append(f);a.endWaiting();d.resolve(f)}},hide:function(b){var a=this;a.canvas.hide();b&&a.canvas.parent().hide();a.isVisible=false},show:function(){var a=this;a.canvas.show();a.canvas.parent().show();a.isVisible=true},isVisible:function(){var a=this;return a.isVisible},destroy:function(){},getHomeportalFrameworkIntance:function(){if(!this.workportalFacade.homePortalFramework)this.workportalFacade.homePortalFramework=new bizagi.workportal.homportalFramework(this.workportalFacade);return this.workportalFacade.homePortalFramework}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\webpart\bizagi.framework.homeportal.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.webparts=typeof bizagi.webparts!=="undefined"?bizagi.webparts:{};$.Class.extend("bizagi.workportal.homportalFramework",{},{init:function(b){var a=this;a.workportalFacade=b;a.device=bizagi.detectDevice();a._homeportalShow();a.configureHandler();bizagi.util.mobility.homePortalNavigate=$.proxy(a.homePortalNavigate,a);bizagi.util.mobility.homePortalViewShown=$.proxy(a.homePortalViewShown,a);bizagi.util.mobility.homePortalViewInit=$.proxy(a.homePortalViewInit,a);bizagi.util.mobility.homePortalLayoutInit=$.proxy(a.homePortalLayoutInit,a)},loadComponents:function(c,d){var b=this,a=[];$.each(b.homePortalLayout,function(g,e){if(Array.isArray(e.position))$.each(e.position,function(h,i){if(i===c){var f=$("#"+e.canvas[h],d),j=b.workportalFacade.executeWebpart({webpart:g,canvas:f,pane:f.parent('div[data-role="pane"]')});a.push(j)}});else if(e.position===c){var f=$("#"+e.canvas,d),h=b.workportalFacade.executeWebpart({webpart:g,canvas:f,pane:f.parent('div[data-role="pane"]')});a.push(h)}});return $.when(a)},loadComponentTemplate:function(e,d,g){var a=this,c=[],f=kendo.template(a.workportalFacade.getTemplate(e),{useWithBlock:false});for(var b in a.homePortalLayout)if(Array.isArray(a.homePortalLayout[b].position))$.each(a.homePortalLayout[b].position,function(e,f){f===d&&c.push({canvas:a.homePortalLayout[b].canvas[e]})});else a.homePortalLayout[b].position===d&&c.push(a.homePortalLayout[b]);return f($.extend(g,{homePortalLayoutPositions:c}))},_homeportalShow:function(){var a=this;bizagi.webpart.subscribe("homeportalShow",function(j,b){b.params=b.params||{};var d="",c=false;switch(b.what){case"reportsChart":d="reportsChart";c=false;break;case"newCase":case"menu":case"render":c=true;break;default:d=b.what}if(!c){var f=a.homePortalLayout[d].canvas,e=$("#"+f);if(e.css("display")==="none"){var i=e.parent(),g=i.data("kendoMobilePane");g.navigate(f);if(b.title){var h=g.view().header.find(".km-navbar").data("kendoMobileNavBar");h.title(b.title)}}a.closeDrawers()}else a.showDrawer(b.what);bizagi.webpart.publish("homeportalShow-"+b.what,b.params)})},configureHandler:function(){var a=this;bizagi.webpart.subscribe("homeportalBack",function(){a._back()})},closeDrawers:function(){$("#menu-drawer").data("kendoMobileDrawer").hide();$("#newCase-drawer").data("kendoMobileDrawer").hide();$("#bz-render-drawer").data("kendoMobileDrawer").hide()},showDrawer:function(a){if(a==="newCase")$("#newCase-drawer").data("kendoMobileDrawer").show();else if(a==="menu")$("#menu-drawer").data("kendoMobileDrawer").show();else a==="render"&&$("#bz-render-drawer").data("kendoMobileDrawer").show()},homePortalViewInit:function(a){a.view.element.trigger("view-initialized",a.view)},homePortalLayoutInit:function(){}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\tablet\js\bizagi.framework.tablet.homeportal.js */ 
bizagi=typeof bizagi!=="undefined"?bizagi:{};bizagi.webparts=typeof bizagi.webparts!=="undefined"?bizagi.webparts:{};bizagi.workportal.homportalFramework.extend("bizagi.workportal.homportalFramework",{},{init:function(c){var a=this;a._super(c);a.deviceType="tablet";var b=bizagi.localization.getResource("workportal-taskfeed-title");if(!a.workportalFacade.dataService.online&&bizagi.util.hasOfflineForm())b=bizagi.util.getItemLocalStorage("inputtray")=="false"?bizagi.localization.getResource("workportal-menu-outbox"):bizagi.localization.getResource("workportal-menu-drafts");a.homePortalLayout={menuHome:{position:"homePortal",direction:"right",canvas:"menuHome",title:"",layout:"main-default"},taskFeed:{position:"homePortal",direction:"left",canvas:"taskFeed",title:b},summaryItem:{position:"menuHome",canvas:"summaryItem"},reportsChart:{position:"homePortal",direction:"right",canvas:"reportsChart",title:"Reports",layout:"main-default-close"}}},_back:function(){var a=this;$("#right-pane").data("kendoMobilePane").navigate("#:back")},configureHandler:function(){var a=this;a._super()},homePortalNavigate:function(a){var b=a.url,c=a.sender.element},homePortalViewShown:function(b){var a=this;a.destination=b.view.id.replace(/#/i,"");a.content=b.view.element;a.container=b.view.container;if(a.destination==="reportsChart"){var c=b.sender.history.length;b.sender.history[c-1]===b.sender.history[c-3]&&b.sender.history.pop();$(".km-view-title",a.content).children().text("");$(".btn-close",a.content).unbind("click").bind("click",function(){$("iframe",".bz-wp-reports-chart").remove();$(".bz-wp-reports-chart",a.content).empty();$("#left-pane",a.conten).toggle();$(".km-view-title",a.content).children().text("");a._back()});$("#left-pane",a.conten).toggle()}},homePortalLayoutInit:function(){}});
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\reporting\bizagi.reporting.definition.js */ 
if(!bizagi.reporting)bizagi.reporting={};bizagi.reporting={BAMProcess:{defaultReport:"bamprocessloadanalysis",info:{reportSet:1},reports:{bamprocessloadanalysis:{components:["filters-processversion","filters-dimension"],resource:"bz-rp-loadanalysis-tab"},bamprocessworkinprogress:{components:["filters-processversion","filters-dimension"],resource:"bz-rp-workinprogress-tab"}}},BAMTask:{defaultReport:"bamtasksworkinprogress",info:{reportSet:2},reports:{bamtasksworkinprogress:{components:["filters-processversion","filters-dimension"],resource:"bz-rp-workinprogress-tab"},bamtaskloadanalysis:{components:["filters-processversion","filters-dimension"],resource:"bz-rp-loadanalysis-tab"}}},AnalyticsProcess:{defaultReport:"analyticsprocesscycletime",info:{reportSet:3},reports:{analyticsprocesscycletime:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-cycletime-tab"},analyticsprocessdurationhistogram:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-durationhistogram-tab"},analyticsprocessactivity:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-processactivity-tab"},analyticsprocessactivationranking:{components:["filters-dimension","filters-time"],resource:"bz-rp-activationranking-tab"},analyticsprocessfrequentspaths:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-frequentspaths-tab"}}},AnalyticsTask:{defaultReport:"analyticstaskcycletime",info:{reportSet:4},reports:{analyticstaskcycletime:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-cycletime-tab"}}},ResourceBAM:{callBack:"myTeamCallBack",defaultReport:"bamresourcemonitorworkinprogress",info:{reportSet:6},reports:{bamresourcemonitorworkinprogress:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-workinprogress-tab"},bamresourcemonitorworkinprogressuser:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-workinprogressuser-tab"},bamresourcemonitorworkinprogressteam:{components:["filters-processversion","filters-dimension","filters-time"],resource:"bz-rp-workinprogressteam-tab"}}},AnalyticsSensor:{defaultReport:"sensors",reports:{sensors:{components:["filters-dimension","filters-time"]}},info:{reportSet:5}}};
/* D:\DV\1070x\Source\BizAgiBPM\BizAgiBPM\jquery\workportalflat\common\webpart\commands\bizagi.workportal.command.search.js */ 
$.Class("bizagi.workportal.command.search",{SEARCH_PAGE_SIZE:30,SEARCH_DELAY:700},{init:function(b){var a=this;a.isTabletDevice=bizagi.util.isTabletDevice();a.enableOfflineForm=bizagi.util.hasOfflineForm();a.dataService=b},searchList:function(a){var b=this;a=a||{};a.inputtray=bizagi.util.inputTray(b.dataService.online);var c={radNumber:$.trim(a.query),idWorkflow:"",taskState:"all",onlyFavorites:false,order:"",numberOfFields:2,orderFieldName:"",orderType:"0",page:a.page||1,pageSize:b.Class.SEARCH_PAGE_SIZE,inputtray:a.inputtray?a.inputtray:"inbox"};return typeof BIZAGI_ENABLE_QUICK_SEARCH!="undefined"&&eval(BIZAGI_ENABLE_QUICK_SEARCH)?b.loadDataListBeta(c):b.loadDataList(c)},loadDataList:function(b){var a=this;return a.dataService.getCasesByWorkflow(b).done(function(c){c.elements=[];var j=[];c.totalPages===0&&j.push({noResults:true,message:bizagi.localization.getResource("workportal-menu-search-found-no-cases")});for(var d=0,r=c.rows.length;r>d;d++){var f=c.rows[d].fields,p=c.rows[d].radnumber||c.rows[d].id,b={},i=f[f.length-1],e={};b.radNumber=p;if(typeof i.Value!="string")for(var m=0,n=i.Value.length;m<n;m++){var l=i.Value[m];if(l!==""&&typeof l=="string"){e.solutionDate=l;break}}else e.solutionDate=i.Value;e.solutionDate=bizagi.util.getTaskStatus(e.solutionDate);for(var k=0;k<f.length;k++)if(f[k].DisplayName==="Process"){e.processName=f[k].Value;break}if(!e.processName)e.processName=f[0].Value;if(typeof c.rows[d].workitems!="undefined")for(var h=c.rows[d].workitems,g=0;h.length>g;g++){b[0]=c.rows[d].id;b[1]=h[g].idWorkItem;b[2]=h[g].idTask;b.state=h[g].State.toLowerCase()||"gray";b[5]=h[g].TaskName||bizagi.localization.getResource("webpart-cases-summary-without-name");b[6]=[];b[6][0]=[];b[6][0]=[0,c.rows[d].id];b[4]=e.processName;b[3]=e.solutionDate;if(a.isTabletDevice&&c.rows[d].isOfflineForm){b[7]=c.rows[d].isOfflineForm;b[8]=c.rows[d].isOpen}var o=$.extend(true,{},b);j.push(o)}else{b[0]=c.rows[d].id;b[1]=b[2]=b[5]=bizagi.localization.getResource("webpart-cases-summary-without-name");b.state="gray";b[6]=[];b[6][0]=[];b[6][0]=[0,c.rows[d].id];b[4]=e.processName;b[3]=e.solutionDate;if(a.isTabletDevice&&c.rows[d].isOfflineForm){var q=bizagi.getGroupedData(f[3]);b[7]=c.rows[d].isOfflineForm;b[8]=c.rows[d].isOpen;b.state=q.status}j.push(b)}}c.elements=j;return c.elements}).fail(function(){var a={elements:[],page:1,totalPages:1};a.elements.push({noResults:true,message:bizagi.localization.getResource("workportal-menu-search-found-no-cases")});return a.elements})},loadDataListBeta:function(a){var b=this;return b.dataService.getCasesListBeta(a).done(function(a){a.elements.length===0&&a.elements.push({noResults:true,message:bizagi.localization.getResource("workportal-menu-search-found-no-cases")});return a.elements}).fail(function(){var a={elements:[],page:1,totalPages:1};a.elements.push({noResults:true,message:bizagi.localization.getResource("workportal-menu-search-found-no-cases")});return a.elements})},getCaseData:function(c){var a=c.dataItem,b={};if(a&&typeof a.noResults==="undefined")if(a[1]!==-1&&a[2]!==-1){b.idCase=a[0];b.idWorkitem=a[1];b.idTask=a[2];b.displayName=a[5]||a[6];b.radNumber=a.radNumber;b.isOfflineForm=a[7]===true||false;b.formsRenderVersion=a[7]===true?2:0}else{b.idCase=a[0];b.displayName=a[4]||a[0]}return b}});
