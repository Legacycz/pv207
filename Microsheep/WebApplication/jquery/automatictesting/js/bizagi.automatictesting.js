﻿var mainDialog = null;
var queryStringParams = bizagi.automatictesting.host.getQueryStringParams();
var recorderOptions = {
    userName: queryStringParams["userName"], 
    domain: queryStringParams["domain"], 
    allowQueryForms: queryStringParams["allowQF"] == "true"
};

if (window.location.protocol == "https:")
    bizagi.automatictesting.host.setRecorderHostUrl("https://localhost/AutoTesting.svc/Recorder");
else
    bizagi.automatictesting.host.setRecorderHostUrl("http://localhost/AutoTesting.svc/Recorder");

var recorder = new bizagi.automatictesting.recorder(recorderOptions);
recorder.render();

var autotest = {};
autotest.getParams = function(input) {
    var i, pos, name, value, 
        pairs = input.split("&"),
        l = pairs.length,
        result = {};
    
    for (i = 0; i < l; i++) {
        pos = pairs[i].indexOf('=');
        if (pos == -1) continue;
        
        name = pairs[i].substring(0, pos);
        value = pairs[i].substring(pos + 1);
        result[name] = unescape(value);
    }
    return result;
};