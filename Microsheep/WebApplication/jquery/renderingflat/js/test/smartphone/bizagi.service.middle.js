/**
* Extend base functionality to serve mocks of real calls to REST services
*
* Based on DiegoP code and modified to automatization process
*
* @author RicharU
*/

bizagi.loader.loadFile(
    bizagi.getJavaScript("common.base.dev.jquery.mockjax"),
    bizagi.getJavaScript("common.base.dev.jquery.mockjson")
    ).then(function () {
        var BIZAGI_RESPONSE_TIME = 0;

        // Define scenarios storage

        bizagi.scenarios = {
            name: "",
            description: "",
            tests: []
        };

        // DUMMIES
        if (BIZAGI_ENABLE_MOCKS) {
            $.mockjax({
                url: 'Rest/Users/CurrentUser',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.currentUser.txt"
            });

            $.mockjax({
                url: 'Rest/Authorization/MenuAuthorization',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.authorization.json"
            });
        }
    });