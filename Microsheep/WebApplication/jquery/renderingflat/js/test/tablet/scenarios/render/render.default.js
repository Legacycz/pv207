bizagi.loader.loadFile(
    bizagi.getJavaScript("common.base.dev.jquery.mockjax"),
    bizagi.getJavaScript("common.base.dev.jquery.mockjson")
    ).then(function() {
        var BIZAGI_RESPONSE_TIME = 0;

        // DUMMIES
        if (BIZAGI_ENABLE_MOCKS) {
            $.mockjax({
                url: 'Rest/Handlers/Render',
                contentType: "json",
                responseTime: BIZAGI_RESPONSE_TIME,
                mockjson: BIZAGI_MOCKS_PATH + "dummy.render.default.txt"
            });
        }
    });