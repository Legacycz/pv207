﻿/*
*   Name: BizAgi Smartphone Tab Container Extension
*   Author: Bizagi Mobile Team
*   Comments:
*   -   This script will redefine the container class to adjust to Smartphone devices
*/

// Auto extend
bizagi.rendering.tab.extend("bizagi.rendering.tab", {}, {

    /* POST RENDER CONTAINER ACTIONS
    =================================================*/
    postRenderContainer: function (tab) {
        var self = this;
        self._super(tab);

        // Apply any plugin or custom code to implement smartphone tabs
        self.suscribeMethods();
    },

    suscribeMethods: function () {
        var self = this;
        var tab = self.container;

        //tabs
        $(tab).bztabs({
            activeTab: (self.activeTab !== undefined && self.activeTab !== null) ? self.activeTab : 0,
            tabNumber: 2,
            domIncluded: self.ready(),
            activate: function (event, ui) {
                self.activeTab = $(event.currentTarget) ? $(event.currentTarget).data("index") : 0;
            }
        });
    }
});
