/*
*   Name: BizAgi Smartphone Render Join Aearch Dialog Extension
*   Author: Bizagi Mobile Team
*   Comments:
*   -   This script will redefine the Join Search Dialog render class to adjust to tablet devices
*/

// Extends itself
bizagi.rendering.search.extend("bizagi.rendering.search", {
    ADVANCED_SEARCH_ID: -1,
    ADVANCED_ADDITION_ID: -2,
    SEARCH_MIN_LENGTH: 3,
    SEARCH_DELAY: 750
}, {

    renderSingle: function () {
        var self = this;
        var properties = self.properties;
        var container = self.getContainerRender();
        var control = self.getControl();
        var textTmpl = self.renderFactory.getTemplate("text");

        if ($(".bz-rn-text", control).length > 0) {
            self.input = $(".bz-rn-text", control);
        } else {
            self.input = $.tmpl(textTmpl).appendTo(control);
        }

        $(".ui-bizagi-render-control", container).html(self.input);
        
        self.getArrow().addClass("bz-rn-input-icon-search");

        if (!properties.editable) {
            container.addClass("bz-command-not-edit");
        } else if (properties.editable && properties.allowTyping) {
            self.input.removeAttr('readonly');
        }

        if (properties.allowSuggest) {
            container.addClass("bz-command-edit-inline");
            self._simplySearch();
        }
        
        if (self.properties.advancedSearch && properties.editable) {
            container.addClass("bz-command-edit-inline");
            self.input.click(function() {
                self.renderSearchForm();
            });
        }

        var currentView = bizagi.kendoMobileApplication.view();
        if (currentView.id == "#homePortal") {
            self.pane = $("#homeportal-pane").data("kendoMobilePane");
        } else {
            self.pane = $("#paneRender").data("kendoMobilePane");
        }
    },

    /*
    *   Add the render data to the given collection in order to send data to the server
    */
    collectData: function (renderValues) {
        var self = this;
        var properties = self.properties;

        // Add the render value
        var xpath = properties.xpath;
        var value = self.getValue();
        var compareValue = properties.originalValue;

        // Filter by valid xpaths and valid values
        // Remove empty validation for value for combos
        if (!compareValue || compareValue.id !== value.id) {//if original value is equal to value
            if (!bizagi.util.isEmpty(xpath) && value !== null && typeof (value) !== "undefined") {
                // Add a validation because sometimes value contains an empty not null object
                if (typeof (value) == "object" && value.id === undefined)
                    return;
                // Add the value to the server
                renderValues[properties.xpath] = value;
            }
        }
    },

    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;
        var control = self.getControl();

        var label = "";
        if (value != undefined && value != null) {
            
            if (typeof (value.value) != "undefined" && value.value) {
                label = value.value;
            } else if (value.additionallabel && value.label) {
                label = value.label + "," + value.additionallabel;
            } else {
                label = value.label;
            }
           
            self.setValue(value, true);
            self.input.val(label);
            self.input.attr("id", value.id);                        
            self.input.html(label);
           
        }

    },

    renderEdition: function () {
        this._super();
        var self = this;

        if (self.properties.advancedSearch) {
            self._advanceSearch();
        }
        else {
            self._simplySearch();
        }
    },

    _simplySearch: function () {
        var self = this;
        var control = self.getControl();
        
        self.input.addClass("ui-autocomplete-input");
        self.input.after("<ul class='bz-rn-autocomplete'></ul>");        

        self.input.on("click keyup", function (e, a) {
            self.autoCompleteSearchResults();
        });

        control.on("touchstart mousedown", "li", function () {
            var elemObject = $.data(this).itemautocomplete;
            self.setDisplayValue({ id: elemObject.id, label: elemObject.value });
            $(this).parent().hide();
        });

        control.on("focusout", "input", function () {
            control.find("ul").hide();
        });
    },

    /* Apply autocomplete capabilities to a single input*/
    autoCompleteSearchResults : function(){
        var self = this;

        $.when(
            self.processRequest({ term: self.input.val() }, function () { })
        ).done(function (resp) {

            var elementUl = self.input.parent().find("ul");
            elementUl.find("li").remove();

            elementUl.css({
                display: "inline-block",
                left: self.input.position().left,
                top: self.input.position().top + 15,
                width: self.input.css("width")
            });

            for (var i = 0 ; i < resp.length; i++) {
                var searchItem = self.renderSearchItem(resp[i]);                       
                $(searchItem).addClass("bz-rn-autocomplete-item");                       
                searchItem.appendTo(elementUl);
            }
        });
    },

    _advanceSearch: function () {
        var self = this,
         properties = self.properties,
         textTmpl = self.renderFactory.getTemplate("edition.search");

        self.inputEdition = $.tmpl(textTmpl, {
            id: properties.id,
            xpath: properties.xpath,
            allowTyping: properties.allowTyping,
            advancedSearch: true
        });

        self.inputEdition.find(".bz-rn-search-button").bind("click", function () {
            self.renderSearchForm();
        });
    },

    renderEditionSearchForm: function (params) {
        var self = this;
        var properties = self.properties;
        var container = $(params.id + " .inputEditionContent");
        var canvas = $(params.id + " .inputEditionContent");
        var kendoView = $(params.id, 'body');

        self.viewContainer = params.viewContainer;

        var formSearchParams = $.extend({}, self.getParams(), {
            container: container,
            canvas: canvas,
            kendoView: kendoView
        });

        var searchForm = new bizagi.rendering.smartphone.helpers.searchForm(self.dataService, self.renderFactory, properties.searchForms, {
            allowFullSearch: properties.allowFullSearch,
            maxRecords: properties.maxRecords
        }, formSearchParams);

        searchForm.renderEdition({
            idRender: properties.id,
            xpath: properties.xpath,
            xpathContext: properties.xpathContext,
            idPageCache: properties.idPageCache
        }).done(function (itemId) {
            self.setValue({ id: itemId, label: "" });
            //send the item selected
            properties.submitOnChange = true;
            bizagi.util.smartphone.startLoading();

            self.pane.navigate("#:back");
            self.submitData();

        }).fail(function () {
            self.pane.navigate("#:back");            
        });

        self.pane.navigate(params.id);
    },

    renderSearchForm: function () {
        var self = this;
        var params = {
            'idCase': bizagi.context.idCase,
            'displayName': self.properties.displayName,
            'content': [],
            'callback': function (params) {
                self.renderEditionSearchForm(params);
            }
        };

        bizagi.webpart.publish("createEditionView", params);

    },

    renderSearchItem: function (item) {
        var self = this;
        var properties = self.properties;
        var searchItem = $.tmpl(self.renderFactory.getTemplate("edition.search.item"), {
            id: item.id,
            label: item.label,
            value: item.value
        });

        searchItem.data("itemautocomplete", item);

        return searchItem;
    },

    processRequest: function (req, add) {
        var self = this,
        properties = self.properties,
            defer = new $.Deferred();
        // Create array for response objects   
        var suggestions = [];
        //self.properties.allowSuggest &&
        if ((req.term.length >= this.Class.SEARCH_MIN_LENGTH)) {
            // Update term property to reflect the search
            properties.term = req.term;
            this.getData()
        .done(function (data) {
            // Process response to highlight matches
            $.each(data, function (i, val) {
                suggestions.push({
                    id: val.id,
                    label: val.value.toString().replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + $.ui.autocomplete.escapeRegex(req.term) + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>"),
                    value: val.value
                });
            });

            addToSuggestions(suggestions);
         
        }).fail(function () {
            //fail
            defer.reject();
        });

        } else {
            addToSuggestions(suggestions);
        }

        /* 
        *   Method to add the special items to the suggestions 
        */
        function addToSuggestions(alSuggestions) {
          
            // Add the clear option
            if (properties.allowClear && (self.value !== undefined)) {
                alSuggestions.push({ id: self.Class.CLEAR_SEARCH_ID, label: '', value: '' });
            }
            // Add additional option if advanced search is on
            if (properties.advancedSearch) {
                alSuggestions.push({ id: self.Class.ADVANCED_SEARCH_ID, label: '', value: '' });
            }
            // Add additional option if allow addition is on
            if (properties.allowAdd) {
                alSuggestions.push({ id: self.Class.ADVANCED_ADDITION_ID, label: '', value: '' });
            }
            // Pass array to callback   
           
            defer.resolve(alSuggestions);
            
        }

        return defer.promise();
    },

    setDisplayValueEdit: function (value) {
        var self = this;
        self.inputEdition.find(".ui-bizagi-render-search").val(self.selectedValue);
    },

    //on the desktop only sends the id to submit 
    //in the smartphone it send the id and the value
    submitData: function () {
        var self = this;
        var properties = self.properties;
        // Add current data
        var data = {};
        data[properties.xpath] = self.getValue();
        // Executes submit on change
        if (properties.submitOnChange && data[properties.xpath].id != undefined)
            self.submitOnChange(data, true);
    },

    actionSave: function () {
        var self = this;
        self.input.html(self.value.value || self.value.label);
        self.input.val(self.value.value || self.value.label);
        self.input.attr('id', self.value.id);
    },

    changeRequired: function (argument) {
        var self = this,
        properties = self.properties;
        if (typeof properties.value != "object") {
            self.super();
            return;
        }
        var labelElement = $("label", self.getLabel());
        // Update properties
        properties.required = argument;

        // Changes label
        if ($.isEmptyObject(properties.value)) {
            labelElement.text((properties.displayName || "") + ' ');

            labelElement.before('<div class="bz-rn-required" ></div>');

            self.setPlaceHolder();
        } else {
            // labelElement.css("font-weight", "");
            labelElement.text((properties.displayName || "")); //+ ' :'
        }
        // Perform validations again to check if the form is valid after this change
        self.triggerRenderValidate();
    }
});
