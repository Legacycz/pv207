﻿/*
*   Name: BizAgi smartphone Render Label Extension
*   Author: oscaro
*   Comments:
*   -   This script will redefine the label render class to adjust to smartphones devices
*/

// Extends itself
bizagi.rendering.upload.extend("bizagi.rendering.upload", {
    BA_ACTION_PARAMETER_PREFIX: bizagi.render.services.service.BA_ACTION_PARAMETER_PREFIX,
    BA_CONTEXT_PARAMETER_PREFIX: bizagi.render.services.service.BA_CONTEXT_PARAMETER_PREFIX,
    QUALITY_PICTURE: 80,
    LIMIT: 1, //limit: The maximum number of audio clips,video clips,etc in the device user can record in a single capture operation.
    EXTENSIONSIMG: ["image/jpeg", "jpeg", "image", "png", "jpg"],
    EXTENSIONSVIDEO: ["video/quicktime", "quicktime", "qt", "mov"],
    EXTENSIONSAUDIO: ["audio/wav", "audio", "wav"]
}, {
    renderUploadImage: function(params) {
        var self = this;

        var properties = self.properties;
        var parameters = {
            url: properties.dataUrl,
            xpath: properties.xpath,
            idRender: properties.id,
            xpathContext: properties.xpathContext,
            idPageCache: properties.idPageCache,
            property: "fileContent",
            dataType: "text",
            getRequest: true,
            fileId: params.fileid
        };

        bizagi.util.smartphone.startLoading();
        self.dataService.multiaction().getPropertyData(parameters, "text").done(function(data) {
            if (data[1] != "") {
                url = "data:image/png;base64," + data[0][1];
            }

            var createViewParams = {
                'idCase': bizagi.context.idCase,
                'displayName': params.displayName,
                'content': $.tmpl(self.renderFactory.getTemplate("image-preview"), { 'url': url }),
                'kendoViewOptions': { 'zoom': true, 'hide': function() { this.destroy(); } },
                'titleLable': false,
                'kendoViewFinish': function() {
                    setTimeout(function() {
                        var view = bizagi.kendoMobileApplication.view().element;
                        var originalImageWidth = $('.ui-bizagi-render-preview', view).width();
                        var adjustedImageWidthScale = (view.width() < originalImageWidth) ? (view.width() / originalImageWidth) : 1;
                        bizagi.kendoMobileApplication.scroller().movable.scaleTo(adjustedImageWidthScale);
                        $('.ui-bizagi-container-inputs', view).css('padding', 0);
                    }, 200);
                },
                getNavigation: true,
                'callback': function(params) {
                    bizagi.kendoMobileApplication.navigate(params.id);
                    params.navigation.setNavigationButtons(params.viewContainer, 'preview');
                }
            };
            bizagi.webpart.publish("createEditionView", createViewParams);
            bizagi.util.smartphone.stopLoading();
        });
    },

    postRenderSingle: function() {
        var self = this;
        var properties = self.properties;
        var container = self.getControl();
        var containerRender = self.getContainerRender();
        var body = $("body");
        self.configureHelpText();
        self.getArrowContainer().hide();

        if (!properties.editable) {
            containerRender.addClass("bz-command-not-edit");
        } else {
            containerRender.addClass("bz-command-edit-inline");
            container.addClass("ui-padding-bottom-10");
        }

        self.itemAddfile = $(".bz-rn-upload-show-menu", container);
        self.deleteIconHandler();

        if (typeof(cordova) === "undefined" || properties.editable == false) {
            self.itemAddfile.hide();
            return;
        }

        $(".ui-bizagi-render-upload-item > span", self.getControl()).click(function(e) {
            window.open($(this).data("url"), '_system', 'location=yes');
        });

        $(".ui-bizagi-render-edition-arrow", self.getControl().parent()).hide();

        var itemsUpload = self.checkExtensions();
        var actionSheetUploadTmpl = kendo.template(bizagi.templates.services.service.cachedTemplates["actionsheetUpload-tmpl"], { useWithBlock: false });
        var actionSheetUpload = actionSheetUploadTmpl(itemsUpload);
        $(container).append(actionSheetUpload);
        self.actionSheetUpload = $(".actionsheetUpload", container).kendoMobileActionSheet();


        // Resolution upload menu
        var resolutionUpload = {
            'buttons': [
                { 'ordinal': 1, 'caption': bizagi.localization.getResource("workportal-size-small") },
                { 'ordinal': 2, 'caption': bizagi.localization.getResource("workportal-size-medium") },
                { 'ordinal': 3, 'caption': bizagi.localization.getResource("workportal-size-large") },
                { 'ordinal': 4, 'caption': bizagi.localization.getResource("workportal-size-original") }
            ],
            'id': self.properties.id
        };


        var actionSheetResolutionUploadTmpl = kendo.template(bizagi.templates.services.service.cachedTemplates["actionSheetResolutionUpload-tmpl"], { useWithBlock: false });
        var actionSheetResolutionUpload = actionSheetResolutionUploadTmpl(resolutionUpload);
        $(container).append(actionSheetResolutionUpload);
        self.actionSheetResolutionUpload = $(".actionsheetResolutionUpload", container).kendoMobileActionSheet();


        self.itemAddfile.bind("click", function(e) {
            e.preventDefault();
            e.stopPropagation();
            $(self.actionSheetUpload).data('kendoMobileActionSheet').open();
        });

        self.addListener();
        self.checkMaxFiles();
    },

    deleteIconHandler: function() {
        var self = this;

        $(".bz-rn-upload-delete-icon", self.getControl()).bind("click", function() {
            var item = $(this).parent(".ui-bizagi-render-upload-item");
            var file = item.data();
            $.when(self.deleteUploadItem(item, file.id)).done(function() {
                // Remove item
                item.hide();

                // Detach item
                item.remove();

                // Check maxFiles
                self.checkMaxFiles();

                // Trigger change
                self.triggerRenderChange();
            });
        });
    },

    addListener: function() {
        var self = this;

        $("a[data-bz-ordinal]", self.actionSheetUpload).kendoMobileButton({
            click: function(e) {


                var photoParams = { quality: self.Class.QUALITY_PICTURE, targetWidth: 1280, targetHeight: 960 };

                if (e.button.data("bz-ordinal") === 2) { //select pic
                    navigator.camera.getPicture(function(dataImage) {

                        self.dataImage = dataImage;
                        bizagi.util.smartphone.startLoading();
                        $.when(self.checkMaxSize(dataImage)).done(function(resp) {
                            if (resp) {
                                $(self.actionSheetUpload).data('kendoMobileActionSheet').close();
                                $(self.actionSheetResolutionUpload).data('kendoMobileActionSheet').open();
                            }
                            bizagi.util.smartphone.stopLoading();
                        });
                    }, self.onFail, {
                        quality: self.Class.QUALITY_PICTURE,
                        sourceType: Camera.PictureSourceType.PHOTOLIBRARY
                    });
                } else if (e.button.data("bz-ordinal") === 1) { //take pic
                    navigator.camera.getPicture(function(dataImage) {

                        bizagi.util.smartphone.startLoading();
                        $.when(self.checkMaxSize(dataImage)).done(function(responseInternal) {
                            if (responseInternal)
                                self.saveImage(self, dataImage);
                            bizagi.util.smartphone.stopLoading();
                        });
                    },
                        self.onFail,
                        {
                            quality: self.Class.QUALITY_PICTURE,
                            sourceType: navigator.camera.PictureSourceType.CAMERA,
                            mediaType: navigator.camera.MediaType.PICTURE,
                            destinationType: Camera.DestinationType.FILE_URI,
                            correctOrientation: true,
                            saveToPhotoAlbum: true,
                            targetWidth: 1280,
                            targetHeight: 960
                        });
                } else if (e.button.data("bz-ordinal") === 3) { //capture video
                    navigator.device.capture.captureVideo(function(dataImage) {

                        bizagi.util.smartphone.startLoading();
                        $.when(self.checkMaxSizeVideo(dataImage)).done(function(responseInternal) {
                            if (responseInternal)
                                self.saveVideo(self, dataImage);
                            bizagi.util.smartphone.stopLoading();
                        });
                    }, self.onFail, { limit: self.Class.LIMIT });
                } else if (e.button.data("bz-ordinal") === 4) { //capture audio
                    navigator.device.capture.captureAudio(function(mediaFiles) {

                        bizagi.util.smartphone.startLoading();
                        var audioUrl = mediaFiles[0].localURL;
                        $.when(self.checkMaxSize(audioUrl)).done(function(responseInternal) {
                            if (responseInternal)
                                self.saveAudio(self, mediaFiles);
                            bizagi.util.smartphone.stopLoading();
                        });

                    }, self.onFail, { limit: self.Class.LIMIT });

                }
            }
        });

        $("a[data-bz-ordinal]", self.actionSheetResolutionUpload).kendoMobileButton({
            click: function(e) {

                bizagi.util.smartphone.startLoading();
                var objWidth = 0;
                var objHeight = 0;


                if (e.button.data("bz-ordinal") == 1) {
                    objWidth = 320;
                    objHeight = 240;
                } else if (e.button.data("bz-ordinal") == 2) {
                    objWidth = 640;
                    objHeight = 480;
                } else if (e.button.data("bz-ordinal") == 3) {
                    objWidth = 1280;
                    objHeight = 960;
                }

                //if original, save image normally, if not, reduce quality and size
                if (e.button.data("bz-ordinal") == 4) {
                    self.saveImage(self, self.dataImage);
                } else {
                    $.when(self.transformImageSize(self.dataImage, objWidth, objHeight)).done(function() {
                        self.saveImage(self, self.dataImage);
                        bizagi.util.smartphone.stopLoading();
                    });
                }

            }
        });
    },


    /*
    *   Template method to implement in each children to customize each control
    */
    renderControl: function() {
        var self = this,
            properties = self.properties;

        var template = self.renderFactory.getTemplate("upload");

        // Render template
        var html = $.fasttmpl(template, {
            xpath: bizagi.util.encodeXpath(self.getUploadXpath()),
            editable: properties.editable,
            noFiles: (self.filesCount == 0),
            allowSendInMail: properties.allowSendInMail
        });

        // Render current children
        var items = "";
        for (var i = 0; i < self.filesCount; i++) {
            var file = { id: self.files[i][0], fileName: self.files[i][1] };
            var item = self.renderUploadItem(file);
            items += item;
        }
        html = self.replaceFilesHtml(html, items);
        return html;
    },

    /* 
    *   Renders a single upload item 
    */
    renderUploadItem: function(file) {
        var self = this;
        var properties = self.properties;
        var mode = self.getMode();
        var image = false;


        var template = self.renderFactory.getTemplate("uploadItem");
        var url = self.buildItemUrl(file);

        // Don't render urls when not running in execution mode
        if (mode != "execution")
            url = "javascript:void(0);";

        if (self.checkImageFile(file.fileName)) {
            image = true;
        }

        var html = $.fasttmpl(template, {
            url: url,
            image: image,
            allowDelete: properties.allowDelete,
            filename: file.fileName,
            id: file.id,
            mode: mode
        });

        return html;
    },

    checkImageFile: function(fileName) {
        var self = this;
        var validExtensions = self.Class.EXTENSIONSIMG;

        for (var i = 0; i < validExtensions.length; i++) {
            var image = fileName.indexOf('.' + validExtensions[i]);

            if (image >= 0) {
                return true;
            }
        }
        return false;
    },

    getTemplateName: function() {
        return "upload";
    },

    getTemplateItemName: function() {
        return "uploadItem";
    },

    getTemplateEditionName: function() {
        return "edition.upload";
    },

    getTemplateEditionMenu: function() {
        return "edition.upload.menu";
    },

    saveImage: function(context, dataImage) {
        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);
        window.resolveLocalFileSystemURI(dataImage, function(fileEntry) {

            fileEntry.file(function(file) {

                data.filename = options.fileName;

                if (!self.validateFileExtensions(options.fileName)) {
                    var extension = file.type.substr(file.type.lastIndexOf('/') + 1);
                    options.fileName += "." + extension;
                }

                options.mimeType = "image/jpeg";
                options.params = data;
                var ft = new FileTransfer();

                if (self.editedImageURL) {
                    dataImage = self.editedImageURL;
                }

                //fix android 4.4 getting images from recent folder
                if (dataImage.substring(0, 21) == "content://com.android") {
                    var photoSplit = dataImage.split("%3A");
                    dataImage = "content://media/external/images/media/" + photoSplit[1];
                }

                ft.upload(dataImage, properties.addUrl,
                    function(r) {
                        self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
                    },
                    function(error) {
                        bizagi.log("An error has occurred: Code = " + error.code);
                    }, options);

                bizagi.util.smartphone.hideStatusBar();
                bizagi.util.smartphone.stopLoading();

            }, function() { //error
            });

        }, function() { // error 
        });
    },

    saveAudio: function(context, dataAudio) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileName = dataAudio[0].name;
        //options.mimeType = "audio/wav";
        options.params = data;
        var ft = new FileTransfer();


        ft.upload(dataAudio[0].fullPath, properties.addUrl,
            function(r) {
                var response = JSON.parse(decodeURIComponent(r.response));
                if (response.type === 'error') {
                    bizagi.showMessageBox(response.message, response.errorType);
                } else {
                    self.onUploadFileCompleted(context, response);
                }
            },
            function(error) {
                bizagi.log("An error has occurred: Code = " + error.code);
            }, options);

        bizagi.util.smartphone.hideStatusBar();

    },

    saveVideo: function(context, dataVideo) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileName = dataVideo[0].name;
        // options.mimeType = "video/quicktime";
        options.params = data;
        var ft = new FileTransfer();

        ft.upload(dataVideo[0].fullPath, properties.addUrl,
            function(r) {
                var response = JSON.parse(decodeURIComponent(r.response));
                if (response.type === 'error') {
                    bizagi.showMessageBox(response.message, response.errorType);
                } else {
                    self.onUploadFileCompleted(context, response);
                }

            },
            function(error) {

                bizagi.log("An error has occurred: Code = " + error.code);

            }, options);

        bizagi.util.smartphone.hideStatusBar();

    },

    onFail: function(error) {
        bizagi.log('Error code: ' + error.code);
        bizagi.util.smartphone.stopLoading();
    },

    onUploadFileCompleted: function(context, response) {
        var self = context,
            control = self.getControl();

        var uploadWrapper = $(".bz-rn-upload-show-menu", control);

        var result = response;

        if (result.id && result.fileName) {
            var newItem = self.renderUploadItem(result);

            self.files.push([result.id, result.fileName]);

            $(".ui-bizagi-render-upload-container .files", control).append(newItem);

            // Increment counter

            self.filesCount = self.filesCount + 1;

            self.triggerRenderChange();

            $("span", self.control).unbind("click").bind("click", function(e) {
                window.open($(this).data("url"), '_system', 'location=yes');
            });

            self.deleteIconHandler();

            // Check maxFiles

            self.checkMaxFiles();
            if (self.properties.type == "columnUpload") {
                //refresh to have synchronized the file between sever and client
                self.triggerRenderChange();                     
                bizagi.util.smartphone.stopLoading();

                setTimeout(function () {
                    bizagi.util.smartphone.startLoading();

                    self.getFormContainer().refreshForm();

                    // bizagi.util.smartphone.stopLoading();
                }, 200);
            } else {
                self.refreshControl();
            }

            self.editedImageURL = null;

        } else {
            bizagi.log("Error:" + result.message);

        }

        bizagi.util.smartphone.hideStatusBar();
    },

    checkMaxFiles: function() {
        var self = this,
            properties = self.properties,
            maxFiles = properties.maxfiles,
            actualFiles = properties.value.length;

        if ( maxFiles != 0 && ( (self.filesCount >= maxFiles && actualFiles >= maxFiles)) ){
            self.itemAddfile.hide();
        }
        else{
            self.itemAddfile.show();
        }
    },

    checkExtensions: function() {
        var self = this;
        var properties = self.properties;
        var validExtensions = properties.validExtensions;

        var enableVideo = enableAudio = enableImage = false;

        if (typeof validExtensions === "undefined" || validExtensions == "") {
            enableVideo = enableAudio = enableImage = true;
        } else {

            validExtensions = validExtensions.split(";");

            if (validExtensions.length == 1 && validExtensions[0].indexOf("*.*") !== -1) {
                enableVideo = enableAudio = enableImage = true;
            } else {

                for (var i = 0; i < validExtensions.length; i++) {
                    validExtensions[i] = validExtensions[i].replace("*.", "");
                    var image = self.Class.EXTENSIONSIMG.toString().indexOf(validExtensions[i]);
                    var audio = self.Class.EXTENSIONSAUDIO.toString().indexOf(validExtensions[i]);
                    var video = self.Class.EXTENSIONSVIDEO.toString().indexOf(validExtensions[i]);

                    if (image >= 0) {
                        enableImage = true;
                    }
                    if (audio >= 0) {
                        enableAudio = true;
                    }

                    if (video >= 0) {
                        enableVideo = true;
                    }

                }
            }
        }

        var itemsUpload = { 'buttons': [], 'id': properties.id };

        //add image options 
        if (enableImage) {
            itemsUpload.buttons.push(
                {
                    'ordinal': 1,
                    'caption': bizagi.localization.getResource("workportal-actionsheet-upload-take-photo")
                });
            itemsUpload.buttons.push(
                {
                    'ordinal': 2,
                    'caption': bizagi.localization.getResource("workportal-actionsheet-upload-choose-photo")
                });
        }
        //add video options         
        if (enableVideo) {
            itemsUpload.buttons.push(
                {
                    'ordinal': 3,
                    'caption': bizagi.localization.getResource("workportal-actionsheet-upload-take-video")
                });
        }
        //add audio options         
        if (enableAudio) {
            itemsUpload.buttons.push(
                {
                    'ordinal': 4,
                    'caption': bizagi.localization.getResource("workportal-actionsheet-upload-record-audio")
                });
        }

        return itemsUpload;


    },
    checkMaxSize: function(objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURL(objectUri, function(fileEntry) {
            fileEntry.file(function(fileObj) {

                if (fileObj.size >= properties.maxSize) {
                    //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    bizagi.showMessageBox(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize), "Error"); 
                    defer.resolve(false);
                } else {
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    },

    checkMaxSizeVideo: function(objectVideo) {

        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        var size = objectVideo[0].size;

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        if (size >= properties.maxSize) {
            //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
            bizagi.showMessageBox(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize), "Error");
            defer.resolve(false);
        } else {
            defer.resolve(true);
        }


        return defer.promise();

    },
    validateFileExtensions: function(nameArchivo) {
        var validExtensions;
        var mensaje;
        validExtensions = /(.jpg|.gif|.png|.bmp|.jpeg|.mov|.avi|.mpg|.mpeg|.3gp|.asf|.wmv|.flv|.mp4)$/i;

        if (validExtensions.test(nameArchivo)) {
            return true;
        }
        return false;

    },

    saveBlobToFile: function(blob, defer) {
        // root file system entry
        var self = this;

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
            function(fileSystem) {
                var root = fileSystem.root;
                // writes a file
                var writeFile = function(writer) {

                    writer.onwriteend = function(e) {
                        self.editedImageURL = writer.localURL;
                        defer.resolve();
                        console.log('Write completed.');
                    };

                    writer.onerror = function(e) {
                        console.log('Write failed: ' + e.toString());
                    };

                    // write to file
                    writer.write(blob);
                };

                // creates a FileWriter object
                var createWriter = function(fileEntry) {
                    fileEntry.createWriter(writeFile, null);
                };

                var newId = "bizagiImgTmp";

                // create a file and write to it
                root.getFile(newId + '.jpg', { create: true }, createWriter, null);
            },
            function() {
            });
    },
    /*
    *
    */
    transformImageSize: function(objectUri, objWidth, objHeight) {
        var self = this;
        var defer = new $.Deferred();

        window.resolveLocalFileSystemURL(objectUri, function(fileEntry) {
            fileEntry.file(function(fileObj) {

                var reader = new FileReader();

                // Create a function to process the file once it's read
                reader.onloadend = function(evt) {
                    // Create an image element that we will load the data into
                    var image = new Image();
                    image.onload = function(evt) {

                        var width = image.width;
                        var height = image.height;

                        if (objWidth != 0) {

                            if (width > height) {
                                if (width > objWidth) {
                                    height *= objWidth / width;
                                    width = objWidth;
                                }
                            } else {
                                if (height > objHeight) {
                                    width *= objHeight / height;
                                    height = objHeight;
                                }
                            }
                        }

                        var canvas = document.createElement("canvas");
                        canvas.width = width;
                        canvas.height = height;
                        canvas.getContext("2d").drawImage(this, 0, 0, width, height);

                        if (self.value && self.properties.value) {
                            self.value[0] = self.properties.value[0] = self.properties.url = canvas.toDataURL("image/jpeg", 0.5);
                        } else {
                            self.properties.url = canvas.toDataURL("image/jpeg", 0.5);
                        }

                        var imageToSaveLocal = self.properties.url.replace("data:image/jpeg;base64,", "");
                        var imageBlob = bizagi.util.b64toBlob(imageToSaveLocal);
                        self.saveBlobToFile(imageBlob, defer);

                        image = null;

                    };
                    // Load the read data into the image source. It's base64 data
                    image.src = evt.target.result;
                };
                // Read from disk the data as base64
                reader.readAsDataURL(fileObj);

            });
        });
        return defer.promise();
    }
});
