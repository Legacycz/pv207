/*
*   Name: BizAgi Render Image Class
*   Author: Edward J Morales
*   Comments:
*   -   This script will redefine the image render class to adjust to smathphone devices
*/

// Extends itself
bizagi.rendering.image.extend("bizagi.rendering.image", {
    QUALITY_PICTURE: 80,
    LIMIT: 1
}, {

    renderSingle: function () {
        var self = this;
        var control = self.getControl();
        //var container = self.getContainerRender();
        //container.addClass("bz-command-edit-inline");
        $('.ui-bizagi-render-edition-arrow', self.getControl().parent()).hide();
        //bug render image control
        if (typeof (cordova) !== "undefined") {
            self.activeUploadNative();
        } else {
            $('.bz_rn_upload_container_upload_items_inline', control).hide();
        }
        if (control.find('img').length > 0) {
            $('.ui-bizagi-render-image-wrapper', control).addClass('no-after-image'); //removes little thumb
        }
        if (control.find(".image-wrapper").length == 1) {
            return true;
        }
        $.when(self.renderControl()).done(function (html) {
            control.append(html);
            if (!self.properties.editable) {
                $('.ui-bizagi-render-image-wrapper', control).addClass('no-after-image'); //removes little thumb
                $('.ui-bizagi-render-image-wrapper', control).find(".bz_rn_upload_container_upload_items_inline").hide();
            }
        });
    },


    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;
        // Call base
        self._super();
    },

    activeUploadNative: function () {
        var self = this;
        var container = self.getContainerRender();

        var itemsUpload = { 'buttons': [
                            {
                                'ordinal': 1,
                                'caption': bizagi.localization.getResource("workportal-actionsheet-upload-take-photo")
                            }, {
                                'ordinal': 2,
                                'caption': bizagi.localization.getResource("workportal-actionsheet-upload-choose-photo")
                            }
         ], 'id': self.properties.id
        };

        var resolutionUpload = { 'buttons': [
            	     	            { 'ordinal': 1, 'caption': bizagi.localization.getResource("workportal-size-small") },
            	     	            { 'ordinal': 2, 'caption': bizagi.localization.getResource("workportal-size-medium") },
            	     	            { 'ordinal': 3, 'caption': bizagi.localization.getResource("workportal-size-large") },
            	     	            { 'ordinal': 4, 'caption': bizagi.localization.getResource("workportal-size-original") }
            	    ], 'id': self.properties.id
        };


        var actionSheetResolutionUploadTmpl = kendo.template(bizagi.templates.services.service.cachedTemplates["actionSheetResolutionUpload-tmpl"], { useWithBlock: false });
        var actionSheetResolutionUpload = actionSheetResolutionUploadTmpl(resolutionUpload);
        $(container).append(actionSheetResolutionUpload);
        self.actionSheetResolutionUpload = $(".actionsheetResolutionUpload", container).kendoMobileActionSheet();



        var actionSheetUploadTmpl = kendo.template(bizagi.templates.services.service.cachedTemplates["actionsheetUpload-tmpl"], { useWithBlock: false });
        var actionSheetUpload = actionSheetUploadTmpl(itemsUpload);
        $(container).append(actionSheetUpload);
        self.actionSheetUpload = $(".actionsheetUpload", container).kendoMobileActionSheet();

        $(".options", container).bind("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(self.actionSheetUpload).data('kendoMobileActionSheet').open();
        });
        self.addListener();

    },

    addListener: function () {

        var self = this;

        //The action sheet link was changed to a kendo button because the action click wasn't working
        //Kendo should release a click event eventually
        $("a[data-bz-ordinal]", self.actionSheetUpload).kendoMobileButton({
            click: function (e) {

                self.actionSelected = e.button.data("bz-ordinal");


                var fileParams = { quality: self.Class.QUALITY_PICTURE, sourceType: Camera.PictureSourceType.PHOTOLIBRARY };
                var photoParams = { quality: self.Class.QUALITY_PICTURE, targetWidth: 1280, targetHeight: 960 };

                if (self.actionSelected === 2) { //select pic
                    navigator.camera.getPicture(function (dataImage) {

                        self.dataImage = dataImage;
                        $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                            if (resp) {
                                bizagi.util.smartphone.stopLoading();
                                $(self.actionSheetUpload).data('kendoMobileActionSheet').close();
                                $(self.actionSheetResolutionUpload).data('kendoMobileActionSheet').open();
                            } else {
                                bizagi.util.smartphone.stopLoading();
                            }
                        });

                    }, self.onFail, fileParams);
                } else if (self.actionSelected === 1) { //take pic
                    navigator.camera.getPicture(function (dataImage) {
                        $.when(self.checkMaxSize(dataImage)).done(function (resp) {
                            if (resp) {
                                self.saveImage(self, dataImage);
                            }
                            else {
                                bizagi.util.smartphone.stopLoading();
                            }
                        });
                    },
                        self.onFail,
                        photoParams);
                }



            }
        });

        $("a[data-bz-ordinal]", self.actionSheetResolutionUpload).kendoMobileButton({

            click: function (e) {

                bizagi.util.smartphone.startLoading();
                var objWidth = 0;
                var objHeight = 0;


                if (e.button.data("bz-ordinal") == 1) {
                    objWidth = 320;
                    objHeight = 240;
                } else if (e.button.data("bz-ordinal") == 2) {
                    objWidth = 640;
                    objHeight = 480;
                } else if (e.button.data("bz-ordinal") == 3) {
                    objWidth = 1280;
                    objHeight = 960;
                }

                //if original, save image normally, if not, reduce quality and size
                if (e.button.data("bz-ordinal") == 4) {
                    self.saveImage(self, self.dataImage);
                }
                else {
                    $.when(self.transformImageSize(self.dataImage, objWidth, objHeight)).done(function () {
                        self.saveImage(self, self.dataImage);
                    });
                }

            }

        });
    },

    saveImage: function (context, dataImage) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();
        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.params = data;
        var ft = new FileTransfer();

        if (self.editedImageURL) {
            dataImage = self.editedImageURL;
        }

        //fix android 4.4 getting images from recent folder
        if (dataImage.substring(0, 21) == "content://com.android") {
            var photoSplit = dataImage.split("%3A");
            dataImage = "content://media/external/images/media/" + photoSplit[1];
        }

        ft.upload(dataImage, properties.addUrl,
         function (r) {
             var response = JSON.parse(decodeURIComponent(r.response));
             if (response.type === 'error') {
                 bizagi.showMessageBox(response.message, response.errorType);
                 bizagi.util.smartphone.stopLoading();
             } else {
                 self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
             }
         },
         function (error) {
             bizagi.log("An error has occurred: Code = " + error.code);
             bizagi.util.smartphone.stopLoading();
         }
         , options);

        bizagi.util.smartphone.hideStatusBar();
    },

    onFail: function (error) {
        bizagi.log('Error code: ' + error.code);
        bizagi.util.smartphone.stopLoading();
        setTimeout(function () {
            //hide statusBar
            if (device && device.platform === 'iOS' && window.plugin && window.plugin.statusbarOverlay) {
                window.plugin.statusbarOverlay.hide();
            }
        }, 1000);

    },

    onUploadFileCompleted: function (context, response) {
        var self = context;
        try {
            //self.submitOnChange();
            $.when(self.renderUploadItem()).done(function (htmlImage) {

                // Empty container and add new image            	
                var control = self.getControl();
                $('.ui-bizagi-render-image-wrapper', control).addClass('no-after-image');
                var imageWrapper = $(".ui-bizagi-render-image-wrapper", control);

                $(imageWrapper).html("");


                //this is needed to update the state of the control and delete the required icon
                self.setValue([true]);

                //set to the new image the click event
                self.activeUploadNative();


                if (self.properties.type == "columnImage") {
                    //refresh to have synchronized the image between sever and client, it is needed to view the image in the zoom view once it is uploaded
                    bizagi.util.smartphone.stopLoading();
                    self.getFormContainer().saveForm();
                    self.getFormContainer().refreshForm();
                } else {
                    $.when(self.refreshControl()).done(function () {
                        $(imageWrapper).append(htmlImage);
                        self.editedImageURL = null;

                        setTimeout(function () {
                            bizagi.util.smartphone.stopLoading();
                        }, 1000);

                    });
                }

            });
        } catch (e) {
            bizagi.util.smartphone.stopLoading();
            self.getFormContainer().refreshForm();
        }
    },

    /*
    *   Template method to implement in each device to customize the render's behaviour when rendering in design mode
    */
    configureDesignView: function () {
        var self = this;
        // Call base
        self._super();
    },

    /*
    *   Template method to implement in each device to customize each render after processed in read-only mode
    */
    postRenderReadOnly: function () {
        var self = this;
        var container = self.getContainerRender();
        container.addClass("bz-command-edit-inline");
    },

    saveBlobToFile: function (blob, defer) {
        // root file system entry
        var self = this;

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
            function (fileSystem) {
                var root = fileSystem.root;
                // writes a file
                var writeFile = function (writer) {

                    writer.onwriteend = function (e) {
                        self.editedImageURL = writer.localURL;
                        defer.resolve();
                        console.log('Write completed.');
                    };

                    writer.onerror = function (e) {
                        console.log('Write failed: ' + e.toString());
                    };

                    // write to file
                    writer.write(blob);
                };

                // creates a FileWriter object
                var createWriter = function (fileEntry) {
                    fileEntry.createWriter(writeFile, null);
                };

                var newId = "bizagiImgTmp";

                // create a file and write to it
                root.getFile(newId + '.jpg', { create: true }, createWriter, null);
            },
            function () { });
    },
    /*
    *
    */
    transformImageSize: function (objectUri, objWidth, objHeight) {
        var self = this;
        var defer = new $.Deferred();

        window.resolveLocalFileSystemURL(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {

                var reader = new FileReader();

                // Create a function to process the file once it's read
                reader.onloadend = function (evt) {
                    // Create an image element that we will load the data into
                    var image = new Image();
                    image.onload = function (evt) {

                        var width = image.width;
                        var height = image.height;

                        if (objWidth != 0) {

                            if (width > height) {
                                if (width > objWidth) {
                                    height *= objWidth / width;
                                    width = objWidth;
                                }
                            } else {
                                if (height > objHeight) {
                                    width *= objHeight / height;
                                    height = objHeight;
                                }
                            }
                        }

                        var canvas = document.createElement("canvas");
                        canvas.width = width;
                        canvas.height = height;
                        canvas.getContext("2d").drawImage(this, 0, 0, width, height);

                        if (self.value && self.properties.value) {
                            self.value[0] = self.properties.value[0] = self.properties.url = canvas.toDataURL("image/jpeg", 0.5);
                        } else {
                            self.properties.url = canvas.toDataURL("image/jpeg", 0.5);
                        }

                        var imageToSaveLocal = self.properties.url.replace("data:image/jpeg;base64,", "");
                        var imageBlob = bizagi.util.b64toBlob(imageToSaveLocal);
                        self.saveBlobToFile(imageBlob, defer);

                        image = null;

                    };
                    // Load the read data into the image source. It's base64 data
                    image.src = evt.target.result;
                };
                // Read from disk the data as base64
                reader.readAsDataURL(fileObj);

            });
        });
        return defer.promise();
    },

    /*
    *
    */
    checkMaxSize: function (objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURI(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {
                if (fileObj.size >= properties.maxSize) {
                    //'the file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    bizagi.showMessageBox(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize), "Error");
                    defer.resolve(false);
                }
                else {
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    },

    renderEdition: function () {
        var self = this;


        $(".ui-bizagi-render-preview").remove();
        self.inputEdition = $.tmpl(self.renderFactory.getTemplate("image-preview"), { 'url': self.value[0] });
        self.kendoViewOptions = { 'zoom': true };
        self.renderEditionType = 'preview';
        self.titleLable = false;
        self.kendoViewFinish = function () {
            var originalImageWidth = self.inputEdition.width();
            var view = bizagi.kendoMobileApplication.view().element;
            var adjustedImageWidthScale = (view.width() < originalImageWidth) ? (view.width() / originalImageWidth) : 1;

            if (bizagi.kendoMobileApplication.scroller() && bizagi.kendoMobileApplication.scroller().movable) {
                bizagi.kendoMobileApplication.scroller().movable.scaleTo(adjustedImageWidthScale);
            }

            $('.ui-bizagi-container-inputs', view).css('padding', 0);

            bizagi.util.smartphone.hideStatusBar();
        };

        $.when(self.buildFullItemUrl()).done(function (dataUrl) {

            if (dataUrl.indexOf("type:error") > 0) {
                //do nothing
            } else {
                $(".ui-bizagi-render-preview").attr("src", dataUrl);
                var originalImage = $(".ui-bizagi-render-preview")[0];
                var originalImageWidth = $(originalImage).css("width");
                originalImageWidth = Number(originalImageWidth.substring(0, originalImageWidth.length - 2));

                if (self.properties.width != -1 && self.properties.width < originalImageWidth) {
                    $(".ui-bizagi-render-preview").css("width", "100%");
                }
            }

        });

    }
});
