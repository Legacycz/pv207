﻿/*
 *   Name: BizAgi Tablet Render Factory
 *   Author: Diego Parra
 *   Comments:
 *   -   This script will define a render factory to create tablet versions of each render type
 */

bizagi.rendering.base.factory.extend("bizagi.rendering.tablet.factory", {}, {

    /*
    *   Load all the templates used for rendering
    */
    loadTemplates: function () {
        var self = this;
        var defer = new $.Deferred();

        $.when(
        // Common
            self.loadTemplate("form-error", bizagi.getTemplate("bizagi.renderingflat.tablet.form-error")),
            self.loadTemplate("form-waiting", bizagi.getTemplate("bizagi.renderingflat.tablet.form-waiting")),

        // Containers
            self.loadTemplate("form", bizagi.getTemplate("bizagi.renderingflat.tablet.form") + "#ui-bizagi-render-form"),
            self.loadTemplate("complexgateway", bizagi.getTemplate("bizagi.renderingflat.tablet.form") + "#ui-bizagi-render-complexGateway"),
            self.loadTemplate("panel", bizagi.getTemplate("bizagi.renderingflat.tablet.panel")),
            self.loadTemplate("tab", bizagi.getTemplate("bizagi.renderingflat.tablet.tab")),
            self.loadTemplate("tabItem", bizagi.getTemplate("bizagi.renderingflat.tablet.tabItem")),
            self.loadTemplate("horizontal", bizagi.getTemplate("bizagi.renderingflat.tablet.horizontal")),
            self.loadTemplate("accordion", bizagi.getTemplate("bizagi.renderingflat.tablet.accordion")),
            self.loadTemplate("accordionItem", bizagi.getTemplate("bizagi.renderingflat.tablet.accordionItem")),
            self.loadTemplate("group", bizagi.getTemplate("bizagi.renderingflat.tablet.group")),
            self.loadTemplate("searchForm", bizagi.getTemplate("bizagi.renderingflat.tablet.searchForm")),
            self.loadTemplate("contentPanel", bizagi.getTemplate("bizagi.renderingflat.tablet.contentPanel")),

        // Renders
            self.loadTemplate("render", bizagi.getTemplate("bizagi.renderingflat.tablet.render")),
            self.loadTemplate("text", bizagi.getTemplate("bizagi.renderingflat.tablet.text")),
            self.loadTemplate("extendedText", bizagi.getTemplate("bizagi.renderingflat.tablet.extendedText")),
            self.loadTemplate("number", bizagi.getTemplate("bizagi.renderingflat.tablet.number")),
            //self.loadTemplate("numberScientificNotation", bizagi.getTemplate("bizagi.rendering.tablet.numberScientificNotation")),
            self.loadTemplate("date", bizagi.getTemplate("bizagi.renderingflat.tablet.date") + "#bz-rn-date"),
            self.loadTemplate("yesno", bizagi.getTemplate("bizagi.renderingflat.tablet.yesno")),
            self.loadTemplate("check", bizagi.getTemplate("bizagi.renderingflat.tablet.check")),
            self.loadTemplate("combo", bizagi.getTemplate("bizagi.renderingflat.tablet.combo") + "#ui-bz-mobile-combo-control"),
            self.loadTemplate("comboModalView", bizagi.getTemplate("bizagi.renderingflat.tablet.combo") + "#ui-bz-wp-popup-combo-control"),
            self.loadTemplate("list", bizagi.getTemplate("bizagi.renderingflat.tablet.list") + "#ui-bz-mobile-list-control"),
            self.loadTemplate("listModalView", bizagi.getTemplate("bizagi.renderingflat.tablet.list") + "#ui-bz-wp-popup-list-control"),
            self.loadTemplate("radio", bizagi.getTemplate("bizagi.renderingflat.tablet.radio")),
            self.loadTemplate("image", bizagi.getTemplate("bizagi.renderingflat.tablet.image") + "#ui-bizagi-render-image"),
            self.loadTemplate("image-item", bizagi.getTemplate("bizagi.renderingflat.tablet.image") + "#ui-bizagi-render-image-item"),
            self.loadTemplate("upload", bizagi.getTemplate("bizagi.renderingflat.tablet.upload")),
            self.loadTemplate("uploadItem", bizagi.getTemplate("bizagi.renderingflat.tablet.uploadItem")),
            self.loadTemplate("uploadSlide", bizagi.getTemplate("bizagi.renderingflat.tablet.slide.upload")),
            self.loadTemplate("grid", bizagi.getTemplate("bizagi.renderingflat.tablet.grid")),
            self.loadTemplate("cell", bizagi.getTemplate("bizagi.renderingflat.tablet.cell")),
            self.loadTemplate("cell.readonly", bizagi.getTemplate("bizagi.renderingflat.tablet.cell.readonly")),
            self.loadTemplate("cell.upload", bizagi.getTemplate("bizagi.renderingflat.tablet.cell.upload")),
            self.loadTemplate("search", bizagi.getTemplate("bizagi.renderingflat.tablet.search")),
            self.loadTemplate("searchItem", bizagi.getTemplate("bizagi.renderingflat.tablet.searchItem")),
            self.loadTemplate('searchList', bizagi.getTemplate("bizagi.renderingflat.tablet.searchList")),
            self.loadTemplate("letter", bizagi.getTemplate("bizagi.renderingflat.tablet.letter")),
            self.loadTemplate("letter.readonly", bizagi.getTemplate("bizagi.renderingflat.tablet.letter.readonly")),
            self.loadTemplate("letter.dialog", bizagi.getTemplate("bizagi.renderingflat.tablet.dialog-letter")),
            self.loadTemplate("button", bizagi.getTemplate("bizagi.renderingflat.tablet.button")),
            self.loadTemplate("link", bizagi.getTemplate("bizagi.renderingflat.tablet.link")),
            self.loadTemplate("geolocation", bizagi.getTemplate("bizagi.renderingflat.tablet.geolocation")),
            self.loadTemplate('association', bizagi.getTemplate("bizagi.renderingflat.tablet.association")),
            self.loadTemplate('ecm', bizagi.getTemplate("bizagi.renderingflat.tablet.ecm") + "#bz-rn-ecm-view-default-item"),
            self.loadTemplate("ecm-metadata", bizagi.getTemplate("bizagi.renderingflat.tablet.ecm") + "#bz-rn-ecm-metadata"),

            self.loadTemplate("document", bizagi.getTemplate("bizagi.renderingflat.tablet.document") + "#bz-document-generator"),
            self.loadTemplate("document-item", bizagi.getTemplate("bizagi.renderingflat.tablet.document") + "#bz-document-generator-item"),
            self.loadTemplate("document-item-preview", bizagi.getTemplate("bizagi.renderingflat.tablet.document") + "#bz-document-generator-item-preview"),

        // Slide View 
            self.loadTemplate("render-slide-view", bizagi.getTemplate("bizagi.renderingflat.tablet.slide.view")),

        // Grid plugin stuff
            self.loadTemplate("bizagi.grid.grid", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid"),
            self.loadTemplate("bizagi.grid.waiting", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-waiting"),
            self.loadTemplate("bizagi.grid.table", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-table"),
            self.loadTemplate("bizagi.grid.table.empty", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-table-empty"),
            self.loadTemplate("bizagi.grid.column", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-column"),
            self.loadTemplate("bizagi.grid.column.special", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-column-special"),
            self.loadTemplate("bizagi.grid.row", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-row"),
            self.loadTemplate("bizagi.grid.row.buttons", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-row-buttons"),
            self.loadTemplate("bizagi.grid.cell", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-cell"),
            self.loadTemplate("bizagi.grid.cell.special", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-cell-special"),
            self.loadTemplate("bizagi.grid.pager", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-pager"),
            self.loadTemplate("bizagi.grid.buttons", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-buttons"),
            self.loadTemplate("bizagi.grid.dynamicpager", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-dynamic-pager"),
            self.loadTemplate("bizagi.grid.totalizer", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-totalizer"),
        // Grid Offline
            self.loadTemplate("bizagi.grid.buttons.offline", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-buttons-offline"),
            self.loadTemplate("bizagi.grid.row.buttons.offline", bizagi.getTemplate("bizagi.renderingflat.tablet.grid.component") + "#ui-bizagi-grid-row-buttons-offline")

        ).done(function () {

            // Resolve when all templates are loaded
            defer.resolve();
        });

        return defer.promise();
    },

    getRender: function(params) {
        var self = this;
        var type = params.type;
        var data = params.data;
        var renderParams = $.extend(params, {
            renderFactory: this,
            dataService: params.dataService || this.dataService
        });

        if (type == "grid") {
            var properties = data.properties;
            // Review the offline status or context 
            if (bizagi.util.isTabletDevice()) {
                if (typeof(bizagi.context.isOfflineForm) != "undefined" && bizagi.context.isOfflineForm) {
                    return new bizagi.rendering.grid.offline(renderParams);
                } else {
                    return new bizagi.rendering.grid(renderParams);
                }
            } else {
                return new bizagi.rendering.grid(renderParams);
            }
        }

        if (type == "upload") {
            var properties = data.properties;
            // Review the offline status or context  
            if (bizagi.util.isTabletDevice()) {
                if (typeof(bizagi.context.isOfflineForm) != "undefined" && bizagi.context.isOfflineForm) {
                    return new bizagi.rendering.upload.offline(renderParams);
                } else {
                    return new bizagi.rendering.upload(renderParams);
                }
            } else {
                return new bizagi.rendering.upload(renderParams);
            }
        }

        if (type == "number") {
            var properties = data.properties;
            if (properties && properties.dataType === 29) {
                return new bizagi.rendering.numberScientificNotation(renderParams);
            }

        }

        // Call base
        return self._super(params);
    },

    /*
    *   Returns the appropiate column based on the render type
    */
    getColumn: function (params) {
        var self = this;
        var type = params.type;
        var columnParams = $.extend(params, {
            renderFactory: this,
            dataService: params.dataService || this.dataService,
            singleInstance: bizagi.util.isEmpty(params.singleInstance) ? true : params.singleInstance
        });

        if (type === "columnNumber") {
            var data = params.data;
            var properties = data.properties;
            if (properties && properties.dataType === 29) {
                columnParams.decorated = bizagi.rendering.numberScientificNotation;
                return new bizagi.rendering.columns.numberScientificNotation(columnParams);
            }
        }

        // Call base
        return self._super(params);
    }

});
