/*
*   Name: BizAgi Tablet slide view implementation
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   Serves as an slide form that can render a inner form
*/

// Extends itself
$.Class.extend('bizagi.rendering.tablet.slide.view', {}, {
    /* CONSTRUCTOR
    =====================================================*/
    init: function (dataService, renderFactory, slideFormParams) {

        var self = this;

        // Define instance variables
        self.dataService = dataService;
        self.renderFactory = renderFactory;
        self.slideFormParams = slideFormParams || {};


        if (!self.slideFormDeferred) {
            self.slideFormDeferred = new $.Deferred();
        }

        // Create container    	
        if (!self.view) {
            self.slideForm = $($.fasttmpl(self.renderFactory.getTemplate('render-slide-view'), self.slideFormParams));
            $('#paneRender').append(self.slideForm);
            self.view = new kendo.mobile.ui.View(self.slideForm, { 'useNativeScrolling': true });
        }
        
        self.pane = $("#paneRender").data("kendoMobilePane");

        self.processButtons()
            .done(function (data) {
                self.slideFormDeferred.resolve(data);
            }).fail(function () {
                self.slideFormDeferred.reject();
            });

        self.configureViewHandlers();
    },

    processButtons: function () {
        var self = this;
        var dfd = new $.Deferred();

        // Create buttons object
        var objFormButtons = { buttons: [] };

        // If form is editable, add "save" button
        if (self.slideFormParams.showSaveButton) {
            objFormButtons.buttons.push({
                text: bizagi.localization.getResource("render-form-dialog-box-save"),
                click: function () {
                    if (self.form) {
                        // Perform validations
                        if (self.form.validateForm()) {

                            // Collect data
                            var data = {};
                            self.form.collectRenderValues(data);

                            if (self.slideFormParams.allowGetOriginalFormValues) {
                                data.formValues = self.form.children;
                            }

                            // Add page cache for this form
                            data.idPageCache = self.form.getPageCache();

                            // Call user callback
                            if (self.slideFormParams.onSave) {
                                $.when(self.slideFormParams.onSave(data))
                                    .done(function (result) {
                                        if (result == null || result == true || result.type == "success") {
                                            // Close slide      
                                            self.goBack();

                                            // Resolve deferred
                                            dfd.resolve();

                                        } else if (result.type == "validationMessages") {
                                            // Add validation messages 
                                            self.form.addValidationMessage(result.messages);
                                        } else if (result.type == "error") {
                                            // Add error messages 
                                            self.form.addErrorMessage(result.message);
                                        }
                                    }).fail(function (dataFail) {

                                        var message = (dataFail.responseText) ? dataFail.responseText : ((typeof dataFail == "string") ? dataFail : dataFail.toString());

                                        // Convert String to object
                                        if (typeof message == "string") {
                                            try {
                                                message = JSON.parse(message).message;
                                            } catch (e) {
                                                message = message.match(/"message":(.*)",/)[0];
                                                message = message.substr(11, message.length - 13);
                                            }

                                        } else if (!message.message) {
                                            message = dataFail;
                                        }

                                        self.form.validateForm();
                                        self.form.clearValidationMessages();
                                        self.form.addValidationMessage(message);
                                    });

                            } else {
                                // Close slide
                                self.goBack();

                                //reject defered
                                dfd.reject();
                            }
                        }
                    }
                }
            });
        }

        // Add cancel button by default
        objFormButtons.buttons.push({
            text: bizagi.localization.getResource("render-form-dialog-box-cancel"),
            click: function () {
                // Close slide
                self.goBack();

                //reject defered
                dfd.reject();
            }
        });

        // Render Currents buttons
        var slideOptions = $.extend(objFormButtons, this.slideFormParams);
        self._renderButtons(slideOptions);

        // Return promise
        return dfd.promise();
    },

    /*
    *   Render the form
    *   The params are the same that will be send to the ajax service
    *   Returns a deferred
    */
    render: function (params) {
        var self = this;

        // Fill content
        self.renderSlideForm(self.slideForm, params);

        // Return promise
        return self.slideFormDeferred.promise();
    },

    /* RENDERS slideForm
    =====================================================*/
    renderSlideForm: function (slideForm, params) {

        var self = this;
        var defer = new $.Deferred();

        // Load template and data
        $.when(self.dataService.getFormData(params))
            .pipe(function (data) {
                /*** SUCCESS FILTER **/

                // Apply editable param
                if (params.editable == false) data.form.properties.editable = false;

                // Render dialog template
                self.form = self.renderFactory.getContainer({
                    type: "form",
                    data: data.form
                });

                // Return rendering promise
                return self.form.render();

            }, function (message) {
                /*** FAIL FILTER **/
                var errorTemplate = self.renderFactory.getCommonTemplate('form-error');
                $.tmpl(errorTemplate, { message: message }).appendTo(slideForm.find('.scroll-content'));
            }
            ).done(function (element) {

                // Append form in the slide view
                self.slideContent = $("#render-content", self.view.contentElement());
                self.slideContent.html(element);

                // Navigate on view
                self.id = self.pane.view().id;
                self.pane.navigate(self.view.id);

                // Remove default form buttons            
                $('.ui-bizagi-button-container .action-button', self.form.container).remove();
                // Add Button
                $('.ui-bizagi-button-container', self.form.container).append(self.buttonContainer);

                // Publish an event to check if the form has been set in the DOM
                self.form.triggerHandler('ondomincluded');

                // Attach a refresh handler to recreate all the form
                self.form.bind('refresh', function () {
                    self.init(self.dataService, self.renderFactory, self.slideFormParams);
                });
            });

        return defer.promise();
    },

    // Go back and destroy
    goBack: function () {
        var self = this;

        self.pane.navigate("#:back");
        self.view.destroy();
        self.view.element.remove();
    },

    /**
    * Handlers de la vista de kendo
    */
    configureViewHandlers: function () {
        var self = this;
        $(".bz-render-back", self.slideForm).bind("click", function () {
            self.goBack();
        });
    },

    /**
    * Process render buttons
    */
    _renderButtons: function (options) {
        var self = this;
        var content = $("<div class='bz-slide-button-container'></div>");

        $.each(options.buttons, function (ui, value) {
            var button = $("<div class='action-button'>" + value.text + "</div>").click(
                value.click
            ).appendTo(content);
        });

        self.buttonContainer = content;
    }
});