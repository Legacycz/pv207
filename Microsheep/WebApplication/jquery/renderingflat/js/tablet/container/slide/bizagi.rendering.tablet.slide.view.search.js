﻿/*
*   Name: BizAgi Tablet Search Slide view Implementation
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will shows a sliding search form  order to edit
*/

// Extends itself
$.Class.extend("bizagi.rendering.tablet.slide.view.search", {}, {

    /* 
    *   Constructor
    */
    init: function (dataService, renderFactory, searchForms, searchParams, slideParams) {
        var self = this;

        self.dataService = dataService;
        self.renderFactory = renderFactory;
        self.searchForms = searchForms;
        self.searchParams = searchParams;
        self.slideParams = slideParams;
        self.slideDeferred = new $.Deferred();
        self.extraButtons = (slideParams.hasOwnProperty("buttons")) ? slideParams.buttons : false;

        // Create container    	
        self.slideForm = $($.fasttmpl(self.renderFactory.getTemplate('render-slide-view'), self.slideParams));
        $('#paneRender').append(self.slideForm);

        self.view = new kendo.mobile.ui.View(self.slideForm, { 'useNativeScrolling': true });
        self.pane = $("#paneRender").data("kendoMobilePane");

        self.processButtons()
            .done(function (data) {
                self.slideDeferred.resolve(data);
            }).fail(function () {
                self.slideDeferred.reject();
            });

        self.configureViewHandlers();
    },

    /*
    *   Render the form
    *   The params are the same that will be send to the ajax service
    *   Returns a deferred
    */
    render: function (params) {
        var self = this;

        // Render search dialog
        self.renderSearchDialog(params);

        // Return promise
        return self.slideDeferred.promise();
    },

    /*
    * Render the search form
    */
    renderSearchDialog: function (params) {
        var self = this;

        // Creates a dialog
        $.when(self.getSearchContainerData(params))
        .pipe(function (data) {

            // Render dialog form
            form = self.renderFactory.getContainer({
                type: "form",
                data: data.form
            });

            // Save form reference
            self.form = form;

            // Return rendering promise
            return form.render();
        }).done(function (element) {


            // Append form in the slide view
            self.slideContent = $("#render-content", self.view.contentElement());
            self.slideContent.html(element);

            // Add Button
            $('.ui-bizagi-button-container', self.form.container).append(self.buttonContainer);

            // Navigate on view
            self.id = self.pane.view().id;
            self.pane.navigate(self.view.id);

            // Hide title of html content
            var searchResultContainer = $(".ui-bizagi-container-search-results", element);
            searchResultContainer.hide();

            // Publish an event to check if the form has been set in the DOM
            form.triggerHandler("ondomincluded");

            // Set currents search form
            self.currentSearchForm = self.form.firstChild();
            self.currentSearchForm.triggerHandler("ondomincluded");

            self.currentSearchForm.bind("instancerefresh", function () {
                // Show search results container
                searchResultContainer.show();
            });

            // Add row selected handlers to resolve the deferred
            self.currentSearchForm.bind("instanceSelected", function (ev, ui) {
                // Close slide      
                self.goBack();

                self.slideBoxDeferred.resolve({
                    id: ui.id,
                    label: ''
                });
            });
        });
    },

    /*
    *   Shows the dialog box in the browser
    *   Returns a promise that the dialog will be closed
    */
    processButtons: function () {
        var self = this;
        self.slideBoxDeferred = new $.Deferred();

        // Create buttons object
        var slideOptions = { buttons: [] };

        // Add search button by default
        slideOptions.buttons.push({
            text: bizagi.localization.getResource("render-form-dialog-box-search"),
            click: function () {
                self.currentSearchForm.performSearch(self.searchParams);
            }
        });

        slideOptions.buttons.push({
            text: bizagi.localization.getResource("render-form-dialog-box-cancel"),
            click: function () {
                // Close slide      
                self.goBack();
            }
        });

        // Apply slide plugin
        slideOptions = $.extend(slideOptions, self.slideParams);
        self._renderButtons(slideOptions);

        // Return promise
        return self.slideBoxDeferred.promise();
    },

    /*
    *   Creates the search dialog sub-containers
    */
    getSearchContainerData: function (params) {
        var self = this;
        var deferred = new $.Deferred();
        var data = {
            form: { properties: {}, elements: [] }
        };

        var container = data.form;
        var searchFormToLoad = self.searchForms[0];

        // When there are more than one search form, render a tab container
        if (self.searchForms.length > 1) {

            var tabContainer = {};
            tabContainer.properties = { type: "tab" };
            tabContainer.elements = [];
            var otherElements = [];

            $.each(self.searchForms, function (i, searchForm) {
                var isDefault = searchForm["default"] || false;

                // Create tab item
                var tab = {};
                tab.properties = {
                    id: i,
                    type: 'tabItem',
                    displayName: searchForm.caption
                };

                tab.elements = [];

                // Append children 
                if (isDefault) {

                    tabContainer.elements.push({ container: tab });

                    // Set properties to load default container
                    container = tab;
                    searchFormToLoad = self.searchForms[0];

                } else {
                    otherElements.push({ container: tab });
                }
            });

            // Append no defaults elements
            $.each(otherElements, function (i, element) {
                tabContainer.elements.push(element);
            });

            data.form.elements.push({
                container: tabContainer
            });
        }

        // Loads default form
        $.when(self.getSearchFormData(params, searchFormToLoad))
            .done(function (searchFormData) {
                container.elements.push(searchFormData);

                // Resolve deferred
                deferred.resolve(data);
            });

        return deferred.promise();
    },

    /*
    *   Loads a search form
    */
    getSearchFormData: function (params, searchForm) {
        var self = this;
        return self.dataService.getSearchFormData($.extend(params, {
            idSearchForm: searchForm.id,
            url: searchForm.url

        })).pipe(function (data) {
            // Append search render properties
            data.form.properties.idRender = params.idRender;

            return data;
        });
    },

    // Go back and destroy
    goBack: function () {
        var self = this;

        self.pane.navigate("#:back");
        self.view.destroy();
        self.view.element.remove();
    },

    /**
    * Handlers de la vista de kendo
    */
    configureViewHandlers: function () {
        var self = this;
        $(".bz-render-back", self.slideForm).bind("click", function () {
            self.goBack();
        });
    },

    /**
    * Process render buttons
    */
    _renderButtons: function (options) {
        var self = this;
        var content = $("<div class='bz-slide-button-container'></div>");

        $.each(options.buttons, function (ui, value) {
            var button = $("<div class='action-button'>" + value.text + "</div>").click(
                value.click
            ).appendTo(content);
        });

        self.buttonContainer = content;
    }
});
