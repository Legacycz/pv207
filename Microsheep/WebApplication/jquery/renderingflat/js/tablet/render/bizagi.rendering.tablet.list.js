﻿/*  
*   Name: BizAgi Tablet Render list Extension
*   Author: Cristian O
*   Date: 2015 - Oct - 10
*   Comments:
*   -   This script will redefine the list render class to adjust to tablet devices
*/

// Extends from base list 
bizagi.rendering.list.extend("bizagi.rendering.list", {}, {
    postRender: function () {
        // Call combo postRender
        var self = this;
        var control = self.getControl();

        // Call base 
        this._super();

        // Set control container to behave as a block
        control.addClass("ui-bizagi-render-display-block");
        control.addClass("ui-bizagi-render-boolean-yesno");

        // Apply plugin
        self.list = $(".ui-bizagi-render-list", control);
        self.list.selectable({
            filter: "li.bz-rn-regular-element",
            select: function (e) {
                if (e.isPropagationStopped()) {
                    return;
                }
                
                var selected = $("li[tabindex='0']", this);
                var _value = $(selected).data("value");
                var label = $(selected).text();

                // Updates internal value
                var newValue = { 'id': _value, 'value': label };
                self.setValue(newValue);
                self.selectedValue = newValue.value;
                self.focusSelectedItem(selected);
                e.stopPropagation();
            }
        });

        if(!self.properties.editable){
            //Add styles class
            container.addClass("bz-command-not-edit");
            self.list.attr('readonly', "readonly");

            var value = self.properties.value ? self.properties.value.value : "";

            self.control.html("<span class=\"bz-command-not-edit bz-rn-text\">" + value + "</span>");
        }

        self.configureListHandlers();
    },

    /**
     * Setting the control events
     * */
    configureListHandlers: function(){
        var self = this;

        self.list.find(".see-more-icon").unbind().bind("click", function(){
            //Creating modal view
            var modalViewTemplate = kendo.template(self.renderFactory.getTemplate('listModalView'), { useWithBlock: false });
            var modalView = $(bizagi.util.trim(modalViewTemplate({'items': self.properties.data, 'displayName': self.properties.displayName || ""}))).clone();

            modalView.kendoMobileModalView({
                close: function () {
                    this.destroy();
                    this.element.remove();
                },
                useNativeScrolling: true,
                modal: false
            });

            self.configureModalViewHandlers();
            modalView.kendoMobileModalView('open');
            modalView.closest(".k-animation-container").addClass("bz-rn-new-modalview-position");
        });
    },

    /**
     * Configure the modalView Handlers for the new combo control.
     * */
    configureModalViewHandlers: function(){
        var self = this;
        //getting modalView
        var inputContainer = $("#modalview-filter-list", "body");
        //getting combo list elements
        var container = inputContainer.find(".ui-bizagi-render-list-elements li");

        //Hide the clear text icon
        inputContainer.find(".bz-wp-list-cancel-icon-list").hide(500);

        //Closing modalview
        inputContainer.delegate("#ui-bizagi-cancel-button", "click", function () {
            inputContainer.data("kendoMobileModalView").close();
        });

        //Adding styles to modalView
        $("#ui-bizagi-cancel-button", "body").closest("#filter-footer").addClass("bz-rn-new-modalview-footer-styles");
        $(".bz-wp-search-on-list-back-icon-list", "body").closest("#filter-header").addClass("bz-rn-new-modalview-header-styles");
        $("#ui-bizagi-wp-filter-results-list-wrapper", "body").closest(".km-native-scroller").addClass("bz-rn-new-modalview-content-styles");

        //Filtering list
        inputContainer.find(".input-filter-results-on-list").bind('change keypress  keyup change', function () {
            self.filterList(this.value, container, inputContainer);
        });

        //Cleaning list
        inputContainer.find(".bz-wp-list-cancel-icon-list").bind('click', function(){
            self.filterList("", container, inputContainer);
            inputContainer.find(".input-filter-results-on-list").val("");
            $(this).hide(500);
        });

        //Configuring list as a selectable element
        self.modalViewList = $(".ui-bizagi-render-list-elements", inputContainer);
        self.modalViewList.selectable();
        self.setModalViewDisplayValue();

        //Setting the initial selected display value
        self.modalViewList.find("li").bind("click", function(){
            var selectedElements = self.modalViewList.find(".ui-state-active");
            self._newSelectedElement = this;

            selectedElements.each(function(index, element){
                if(self._newSelectedElement.id !== element.id){
                    $(element).removeClass("ui-state-focus ui-selected ui-state-active");
                }
            });

            $(".input-filter-results-on-list", inputContainer).blur();
        });

        inputContainer.delegate("#ui-bizagi-apply-button", "click", function () {
            var selected = $("li.ui-selected", self.modalViewList);
            var _value = $(selected).data("value");
            var label = $(selected).text();

            //Update internal value
            if(typeof _value !== "undefined"){
                var newValue = { 'id': _value, 'value': label };
                self.setValue(newValue);
                self.selectedValue = newValue.value;
                self.setDisplayValue(newValue);
            }

            self.focusSelectedItem(selected);

            inputContainer.data("kendoMobileModalView").close();
        });

        inputContainer.find(".bz-wp-search-on-list-back-icon-list").bind("click", function(){
            inputContainer.data("kendoMobileModalView").close();
        });
    },

    /**
     * Filtering list of elements
     * */
    filterList: function(search, $li, inputContainer){
        if(search !== ""){
            inputContainer.find(".bz-wp-list-cancel-icon-list").show(500);
        }else{
            inputContainer.find(".bz-wp-list-cancel-icon-list").hide(500);
        }

        $li.filter(function() {
            var stringContainer = $(this).text().toUpperCase();
            var stringToSearch = search.toUpperCase();

            if(stringContainer.indexOf(stringToSearch) >= 0){
                return $(this);
            }else{
                $(this).hide();
            }
        }).show();
    },

    /* SETS THE VALUE IN THE RENDERED CONTROL
    =====================================================*/    
    /*
    *   Sets the value in the rendered control
    */
    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;

        if (properties.editable) {
            if (value !== undefined) {
                //Set the selected value
                self.list.selectable("select", "[id='ui-bizagi-list-" + value.id + "']");

                //take the first child
                var firstChild = self.list.find("li:first-child");

                //Clone the first node and add the copy on final
                firstChild.clone().addClass("hidden").appendTo(self.list.find("li:first-child").parent("ul"));

                //If the selected value is the empty value
                if(value.id === "" && $("li.ui-selected", self.list).length === 0 ){
                    firstChild.replaceWith(self.list.find("li#ui-bizagi-list-"));
                    self.list.find("li#ui-bizagi-list-").removeClass("hidden");
                }else{
                    //Show the new selected element
                    $("li.ui-selected", self.list).removeClass("hidden");

                    //Put the new selected value at start
                    self.list.find("li.ui-selected").insertBefore(firstChild);

                    //If there are less of 4 elements visibles dont hidden the first element, just show it, else remove it
                    // because i already have clone the element
                    if(self.properties.data.length > 10 && (self.properties.data.length - $("li.bz-rn-regular-element.hidden", self.list).length + 1) > 10 ){
                        //remove the old first child
                        firstChild.remove();
                    }else{
                        //dont hide the first child element
                        firstChild.removeClass("hidden");
                        self.list.find("li:last-child").remove();
                    }
                }

                //Show the see more button at final.
                var seeMoreIcon = self.list.find("li.see-more-icon");

                if(seeMoreIcon.length === 1){
                    self.list.find("li.see-more-icon").appendTo(self.list.find("li:first-child").parent("ul"));
                }
            }
        }
    },

    /**
     * Setting the selected element.
     * */
    setModalViewDisplayValue: function () {
        var self = this;
        var properties = self.properties;

        if(self.list && properties.editable){
            var selected = $("li[tabindex='0']", self.list);

            if(!($(selected).data("value") === "see-more-icon")){
                var value = $(selected).data("value");

                if (value && value !== undefined && value !== null && value.id != "") {
                    self.modalViewList.find("#ui-bizagi-list-" + value).addClass("ui-state-focus ui-selected ui-state-active");
                }
            }
        }
    },

    /**
     * Put focus on new selected element
     * */
    focusSelectedItem: function (elList) {
        var self = this;
        $.when(self.ready())
            .done(function () {
                elList.focus();
            });
    },

    /**
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this;

        // Call base
        self._super();
        // Nothing todo, all events apply in selectable plugin
    },

    /**
    *   Template method to implement in each device to customize the render's behaviour when rendering in design mode
    */
    configureDesignView: function () {
        var self = this;
        var properties = self.properties;

        // Call base
        self._super();

        // Un-bind selection handlers
        if (properties.editable) {
            self.list.unbind("mousedown.selectable");
            self.list.unbind("focus.selectable");
            self.list.unbind("blur.selectable");
            self.list.unbind("keydown.selectable");
        }
    },    

    /**
    *   Sets the value in the rendered control
    */
    clearDisplayValue: function () {
        var self = this;
        self.list.selectable("select", "");
    },

    /**
    *   Returns the selected value in the template
    */
    getSelectedValue: function () {
        var self = this;
        return self.selectedValue;
    }
});
