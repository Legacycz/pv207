﻿/*
*   Name: BizAgi Tablet Render upload Extension
*   Author: oscaro
*   Comments:
*   -   This script will redefine the label render class to adjust to smartphones devices
*/

// Extends itself
bizagi.rendering.upload.extend("bizagi.rendering.upload", {
    BA_ACTION_PARAMETER_PREFIX: bizagi.render.services.service.BA_ACTION_PARAMETER_PREFIX,
    BA_CONTEXT_PARAMETER_PREFIX: bizagi.render.services.service.BA_CONTEXT_PARAMETER_PREFIX,
    QUALITY_PICTURE: 80,
    LIMIT: 1, //limit: The maximum number of audio clips,video clips,etc in the device user can record in a single capture operation.
    EXTENSIONSIMG: ["image/jpeg", "jpeg", "image", "png", "jpg"],
    EXTENSIONSVIDEO: ["video/quicktime", "quicktime", "qt", "mov"],
    EXTENSIONSAUDIO: ["audio/wav", "audio", "wav"]
}, {
    postRender: function () {
        var self = this;

        // Call base 
        self._super();

        if (typeof (cordova) === "undefined")
            self.addEventToOpenSlide();
        else {
            self.activateUploadNative();
        }

        self.deleteIconHandler();
    },

    postRenderReadOnly: function () {
        var self = this;

        self._super();

        if (typeof (cordova) === "undefined")
            self.addEventToOpenSlide();
    },

    addEventToOpenSlide: function () {
        var self = this;
        var mode = self.getMode();
        var control = self.getControl();
        var container = self.getControl();

        self.itemAddfile = $(".bz-rn-upload-show-menu", container);
        self.itemAddfile.hide();

        $("li", control).click(function () {
            var url = $(this).data('url');

            if (mode == "execution") {
                var slideView = new bizagi.rendering.tablet.slide.view.upload(self.dataService, self.renderFactory, {
                    title: self.properties.displayName || "",
                    container: self.getFormContainer().container,
                    url: url
                });

                slideView.render({ url: url });
            }
        });
    },

    activateUploadNative: function () {
        var self = this;
        var properties = self.properties;
        var container = self.getControl();
        var body = $("body");

        self.renderControl();
        self.itemAddfile = $(".bz-rn-upload-show-menu", container);
        

        if (typeof (cordova) === "undefined" || properties.editable == false) {
            self.itemAddfile.hide();
            return;
        }

        self.itemsUpload = {
            "file": $(".file", container),
            "image": $(".image", container),
            "cimage": $(".cimage", container),
            "caudio": $(".caudio", container),
            "cvideo": $(".cvideo", container)
        };
        
        
        self.itemAddfile.bind("click", function (e) {
            var containerUploadItems = $(".bz_rn_upload_container_upload_items", container);
            // mejorar la implementacion cuando abro varios casos especiales al tiempo
            //bug IOS : z-inded for first item
            if (containerUploadItems.hasClass("bz_clone_active")) {
                containerUploadItems.removeClass("bz_clone_active");

                var divClone = $("#bz_active_clone_Upload", body);

                divClone.hide();
                divClone.remove();

                containerUploadItems.hide();
                self.removeListener();

                return;
            }

            if (containerUploadItems.is(':visible')) {
                containerUploadItems.hide();
                self.removeListener();

                return;
            }

            //this try/catch is use for close other upload controls in the form
            try {
                var formContainer = self.getContainerRender().parent();
                if (formContainer) {
                    formContainer.find(".bz_rn_upload_container_upload_items:visible").hide();
                }
            } catch (e) {
            }

            containerUploadItems.show();

            //verify the visivility for container and puts the class for arrow
            var heigthContainer = parseInt(containerUploadItems.css("height"));
            if (((self.itemAddfile.offset().top - 60) - heigthContainer) > 0) {

                containerUploadItems.addClass("bottomArrow");

                containerUploadItems.position({
                    of: self.itemAddfile,
                    my: "center bottom",
                    at: "center top",
                    offset: "-10px",
                    collision: "flipfit flipfit"
                });
            } else {
                //bug IOS : z-inded for first item
                containerUploadItems.hide();
                containerUploadItems.addClass("bz_clone_active");
                var cloneUpload = containerUploadItems.clone();
                cloneUpload.attr("id", "bz_active_clone_Upload");
                cloneUpload.addClass("upArrow");
                cloneUpload.appendTo(body);
                cloneUpload.css('display', 'inline-block');

                cloneUpload.position({
                    of: self.itemAddfile,
                    my: "center top",
                    at: "center bottom",
                    offset: "5px",
                    collision: "flipfit flipfit"
                });
            }

            self.itemsUpload = {
                "file": $(".file", cloneUpload),
                "image": $(".image", cloneUpload),
                "cimage": $(".cimage", cloneUpload),
                "caudio": $(".caudio", cloneUpload),
                "cvideo": $(".cvideo", cloneUpload)
            };
            self.addListener();
        });

        self.checkExtensions();
        self.checkMaxFiles();
    },
    
    deleteIconHandler: function () {
        var self = this;

        $(".bz-rn-upload-delete-icon", self.getControl()).bind("click", function () {
            var item = $(this).parent(".ui-bizagi-render-upload-item");
            var file = item.data();
            $.when(self.deleteUploadItem(item, file.id)).done(function () {
                // Remove item
                item.hide();

                // Detach item
                item.remove();

                // Check maxFiles
                self.checkMaxFiles();

                // Trigger change
                self.triggerRenderChange();
            });
        });
    },
    
    removeListener: function () {
        var self = this;

        self.itemsUpload.image.unbind("click");
        self.itemsUpload.file.unbind("click");
        self.itemsUpload.cimage.unbind("click");
        self.itemsUpload.caudio.unbind("click");
        self.itemsUpload.cvideo.unbind("click");
    },

    addListener: function () {
        var self = this;

        var buttons = [
            {
                'guid': 1,
                'displayName': bizagi.localization.getResource("workportal-size-small")
            }, {
                'guid': 2,
                'displayName': bizagi.localization.getResource("workportal-size-medium")
            }, {
                'guid': 3,
                'displayName': bizagi.localization.getResource("workportal-size-large")
            }, {
                'guid': 4,
                'displayName': bizagi.localization.getResource("workportal-size-original")
            }
        ];

        //Attach document
        $('.bz-resolution-container', self.getControl()).actionSheet({
            actions: buttons,
            actionClicked: function (action) {
                self.resolutionButtonHandler(action.guid);

            }
        });

        self.itemsUpload.image.bind("click", function () {

            navigator.camera.getPicture(function (dataImage) {

                if (device.platform === 'iOS' && window.plugin.statusbarOverlay) {
                    window.plugin.statusbarOverlay.hide();
                }

                $.when(self.checkMaxSize(dataImage))
                    .done(function (responseInternal) {
                        if (responseInternal) {
                            self.dataImage = dataImage;

                            // "new Blob" constructor is not supported in android version 4.2.2
                            if (bizagi.detectSO() == "android" && bizagi.util.detectAndroidVersion() <= 422) {
                                self.saveImage(self, self.dataImage);
                            } else {
                                //force click to show actionsheet menu
                                $('.bz-resolution-container', self.getControl()).click();
                            }
                        }

                    });

                self.itemAddfile.click();

            }, self.onFail, {
                quality: self.Class.QUALITY_PICTURE,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY
            });

        });

        //capture file
        self.itemsUpload.file.bind("click", function () { alert("in development"); });
        //capture image from camera
        self.itemsUpload.cimage.bind("click", function () {
            navigator.camera.getPicture(function (dataImage) {

                if (device.platform === 'iOS' && window.plugin.statusbarOverlay) {
                    window.plugin.statusbarOverlay.hide();
                }

                $.when(self.checkMaxSize(dataImage)).done(function (responseInternal) {
                    if (responseInternal)
                        self.saveImage(self, dataImage);
                });

                self.itemAddfile.click();
            },
                self.onFail,
                { quality: self.Class.QUALITY_PICTURE, targetWidth: 1280, targetHeight: 960 });

        });

        //capture audio
        self.itemsUpload.caudio.bind("click", function () {
            navigator.device.capture.captureAudio(function (dataImage) {
                self.saveAudio(self, dataImage);

                self.itemAddfile.click();
            }, self.onFail, { limit: self.Class.LIMIT });

        });

        //capture video 
        self.itemsUpload.cvideo.bind("click", function () {
            navigator.device.capture.captureVideo(function (dataImage) {
                self.saveVideo(self, dataImage);

                self.itemAddfile.click();
            }, self.onFail, { limit: self.Class.LIMIT });
        });

    },

    resolutionButtonHandler: function (buttonOrdinal) {
        var self = this;
        
        bizagi.util.tablet.startLoading();
        var objWidth = 0;
        var objHeight = 0;


        if (buttonOrdinal == 1) {
            objWidth = 320;
            objHeight = 240;
        } else if (buttonOrdinal == 2) {
            objWidth = 640;
            objHeight = 480;
        } else if (buttonOrdinal == 3) {
            objWidth = 1280;
            objHeight = 960;
        }

        //if original, save image normally, if not, reduce quality and size
        if (buttonOrdinal == 4) {
            self.saveImage(self, self.dataImage);
        }
        else {
            $.when(self.transformImageSize(self.dataImage, objWidth, objHeight)).done(function () {
                self.saveImage(self, self.dataImage);
            }).fail(function () {
                bizagi.util.tablet.stopLoading();
            });
        }

        $(".ui-bizagi-resolution-container").css("display", "none");
        $(".ui-bizagi-mask").remove();
    },

    renderUploadItem: function (file) {
        var self = this;
        var properties = self.properties;
        var html = self._super(file);

        return html;
    },

    getTemplateName: function () {
        return "upload";
    },

    getTemplateItemName: function () {
        return "uploadItem";
    },

    getTemplateEditionName: function () {
        return "edition.upload";
    },

    getTemplateEditionMenu: function () {
        return "edition.upload.menu";
    },

    saveImage: function (context, dataImage) {
        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileKey = "file";
        options.fileName = dataImage.substr(dataImage.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        options.params = data;

        //fix android 4.4 getting images from recent folder
        if (dataImage.substring(0, 21) == "content://com.android") {
            var photoSplit = dataImage.split("%3A");
            if (photoSplit.length > 1) {
                dataImage = "content://media/external/images/media/" + photoSplit[1];
            } else {
                photoSplit = dataImage.split("/");
                dataImage = "content://media/external/images/media/" + photoSplit[photoSplit.length - 1];
            }
        }

        // Add extension image file
        //                var extension = file.type.substr(file.type.lastIndexOf('/') + 1);
        options.fileName += self.fileExtension ? "." + self.fileExtension : "";

        if (self.editedImageURL) {
            dataImage = self.editedImageURL;
        }

        var ft = new FileTransfer();
        ft.upload(dataImage, properties.addUrl,
            function (r) {
                self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
            },
            function (error) {
                bizagi.log("An error has occurred: Code = " + error.code);
            }, options);
    },

    saveAudio: function (context, dataAudio) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileName = dataAudio[0].name;
        options.params = data;

        var ft = new FileTransfer();

        ft.upload(dataAudio[0].fullPath, properties.addUrl,
            function (r) {
                self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
            },
            function (error) {
                bizagi.log("An error has occurred: Code = " + error.code);
            }, options);
    },

    saveVideo: function (context, dataVideo) {

        var self = context;
        var properties = self.properties;
        var data = self.buildAddParams();

        var queueID = "q_" + bizagi.util.encodeXpath(self.getUploadXpath());
        data.queueID = queueID;

        var options = new FileUploadOptions();

        options.fileName = dataVideo[0].name;
        options.mimeType = "video/quicktime";
        options.params = data;

        var ft = new FileTransfer();
        ft.upload(dataVideo[0].fullPath, properties.addUrl,
            function (r) {
                self.onUploadFileCompleted(context, JSON.parse(decodeURIComponent(r.response)));
            },
            function (error) {
                bizagi.log("An error has occurred: Code = " + error.code);
            }, options);
    },

    onFail: function (error) {
        bizagi.log('Error code: ' + error.code);

        if (device.platform === 'iOS' && window.plugin.statusbarOverlay) {
            window.plugin.statusbarOverlay.hide();
        }
    },

    onUploadFileCompleted: function (context, response) {
        var self = context,
            control = self.getControl();

        var uploadWrapper = $(".bz-rn-upload-show-menu", control);

        var result = response;

        if (result.id && result.fileName) {
            var newItem = self.renderUploadItem(result);

            self.files.push([result.id, result.fileName]);
            // Locate it before the upload wrapper
            $(newItem).insertBefore(uploadWrapper);
            // Increment counter
            self.filesCount = self.filesCount + 1;

            self.changeRequired(false);
            self.triggerRenderChange();

            control.find(".ui-bizagi-render-upload-item-no-upload").hide();

            self.deleteIconHandler();
            
            // Check maxFiles
            self.checkMaxFiles();
            if (self.properties.type == "columnUpload") {
                //refresh to have synchronized the file between sever and client
                bizagi.util.smartphone.stopLoading();
                self.getFormContainer().saveForm();
                self.getFormContainer().refreshForm();
            } else {
                self.refreshControl();
            }

            self.editedImageURL = null;
            bizagi.util.tablet.stopLoading();
        } else {
            bizagi.log("E:" + result.message);
        }
    },
    saveBlobToFile: function (blob, defer) {
        // root file system entry
        var self = this;

        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
            function (fileSystem) {
                var root = fileSystem.root;
                // writes a file
                var writeFile = function (writer) {

                    writer.onwriteend = function (e) {
                        self.editedImageURL = writer.localURL;
                        defer.resolve();
                        console.log('Write completed.');
                    };

                    writer.onerror = function (e) {
                        console.log('Write failed: ' + e.toString());
                    };

                    // write to file
                    writer.write(blob);
                };

                // creates a FileWriter object
                var createWriter = function (fileEntry) {
                    fileEntry.createWriter(writeFile, null);
                };

                var newId = "bizagiImgTmp";

                // create a file and write to it
                root.getFile(newId + '.jpg', { create: true }, createWriter, null);
            },
            function () { });
    },
    /*
    *
    */
    transformImageSize: function (objectUri, objWidth, objHeight) {
        var self = this;
        var defer = new $.Deferred();

        window.resolveLocalFileSystemURL(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {

                var reader = new FileReader();

                // Create a function to process the file once it's read
                reader.onloadend = function (evt) {
                    // Create an image element that we will load the data into
                    var image = new Image();
                    image.onload = function (evt) {

                        var width = image.width;
                        var height = image.height;

                        if (objWidth != 0) {

                            if (width > height) {
                                if (width > objWidth) {
                                    height *= objWidth / width;
                                    width = objWidth;
                                }
                            } else {
                                if (height > objHeight) {
                                    width *= objHeight / height;
                                    height = objHeight;
                                }
                            }
                        }

                        var canvas = document.createElement("canvas");
                        canvas.width = width;
                        canvas.height = height;
                        canvas.getContext("2d").drawImage(this, 0, 0, width, height);

                        if (self.value && self.properties.value) {
                            self.value[0] = self.properties.value[0] = self.properties.url = canvas.toDataURL("image/jpeg", 0.5);
                        } else {
                            self.properties.url = canvas.toDataURL("image/jpeg", 0.5);
                        }

                        var imageToSaveLocal = self.properties.url.replace("data:image/jpeg;base64,", "");
                        var imageBlob = bizagi.util.b64toBlob(imageToSaveLocal);
                        self.saveBlobToFile(imageBlob, defer);

                        image = null;

                    };
                    // Load the read data into the image source. It's base64 data
                    image.src = evt.target.result;
                };
                // Read from disk the data as base64
                reader.readAsDataURL(fileObj);

            });
        });
        return defer.promise();
    },

    checkMaxFiles: function () {
        var self = this,
            properties = self.properties,
            maxFiles = properties.maxfiles,
            actualFiles = properties.value.length;

        if ( maxFiles != 0 && ( (self.filesCount >= maxFiles && actualFiles >= maxFiles)) ){
            self.itemAddfile.hide();
        }
        else{
            self.itemAddfile.show();
        }
    },

    checkExtensions: function () {
        var self = this;
        var properties = self.properties;
        var validExtensions = properties.validExtensions;

        if (typeof validExtensions === "undefined" || validExtensions == "") {
            return;
        }

        validExtensions = validExtensions.split(";");

        if (validExtensions.length == 1 && validExtensions[0].indexOf("*.*") !== -1) {
            return;
        }

        var enableVideo = false;
        var enableAudio = false;
        var enableImage = false;

        for (var i = 0; i < validExtensions.length; i++) {
            validExtensions[i] = validExtensions[i].replace("*.", "");

            var image = self.Class.EXTENSIONSIMG.toString().indexOf(validExtensions[i]);
            var audio = self.Class.EXTENSIONSAUDIO.toString().indexOf(validExtensions[i]);
            var video = self.Class.EXTENSIONSVIDEO.toString().indexOf(validExtensions[i]);

            if (image >= 0) {
                enableImage = true;
            }

            if (audio >= 0) {
                enableAudio = true;
            }

            if (video >= 0) {
                enableVideo = true;
            }
        }

        if (!enableAudio)
            self.itemsUpload.caudio.hide();
        if (!enableImage) {
            self.itemsUpload.image.hide();
            self.itemsUpload.cimage.hide();
        }
        if (!enableVideo)
            self.itemsUpload.cvideo.hide();

        if (!enableAudio && !enableImage && !enableVideo) {
            self.itemAddfile.hide();
            self.removeListener();
        }
    },

    checkMaxSize: function (objectUri) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        window.resolveLocalFileSystemURI(objectUri, function (fileEntry) {
            fileEntry.file(function (fileObj) {

                if (fileObj.size >= properties.maxSize) {
                    // The file is heavier than allowed: ' + properties.maxSize +"Bytes");
                    if (device.platform === 'iOS' && window.plugin.statusbarOverlay) {
                        window.plugin.statusbarOverlay.hide();
                    }

                    bizagi.showMessageBox(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize), "Error");
                    defer.resolve(false);
                } else {
                    self.fileExtension = fileObj.type ? fileObj.type.substr(fileObj.type.lastIndexOf('/') + 1) : "";
                    defer.resolve(true);
                }

            });
        });
        return defer.promise();
    },


    checkMaxSizeVideo: function (objectVideo) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        var size = objectVideo[0].size;

        if (properties.maxSize == "undefined" || properties.maxSize == null || properties.maxSize == "") {
            defer.resolve(true);
        }

        if (size >= properties.maxSize) {
            // The file is heavier than allowed: ' + properties.maxSize +"Bytes");
            if (device.platform === 'iOS' && window.plugin.statusbarOverlay) {
                window.plugin.statusbarOverlay.hide();
            }

            bizagi.showMessageBox(self.getResource("render-upload-alert-maxsize").replace("{0}", properties.maxSize), "Error");
            defer.resolve(false);
        } else {
            defer.resolve(true);
        }

        return defer.promise();
    }
});
