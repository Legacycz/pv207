﻿/*
*   Name: BizAgi Tablet Render Date Extension
*   Author: Cristian Olaya
*   Date: 2015-sep-29
*   Comments:
*   -   This script will redefine the Date render class to adjust to tablet devices
*/

// Extends itself
bizagi.rendering.date.extend("bizagi.rendering.date", {
    AVAILABLE_LANGUAGES: {
        "es": 'es', "es-ES": 'es', "es-AR": 'es', "es-BO": 'es', "es-Cl": 'es', "es-CO": 'es', "es-CR": 'es', "es-DO": 'es', "es-EC": 'es', "es-SV": 'es', "es-GT": 'es', "es-HN": 'es', "es-MX": 'es', "es-NI": 'es', "es-PA": 'es', "es-PY": 'es', "es-PR": 'es', "es-PE": 'es', "es-US": 'es', "es-UY": 'es', "es-VE": 'es',
        "en": 'en-US', "en-AU": 'en-US', "en-BZ": 'en-US', "en-CA": 'en-US', "en-029": 'en-US', "en-IN": 'en-US', "en-IE": 'en-US', "en-JM": 'en-US', "en-MY": 'en-US', "en-NZ": 'en-US', "en-PH": 'en-US', "en-SG": 'en-US', "en-ZA": 'en-US', "en-TT": 'en-US', "en-GB": 'en-US', "en-US": 'en-US', "en-ZW": 'en-US',
        "de": 'de', "de-AT": 'de', "de-DE": 'de', "de-LI": 'de', "de-LU": 'de', "de-CH": 'de',
        "fr": 'fr', "fr-BE": 'fr', "fr-CA": 'fr', "fr-LU": 'fr', "fr-MC": 'fr', "fr-CH": 'fr', "fr-FR": 'fr',
        "it": 'it', "it-IT": 'it', "it-CH": 'it',
        "ja": 'ja', "ja-JA": 'ja', "ja-JP": 'ja',
        "nl": 'nl', "nl-NL": 'nl', "nl-BE": 'nl',
        "pt": 'pt-PT', "pt-BR": 'pt-PT', "pt-PT": 'pt-PT',
        "ru": 'ru', "ru-RU": 'ru',
        "cs": 'cs', "cs-CZ": 'cs',
        "zh": 'zh', "zh-HK": 'zh', "zh-MO": 'zh', "zh-CN": 'zh', "zh-Hans": 'zh', "zh-SG": 'zh', "zh-TW": 'zh', "zh-Hant": 'zh'
    }
}, {
    /**
	 *  Rendering the control
	 * */
    initializeDateControl: function () {
        var self = this;
        var properties = self.properties;
        var control = self.getControl();
        var container = control.parent().parent().parent().parent();//self.getContainerRender();

        //Getting the input
        self.input = $(".new-mobiscroll-date-control", control);

        if (!properties.editable) {
            //if isn't a editable control, then show a non editable textbox
            var textTmpl = self.renderFactory.getTemplate("text");
            self.input = $.tmpl(textTmpl, {}).appendTo(control);

            //Add styles class
            container.addClass("bz-command-not-edit");
            self.input.attr('readonly', "readonly");
        }
    },

    configureHandlers: function () {
        var self = this;
    },

    /**
	 * To configure the mobiscroll plugin
	 * */
    postRender: function () {
        var self = this;

        self._super();

        //IniTialicing the control
        self.initializeDateControl();

        var inputControl = self.getDateControl();
        var controlElements = controlElements = self.properties.showTime ? ['calendar', 'time'] : ['calendar'];
        var minDateControl = self.getFormattedDate(self.properties.minValue, false, false);
        var maxDateControl = self.getFormattedDate(self.properties.maxValue, false, false);
        var defaultDateControl = self.getFormattedDate(self.properties.value, self.properties.showTime, true);
        var language = self.Class.AVAILABLE_LANGUAGES[BIZAGI_LANGUAGE];
        language = language ? language : "en";

        inputControl.mobiscroll().calendar({
            //theme: theme,
            mode: 'mixed',       // Specify scroller mode like: mode: 'mixed' or omit setting to use default
            display: 'bottom', // Specify display mode like: display: 'bottom' or omit setting to use default
            controls: controlElements,
            minDate: minDateControl,
            maxDate: maxDateControl,
            defaultValue: defaultDateControl,
            lang: language,
            buttons: [
				'set',
				{
				    text: this.getResource("render-plugin-search-users-clear-button-name"),
				    handler: function (e, inst) {
				        self.date = undefined;
				        self.time = undefined;
				        self.timeS = undefined;
				        self.onChangeHandler(true);
				        self.onSelect("", undefined);
				        inst.clear();
				    }
				},
				'cancel'
            ],
            onSelect: function (valueText, inst) {
                var data = inst.getValue();
                self.date = { 'day': data[1], 'month': data[0], 'year': data[2] };

                if (data.length > 3) {
                    self.time = { 'hours': data[3], 'minutes': data[4] };
                    self.timeS = (data[5] === "0" || data[5] === 0) ? "am" : "pm";
                }

                self.onChangeHandler();
                self.onSelect(valueText, inst);
            }
        });

        // Bind event for change value
        inputControl.on('focus', function (e) {
            e.preventDefault(); e.stopPropagation();

            if (typeof (cordova) !== "undefined") {
                cordova.plugins.Keyboard.disableScroll(true);
                cordova.plugins.Keyboard.close();
            }
        });

        //Set input displayValue if it has default value
        if (self.properties.displayValue === "" && self.properties.value) {
            var date = self.getFormattedDate(self.properties.value, self.properties.showTime, true, false);
            var displayValue = bizagi.util.dateFormatter.formatDate(date, self.properties.fullFormat);
            self.input.prop('value', displayValue);
        }

        inputControl.attr('keyboard', 'disable');
    },

    /**
	 * Get Date whit the Invariant and ISO Format applied.
	 * */
    getFormattedDate: function (date, showTime, inicializer) {
        var self = this;

        if (!date) { return; }

        //There are some dates that haven't time,... actions and validations for example or when we have to put a initial value (default value without time),
        //in that cases we should identify the case and if the date haven't the time value, we have to add (00:00:00) it to can apply the format (getDateFromInvariant).
        var time = (showTime && (self.getTime() === "0:0" || inicializer)) ? " " + date.substr(date.length - 8, date.length) : "";
        date = bizagi.util.dateFormatter.getDateFromInvariant(((inicializer && showTime && time.split("/").length > 1) || (date.split(":").length === 1)) ? date += " 00:00:00" : date, showTime);

        var fullDateFormated = self.setHour(self.getFullFormatedDate(date, showTime ? time !== "" ? time : " " + self.getTime() : " 00:00"));
        fullDateFormated = bizagi.util.dateFormatter.getDateFromISO(fullDateFormated, true);

        return fullDateFormated;
    },

    /**
	 * Sut the display value on input
	 * */
    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;
        var date = self.getFormattedDate(value, properties.showTime, false);
        var displayValue = bizagi.util.dateFormatter.formatDate(date, properties.fullFormat);
        var control = self.getControl();

        displayValue = self.applyDateFormat(displayValue, properties.showTime);

        if (properties.editable == false) {
            // Render as simple value
            if (typeof (displayValue) === "string" || typeof (displayValue) === "number") {
                $(control).html("<label class='readonly-control'>" + displayValue + "</label>");
            }
        } else {
            self.input.prop('value', displayValue);
        }
    },

    /**
	 * Get the DOM Control
	 * */
    getDateControl: function () {
        var self = this;
        return $("input.new-mobiscroll-date-control", self.getControl());
    },

    /**
	 * Get Time from mobiscroll
	 * */
    getTime: function () {
        var self = this;

        //Converting the time in 24h format.
        if (self.properties.showTime && (self.time && self.time["minutes"] !== "")) {
            var time = self.time["hours"] + ":" + self.time["minutes"];
            time += self.timeS;

            if (time.indexOf('am') != -1 && self.time["hours"] == 12) {
                time = time.replace('12', '0');
            }

            if (time.indexOf('pm') != -1 && self.time["hours"] < 12) {
                time = time.replace(self.time["hours"], (parseInt(self.time["hours"]) + 12));
            }

            return time.replace(/(am|pm)/, '');
        } else {
            return "";
        }
    },

    /**
	 * Get Date from mobiscroll
	 * */
    getDate: function () {
        var self = this;
        return (self.date && self.date["month"] !== "") ? self.date["year"] + "-" + (parseInt(self.date["month"]) + 1) + "-" + self.date["day"] : "";
    },

    /**
	 * Event onSelect of Mobiscroll, catch the selected value
	 * */
    onSelect: function (valueText, inst) {
        var self = this;
        if (inst) {
            self.invariantDate = inst.getDate();
            var formatedFullDate = self.setHour(self.getFullFormatedDate(self.invariantDate, ""));

            formatedFullDate = bizagi.util.dateFormatter.getDateFromISO(formatedFullDate, self.properties.showTime);
            self.setNewDateValue(formatedFullDate, self.properties.showTime);
            self.input.prop('value', self.properties.displayValue);
        } else {
            self.setNewDateValue("", self.properties.showTime);
            self.input.prop('value', "");
        }
    },

    /**
	 * Set the control value when the control change
	 */
    onChangeHandler: function (isClearButton) {
        var self = this;
        var properties = self.properties;
        var currentDate = self.getDate();
        var currentTime = (properties.showTime) ? " " + self.getTime() : " 0:00";

        // If curren date is empty reset actual value
        if ((currentDate === "" && !properties.showTime) || (currentDate === "" && currentTime === "") || (isClearButton)) {
            self.setValue("");
            properties.displayValue = "";
        } else {
            var formatedFullDate = self.setHour(self.getFullFormatedDate(currentDate, currentTime));
            formatedFullDate = bizagi.util.dateFormatter.getDateFromISO(formatedFullDate, true);

            if (!formatedFullDate || formatedFullDate == 0) { return; }

            // Set date value
            self.setNewDateValue(formatedFullDate, properties.showTime);

            var displayValue = bizagi.util.dateFormatter.formatDate(formatedFullDate, properties.fullFormat);
            displayValue = self.applyDateFormat(displayValue, properties.showTime);
            properties.displayValue = displayValue;
        }
    },

    /**
	 * Apply the final format that will be showed on the input
	 * */
    getFullFormatedDate: function (currentDate, currentTime) {
        var fullDate = "";

        if (currentDate.length > 0) {
            fullDate = currentDate;
        } else if (currentDate !== undefined) {
            fullDate = bizagi.util.dateFormatter.formatISO(currentDate, false);
        } else {
            fullDate = bizagi.util.dateFormatter.formatISO(new Date(), false);
        }

        fullDate += currentTime;
        var dateFromFormat = bizagi.util.dateFormatter.getDateFromFormat(fullDate, "yyyy-MM-dd H:m");

        return bizagi.util.dateFormatter.formatDate(dateFromFormat !== 0 ? dateFromFormat : currentDate, "yyyy-MM-dd H:mm:ss");
    },

    /**
	 * Set the control value
	 * */
    setNewDateValue: function (formatedFullDate, showTime) {
        var self = this;
        var value = formatedFullDate != "" ? bizagi.util.dateFormatter.formatInvariant(formatedFullDate, showTime) : "";
        self.setValue(value);
    },

    /**
	 * Removing the time when be necesary
	 * */
    applyDateFormat: function (date, showTime) {
        if (!showTime) {
            var initIndexTime = null;
            if (date.indexOf("00:00") != -1) {
                initIndexTime = date.indexOf("00:00");
            } else if (date.indexOf("0:00") != -1) {
                initIndexTime = date.indexOf("0:00");
            } else if (date.indexOf("12:00") != -1) {
                initIndexTime = date.indexOf("12:00");
            } else {
                initIndexTime = "-1";
            }

            var endIndexTime = initIndexTime + 8;
            date = initIndexTime === -1 ? date : date.replace(date.substring(initIndexTime, endIndexTime), "").replace(" am", "").replace(" pm", "");
        }

        return date;
    },

    /**
	 * Public method to determine if a value is valid or not
	 */
    isValid: function (invalidElements) {
        var self = this,
			properties = self.properties;
        var bValid = true;
        var value = self.getValue();

        if (value) {
            if (bizagi.util.parseBoolean(properties.editable) == true) {
                value = bizagi.util.dateFormatter.getDateFromInvariant(value);

                if (value != 0) {
                    // Clear time for validations
                    value.setHours(0, 0, 0, 0);

                    // Check min value and max value
                    if (properties.minValue && typeof properties.minValue == 'string') {
                        bValid = self.validateMinAndMaxValue(value, true, properties, invalidElements) ? false : bValid;
                    }

                    if (properties.maxValue && typeof properties.maxValue == 'string') {
                        bValid = self.validateMinAndMaxValue(value, false, properties, invalidElements) ? false : bValid;
                    }
                }
            }
        }

        if (bizagi.util.parseBoolean(properties.editable) == true) {
            // Check required
            if (properties.required && self.hasValue()) {
                var control = self.getDateControl();

                if (control.length > 0 && control.val().length <= 0) {
                    bValid = self.showMandatoryMessage(properties, invalidElements);
                }
            } else if (properties.required && !self.hasValue()) {
                self.showMandatoryMessage(properties, invalidElements);
            }
        }

        return bValid;
    },

    /**
	 * To validate the min and max values
	 * */
    validateMinAndMaxValue: function (value, isMinValue, properties, invalidElements) {
        var vValue = bizagi.util.dateFormatter.getDateFromInvariant(isMinValue ? properties.minValue : properties.maxValue);
        vValue.setHours(0, 0, 0, 0);

        if ((isMinValue && value < vValue) || (!isMinValue && value > vValue)) {
            var message = isMinValue ?
				this.getResource("render-date-minimum-validation").replaceAll("#label#", properties.displayName).replaceAll("#minValue#", bizagi.util.dateFormatter.formatDate(vValue, properties.fullFormat))
				: this.getResource("render-date-maximum-validation").replaceAll("#label#", properties.displayName).replaceAll("#maxValue#", bizagi.util.dateFormatter.formatDate(vValue, properties.fullFormat));

            invalidElements.push({
                xpath: properties.xpath,
                message: message
            });
            return true;
        }

        return false;
    },

    /**
	 * To show the mandatory values message
	 * */
    showMandatoryMessage: function (properties, invalidElements) {
        var self = this;
        var message = self.getResource("render-required-text").replaceAll("#label#", properties.displayName);

        invalidElements.push({
            xpath: properties.xpath,
            message: message
        });

        return false;
    },

    setHour: function (date) {
        return date.replace(" 0:", " 00:").replace(" 1:", " 01:").replace(" 2:", " 02:").replace(" 3:", " 03:").replace(" 4:", " 04:").replace(" 5:", " 05:").replace(" 6:", " 06:").replace(" 7:", " 07:").replace(" 8:", " 08:").replace(" 9:", " 09:");
    }
});