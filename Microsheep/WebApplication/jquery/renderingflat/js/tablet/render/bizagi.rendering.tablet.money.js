﻿/*
*   Name: BizAgi Tablet Render Money Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the money render class to adjust to tablet devices
*/

// Extends itself
bizagi.rendering.money.extend("bizagi.rendering.money", {}, {

    /* POSTRENDER 
    =====================================================*/
    postRender: function () {
        var self = this;

        // Call base
        this._super();

        self.numericInput = $("input", self.getControl());
        self.numericIcon = $("span", self.getControl());

        self.numericIcon.removeClass('bz-rn-icon-number').addClass('bz-rn-icon-coin');        

        var input = $("input", self.element);
        if (!input || input.length == 0) return;

        input.keyup(function (event) {
            event = event || window.event;
            this.value = this.value.replace(/[^0-9.,]/g, '');
            event.preventDefault();
        });
    },

    /* SETS THE VALUE IN THE RENDERED CONTROL
    =====================================================*/
    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;

        // Call base
        this._super(value);

        // Set value in input
        if (value != null && properties.editable) {
            self.numericInput.val(value);
            // Formats the input
            self.attachFormatCurrency();
            self.executeFormatCurrencyPlugin();
        }
    }
});
