﻿/*
 *   Name: BizAgi smartphone Render option select
 *   Author: CristianO
 *   Date: 29/10/2015
 *   Comments:
 *   -   Render a single input with a modal view, this modal view contain a
 *       selectable list.
 */

// Extends itself
bizagi.rendering.combo.extend("bizagi.rendering.combo", {
}, {
    /**
     * Initilizing the combo control
     * */
    initializeCombo: function () {
        var self = this;
        var properties = self.properties;
        var control = self.getControl();
        var container = control.parent().parent().parent().parent();//self.getContainerRender();
        var params = {};

        //Getting the input
        self.input = $(".new-combo-control", control);
        if (properties.parentComboValue && properties.parentComboValue != null) {
            params[self.Class.BA_ACTION_PARAMETER_PREFIX + "parent"] = properties.parentComboValue;
        }

        if (!properties.editable) {
            //if isn't a editable control, then show a non editable textbox
            var textTmpl = self.renderFactory.getTemplate("text");
            self.input = $.tmpl(textTmpl, {}).appendTo(control);

            //Add styles class
            container.addClass("bz-command-not-edit");
            self.input.attr('readonly', "readonly");

            if (self.input) {
                self.inputSpan = self.input.html("<span class=\"bz-command-not-edit bz-rn-text\"></span>").find("span");
                if (self.value != null) {
                    self.inputSpan.html(self.value.value || self.value.id);
                }
            }
        }else{
            if (typeof self.initialValueSet !== "undefined" && !self.initialValueSet) {
                $.when(self.renderCombo(params)).done(function (html) {
                    self.getControl().html(html);
                    self.input = self.combo = self.getControl();
                    self.configureCombo();
                });
            } else {
                self.configureCombo();
            }
        }
    },

    /**
     * Postrender Method
     * */
    postRender: function () {
        var self = this;

        //Initialicing the control
        self.initializeCombo();

        var properties = self.properties;
        var control = self.getControl();
        var container = control.parent().parent().parent().parent();//self.getContainerRender();

        if (properties.editable) {
            container.addClass("bz-command-edit-inline");
        }

        self._super();
    },

    renderEdition: function () {},

    /**
     * Setting the combo's handlers
     * */
    configureCombo: function () {
        var self = this;

        self.inputSpan = self.input.find(".bz-rn-cm-text-label");
        self.inputSpan.html($("option:selected", self.input).text());
        self.inputSpan.val($("option:selected", self.input).text());

        self.input.unbind().bind("click", function () {
            //After click on input combo, remove focus of it.
            self.input.find(".new-combo-control").blur();
            $(self.input).blur();
            self.input.attr('keyboard', 'disable');

            //Creating modal view
            var modalViewTemplate = kendo.template(self.renderFactory.getTemplate('comboModalView'), { useWithBlock: false });
            var modalView = $(bizagi.util.trim(modalViewTemplate({'items': self.properties.data, 'displayName': self.properties.displayName || ""}))).clone();

            modalView.kendoMobileModalView({
                close: function () {
                    this.destroy();
                    this.element.remove();
                },
                useNativeScrolling: true,
                modal: false
            });

            self.configureModalViewHandlers();
            modalView.kendoMobileModalView('open');
            modalView.closest(".k-animation-container").addClass("bz-rn-new-modalview-position");
        });

        //Setting the Display Value
        /**
         * Case 1: Is a cascading combo and all combos have values (when form is rendering or the form is saved)
         * Case 2: Is a cascading combo and in the parent combo was selected a new value
         * Case 3: Is a normal combo.
         * */
        if(self.initializingCascadingCombo && self.properties.value) {
            self.onComboChange(self.properties.value);
            self.setDisplayValue(self.properties.value);
        }else if(self.initializingCascadingCombo != undefined && !self.initializingCascadingCombo && self.parentCombo && self.parentCombo.properties.previousValue
                && self.parentCombo.properties.value.value != (self.parentCombo.properties.previousValue.value || self.parentCombo.properties.previousValue[0].value)){
            self.setDisplayValue({id: "", value: "-------------"});
        }else if(self.initializingCascadingCombo != undefined && !self.initializingCascadingCombo && self.properties.value){
            self.setDisplayValue(self.properties.value);
        }else if(self.properties.value){
            self.setDisplayValue(self.properties.value);
        }
    },

    /**
     * Configure the modalView Handlers for the new combo control.
     * */
    configureModalViewHandlers: function(){
        var self = this;
        //getting modalView
        var inputContainer = $("#modalview-filter-combo", "body");
        //getting combo list elements
        var container = inputContainer.find(".ui-bizagi-render-list-combo li");

        //Hide the clear text icon
        inputContainer.find(".bz-wp-combo-cancel-icon-list").hide(500);

        inputContainer.delegate("#ui-bizagi-cancel-button", "click", function () {
            inputContainer.data("kendoMobileModalView").close();
        });

        //Adding styles to modalView
        $("#ui-bizagi-cancel-button", "body").closest("#filter-footer").addClass("bz-rn-new-modalview-footer-styles");
        $(".bz-wp-search-on-combo-back-icon-list", "body").closest("#filter-header").addClass("bz-rn-new-modalview-header-styles");
        $("#ui-bizagi-wp-filter-results-combo-wrapper", "body").closest(".km-native-scroller").addClass("bz-rn-new-modalview-content-styles");

        //Filtering list
        inputContainer.find(".input-filter-results-on-combo").bind('change keypress  keyup change', function () {
            self.filterList(this.value, container, inputContainer);
        });

        //Cleaning list
        inputContainer.find(".bz-wp-combo-cancel-icon-list").bind('click', function(){
            self.filterList("", container, inputContainer);
            inputContainer.find(".input-filter-results-on-combo").val("");
            $(this).hide(500);
        });

        //Configuring list as a selectable element
        self.list = $(".ui-bizagi-render-list-combo", inputContainer);
        self.list.selectable();
        self.setModalViewDisplayValue(self.properties.value || false);

        self.inputSpan = inputContainer.find(".bz-rn-cm-text-label");
        self.inputSpan.html($("li.ui-selected", self.list).text());
        self.inputSpan.val($("li.ui-selected", self.list).text());

        //Setting the initial selected display value
        self.list.find("li").bind("click", function(){
            var selectedElements = self.list.find(".ui-state-active");
            self._newSelectedElement = this;

            selectedElements.each(function(index, element){
                if(self._newSelectedElement.id !== element.id){
                    $(element).removeClass("ui-state-focus ui-selected ui-state-active");
                }
            });

            self.input.blur();
            $(".input-filter-results-on-combo", inputContainer).blur();
        });

        inputContainer.delegate("#ui-bizagi-apply-button", "click", function () {
            var selected = $("li.ui-selected", self.list);
            var value = $(selected).data("value");
            var label = $(selected).text();

            //Update internal value
            var newValue = { id: value, value: label };
            self.setValue(newValue);
            self.selectedValue = newValue.value;
            self.setDisplayValue(newValue);
            self.changeCombo(newValue.value, newValue);
            inputContainer.data("kendoMobileModalView").close();
        });

        inputContainer.find(".bz-wp-search-on-combo-back-icon-list").bind("click", function(){
            inputContainer.data("kendoMobileModalView").close();
        });
    },

    /**
     * Filtering list of elements
     * */
    filterList: function(search, $li, inputContainer){
        if(search !== ""){
            inputContainer.find(".bz-wp-combo-cancel-icon-list").show(500);
        }else{
            inputContainer.find(".bz-wp-combo-cancel-icon-list").hide(500);
        }

        $li.filter(function() {
            var stringContainer = $(this).text().toUpperCase();
            var stringToSearch = search.toUpperCase();

            if(stringContainer.indexOf(stringToSearch) >= 0){
                return $(this);
            }else{
                $(this).hide();
            }
        }).show();
    },

    actionSave: function () {
        var self = this;
        var selected = this.inputEdition.find('.bz-rn-combo-selected');
        var valueItem = selected.find(">div >span").html();
        var valueObjet = { id: selected.attr("id"), value: valueItem };
        self.changeCombo(valueItem, valueObjet);
    },

    /**
     * Setting the selected element.
     * */
    setModalViewDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;

        if (self.list && properties.editable && value && value !== undefined && value !== null && value.id != "") {
            self.list.find("#ui-bizagi-list-" + value.id).addClass("ui-state-focus ui-selected ui-state-active");
        }
    },

    /**
     * Setting the display value.
     * */
    setDisplayValue: function (value) {
        var self = this;
        var properties = self.properties;
        var control = self.getControl();
        var valuers = value && typeof(value.value) !== "undefined" ? bizagi.rendering.combo.prototype.formatItem.call(self, value.value) : value;

        if (properties.editable) {
            $(".new-combo-control", control).val(value.value);
            self.inputSpan.html(valuers);
            self.inputSpan.val(valuers);
        } else {
            var tmpreadonly = $("<span class=\"bz-rn-text\" disabled=\"disabled\"></span>");
            control.html(tmpreadonly.html(valuers));
        }
    },

    /**
     * Adding a display Value
     * */
    addDisplayValue: function (children) {
        var self = this;
        var self_children = children;
        var value = self_children.value;
        var control = self.getControl();
        var valuers = typeof(value.value) !== "undefined" ? self.formatItem(value.value) : value.value;

        self_children.input.html(valuers);
        self_children.input.attr("id", value.id);
        self_children.input.appendTo(control);
    },

    /**
     * Add combo on cascading combos
     * */
    addComboToControl: function (children) {
        var self = this;
        var self_children = children;
        var control = self.getControl();
        var parentElement = (control.find("#" + self_children.properties.parentCombo).length > 0) ? control.find("#" + self_children.properties.parentCombo) : '';

        (control.find("#" + self_children.properties.id).length > 0) ?
                control.find("#" + self_children.properties.id).replaceWith(self_children.input) :
                (parentElement != "") ?
                        parentElement.after(self_children.input) :
                        self_children.input.appendTo(control);
    },

    /**
     * Change event on cascading combos.
     * */
    changeCombo: function (valueItem, valueObjet) {
        var self = this;
        self.setValue(valueObjet);
        self.selectedValue = valueItem;
        self.trigger('selectElement', valueObjet);
        self.inputSpan.html(valueItem);
        self.inputSpan.val(valueItem);
    },

    /**
     * Getting the templates
     * */
    getTemplateName: function () {
        return "combo";
    },

    /**
     * Getting the templates
     * */
    getTemplateEditionName: function () {
        return "edition.combo";
    }
});