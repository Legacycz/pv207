﻿/*
*   Name: BizAgi Desktop Form Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the container class to adjust to desktop devices
*   -   Will apply a desktop form template
*/

// Auto extend
bizagi.rendering.form.extend("bizagi.rendering.form", {

    /* 
    *   Template method to implement in each device to customize each container after processed
    */
    postRenderContainer: function (container) {
        var self = this;
        var buttons = self.getButtons();

        // Call base
        self._super(container);

        // Style buttons
        buttons.button();

        //Set button length
        var lengthButtons = (buttons) ? buttons.length : 0;

        if (lengthButtons) {

            $(document).data('auto-save', 'auto-save');

            //bind event auto-save
            $(document).unbind("save-form").bind("save-form", function (e, deferredSave) {
                self.autoSaveEvents(deferredSave);
            });

            //bind event beforeunload
            $(window).unbind('beforeunload').bind('beforeunload', function (e) {

                var newData = {};
                self.collectRenderValues(newData);
                //if there are changes in the form show a message
                if (!$.isEmptyObject(newData) && $(document).data('auto-save')) {

                    return bizagi.localization.getResource("confirmation-savebox-message2");
                }

                return;

            });

        }

    },
    /*
    * Auto Save Events
    */
    autoSaveEvents: function (deferredSave, saveBox) {

        var self = this;
        var data = {};
        self.collectRenderValues(data);

        if (!$.isEmptyObject(data)) {

            $.when(bizagi.showSaveBox(bizagi.localization.getResource("confirmation-savebox-message1"), "Bizagi", "warning")).done(function () {
                $.when(self.saveForm()).done(function() {
                    deferredSave.resolve();
                });
            }).fail(function () {
                deferredSave.resolve();
            });

        } else {
            deferredSave.resolve();
        }

    },

    /*
    *   Template method to get the buttons objects 
    */
    getButtons: function () {
        var self = this;
        var container = self.container;

        return $(".ui-bizagi-button-container .ui-bizagi-button", container);
    },

    /*
    *   Template method to implement in each device to customize the container's behaviour to show layout
    */
    configureLayoutView: function () {
        // Do nothing
    }
});
