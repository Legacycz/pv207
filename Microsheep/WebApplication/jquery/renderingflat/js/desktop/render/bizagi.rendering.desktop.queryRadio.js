﻿/*
 *   Name: BizAgi Desktop Render Query Radio Extension
 *   Author: Paola Herrera
 *   Comments:
 *   -   This script will redefine the query list render class to adjust to desktop devices
 */

// Extends itself
bizagi.rendering.radio.extend("bizagi.rendering.queryRadio", {}, {
    /*
     *  configure Handlers for check, column included in the response
     */
    configureHandlers: function () {
        var self = this;
        self._super();
        var checkIncluded = self.element.find(".ui-bizagi-render-control-included");
        checkIncluded.change(function () {
            self.properties.included = $(this).is(":checked");
        });
    },
    /*
     *   Add the render data to the given collection in order to send data to the server
     */
    collectData: function (renderValues) {
        var self = this;
        var properties = self.properties;
        var xpath = properties.xpath;
        var value = self.getValue();
        if (!bizagi.util.isEmpty(xpath)) {
            var filter = false;
            if (value !== null && typeof (value) !== "undefined") {
                // Add a validation because sometimes value contains an empty not null object
                if (typeof (value) == "object" && value.id === undefined)
                {return;}
                filter = typeof (value.id) !== "undefined" && !bizagi.util.isEmpty(value.id);
            }
            if (filter || self.properties.included) {
                renderValues.push({
                    "value": (filter) ? value.id : null,
                    "visible": self.properties.included,
                    "xpath": properties.xpath,
                    "operation": properties.range
                });
            }
        }
    }

});

