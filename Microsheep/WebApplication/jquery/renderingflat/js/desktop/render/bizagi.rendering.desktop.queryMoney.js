﻿/*
*   Name: BizAgi Desktop Render Query Money Extension
*   Author: Paola Herrera
*   Comments:
*   -   This script will redefine the query money render class to adjust to desktop devices
*/

// Extends itself
bizagi.rendering.money.extend("bizagi.rendering.queryMoney", {}, {
    /*
     *  configure Handlers for check, column included in the response
     */
    configureHandlers: function () {
        var self = this;
        self._super();
        var checkIncluded = self.element.find(".ui-bizagi-render-control-included");
        checkIncluded.change(function () {
            self.properties.included = $(this).is(":checked");
        });
    },
    /*
     *   Add the render data to the given collection in order to send data to the server
     */
    collectData: function (renderValues) {
        var self = this;
        var properties = self.properties;
        var xpath = properties.xpath;
        var value = self.getValue();
        var filter =false;
        if (!bizagi.util.isEmpty(xpath)) {
            filter=typeof (value) !== "undefined" && !bizagi.util.isEmpty(value);
            if (filter || self.properties.included) {
                renderValues.push({
                    "value": (filter) ? value : null,
                    "visible": self.properties.included,
                    "xpath": properties.xpath,
                    "operation": properties.range
                });
            }
        }
    }

});
