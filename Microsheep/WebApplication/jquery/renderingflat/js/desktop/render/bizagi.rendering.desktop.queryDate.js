/*
 *   Name: BizAgi Desktop queryDate
 *   Author: Iván Ricardo Taimal Narváez
 *   Comments:
 *   -
 */

// Extends itself
bizagi.rendering.date.extend("bizagi.rendering.queryDate", {}, {
    /*
     *  configure Handlers for check, column included in the response
     */
    configureHandlers: function () {
        var self = this;
        self._super();
        var checkIncluded = self.element.find(".ui-bizagi-render-control-included");
        checkIncluded.change(function () {
            self.properties.included = $(this).is(":checked");
        });
    },
    /*
     *   Add the render data to the given collection in order to send data to the server
     */
    collectData: function (renderValues) {
        var self = this;
        var properties = self.properties;
        var xpath = properties.xpath;
        var value = self.getValue();
        var compareValue = properties.originalValue;
        if (!bizagi.util.isEmpty(xpath)) {
            if (!bizagi.util.isEmpty(value)) {
                // Formats the value in full invariant (with time) in order to send to the server
                var date = bizagi.util.dateFormatter.getDateFromInvariant(value, properties.showTime);
                if (!properties.showTime) {
                    date.setHours(0, 0, 0, 0);
                }
                var filter = (compareValue == value) ? false:true;
                if (filter || self.properties.included) {
                    renderValues.push({
                        "value": (filter) ? bizagi.util.dateFormatter.formatInvariant(date, true) : null,
                        "visible": self.properties.included,
                        "xpath": properties.xpath,
                        "operation": properties.range
                    });
                }
            }
        }
    }
});
