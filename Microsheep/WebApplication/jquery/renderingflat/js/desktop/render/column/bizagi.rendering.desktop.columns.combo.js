﻿/*
*   Name: BizAgi Desktop Column Decorator Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the column decorator class to adjust to desktop devices
*/


// Extends itself
bizagi.rendering.columns.combo.extend("bizagi.rendering.columns.combo", {}, {

    /*
    *   Post process the element after it has been rendered
    */
    postRender: function (surrogateKey, cell, tableCell) {
        // Call base
        this._super(surrogateKey, cell, tableCell);

        // Apply styles to input
        var input = $(".ui-selectmenu-status", cell);
        this.applyColumnStyles(input);

        // Remove padding for td
        if (this.properties.editable && tableCell) {
            tableCell.addClass("ui-bizagi-cell-combo");
        }
    },
    /*renderReadOnly: function (surrogateKey, value) {
        var self = this;
        return value
    },*/
    /*
    *   Post process the element after it has been rendered
    */
    postRenderReadOnly: function (surrogateKey, cell, tableCell) {
        // Call base
        this._super(surrogateKey, cell, tableCell);

        // Apply styles to input
        var input = $(".ui-selectmenu-status", cell);
        this.applyColumnStyles(input);

        // Add padding for td
        if (tableCell)
            tableCell.removeClass("ui-bizagi-cell-combo");
    }
});
