/*
*   Name: BizAgi Desktop Link Column Decorator Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the link column decorator class to adjust to desktop devices
*/

// Extends from column
bizagi.rendering.columns.column.extend("bizagi.rendering.columns.link", {}, {

    /*
    *   Apply custom overrides to each decorated instance
    */
    applyOverrides: function (decorated) {
        this._super(decorated);
        decorated.getControl = this.getControl;
        // Hacks the getControl method in the decorated render to add features
        if (decorated.properties.type == "columnFormLink") {
            decorated.getFormLinkXpath = this.getFormLinkXpath;
        }
    },

    /*
    *   Template method to get the control element
    *   This will execute under the decorated element context
    */
    getControl: function () {
        var self = this;
        var properties = self.properties;

        if (properties.editable) {
            return $(".ui-bizagi-render-control", self.element);
        } else {
            return $(".ui-bizagi-cell-readonly", self.element);
        }
    },

    /*
    *   Returns the in-memory processed element
    *   so the caller could append it to any place
    */
    render: function (surrogateKey, value, tableCell) {
        var self = this;
        var decorated = self.getDecorated(surrogateKey);

        // Change the xpath context
        decorated.properties.xpathContext = self.grid.properties.xpath + "[id=" + surrogateKey + "]";

        // Render the control
        var result = this._super(surrogateKey, value, tableCell);

        return result;
    },
    /*
    *   Returns the cell html to be inserted in the table
    */
    renderReadOnly: function (surrogateKey, value, tableCell) {
        var self = this;

        // Render the control editable
        self.properties.editable = true;
        var result = this.render(surrogateKey, value, tableCell);

        return result;
    },

    /*
    *   Post process the element after it has been rendered
    */
    postRenderReadOnly: function (surrogateKey, cell, tableCell) {
        var self = this;

        // Render the control editable
        self.properties.editable = true;
        this.postRender(surrogateKey, cell, tableCell);
    },

    /*
    *   Get Form Link xpath to use
    */
    getFormLinkXpath: function () {
        if (!this.properties.xpath) return;
        return this.grid.properties.xpath + "[id=" + this.surrogateKey + "]." + this.properties.xpath;
    }


});
