/*
 *   Name: BizAgi Desktop Render Yes-No Extension
 *   Author: Diego Parra
 *   Comments:
 *   -   This script will redefine the Yes-No render class to adjust to desktop devices
 */

// Extends itself
bizagi.rendering.searchList.extend("bizagi.rendering.searchList", {
    ADVANCED_SEARCH_ID: -1,
    ADVANCED_ADDITION_ID: -2,
    CLEAR_SEARCH_ID: -3,
    SEARCH_MIN_LENGTH: 2
}, {

    /*
     *   Constructor
     */
    init: function(renderFactory, dataService, parent, data){
        // Call base
        this._super(params);

        // Fill default properties
        var properties = this.properties;
        properties.searchImage = properties.searchImage || this.dataService.getSearchDefaultImage();
    },

    /*
     *   Template method to implement in each device to customize each render after processed
     */
    postRender: function(){
        var self = this;
        var control = self.getControl();
        
        // Call base 
        this._super();

        // Apply plugin
        self.searchInput = $("input", control);
        self.applyAutocompletePlugin();
    },

    /* Apply autocomplete capabilities to a single input*/
    applyAutocompletePlugin: function(){
        var self = this,
        control =  self.getControl(),
        searchInput = self.searchInput;

        searchInput.autocomplete({
            messages: {
                noResults: null, //"No search results.",
                results: function () { }
            },
            source: function (req, add) {                
                self.processRequest(req, add);
            },
            select: function(event, ui){
                self.appendFoundItem(event, ui);
                searchInput.autocomplete('close');
            },
            close: function(){
                if($('.biz-render-input-search',control).data('selected') == 1){
                    $('.biz-render-input-search',control).val('');       
                                
                    // Turn off the flag
                    $('.biz-render-input-search',control).data('selected','0');
                    
                    // Set focus on input field
                    $('.biz-render-input-search',control).focus();
                }
            },
            minLength: self.Class.SEARCH_MIN_LENGTH
        });
        
        
        $('.biz-render-input-search',control).bind('keydown',function(event,ui){
            event.stopPropagation();
            var input = $('.biz-render-input-search',control);
            var ul = $('.biz-render-searchlist ul',control);
            var lastElement = $('li',ul).not(':last').last();  

            // if press back or delete key. Does not work on webkit
            if((event.keyCode == 8 || event.keyCode == 46)){ 
                                
                if($(input).val().length == 0 && $(lastElement).hasClass('selected')){
                    // Delete Element
                    $('.closebutton',$(lastElement)).trigger('click'); 
                    
                    // Set focus on input field
                    $(input).focus();
                }else if($(input).val().length == 0 ){
                    // Add selected class
                    $(lastElement).addClass('selected');
                }
            }
                       
        });
        
        // Set close event
        $('.closebutton',control).click(function(){            
            var li = $(this).parent('li');
            // Remove from main memory and set the new value    
            self.removeElement({
                id: li.data('id')
            });
            
            // Remove list element
            li.remove();
                     
            // Set focus on input element
            $('.biz-render-input-search',control).focus();
        });
        
        // Open advanced search
        $('.button-search',control).click(function(){
            self.showSearchDialog();
        });
    },
    
    
    /* 
     * Append matched element
     */
    appendFoundItem : function(event, ui){   
        var control = this.getControl();
        var ul = $('.biz-render-input-search').parents('ul');
        var last = $('li:last',ul);           
        var newElement = $('<li data-id="'+ui.item.id+'"><label>'+ ui.item.value +'</label><a class="closebutton"></a></li>');

        // Append new li with match word
        $(last).before(newElement);
                    
        // Set close event
        $('.closebutton:last',ul).click(function(){            
            $(this).parent('li').remove();
                     
            // Set focus on input element
            $('input',ul).focus();
        });
        
        // Set selected flag
        $('.biz-render-input-search',control).data('selected','1');
        
        // Set focus on input field
        $('.biz-render-input-search',control).focus();
        
                    
        // Remove selected css class from all elements
        $('li',ul).removeClass('selected');
        
        // Set value
        this.addElement({
            id:ui.item.id,
            value:ui.item.value
        });
    },

    /* 
     *   Renders a search item
     */
    renderSearchItem: function(item){
        var self = this;
        var properties = self.properties;
        var template = self.renderFactory.getTemplate("searchItem");

        var searchItem = $.tmpl(template, {
            image: properties.searchImage,
            label: item.label
        });

        searchItem.data("item.autocomplete", item);

        return searchItem;
    },

    /* Renders the advanced item */
    renderAdvancedItem: function(){
        var self = this;
        var searchItem = self.renderSearchItem({
            id: self.Class.ADVANCED_SEARCH_ID, 
            label: this.getResource("render-search-advanced-label")
        });

        // Bind click event
        searchItem.click(function () {
            self.showSearchDialog();
        });

        return searchItem;
    },

    /* Renders the advanced item */
    renderClearItem: function(){
        var self = this;
        var searchItem = self.renderSearchItem({
            id: self.Class.CLEAR_SEARCH_ID, 
            label: this.getResource("render-search-clear-label")
        });

        // Bind click event
        searchItem.click(function () {
            self.setValue(null);

            // Submit info to server
            self.submitData();
        });

        return searchItem;
    },

    /* Renders the add item*/
    renderAddItem: function(){
        var self = this;
        var searchItem = self.renderSearchItem({
            id: self.Class.ADVANCED_ADDITION_ID, 
            label: this.getResource("render-search-add-label")
        });

        // Bind click event
        searchItem.click(function () {
            // TODO: Implement this
            });

        return searchItem;
    },


    /* 
     *   Process the request data for the autocomplete control
     */
    processRequest: function(req, addFunctionCallBack){
        var self = this,
        properties = self.properties;
        

        // Create array for response objects   
        var suggestions = [];

        if ((req.term.length >= this.Class.SEARCH_MIN_LENGTH)) {
            
            // Update term property to reflect the search
            properties.term = req.term;
            this.getData()
            .done(function(data){
                // elements in the list
                var listIds = $('li:[data-id]',self.getControl);
                // Process response to highlight matches
                $.each(data, function (i, val){                    
                    var inTheList = false;
                    
                    if(val.value != "" && val.id != ""){
                        // check if element is already appended in the list
                        $.each(listIds, function(key,value){
                            if(inTheList || $(value).data('id') == val.id){
                                inTheList = true;                                
                            }
                        });
                        if(!inTheList){
                            suggestions.push({
                                id: val.id,
                                label: val.value,
                                value: val.value
                            });
                        }
                    }
                });

                addFunctionCallBack(suggestions);
            });

        } else {
        //    addFunctionCallBack(suggestions);
        }
    },

    /* 
     *   Returns the desired width for the suggestion menu
     */
    calculateSuggestionWidth: function(){
        var self = this;
        var suggestMenu = self.searchInput.data("ui-autocomplete").menu.element;

        return suggestMenu.width();
    },

    /*
     *   Sets the value in the rendered control
     */
    setDisplayValue: function(value){
        var self = this;
        var properties = self.properties;

        if (value !== undefined){
            if (properties.editable) {
                self.searchInput.val(value.label);
            }
        }

        // Call base
        //this.setValue(value);
        //this._super(value);
    },

    /*
     *   Returns the value to display, ex. non-editable renders
     */
    getDisplayValue: function(){
        return this.selectedValue;
    },
    
    /**
     * Open Add new record dialog 
     */
    showAddRecordDialog: function(){
        var self = this;
        var properties = self.properties;
        var  buttons={
            text:bizagi.localization.getResource("render-form-dialog-box-add"),
            click:function(){
                // Show dialog with add form
                var dialog = new bizagi.rendering.dialog.form(self.dataService, self.renderFactory, {
                    showSaveButton: properties.editable,
                    title: bizagi.localization.getResource("render-dialog-entity"),
                    onSave: function (data) {
                        // Remove pagecache from data
                        var idPageCache = data.idPageCache;

                        var params = {
                            action: "SAVE",
                            data: data,
                            idPageCache: idPageCache,
                            guidEntity: properties.entity,
                            contexttype: 'entity'
                        }
                            
                        $.when(
                            self.dataService.submitData(params)
                            ).done(function(data){
                            // SUCCESS
                            });
                    }
                });
                    
                dialog.render({
                    idRender: properties.id,
                    idPageCache: properties.idPageCache,
                    requestedForm: "addForm",
                    guidEntity: properties.entity,	
                    contextType: 'entity'
                }).fail(function () {
                    //console.log("failed");
                    });               
            }  
        }
        return {
            buttons: buttons
        };
    },

    /* 
     *   Opens the search dialog
     */
    showSearchDialog: function(){
        var self = this,
        control = self.getControl(),        
        properties = self.properties,
        li = $('li:[data-id]',control),
        canBeAdd = true,
        extraButtons;

        // Define add button
        if(properties.allowNew){
            extraButtons = self.showAddRecordDialog();
        }
    
        // Show search dialog
        var dialog = new bizagi.rendering.dialog.search(self.dataService, self.renderFactory, properties.searchForms, {
            allowFullSearch: properties.allowFullSearch	,
            maxRecords: properties.maxRecord,
            multiSelect: true
        },extraButtons);

        dialog.render({
            idRender: properties.id,
            xpath: properties.xpath,
            xpathContext: properties.xpathContext,
            idPageCache: properties.idPageCache
        })
        .done(function(item){
            var label = [];

            // Parse label
            $.each(item.label, function(key,value){
                if(key!='id'){
                    label.push($(value).text());
                }
            });
            
            // check if element is already appended in the list
            $.each(li,function(key, value){
                if($(value).data('id') == item.id ){
                    canBeAdd = false;
                }                
            });
            
            if(canBeAdd){
                self.appendFoundItem('',{
                    item:{
                        id: item.id, 
                        value: label.join(',')
                    }
                });                
            }
        });
    },
    
    
    /*
     *   Method to render non editable values
     */
    postRenderReadOnly: function() {
        var self = this;
        var control = self.getControl();
        var properties = self.properties;
        var template = self.renderFactory.getTemplate("searchList");
        
        // Clear base actions
        control.empty();

        return $.tmpl(template,{
            value: properties.value,
            hasSearchForm: (properties.searchForms)?true:false,
            editable: false
        }).appendTo(control);
    }
});
