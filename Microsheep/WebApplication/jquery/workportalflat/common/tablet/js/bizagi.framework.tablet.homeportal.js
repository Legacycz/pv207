/*
*   Name:
*   Author: Luis Cabarique - LuisCE
*   Comments:
*   -   This script will define homePortal Framework
*/
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.webparts = (typeof (bizagi.webparts) !== "undefined") ? bizagi.webparts : {};


bizagi.workportal.homportalFramework.extend("bizagi.workportal.homportalFramework", {}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade) {
        var self = this;

        self._super(workportalFacade);
        self.deviceType = 'tablet';

        var titleTaskFeed = bizagi.localization.getResource("workportal-taskfeed-title");

        if (!self.workportalFacade.dataService.online && bizagi.util.hasOfflineForm()) {
            titleTaskFeed = bizagi.util.getItemLocalStorage("inputtray") == "false" ? bizagi.localization.getResource("workportal-menu-outbox") : bizagi.localization.getResource("workportal-menu-drafts");
        }

        self.homePortalLayout = {
            "menuHome": {
                "position": "homePortal",
                "direction": "right",
                "canvas": "menuHome",
                "title": "",
                "layout": "main-default"
            },
            "taskFeed": {
                "position": "homePortal",
                "direction": "left",
                "canvas": "taskFeed",
                "title": titleTaskFeed
            },
            "summaryItem": {
                "position": "menuHome",
                "canvas": "summaryItem"
            },
            "reportsChart": {
                "position": "homePortal",
                "direction": "right",
                "canvas": "reportsChart",
                "title": "Reports",
                "layout": "main-default-close"
            }
        };
    },

    _back: function () {
        var self = this;

        $('#right-pane').data('kendoMobilePane').navigate('#:back');
    },

    configureHandler: function () {
        var self = this;
        self._super();
    },

    /**
    * navigation control
    * @param navParams
    */
    homePortalNavigate: function (navParams) {
        var destination = navParams.url;
        var canvas = navParams.sender.element;
    },

    /**
    * when view is shown
    * @param navParams
    */
    homePortalViewShown: function (navParams) {
        var self = this;
        self.destination = navParams.view.id.replace(/#/i, "");
        self.content = navParams.view.element;
        self.container = navParams.view.container;

        if (self.destination === "reportsChart") {
            var navLength = navParams.sender.history.length;
            if (navParams.sender.history[navLength - 1] === navParams.sender.history[navLength - 3]) {
                navParams.sender.history.pop();
                //navParams.sender.history.pop();
            }

            $('.km-view-title', self.content).children().text("");

            $('.btn-close', self.content).unbind('click').bind('click', function () {
                $("iframe", ".bz-wp-reports-chart").remove();
                $(".bz-wp-reports-chart", self.content).empty();
                
                $("#left-pane", self.conten).toggle();
                $('.km-view-title', self.content).children().text("");
                self._back();
            });

            // Ocultar pane left
            $("#left-pane", self.conten).toggle();
        }
    },

    homePortalLayoutInit: function (context) {

    }
});