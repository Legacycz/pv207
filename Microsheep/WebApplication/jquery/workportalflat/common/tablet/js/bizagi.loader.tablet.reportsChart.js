﻿
// Create or Define BizAgi namespace
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};

var BIZAGI_DEFAULT_DEVICE = this.args.env.BIZAGI_DEFAULT_DEVICE != "tablet_android" ? "tablet" : this.args.env.BIZAGI_DEFAULT_DEVICE;
var BIZAGI_ENVIRONMENT = this.args.env.BIZAGI_ENVIRONMENT;
var BIZAGI_PATH_TO_BASE = this.args.env.BIZAGI_PATH_TO_BASE;
var BIZAGI_LOCAL_RESOURCES = this.args.env.BIZAGI_LOCAL_RESOURCES;
var BIZAGI_USE_ABSOLUTE_PATH = this.args.env.BIZAGI_USE_ABSOLUTE_PATH;
var BIZAGI_DEFAULT_CURRENCY_INFO = this.args.env.BIZAGI_DEFAULT_CURRENCY_INFO;
var BIZAGI_SETTINGS = this.args.env.BIZAGI_SETTINGS;
var BIZAGI_LANGUAGE = this.args.env.BIZAGI_LANGUAGE;
var BIZAGI_PROXY_PREFIX = this.args.env.BIZAGI_PROXY_PREFIX;

var reportLoader = {
    initialize: function (params) {
        var self = this;
        params = params || {};

        self.context = params.context;
        self.env = params.env;

        self.report = params.reporting.report;
        self.info = params.reporting.info;
        self.components = params.reporting.components;
        self.endPoint = params.reporting.endPoint;
        self.filters = params.reporting.filters;
        self.myTeam = params.reporting.myTeam;
        self.height = params.height;

        bizagi.currentUser = self.context.currentUser;

        this.initReports();
    },

    initReports: function () {
        var self = this;
        var loader = bizagi.loader;

        loader.preInit(["bizagiDefault", BIZAGI_ENVIRONMENT, undefined, BIZAGI_PATH_TO_BASE],
            [BIZAGI_LANGUAGE, false, "",
                [bizagi.currentUser.symbol, bizagi.currentUser.groupSeparator, bizagi.currentUser.decimalSeparator, bizagi.currentUser.decimalDigits],
                [bizagi.currentUser.shortDateFormat, bizagi.currentUser.timeFormat, bizagi.currentUser.longDateFormat],
                [BIZAGI_SETTINGS.UploadMaxFileSize], "", "ASP.NET_SessionId", false, false
            ]);

        var urlDefinition = '/jquery/bizagi.module.definition.json.txt';
        if (BIZAGI_DEFAULT_DEVICE === 'tablet_android') {
            urlDefinition = 'jquery/bizagi.module.definition.json.txt';
        }

        loader.init({
            url: urlDefinition,
            callback: function () {

                loader.start("workportal").then(function () {

                    var workportal = new bizagi.workportal.facade({ proxyPrefix: BIZAGI_PROXY_PREFIX });

                    bizagi.util.smartphone.startLoading();

                    loader.start("reporting").then(function () {

                        var reportingModule = new bizagi.reporting.facade({
                            proxyPrefix: (typeof (bizagi.proxyPrefix) !== "undefined") ? bizagi.proxyPrefix : ""
                        });

                        $.when(reportingModule.render({
                            canvas: $("#reports-canvas"),
                            report: self.report,
                            info: self.info,
                            components: self.components,
                            filters: self.filters,
                            myTeam: self.myTeam
                        })).done(function (reportResult) {
                            $("#bizagi-reporting-wrapper").outerHeight(self.height);
                            self.attachReportEvents(reportResult);
                        });

                        bizagi.util.smartphone.stopLoading();
                    });
                });
            }
        });
    },

    /*
    * Attach events for reports object
    */
    attachReportEvents: function (report) {

        var self = this;

        report.bindWindowResize();

        report.subscribe("filterChange", function (event, filters) {
            self.filters = filters;
        });

        report.subscribe("opencase", function (event, data) {

            event.preventDefault();
            event.stopPropagation();


            //            self.publish("executeAction", {
            //                action: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
            //                idCase: data.caseData.idCase,
            //                idWorkItem: data.caseData.idWorkItem,
            //                idTask: data.caseData.idTask
            //            });

            var data = {
                'idCase': data.caseData.idCase,
                'idWorkitem': data.caseData.idWorkItem || "",
                'idTask': data.caseData.idTask || ""
            };

            console.log("opencase: " + JSON.stringify(data));

            //bizagi.webpart.publish("render-case", data);
            return;

        });
    }
};

// Loading Reporting
reportLoader.initialize(this.args);