/*
*   Name:
*   Author: Luis Cabarique - LuisCE
*   Comments:
*   -   This script will define homePortal Framework
*/
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.webparts = (typeof (bizagi.webparts) !== "undefined") ? bizagi.webparts : {};


$.Class.extend("bizagi.workportal.homportalFramework", {}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade) {
        var self = this;

        // Set workportal facade
        self.workportalFacade = workportalFacade;
        self.device = bizagi.detectDevice();

        self._homeportalShow();
        self.configureHandler();

        bizagi.util.mobility.homePortalNavigate = $.proxy(self.homePortalNavigate, self);
        bizagi.util.mobility.homePortalViewShown = $.proxy(self.homePortalViewShown, self);
        bizagi.util.mobility.homePortalViewInit = $.proxy(self.homePortalViewInit, self);
        bizagi.util.mobility.homePortalLayoutInit = $.proxy(self.homePortalLayoutInit, self);
    },

    /**
    * finds and executes child components based on the given layout
    * @param component name of the component
    * @param content jquery content container
    * @returns {Array} promise of executing child components
    */
    loadComponents: function (component, content) {
        var self = this;
        var components = [];

        $.each(self.homePortalLayout, function (wp, v) {
            if (Array.isArray(v.position)) {
                $.each(v.position, function (index, value) {
                    if (value === component) {
                        var $canvas = $(('#' + v.canvas[index]), content);
                        var func = self.workportalFacade.executeWebpart({
                            webpart: wp,
                            canvas: $canvas,
                            pane: $canvas.parent('div[data-role="pane"]')
                        });

                        components.push(func);
                    }
                });
            } else if (v.position === component) {
                var $canvas = $(('#' + v.canvas), content);
                var func = self.workportalFacade.executeWebpart({
                    webpart: wp,
                    canvas: $canvas,
                    pane: $canvas.parent('div[data-role="pane"]')
                });

                components.push(func);
            }
        });

        return $.when(components);
    },

    /**
    * load requested template and fix the layout
    * @param templateName name of the template
    * @param component name of the component
    * @param params aditional paramethers
    * @returns {jquery} template object
    */
    loadComponentTemplate: function (templateName, component, params) {
        var self = this;
        var components = [];
        var template = kendo.template(self.workportalFacade.getTemplate(templateName), {
            useWithBlock: false
        });

        for (var wp in self.homePortalLayout) {
            if (Array.isArray(self.homePortalLayout[wp].position)) {
                $.each(self.homePortalLayout[wp].position, function (index, value) {
                    if (value === component) {
                        components.push({
                            canvas: self.homePortalLayout[wp].canvas[index]
                        });
                    }
                });
            } else if (self.homePortalLayout[wp].position === component) {
                components.push(self.homePortalLayout[wp]);
            }
        }

        return template($.extend(params, {
            "homePortalLayoutPositions": components
        }));
    },

    /**
    * Show the component
    * @private
    */
    _homeportalShow: function () {
        var self = this;

        bizagi.webpart.subscribe("homeportalShow", function (e, params) {
            params.params = params.params || {};
            var where = "";
            var openDrawer = false;

            switch (params.what) {
                case "reportsChart":
                    where = "reportsChart";
                    openDrawer = false;
                    break;
                case "newCase":
                case "menu":
                case "render":                
                    openDrawer = true;
                    break;
                default:
                    where = params.what;
                    break;
            }

            if (!openDrawer) {
                var canvas = self.homePortalLayout[where].canvas;
                var element = $('#' + canvas);

                if (element.css('display') === 'none') {
                    var parent = element.parent();
                    var pane = parent.data('kendoMobilePane');
                    pane.navigate(canvas);

                    if (params.title) {
                        var navbar = pane.view().header.find(".km-navbar").data("kendoMobileNavBar");
                        navbar.title(params.title);
                    }
                }

                self.closeDrawers();

            } else {
                self.showDrawer(params.what);
            }

            bizagi.webpart.publish('homeportalShow-' + params.what, params.params);
        });
    },

    configureHandler: function () {
        var self = this;
        bizagi.webpart.subscribe("homeportalBack", function (e, params) {
            self._back();
        });
    },

    closeDrawers: function () {
        $("#menu-drawer").data("kendoMobileDrawer").hide();
        $("#newCase-drawer").data("kendoMobileDrawer").hide();
        $("#bz-render-drawer").data("kendoMobileDrawer").hide();
    },

    showDrawer: function(drawer) {
        if (drawer === 'newCase') {
            $("#newCase-drawer").data("kendoMobileDrawer").show();
        } else if (drawer === 'menu') {
            $("#menu-drawer").data("kendoMobileDrawer").show();
        } else if (drawer === 'render') {
            $("#bz-render-drawer").data("kendoMobileDrawer").show();
        }
    },

    /**
    *
    * @param context
    */
    homePortalViewInit: function (context) {
        context.view.element.trigger("view-initialized", context.view);
    },

    homePortalLayoutInit: function (context) {

    }
});
