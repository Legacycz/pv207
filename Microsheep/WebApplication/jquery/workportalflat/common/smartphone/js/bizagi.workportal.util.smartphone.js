bizagi = (typeof (bizagi) !== 'undefined') ? bizagi : {};
bizagi.util = (typeof (bizagi.util) !== 'undefined') ? bizagi.util : {};
bizagi.util.smartphone = (typeof (bizagi.util.smartphone) !== "undefined") ? bizagi.util.smartphone : {};
bizagi.util.mobility = (typeof (bizagi.util.mobility) !== "undefined") ? bizagi.util.mobility : {};
bizagi.environment = typeof (BIZAGI_ENVIRONMENT) !== "undefined" ? BIZAGI_ENVIRONMENT : (queryString["environment"] || "debug");

bizagi.util.smartphone.startLoading = function (container) {
    bizagi.util.mobility.showLoading();
};

bizagi.util.smartphone.stopLoading = function (container) {
    bizagi.util.mobility.hideLoading();
};

bizagi.util.smartphone.startkendo = function () {

    if (typeof (bizagi.kendoMobileApplication) === "undefined") {
        bizagi.kendoMobileApplication = new kendo.mobile.Application($("body"), {
            transition: "slide",
            skin: "flat",
            initial: "initKendo",
            init: function (e) {
                bizagi.util.mobility.showLoading({ context: this });
            }
        });

        bizagi.kendoMobileApplication.bizagiNavigate = function (e, t) {
            this.navigate(e, t);
        };
    }
};

bizagi.util.smartphone.hideStatusBar = function () {

    // Hide statusBar
    if (typeof (device) != "undefined" && device.platform === 'iOS' && window.plugin && window.plugin.statusbarOverlay) {
        window.plugin.statusbarOverlay.hide();
    }
};