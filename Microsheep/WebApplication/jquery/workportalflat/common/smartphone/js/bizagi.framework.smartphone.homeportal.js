/*
*   Name:
*   Author: Luis Cabarique - LuisCE
*   Comments:
*   -   This script will define homePortal Framework
*/
bizagi = (typeof (bizagi) !== "undefined") ? bizagi : {};
bizagi.webparts = (typeof (bizagi.webparts) !== "undefined") ? bizagi.webparts : {};


bizagi.workportal.homportalFramework.extend("bizagi.workportal.homportalFramework", {}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade) {
        var self = this;
        self._super(workportalFacade);
        self.deviceType = 'smartphone';
        self.homePortalLayout = {
            "taskFeed": {
                "position": "homePortal",
                "direction": "left",
                "canvas": "taskFeed",
                "title": bizagi.localization.getResource("workportal-taskfeed-title"),
                "layout": "main-default"
            }
        };
    },

    _back: function () {
        var self = this;
        /*
        bizagi.webpart.subscribe("homeportalBack", function (e, params) {
            $('#homeportal-pane').data('kendoMobilePane').navigate('#:back');
        });*/
        self.container.data('kendoMobilePane').navigate('#:back');
    },

    configureHandler: function () {
        var self = this;
        self._super();
    },

    /**
    * navigation control
    * @param navParams
    */
    homePortalNavigate: function (navParams) {
        var destination = navParams.url;
        var canvas = navParams.sender.element;
    },

    /**
    * when view is shown
    * @param navParams
    */
    homePortalViewShown: function(navParams) {
        var self = this;
        
        self.destination = navParams.view.id.replace(/#/i, "");
        self.content = navParams.view.element;
        self.container = navParams.view.container;
    }
});