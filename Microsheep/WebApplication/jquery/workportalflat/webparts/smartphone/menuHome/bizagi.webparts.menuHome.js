/**
 *   Name: BizAgi Test
 *   Author: Oscar Osorio
 *   *   Comments:
 *   -   This script will define a hello world test
 */
bizagi.workportal.webparts.menuHome.extend("bizagi.workportal.webparts.menuHome", {}, {
    /**
     *   Constructor
     */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        self._super(workportalFacade, dataService, initialParams);
    },
    /**
     custom method for webpart
     */
    configureHandlers: function () {
        var self = this;
        self._super();
        $('.bz-db-tab', self.content).bind('click', function(){
            var tab = $(this).data('tab');
            self.publish("homeportalShow", {"what": "dashBoard", "params": {"tab": tab}});
        });
    }
});
