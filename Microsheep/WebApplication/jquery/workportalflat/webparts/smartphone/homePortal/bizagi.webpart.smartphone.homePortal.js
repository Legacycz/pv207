/**
 *   Name: BizAgi Test
 *   Author: Luis Cabarique
 *   *   Comments:
 *   -   This script will define a webpart homePortal on smartphone
 */
bizagi.workportal.webparts.homePortal.extend("bizagi.workportal.webparts.homePortal", {}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        self._super(workportalFacade, dataService, initialParams);
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        return this._super(params).done(function () {
            self.configureHandlers();
            self.showView();
        });
    },
    /**
    custom method for webparts
    */
    showView: function () {
        var self = this;

        $(self.content).kendoMobileSplitView({
            'show': function(e) {

                self.getContent().find(".km-leftitem .km-button.btn-back").hide();
                self.getContent().find(".km-leftitem .km-button.btn-menu").show();

                $(".km-rightitem", self.getContent()).unbind('click').bind('click', function () {
                    bizagi.webpart.publish("homeportalShow", { "what": "newCase" });
                });

                $(".km-leftitem .km-button.btn-menu", self.getContent()).unbind('click').bind('click', function () {
                    bizagi.webpart.publish("homeportalShow", { "what": "menu" });
                });

                self.getContent().find(".km-leftitem .km-button.btn-back").unbind('click').bind('click', function() {
                    bizagi.webpart.publish("homeportalBack");

                    self.getContent().find(".km-leftitem .km-button.btn-back").hide();
                    self.getContent().find(".km-leftitem .km-button.btn-menu").show();
                });
            }
        });
    }
});
