﻿describe("Test webpart Menu", function () {
    BIZAGI_ENABLE_LOG = true;

    var webpartMenu;
    var canvas = $("body");
    it("Executing webpart", function (done) {
        var workportal = bizagi.collection.get("workportal");


        workportal.executeWebpart({
            webpart: "menu",
            canvas: canvas
        }).done(function (wp) {
            webpartMenu = wp.webpart;

            webpartMenu.Class.authCurrentUserAdministration = true;
            webpartMenu.Class.authAnalysisReports = true;
            webpartMenu.Class.authAdmin = true;
            webpartMenu.Class.enableDebug = true;

            //tracks that the spy was called
            expect(webpartMenu.renderContent).toHaveBeenCalled();
            expect(webpartMenu.postRender).toHaveBeenCalled();
            expect(webpartMenu.configureHandlers).toHaveBeenCalled();
            expect(webpartMenu.getSecurityMenu).toHaveBeenCalled();
            expect(webpartMenu.dataService.getCurrentUser).toHaveBeenCalled();

            //tracks that the spy was called one time
            expect(webpartMenu.renderContent.calls.count()).toEqual(1);
            expect(webpartMenu.postRender.calls.count()).toEqual(1);
            expect(webpartMenu.configureHandlers.calls.count()).toEqual(1);
            expect(webpartMenu.getSecurityMenu.calls.count()).toEqual(1);
            expect(webpartMenu.dataService.getCurrentUser.calls.count()).toEqual(1);

            done();
        });

        $(document).one("bizagi-webpart-created", function (e, wp) {
            var webpart = wp.webpart;
            spyOn(webpart, 'renderContent').and.callThrough();
            spyOn(webpart, 'postRender').and.callThrough();
            spyOn(webpart, 'configureHandlers').and.callThrough();
            spyOn(webpart, 'getSecurityMenu').and.callThrough();
            spyOn(webpart, 'convertSecurityData').and.callThrough();
            spyOn(webpart.dataService, 'getCurrentUser').and.callThrough();
        });
    });

    it("Instance correct webpart", function () {
        expect(typeof webpartMenu).toEqual('object');
    });



    it("Test checkRootCategory whit undefined values", function () {
        var rs = webpartMenu.checkRootCategory({},"");
        expect(rs).toEqual(false);
    });

    describe("test action", function(){

        beforeEach(function(){
            var deferred = $.Deferred();
            spyOn(bizagi.webpart, 'publish').and.callThrough();
            spyOn(webpartMenu.dataService, 'logoutMobile').and.callFake(function () {
                setTimeout(function(){
                    deferred.resolve();
                }, 50);
                return deferred.promise();
            });
            spyOn(webpartMenu, 'defaultLogout').and.callFake(function(){
                return;
            });
            spyOn(window.location, 'replace').and.callThrough();
        });

        it("open new case", function(done){
            $.when(webpartMenu.openNewCaseAction()).done(function(){
                expect(bizagi.webpart.publish).toHaveBeenCalled();
                expect(bizagi.webpart.publish).toHaveBeenCalledWith("bz-new-case");
                done();
            });
        });


        it("log out", function(done){
            $.when(webpartMenu.logOutAction()).done(function(){
                expect(bizagi.webpart.publish).toHaveBeenCalled();
                expect(bizagi.webpart.publish).toHaveBeenCalledWith("bz-logout");
                expect(webpartMenu.dataService.logoutMobile).toHaveBeenCalled();
                done();
            });
        });
    });
});

