/*
*   Name: BizAgi Menu Search
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.menu.extend("bizagi.workportal.webparts.menu", {
    PAGE_SIZE: 30,
    SEARCH_DELAY: 700
}, {
    postRender: function (params) {
        var self = this;
        self._super(params);

        var templateContent = self.getTemplate("item-search-menu-tmpl");
        self.commandSearch = new bizagi.workportal.command.search(self.dataService);

        var listview = $(".bz-menu-search", self.canvas).kendoMobileListView({
            dataSource: new kendo.data.DataSource(),
            template: templateContent,
            selectable: "single",
            click: function (element) {
                /* istanbul ignore next */
                self.renderCase(element);
            }
        });

        self.listViewSearch = listview.data("kendoMobileListView");

        self.configureHandlers();
    },

    /*
    * Method Search data
    */
    search: function (params) {
        var self = this;

        if (params.query === self.lastProcessedValue && !params.force) {
            return;
        }

        self.lastProcessedValue = params.query;

        bizagi.util.mobility.startLoading();

        $.when(self.commandSearch.searchList(params)).done(function (data) {
            self.listViewSearch.dataSource.data(data.elements);
            bizagi.util.mobility.stopLoading();
        }).fail(function (data) {
            data = data || {};
            
            if (data && typeof (data.elements) === "undefined") {
                data = { "elements": [], "page": 1, "totalPages": 1 };
                data.elements.push({ noResults: true, message: bizagi.localization.getResource("workportal-menu-search-found-no-cases") });
            }
            
            self.listViewSearch.dataSource.data(data.elements);
            bizagi.util.mobility.stopLoading();
        });
    },

    /**
    *  Render the case
    */
    renderCase: function(params) {
        var self = this;
        var caseData = self.commandSearch.getCaseData(params);

        if (!bizagi.util.isEmpty(caseData)) {
            $(".bz-wp-mu-button-search", self.canvas).click();

            self.publish("changeCase", caseData);
        }
    },    

    /*
    * Custom method for webpart
    */
    configureHandlers: function () {
        var self = this;

        var drawer = $("#menu-drawer", self.canvas);
        var drawerData = drawer.data("kendoMobileDrawer");

        // Event Search
        $(".bz-wp-mu-input-search-menu", self.canvas).bind("click", function (e) {

            if (typeof (self.originalWidth) === "undefined") {
                self.originalWidth = drawerData.getSize().width;
            }

            if (typeof (self.searchWidth) == "undefined") {
                self.searchWidth = $(".bz-wp-mu-input-search-menu").width();
            }

            drawerData.element.width(window.innerWidth);
            drawerData.show();

            $(".bz-wp-mu-input-search-menu", self.canvas).animate({ width: "75%" });
            $(".bz-menu", self.canvas).fadeOut(undefined, undefined, function () {
                $(".bz-menu-search", self.canvas).fadeIn();
                $(".bz-wp-mu-button-search").show();
            });

        }).keyup(function (e) {
            if (e.which === 13) {
                self.search({ query: this.value, force: true });
                return;
            } else {
                // if (e.which !== 13 && this.value.toString().length >= 3) {
                var that = this;
                setTimeout(function (e) {
                    self.search({ query: that.value });
                }, self.Class.SEARCH_DELAY);
                return;
            }
        });

        // Event cancel button        
        $(".bz-wp-mu-button-search", self.canvas).bind("click", function (e) {

            self.lastProcessedValue = null;
            self.listViewSearch.dataSource.data([]);
            $(".bz-wp-mu-input-search-menu", self.canvas).val("");

            drawerData.element.width(self.originalWidth);
            drawerData.hide();

            setTimeout(function () {
                $(".bz-menu", self.canvas).show();
                $(".bz-menu-search", self.canvas).hide();
                $(".bz-wp-mu-button-search").hide();
                $(".bz-wp-mu-input-search-menu", self.canvas).animate({ width: self.searchWidth });
            }, 500);
        });
    }
});
