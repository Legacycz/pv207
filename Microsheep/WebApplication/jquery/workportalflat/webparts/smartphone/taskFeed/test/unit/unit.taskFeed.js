﻿
describe('Testing webpart Task Feed.', function () {
    'use strict';

    var webpartTaskFeed;
    var canvas = $('body');

    it('Executing webpart', function (done) {
        var workportal = bizagi.collection.get('workportal');

        $(document).one('bizagi-webpart-created', function (e, wp) {
            var webpartTaskFeed = wp.webpart;

            spyOn(webpartTaskFeed, 'renderContent').and.callThrough();
            spyOn(webpartTaskFeed, 'postRender').and.callThrough();
            spyOn(webpartTaskFeed, 'configureHandlers').and.callThrough();
            spyOn(webpartTaskFeed, 'showView').and.callThrough();
            spyOn(webpartTaskFeed, 'updateDataListByPullRefresh').and.callThrough();

        });

        workportal.executeWebpart({
            webpart: 'taskFeed',
            canvas: canvas
        }).done(function (wp) {
            webpartTaskFeed = wp.webpart;

            // Tracks that the spy was called
            expect(webpartTaskFeed.renderContent).toHaveBeenCalled();
            expect(webpartTaskFeed.postRender).toHaveBeenCalled();
            expect(webpartTaskFeed.configureHandlers).toHaveBeenCalled();

            // Tracks that the spy was called one time
            expect(webpartTaskFeed.renderContent.calls.count()).toEqual(1);
            expect(webpartTaskFeed.postRender.calls.count()).toEqual(1);
            expect(webpartTaskFeed.configureHandlers.calls.count()).toEqual(1);
            expect(webpartTaskFeed.showView.calls.count()).toEqual(1);
            expect(webpartTaskFeed.updateDataListByPullRefresh.calls.count()).toEqual(0);

            done();
        });
    });

    it('Test instance correct webpart', function () {
        expect(typeof webpartTaskFeed).toEqual('object');
    });

    it('Call to click events that make a publish', function () {
        $('.km-rightitem', webpartTaskFeed.getContent()).trigger('click');
        $('.km-leftitem', webpartTaskFeed.getContent()).trigger('click');
    });

    it('Call to click cases in list view control', function (done) {

        expect($('#modalview-summary').parent().css('display')).toEqual('none');

        webpartTaskFeed.showSummary({
            dataItem: [61457,
                66807,
                373,
                '05/07/2014 14:53',
                'DemoECMUpload',
                'Task 7',
                [
                    [
                        'Caso No',
                        '61457'
                    ],
                    [
                        'Proceso',
                        'DemoECMUpload'
                    ],
                    [
                        'Fecha creación proceso',
                        '05/07/2014 14:53'
                    ]
                ]]
        });

        expect($('#modalview-summary').parent().css('display')).toEqual('block');

        $('#modalview-summary', canvas).find('#closeModalView').click();

        setTimeout(function () {
            expect($('#modalview-summary').parent().css('display')).toEqual('none');

            webpartTaskFeed.showSummary({
                dataItem: [61457,
                    66807,
                    373,
                    '05/07/2014 14:53',
                    'DemoECMUpload',
                    'Task 7',
                    [
                        [
                            'Caso No',
                            '61457'
                        ],
                        [
                            'Proceso',
                            'DemoECMUpload'
                        ],
                        [
                            'Fecha creación proceso',
                            '05/07/2014 14:53'
                        ]
                    ]]
            });

            $('#modalview-summary', canvas).find('#WorkOnItModalView').click();
            $('#modalview-summary', canvas).find('#closeModalView').click();
            done();
        }, 500);
    });

    it('Call load more cases from click event', function (done) {
        $('#loadMoreTaskFeed', webpartTaskFeed.getContent()).trigger('click');

        $('#loadMoreTaskFeed', webpartTaskFeed.getContent()).trigger('click', function () {
            expect($('div.bz-wp-item-container', webpartTaskFeed.getContent()).length).toEqual((webpartTaskFeed.properties.pageSize * 3));
            done();
        });
    });

    it('Call publish to ShowView', function () {
        webpartTaskFeed.publish('bz-inbox');
    });

    it('Call publish to update list cases', function () {
        var params = { updateDataList: true };

        webpartTaskFeed.publish('bz-update-list-cases', params);
    });

    it('load more cases from method', function (done) {
        webpartTaskFeed.properties.page += 1;

        expect($('#loadMoreTaskFeed', webpartTaskFeed.getContent()).css('display')).toEqual('block');

        webpartTaskFeed.loadDataList().then(function () {
            expect($('#loadMoreTaskFeed', webpartTaskFeed.getContent()).css('display')).toEqual('block');

            done();
        });
    });

    it('Click on item from ListWiew', function (done) {
        var listview = $('#items-content-taskFeed', webpartTaskFeed.getContent()).data('kendoMobileListView');

        expect($('#modalview-summary').parent().css('display')).toEqual('none');

        listview.items().first().mousedown().mouseup();

        setTimeout(function () {
            expect($('#modalview-summary').parent().css('display')).toEqual('block');

            done();
        }, 700);
    });

    it('Update DataList By PullRefresh', function(done) {       
        var params = { numberOfFields: 2, page: 2, pageSize: 10, idWfClass: -1 };
        webpartTaskFeed.updateDataListByPullRefresh(params).then(function(data) {
            console.log('Update data');

            done();
        });

    });

});
