/**
*   Name: BizAgi Webpart taskFeed
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to task feed
*/

bizagi.workportal.webparts.taskFeed.extend("bizagi.workportal.webparts.taskFeed", {}, {
    /**
     * Constructor
     * @param {} workportalFacade 
     * @param {} dataService 
     * @param {} initialParams 
     * @returns {} 
     */
    init: function (workportalFacade, dataService, initialParams) {
        // Call base
        this._super(workportalFacade, dataService, initialParams);
    },
    /**
     * Renders the content for the current controller
     * @param {} params 
     * @returns {} 
     */
    renderContent: function (params) {
        return this._super(params);
    },
    /**
     * Customize the web part in each device
     * @param {} params 
     * @returns {} 
     */
    postRender: function (params) {
        var self = this;
	
        self._super(params);
        bizagi.webpart.subscribe("homeportalShow-taskFeed", function (e, params) {
            self.initTaskFeedKendoView();
            self.updateDataList();
        });
    },

    /**
     * Event Handlers
     * @returns {} 
     */
    configureHandlers: function () {
        var self = this;

        /* NewCase action */
        self.getContent().find(".km-rightitem").click(function () {
            bizagi.webpart.publish("bz-new-case");
        });

        // Call Super
        self._super();
    },

    /**
     * Configure ModalView Handlers
     * @returns {} 
     */
    configureModalViewHandlers: function () {
        var self = this;

        // #closeModalView
        $("#modalview-summary").delegate(".bz-wp-modal-newcase-footer .km-leftitem", "click", function (e) {
            $("#modalview-summary").data("kendoMobileModalView").close();
        });

        // #WorkOnItModalView
        $("#modalview-summary").delegate(".bz-wp-modal-newcase-footer .km-rightitem", "click", function (e) {
            var item = $("#WorkOnItModalView").data("item");
            var radNumber = null;

            if (item[6] && item[6][0] && item[6][0][1]) {
                radNumber = item[6][0][2] !== "DateTime" ? item[6][0][1] : null;
            }

            bizagi.webpart.publish("changeCase", {
                idCase: item[0],
                idWorkitem: item[1],
                idTask: item[2],
                displayName: item[5],
                radNumber: radNumber
            });
        });

        /* Task as Favorite */
        $("#modalview-summary").delegate(".bz-favorite-case", "click", function (e) {
            self.setFavorite(this, $(".bz-wp-modal-content", $(this).parents("#modalview-summary")).data("case"));
        });
    },

    /**
     * Show Summary task feed
     * @param {} params 
     * @returns {} 
     */
    showSummary: function (params) {

        var self = this;
        var data = params.dataItem;
        var caseRadNumber = params.dataItem[0];

        var customColumnsParams = {
            'mobileDevice': true,
            'idWorkflow': params.dataItem[7]
        };

        if (bizagi.util.isNumeric(caseRadNumber)) {
            customColumnsParams["idCase"] = caseRadNumber;
        } else {
            customColumnsParams["radNumber"] = caseRadNumber;
        }

        bizagi.util.mobility.startLoading();

        $.when(self.dataService.getCasesByWorkflow(customColumnsParams)).done(function (casesData) {

            for (var i = 0, lengthRow = casesData.rows.length; i < lengthRow; i++) {
                var caseDataRow = casesData.rows[i];
                if (caseDataRow.id == data[0]) {
                    data[6] = [];

                    if (caseDataRow.radnumber && caseDataRow.casenumberdisplayname) {
                        data[6].push([caseDataRow.casenumberdisplayname, caseDataRow.radnumber]);
                    }

                    for (var j = 0, lengthField = caseDataRow.fields.length; j < lengthField; j++) {

                        if (typeof (caseDataRow.fields[j].Value) == "object") {

                            for (var k = 0, lengthItem = caseDataRow.fields[j].Value.length; k < lengthItem; k++) {

                                var arrayField = caseDataRow.fields[j].Value[k];
                                if (arrayField !== "") {
                                    data[6].push([caseDataRow.fields[j].DisplayName, arrayField, caseDataRow.fields[j].DataType]);
                                    break;
                                }
                            }
                        } else {
                            data[6].push([caseDataRow.fields[j].DisplayName, caseDataRow.fields[j].Value, caseDataRow.fields[j].DataType]);
                        }
                    }
                    break;
                }
            }

            var templateSummary = kendo.template(self.getTemplate("summary-taskFeed-kendo-tmpl"), { useWithBlock: false, useNativeScrolling: true });
            var summaryModal = $(templateSummary(data));

            // Remove element of DOM 
            summaryModal.kendoMobileModalView({
                close: function (args) {

                    this.destroy();
                    this.close();
                    this.element.remove();
                }
            });

            // Configure Handler modal summary case
            self.configureModalViewHandlers();

            // footer 48 | header  48 | autosize            
            $("#WorkOnItModalView").data("item", data);
            $(".bz-favorite-case", "#modalview-summary").attr("data-id-object", self.cacheFavorites[data[0]] ? self.cacheFavorites[data[0]].idFavorites : data[8]);

            self.isFavorite($(".bz-favorite-case", "#modalview-summary"));
            var modalView = $("#modalview-summary").data("kendoMobileModalView");

            modalView.open();

            /* istanbul ignore next: untestable */
            modalView.adjustSize = function () {
                // Adjust summary popup to window size            
                var listHeight = $("ul.bz-wp-modal-list", this.element).height();
                var maxHeight = 0;
                var windowHeightSize = window.innerHeight;
                var popupHeight = $(".km-header", this.wrapper).height() + $(".km-footer", this.wrapper).height();

                if (listHeight + popupHeight >= windowHeightSize) {
                    maxHeight = windowHeightSize - 20;
                } else {
                    maxHeight = listHeight + popupHeight + 10;
                }

                var top = ((windowHeightSize - maxHeight) / 2);

                this.wrapper.parent().css("top", top);
                this.wrapper.parent().css("height", maxHeight);
            };

            modalView.adjustSize();

            /* istanbul ignore next: untestable */
            window.addEventListener("orientationchange", function () {
                // Desbindiar
                if (bizagi.util.isAndroid()) {
                    setTimeout(function () {
                        modalView.adjustSize();
                    }, 500);
                } else {
                    modalView.adjustSize();
                }
            }, false);

            bizagi.util.mobility.stopLoading();

        }).fail(function (error) {
            console.log(error.responseText);

            bizagi.util.mobility.stopLoading();
        });
    }
});