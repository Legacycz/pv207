/*
 *   Name: BizAgi Test
 *   Author: oscaro
 *   Comments:
 *   -   This script will define a base class to all widgets
 */

bizagi.workportal.webparts.render.extend("bizagi.workportal.webparts.render", {
}, {
    renderCase: function (params) {
        var self = this;
        bizagi.webpart.publish("bz-summary-case", params);

        return self._super(params);
    },

    setButtonsHandler: function() {
        $(".bz-wp-render-summary").bind("click", function () {            
            bizagi.webpart.publish("homeportalShow", { "what": "render" });
        });
    }
});
