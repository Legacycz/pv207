/**
*   Name: BizAgi Test
*   Author: Richar Urbano - RicharU (based LuisCE)
*   *   Comments:
*   -   This script will define a homePortal
*/
bizagi.workportal.webparts.homePortal.extend("bizagi.workportal.webparts.homePortal", {}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        self._super(workportalFacade, dataService, initialParams);
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        return this._super(params)
            .done(function () {
                // Enable menu offline
                if (self.hasOfflineForm) {
                    self.renderMenuOptions();
                }

                self.configureHandlers();
                self.showView();
            });
    },

    /**
    custom method for webparts
    */
    showView: function () {
        var self = this;

        $(self.content).kendoMobileSplitView({
            'show': function () {
                $("#right-pane", self.content).find(".km-rightitem").unbind('click').bind('click', function () {
                    bizagi.webpart.publish("homeportalShow", { "what": "newCase" });
                });

                $("#left-pane", self.content).find(".km-leftitem").unbind('click').bind('click', function () {
                    bizagi.webpart.publish("homeportalShow", { "what": "menu" });
                });

                if (self.hasOfflineForm) {
                    $("#left-pane", self.content).find(".km-rightitem").unbind('click');
                    $("#left-pane", self.content).find(".km-rightitem").bind('click', function () {
                        var content = $('.bz-wp-home-offline-menu-container', self.content);
                        if (content.css('display') == 'none') {
                            content.fadeIn();
                        } else {
                            content.fadeOut();
                        }
                    });
                }
            }
        });
    },
    /**
    * custom method for webpart
    */
    configureHandlers: function () {
        var self = this;
        var context = self.getContent();

        self.status = $(".bz-wp-status-notification", context);
        self.tittle = $(".bz-wp-status-title", context);

        if (self.dataService.online == false) {
            self.setStatusOffline();
        }

        $(document).off("online.inbox");
        $(document).on("online.inbox", function () {

            self.dataService.online = true;

            self.setStatusOnline();
            self.inputTrayManager("inbox");

            // Enable inbox
            self.menuOffline.find("[data-tray=inbox]").removeClass("bz-wp-offline-menu-opacity");

        });

        $(document).off("offline.inbox");
        $(document).on("offline.inbox", function () {
            self.dataService.online = false;

            self.setStatusOffline();
            self.inputTrayManager("drafts");

            // Disable inbox
            self.menuOffline.find("[data-tray=inbox]").addClass("bz-wp-offline-menu-opacity");

        });
    },
    /**
    * Enable menu offline
    */
    renderMenuOptions: function () {
        var self = this;

        // Add menu options
        var template = kendo.template(self.getTemplate('menu-offline-tmpl'), { useWithBlock: false });
        $(".bz-wp-home-offline-menu-container ul", self.content).remove();
        $(".bz-wp-home-offline-menu-container", self.content).append(template({ online: self.dataService.online }));

        self.menuOffline = $(".bz-wp-home-offline-menu-list", self.content).kendoMobileListView({
            click: function (data) {
                var item = data.item;
                var mode = $(item).data("tray");

                data.preventDefault();

                // Verified offline
                if (!self.dataService.online && mode.trim() === "inbox") return;

                // Manager of inputTray
                self.inputTrayManager(mode);

                // Select current item on menu offline 
                $("#left-pane", self.content).find(".km-rightitem").click();
            }
        });

        // Select current item on menu offline
        var currentItem = self.dataService.online ? "inbox" : "drafts";
        self.inputTrayManager(currentItem);
    },

    inputTrayManager: function (mode) {
        var self = this;
        var inputTray = self.modeInbox;
        var title = "Task feed";

        switch (mode) {
            case "outbox":
                inputTray = "false";
                title = bizagi.localization.getResource("workportal-menu-outbox");
                break;
            case "drafts":
                inputTray = "true";
                title = bizagi.localization.getResource("workportal-menu-drafts");
                break;
            case "inbox":
                inputTray = "inbox";
                title = bizagi.localization.getResource("workportal-taskfeed-title");
                break;
        }

        // Update Title TaskFeed
        $('#taskFeed', self.content).find(".km-view-title").children().text(title);

        $('.bz-offline-selected', self.content).removeClass("bz-offline-selected");
        self.menuOffline.find("[data-tray=" + mode + "]").addClass("bz-offline-selected");

        bizagi.util.setItemLocalStorage("inputtray", inputTray);

        // Update TaskFeed
        self.publish("bz-update-list-cases", { updateDataList: true });
        bizagi.webpart.publish("bz-summary-item-clear");
    },

    setStatusOffline: function () {
        var self = this;

        self.status.removeClass("online");
        self.status.addClass("offline");
        self.tittle.text(self.getResource("workportal-offline-status-offline"));        
    },

    setStatusOnline: function () {
        var self = this;

        self.status.removeClass("offline");
        self.status.addClass("online");
        self.tittle.text(self.getResource("workportal-offline-status-online"));
    }
});
