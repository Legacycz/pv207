﻿//Unit test for webpart hello

describe('Testing webpart Hello', function () {
    'use strict';
    var webpartHello;

    it('Executing webpart', function (done){
        var workportal = bizagi.collection.get('workportal');

        $(document).one('bizagi-webpart-created', function(e, wp){
            webpartHello = wp.webpart;
            spyOn(webpartHello, 'renderContent').and.callThrough();
            spyOn(webpartHello, 'postRender').and.callThrough();
            spyOn(webpartHello, 'configureHandlers').and.callThrough();
            spyOn(webpartHello, 'showView').and.callThrough();
        });

        workportal.executeWebpart({
            webpart: 'hello',
            canvas: $('body')
        }).done(function (wp) {
            webpartHello = wp.webpart;
            //tracks that the spy was called
            expect(webpartHello.renderContent).toHaveBeenCalled();
            expect(webpartHello.postRender).toHaveBeenCalled();
            expect(webpartHello.configureHandlers).toHaveBeenCalled();

            //tracks that the spy was called one time
            expect(webpartHello.renderContent.calls.count()).toEqual(1);
            expect(webpartHello.postRender.calls.count()).toEqual(1);
            expect(webpartHello.configureHandlers.calls.count()).toEqual(1);
            expect(webpartHello.showView.calls.count()).toEqual(1);

            done();
        });


    });

    it('This webpart is an Object', function () {
        expect(typeof webpartHello).toEqual('object');
    });

    it('Test of click event dispatch', function () {
        $('.km-list > li', webpartHello.getContent()).trigger( 'click' );
        expect($('.km-list > li', webpartHello.getContent()).length).toBeGreaterThan(1);
    });
});