/*
*   Name: Webpart menu for tablet
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will define a base class of webpart menu on tablet
*/
bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.menu", {
    DEVICE_TABLET_IOS: "tablet",
    DEVICE_TABLET_ANDROID: "tablet_android",
    authCurrentUserAdministration: false,
    authAnalysisReports: false,
    authAdmin: false,
    enableLogOut: true,
    enableSettings: false
}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        self._super(workportalFacade, dataService, initialParams);

        // TODO: Review Reports
        self.reports = new bizagi.workportal.widget.reportsMenu(workportalFacade, dataService, initialParams);
    },

    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        deferred.resolve("");

        return deferred.promise();
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        $.when(self.renderMenuItems())
            .done(function (content) {
                // Render content      
                self.contentInternal = self.content = content;

                $("#content-menu-drawer .km-scroll-container", self.canvas).append(self.contentInternal);
                $(".bz-wp-menu-list", self.canvas).kendoMobileListView({ style: "inset" });
                $(".bz-wp-menu-list li", self.canvas).first().addClass('bz-menu-selected');

                self.configureHandlers();
            });
    },

    renderMenuItems: function () {
        var self = this;
        var device = bizagi.detectDevice();
        var deferred = $.Deferred();
        var template = kendo.template(self.getTemplate('menu-tmpl'), { useWithBlock: false });

        // Call security command
        self.security = new bizagi.workportal.command.security(self.dataService);
        
        $.when(self.security.getSecurity())
            .done(function (dataSecurity) {

                // Enable native preferences for tablet Android                
                var enableSettings = (device == self.Class.DEVICE_TABLET_ANDROID) && typeof (cordova) != "undefined";

                /* overrides for default menu */
                dataSecurity.authEnableSettings = enableSettings || self.Class.enableSettings;
                dataSecurity.authEnableLogOut = self.Class.enableLogOut;

                // Disable administration menu items on this release
                dataSecurity.CurrentUserAdministration = false;
                dataSecurity.Admin = false;

                // Offline
                if (self.dataService.online == false) {
                    dataSecurity.authAnalysisReports = false;
                } else {
                    // Call render reports items
                    self.reports.initialize();
                }

                // Render template
                self.contentInternal = template($.extend(dataSecurity, bizagi.currentUser));

                deferred.resolve(self.contentInternal);
            });

        return deferred.promise();
    },

    /**
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        $(".bz-wp-menu-list li", self.canvas).bind('click', function (e) {
            e.preventDefault();
            self.triggerMenuAction(this);
        });

        bizagi.webpart.subscribe("bz-menu", function () {
            $("#menu-drawer").data("kendoMobileDrawer").show();
        });
    },

    /**
    *   Customize the web part in each device
    */
    triggerMenuAction: function (param) {
        var self = this;
        var action = $(param, self.canvas).data("bz-menu-action");

        switch (action) {
            case "taskfeed":
                bizagi.webpart.publish('homeportalShow', { "what": "taskFeed" });
                break;
            case "newCase":
                bizagi.webpart.publish('homeportalShow', { "what": "newCase" });
                break;
            case "reports":
                self.renderReportsMenu();
                break;
            case "preferences":
                self.preferenceAction();
                break;
            case "logout":
                self.logOutAction();
                break;
        }

        $('.bz-menu-selected', self.canvas).removeClass("bz-menu-selected");
        $(param).addClass("bz-menu-selected");
    },

    /**
    *   Logout execute
    */
    logOutAction: function () {
        var self = this;
        var defer = new $.Deferred();

        bizagi.webpart.publish("bz-logout");

        if (self.dataService.online == false) {
            var resp = window.confirm(self.resources.getResource("confirmation-savebox-message3"));
            if (resp == true) {
                self.defaultLogout();
                defer.resolve();
            }
        } else {
            $.when(self.dataService.logoutMobile())
                .always(function (response) {
                    self.defaultLogout();
                    defer.resolve();
                });
        }

        return defer.promise();
    },

    /**
    *   Customize the web part in each device
    */
    defaultLogout: function () {
        /* istanbul ignore next: untestable */
        if (typeof (cordova) !== "undefined") {
            window.location = bizagi.services.ajax.logoutPage;
        } else {
            window.location.replace('');
        }
    },

    /**
    *   Launch preferences on Android device 
    */
    preferenceAction: function () {
        var self = this;

        if (bizagi.detectDevice() == self.Class.DEVICE_TABLET_ANDROID && typeof (cordova) != "undefined") {
            console.log("Show Android preferences");
            window.Bizagi.showSettingsActivity();
        }
    },

    /* REPORTS
    =================================================*/

    /**
    *   Show Report Menu
    */
    renderReportsMenu: function () {
        var self = this;
        var context = $('.bz-wp-list-items', self.canvas);

        if ($(context).css('display') == 'none') {
            $(context).fadeIn();
        } else {
            $(context).fadeOut();
        }
    }
});
