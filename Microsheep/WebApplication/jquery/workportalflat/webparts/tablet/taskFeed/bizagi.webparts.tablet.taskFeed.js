/**
*   Name: BizAgi Webpart taskFeed
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will define a base class to task feed
*/

bizagi.workportal.webparts.taskFeed.extend("bizagi.workportal.webparts.taskFeed",
{
    PAGE_SIZE: 10,
    SEARCH_PAGE_SIZE: 30,
    SEARCH_DELAY: 700
}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;        

        // Call base
        self._super(workportalFacade, dataService, initialParams);

        // Search on TaskFeed
        self.taskFeedSearch = new bizagi.workportal.webparts.taskFeed.search(workportalFacade, dataService, initialParams);        

        /* Override kendo behaviour, it set a specific height size on ios, generating visual problems*/
        if (bizagi.detectSO() == "ios") {
            window.addEventListener("orientationchange", function () {
                $("body").css("height", "auto");
            }, false);
        }
    },

    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        return this._super(params);
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        self._super(params);

        self.dataSource = new kendo.data.DataSource({
            group: { field: "group" }
        });

        $(document).bind('webpartsLoaded', function (params) {
            self.initTaskFeedKendoView();

            self.taskFeedSearch.initSearchKendoView();

            if (!self.initTaskfeed) {
                self.updateDataList();
            }
        });

        bizagi.webpart.subscribe('homeportalShow-taskFeed', function (e, params) {
            self.taskFeedSearch.initSearchKendoView();

            self.initTaskFeedKendoView();
            self.updateDataList();
        });
    },

    /**
    * Show Summary task feed
    */
    showSummary: function (params) {

        var self = this;
        params = params.dataItem || {};

        bizagi.util.tablet.startLoading();

        // Call webpart summary item
        bizagi.webpart.publish("bz-summary-item", params);
    }
});