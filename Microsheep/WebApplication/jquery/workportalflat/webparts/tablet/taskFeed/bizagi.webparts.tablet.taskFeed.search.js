﻿/*
*   Name: BizAgi TaskFeed Search
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will define a base class to all widgets
*/

$.Class.extend("bizagi.workportal.webparts.taskFeed.search", {
    SEARCH_PAGE_SIZE: 30,
    SEARCH_DELAY: 700
},
    {
        /*
        *   Constructor
        */
        init: function (workportalFacade, dataService, initialParams) {
            var self = this;

            // self.initSearch = true; TODO: Review one instance

            self.canvas = initialParams.canvas;
            self.dataService = dataService;
            self.workportalFacade = workportalFacade;

            self.commandSearch = new bizagi.workportal.command.search(dataService);

            self.isTabletDevice = bizagi.util.isTabletDevice();
            self.enableOfflineForm = bizagi.util.hasOfflineForm();
        },

        /**
        *   Renders the content for the current controller
        */
        renderContent: function (params) {
            var self = this;
            var deferred = $.Deferred();

            self.content = "";
            deferred.resolve(self.content);

            return deferred.promise();
        },

        initSearchKendoView: function () {
            var self = this;

            var templateContent = self.workportalFacade.getTemplate('item-search-kendo-tmpl');

            var listview = $('.bz-wp-tf-search-list', self.canvas).kendoMobileListView({
                dataSource: new kendo.data.DataSource(),
                template: templateContent,
                selectable: 'single',
                click: function (element) {
                    /* istanbul ignore next */
                    self.renderCase(element);
                }
            });

            self.listViewSearch = listview.data('kendoMobileListView');
            self.configureHandlers();
        },

        /*
        * Custom method for webpart
        */
        configureHandlers: function () {
            var self = this;


            $('.bz-wp-tf-search-input', self.canvas).bind('click', function (e) {

                if ($('.bz-wp-tf-list-container', self.canvas).css('display') == 'block') {
                    // Clear content data on summary items of taskFeed
                    bizagi.webpart.publish("bz-summary-item-clear");

                    // Hidden Listview taskFeed            
                    $('.bz-wp-tf-list-container', self.canvas).fadeOut(undefined, undefined, function () {
                        // Show Search list                
                        $('.bz-wp-tf-search-list', self.canvas).fadeIn();
                    });

                    //if paginator is visible
                    self.isPaginatorActive = $(".bz-wp-taskfeed-paginator-container").data("paginator") || false;
                    if (self.isPaginatorActive == true) {
                        $(".bz-wp-taskfeed-paginator-container").css("display", "none");
                    }
                }
            }).keyup(function (event) {

                // Show cancel option
                if ($('.bz-wp-tf-cancel-icon').css('display') == 'none') {
                    $('.bz-wp-tf-cancel-icon', self.canvas).show();
                }

                if (event.which == 13) {
                    self.search({ query: this.value, force: true });
                    return;
                }

                if (event.which !== 13 && this.value.toString().length >= 3) {
                    var that = this;
                    setTimeout(function (e) {
                        self.search({ query: that.value });
                    }, self.Class.SEARCH_DELAY);
                    return;
                }

                if (event.which == 8 && this.value == '') {

                    self.lastProcessedValue = null;
                    self.listViewSearch.dataSource.data([]);
                    $('.bz-wp-tf-search-input', self.canvas).val('');

                    setTimeout(function () {
                        $('.bz-wp-tf-cancel-icon', self.canvas).hide();
                        $('.bz-wp-tf-list-container', self.canvas).fadeIn();
                    }, 500);
                }
            }).on('focusout', function (event) {

                event.stopPropagation();
                event.preventDefault();

                if ((event.type === 'blur' || event.type === 'focusout') && this.value.length === 0) {
                    setTimeout(function () {
                        $('.bz-wp-tf-cancel-icon', self.canvas).hide();
                        $('.bz-wp-tf-list-container', self.canvas).fadeIn();

                        //if paginator is visible
                        if (self.isPaginatorActive == true) {
                            $(".bz-wp-taskfeed-paginator-container").css("display", "block");
                        }
                    }, 500);
                }
            });

            // Event cancel button        
            $('.bz-wp-tf-cancel-icon', self.canvas).bind('click', function (e) {

                self.lastProcessedValue = null;
                self.listViewSearch.dataSource.data([]);
                $('.bz-wp-tf-search-input', self.canvas).val('');

                setTimeout(function () {
                    $('.bz-wp-tf-cancel-icon', self.canvas).hide();
                    $('.bz-wp-tf-list-container', self.canvas).fadeIn();

                    if (self.isPaginatorActive == true) {
                        $(".bz-wp-taskfeed-paginator-container").css("display", "block");
                    }
                }, 500);
            });
        },

        /*
        * Method Search data
        */
        search: function (params) {
            var self = this;

            if (params.query === self.lastProcessedValue && !params.force) {
                return;
            }

            self.lastProcessedValue = params.query;

            bizagi.util.mobility.startLoading();

            $.when(self.commandSearch.searchList(params)).done(function (data) {
                self.listViewSearch.dataSource.data(data.elements);
                bizagi.util.mobility.stopLoading();
            }).fail(function (data) {
                data = data || {};
                
                if (data && typeof(data.elements) === 'undefined') {
                    data = { 'elements': [], 'page': 1, 'totalPages': 1 };
                    data.elements.push({ noResults: true, message: bizagi.localization.getResource('workportal-menu-search-found-no-cases') });
                }
                
                self.listViewSearch.dataSource.data(data.elements);
                bizagi.util.mobility.stopLoading();
            });
        },

        renderCase: function (params) {
            var self = this;
            var caseData = self.commandSearch.getCaseData(params);

            if (!bizagi.util.isEmpty(caseData)) {
                // Clean Data search
                $('.bz-wp-tf-cancel-icon', self.canvas).click();

                // Close Keyboard
                if (typeof (cordova) !== "undefined" && self.isTabletDevice) {
                    cordova.plugins.Keyboard.close();
                }

                // Call webpart summary item
                bizagi.webpart.publish("changeCase", caseData);

                //if paginator is visible
                if (self.isPaginatorActive == true) {
                    setTimeout(function () {
                        $(".bz-wp-taskfeed-paginator-container").css("display", "block");
                    }, 1000);
                }
            }

        }
    });
