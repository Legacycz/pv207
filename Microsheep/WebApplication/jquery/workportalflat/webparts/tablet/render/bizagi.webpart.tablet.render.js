/*
 *   Name: BizAgi Test
 *   Author: oscaro
 *   Comments:
 *   -   This script will define a base class to all widgets
 */

bizagi.workportal.webparts.render.extend("bizagi.workportal.webparts.render", {
}, {

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;
        self._super(params);

        self.summary = self.workportalFacade.executeWebpart({
            webpart: "summaryCase",
            canvas: $("#bz-render-drawer", self.content)
        });
    },

    renderCase: function (params) {
        var self = this;

        $.when(self.summary).done(function () {
            bizagi.webpart.publish("bz-summary-case", params);
        });

        return self._super(params);
    },

    setButtonsHandler: function() {
        $(".bz-wp-render-summary").bind("click", function() {

            bizagi.webpart.publish("homeportalShow", { "what": "render" });
        });
    }
});
