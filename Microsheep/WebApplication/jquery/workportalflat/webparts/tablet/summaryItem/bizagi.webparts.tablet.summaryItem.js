/*
*   Name: Webpart summaryItem for tablet
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will define a base class of webpart summaryItem on tablet
*/
bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.summaryItem", {
}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        self._super(workportalFacade, dataService, initialParams);

        // Offline    
        self.isTabletDevice = bizagi.util.isTabletDevice();
        self.enableOfflineForm = bizagi.util.hasOfflineForm();
        self.online = self.isTabletDevice ? self.dataService.online : true;

        // Load Templates
        self.summaryItemTemplate = kendo.template(self.getTemplate('summary-item-main'), { useWithBlock: false });
        self.summaryItemTemplateContent = kendo.template(self.getTemplate('summary-item-content'), { useWithBlock: false });
    },
    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        self.content = self.summaryItemTemplate({});
        deferred.resolve(self.content);

        return deferred.promise();
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        // Configure handlers to click events
        self.configureHandlers();

    },

    /*
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        bizagi.webpart.subscribe('bz-summary-item', function (e, params) {
            return self.renderSummary(params);
        });

        $("#bz-wp-summary-item-list", self.content).delegate("#bz-wp-sm-item-remove", "click", function (e) {
            e.preventDefault();
            e.stopPropagation();

            var item = $('.bz-wp-sm-item', self.content).data("item");

            var confirmationMsg = bizagi.localization.getResource("workportal-widget-inbox-delete-case-title");

            $.when(bizagi.showConfirmationBox(confirmationMsg, "Bizagi", "warning"))
                .done(function () {
                    $.when(self.dataService.deleteCase({ 'idCase': item[0] }))
                        .done(function () {
                            // Update TaskFeed
                            self.publish("bz-update-list-cases", { updateDataList: true });
                            bizagi.webpart.publish("bz-summary-item-clear");

                        }).fail(function (error) {
                            bizagi.log(error);
                        });
                });
        });

        $("#bz-wp-summary-item-list", self.content).delegate("#bz-wp-sm-item-workonit", "click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var item = $('.bz-wp-sm-item', self.content).data("item");

            // Call Clear content data
            self.clearContent({ clear: true });

            // RadNumber
            var radNumber = null;
            if (item[6] && item[6][0] && item[6][0][1]) {
                radNumber = item[6][0][2] !== "DateTime" ? item[6][0][1] : null;
            }

            // Render conntent summary
            bizagi.webpart.publish("changeCase", {
                idCase: item[0],
                idWorkitem: item[1],
                idTask: item[2],
                displayName: item[5],
                isOfflineForm: item[7] && item[7] === true || false,
                formsRenderVersion: item[7] && item[7] === true ? 2 : 0,
                radNumber: radNumber
            });
        });

        // Clear content data
        bizagi.webpart.subscribe('bz-summary-item-clear', function (e, params) {
            // Call Clear content data
            self.clearContent({ clear: true });
        });
    },

    /*
    *   Renders the summary component of the webpart
    */
    renderSummary: function (params) {
        var self = this;
        var defer = new $.Deferred();

        self.params = params || {};

        // Clear content data       
        self.clearContent({ clear: false });

        var caseRadNumber = params[0];

        var customColumnsParams = {
            'mobileDevice': true,
            'idWorkflow': params[7],
            'inputtray': bizagi.util.inputTray(self.online)
        };

	    if (bizagi.util.isNumeric(caseRadNumber)) {
		    customColumnsParams['idCase'] = caseRadNumber;
	    } else {
		    customColumnsParams['radNumber'] = caseRadNumber;
	    }

	    $.when(self.dataService.getCasesByWorkflow(customColumnsParams)).done(function (casesData) {

            for (var i = 0, lengthRow = casesData.rows.length; i < lengthRow; i++) {
                var caseDataRow = casesData.rows[i];
                if (caseDataRow.id == self.params[0]) {
                    self.params[6] = [];

                    if (caseDataRow.radnumber && caseDataRow.casenumberdisplayname) {
                        self.params[6].push([caseDataRow.casenumberdisplayname, caseDataRow.radnumber]);
                    }

                    for (var j = 0, lengthField = caseDataRow.fields.length; j < lengthField; j++) {

                        if (typeof (caseDataRow.fields[j].Value) == "object") {

                            for (var k = 0, lengthItem = caseDataRow.fields[j].Value.length; k < lengthItem; k++) {

                                var arrayField = caseDataRow.fields[j].Value[k];
                                if (arrayField != "") {
                                    self.params[6].push([caseDataRow.fields[j].DisplayName, arrayField, caseDataRow.fields[j].DataType]);
                                    break;
                                }
                            }

                        } else {
                            self.params[6].push([caseDataRow.fields[j].DisplayName, caseDataRow.fields[j].Value, caseDataRow.fields[j].DataType]);
                        }
                    }
                    break;
                }
            }

            var htmlContent = $(bizagi.util.trim(self.summaryItemTemplateContent(self.params)));

            // Assign content data
            $('.bz-wp-summary-item-header', self.content).html(params[5]);
            $('#bz-wp-summary-item-list', self.canvas).html(htmlContent);
            $('.bz-wp-sm-item', self.content).data('item', params);

            bizagi.util.tablet.stopLoading();

            defer.resolve('');

        }).fail(function (error) {
            console.log(error.responseText);

            bizagi.util.tablet.stopLoading();
            defer.reject('');
        });

        return defer.promise();
    },

    /*
    *   Clear content of homePortal
    */
    clearContent: function (params) {
        params = params || {};

        // Clear content
        $('#bz-wp-summary-item-list', self.content).fadeIn();
        $('#bz-wp-summary-item-list li', self.content).empty();
        $('.bz-wp-summary-item-header', self.content).html('&nbsp;');

        // Add image background
        if ($('.bz-menu-home').hasClass('hidden') && params.clear) {
            $('.bz-menu-home').removeClass('hidden');
            return;
        }

        if (!$('.bz-menu-home').hasClass('hidden') && !params.clear) {
            $('.bz-menu-home').addClass('hidden');
            return;
        }
    }
});
