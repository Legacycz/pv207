/*
*   Name: Webpart reportsChart for tablet
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will define a base class of webpart reportsChart on tablet
*/
bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.reportsChart", {

}, {
    /**
    *   Constructor
    */
    init: function(workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        self._super(workportalFacade, dataService, initialParams);
        self.homePortalFramework = self.getHomeportalFrameworkIntance();


        // Load Templates        
        self.contentTemplate = self.homePortalFramework.loadComponentTemplate('reports-main', 'reportsChart', { useWithBlock: false });
        self.reportingMenuTemplate = kendo.template(self.workportalFacade.getTemplate("reports-menu"), { useWithBlock: false });
        self.reportingContentTemplate = kendo.template(self.workportalFacade.getTemplate("reports-container"), { useWithBlock: false });

        /* Override kendo behaviour, it set a specific height size on ios, generating visual problems*/
        if (bizagi.detectSO() === "ios") {
            window.addEventListener("orientationchange", function() {
                $("body").css("height", "auto");
                var reportIframe = document.getElementsByClassName("bz-wp-reports-iframe")[0].contentDocument.body;
                reportIframe.style.height = $("body").outerHeight() - 100;
                $("#bizagi-reporting-wrapper", reportIframe).css("height", $("body").outerHeight() - 100);
            }, false);
        }
    },
    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        deferred.resolve(self.contentTemplate);
        return deferred.promise();
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        self.subscribe("homeportalShow-reporstChart", $.proxy(self.updateContent, self));
        self.canvas.bind("view-initialized", $.proxy(self.onAfterViewInitialized, self));

        // Configure handlers to click events
        self.configureHandlers();
    },

    /*
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;        

        // show the render form to case
        self.subscribe("homeportalShow-reportsChart", function (e, params) {
            var deferred = $.Deferred();
            bizagi.util.mobility.startLoading();

            $.when(self.renderMenu(params))
                .done(function () {
                    self.loadReport(self.endPoint.defaultReport, self.endPoint.info);
                    self.eventsHandler();

                    deferred.resolve();
                }).fail(function () {
                    bizagi.util.mobility.stopLoading();
                    deferred.reject();
                });

            return deferred.promise();

        });
    },

    /*
    * Events Handler
    */
    eventsHandler: function () {

        var self = this;
        var context = self.getContent();

        $("#reports-menu, li", self.content).on("click", function () {

            bizagi.util.mobility.startLoading();

            var element = $(this);
            var report = element.data('report');

            element.siblings().removeClass("ui-bizagi-wp-widget-reports-menu-active");
            element.addClass("ui-bizagi-wp-widget-reports-menu-active");
            self.loadReport(report, self.endPoint.info);
        });
    },

    /**
    * Set the view header
    */
    onAfterViewInitialized: function (e, data) {
        var self = this;
    },

    renderMenu: function (params) {
        var self = this;
        var content = self.getContent();


        var deferred = new $.Deferred();

        self.endPoint = bizagi.reporting[params.endPoint];
        self.endPoint.defaultReport = params.defaultReport || self.endPoint.defaultReport;
        self.filters = params.filters || {};
        self.myTeam = [];

        var data = self.endPoint;
        data["getResource"] = function (resource) {
            return bizagi.localization.getResource(resource);
        };

        var containerMenu = $(".bz-wp-reports-menu", content).html(self.reportingMenuTemplate(data));

        //Evaluate callbacks
        if (typeof self.endPoint.callBack !== "undefined") {
            self[self.endPoint.callBack]();
        }

        deferred.resolve(containerMenu);

        return deferred.promise();
    },


    /*
    * Load Report
    */
    loadReport: function (report, info) {

        var self = this;

        $("iframe", ".bz-wp-reports-chart").remove();
        $(".bz-wp-reports-chart", self.content).empty();

        return self.renderIframeLoader({ report: report, info: info }).done(function () {
            setTimeout(function () {
                bizagi.util.mobility.stopLoading();
            }, 2000);
        });
    },

    loadEnvironmentData: function () {
        var env = {};

        env.BIZAGI_LOCAL_RESOURCES = BIZAGI_LOCAL_RESOURCES || false;
        env.BIZAGI_PATH_TO_BASE = BIZAGI_PATH_TO_BASE || '../../';

        env.BIZAGI_USE_ABSOLUTE_PATH = BIZAGI_USE_ABSOLUTE_PATH || false;
        env.BIZAGI_DEFAULT_DEVICE = BIZAGI_DEFAULT_DEVICE || "tablet_android";
        env.BIZAGI_LANGUAGE = BIZAGI_LANGUAGE || 'en-US';
        env.BIZAGI_ENVIRONMENT = BIZAGI_ENVIRONMENT || "release";

        env.BIZAGI_PROXY_PREFIX = typeof (BIZAGI_PROXY_PREFIX) !== 'undefined' ? BIZAGI_PROXY_PREFIX : "";

        env.BIZAGI_DEFAULT_CURRENCY_INFO = BIZAGI_DEFAULT_CURRENCY_INFO || {
            "symbol": "$",
            "decimalSeparator": ",",
            "groupSeparator": ".",
            "decimalDigits": "2"
        };

        env.BIZAGI_DEFAULT_DATETIME_INFO = BIZAGI_DEFAULT_DATETIME_INFO || {
            "shortDateFormat": 'dd/MM/yyyy',
            "timeFormat": 'H:mm',
            "longDateFormat": "dddd, dd' de 'MMMM' de 'yyyy",
            "twoDigitYearMax": '2029',
            "twoDigitYearMaxDelta": '16'
        };

        env.BIZAGI_SETTINGS = BIZAGI_SETTINGS || {
            "UploadMaxFileSize": '10048576',
            "skipSubprocessGlobalForm": 'true'
        };

        return env;
    },

    loadReportingData: function (report, info) {
        var self = this;
        var reporting = {};

        reporting.report = report;
        reporting.info = info;
        reporting.components = self.endPoint.reports[report].components;
        reporting.filters = self.filters;
        reporting.myTeam = self.myTeam;

        return reporting;
    },

    /*
    * callBack for report myTeam
    */
    myTeamCallBack: function () {

        var self = this;

        return $.when(self.dataService.getDataForMyTeam()).done(function (result) {

            var $teamTab = $("li[data-report='bamresourcemonitorworkinprogressteam']", self.content);

            if (!result.items.length) {
                $teamTab.css('display', 'none');
            } else {
                self.myTeam = result;
            }

        });
    },

    renderIframeLoader: function (params) {
        var self = this;

        var deferred = new $.Deferred();

        self.reportingElementId = "reporting_" + (Math.round(Math.random() * 99999)).toString();

        var iFrameContainer = self.reportingContentTemplate();
        var iframe = document.createElement('iframe');

        $(iframe).attr({
            style: "position:relative;width:100%;height:500px;overflow:auto;",
            class: "bz-wp-reports-iframe",
            id: self.reportingElementId || "canvas",
            frameBorder: 0,
            src: "about:blank"
        });

        $(".bz-wp-reports-chart", self.content).append(iFrameContainer);
        $(".bz-wp-reports-canvas", self.content).append(iframe);

        var documentToWriteTo = (iframe.contentWindow || iframe.contentDocument);
        if (documentToWriteTo.document)
            documentToWriteTo = documentToWriteTo.document;

        documentToWriteTo.open();

        iframe = document.frames ? document.frames[self.reportingElementId] : document.getElementById(self.reportingElementId);

        $(iframe).load(function () {
            var that = this;

            var listResources = [
                "jquery/steal.js",
                "jquery/bizagi.loader.js",
                bizagi.getJavaScript("bizagi.workportalflat.tablet.report.loader")
            ];

            var headiFrame = that.contentDocument.head;
            var bodyiFrame = that.contentDocument.body;

            // Load resources
            $(headiFrame).append($("<link/>", { rel: "stylesheet", href: BIZAGI_PATH_TO_BASE + bizagi.getStyleSheet("bizagi.workportalflat.override.reports"), type: "text/css" }));

            $.each(listResources, function (item, value) {
                var newScript = document.createElement('script');

                newScript.type = 'text/javascript';
                newScript.src = BIZAGI_PATH_TO_BASE + value;
                headiFrame.appendChild(newScript);
            });

            // Canvas container reporting
            var newContentDiv = document.createElement('div');
            var newDiv = document.createElement('div');

            $(newContentDiv).attr({ id: "bizagi-reporting-wrapper" });
            $(newDiv).attr({ id: "reports-canvas", class: "ui-bizagi-wp-widget-reports-chart" });
            newContentDiv.appendChild(newDiv);

            bodyiFrame.appendChild(newContentDiv);

            // Load data context
            $(iframe).callInside(function (windowParams) {
                console.log("Loading data...");
            }, {
                jQuery: $,
                context: bizagi,
                reporting: self.loadReportingData(params.report, params.info),
                env: self.loadEnvironmentData(),
                height: self.height - 50
            });

            deferred.resolve();
        });

        // Resize iframe
        $(iframe).ready(function () {
            var height = $(window).outerHeight();
            self.height = height - 100;
            $("#" + self.reportingElementId).outerHeight(height);
        });

        documentToWriteTo.close();

        return deferred.promise();
    }

});
