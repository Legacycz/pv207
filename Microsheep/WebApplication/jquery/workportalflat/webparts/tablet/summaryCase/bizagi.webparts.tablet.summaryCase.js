/**
*   Name: BizAgi Webpart summaryCase
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to render case summary
*/

bizagi.workportal.webparts.summaryCase.extend("bizagi.workportal.webparts.summaryCase", {}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        self._super(workportalFacade, dataService, initialParams);
    },

    /*
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        self._super();

        $("#bz-render-drawer", "body").data("kendoMobileDrawer").bind("show", function () {
            $(".bz-header_select").trigger("resizeHeader");
        });

        $("#bz-render-drawer", "body").delegate(".summaryLink", "click", function (e) {

            e.preventDefault();

            self.routingExecute($(this));

            $($(this), self.canvas).remove();
            self.cache = {};
        });
    },

    /*
    *   Renders the summary component of the webpart
    */
    renderSummary: function (params) {
        var self = this;
        var defer = new $.Deferred();

        self.params = params || {};
        self.cache = {};

        self.dataService.summaryCaseDetails({
            idCase: params.idCase,
            eventAsTasks: params.eventAsTasks || false,
            onlyUserWorkItems: params.onlyUserWorkItems || false,
            idWorkitem: params.idWorkitem,
            isOfflineForm: params.isOfflineForm || false
        }).done(function (data) {
            var date = typeof (data.estimatedSolutionDate) !== 'undefined' ? data.estimatedSolutionDate : data.solutionDate;

            // Offline forms
            if (params.isOfflineForm) {
                date = date ? date : data.creationDate;
                data.caseNumber = params.idCase;
            }

            data.params = self.params;

            data.group = bizagi.getGroupedData(date);
            self.data = data;


            self.data.estimatedSolutionDate = bizagi.util.dateFormatter.formatDate(new Date(self.data.estimatedSolutionDate), self.getResource("dateFormat") + " " + self.getResource("timeFormat"));
            self.data.solutionDate = bizagi.util.dateFormatter.formatDate(new Date(self.data.solutionDate), self.getResource("dateFormat") + " " + self.getResource("timeFormat"));
            self.data.creationDate = bizagi.util.dateFormatter.formatDate(new Date(self.data.creationDate), self.getResource("dateFormat") + " " + self.getResource("timeFormat"));

            var content = self.summaryCaseTemplate(self.data);

            $("#content-render-drawer .km-scroll-container").empty();
            $("#content-render-drawer .km-scroll-container").html(content);
            $(".km-content:visible").data('kendoMobileScroller').reset();

            self.content = $('#content-render-drawer .km-scroll-container');

            //self.canvas.empty();
            self.canvas = "body";

            /* istanbul ignore next */
            $(".ui-bizagi-container-tab").bztabs({
                activate: function (e) {                    
                    bizagi.util.tablet.startLoading();

                    data.currentTab = $(e.currentTarget).data("reference-tab");

                    self.renderCurrentTab(data);
                },
                tabNumber: 2
            });

            if ((data.currentState && data.currentState.length > 0) && (typeof (data.currentState[0].allowReleaseActivity) !== "undefined" && data.currentState[0].allowReleaseActivity)) {
                /* istanbul ignore next */
                $(".bz-wp-summary-release", self.content).bind('click', function () {
                    self.release(params);
                });
            }

            defer.resolve();
        });
        return defer.promise();
    }
});
