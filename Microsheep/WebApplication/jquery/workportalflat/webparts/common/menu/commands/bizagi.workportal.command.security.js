﻿/*
*   Name: BizAgi Workportal Security Command
*   Author: Richar Urbano - RicharU
*   Comments:
*   -   This script will define a common class to access the security data
*/

$.Class("bizagi.workportal.command.security", {}, {


    init: function (dataService) {
        var self = this;
        self.dataService = dataService;
        self.rawData = {};
    },

    /**
    *   Security data
    */
    getSecurity: function() {
        var self = this;
        var def = new $.Deferred();
        var authMenu = {};

        self.jsonSecurityList = {};

        if (bizagi.menuSecurity) {
            def.resolve(bizagi.menuSecurity);
        } else {
            $.when(self.dataService.getMenuAuthorization())
                .done(function(data) {
                    self.rawData = data;
                    bizagi.menuSecurity = self.convertSecurityData(data);

                    self.jsonSecurityList = data;

                    authMenu.authNewCase = bizagi.menuSecurity.NewCase || false;
                    authMenu.authAnalysisReports = self.checkRootCategory(data, "AnalysisReports") || false; // TODO: Review Reports
                    authMenu.authAdmin = self.checkRootCategory(data, "Admin") || false;
                    authMenu.authCurrentUserAdministration = bizagi.menuSecurity.CurrentUser || false;

                    def.resolve($.extend(bizagi.menuSecurity, authMenu));
                }).fail(function(data) {
                    self.rawData = {};
                    bizagi.menuSecurity = {};

                    self.jsonSecurityList = {};

                    authMenu.authNewCase = false;
                    authMenu.authAnalysisReports = false;
                    authMenu.authAdmin = false;
                    authMenu.authCurrentUserAdministration = false;

                    def.resolve($.extend(bizagi.menuSecurity, authMenu));
                });
        }

        return def.promise();
    },

    convertSecurityData: function (data) {        
        var permsMenu = {};
        
        var getRecursivePerms = function (perms) {
            $.each(perms, function (key, value) {
                if (typeof value == 'object') {
                    if (typeof key != "number") {
                        permsMenu[key] = true;
                    }
                    getRecursivePerms(value);
                } else {
                    permsMenu[value] = true;
                }
            });
        };
        
        getRecursivePerms(data.permissions);
        
        return permsMenu;
    },

    getRawData: function () {
        return this.rawData;
    },
    
     /**
    *   Customize the web part in each device
    */
    checkRootCategory: function (data, key) {
        if (data.permissions != undefined) {
            for (var i = 0; i < data.permissions.length; i++) {
                if (data.permissions[i][key] != undefined) {
                    return true;
                }
            }
        }
        return false;
    },
});