/*
*   Name: BizAgi Workportal Tablet Reports Widget Controller
*   Author: Richar Urbano - RicharU
*/

// Auto extend
$.Class.extend("bizagi.workportal.widget.reportsMenu", {}, {
    init: function(workportalFacade, dataService, params) {
        var self = this;

        self.workportalFacade = workportalFacade;
        self.dataService = dataService;
        self.params = params;

        self.endPoint = ["Reports", "BAMProcess", "BAMTask", "AnalyticsProcess", "AnalyticsTask", "AnalyticsSensor", "ResourceBAM"];
    },

    initialize: function() {
        var self = this;

        self.reportsMenu = self.getRawReportsJSON();

        // prepare json
        $.when(self.buildAnalysisJSON()).done(function() {

            //render reports menu
            $(".bz-wp-list-items").html(self.renderReportsMenu("Reports"));
            $(".bz-wp-list-items").kendoMobileListView({ style: "inset" });

            //Events handler
            self.eventsHandler();
        });
    },     

    /**
    * Get getAnalisysQueries service and parse json to
    * create tree categories
    */
    getAnalisysQueries: function() {

        var self = this;
        var analysisJSON = [];
        var def = new $.Deferred();

        $.when(self.dataService.getReporstAnalysisQuery()).done(function(data) {
            $.each(data.queries, function(key, value) {
                analysisJSON.push({
                    displayName: value.name || "",
                    description: value.description || "",
                    icon: "AnalyticsQueryItems",
                    show: true,
                    filters: value.filterParameters,
                    endPoint: self.endPoint[value.reportSet],
                    reportSet: value.reportSet,
                    activeEdit: true,
                    id: value.id,
                    subItems: []
                });
            });

            def.resolve(analysisJSON);
        });

        return def.promise();
    },

    /*
    * Render list categories for each idCategory
    */
    renderReportsMenu: function(index) {

        var self = this;
        var tmplItems = kendo.template(self.workportalFacade.getTemplate("report-item-tmpl"), { useWithBlock: false });
        var tmplNoItems = kendo.template(self.workportalFacade.getTemplate("report-noitem-tmpl"), { useWithBlock: false });

        var elements = self.reportsMenu[index];
        var reportsContainer;

        if (elements.length) {
            reportsContainer = tmplItems(elements);
        } else {
            reportsContainer = tmplNoItems(elements);
        }

        return reportsContainer;
    },

    /*
    * Build menu JSON
    */
    buildAnalysisJSON: function() {

        var self = this;
        var deferred = $.Deferred();

        $.when(self.getAnalisysQueries()).pipe(function(data) {

            if (bizagi.menuSecurity.AnalysisQueries && (data.length > 0)) {

                self.reportsMenu["AnalysisQuery"] = data;

                self.reportsMenu["Reports"].push({
                    displayName: bizagi.localization.getResource('workportal-menu-submenu-AnalysisQueries'),
                    show: true,
                    icon: "AnalyticsQuery",
                    endPoint: "",
                    subItems: "AnalysisQuery"
                });
            }

            deferred.resolve();
        });

        return deferred.promise();
    },

    /*
    * Events Handler
    */
    eventsHandler: function() {

        var self = this;
        var menuList = $(".bz-wp-list-items");

        // Event for selected menu item
        menuList.on("click", "li.bz-wp-widget-reportsmenu-readmode", function(event) {

            event.stopPropagation();

            var context = $(this);
            var subItems = context.data("subitems"),
                displayName = context.data("displayname"),
                endPoint = context.data("endpoint"),
                id = context.find(".bz-wp-widget-reportsmenu-itemactions").data("id");

            //execute selected item
            self.execSelectedItem(context, id, subItems, endPoint);

        });
    },

    /*
    * Actions for the selected item in menu
    */
    execSelectedItem: function(context, id, subItems, endPoint) {

        var self = this;

        if (endPoint === "") {
            if (context.find(".bz-wp-list-subitems").has("li").length > 0) {
                var currentElement = $(".bz-wp-list-subitems", context);
                if (currentElement.css('display') == 'none') {
                    currentElement.fadeIn();
                } else {
                    currentElement.fadeOut();
                }
            } else {
                var subContent = self.renderReportsMenu(subItems);
                $(".bz-wp-list-subitems", context).html(subContent);
                $(".bz-wp-list-subitems").kendoMobileListView({ style: "inset" });
            }
        } else {
            var filters = (!id) ? {} : self.getFiltersById(id);

            // Collapse items
            $(".bz-wp-list-subitems").toggle();
            $(".bz-wp-list-items").toggle();
            bizagi.webpart.publish("homeportalShow", { "what": "reportsChart", "title": "Reports", "params": { filters: filters, endPoint: endPoint } });
        }
    },    

    /*
    * Get Reports Data
    */
    getReportsData: function(id) {

        var self = this;
        var queryItems = self.reportsMenu["AnalysisQuery"];

        return queryItems.filter(function(obj) {
            return (obj.id === id);
        });
    },

    /*
    * Get filter by id
    */
    getFiltersById: function(id) {

        var self = this;
        var rpData = self.getReportsData(id);

        return rpData[0].filters;
    },
    
    /*
    * JON for reports menu
    */

    getRawReportsJSON: function() {

        return {
            "Reports": [
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-BAMMenu"),
                    icon: "BAM",
                    show: (bizagi.menuSecurity.BAMProcess || bizagi.menuSecurity.BAMTask || bizagi.menuSecurity.BAMResourceMonitor) ? true : false,
                    endPoint: "",
                    subItems: "BAM"
                },
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-AnalyticsMenu"),
                    icon: "Analytics",
                    show: (bizagi.menuSecurity.AnalyticsProcess || bizagi.menuSecurity.AnalyticsTask) ? true : false,
                    endPoint: "",
                    subItems: "Analytics",
                },
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-AnalyticsSensor"),
                    show: bizagi.menuSecurity.AnalyticsSensor ? true : false,
                    icon: "AnalyticsSensor",
                    endPoint: "AnalyticsSensor",
                    subItems: []
                }
            ],
            "BAM": [
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-AnalyticsProcess"),
                    show: bizagi.menuSecurity.BAMProcess,
                    icon: "BAMProcess",
                    endPoint: "BAMProcess",
                    subItems: []
                },
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-AnalyticsTask"),
                    show: bizagi.menuSecurity.BAMTask,
                    icon: "BAMTask",
                    endPoint: "BAMTask",
                    subItems: []
                },
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-BAMResourceMonitor"),
                    show: bizagi.menuSecurity.BAMResourceMonitor,
                    icon: "BAMResourceMonitor",
                    endPoint: "ResourceBAM",
                    subItems: []
                }
            ],
            "Analytics": [
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-AnalyticsProcess"),
                    show: bizagi.menuSecurity.AnalyticsProcess,
                    icon: "AnalyticsProcess",
                    endPoint: "AnalyticsProcess",
                    subItems: []
                },
                {
                    displayName: bizagi.localization.getResource("workportal-menu-submenu-AnalyticsTask"),
                    show: bizagi.menuSecurity.AnalyticsTask,
                    icon: "AnalyticsTask",
                    endPoint: "AnalyticsTask",
                    subItems: []
                }
            ],
            "AnalysisQuery": [
            ]
        };
    }
});