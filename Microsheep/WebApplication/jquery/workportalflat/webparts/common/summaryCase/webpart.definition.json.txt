﻿{
  "name": "summaryCase",
  "devices": {
    "tablet_ios": {
      "js": [
        {
          "src": "bizagi.webparts.summaryCase.js"
        },
        {
          "src": "../../tablet/summaryCase/bizagi.webparts.tablet.summaryCase.js"
        },
        {
          "src": "plugins/jquery.tabs.js"
        }
      ],
      "css": [
        {
          "src": "../../tablet/summaryCase/bizagi.webpart.tablet.summaryCase.css"
        }
      ],
      "tmpl": [
        {
          "alias": "summary-case-main",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-main"
        },
        {
          "alias": "summary-case-assignees",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-assignees"
        },
        {
          "alias": "summary-case-comments",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-comments"
        },
        {
          "alias": "summary-case-subprocess",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-subprocess"
        },
        {
          "alias": "summary-case-events",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-events"
        },
        {
          "alias": "summary-case-activities",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-activities"
        }
      ]
    },
    "tablet_android": {
      "js": [
        {
          "src": "bizagi.webparts.summaryCase.js"
        },
        {
          "src": "../../tablet/summaryCase/bizagi.webparts.tablet.summaryCase.js"
        },
        {
          "src": "plugins/jquery.tabs.js"
        }
      ],
      "css": [
        {
          "src": "../../tablet/summaryCase/bizagi.webpart.tablet.summaryCase.css"
        }
      ],
      "tmpl": [
        {
          "alias": "summary-case-main",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-main"
        },
        {
          "alias": "summary-case-assignees",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-assignees"
        },
        {
          "alias": "summary-case-comments",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-comments"
        },
        {
          "alias": "summary-case-subprocess",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-subprocess"
        },
        {
          "alias": "summary-case-events",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-events"
        },
        {
          "alias": "summary-case-activities",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-activities"
        }
      ]
    },
    "smartphone_ios": {
      "js": [
        {
          "src": "bizagi.webparts.summaryCase.js"
        },
        {
          "src": "../../smartphone/summaryCase/bizagi.webparts.smartphone.summaryCase.js"
        },
        {
          "src": "plugins/jquery.tabs.js"
        }
      ],
      "css": [
        {
          "src": "../../smartphone/summaryCase/bizagi.webpart.smartphone.summaryCase.css"
        }
      ],
      "tmpl": [
        {
          "alias": "summary-case-main",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-main"
        },
        {
          "alias": "summary-case-assignees",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-assignees"
        },
        {
          "alias": "summary-case-comments",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-comments"
        },
        {
          "alias": "summary-case-subprocess",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-subprocess"
        },
        {
          "alias": "summary-case-events",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-events"
        },
        {
          "alias": "summary-case-activities",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-activities"
        }
      ]
    },
    "smartphone_android": {
      "js": [
        {
          "src": "bizagi.webparts.summaryCase.js"
        },
        {
          "src": "../../smartphone/summaryCase/bizagi.webparts.smartphone.summaryCase.js"
        },
        {
          "src": "plugins/jquery.tabs.js"
        }
      ],
      "css": [
        {
          "src": "../../smartphone/summaryCase/bizagi.webpart.smartphone.summaryCase.css"
        }
      ],
      "tmpl": [
        {
          "alias": "summary-case-main",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-main"
        },
        {
          "alias": "summary-case-assignees",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-assignees"
        },
        {
          "alias": "summary-case-comments",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-comments"
        },
        {
          "alias": "summary-case-subprocess",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-subprocess"
        },
        {
          "alias": "summary-case-events",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-events"
        },
        {
          "alias": "summary-case-activities",
          "src": "summaryCase.kendo.tmpl.html#ui-bz-wp-summaryCase-activities"
        }
      ]
    }
  }
}