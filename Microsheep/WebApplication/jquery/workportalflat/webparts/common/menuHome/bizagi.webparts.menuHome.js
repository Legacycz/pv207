/**
 *   Name: BizAgi Test
 *   Author: Oscar Osorio
 *   *   Comments:
 *   -   This script will define a hello world test
 */
bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.menuHome", {}, {
    /**
    *   Constructor
    */
    init: function(workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        self._super(workportalFacade, dataService, initialParams);
        self.homePortalFramework = self.getHomeportalFrameworkIntance();
    },

    /**
    *   Renders the content for the current controller
    */
    renderContent: function(params) {
        var self = this;
        var deferred = $.Deferred();

        self.content = self.homePortalFramework.loadComponentTemplate('menuHome-tmpl', 'menuHome', {});

        deferred.resolve(self.content);
        return deferred.promise();
    },
    /**
    *   Customize the web part in each device
    */
    postRender: function(params) {
        var self = this;
        return self.homePortalFramework.loadComponents('menuHome', self.content)
            .done(function() {
                // Configure handlers to click events
                self.configureHandlers();
            });
    },
    /**
    custom method for webpart
    */
    configureHandlers: function() {
        var self = this;
    },
    /**
    custom method for webparts
    */
    showView: function() {

    }
});
