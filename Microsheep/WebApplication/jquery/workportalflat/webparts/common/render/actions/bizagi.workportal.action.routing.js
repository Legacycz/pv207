/**
 * Created by LuisCE on 12/06/2014.
 */
/*
 *   Name: BizAgi Workportal Smarthpone Routing controller
 *   Author: luisce
 *   Comments:
 *   -   This script will provide action routing
 */

// Auto extend
$.Class.extend("bizagi.workportal.action.routing", {

}, {
    /*
    *   Constructor
    */
    init: function (params) {
        var self = this;
        // Set data service
        this.dataService = params.dataService;

        // Set l10n service
        this.resources = params.resources;
        this.getResource = params.getResource;
        /* istanbul ignore next: untestable */
        bizagi.webpart.subscribe('changeCase', function (e, params) {
            bizagi.util.mobility.startLoading();
            self.executeRouting(params);
        });
    },
    /*
    *   Executes the action
    *   Could return a deferred
    */
    executeRouting: function (params) {
        var self = this;
        self.params = params || {};
        var deferred = $.Deferred();

        /* New implementation of routing*/
        bizagi.util.mobility.startLoading();
        
        $.when(self.dataService.routing.getRoute(params)).then(function (route) {
            route = route || {};

            if (self.params.isOfflineForm != "undefined" && self.params.isOfflineForm === true) {
                route.moduleParams = $.extend(route.moduleParams, { formsRenderVersion: self.params.formsRenderVersion, isOfflineForm: self.params.isOfflineForm, idCase: self.params.idCase, idWorkitem: self.params.idWorkitem }) || {};
            } else {
                route.moduleParams = route.moduleParams || {};
            }

            if ((route.moduleParams.displayName === "" || typeof route.moduleParams.displayName === 'undefined') && ((self.params.workItems && self.params.workItems.length > 0) || self.params.displayName)) {
                route.moduleParams = $.extend(route.moduleParams, { displayName: self.params.displayName || self.params.workItems[0].displayName });
            }

            switch (route.module) {
            case bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_RENDER:
                return $.when(bizagi.webpart.publish("render-case", route.moduleParams));
                break;
            case bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ASYNC:
                return $.when(bizagi.webpart.publish("render-async", route.moduleParams));
                break;
            case bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ROUTING:
                var parameters = {
                    title: self.getResource("workportal-widget-routing-window-selector"),
                    width: 670,
                    height: 400,
                    onClose: route.moduleParams.onClose
                };

                bizagi.webpart.publish("showDialog", {
                    widgetName: bizagi.workportal.webparts.webpart.BIZAGI_WORKPORTAL_WEBPART_ROUTING,
                    data: route.moduleParams.data,
                    modalParameters: parameters,
                    onClose: parameters.onClose
                });
                break;
            }
        }).done(function () {
            deferred.resolve();
        }).fail(function () {
            deferred.reject();
        }).always(function () {
            bizagi.util.mobility.stopLoading();
        });
        
        return deferred.promise();
    }
});
