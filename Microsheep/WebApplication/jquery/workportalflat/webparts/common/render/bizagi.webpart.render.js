/*
*   Name: BizAgi Test
*   Author: oscaro
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.render", {
    BIZAGI_WORKPORTAL_ACTION_ROUTING: "rounting"
}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        self.renderingStarted = new $.Deferred();
        self.navigation = initialParams.navigation;
        self.viewStack = [];

        // Call base
        self._super(workportalFacade, dataService, initialParams);

        self.routing = new bizagi.workportal.action.routing(self);
        self.async = new bizagi.workportal.widget.async(self);
        self.navigation = {};

        /* istanbul ignore next: untestable */
        this.subscribe("showDialog", function (e, params) {
            // Shows a dialog
            self.showDialog(params);
        });

        this.subscribe("stackNotifier", function (e, params) {
            if (params.type == 'add')
                self.viewStack.push(params.data);

            if (params.type == 'remove')
                self.destroyView(params.data);

        });

        /* istanbul ignore next: untestable */
        this.subscribe("createEditionView", function (e, params) {
            // Create a RenderEdition View
            $.proxy(self.createEditionView(params), self);
        });

    },

    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {

        bizagi.services.ajax.loginPage = function () {
            //TODO: Redirigir al login
        };

        var self = this;
        var deferred = $.Deferred();
        var template = kendo.template(self.getTemplate('render-tmpl'), { useWithBlock: false });

        self.content = $(template({}));
        self.canvas.append(self.content);

        deferred.resolve(self.content);
        return deferred.promise();
    },

    /**
    *   Renders the form component of the widget
    */
    renderForm: function (params) {
        var self = this;
        var originalParams = params;

        // if view exists
        if (self.view) {
            self.view.destroy();
        }

        var template = kendo.template(self.getTemplate('render-view'), { useWithBlock: false });
        var formTitle = "";

        if (params.radNumber) {
            formTitle = params.radNumber;
        } else {
            var radNumber = $(".bz-item-case", self.canvas).data("radnumber");
            if (radNumber) {
                formTitle = radNumber;
            } else {
                formTitle = params.idCase ? params.idCase : "";
            }
        }

        self.tmpl = $(template({ 'title': formTitle }));
        $('#paneRender', self.content).append(self.tmpl);

        self.content.kendoMobileSplitView();
        self.initNavigation();
        self.setButtonsHandler();

        self.view = new kendo.mobile.ui.View(self.tmpl, { 'useNativeScrolling': true });

        self.view.bind('show', function () {
            self.navigation.setVisibleIdView(this.id.substring(1, this.id.length));
        }).bind('destroy', function (args, e) {

            this.element.remove();

            if (typeof ($("#modalview-async-message")).data('kendoMobileModalView') !== 'undefined') {
                $("#modalview-async-message").data('kendoMobileModalView').destroy();
            }
        });

        var renderContainer = $("#render-content", self.tmpl);

        self.idRender = self.view.id;

        if (bizagi.kendoMobileApplication.view().id !== "#renderSplitView") {
            bizagi.kendoMobileApplication.navigate("#renderSplitView");
        }

        $('#paneRender', self.content).data("kendoMobilePane").navigate(self.idRender);

        if (params.withOutGlobalForm) {
            var errorTemplate = kendo.template(self.workportalFacade.getTemplate("info-message"), { useWithBlock: false });
            var message = params.messageForm || self.resources.getResource("render-without-globalform");

            // Offline message
            if (typeof params != "undefined" && typeof params.isOfflineForm !== "undefined" && params.isOfflineForm == true) {
                message = self.resources.getResource("render-without-globalform-offline");
            }

            var errorHtml = errorTemplate({ 'message': message });

            self.navigation.initContextualButtons();

            $(self.navigation.buttonsContextual.back).bind("click", function (e) {
                /* istanbul ignore next: untestable */
                bizagi.kendoMobileApplication.navigate('homePortal');
            });

            $(renderContainer).append(errorHtml);

            $(".ui-bizagi-info-message-button", renderContainer).click(function () {
                // Redireccionar a taskfeed
                bizagi.kendoMobileApplication.navigate('homePortal');
                self.publish("bz-update-list-cases", { updateDataList: true });
            });

            return renderContainer;
        }

        // Load render page
        /* istanbul ignore if: rendering is declared on unit test */
        if (typeof (self.rendering) === "undefined") {
            var paramsFacade = {};

            // Allow user to access offline form
            if (bizagi.util.isTabletDevice()) {
                paramsFacade.database = (typeof (self.dataService.database) !== "undefined") ? self.dataService.database : "";
            }

            paramsFacade.proxyPrefix = params.proxyPrefix || self.dataService.serviceLocator.proxyPrefix;
            self.rendering = new bizagi.rendering.facade(paramsFacade);
        }

        //Executes rendering into render container
        var resultRender = self.rendering.execute({
            canvas: renderContainer,
            navigation: self.navigation,
            action: "LOADFORM",
            idCase: params.idCase,
            displayName: params.displayName || params.taskName,
            idWorkitem: params.idWorkitem,
            idTask: params.idTask,
            isOfflineForm: params.isOfflineForm || false,
            postRenderEdit: self.postRenderEdit.bind(self),
            originalParams: originalParams,
            getTemplate: self.getTemplate,
            notification_box: $('#notification-box', self.content)
        }).done(function (e) {
            console.log("Termino ejecucion");
        });

        bizagi.util.setContext({
            idCase: params.idCase || null,
            displayName: params.displayName || params.taskName,
            caseNumber: params.caseNumber || null,
            idWorkitem: params.idWorkitem || null,
            idTask: params.idTask || null,
            isOfflineForm: params.isOfflineForm || false,
            formsRenderVersion: params.formsRenderVersion || null,
            fromTask: params.fromTask || null,
            radNumber: params.radNumber || null
        });


        // Attach handler to render container to subscribe for routing events
        renderContainer.bind("routing", function (context, triggerparams) {
            // Executes routing action
            var deferred = $.Deferred();
            var params = {
                'action': 'routing',
                'idCase': bizagi.context.idCase || triggerparams.idCase,
                'displayName': bizagi.context.displayName,
                'idTask': bizagi.context.idTask,
                'fromTask': bizagi.context.fromTask,
                'isOfflineForm': bizagi.context.isOfflineForm,
                'formsRenderVersion': bizagi.context.formsRenderVersion,
                'radNumber': bizagi.context.radNumber
            };
            params = $.extend(params, triggerparams);
            $.when(self.routing.executeRouting(params)).done(function () {
                self.publish("bz-update-list-cases");
                deferred.resolve();
            });
            return deferred.promise();
        });

        renderContainer.one("refresh", function (context, triggerparams) {
            var params = {
                'idCase': bizagi.context.idCase,
                'idTask': bizagi.context.idTask,
                'idWorkitem': bizagi.context.idWorkitem,
                'isOfflineForm': bizagi.context.isOfflineForm,
                'formsRenderVersion': bizagi.context.formsRenderVersion
            };

            self.refresh($.extend(params, triggerparams));
        });

        return resultRender;
    },

    initNavigation: function () {
        var self = this;

        self.navigation = new bizagi.workportal.action.navigation({ 'content': self.getContent() });

        return self.navigation;
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        self.configureHandlers();
    },

    renderCase: function (params) {
        var self = this;

        var deferred = $.Deferred();

        if (typeof params.idWorkitem === "undefined") {
            self.dataService.getWorkitems(params).done(function (resp) {
                var idWorkItem = resp.workItems[0] ? resp.workItems[0].idWorkItem : resp.idWorkitem;
                var idTask = resp.workItems[0] ? resp.workItems[0].idTask : resp.idTask;
                var data = {
                    'idCase': resp.idCase || resp.workItems[0].idCase,
                    'idWorkitem': idWorkItem || "",
                    'idTask': idTask || ""
                };

                $.when(self.renderForm($.extend(params, data, { refresh: true })))
                    .done(function () {
                        deferred.resolve();
                    })/* istanbul ignore next: untestable */
                    .fail(function (e) {
                        deferred.fail();
                    });
            });

            bizagi.util.mobility.stopLoading();
        } else {

            $.when(self.renderForm($.extend(params, { refresh: true })))
                .done(function () {
                    deferred.resolve();
                }).fail(function (e) {
                    self.navigation.initContextualButtons();
                    $(self.navigation.buttonsContextual.back).bind("click", function (e) {
                        /* istanbul ignore next: untestable */
                        bizagi.kendoMobileApplication.navigate('homePortal');
                    });
                    deferred.reject();
                });
        }
        return deferred.promise();
    },

    //edit Items on render
    postRenderEdit: function (renderControl) {
        var self = this;
        var render = renderControl;
        var properties = render.properties;

        var currentView = bizagi.kendoMobileApplication.view();

        if (currentView.id == "#homePortal") {
            self.pane = $("#homeportal-pane");
            self.initNavigation();
        } else {
            self.pane = $("#paneRender");
        }

        var viewParams = (typeof (render.titleLable) !== undefined && render.titleLable !== false) ? { "label": properties.displayName} : {}
        var editRender = $.tmpl(render.renderFactory.getTemplate("editRender"), viewParams);

        self.createEditionView({
            'idCase': self.params.idCase || bizagi.context.idCase || '',
            'displayName': bizagi.context.radNumber || bizagi.context.idCase,
            'content': editRender,
            'callback': function (params) {

                render.kendoView = $(params.id, 'body');
                render.kendoView.find(".ui-bizagi-container-inputs").html(render.inputEdition);

                self.pane.data("kendoMobilePane").navigate(params.id);

                render.getParams().navigation.setNavigationButtons(render, render.renderEditionType || 'edition');
            },
            'kendoViewOptions': render.kendoViewOptions,
            'kendoViewFinish': render.kendoViewFinish
        });
    },
    /**
    custom method for webpart
    */
    configureHandlers: function () {
        var self = this;

        // show the render form to case
        self.subscribe("render-case", function (e, params) {
            bizagi.util.mobility.startLoading();
            var deferred = $.Deferred();
            $.when(self.workportalFacade.modules["renderingflat"]).then(function () {
                return self.renderCase(params);
            }).done(function () {
                bizagi.util.mobility.stopLoading();
                deferred.resolve();
            }).fail(function () {
                bizagi.util.mobility.stopLoading();
                deferred.reject();
            });
            return deferred.promise();
        });
    },


    /*
    *   Shows a component inside a modal dialog
    */
    showDialog: function (data) {
        var self = this;

        var dialog = new bizagi.workportal.widget.dialog(self);
        dialog.renderDialog(data);

        return dialog;
    },

    createEditionView: function (params) {
        var self = this;
        var properties = self.properties;
        var now = $.now();
        var renderEdition = kendo.template(self.getTemplate("renderEdition-tmpl"), { useWithBlock: false });

        // View stack
        var stackParams = { id: '#inputEdition_' + now };
        self.publish("stackNotifier", { type: 'add', data: stackParams });

        renderEdition = renderEdition({ 'now': now, 'displayName': params.displayName || params.idCase, 'content': params.content });
        var currentView = bizagi.kendoMobileApplication.view();

        if (currentView.id == "#homePortal") {
            self.pane = $("#homeportal-pane");
            self.initNavigation();
        } else {
            self.pane = $("#paneRender");
        }

        $(self.pane).append(renderEdition);

        if (params.kendoViewOptions) {
            if (!params.kendoViewOptions.zoom) {
                params.kendoViewOptions.useNativeScrolling = true;
            }
        } else {
            params.kendoViewOptions = { useNativeScrolling: true };
        }

        var viewContainer = $('#inputEdition_' + now);

        viewContainer.kendoMobileView($.extend({
            'show': function () {
                self.navigation.setVisibleIdView(this.id.substring(1, this.id.length));
                if (typeof (params.kendoViewFinish) !== 'undefined')
                    params.kendoViewFinish();
            }, /* istanbul ignore next: untestable */
            'destroy': function (args, e) {
                this.element.remove();
            }
        }, params.kendoViewOptions));

        if (params.callback) {
            var callbackParams = (typeof (params.getNavigation) !== 'undefined' && params.getNavigation === true) ?
                { 'viewContainer': viewContainer, 'id': '#inputEdition_' + now, 'navigation': self.navigation} :
                { 'viewContainer': viewContainer, 'id': '#inputEdition_' + now };
            params.callback(callbackParams);
        }
    },

    /**
    * Funcion heredada en los dispositivos especificos
    */
    setButtonsHandler: function () {
    },

    /**
    * Eliminar vista actual (RenderEdition)
    */
    destroyView: function (params) {
        var self = this;
        params = params || {};
        
        // TODO revisar escenario de forma de adicion de una grilla (boton guardar)
        //if ((params.action == "back" || params.action == "buttom") && (self.viewStack && self.viewStack.length > 0)) {
        if ((params.action == "back" ) && (self.viewStack && self.viewStack.length > 0)) {
            var currentView = self.viewStack[self.viewStack.length - 1];
            if ($(currentView.id).kendoMobileView("view")) {
                console.log("Stack - " + currentView.id);
                self.viewStack.pop();
                $(currentView.id).kendoMobileView("view").destroy();
            }
        }
    }
});
