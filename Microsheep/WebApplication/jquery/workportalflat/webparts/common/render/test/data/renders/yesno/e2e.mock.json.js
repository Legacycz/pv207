/**
 * Created by FernandoL on 08/10/2014.
 */

bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
)
    .then(function () {

        // DUMMIES
        $.mockjax(function (settings) {
            if (settings.dataType == "text") {
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return "false";
                }
            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    if(CONTROL_EDITABLE) {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/yesno/dummy.rendering.yesno.txt",
                            transform: function (response, originalSettings) {
                                return response;
                            }
                        };
                    }
                    else{
                        return {
                            mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/yesno/dummy.rendering.yesno.noeditable.txt",
                            transform: function (response, originalSettings) {
                                return response;
                            }
                        };
                    }
                }
            }
        });
    });