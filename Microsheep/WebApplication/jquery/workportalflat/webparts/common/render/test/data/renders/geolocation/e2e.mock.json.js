/**
 * Created by FernandoL on 06/10/2014.
 */

bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
)
    .then(function () {
        // DUMMIES
        $.mockjax(function (settings) {
            if (settings.dataType == "text") {
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return "false";
                }
            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {

                    return {
                        mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/geolocation/dummy.rendering.geolocation.txt",
                        transform: function (response, originalSettings) {
                            return response;
                        }
                    };
                }
            }
        });
    });