{
  "form": {
    "properties": {
      "id": "D36DEF60-3597-4A85-A733-3201B30A9D06",
      "orientation": "ltr",
      "xpathContext": ""
    },
    "elements": [
      {
        "container": {
          "properties": {
            "id": "38602f48-7447-4140-bd53-cd9bc04228dd",
            "type": "tab",
            "displayName": "Tab"
          },
          "elements": [
            {
              "container": {
                "properties": {
                  "id": "aabef522-1be8-48a1-8879-7352f5659ce9",
                  "type": "tabItem",
                  "displayName": "Tab 1"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "2fed8528-b419-4d38-bdfe-655cc5cbb895",
                        "type": "text",
                        "xpath": "Offlineprocess.Nombre",
                        "displayName": "Nombre",
                        "maxLength": 50,
                        "dataType": 15,
                        "helpText": "",
                        "autoExtend": false
                      }
                    }
                  },
                  {
                    "render": {
                      "properties": {
                        "id": "bf82f7df-50b2-4c87-97c0-600e80ef4bd4",
                        "type": "radio",
                        "xpath": "Offlineprocess.Genero",
                        "value": [],
                        "displayName": "Genero",
                        "dataType": 2,
                        "data": [
                          {
                            "id": 1,
                            "value": "Masculino"
                          },
                          {
                            "id": 2,
                            "value": "Femenino"
                          }
                        ],
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            },
            {
              "container": {
                "properties": {
                  "id": "0bf06377-e627-4aa6-b4eb-95d46a65ab6e",
                  "type": "tabItem",
                  "displayName": "Tab 2"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "2c9efde7-6b22-43cd-82a0-5d416019d19d",
                        "type": "label",
                        "displayName": "Tab2",
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            },
            {
              "container": {
                "properties": {
                  "id": "0a1e3767-9d98-48eb-9bd3-d2ed1a792563",
                  "type": "tabItem",
                  "displayName": "Tab 3"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "4af5a934-0e84-4b9b-af68-f59298ebe5b8",
                        "type": "label",
                        "displayName": "Tab3",
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            },
            {
              "container": {
                "properties": {
                  "id": "baabb30b-a751-4afd-b3a3-c651440ecd9e",
                  "type": "tabItem",
                  "displayName": "Tab 4"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "4637e4b9-d154-4522-8d89-9b891d3acf71",
                        "type": "label",
                        "displayName": "Tab4",
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            },
            {
              "container": {
                "properties": {
                  "id": "1104e41b-657a-4f90-a32e-0cebb6342015",
                  "type": "tabItem",
                  "displayName": "Cabecero5"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "9da3ec7a-c3e1-4f1d-a315-9eb3aef029c2",
                        "type": "label",
                        "displayName": "Cabecero 5",
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            },
            {
              "container": {
                "properties": {
                  "id": "94eed267-80cf-4378-a8de-1703a1d76b3b",
                  "type": "tabItem",
                  "displayName": "Cabecero6"
                },
                "elements": [
                  {
                    "render": {
                      "properties": {
                        "id": "5fa39aa8-70b6-429a-8512-784dd90da9b1",
                        "type": "label",
                        "displayName": "Cabecero6",
                        "helpText": ""
                      }
                    }
                  }
                ]
              }
            }
          ]
        }
      }
    ],
    "buttons": [
      {
        "button": {
          "properties": {
            "id": "ec0a5a02-0f81-44a7-8d64-ac090a01d61b",
            "caption": "render-form-button-save",
            "actions": [
              "submitData",
              "refresh"
            ]
          }
        }
      },
      {
        "button": {
          "properties": {
            "id": "ba81c7af-0d83-4139-879d-b040f0f81bd7",
            "caption": "render-form-button-next",
            "actions": [
              "validate",
              "submitData",
              "next",
              "routing"
            ]
          }
        }
      }
    ],
    "sessionId": "ukjwumqywpw4rbkmzwnc0wqt",
    "pageCacheId": 433001510
  },
  "type": "form"
}