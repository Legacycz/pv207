/**
 * Created by FernandoL on 30/09/2014.
 */

bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
)
    .then(function () {
        // DUMMIES
        $.mockjax(function (settings) {
            if (settings.dataType == "text") {
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return "false";
                }
            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/totalizer/dummy.rendering.totalizer.txt",
                        transform: function (response, originalSettings) {
                            return response;
                        }
                    };
                }
            }

            if (settings.url.indexOf("Rest/Handlers/MultiAction") > -1) {
                if (settings.data.h_action === "multiaction") {
                    var data = JSON.parse(settings.data.h_actions);
                    if (data[0].h_action == "PROCESSPROPERTYVALUE") {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/totalizer/dummy.rendering.multiaction.txt",
                            transform: function (response, originalSettings) {
                                var arrayResponse = [];
                                response[0].tag = data[0].h_tag;
                                arrayResponse.push(response);
                                return arrayResponse[0];
                            }
                        };
                    }
                }
            }

        });
    });