
 /*
*   Name: Bizagi Rendering Smartphone numberScientificNotation e2e mock json
*   Author: Luis Cabarique - LuisCE
*   Comments:
*   -   This script will define the e2e mock json class to define the Scientific number notation services
*/

 bizagi.loader.loadFile(
     { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
     { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
 )
     .then(function () {

         //$.mockjaxSettings.responseTime = 5000; //4000; //10; //2000;
         // DUMMIES
         $.mockjax(function (settings) {
             if (settings.dataType == "text") {
                 if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                     return "false";
                 }
             }

             if (settings.dataType == "json") {
                 if (!BIZAGI_ENABLE_MOCKS) return;
                 if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                     if(CONTROL_EDITABLE) {
                         return {
                             mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/numberScientificNotation/dummy.rendering.numberScientificNotation.txt",
                             transform: function (response, originalSettings) {
                                 return response;
                             }
                         };
                     }
                     else{
                         return {
                             mockjson: "jquery/workportalflat/smartphone/render/test/data/renders/numberScientificNotation/dummy.rendering.numberScientificNotation.noeditable.txt",
                             transform: function (response, originalSettings) {
                                 return response;
                             }
                         };
                     }
                 }
             }
         });
     });