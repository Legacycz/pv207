/**
* Created by LuisCE on 12/06/2014.
*/
/*
*   Name: BizAgi Smartphone Dialog Implementation
*   Author: luisce
*   Comments:
*   -   This script will shows a component inside a modal dialog
*/

// Extends itself
$.Class.extend("bizagi.workportal.widget.dialog", {

}, {

    /*
    *   Constructor
    */
    init: function (params) {
        this.dataService = params.dataService;
        this.workportalFacade = params.workportalFacade;
    },

    /*
    *   Render the Dialog
    *   Returns a deferred
    */
    renderDialog: function (params) {
        var self = this;
        if ($('#dialog').length > 0) {
            self.close();
        }
        var defer = new $.Deferred();
        var routingWidget = new bizagi.workportal.widget.routing();

        routingWidget.renderContent(params);

        var routingTemplate = routingWidget.getContent();
        var dialogTemplate = kendo.template(bizagi.templates.services.service.cachedTemplates["dialog-tmpl"], { useWithBlock: false });

        //TODO internationalization
        dialogTemplate = dialogTemplate({ 'content': routingTemplate });

        $('body').append(dialogTemplate);

        routingWidget.setContent($('#dialog'));
        routingWidget.postRender({ 'close': self.close });

        var dialog = $("#dialog").kendoMobileModalView();
        var modalView = dialog.data("kendoMobileModalView");

        modalView.open();

        /* istanbul ignore next: untestable */
        modalView.adjustSize = function() {
            // Adjust summary popup to window size            
            var maxHeight = 0;            

            var listHeight = $('#ui-bizagi-wp-app-inbox-activities-routing-wrapper', this.element).height();
            var maxWidth = $('#ui-bizagi-wp-app-inbox-activities-routing-wrapper', this.element).width();

            var popupHeight = $('.text-description-title', this.wrapper).height() + $('.wp-routing-container-fother', this.wrapper).height();

            var windowHeightSize = window.innerHeight;
            var windowWidthSize = window.innerWidth;

            if (listHeight + popupHeight >= windowHeightSize) {
                maxHeight = windowHeightSize - 20;
            } else {
                maxHeight = listHeight + popupHeight + 10;
            }

            var top = ((windowHeightSize - maxHeight) / 2);
            var left = ((windowWidthSize - maxWidth) / 2);

            this.wrapper.parent().css('top', top);
            this.wrapper.parent().css('left', left);

            this.wrapper.parent().css('height', maxHeight);
            $("#dialog").css('height', maxHeight);
        };

        modalView.adjustSize();

        /* istanbul ignore next: untestable */
        window.addEventListener("orientationchange", function () {
            // Desbindiar
            if (bizagi.util.isAndroid()) {
                setTimeout(function () {
                    modalView.adjustSize();
                }, 500);
            } else {
                modalView.adjustSize();
            }
        }, false);
    },

    register: function (widget) {
    },

    /*
    *   Shows the dialog box in the browser
    *   Returns a promise that the dialog will be closed
    */
    showDialogBox: function (dialogBox, params) {
    },

    /*
    *   Close dialog
    */
    close: function () {
        var dialog = $("#dialog");
        dialog.data("kendoMobileModalView").close();
        dialog.data("kendoMobileModalView").destroy();
        $(dialog).remove();
    }
});
