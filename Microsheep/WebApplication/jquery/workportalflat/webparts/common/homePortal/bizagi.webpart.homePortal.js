/**
*   Name: BizAgi Test
*   Author: Oscar Osorio
*   *   Comments:
*   -   This script will define a hello world test
*/
bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.homePortal", {
    DEVICE_TABLET_IOS: "tablet_ios",
    DEVICE_TABLET_ANDROID: "tablet_android"
}, {
    /**
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        this._super(workportalFacade, dataService, initialParams);
        self.homePortalFramework = self.getHomeportalFrameworkIntance();
    },
    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        // Status of connection on tablet
        if (bizagi.util.isTabletDevice()) {
            self.hasOfflineForm = bizagi.util.hasOfflineForm();
            self.modeInbox = self.dataService.online || !self.hasOfflineForm ? "inbox" : "true";

            // Define initial Local Storage
            if (bizagi.util.getItemLocalStorage("inputtray") === null) {
                bizagi.util.setItemLocalStorage("inputtray", self.modeInbox);
            }

        } else {
            BIZAGI_ENABLE_OFFLINE_FORMS = false;
            self.hasOfflineForm = false;
        }

        self.content = self.homePortalFramework.loadComponentTemplate('homePortal-tmpl', 'homePortal', { hasOfflineForm: self.hasOfflineForm });
        deferred.resolve(self.content);

        return deferred.promise();
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        return self.homePortalFramework.loadComponents('homePortal', self.content)
            .done(function () {
                $(document).bind('webpartsLoaded', function (params) {
                    bizagi.kendoMobileApplication.bizagiNavigate("#homePortal");
                });
            });
    },

    /**
    custom method for webpart
    */
    configureHandlers: function () {
        var self = this;
    }
});
