/**
*   Name: BizAgi Webpart taskFeed
*   Author: RicharU
*   Comments:
*   -   This script will define a base class to task feed
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.taskFeed", {
    PAGE_SIZE: 10,
    PAGINATOR_MAX_SIZE: 40,
    NUMBER_OF_FIELDS: -1
}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;


        // Call base
        this._super(workportalFacade, dataService, initialParams);

        self.initTaskfeed = true;

        self.properties = {};
        self.properties.page = 1;
        self.properties.pageSize = self.Class.PAGE_SIZE;
        self.properties.totalPages = 0;
        self.properties.elementPages = self.Class.PAGE_SIZE;
        self.properties.numberOfFields = self.Class.NUMBER_OF_FIELDS;
        self.properties.idWfClass = -1;
        self.properties.data = [];
        self.routingCalled = true; // the first time is true to allow load data
        self.cacheFavorites = {};
        self.isPaginatorActive = false;

        // Offline
        self.isTabletDevice = bizagi.util.isTabletDevice();
        self.enableOfflineForm = bizagi.util.hasOfflineForm();
        self.online = self.isTabletDevice ? self.dataService.online : true;

    },
    /**
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        bizagi.chrono.initAndStart("taskFeed-all", true);

        var templateHeader = kendo.template(self.getTemplate("main-taskFeed-kendo-tmpl"), { useWithBlock: false });

        self.content = templateHeader({});
        deferred.resolve(self.content);

        return deferred.promise();
    },

    /**
    *   Customize the web part in each device
    */
    postRender: function (params) {
        var self = this;

        self.dataSource = new kendo.data.DataSource({
            group: { field: "group" }
        });

        $(document).bind('webpartsLoaded', function (params) {
            self.initTaskFeedKendoView();
            if (!self.initTaskfeed) {
                self.updateDataList();
            }
        });
    },

    /**
    * Init faskfeed list view when task feed is displayed
    */
    initTaskFeedKendoView: function () {
        var self = this;

        if (self.initTaskfeed && typeof ($(self.canvas).data('kendoMobileView')) !== 'undefined') {
            self.initTaskfeed = false;

            var templateContent = self.getTemplate('item-taskFeed-kendo-tmpl');
            self.isFirstElement = true;

            $("#items-content-taskFeed", self.content).kendoMobileListView({
                dataSource: self.dataSource,
                headerTemplate: '#=(value.split("|"))[1]#',
                fixedHeaders: true,
                template: templateContent,
                selectable: "single",
                pullToRefresh: true,
                pullTemplate: "<div class='km-loader-widget-pull'></div>",                
                refreshTemplate: "<div class='km-loader-widget-refresh'></div>",
                releaseTemplate: "<div class='km-loader-widget-refresh'></div>",
                pullParameters: function (item) {
                    /*istanbul ignore next*/
                    return self.updateDataListByPullRefresh(self.options);
                },
                click: function (e) {
                    e.preventDefault();

                    // if not exist data
                    if (typeof (e.dataItem) === 'undefined') return;

                    self.showSummary(e);
                },
                dataBound: function (e) {

                    $(".bz-wp-group-container .km-list li:first-child").each(function () {
                        var that = this;
                        var status = $(that.firstChild).data("status");                        

                        $(that).addClass(status).attr("data-status", status);

                        //if tablet, show first element summary
                        if (self.isTabletDevice && $("#items-content-taskFeed").data("kendoMobileListView") && $("#items-content-taskFeed").data("kendoMobileListView").dataSource) {
                            var summaryParam = {};

                            summaryParam.dataItem = $("#items-content-taskFeed").data("kendoMobileListView").dataSource.data()[0];
                            if (summaryParam.dataItem && self.isFirstElement) {
                                self.isFirstElement = false;
                                self.showSummary(summaryParam);
                            }
                        }
                    });
                }
            });

            self.configureHandlers();
        }
    },

    updateDataListByPullRefresh: function (params) {
        var self = this;
        var deferred = new $.Deferred();
        var pullParemetersOptions;

        if (!self.isPaginatorActive) {
            pullParemetersOptions = $.extend(params, { 'page': 1, 'pageSize': params.page * params.pageSize });
        } else {
            pullParemetersOptions = $.extend(params, { 'page': 1, 'pageSize': self.Class.PAGE_SIZE });
            self.properties.page = 1;
        }

        // Offline type of tray
        pullParemetersOptions.inputtray = bizagi.util.inputTray(self.online);      

        $.when(
            self.dataService.getCasesListBeta(pullParemetersOptions)
        ).done(function (data) {

            if (!self.isTabletDevice || (self.isTabletDevice && self.online)) {
                self.totalPages = data.totalPages;
                $(".bz-wp-taskfeed-paginator-pages span").html(self.getResource("render-grid-page") + " " + data.page + " " + self.getResource("render-grid-conector") + " " + data.totalPages);

                if (data.totalPages > 4) {
                    $(".bz-wp-taskfeed-paginator-container").css("display", "block");
                    self.disableLoadMore();
                }
            } else if (self.isTabletDevice && !self.online) {
                $(".bz-wp-taskfeed-paginator-container").css("display", "none");
            }

            deferred.resolve(data);

            if (data.elements && data.elements.length == 0) {
                $("#items-content-taskFeed", self.content).html("<li class='bz-wp-taskfeed-no-records'>" + self.getResource("workportal-general-no-records-found") + "</li>");
            }

            self.cacheFavorites = {};            
        }).fail(function (error) {
            bizagi.debug(error.responseText);

            if ($("#items-content-taskFeed li", self.content).length == 0) {
                $("#items-content-taskFeed", self.content).html("<li class='bz-wp-taskfeed-no-records'>" + self.getResource("workportal-general-no-records-found") + "</li>");
            }

            deferred.reject("");
        });

        return deferred;
    },

    loadDataList: function (params) {
        var self = this;
        var deferred = new $.Deferred();
        params = params || {};
        self.options = self.options || {};

        bizagi.chrono.initAndStart("taskFeed-data", true);

        self.options.numberOfFields = self.properties.numberOfFields;
        self.options.page = params.page || self.properties.page;

        self.options.idWfClass = self.properties.idWfClass;

        if (!self.isPaginatorActive) {
            self.options.pageSize = params.pageSize || self.properties.pageSize;
        } else {
            self.options.pageSize = self.Class.PAGE_SIZE;
        }

        // Offline type of tray
        self.options.inputtray = bizagi.util.inputTray(self.online);

        $.when(
            self.dataService.getCasesListBeta(self.options)
        ).done(function(data) {

            if (!self.isTabletDevice || (self.isTabletDevice && self.online)) {
                self.totalPages = data.totalPages;
                $(".bz-wp-taskfeed-paginator-pages span").html(self.getResource("render-grid-page") + " " + data.page + " " + self.getResource("render-grid-conector") + " " + data.totalPages);

                if (self.isPaginatorActive && data.elements.length * data.totalPages > self.Class.PAGINATOR_MAX_SIZE) {
                    self.options.pageSize = 10;
                }
            }

            if (params.more) {
                // Load more
                self.dataSourceNew.pushCreate(data.elements);
                deferred.resolve();
            } else {
                // Refresh data
                self.dataSourceNew = new kendo.data.DataSource({
                    transport: {
                        read: function(options) {
                            $.when(options.data).done(function(dataDeferred) {
                                var listData = (dataDeferred.elements) ? dataDeferred : data;
                                options.success(listData.elements);
                                bizagi.chrono.stopAndLog("taskFeed-data");
                                bizagi.chrono.stopAndLog("taskFeed-all");
                            });
                        }
                    },
                    group: { field: "group" }
                });

                deferred.resolve(self.dataSourceNew);
            }

            if (data.page >= data.totalPages) {
                self.disableLoadMore();
            }

            if ((!self.isTabletDevice || (self.isTabletDevice && self.online == true)) && (data.totalPages > 4 || (data.elements.length * data.totalPages) > self.Class.PAGINATOR_MAX_SIZE)) {
                $(".bz-wp-taskfeed-paginator-container").css("display", "block");
                $(".bz-wp-taskfeed-paginator-container").data("paginator", true);
                self.disableLoadMore();
                self.isPaginatorActive = true;

                if (self.options.pageSize > self.Class.PAGE_SIZE) {
                    $("#items-content-taskFeed").data("kendoMobileListView").dataSource.data().splice(10, 10);
                }
            } else if (self.isTabletDevice && !self.online) {
                $(".bz-wp-taskfeed-paginator-container").css("display", "none");
                $(".bz-wp-taskfeed-paginator-container").data("paginator", false);
            }
        }).fail(function(error) {
            bizagi.logError(error.responseText);

            self.disableLoadMore();

            bizagi.util.mobility.stopLoading();
            deferred.reject("");
        });

        return deferred.promise();
    },

    updateDataList: function() {
        var self = this;

        if (self.routingCalled) {
            var params = {
                page: 1,
                pageSize: (self.options) ? (self.options.page * self.options.pageSize) : (self.properties.page * self.properties.pageSize)
            };

            bizagi.util.mobility.startLoading();

            $.when(self.loadDataList(params)).done(function(data) {
                var dataSource = data;
                if (dataSource) {
                    $("#items-content-taskFeed", self.content).data("kendoMobileListView").setDataSource(dataSource);
                }

                if ($("#items-content-taskFeed li", self.content).length == 0) {
                    $("#items-content-taskFeed", self.content).html("<li class='bz-wp-taskfeed-no-records'>" + self.getResource("workportal-general-no-records-found") + "</li>");
                }

                bizagi.util.mobility.stopLoading();
            }).fail(function(error) {
                bizagi.logError(error.responseText);

                if ($("#items-content-taskFeed li", self.content).length == 0) {
                    $("#items-content-taskFeed", self.content).html("<li class='bz-wp-taskfeed-no-records'>" + self.getResource("workportal-general-no-records-found") + "</li>");
                }

                bizagi.util.mobility.stopLoading();
            });

            self.routingCalled = false;
        }
    },

    /**
    * Disable Button Load More
    */
    disableLoadMore: function () {
        var self = this;

        $('#loadMoreTaskFeed', self.content).hide();
    },

    /**
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        $("#taskfeedPaginatorFirst", self.content).bind("click", function (e, callback) {
            self.properties.page = 1;
            self.isFirstElement = true;

            bizagi.util.mobility.startLoading();

            $("#items-content-taskFeed").data("kendoMobileListView").dataSource.data([]);

            $.when(self.loadDataList({ 'more': true })).done(function (data) {
                if (callback) {
                    callback();
                }
                bizagi.util.mobility.stopLoading();
            });
        });

        $("#taskfeedPaginatorBack", self.content).bind("click", function (e, callback) {
            if (self.totalPages && self.properties.page > 1) {
                self.properties.page -= 1;
                self.isFirstElement = true;

                bizagi.util.mobility.startLoading();

                $("#items-content-taskFeed").data("kendoMobileListView").dataSource.data([]);

                $.when(self.loadDataList({ 'more': true })).done(function (data) {
                    if (callback) {
                        callback();
                    }
                    bizagi.util.mobility.stopLoading();
                });
            }
        });

        $("#taskfeedPaginatorNext", self.content).bind("click", function (e, callback) {
            if (self.totalPages && self.totalPages > self.properties.page) {
                self.properties.page += 1;
                self.isFirstElement = true;

                bizagi.util.mobility.startLoading();

                $("#items-content-taskFeed").data("kendoMobileListView").dataSource.data([]);

                $.when(self.loadDataList({ 'more': true })).done(function (data) {
                    if (callback) {
                        callback();
                    }
                    bizagi.util.mobility.stopLoading();
                });
            }
        });

        $("#taskfeedPaginatorLast", self.content).bind("click", function (e, callback) {
            if (self.totalPages) {
                self.properties.page = self.totalPages;
                self.isFirstElement = true;

                bizagi.util.mobility.startLoading();

                $("#items-content-taskFeed").data("kendoMobileListView").dataSource.data([]);

                $.when(self.loadDataList({ 'more': true })).done(function (data) {
                    if (callback) {
                        callback();
                    }
                    bizagi.util.mobility.stopLoading();
                });
            }
        });

        $("#loadMoreTaskFeed", self.content).bind("click", function (e, callback) {

            self.properties.page += 1;
            bizagi.util.mobility.startLoading();

            $.when(self.loadDataList({ 'more': true })).done(function (data) {
                if (callback) {
                    callback();
                }

                bizagi.util.mobility.stopLoading();
            });
        });

        self.subscribe("bz-update-list-cases", function (e, params) {
            params = params || {};
            self.routingCalled = true;
            /* istanbul ignore next */
            if (params.updateDataList) {
                self.updateDataList();
            }
        });
    },

    /**
    Show Summary task feed
    */
    showSummary: function (params) {
    },

    setFavorite: function (favorite, idCase) {
        var self = this;

        if ($(favorite).attr("data-id-object") === "") {

            favoriteOptions = { idObject: idCase, favoriteType: "CASES" };

            $.when(
                self.dataService.addFavorite(favoriteOptions)
            ).done(function (favoritesData) {
                $(favorite).attr("data-icon", "favorites-full");
                $('.km-icon', favorite).removeClass('km-favorites').addClass('km-favorites-full');
                $(favorite).attr("data-id-object", favoritesData["idFavorites"]);
                self.cacheFavorites[idCase] = { "add": true, "idFavorites": favoritesData["idFavorites"] };
            });
        } else {
            favoriteOptions = { idObject: $(favorite).attr("data-id-object"), favoriteType: "CASES" };

            $.when(
                self.dataService.delFavorite(favoriteOptions)
            ).done(function (favoritesData) {
                if (favoritesData["deleted"] == "true") {
                    $(favorite).attr("data-icon", "favorites");
                    $('.km-icon', favorite).removeClass('km-favorites-full').addClass('km-favorites');
                    $(favorite).attr("data-id-object", "");
                    self.cacheFavorites[idCase] = { "add": false, "idFavorites": "" };
                }
            });
        }
    },

    isFavorite: function (favorite) {
        var self = this;

        if ($(favorite).attr("data-id-object") === "") {
            $(favorite).attr("data-icon", "favorites");
            $('.km-icon', favorite).removeClass('km-favorites-full').addClass('km-favorites');
        } else {
            $(favorite).attr("data-icon", "favorites-full");
            $('.km-icon', favorite).removeClass('km-favorites').addClass('km-favorites-full');
        }
    }
});
