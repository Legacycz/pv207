
bizagi.loader.loadFile(
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjax"), "coverage": false },
    { "src": bizagi.getJavaScript("common.base.dev.jquery.mockjson"), "coverage": false }
)
    .then(function () {

        //$.mockjaxSettings.responseTime = 5000; //4000; //10; //2000;
        // DUMMIES
        $.mockjax(function (settings) {
            if (settings.dataType == "text") {
                if (settings.url.indexOf("Rest/Handlers/Render") > -1) {
                    return "false";
                }
            }

            if (settings.dataType == "json") {
                if (!BIZAGI_ENABLE_MOCKS) return;
                if (settings.url.indexOf("Rest/Processes/RecentProcesses") > -1) {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.recentprocesses.txt"
                    };
                }
            }

            // New Case Webpart
            if (settings.url.indexOf("Rest/Processes/Categories") > -1) {
                if (settings.data.idCategory) {
                    if (settings.data.idCategory === "1000") {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.allprocess.txt"
                        };
                    }
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.txt"
                    };
                } else if (settings.data.idApp) {
                    if (settings.data.idApp === 1000) {
                        return {
                            mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.app.allprocess.txt"
                        };
                    }
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.app.txt"
                    };
                } else {
                    return {
                        mockjson: "jquery/workportalflat/smartphone/newcase/test/data/dummy.categories.group.txt"
                    };
                }
            }

        });
    });