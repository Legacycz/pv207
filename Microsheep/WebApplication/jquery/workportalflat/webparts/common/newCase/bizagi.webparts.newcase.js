﻿/*
*   Name: Bizagi newcase webpart
*   Author: luisce
*   Comments:
*   -   This script will define the newcase webpart
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.newcase", {}, {
    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base        
        self._super(workportalFacade, dataService, initialParams);

        self.isTabletDevice = bizagi.util.isTabletDevice();
        self.online = self.isTabletDevice ? self.dataService.online : true;
    },
    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        var deferred = $.Deferred();

        var newCaseTemplate = kendo.template(self.getTemplate('newcase'), { useWithBlock: false });

        self.content = newCaseTemplate({ online: self.online });

        $("#content-newCase-drawer", self.canvas).empty();
        $("#content-newCase-drawer", self.canvas).html(self.content);
        $('.km-content:visible').data('kendoMobileScroller').reset();

        var buttongroup = $('#select-process-type', self.canvas).kendoMobileButtonGroup();
        var breadCrumb = params.breadCrumb || [{ name: bizagi.localization.getResource("workportal-widget-newcase-title") }];

        self.changeBreadcrumb(breadCrumb);

        var status = params.status || "recent";

        // Offline
        if (self.isTabletDevice && !self.online) {
            status = "all";
        }

        if (status === "recent" || !self.online) {
            buttongroup.data("kendoMobileButtonGroup").select(0);
        } else { /* istanbul ignore next: untestable */
            buttongroup.data("kendoMobileButtonGroup").select(1);
        }

        var templateParams = { status: status, idApp: params.idApp, idCategory: params.idCategory };

        $(".bz-newcase-list", self.canvas).kendoMobileListView({
            filterable: false,
            template: self.getListTemplate(templateParams),
            click: function (element) {
                /* istanbul ignore next: untestable */
                var renderJson = self.selectListElement(element, breadCrumb);

                /* istanbul ignore next: untestable */
                if (renderJson.idWfClass) {
                    if (bizagi.util.parseBoolean(bizagi.currentUser.isMultiOrganization)) {
                        self.createOrganizationList(renderJson);
                    } else {
                        self.createNewCase(renderJson);
                    }
                } else {
                    self.renderContent(renderJson);
                }
            }
        });

        var dataSource = {};
        bizagi.util.mobility.startLoading();

        $.when(self.getData(templateParams))
            .done(function (data) {
                dataSource = data;

                $(".bz-newcase-list", self.canvas).data("kendoMobileListView").setDataSource(dataSource);
                bizagi.util.mobility.stopLoading();

                deferred.resolve();

            }).fail(function (error) {
                bizagi.util.mobility.stopLoading();
                deferred.resolve();
            });

        /* istanbul ignore next: untestable */
        $(".bz-grouped-buttons-all, .bz-grouped-buttons-recent", self.canvas).click(function () {
            //remove search field
            $(".km-filter-form", self.canvas).remove();
            $(".bz-newcase-list", self.canvas).data("kendoMobileListView").destroy();

            if ($(this).hasClass("bz-grouped-buttons-all")) {
                self.renderContent({ "status": "all" });
            } else {
                self.renderContent({ "status": "recent" });
            }
        });

        //sets the search
        /* istanbul ignore next: untestable */
        $(".bz-newcase-search", self.canvas).keyup(function () {
            dataSource._filter = {
                logic: "or",
                filters: [
                    {
                        ignoreCase: true,
                        field: 'appName',
                        operator: "contains",
                        value: this.value
                    },
                    {
                        ignoreCase: true,
                        field: "categoryName",
                        operator: "contains",
                        value: this.value
                    },
                    {
                        ignoreCase: true,
                        field: "displayName",
                        operator: "contains",
                        value: this.value
                    }
                ]
            };

            $(".bz-newcase-list", self.canvas).data("kendoMobileListView").setDataSource(dataSource);

        });

        return deferred.promise();
    },

    selectListElement: function (element, breadCrumb) {
        var renderJson = {};

        if ($("a", element.item).data("appid")) {

            $(".bz-newcase-list").data("kendoMobileListView").destroy();
            $('.km-filter-form').remove();

            breadCrumb.push({ name: $("#categoryName", element.item).val(), value: $("a", element.item).data("appid") });
            renderJson = { "status": "all", idApp: $("a", element.item).data("appid"), breadCrumb: breadCrumb };

        } else if ($("#isProcess", element.item).val() === "false") {

            $(".bz-newcase-list").data("kendoMobileListView").destroy();
            $('.km-filter-form').remove();

            breadCrumb.push({ name: $("#categoryName", element.item).val(), value: $("#idCategory", element.item).val() });
            renderJson = { "status": "all", idCategory: $("#idCategory", element.item).val(), breadCrumb: breadCrumb };

        } else {

            var idClass = $("#idWFClass", element.item).val() ? $("#idWFClass", element.item).val() : $("#idCategory", element.item).val();
            var idCategory = $("#idCategory", element.item).val();

            var hasOfflineForm = bizagi.util.parseBoolean($("#hasOfflineForm", element.item).val());

            var newCaseParams = {};
            newCaseParams.idWfClass = idClass;
            newCaseParams.displayName = element.item.text().trim();

            // Allow user to access offline form
            if (hasOfflineForm && this.isTabletDevice && bizagi.util.hasOfflineForm()) {
                newCaseParams.idOrganization = null;
                newCaseParams.isOfflineForm = hasOfflineForm;
                newCaseParams.process = newCaseParams.displayName;
            }

            // Define last idcategory
            bizagi.idCategory = idCategory;

            $("#newCase-drawer").data("kendoMobileDrawer").hide();
            bizagi.util.mobility.startLoading();
            renderJson = newCaseParams;
        }

        return renderJson;
    },

    getData: function (params) {
        var self = this;
        var getParams = {};
        var deferred = new $.Deferred();

        if (params.idCategory) {
            getParams = { idCategory: params.idCategory };
        } else if (params.idApp) {
            getParams = { idApp: params.idApp };
        }

        if (params.status === "recent") {
            $.when(
                self.dataService.getRecentProcesses()
            ).done(function (data) {
                self.dataSource = new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            options.success(data.processes);
                        }
                    }
                });

                deferred.resolve(self.dataSource);

            }).fail(function (error) {
                deferred.reject($.parseJSON(error.responseText));
            });
        } else {
            $.when(
                self.dataService.getCategories(getParams)
            ).done(function (data) {
                self.dataSource = new kendo.data.DataSource({
                    transport: {
                        read: function (options) {
                            options.success(data.category);
                        }
                    }
                });

                deferred.resolve(self.dataSource);

            }).fail(function (error) {
                deferred.reject($.parseJSON(error.responseText));
            });
        }

        return deferred.promise();
    },

    getListTemplate: function (params) {
        var self = this;
        var template = "";

        if (params.status === "recent") {
            template = self.getTemplate('newCase-categories-recent-process');
        } else {
            template = self.getTemplate('newCase-categories-tree');
        }

        return template;
    },

    changeBreadcrumb: function (params) {
        var self = this;
        var breadCrumb = kendo.template(self.getTemplate('newcase-breadcrum'), { useWithBlock: false });

        $('.bz-newcase-title', self.canvas).html(breadCrumb(params));
        $('.bz-newcase-title', self.canvas).click(function () {
            /* istanbul ignore next: untestable */
            self.renderContent(self.clickBreadcrumb(params));
        });
    },

    clickBreadcrumb: function (params) {
        var breadCrumb = {};

        if (params.length >= 3) {
            params.pop();
            breadCrumb = { "status": "all", idCategory: params[params.length - 1].value, breadCrumb: params };
        } else if (params.length === 2) {
            params.pop();
            breadCrumb = { "status": "all", idApp: params[params.length - 1].value, breadCrumb: params };
        } else if (params.length === 1) {
            breadCrumb = { "status": "all" };
        }
        return breadCrumb;
    },

    createNewCase: function (newCaseParams) {
        var self = this;
        var deferred = new $.Deferred();

        bizagi.util.mobility.startLoading();

        self.dataService.createNewCase(newCaseParams)
            .done(function (response) {
                self.publish("changeCase", response);
                self.publish("bz-update-list-cases");

                // Clear content data on summary items of taskFeed
                if (self.isTabletDevice) {
                    bizagi.webpart.publish("bz-summary-item-clear");
                }

                // Activa flag para actualizar lista de casos en la proxima visualización
                self.publish("bz-update-list-cases", { updateDataList: true });

                bizagi.util.mobility.stopLoading();
                deferred.resolve();
            }) /* istanbul ignore next: untestable fail */
            .fail(function (msg) {

                console.log("error cargando la forma");
                bizagi.util.mobility.stopLoading();

                var response = $.parseJSON(msg.responseText);
                bizagi.showMessageBox(response.message, "Error");

                deferred.fail();
            });
        return deferred;
    },

    createOrganizationList: function (newCaseParams) {
        var self = this;
        var deferred = new $.Deferred();

        // Initialize adjusted modalview
        self.isAdjusted = false;

        bizagi.util.mobility.startLoading();

        self.dataService.getOrganizationsList()
            .done(function (data) {

                var multiorganizationTemplate = kendo.template(self.getTemplate("newCase-modal-multiorganization"), { useWithBlock: false });
                var multiorganizationModal = $(multiorganizationTemplate({ list: data.response, idWfClass: newCaseParams.idWfClass }));

                multiorganizationModal.kendoMobileModalView({
                    close: function (args) {
                        this.destroy();
                        this.close();
                        this.element.remove();
                    }
                });

                // Configure Handler modal new Case
                self.configureHandlers();

                var modalView = $(".bz-wp-modal-organization").data("kendoMobileModalView");
                $(".bz-wp-modal-organization-footer div.km-rightitem").addClass("inactive");

                modalView.open();

                /* istanbul ignore next: untestable */
                modalView.adjustSize = function () {
                    // Adjust summary popup to window size                                                    
                    var maxHeight;

                    var listHeight = $(".bz-wp-modal-organization-list", this.element).height();

                    // Do nothing
                    if (listHeight === 0) return;

                    var maxWidth = $(".bz-wp-modal-organization-content", this.element).width();
                    var popupHeight = $(".bz-wp-modal-organization-header", this.wrapper).height() + $(".bz-wp-modal-organization-footer", this.wrapper).height();
                    var windowHeightSize = window.innerHeight;
                    var windowWidthSize = window.innerWidth;

                    if (listHeight + popupHeight >= windowHeightSize) {
                        maxHeight = windowHeightSize - 20;
                    } else {

                        maxHeight = listHeight + popupHeight;

                        if (bizagi.util.detectDevice() !== "tablet_ios") {
                            maxHeight += !self.isAdjusted ? 20 : (Math.abs(window.orientation) === 90) ? 8 : 35;
                        } else {
                            maxHeight += (Math.abs(window.orientation) === 90) ? 20 : 22;
                        }
                    }

                    var top = (windowHeightSize - maxHeight) / 2;
                    var left = (windowWidthSize - maxWidth) / 2;

                    this.wrapper.parent().css("top", top);
                    this.wrapper.parent().css("left", left);
                    this.wrapper.parent().css("height", maxHeight);

                    self.isAdjusted = true;
                };

                modalView.adjustSize();

                /* istanbul ignore next: untestable */
                window.addEventListener("orientationchange", function () {

                    if (bizagi.util.isAndroid()) {
                        setTimeout(function () {
                            modalView.adjustSize();
                        }, 500);
                    } else {
                        modalView.adjustSize();
                    }
                }, false);

                bizagi.util.mobility.stopLoading();

                deferred.resolve();

            }).fail(function (msg) {
                var message;

                bizagi.util.mobility.stopLoading();

                var responseText = msg.responseText;

                try {
                    message = JSON.parse(responseText);
                    message = message.message || responseText;
                } catch (e) {
                    message = responseText;
                }

                bizagi.showMessageBox(message, "Error");

                deferred.fail();
            });

        return deferred;
    },

    /**
    * Event Handlers
    */
    configureHandlers: function () {
        var self = this;

        /* Modal Summary*/
        // .bz-wp-modal-organization-close
        $(".bz-wp-modal-organization").delegate(".bz-wp-modal-organization-footer .km-leftitem", "click", function (event) {
            $(".bz-wp-modal-organization").data("kendoMobileModalView").close();
        });

        $(".bz-wp-modal-organization").delegate(".bz-wp-modal-organization-list > li", "click", function (event) {
            var item = $(event.target);

            // Remove others selected
            $(".bz-wp-modal-organization-list > li").removeClass("bz-wp-modal-organization-list-item-selected");

            // Enable Create new case
            if ($(".bz-wp-modal-organization-footer div.km-rightitem").hasClass("inactive")) {
                $(".bz-wp-modal-organization-footer div.km-rightitem").removeClass("inactive");
                $(".bz-wp-modal-organization-footer div.km-rightitem").addClass("active");
            }

            // Selected item
            item.addClass("bz-wp-modal-organization-list-item-selected");
        });

        // .bz-wp-modal-organization-newcase
        $(".bz-wp-modal-organization").delegate(".bz-wp-modal-organization-footer .km-rightitem", "click", function (event) {

            // If not selected item not action
            if ($(".bz-wp-modal-organization-footer div.km-rightitem").hasClass("inactive")) return;

            var newCaseParams = {};
            newCaseParams.idWfClass = $(this).closest(".bz-wp-modal-organization").data("workflow");
            newCaseParams.idOrganization = $(".bz-wp-modal-organization-list-item-selected").data("id") || null;

            self.createNewCase(newCaseParams).always(function () {
                bizagi.util.mobility.stopLoading();
            });
        });
    }
});
