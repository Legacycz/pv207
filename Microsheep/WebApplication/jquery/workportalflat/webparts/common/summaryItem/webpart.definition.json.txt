﻿{
  "name": "summaryItem",
  "devices": {
    "tablet_ios": {
      "js": [
        {
          "src": "../../tablet/summaryItem/bizagi.webparts.tablet.summaryItem.js"
        }
      ],
      "css": [
        {
          "src": "../../tablet/summaryItem/bizagi.webpart.tablet.summaryItem.css"
        }
      ],
      "tmpl": [
        {
          "alias": "summary-item-main",
          "src": "../../tablet/summaryItem/summaryItem.tmpl.html#ui-bz-wp-summaryItem-main"
        },
        {
          "alias": "summary-item-content",
          "src": "../../tablet/summaryItem/summaryItem.tmpl.html#ui-bz-wp-summaryItem-content"
        }
      ]
    },
    "tablet_android": {
      "js": [
        {
          "src": "../../tablet/summaryItem/bizagi.webparts.tablet.summaryItem.js"
        }
      ],
      "css": [
        {
          "src": "../../tablet/summaryItem/bizagi.webpart.tablet.summaryItem.css"
        }
      ],
      "tmpl": [
        {
          "alias": "summary-item-main",
          "src": "../../tablet/summaryItem/summaryItem.tmpl.html#ui-bz-wp-summaryItem-main"
        },
        {
          "alias": "summary-item-content",
          "src": "../../tablet/summaryItem/summaryItem.tmpl.html#ui-bz-wp-summaryItem-content"
        }
      ]
    }
  }
}