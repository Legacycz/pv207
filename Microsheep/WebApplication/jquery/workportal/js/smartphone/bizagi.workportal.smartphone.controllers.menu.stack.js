/*
*   Name: BizAgi Workportal Iphone Menu Controller extend the staker for menu notification to widgets
*   Author: Oscar Osorio 
*   Comments: Overrides menu controller class to apply custom stuff just for Iphone device
*/

// Auto extend
bizagi.workportal.controllers.menu.extend("bizagi.workportal.controllers.menu", {}, {
      
//    //selete the type of observer : notify all stack element or notify the last element in the stack
//    setTypeObserver: function (notifyAll) {
//        this.notifyObserverAll = notifyAll;
//    },
//    //use this function to add event to notify when (the back, the button, dinamic button,ect) is touched
//    nextSuscribe: function (key, callback) {
//        var self = this;
//        self.changeContextualButtons("back", { visible: true });
//        return self.observerList.push({ "key": key, "callback": callback, "beforeButtons": self.getActualStateContextualButtons() });
//    },
//    //clean the all suscriber and disable back button
//    emptySuscribers: function () {
//        this.observerList = [];
//        this.changeContextualButtons("back", { visible: false });

//    },
//    //when render execute rule or action and this is execute in the server is necessary recovery the last suscriber
//    undoRemoveLastSuscriber: function () {
//        var self = this;
//        if (self.lastElementRemoveHistory == null)
//            return;
//        self.changeContextualButtons("back", { visible: true });
//        self.observerList.push(self.lastElementRemoveHistory);
//        self.lastElementRemoveHistory = null;
//    },
//    // remove one suscriber and posible to activate the old estate of buttons in menu
//    removeSuscriber: function (key, activeBeforeButtons) {
//        var self = this;
//        var tmpbefore;

//        if (key === undefined) {
//            return;
//        }

//        this.observerList = jQuery.grep(self.observerList, function (value) {
//            if (value.key == key) {
//                tmpbefore = value.beforeButtons;
//                self.lastElementRemoveHistory = value;
//            }
//            return value.key != key;
//        });
//        var acbuttons = (activeBeforeButtons !== undefined) ? activeBeforeButtons : true;
//        if (acbuttons) {
//            self.changeContextualButtons("refresh", tmpbefore);
//        }
//    },
//    //this function reuse the removeSuscriber and remove the last suscribe callback (nextSuscribe)
//    removeLastSuscriber: function (activeBeforeButtons) {
//        var self = this;
//        if (typeof (self.observerList[self.observerList.length - 1]) != "object") {
//            return;
//        }
//        var observer = self.observerList[self.observerList.length - 1];
//        var key = observer.key;
//        self.removeSuscriber(key, activeBeforeButtons);

//    },

//    //obtain object {key,callback,beforeButtons} 
//    getLastSuscriber: function () {

//        var self = this;
//        if (typeof (self.observerList[self.observerList.length - 1]) != "object") {
//            return null;
//        }
//        return self.observerList[self.observerList.length - 1];
//    },

//    //when you click in menu x button its call notify observers and send the parameter action:describe the element clicked,
//    //this function call the callback asign when register (nextSuscribe)
//    notifyObservers: function (action, argsExtend) {
//        if (!this.notifyObserverAll) {
//            this.notifyLastObserver(action, argsExtend);
//            return;
//        }
//        for (var itobserver in this.observerList) {
//            var oblist = this.observerList;
//            var observer = oblist[itobserver];
//            var args = {
//                "index": itobserver,
//                "action": action,
//                "key": observer.key,
//                "lastKey": oblist[oblist.length - 1].key
//            };
//            if (argsExtend) {
//                args = jQuery.extend(args, argsExtend);
//            }

//            self.notifiy(observer.callback, args);
//        }
//    },
//    //this function only call from notify observer and this available when you configure the nofify the last observer (setTypeObserver)
//    //its, call the las suscriber and send the arguments {index,action,key}
//    notifyLastObserver: function (action, argsExtend) {
//        var self = this;
//        if (typeof (this.observerList[this.observerList.length - 1]) != "object") {
//            return;
//        }
//        var observer = this.observerList[this.observerList.length - 1];
//        var index = this.observerList.length - 1;
//        var args = {
//            "index": index,
//            "action": action,
//            "key": observer.key,
//            beforeButtons: observer.beforeButtons
//        };

//        if (argsExtend) {
//            args = jQuery.extend(args, argsExtend);
//        }


//        self.notifiy(observer.callback, args);
//    },
//    //function reusable from notify function and determine is array of callbacks or callback and call with arguments(args)
//    notifiy: function (callbacks, args) {
//        if (jQuery.isArray(callbacks)) {
//            for (var identifier in callbacks)
//                callbacks[identifier](args);
//            return;
//        }
//        callbacks(args);
//    }


});
