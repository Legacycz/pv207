/*
*   Name: BizAgi Workportal Routing Widget Controller
*   Author: Diego Parra
*   Comments:
*   -   This script will define a base class to to define the routing widget
*/

bizagi.workportal.widgets.widget.extend("bizagi.workportal.widgets.routing", {}, {
    /*
    *   Returns the widget name
    */
    getWidgetName: function(){  
        return bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ROUTING;
    },
    
    /*
    *   Renders the content for the current controller
    *   Returns a deferred because it has to load the current user
    */
    renderContent: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("routing");
        var def = new $.Deferred();
        //check subprocess
        if(self.params.data.checkProcess || self.params.data.checkWorkItems){
            $.when(
                self.dataService.getCaseSubprocesses({
                    idCase: self.params.data.idCase
                })            
            ).done(function(process){
                self.params.data.subProcessPersonalized = process["subProcesses"];                        
                // Loads case workitems
                // Solve QA-2197
                var content = self.content = $.tmpl(
                        template,
                        self.params.data,
                        {
                            formatDate: function(item){
                                var fullFormat = bizagi.localization.getResource("dateFormat") + ' ' + bizagi.localization.getResource("timeFormat");
                                var dateObj = bizagi.util.dateFormatter.getDateFromInvariant(item.estimatedSolutionDate);

                                return bizagi.util.dateFormatter.formatDate(dateObj, fullFormat);
                            }
                        }
                );
                def.resolve(content);
                
            });
        }
        return def.promise();
    }        
});
