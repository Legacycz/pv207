﻿/*
*   Name: BizAgi Workportal Graphic Query Widget
*   Author: David Romero
*   Comments:
*   -   This script renders the case workflow
*/

bizagi.workportal.widgets.widget.extend("bizagi.workportal.widgets.graphicquery", {}, {


    init: function (workportalFacade, dataService, params) {
        this.subprocesses = [];
        this.currentTasks = [];
        this.path = [];
        this.callStack = [];
        this.currentWorkflow = {
            idCase: params.data.idCase,
            idWorkflow: params.data.idWorkflow
        };

        this._super(workportalFacade, dataService, params);
    },

    /*
    *   Returns the widget name
    */
    getWidgetName: function () {
        return bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_GRAPHIC_QUERY;
    },

    /*
    *   Renders the content for the current controller
    *   Returns a deferred because it has to load the current user
    */
    renderContent: function () {

        var self = this;
        var tmpl = self.workportalFacade.getTemplate("graphicquery-wrapper");
        var content = self.content = $.tmpl(tmpl);

        return content;
    }
});