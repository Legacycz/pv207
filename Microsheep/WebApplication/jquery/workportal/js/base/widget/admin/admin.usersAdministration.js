﻿/*
*   Name: BizAgi Workportal Graphic Query Widget
*   Author: David Romero
*   Comments:
*   -   This script renders the user administration
*/

bizagi.workportal.widgets.widget.extend("bizagi.workportal.widgets.admin.usersAdministration", {}, {



    init: function (workportalFacade, dataService, params) {

        //Pagination data
        this.maxRows = 18;
        this.maxPages = 10;
        this.currentPage = 1;
        this.orderType = "ASC";
        this.orderField = "";
        this.totalPages = 0;
        this.userData = {};
        this.allowCreation = true;

        this.userLinks = [
            {
                name: "edition",
                label: bizagi.localization.getResource("bz-rp-components-dimension-edit"),
                icon: "bz-wp-icon-edit"
            },
            {
                name: "log",
                label: bizagi.localization.getResource("render-actions-log"),
                icon: "bz-wp-icon-log"
            }
        ];

        this._super(workportalFacade, dataService, params);
    },

    /*
    *   Returns the widget name
    */
    getWidgetName: function () {
        return bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_USERS_ADMINISTRATION;
    },

    /*
    *   Renders the content for the current controller
    *   Returns a deferred because it has to load the current user
    */
    renderContent: function () {

        var self = this;
        var tmpl = self.workportalFacade.getTemplate("admin.users");
        var content = self.content = $.tmpl(tmpl);

        return content;
    }
});