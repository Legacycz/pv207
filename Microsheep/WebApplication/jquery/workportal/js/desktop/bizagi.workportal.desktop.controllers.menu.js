/*
 *   Name: BizAgi Workportal Desktop Menu Controller
 *   Author: Diego Parra (based on  Edward Morales version)
 *   Comments:
 *   -   This script will override menu controller class to apply custom stuff just for desktop device
 */


// Auto extend
bizagi.workportal.controllers.menu.extend("bizagi.workportal.controllers.menu", {}, {
    /*
    *   To be overriden in each device to apply layouts
    */
    postRender: function () {
        var self = this;
        var defer = new $.Deferred();
        var content = self.getContent();
        var searchContent = $("#ui-bizagi-wp-widget-searchContainer", content);
        var iconsTemplate = self.workportalFacade.getTemplate("menu.security");
        var menuContent = $("#main_menu", content);

        self.security = new bizagi.workportal.command.security(self.dataService);

        // Render menu
        $.when(self.security.getSecurity()).done(function (sec) {
            $.tmpl(iconsTemplate, sec,
                    {
                        reportsSecurity: function () {
                            if (sec.AnalysisReports) {
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }).appendTo(menuContent);

            // Fix logout button
            if (!sec.LogOut) {
                //Hide it
                $("#logout", content).hide();
            }
            //Prevent event beforeunload to be trigger by clicking the menu
            $("#main_menu a", content).click(function (event) {
                event.preventDefault();
            });

            // Fix search field
            if (!sec.Search) {
                $("#ui-bizagi-wp-widget-searchContainer", content).hide();
            }

            // Bind Widgets button
            $("#main_menu li#menuListWidget", content).click(function () {
                self.showWidgetsPopup();
            });

            // Bind New Case button
            $("#main_menu li#menuListNew", content).click(function () {
                self.showNewCasePopup();
            });

            //  Bind inbox button
            $("#main_menu li#menuListInbox", content).click(function () {
                var inbox = bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_INBOX;
                var grid = bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_INBOX_GRID;
                var actualWidget;
                var storeWidget = ($.isFunction(bizagi.cookie)) ? bizagi.cookie("bizagiDefaultWidget") : grid;

                switch (storeWidget) {
                    case inbox:
                        actualWidget = inbox;
                        break;

                    case grid:
                        actualWidget = grid;
                        break;
                    default:
                        actualWidget = grid;
                        break;
                }


                self.publish("changeWidget", {
                    widgetName: actualWidget
                });
            });

            // Bind New Case button
            $("#main_menu li#menuListQueries", content).click(function () {
                self.showQueriesPopup();
            });

            $("#tempEntities", content).click(function () {
                /*
                self.publish("showDialogWidget", {
                widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_PAGE,
                context: "entitymanagement",
                idForm: 'a5456d8d-3fa2-4265-950e-20ac6fb10b98',
                idEntity: 10966,
                modalParameters: {
                title: self.getResource("workportal-menu-preferences"),
                width:"1020px",
                showCloseButton: false
                }
                });
                */
                /*
                self.publish("showDialogWidget", {
                widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_PAGE,
                context: "entitymanagement",
                idForm: '4541260d-6e4a-4ea1-9941-d7f5975f2586',
                idEntity: 10229,
                modalParameters: {
                title: self.getResource("workportal-menu-preferences"),
                width:"1020px",
                showCloseButton: false
                }
                });
                */
                /*
                self.publish("showDialogWidget", {
                widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_PAGE,
                context: "entitymanagement",
                idForm: '56f9ad66-dc36-45cf-b193-67fbca9f9ac0',
                idEntity: 10640,
                modalParameters: {
                title: self.getResource("workportal-menu-preferences"),
                width:"1020px",
                showCloseButton: false
                }
                });
                */
                self.publish("showDialogWidget", {
                    widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ENTITIES,
                    modalParameters: {
                        title: self.getResource("workportal-menu-preferences"),
                        width: "1020px",
                        showCloseButton: false
                    }
                });
            });

            // Bind admin menu
            $("#menuListAdmin", content).click(function () {
                self.renderAdminMenu();
            });

            $("#menuListReports", content).click(function () {

                    self.renderReportsMenu();
            });

            $("#menuListPreferences", content).click(function () {
                var url = self.dataService.getUrl({
                    "endPoint": "ListPreferences"
                });

                var widgetName = bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_ADMIN_PREFERENCES;

                self.publish("showDialogWidget", {
                    widgetName: widgetName,
                    widgetURL: url,
                    closeVisible: false,
                    maximize: false,
                    modalParameters: {
                        title: self.getResource("workportal-menu-preferences"),
                        height: 690
                    }
                });
            });

            defer.resolve(content);
        });

        // Bind Search field
        var inputSearchField = $("#menuQuery", searchContent);

        // Bind Font Size
        $(".ui-bizagi-wp-bt-font", content).click(function () {
            self.renderFontMenu();
        });

        // Bind Logout                
        $("#logout", content).button({ icons: {
            primary: "ui-icon-logout"
        }
        });

        $("#logout", content).click(function () {

            $.when(bizagi.util.autoSave()).done(function () {
                $(document).data('auto-save', '');
                self.dataService.logout();
            });
        });

        //Bind About
        $("#about", content).button({ icons: {
            primary: "ui-icon-about"
        }
        });

        $("#about", content).click(function () {
            self.showAboutPopup();
        });

        /**
        * Clear input search field when set query 
        */
        inputSearchField.click(function () {
            if ($(this).val() == self.getResource("workportal-menu-search")) {
                $(this).val("");
            }
        });

        $(inputSearchField).bind('keypress', function (e) {
            if (e.keyCode == 13) {
                if (inputSearchField.val() != "" && inputSearchField.val() != self.getResource("workportal-menu-search")) {
                    self.publish("executeAction", {
                        action: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_SEARCH,
                        radNumber: inputSearchField.val(),
                        onlyUserWorkItems: false
                    });
                }
            }
        });

        $(window).trigger("resize");

        return defer.promise();
    },
    /*
    *   Creates a generic menu popup
    */
    showMenuPopup: function (options) {
        var self = this;
        var content = self.getContent();
        var popup = new bizagi.workportal.desktop.popup(self.dataService, self.workportalFacade, {
            sourceElement: options.buttonSelector,
            my: options.my || "center top",
            at: options.at || "center bottom",
            offset: options.offset,
            height: options.height,
            width: options.width
        });

        // If the same popup is opened close it
        if (self.currentPopup == options.buttonSelector) {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows the popup
        self.currentPopup = options.buttonSelector;
        var popupContent = self.workportalFacade.getTemplate(options.template);
        popup.render(popupContent);

        // Adds the active class for buttons
        $(options.buttonSelector, content).addClass("active");

        // Checks for close signal to change the class again
        $.when(popup.closed())
                .done(function () {
                    self.currentPopup = null;
                    $(options.buttonSelector, content).removeClass("active");
                });
    },
    /*
    *   Shows the user popup to access more options
    */
    showUserPopup: function () {
        var self = this;
        self.showMenuPopup({
            buttonSelector: "#ui-bizagi-wp-menu-username",
            template: "menu.modal.user",
            offset: "0 0",
            height: 80,
            width: 90
        });
    },
    /*
    *   Shows the widgets popup to access all dynamic content
    */
    showWidgetsPopup: function (options) {
        options = options || {};
        var self = this;
        self.showMenuPopup({
            buttonSelector: options["buttonSelector"] || "#main_menu li#menuListWidget",
            template: options["template"] || "menu.modal.widgets",
            offset: options["offset"] || "18 -34",
            height: options["height"] || 160,
            width: options["width"] || 175
        });
    },
    /*
    *   Show new case popup to create a case
    */
    showNewCasePopup: function () {
        var self = this;
        var content = self.getContent();

        // Switch buttonstyle
        $("#main_menu li#menuListNew", content).addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "newCase") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows a popup widget
        self.currentPopup = "newCase";
        self.publish("popupWidget", {
            widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_NEWCASE,
            options: {
                sourceElement: "#main_menu li#menuListNew",
                'max-height': '746',
                height: 'auto',
                width: '339',
                offset: "18 -4", //x y
                activeScroll: false,
                closed: function () {
                    self.currentPopup = null;
                    $("#main_menu li#menuListNew", content).removeClass("active");
                }
            }
        });
    },
    /*
    *   Show new case popup to create a case
    */
    showQueriesPopup: function () {
        var self = this;
        var content = self.getContent();

        // Switch buttonstyle
        $("#main_menu li#menuListQueries", content).addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "queries") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }
        var widgetName=bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_QUERIES;

        //update line key disableFrankensteinQueryForms to false
        //if(bizagi.util.parseBoolean(bizagi.override.disableFrankensteinQueryForms) == true){
            widgetName=bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_QUERIES_DEFINITION;
        //}

        // Shows a popup widget
        self.currentPopup = "queries";
        self.publish("popupWidget", {
            widgetName: widgetName,
            options: {
                sourceElement: "#main_menu li#menuListQueries",
                'max-height': '346',
                height: 'auto',
                width: '339',
                offset: "18 -4",
                activeScroll: true,
                closed: function () {
                    self.currentPopup = null;
                    $("#main_menu li#menuListQueries", content).removeClass("active");
                }
            }
        });
    },

    showAboutPopup: function () {

        var self = this;

        var template = self.workportalFacade.getTemplate("menu.modal.about");

        var popupContent = $.tmpl(template, { build: bizagi.loader.productBuildToAbout });
        $(popupContent).dialog({
            width: 430,
            height: 200,
            modal: true,
            title: bizagi.localization.getResource('workportal-menu-about'),
            maximized: false,
            resizable: false
        });
    },

    /*
    *  Check security for principal menu items
    */
    /*  getSecurityMenu: function() {
    var self = this;
    var authMenu = {};
    var df = new $.Deferred();
    $.when(
    self.dataService.getMenuAuthorization()
    ).done(function(data) {
    bizagi.menuSecurity = self.convertSecurityData(data);
    self.jsonSecurityList = data;
    $.each(self.security, function(key, value) {
    authMenu[key] = value;
    });
    df.resolve(bizagi.menuSecurity);
    });

    return df.promise();
    },*/
    /*   convertSecurityData: function(data) {
    var permsMenu = {};
    var getRecursivePerms = function(perms) {
    $.each(perms, function(key, value) {
    if (typeof value == 'object') {
    if (typeof key != "number") {
    permsMenu[key] = true;
    }
    getRecursivePerms(value);
    } else {
    permsMenu[value] = true;
    }
    });
    }
    getRecursivePerms(data.permissions);
    return permsMenu;
    },*/
    checkRootCategory: function (data, key) {
        if (data["permissions"] != undefined) {
            for (var i = 0; i < data["permissions"].length; i++) {
                if (data["permissions"][i][key] != undefined) {
                    return true;
                }
            }
        }
        return false;
    },
    getsRootCategory: function (data, key) {
        for (var i = 0; i < data["permissions"].length; i++) {
            if (data["permissions"][i][key] != undefined) {
                return data["permissions"][i][key];
            }
        }
        return {};
    },
    getSubMenuData: function (data, category) {
        var self = this;
        var result = [];
        var prefix = "workportal-menu-submenu-";
        data = data || self.security.getRawData() || {};
        if (self.checkRootCategory(data, category)) {
            $.each(self.getsRootCategory(data, category), function (k, v) {
                result.push(
                        {
                            "categoryKey": v,
                            "categoryName": self.getResource(prefix + v),
                            "categoryUrl": self.dataService.getUrl({
                                "endPoint": v
                            })
                        });
            });
        }
        return result;
    },
    renderAdminMenu: function () {
        var self = this;
        var content = self.getContent();

        // Switch buttonstyle
        $("#main_menu li#menuListAdmin", content).addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "subMenu") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows a popup widget
        self.currentPopup = "subMenu";
        self.publish("popupWidget", {
            widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_SUBMENU,
            options: {
                sourceElement: "#main_menu li#menuListAdmin",
                'max-height': '346',
                height: 'auto',
                width: '339',
                offset: "18 -4",
                activeScroll: true,
                closed: function () {
                    self.currentPopup = null;
                    $("#main_menu li#menuListAdmin", content).removeClass("active");
                }
                ,
                menuData: self.getSubMenuData(self.jsonSecurityList, "Admin")
            }
        });
    },
    renderReportsMenu: function () {
        var self = this;
        var content = self.getContent();
        var widget = (!bizagi.util.isIE8()) ? bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_REPORTS_MENU : bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_REPORTS;
        // Switch buttonstyle
        $("#main_menu li#menuListReports", content).addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "subMenu") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows a popup widget
        self.currentPopup = "subMenu";
        self.publish("popupWidget", {
            widgetName: widget,
            options: {
                sourceElement: "#main_menu li#menuListReports",
                'max-height': '346',
                height: 'auto',
                width: '339',
                offset: "18 -4",
                activeScroll: true,
                closed: function () {
                    self.currentPopup = null;
                    $("#main_menu li#menuListReports", content).removeClass("active");
                }
                ,
                menuData: self.getSubMenuData(self.jsonSecurityList, "AnalysisReports")
            }
        });
    },
    // Shows the options for font size
    renderFontMenu: function () {
        var self = this;
        var content = self.getContent();

        // Switch buttonstyle
        $(".ui-bizagi-wp-bt-font", content).addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "fontsize") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows a popup widget
        self.currentPopup = "fontsize";
        self.publish("popupWidget", {
            widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_FONTSIZE,
            options: {
                sourceElement: ".ui-bizagi-wp-bt-font",
                offset: "0 16",
                closed: function () {
                    self.currentPopup = null;
                    $(".ui-bizagi-wp-bt-font", content).removeClass("active");
                }

            }
        });
    },
    /*
    *   Renders a custom sub-menu
    */
    renderSubMenu: function (menuButton, submenu, callback) {
        var self = this;
        var content = self.getContent();
        var menuData = [];

        // Transform menu data
        for (i in submenu) {
            menuData.push({
                categoryKey: submenu[i].id,
                categoryName: submenu[i].text,
                categoryClassName: submenu[i].className
            });
        }

        // Switch buttonstyle
        menuButton.addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "subMenu") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows a popup widget
        self.currentPopup = "subMenu";
        self.publish("popupWidget", {
            widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_SUBMENU,
            options: {
                sourceElement: menuButton,
                'max-height': '346',
                height: 'auto',
                width: '339',
                offset: "18 -4",
                activeScroll: true,
                closed: function () {
                    self.currentPopup = null;
                    menuButton.removeClass("active");
                },
                menuData: menuData,
                customClickHandler: function (id) {
                    if (callback)
                        callback(self, id);
                }
            }
        });
    },
    showEntitiesPopup: function (url) {
        var self = this;
        var content = self.getContent();


        // Switch buttonstyle
        $("#main_menu li#temp", content).addClass("active");

        // If the same popup is opened close it
        if (self.currentPopup == "genericiframe") {
            bizagi.workportal.desktop.popup.closePopupInstance();
            return;
        }

        // Shows a popup widget
        self.currentPopup = "genericiframe";
        self.publish("showDialogWidget", {
            widgetName: bizagi.workportal.widgets.widget.BIZAGI_WORKPORTAL_WIDGET_GENERICIFRAME,
            widgetURL: url
        });
    }
});
