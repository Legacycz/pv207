﻿/**

* Name: BizAgi Desktop Widget Admin Entities Implementation

*

* @author Jeison Borja Abril

*/

bizagi.workportal.widgets.admin.entities.extend("bizagi.workportal.widgets.admin.entities", {}, {

    init: function (workportalFacade, dataService, params) {
        this._super(workportalFacade, dataService, params);
        this.filters = {};
        this.internalData = {};
    },

    loadtemplates: function () {
        var self = this;
        var subTitle = self.getResource("workportal-widget-admin-entities-widget-subtitle");
        self.panelWrapper = $.tmpl(self.workportalFacade.getTemplate("admin.entity.panel.wrapper"), { subTitle: subTitle });
        self.entitiesList = self.workportalFacade.getTemplate("admin.entity.list");
        self.entitiesDetail = self.workportalFacade.getTemplate("admin.entity.detail");
        self.entitiesDetailPagination = self.workportalFacade.getTemplate("admin.entity.detail.pagination");
        self.entitiesButtonsForm = self.workportalFacade.getTemplate("admin.entity.buttons.form");
        self.entitiesMessageError = self.workportalFacade.getTemplate("admin.entity.detail.message.error");
    },

    postRender: function () {
        var self = this;
        self.maxElemShow = 10;
        self.maxPageToShow = 5;
        self.mainContent = self.getContent();
        self.elementSelected = null;
        self.currentGuidForm = null;
        self.currentIdRow = null;

        var content = self.getContent();
        content.html(self.panelWrapper);
        var bizagiObjectsListWrapper = $("#bizagi-entities-list-wrapper", self.panelWrapper);
        $.when(self.dataService.getAdminEntitiesList()).done(function (data) {
            self.listEntities = data.administrableEntities.entities;
            $.tmpl(self.entitiesList, { elementList: self.listEntities, subTitle: data.administrableEntities.title }).appendTo(bizagiObjectsListWrapper);
            $("#ListEntities option:first", bizagiObjectsListWrapper).attr('selected', 'selected');
            $("#entity_selected", self.panelWrapper).text($("#ListEntities option:first", bizagiObjectsListWrapper).text());
            $("#ListEntities", bizagiObjectsListWrapper).change(function () {
                var params = self.getParams();
                self.getDetailsEntity(content, params);
            }).trigger("change");

            $("#searchEntity", bizagiObjectsListWrapper).keyup(function () {
                $('#ListEntities option').remove();
                var includeOpt = false;
                var matchString = $(this).val().toLowerCase();
                $.each(self.listEntities, function () {
                    if (matchString.length > 1) {
                        var str = this.entDisplayName;
                        if (str.toLowerCase().indexOf(matchString) != -1) {
                            includeOpt = true;
                        }
                    } else {
                        includeOpt = true;
                    }

                    if (includeOpt) {
                        $("#ListEntities").append($("<option>", {
                            value: this.idEnt,
                            text: this.entDisplayName,
                            "data-guident": this.guidEnt
                        }));
                        includeOpt = false;
                    }
                });
            });
        });
    },

    /*
    * return params according to select
    */
    getParams: function () {
        var self = this;
        var nameEntity = $("#ListEntities option:selected").text();
        var idEntity = $("#ListEntities option:selected").val();
        var guidEntity = $("#ListEntities option:selected").attr('data-guidEnt');
        var params = {
            idEntity: idEntity,
            nameEntity: nameEntity,
            guidEntity: guidEntity,
            pagSize: self.maxElemShow,
            filters: (self.filters[guidEntity]) ? Object.keys(self.filters[guidEntity]).map(function (key) { return self.filters[guidEntity][key]; }) : []
        };
        return params;
    },

    getDetailsEntity: function (content, params) {
        var self = this;
        $('.admin-entities-rightframe', content).empty();
        $('.admin-entities-rightframe', content).addClass('loading-admin-entities');
        //Keep in track the total Records
        $.when(self.dataService.getAdminEntitiesRowData(params))
            .done(function (responseEntity) {
                params.guid = params.guidEntity;
                var guidDisplayForm = responseEntity.entity.idDisplayForm;
                var idDisplayForm = responseEntity.entity.idFormDisplay;
                if (guidDisplayForm != "00000000-0000-0000-0000-000000000000" || idDisplayForm != -1) {
                    $.when(self.dataService.getAdminEntityMigrated(guidDisplayForm))
                        .done(function (versionForm) {
                            if (versionForm.result == 0 || versionForm.result == 10) {
                                params.guidForm = responseEntity.entity.idDisplayForm;
                                $.when(self.dataService.getAdminEntitiesForm(params))
                                    .done(function (responseFormDisplay) {
                                        self.mergeRowsTable(content, params, responseEntity, responseFormDisplay);
                                    });
                            } else {
                                if (versionForm.result == -9) {
                                    self.renderTableEntity(content, params, responseEntity);
                                }
                                else {
                                    self.loadIframeForm('App/Admin/Entity.aspx?idEntity=' + params.idEntity, 'displayForm');
                                }
                            }
                        });
                }
                else {
                    self.renderTableEntity(content, params, responseEntity);
                }
            });
    },


    renderTableEntity: function (content, params, responseEntity) {
        var self = this;
        self.totalRecords = responseEntity.records;
        //keep in track the total pages
        self.totalPages = responseEntity.total;
        self.showRowsEntity(content, responseEntity, params);
        if (responseEntity.records > self.maxElemShow) {
            var pageToshow = (self.maxPageToShow > self.totalPages) ? self.totalPages : self.maxPageToShow;
            var summaryWrapper = $("#biz-wp-table-pager-wrapper");
            var pagerData = {};
            var paginationHtml;
            // show or hide "load more" button
            pagerData.pagination = (self.totalPages > 1) ? true : false;
            pagerData.page = responseEntity.page;
            pagerData.pages = {};
            for (var i = 1; i <= pageToshow; i++) {
                pagerData["pages"][i] = {
                    "pageNumber": i
                };
            }
            //load and append the paginator to the result table
            paginationHtml = $.tmpl(self.entitiesDetailPagination, pagerData);
            summaryWrapper.append(paginationHtml);
            //add data and behaviour to pager
            $("ul#biz-wp-table-pager-wrapper").bizagiPagination({
                maxElemShow: self.maxElemShow,
                totalPages: self.totalPages,
                actualPage: responseEntity.page,
                listElement: $("#biz-wp-table-pager-wrapper"),
                clickCallBack: function (options) {
                    params.pag = options.page;
                    self.getDetailsEntity(content, params);
                }
            });
        }
    },

    //remove elements of the table if exist a display form
    mergeRowsTable: function (content, params, responseEntity, responseFormDisplay) {
        var self = this;
        var newResponseEntity = {};
        newResponseEntity.entity = responseEntity.entity;
        newResponseEntity.page = responseEntity.page;
        newResponseEntity.records = responseEntity.records;
        newResponseEntity.total = responseEntity.total;
        newResponseEntity.header = new Array();
        newResponseEntity.rows = new Array();
        var indexElementsAdd = new Array();
        $.each(responseFormDisplay.form.elements, function (kF, vF) {
            self.renderTable(vF, newResponseEntity, indexElementsAdd, responseEntity);
        });
        indexElementsAdd.push(responseEntity.header.length - 1);
        var desabledElement = responseEntity.header.slice(-1).pop();
        newResponseEntity.header.push(desabledElement);
        //validate if first element responseEntity exist in newResponseEntity else add becouse is subrrogate key
        if (indexElementsAdd.indexOf(0) == -1) {
            responseEntity.header[0].dataType = -11;
            var firstElement = responseEntity.header[0];
            newResponseEntity.header.unshift(firstElement);
            indexElementsAdd.unshift(0);
        }
        // var newDataRow = new Array();
        var indexAdd = 0;
        $.each(responseEntity.rows, function (k, v) {
            var newDataRow = [];
            for (var i = 0; i < indexElementsAdd.length; i++) {
                indexAdd = indexElementsAdd[i];
                newDataRow[i] = v.data[indexAdd];
            }
            newResponseEntity.rows[k] = {};
            newResponseEntity.rows[k].data = new Array();
            newResponseEntity.rows[k].data = newDataRow;
        });
        self.renderTableEntity(content, params, newResponseEntity);
    },

    renderTable: function (element, newResponseEntity, indexElementsAdd, responseEntity) {
        var self = this;
        if (element.container) {
            for (var i = 0; i < element.container.elements.length; i++) {
                self.renderTable(element.container.elements[i], newResponseEntity, indexElementsAdd, responseEntity);
            }
        }
        else {
            $.each(responseEntity.header, function (index, header) {
                if (element.render.properties.xpath) {
                    if (element.render.properties.xpath.toUpperCase() == header.fieldValue.toUpperCase()) {
                        //newResponseEntity.header.push(vE);
                       
                        newResponseEntity.header.push($.extend(element.render.properties, header));
                        indexElementsAdd.push(index);
                    }
                }
            });
        }
    },

    showRowsEntity: function (content, resultList, params) {
        var self = this;
        var bizagiEntityDetail = $("#admin-entities-rightframe", self.panelWrapper);
        $('.admin-entities-rightframe', content).removeClass('loading-admin-entities');
        var resultTemplate = (bizagiEntityDetail).html(
            $.tmpl(self.entitiesDetail,
                {
                    headers: resultList.header,
                    rows: resultList.rows,
                    nameEntity: params.nameEntity,
                    guidEntity: params.guidEntity,
                    ordertype: params.orderType,
                    orderField: params.orderField,
                    guidForms: resultList.entity,
                    allowAdd: resultList.entity.allowAdd,
                    allowEdit: resultList.entity.allowEdit,
                    allowViewData: resultList.entity.allowViewData,
                    filterApplied: self.filterApplied(params.guidEntity),
                    filterEnabled: bizagi.override.enableFilterGrid,
                    attributeSupportFilter: self.attributeSupportFilter
                }));

        self.formatTableEntity(content, resultTemplate, resultList);
        if (bizagi.override.enableFilterGrid) {
            self.setFilters(resultTemplate, resultList.header, params.guidEntity);
        }
    },

    //format colums of the table
    formatTableEntity: function (content, resultTemplate, resultList) {
        var self = this;
        var dateTimeHeaders = $("a[data-type='12']", resultTemplate);
        $.each(dateTimeHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span:contains(':')").addClass("formatDate");
            }
        });
        bizagi.util.formatInvariantDate(resultTemplate, self.getResource("dateFormat") + " " + self.getResource("timeFormat"));

        var moneyHeaders = $("a[data-type='8']", resultTemplate);
        $.each(moneyHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        bizagi.util.formatInvariantMoney(resultTemplate, userLocalization);

        var floatHeaders = $("a[data-type='10']", resultTemplate);
        $.each(floatHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalization);

        var decimalHeaders = $("a[data-type='6']", resultTemplate);
        $.each(decimalHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalization);

        var realHeaders = $("a[data-type='11']", resultTemplate);
        $.each(realHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalization);

        var bigIntHeaders = $("a[data-type='1']", resultTemplate);
        $.each(bigIntHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        var userLocalizationCloned = bizagi.clone(userLocalization);
        userLocalizationCloned.decimalDigits = 0;
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalizationCloned);

        var intHeaders = $("a[data-type='2']", resultTemplate);
        $.each(intHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        var userLocalizationCloned = bizagi.clone(userLocalization);
        userLocalizationCloned.decimalDigits = 0;
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalizationCloned);

        var smallIntHeaders = $("a[data-type='3']", resultTemplate);
        $.each(smallIntHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        var userLocalizationCloned = bizagi.clone(userLocalization);
        userLocalizationCloned.decimalDigits = 0;
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalizationCloned);

        var tinyIntHeaders = $("a[data-type='4']", resultTemplate);
        $.each(tinyIntHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        var userLocalizationCloned = bizagi.clone(userLocalization);
        userLocalizationCloned.decimalDigits = 0;
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalizationCloned);

        var numericHeaders = $("a[data-type='7']", resultTemplate);
        $.each(numericHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span").addClass("formatMoney");
            }
        });
        var userLocalization = (typeof (bizagi.currentUser.symbol) != "undefined") ? bizagi.currentUser : self.getResource("numericFormat");
        var userLocalizationCloned = bizagi.clone(userLocalization);
        userLocalizationCloned.decimalDigits = 0;
        bizagi.util.formatInvariantNumber(resultTemplate, userLocalizationCloned);

        var booleanHeaders = $("a[data-type='5'], a[data-type='-1']", resultTemplate);
        $.each(booleanHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                var valuesBoolean = $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span");
                if (index > 0)
                $.each(valuesBoolean, function (j, itemBoolean) {
                    var contentBoolean = $.trim($(itemBoolean).text());
                    if (contentBoolean == 'true' || contentBoolean == '1') {
                        $(itemBoolean).text('X');
                    }

                    if (contentBoolean == 'false' || contentBoolean == '0') {
                        $(itemBoolean).text('');
                    }
                });
            }
        });


        var fileHeaders = $("a[data-type='4']", resultTemplate);
        if (fileHeaders.length > 0) {
            for (var i = 0; i < fileHeaders.length; i++) {
                var indexFile = Number($(fileHeaders[i]).attr('data-index'));
                var valuesFile = $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + indexFile + ")").find("span");
                $.each(valuesFile, function (j, itemFile) {
                    var val = $(itemFile).text();
                    if (val === "true" || val === "false") {
                        $(itemFile).text('...');
                    }
                });
            }
        }
        var scientificNotationHeaders = $("a[data-type='29']", resultTemplate);
        $.each(scientificNotationHeaders, function (i, item) {
            if ($(item).parent().css('display') != 'none') {
                var index = Number(item.getAttribute('data-index'));
                var oracleNumberList = $(".ui-bizagi-grid-table tr", resultTemplate).find("td:eq(" + index + ")").find("span");
                $.each(oracleNumberList, function (j, numberItem) {
                    var number = $.trim($(numberItem).text());
                    $(numberItem).text(bizagi.util.scientificNotationFormat(number));
                });
            }
        });

        //hide de first column if the user created display form
        if ($("a[data-type='-11']").length > 0) {
            $('.admin-entities-rightframe #detailEntity table.ui-bizagi-grid-table td:nth-child(1)').css('display', 'none');
            $('.admin-entities-rightframe #detailEntity table.ui-bizagi-grid-table th:nth-child(1)').css('display', 'none');
        }


        self.setupButtonsList(content);
    },

    /*
    * Manage button edit row entities or add row
    */
    setupButtonsList: function (content) {
        var self = this;
        var idEntity = $('#ListEntities').val();
        $(".addRowEntity", content).click(function () {
            self.validateVersionForm(this, 'New');
        });

        $(".ui-bizagi-grid-body tr", content).click(function () {
            if ($(this).attr('class') == 'ui-bizagi-state-selected') {
                $(this).removeClass('ui-bizagi-state-selected');
                self.elementSelected = null;
            }
            else {
                $('.ui-bizagi-grid-body tr', content).removeClass('ui-bizagi-state-selected');
                $(this).addClass('ui-bizagi-state-selected');
                self.elementSelected = this;
            }
        });

        $(".editRowEntity", content).click(function () {
            if (self.elementSelected == null) {
                bizagi.showMessageBox(bizagi.localization.getResource("render-grid-message-no-selected-row"), bizagi.localization.getResource("render-grid-header-no-selected-row"));
            }
            else {
                self.validateVersionForm(self.elementSelected, 'Edit');
                self.elementSelected = null;
            }
        });

        $(".btn-vew-data-entity", content).click(function () {
            self.validateVersionForm(this, 'View');
        });

        $(".link-order-entity", content).click(function () {
            var params = self.getParams();
            params.orderField = $(this).attr('data-columnOrderField');
            params.orderType = $(this).attr('data-orderType');
            self.getDetailsEntity(content, params);
        });

    },

    /*
   * Returns true if there is a filter applied to entity
   */
    filterApplied: function (guidEntity) {
        var self = this,
            filters = self.filters;

        if (filters[guidEntity]) {
            return !$.isEmptyObject(filters[guidEntity]);
        }

        return false;
    },

    validateVersionForm: function (element, action) {
        var self = this;
        $('#detailEntity', self.bizagiEntityDetail).empty();
        $('#detailEntity', self.bizagiEntityDetail).addClass('loading-admin-entities');
        var idEntity = $('#ListEntities', self.panelWrapper).val();
        var params = {};
        params.guid = $(element).attr('data-guidEntity');
        if (action != 'New') {
            params.idRow = $(element).attr('data-idrowentity');
        }
        params.guidForm = $(element).attr('data-guidForm');
        params.idForm = $(element).attr('data-idForm');
        if (action == 'View') {
            params.disabled = true;
            action = 'Edit';
        }
        if (params.guidForm == "00000000-0000-0000-0000-000000000000") {
            self.getFormEntity(params);
        }
        else {
            $.when(self.dataService.getAdminEntityMigrated(params.guidForm)).done(function (data) {
                if (data.result == 0 || data.result == 10) {
                    self.getFormEntity(params);
                } else {
                    if (data.result == -9) {
                        var  bizagiEntityDetail = $("#detailEntity", self.panelWrapper);
                        $(bizagiEntityDetail).removeClass('loading-admin-entities');
                        var messageErrorForm = self.getResource("workportal-widget-admin-entities-message-migrate");
                        messageErrorForm = messageErrorForm.replace("{build}", bizagi.loader.productBuildToAbout);
                        var htmlMessage = $.tmpl(self.entitiesMessageError, { messageError: messageErrorForm });
                        $(bizagiEntityDetail).html(htmlMessage);
                        $("#errorButtonCancel", htmlMessage).click(function () {
                            var params = self.getParams();
                            self.getDetailsEntity(self.mainContent, params);
                        });
                    }
                    else {
                        if (action == 'New') {
                            params.idRow = -1;
                        }
                        var url = 'App/Admin/Entity.aspx?idEntity=' + idEntity + '&idSurrogateKey=' + params.idRow + '&referer=entityadmin&Action=' + action + '&idForm=' + params.idForm;
                        self.loadIframeForm(url, 'otherForm');
                    }
                }
            });
        }
    },

    /*
    * Apply filter's plugin to headers in the table
    */
    setFilters: function ($tableContent, headers, guidEntity) {
        var self = this,
            internalData = self.internalData,
            filters = self.filters;

        filters[guidEntity] = filters[guidEntity] || {};
        internalData[guidEntity] = internalData[guidEntity] || {};
        var $columnFilters = $tableContent.find('.ui-bizagi-grid-header .ui-bizagi-render-icon-filter');

        $.each($columnFilters, function (_, element) {
            var $element = $(element),
                $parent = $element.parent(),
                index = $element.data('index'),
                header = headers[index];

            if (self.attributeSupportFilter(header)) {

                (function (header) {
                    var fieldValue = header.fieldValue;

                    $element.bizagi_filters({
                        type: header.dataType,
                        parent: $parent,
                        templates: {
                            filterWrapper: self.workportalFacade.getTemplate("filter-wrapper"),
                            filterString: self.workportalFacade.getTemplate("filter-string"),
                            filterNumber: self.workportalFacade.getTemplate("filter-number"),
                            filterDate: self.workportalFacade.getTemplate("filter-date"),
                            filterBoolean: self.workportalFacade.getTemplate("filter-boolean")
                        },
                        data: filters[guidEntity][fieldValue],
                        entity: header.entity,
                        dataService: {
                            getAdminEntityData: function (params) {
                                return $.when(internalData[guidEntity][fieldValue] || self.dataService.getAdminEntityData(params))
                                    .pipe(function (entityData) {
                                        internalData[guidEntity][fieldValue] = entityData;
                                        return entityData;
                                    });
                            }
                        }
                    });

                    $element.on('bizagi_filterschange', function (ev, value) {

                        if ($.isEmptyObject(value)) {
                            delete filters[guidEntity][fieldValue];
                        }
                        else {
                            value.dataType = header.dataType;
                            value.field = fieldValue;
                            filters[guidEntity][fieldValue] = value;
                        }

                        self.getDetailsEntity(self.getContent(), self.getParams());
                    });

                    // Change the icon when there is a filter applied
                    if (filters[guidEntity][fieldValue]) {
                        $element.addClass('ui-bizagi-column-filter-applied');
                    }

                })(header);
            }

        });
    },

    /*
     * Returns true if the attribute support filter
     * images and files aren't supported 
    */
    attributeSupportFilter: function (header) {
        var attributeType = header.attributeType,
            datatype = header.dataType;

        if (datatype < 0) {
            return false;
        }

        if (attributeType == 1003) {
            return false;
        }

        if (attributeType == 1002) {
            return false;
        }

        return true;
    },

    /*
    * Load form  entity empty if params is just "guid" or with data if params is "guid" and "idRow"
    */
    getFormEntity: function (params) {
        var self = this;
        self.currentGuidForm = params.guidForm;
        self.currentIdRow = params.idRow;
        var facade = new bizagi.rendering.facade();
        $.when(
            self.dataService.getAdminEntitiesForm(params)
        ).done(function (responseForm) {
            //Modify JSON form with params necessary to widget
            responseForm.form.contextType = "entity";
            $.when(facade.execute({ "data": responseForm, "contexttype": "entity", "canvas": $("#detailEntity", self.panelWrapper), "isEntityForm": true }))
                .done(function (formRender) {
                    if (params.idRow) { formRender.surrogatekey = params.idRow }
                    formRender.unbind("refresh");
                    formRender.bind("refresh", function (_, refreshParams) {
                        formRender.dispose();
                        self.configureRefresh(_, refreshParams);
                    });

                    formRender.internalSubmitOnChange = function (params) {
                        var def = $.Deferred();
                        $.when(self.internalSubmitOnChangeEntity(formRender))
                            .done(function (responseSave) {
                                if (responseSave) {
                                    var params = self.getParams();
                                    params.idPageCache = formRender.idPageCache;
                                    params.guidForm = self.currentGuidForm;
                                    params.guid = params.guidEntity;
                                    params.idRow = self.currentIdRow;
                                    self.getFormEntity(params);
                                    def.resolve();
                                }
                            }).fail(function () {
                                def.reject();
                            }); ;
                        return def.promise();
                    }

                    formRender.processButton = function (buttonProperties) {
                        formRender.endLoading();
                        if (buttonProperties.action == 'save') {
                            if (formRender.validateForm()) {
                                return $.when(self.saveFormEntity(formRender))
                                    .done(function (responseSave) {
                                        if (responseSave) {
                                            var params = self.getParams();
                                            self.getDetailsEntity(self.mainContent, params);
                                        }
                                    });
                            }
                        }

                        if (buttonProperties.action == 'cancel') {
                            var params = self.getParams();
                            self.getDetailsEntity(self.mainContent, params);
                        }

                    };
                });
        });
    },


    internalSubmitOnChangeEntity: function (formRender) {
        var self = this;
        var renderValues = {};
        formRender.collectRenderValues(renderValues);
        var serviceRender = new bizagi.render.services.service();
        var defSave = $.Deferred();
        var params = self.getParams();
        $.when(
            serviceRender.multiactionService.submitData({
                action: "SUBMITONCHANGE",
                contexttype: "entity",
                data: renderValues,
                surrogatekey: (typeof formRender.surrogatekey !== "undefined" ? formRender.surrogatekey : false),
                idPageCache: formRender.idPageCache
            })
        )
            .done(function (formRender) {
                if (formRender.type == "success") {
                    defSave.resolve(true);
                }
                else {
                    defSave.reject();
                }
            })
            .fail(function (formError) {
                bizagi.showMessageBox(formError.message);
                defSave.reject();
            });
        return defSave.promise();
    },



    /*
    *
    */
    saveFormEntity: function (formRender) {
        var self = this;
        var data = {}
        var renderValues = {};
        data = formRender.collectRenderValues(renderValues);
        var serviceRender = new bizagi.render.services.service();
        var defSave = $.Deferred();
        var params = self.getParams();
        $.when(
            serviceRender.multiactionService.submitData({
                action: "SAVE",
                contexttype: "entity",
                data: renderValues,
                surrogatekey: (typeof formRender.surrogatekey !== "undefined" ? formRender.surrogatekey : false),
                idPageCache: formRender.idPageCache
            })
        )
            .done(function (formRender) {
                if (formRender.type == "success") {
                    defSave.resolve(true);
                }
                else {
                    defSave.reject();
                }
            })
            .fail(function (formError) {
                bizagi.showMessageBox(formError.message);
                defSave.reject();
            });
        return defSave.promise();
    },

    /*
    * if form have event refresh this function overwrite the event
    */
    configureRefresh: function (_, refreshParams) {
        var self = this;
        var bizagiEntityDetail = $("#detailEntity", self.panelWrapper);
        var canvas = refreshParams.canvas || $("#detailEntity", self.panelWrapper);
        $(canvas).empty();
        var facade = new bizagi.rendering.facade();
        if (facade.executionDeferred && facade.executionDeferred.state) {
            if (facade.executionDeferred.state() == "resolved") {
                facade.executionDeferred = $.Deferred();
            }
        }
        // Re-execute process
        $.when(self.dataService.getAdminEntitiesForm(refreshParams)).done(function (dataRefreshedForm) {
            dataRefreshedForm.form.contextType = "entity";
            $.when(facade.execute($.extend(refreshParams, { "data": dataRefreshedForm, "contexttype": "entity", canvas: canvas, refreshing: true, "formEntityRefreshed": true })))
            .done(function (refreshedForm) {
                refreshedForm.unbind("refresh");
                refreshedForm.bind("refresh", function (_, refreshParams) {
                    self.configureRefresh(_, refreshParams);
                });
                refreshedForm.properties.contexttype = "entity";
                facade.executionDeferred.resolve(refreshedForm);
                self.setupButtonsForm(bizagiEntityDetail, refreshedForm);
                return facade.executionDeferred.promise();
            });
        });
    },


    /////

    /*
    * Manage buttons form entity
    */
    setupButtonsForm: function (content, form) {
        var self = this;
        $(content).removeClass('loading-admin-entities');
        var params = self.getParams();
        $("#formButton0, #formButton1", content).off('click');
        $("#formButton0", content).click(function () {
            if (form.validateForm()) {
                var data = {}
                var renderValues = {};
                data = form.collectRenderValues(renderValues);
                if (Object.keys(data).length > 0 || 1 == 1) {
                    var serviceRender = new bizagi.render.services.service();
                    $.when(
                        serviceRender.multiactionService.submitData({
                            action: "SAVE",
                            contexttype: "entity",
                            data: renderValues,
                            surrogatekey: (typeof form.surrogatekey !== "undefined" ? form.surrogatekey : false),
                            idPageCache: form.idPageCache
                        })
                    )
                    .done(function (formRender) {
                        self.getDetailsEntity(self.mainContent, params);
                    });
                }
                else {
                    self.getDetailsEntity(self.mainContent, params);
                }
            }

        });
        $("#formButton1, #formButtonBack", content).click(function () {
            self.getDetailsEntity(self.mainContent, params);
        });
    },

    /*
    * Loaf iframe with the last form, for the entities not migrated
    */
    loadIframeForm: function (url, action) {
        var self = this;
        var template = self.workportalFacade.getTemplate("admin.entity.detail.frankenstein");
        var content = self.content = $.tmpl(template, { entityFormURL: url });
        var bizagiEntityDetail = '';
        if (action == 'displayForm') {
            bizagiEntityDetail = $("#admin-entities-rightframe", self.panelWrapper);
        }
        else {
            bizagiEntityDetail = $("#detailEntity", self.panelWrapper);
        }
        $(bizagiEntityDetail).removeClass('loading-admin-entities');
        $(bizagiEntityDetail).html(content);
        $('#entityFormIFrame').load(function () {
            if (action == 'displayForm') {
                $('iframe', '.admin-entities-rightframe').contents().find('form').css('margin-top', '20px');
                if ($('#entityFormIFrame').contents().find('input[name="btnSave"]').length == 0) {
                    $('#entityFormIFrame').contents().find('input[name="bt"]').remove()
                }
            }
            else {
                $('iframe', '.admin-entities-rightframe').contents().find('form').css('margin', '10px');
                $('#entityFormIFrame').contents().find('input[type="button"]').click(function () {
                    //wait the iframe load again if there validations in the form
                    $('#entityFormIFrame').load(function () {
                        $('#entityFormIFrame').css('display', 'none');
                        var params = self.getParams();
                        self.getDetailsEntity(self.mainContent, params);
                    });
                });
                var buttonAuxValue = self.getResource("render-form-dialog-box-cancel");
                var buttonAuxCancel = '<a class="WPButton" style="margin-left:15px"><input ordinal="1" id="formButtonCancel" type="button" class="WPButtonI BAMnColor" value="' + buttonAuxValue + '" role="button" aria-disabled="false"></a>';
                $('#entityFormIFrame').contents().find('.WPButton').after(buttonAuxCancel);
                $('#entityFormIFrame').contents().find('#formButtonCancel').click(function () {
                    $('#entityFormIFrame').css('display', 'none');
                    var params = self.getParams();
                    self.getDetailsEntity(self.mainContent, params);
                });
            }
        });
    }
});