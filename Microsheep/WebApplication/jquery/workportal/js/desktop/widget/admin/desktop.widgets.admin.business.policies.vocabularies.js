﻿/**
* Name: BizAgi Desktop Widget Administration Policies - Vocabularies 
* 
* @author Christian Collazos
*/


bizagi.workportal.widgets.admin.business.policies.vocabularies.extend("bizagi.workportal.widgets.admin.business.policies.vocabularies", {}, {
    init: function (workportalFacade, dataService, params) {
        // Call base
        this._super(workportalFacade, dataService, params);
    },

    renderContent: function () {
        var self = this;
        var template = self.workportalFacade.getTemplate("admin.business.policies.vocabularies.wrapper");
        var content;

        content = self.content = $.tmpl(template, {});

        // Override canvas if it has been defined
        if (self.params.canvas) {
            content = $(self.params.canvas).append(content);
        }

        return content;
    },

    postRender: function () {
        var self = this;

        //load templates 
        self.loadtemplates();
    },

    loadtemplates: function () {
        var self = this;

        //Template vars 
        self.decisionTableContent = self.workportalFacade.getTemplate("admin.business.policies.vocabularies.content");
    }
});