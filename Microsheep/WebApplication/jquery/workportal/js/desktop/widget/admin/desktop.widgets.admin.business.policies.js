﻿/**
 * Name: BizAgi Desktop Widget Administration Processes (orchestrator)
 * 
 * @author Christian Collazos
 */


bizagi.workportal.widgets.admin.business.policies.extend("bizagi.workportal.widgets.admin.business.policies", {}, {
    init: function (workportalFacade, dataService, params) {
        // Call base
        this._super(workportalFacade, dataService, params);
    },

    loadtemplates: function () {
        var self = this;

        //Template vars 
        self.generalContentTmpl = self.workportalFacade.getTemplate("admin.business.policies.content");
    },

    postRender: function () {
        var self = this;
        self.loadtemplates();
        self.setupData();
    },

    setupData: function () {
        var self = this,
            content = self.getContent(),
            generalContentTmpl = $.tmpl(self.generalContentTmpl, {});
        
        content.append(generalContentTmpl);
    },

    /*
    * initialize the process tree widget
    */
    setupPoliciesTree: function () {
        var self = this;
        var content = self.getContent();

        bizagi.treeRoutSelected = [];
        $("#treeLinksId").remove();

        self.processTree = new bizagi.workportal.widgets.tree(self.workportalFacade, self.dataService, $.extend(self.params, {
            canvas: $("#processTree", content)
        }));

        self.processTree.renderContent();
        self.processTree.postRender();
    }



    /********************* EVENTS ******************/
    
    
});