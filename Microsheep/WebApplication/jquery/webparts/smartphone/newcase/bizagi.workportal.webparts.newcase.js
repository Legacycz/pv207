﻿bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.newcase", {
    SEARCH_MIN_LENGTH: 3,
    SEARCH_DELAY: 700,
    ACTIVATE_AUTO_CLICK_APP_WHEN_1: true,
    ELEMENTS_TO_SHOW: 100
}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        this._super(workportalFacade, dataService, initialParams);
        //self.processEvent = false;
    },

    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        var self = this;
        self.activateAutoClickApplication = self.Class.ACTIVATE_AUTO_CLICK_APP_WHEN_1;
        return self.content = $.fasttmpl(self.getTemplate('newcase'));
    },

    postRender: function (params) {
        var self = this;
        self.renderCategories();
        self.configureTreeNavigation();
    },
    /**
    * Render List categories for each idCategory
    */
    renderCategories: function (idParentCategory, appId) {
        var self = this;
        var content = self.getContent();
        var categoryContainer = self.getContainer();
        var categoryTree = self.getContainerTree();
        var template = self.getTemplate("newCase-categories");
        var elemToShow = self.Class.ELEMENTS_TO_SHOW;
        $.when(
        self.dataService.getCategories({
            idCategory: idParentCategory
        }))
		.done(function (data) {
		    categoryTree.show();

		    if (appId) {
		        var foundAppId = { category: [] };
		        // Search appId into data json, and return subCategories level				
		        $.each(data.category, function (key, value) {
		            if (value.appId == appId) {
		                foundAppId.category = value.subCategories;
		            }
		        });

		        // Change data value with found data
		        data = foundAppId;
		    }
		    // When it has more than 2 applications
		    else if (data.totalApps > 1) {
		        var transformedData = { category: [] };

		        // Transform data and mark with appId flag
		        $.each(data.category, function (key, value) {
		            transformedData.category.push({
		                "appId": value.appId,
		                "idCategory": '',
		                "categoryName": value.appName,
		                "isProcess": "false",
		                "description": " "
		            });
		        });
		        // Change data value with transformed data
		        data = transformedData;
		    }
		    // Improve load time
		    if (data.category.length > elemToShow) {
		        filterData.category = {};
		        for (i = 0; i <= elemToShow; i++) {
		            filterData.category[i] = data.category[i];
		        }
		        filterData.truncate = true;
		        categoryContent = $.tmpl(template, filterData);
		        categoryContent.appendTo(categoryContainer);

		        self.loadAllElementEvent(data);

		    } else {
		        filterData = data;
		        categoryContent = $.tmpl(template, filterData);
		        categoryContent.appendTo(categoryContainer);
		    }
		    var filteredDataCases = {};
		    filteredDataCases.category = [];
		    var i = 0;
		    $.each(data.category, function (key, obj) {
		        // Add label and value to item object
		        data.category[key].label = obj.categoryName;
		        data.category[key].value = obj.idCategory;
		    });

		    // Assign click event for new content
		    self.assignEvent();

		});


    },

    loadAllElementEvent: function (data) {
        var self = this;
        var content = self.getContent();
        var template = self.workportalFacade.getTemplate("newCase-categories");
        var categoryContainer = self.getContainer();

        $(".loadMoreElements", categoryContainer).click(function (e) {
            e.stopPropagation();
            $(this).find("#loadMoreElementsIcon").removeClass("plus_load_icon").addClass("loading_icon");
            categoryContainer.empty();
            var categoryContent = $.tmpl(template, data);
            categoryContent.appendTo(categoryContainer);
            self.assignEvent();
        });
    },

    getContainer: function () {
        //return  this.getContent().find(".bz-wp-nc-content >div");
        var self = this;
        if (jQuery.type(self.bzwpnccontent) != "undefined") {
            return self.bzwpnccontent;
        } else {
            return self.bzwpnccontent = this.getContent().find(".bz-wp-nc-content >div");
        }
    },
    getContainerTree: function () {
        return this.getContent().find(".bz-wp-nc-content >ul"); //>ul
    },
    configureTreeNavigation: function () {
        var self = this;
        var categoryTree = self.getContainerTree(); //$("#categoryTree", self.getContent()); 
        $("li:last-child", categoryTree).click(function () {
            $(this).nextAll().remove();
            //$(this).find("> span").data("p-cat")
            self.renderCategories($(this).find("> span").data("p-cat"), $(this).find("> span").data("appid"));
        });
    },
    /**
    * Assign click event to list elements
    */
    assignEvent: function (elem, data) {
        var self = this;
        var content = self.getContent();
        var categoryContainer = self.getContainer();
        //var categoryTree = self.getContainerTree(); //$("#categoryTree", self.getContent());
        var liItem = elem || $("li", categoryContainer);

        $(liItem).one("click", function (event) {
            event.stopPropagation();
            event.preventDefault();

            self.processEvent = true;
            var idCategory = bizagi.idCategory = $(this).data("category");
            var appId = $(this).data("appid");
            var isProcess = $(this).hasClass('bz-nc-process');
            var categoryName = $(this).data("category-name") || $(this).find("h3").html();

            bizagi.util.smartphone.startLoading();
            if (bizagi.util.parseBoolean(isProcess) == true) {
                bizagi.util.smartphone.stopLoading();
                self.publish("bz-newcase-render", { idWfClass: idCategory });
                //memory performance
                categoryContainer.empty();
            } else {
                self.drawProcess(idCategory, categoryName, appId);
            }
        });
    },

    drawProcess: function (idCategory, categoryName, appId) {
        var self = this;
        var categoryTree = self.getContainerTree();

        $.when(self.renderCategories(idCategory, appId)).done(function (data) {
            if (data != "") {

                var headerTemplate = self.workportalFacade.getTemplate("newCase-categories-tree");
                $.tmpl(headerTemplate, { idParentCategory: idCategory, categoryName: categoryName || appId, "appId": appId }).appendTo(categoryTree);
                self.configureTreeNavigation();
                bizagi.util.smartphone.stopLoading();
                self.processEvent = false;
            }
        });
    },

    reset: function () {
        var self = this;
        var container = self.getContainerTree();
        container.find("li:first-child").click();
    }


});
