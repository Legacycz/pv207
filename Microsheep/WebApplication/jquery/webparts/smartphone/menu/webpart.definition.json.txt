﻿{
	"name": "menu",
	"js": [ {"src": "bizagi.workportal.webparts.menu.js"}],
	"css": [ {"src": "bizagi.webpart.menu.css"} ],
	"tmpl": [ 
		{alias: "menu-base",  "src": "menu.tmpl.html#ui-bizagi-workportal-menu-content"},
		{alias: "menu-items",  "src": "menu.tmpl.html#ui-bizagi-workportal-widget-menu-items"}
	]
}