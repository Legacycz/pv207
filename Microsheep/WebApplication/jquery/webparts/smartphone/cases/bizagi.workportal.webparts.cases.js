
/*
*   Name: BizAgi Workportal Cases Webpart
*   Author: Diego Parra
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.cases", {
    PAGE_SIZE: 10
}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        self._super(workportalFacade, dataService, initialParams);
        self.isFirtsPageLoad = true;

        self.subscribe("bz-newcase-render", function (e, params) {
            self.disableLoadMore();
            self.cleanCaseList();
        });

        this.subscribe("bz-webparts-back", function (e, params) {
            self.actualParamters.page = 1;
            self.actualParamters.backAction = true;
            self.actualParamters.refresh = true;
            $.when(self.getCasesList(self.actualParamters)).done(
             function () {
               self.enableLoadMore(params);
            });
            
        });


    },

    getCasesList: function (parameters) {
        var self = this;
        var deferred = $.Deferred();
        var context = self.getContent();
        var canvas = self.canvas;
        self.actualParamters = parameters;
        // $.event.special.inview.stopObserverandDestroy();
        /// setTimeout( bizagi.util.smartphone.startLoading,250);
        //si ya el filtro existe se solicita con ese parametro
        $.when(
        self.dataService.getCasesList(parameters)
        ).done(function (list) {

            self.totalPages = list.totalPages;
            self.page = list.page;

            /*
            notify when  self.totalPages cases not created
            */

            if (self.page == 1 || self.totalPages == 0) {
                var html = $.tmpl(self.getTemplate('inbox-mail'), list);
                self.content = html;
                if (parameters.refresh && parameters.refresh == true) {
                    $(html).appendTo(canvas);

                }
                //$(html).appendTo($(".bz-wp-ib-mail-container", context));
                //memoria de referencias de elementos aqui
            }
            else {
                //create elemets and activate in the corresponnd
                //prepend
                //memoria de elementos
                context = context.parent();
                if (list.overdue.length > 0) {
                    $(".bz-wp-header-title-inbox.overdue", context).removeClass("disable");
                    var html = $.fasttmpl(self.getTemplate('item-loadmore'), { element: list.overdue, classItem: "overdue" });
                    $(".bz-wp-header-title-inbox.today", context).before(html);
                }
                if (list.today.length > 0) {
                    $(".bz-wp-header-title-inbox.today", context).removeClass("disable");
                    var html = $.fasttmpl(self.getTemplate('item-loadmore'), { element: list.today, classItem: "today" });
                    $(".bz-wp-header-title-inbox.tomorrow", context).before(html);
                }

                if (list.tomorrow.length > 0) {
                    $(".bz-wp-header-title-inbox.tomorrow", context).removeClass("disable");
                    var html = $.fasttmpl(self.getTemplate('item-loadmore'), { element: list.tomorrow, classItem: "tomorrow" });
                    $(".bz-wp-header-title-inbox.upcoming", context).before(html);
                }

                if (list.upcoming.length > 0) {
                    $(".bz-wp-header-title-inbox.upcoming", context).removeClass("disable");
                    var html = $.fasttmpl(self.getTemplate('item-loadmore'), { element: list.upcoming, classItem: "upcoming" });
                    $(".bz-wp-loadmore", context).before(html);
                }
            }
            //self.enableLoadMore();
            deferred.resolve(self.content);
        });
        return deferred.promise();
    },

    cleanCaseList: function () {
        var self = this;
        self.canvas.empty();
        self.disableLoadMore();
    },

    eventClickCasesList: function () {
        var self = this;
        $(self.canvas).on("click", ".bz-wp-item", function () {
            element = $(this),
            idCase = element.data("case"),
            idWorkitem = element.data("workitem"),
            idTask = element.data("task"),
            summaryinfo = element.data("fieldsummary"),
            regex = /([^,]+),((?:[^,])+)/g,
            list = summaryinfo.match(regex);

            list = $.map(list, function (val, i) {
                var element = val.split(",");
                return { title: element[0], value: element[1] };
            });

            rs = $.fasttmpl(self.getTemplate('summary'), { list: list, idCase: idCase, idWorkitem: idWorkitem, idTask: idTask });
            self.publish("popup", { html: rs, callbackEvents: self.eventsPopUp, ref: self });
        });
    },

    enableLoadMore: function () {
        var self = this,
            context = self.getContent(); //self.getContent();

        context = $(context[context.length - 1]);
        $.event.special.inview.resetObserver();

        context.bind('inview', function (event, visible) {
            if (visible) {
                if (self.totalPages > self.page) {
                    $(".bz-wp-loadmore-inview", context).show();
                    $(".bz-wp-loadmore-noview", context).hide();

                    $.when(self.getCasesList({ idWorkflow: self.idWorkflow, page: (parseInt(self.page) + 1), idWfClass: (self.historycat) ? self.historycat.idWfClass : "-1" })).done(
                    function () {
                        $(".bz-wp-loadmore-inview", context).hide();
                        $(".bz-wp-loadmore-noview", context).show();
                    });
                }
                else {
                    $(this).unbind('inview');
                    context.hide();
                    self.disableLoadMore();
                }

            }
        });

    },

    eventsPopUp: function (element, parameters) {
        var self = parameters.ref;
        $(".bz-cases-work", element).one("click", function () {
            var element = $(this).parents(".bz-cases-content-popup"),
          idCase = element.data("case"),
          idWorkitem = element.data("workitem"),
          idTask = element.data("task");
            //name to put in header in the render view

            $.when(self.getCaseFormsRenderVersion(idCase))
            .done(function (formsVersion) {

                if (formsVersion == 1) {
                    alert(bizagi.localization.getResource("workportal-widget-oldrenderintegration-mobile") || "Sorry o Error. Bizagi Go cannot display Forms created in previous versions. Please upgrade Form.");
                    return;
                }

                parameters.closepopup();
                $.event.special.inview.stopObserverandDestroy();
                $(".bz-cases-close", element).off("click");
                self.publish("bz-show-render", { idCase: idCase, idWorkitem: idWorkitem, idTask: idTask });

            });
        });
        $(".bz-cases-close", element).one("click", function () {
            parameters.closepopup();
            $(".bz-cases-work", element).off("click");
        });
    },

    removeEventClickCasesList: function () {
        var self = this;
        $(self.canvas).off("click", ".bz-wp-item");
    },

    disableLoadMore: function () {
        //disable load more
        $.event.special.inview.stopObserverandDestroy();
    },

    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        //Override the login function, this is only necesary in WorkPortal
        // bizagi.services.ajax.loginPage = function () { };
        var self = this;
        var defer = new $.Deferred();
        self.historycat = JSON.parse(bizagi.util.getItemLocalStorage("scategory"));
        $.when(
                self.getCasesList(self.historycat || { pageSize: self.Class.PAGE_SIZE })
        ).done(function () {
            self.eventClickCasesList();


            defer.resolve(self.content);
        });
        return defer.promise();
    },

    /*
    *   Customize the web part in each device  
    */
    postRender: function (params) {
        var self = this;
        var content = self.getContent();
        self.subscribe("getCasesList", function (events, args) {
            self.cleanCaseList();
            self.historycat = JSON.parse(bizagi.util.getItemLocalStorage("scategory"));
            $.when(self.getCasesList($.extend(args, { refresh: true }))).done(function () {
                self.enableLoadMore();
            });
        });
        self.enableLoadMore();
    },

    getCaseFormsRenderVersion: function (idCase) {
        var self = this,
        defer = new $.Deferred();
        // Try get from parameters
        var formsVersion = 0;
        // Get from service
        $.when(self.dataService.getCaseFormsRenderVersion({
            idCase: idCase
        })).done(function (data) {
            formsVersion = parseInt(data.formsRenderVersion);
            defer.resolve(formsVersion);
        });
        return defer.promise();
    }
});
