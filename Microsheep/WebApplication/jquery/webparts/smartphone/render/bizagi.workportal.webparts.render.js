
/*
*   Name: BizAgi Workportal Cases Webpart
*   Author: oscaro
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.render", {}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;

        // Call base
        this._super(workportalFacade, dataService, initialParams);

        // show the render for case exist
        this.subscribe("bz-show-render", function (e, params) {
            bizagi.kendoMobileApplication.navigate("render");
            self.renderForm(params);
        });
        // process and show the render for new case
        this.subscribe("bz-newcase-render", function (e, params) {
            bizagi.kendoMobileApplication.navigate("render");
            self.cleanRender();
            self.newCaseRenderForm(params);
        });


        //notificar que debe guardar el formulario
        this.subscribe("bz-webparts-back", function (e, params) {
            self.backBtn(params);
            self.cleanRender(params);
        });

        //[uede ser que para activar el menu o para hacer el next
        this.subscribe("bz-webparts-click-btn", function (e, params) {
            self.clickBtn(params);
        });

        //        // Se genera un evento desde cases para que se despliegue la info desde render
        //        this.subscribe("ui-bizagi-process-newcase", function (e, params) { self.renderForm(params); });
        //        //el webPart de processes lanza este envento a cases, en ese momento la informaci�n del render ser�a inconsistente
        //        this.subscribe("ui-bizagi-show-cases", function (e, params) { self.emptyRenderForm(params); });

    },

    renderContent: function (params) {
        var self = this;
        self.content = $.fasttmpl(self.getTemplate('render-container'));
        return self.content;
    },
    /*
    *   Customize the web part in each device  
    */
    postRender: function (params) {
        var self = this;
        var content = self.getContent();
        // Attach handler to render container to subscribe for routing events
    },

    renderGateway: function (params) {
        var def = new $.Deferred();
        var self = this;
        var content = self.getContent();

        if (params.showgateway && params.showgateway == false) {
            def.resolve(false);
        }

        $.when(self.dataService.getWorkitems({ idCase: params.idCase,
            onlyUserWorkItems: true
        })).done(function (data) {
            if (data.workItems.length == 0) {
                bizagi.util.smartphone.stopLoading();
                def.resolve(true);
                return;
            }
            data.checkWorkItems = (data.workItems.length > 1) ? true : false;
            data.checkProcess = (data.subProcesses.length >= 1) ? true : false;
            var workItemsTemplate = self.getTemplate("render-routing");
            var workItemsHtml = $.tmpl(workItemsTemplate, data); //$.fasttmpl(workItemsTemplate, data);
            self.publish("popup", { html: workItemsHtml, callbackEvents: self.eventsPopUp, ref: self });
            bizagi.util.smartphone.stopLoading();
            content.empty();
            def.resolve(true);
        });
        return def.promise();
    },

    eventsPopUp: function (element, parameters) {
        var self = parameters.ref;
        $("button", element).one("click", function (e) {
            var idWorkitem = e.target.dataset.idworkitem;
            var idCase = e.target.dataset.idcase;
            var idTask = e.target.dataset.idtask;
            var isadynch = e.target.dataset.isasynch;
            if (idCase && (idWorkitem || idTask)) {
                self.publish("bz-show-render", { idCase: idCase, idWorkitem: idWorkitem, idTask: idTask });
            }
            parameters.closepopup();
            $("button", element).off("click");
        });
    },

    renderForm: function (params) {
        /*
        *   Creates a new case based on the selected process
        */
        var self = this;
        if (self.previousLoadRender) {
            // load autosave
            $.when(bizagi.util.autoSave()).done(function () {
                self.previousLoadRender = true;
                self.renderFormSub(params);
            });
        }
        else {
            self.previousLoadRender = true;
            self.renderFormSub(params);
        }

    },

    renderFormSub: function (params) {
        var self = this;
        // self.helper.addWaitContainer(self.waitContainer);
        bizagi.util.smartphone.startLoading();
        if (params.idWorkitem != undefined) {
            self.renderSmartphone(params);
        }
        else if (params.data != undefined && params.data.workItems != undefined && params.data.workItems.length > 0) {
            if (params.data.workItems.length == 1) {
                self.renderSmartphone({ "idCase": params.idCase, "idWorkitem": params.data.workItems[0].idWorkItem, showgateway: params.showgateway });
            }
            else {
                self.renderSmartphone({ "idCase": params.idCase, showgateway: true });
            }
        }
        else {
            //Render whit posible parallelGateway summary form
            self.renderSmartphone({ "idCase": params.idCase, showgateway: params.showgateway });
        }
    },
    newCaseRenderForm: function (params) {
        /*
        *   Creates a new case based on the selected process
        */
        var self = this;
        if (self.previousLoadRender) {
            // load autosave
            $.when(bizagi.util.autoSave()).done(function () {
                self.previousLoadRender = false;
                //El true se asigna en renderForm porque el evento se pudo originar en Cases
                self.newCaseRenderFormSub(params);
            });
        }
        else {
            self.previousLoadRender = false;
            //El true se asigna en renderForm porque el evento se pudo originar en Cases
            self.newCaseRenderFormSub(params);
        }
    },

    newCaseRenderFormSub: function (params) {
        var self = this;
        var idWfClass = params.idWfClass;

        bizagi.util.smartphone.startLoading();

        var defer = new $.Deferred();
        // Creates a new case
        $.when(self.dataService.createNewCase({
            idWfClass: idWfClass
        })).done(function (data) {

            //Load NewCase data in render form 
            params.idCase = data.idCase;
            params.data = data;
            params.idWfClass = null;
            self.renderForm(params);
            defer.resolve(data);
        }).fail(function (msg) {
            self.manageError(msg, defer);
        });
        return defer.promise();
    },

    renderSmartphone: function (params) {
        var self = this;
        bizagi.util.smartphone.startLoading();
        var content = self.getContent();
        self.cleanRender();
        if (content) {
            content.unbind("routing");
            content.bind("routing", function () {

                content.unbind("routing");
                //clean el del save
                //clean
                self.publish("executeAction", {
                    action: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
                    params: params
                });

            });
            //clean whe assigne
            $(self.canvas).find("#bz-container-render").bind("errorform", function (event, type) {
                bizagi.util.smartphone.stopLoading();
                $(self.canvas).find("#bz-container-render").unbind("errorform");
            });

        }


        if (typeof (self.rendering) == "undefined") {
            self.rendering = new bizagi.rendering.facade($.extend(params, { proxyPrefix: self.workportalFacade.dataService.serviceLocator.proxyPrefix }));
        }

        if (params.idWfClass != null) {
            self.newCaseRenderForm(params);
            return;
        }
        if (params.idWorkitem) {
            self.executeRender(params);
        }
        else {
            if (params && typeof params.isRenderGateway !== "undefined" && !params.isRenderGateway) {
                self.executeRender(params);
            }
            else {

                //call action routing
                content.empty();
                self.publish("executeAction", {
                    action: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
                    params: params
                });
            }
        }
    },
    executeRender: function (params) {
        var self = this;
        var content = self.getContent();
        self.rendering.execute($.extend(params, {
            canvas: $("#bz-container-render", self.canvas)//$("#bz-container-render")//content
        })).done(function (form) {
            self.form = form;

            self.form.observableElement.bind("routing", function () {
                self.form.dispose();

                self.publish("executeAction", {
                    action: bizagi.workportal.actions.action.BIZAGI_WORKPORTAL_ACTION_ROUTING,
                    params: params
                });

            });



            bizagi.util.smartphone.stopLoading();
        });

    },

    showFinishMessage: function (params) {
        var self = this;
        alert(self.resources.getResource("render-without-globalform"));

        //        var self = this;
        //        var content = self.getContent();
        //        //Add finish message when case is finish
        //        var errorTemplate = self.workportalFacade.getTemplate("info-render");
        //        var message = self.resources.getResource("render-without-globalform");
        //        var endMessageHtml = $.tmpl(errorTemplate, {
        //            message: message
        //        });
        //        // Load end Message   
        //        content.empty();
        //        endMessageHtml.appendTo(content);

    },

    cleanRender: function () {
        var self = this;
        $("#bz-container-render", self.canvas).empty();
    },

    clickBtn: function (params) {
        var self = this;
        self.form.performNext();
    },

    backBtn: function (params) {
        var self = this;
        if (self.form && self.form.performSave) {
            $.when(self.form.performSave()).done(function () {
                params.callback();

            });
        } else {
            params.callback();
            bizagi.util.smartphone.stopLoading();
        }
    }

    //revisar por que se agregar muchos next
    //manageError: function (params,defer) { }
});
