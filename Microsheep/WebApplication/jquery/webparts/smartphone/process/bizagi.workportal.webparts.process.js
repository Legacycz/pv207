
/*
*   Name: BizAgi Workportal Cases Webpart
*   Author: Diego Parra
*   Comments:
*   -   This script will define a base class to all widgets
*/

bizagi.workportal.webparts.webpart.extend("bizagi.workportal.webparts.process", {
    PAGE_SIZE: 10
}, {

    /*
    *   Constructor
    */
    init: function (workportalFacade, dataService, initialParams) {
        var self = this;
        // Call base
        this._super(workportalFacade, dataService, initialParams);
    },
    /*
    *   Renders the content for the current controller
    */
    renderContent: function (params) {
        //Override the login function, this is only necesary in WorkPortal
        bizagi.services.ajax.loginPage = function () { };
        var self = this;
        self.historycat = JSON.parse(bizagi.util.getItemLocalStorage("scategory"));
        return self.content = $.fasttmpl(self.getTemplate('container-process'));
    },
    /*
    *   Customize the web part in each device  
    */
    postRender: function (params) {
        var self = this;
        self.enableCategoryList();

        if (self.historycat && self.historycat.title)
         {
            self.setNameCategory(self.historycat.title);
        }
    },
    enableCategoryList: function (historycat) {
        var self = this;
        var context = self.getContent();
        $(".bz-wp-category-item.selecteditem", context).one("click", function () {
            var elementoclick = $(this)
            $.when(self.getCategoryList(historycat)).done(function () {

            });
        });
    },
    getCategoryList: function (params) {
        var self = this;
        var deferred = $.Deferred();
        var context = self.getContent();
        //cuando un filtro este seleccionado se debe recordar.
        //si no existe el filtro se trae con todas
        $.when(self.dataService.getAllProcesses({ taskState: "all", onlyFavorites: false })).done(function (list) {
            var html = $.fasttmpl(self.workportalFacade.getTemplate('process-items'), list);
            $(".bz-wp-ib-filter-category", context).html(html);
            $(".bz-wp-ib-filter-category .bz-wp-category-item", context).click(function () { self.eventSelectCategory(this) });
            $(".bz-wp-category-item.selecteditem", context).click(function () { $(".bz-wp-ib-filter-category", context).toggle("blind"); });
            //si no hay cookie siempre por defecto se pide all y el servidor responde por all cases siempre en 0
            if (params && params.title) {
            self.setNameCategory(params.title);
            }
            else {
            self.setNameCategory($(".bz-wp-ib-filter-category  > .bz-wp-category-item:first .workflow-name", context).text());
            }
            deferred.resolve();
        });

        return deferred.promise();
    },

    eventSelectCategory: function (element) {
        var self = this,
            context = self.getContent(),
            element = $(element),
            tmptitle = $(".workflow-name", element).text(),
            tmpidWfClass = element.data("idworkflow");
        $(".workflow-name.selected", context).text(tmptitle);
        $(".bz-wp-ib-mail-container", context).empty();
        var jsonCategory = JSON.stringify({ 'pageSize': self.Class.PAGE_SIZE, 'idWfClass': tmpidWfClass, 'title': tmptitle })
        bizagi.util.setItemLocalStorage("scategory", jsonCategory);
        self.historycat = JSON.parse(jsonCategory);

        self.publish("getCasesList", { pageSize: self.Class.PAGE_SIZE, idWfClass: tmpidWfClass });

        $(".bz-wp-ib-filter-category", context).hide();
    },

    setNameCategory: function (title) {
        var self = this;
        var context = self.getContent();
        $(".workflow-name.selected", context).text(title);
    }



});
