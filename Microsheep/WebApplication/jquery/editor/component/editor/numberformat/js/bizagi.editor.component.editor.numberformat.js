﻿/*
@title: Editor numberformat
@authors: Paola Herrera
*/
bizagi.editor.component.editor(
    "bizagi.editor.component.editor.numberformat", {
        /*
         * Constructor
         */
        init: function (canvas, model, controller) {
            this._super(canvas, model, controller);
            this.focus = null;
        },
        /*
         * Process the information about of editor and render it
         */
        renderEditor: function (container, data) {
            var self = this;

            var elPopUpContent, samplevalue;

            var elEditor = $.tmpl(self.getTemplate("frame"), data);
            elEditor.appendTo(container);

            if(data.value === undefined) {
                data.value = self.defaultValue ? self.defaultValue : bizagi.localization.getResource("formmodeler-component-editor-dateformat-default-format");
            } else {
                if(data.value === null) {
                    data.value = self.defaultValue ? self.defaultValue : bizagi.localization.getResource("formmodeler-component-editor-dateformat-default-format");
                }
            }
            self.inputValue = data.value;

            // modal
            self.elPopUpContent = elPopUpContent = $.tmpl(self.getTemplate("modal-content"), data);
            self.createEditorUIControls(data, elPopUpContent);
            self.dialogReference = self.createDialog(self.elPopUpContent, self.elPopUpActions);
            self.sampleInput = $(".numberformat-modal-number-sample", self.elPopUpContent);

            $('.numberformat-options-default-format-false', elPopUpContent).hide();
            $('.numberformat-modal-thousands', elPopUpContent).hide();
            $('.numberformat-fields .numberformat-field-number', self.element).val(self.inputValue);

            samplevalue = "formmodeler-component-editor-numberformat-sample-" + self.numberFormatComboBox.getValue();
            self.sampleInput.text(bizagi.localization.getResource(samplevalue));
        },

        loadTemplates: function () {
            var deferred = $.Deferred();
            $.when(
                this.loadTemplate("frame", bizagi.getTemplate("bizagi.editor.component.editor.numberformat").concat("#numberformat-frame")),
                this.loadTemplate("modal-content", bizagi.getTemplate("bizagi.editor.component.editor.numberformat").concat("#numberformat-modal"))
                ).done(function () {
                    deferred.resolve();
                });
            return deferred.promise();
        },

        ".numberformat-fields > input click": function (el) {
            var self = this;
            self.openDialog(el);
        },

        openDialog: function (el) {

            var self = this, values, initValueDateFormat;
            self.showDialog(el);

            if (self.options.value === bizagi.localization.getResource("formmodeler-component-editor-dateformat-default-format")) {
                initValueDateFormat = true;
            } else {
                initValueDateFormat = false;
            }

            self.mySwitchDefaultFormat.update(initValueDateFormat);
            self.responseChangeDefaultFormat(initValueDateFormat, null, self);
        },

        // Create UICOntrols
        createEditorUIControls: function (data, elPopUpContent) {
            var self = this;
            var percentage = bizagi.localization.getResource("formmodeler-component-editor-numberformat-percentage-number");
            var thousands = bizagi.localization.getResource("formmodeler-component-editor-numberformat-thousand-number");
            var coma = bizagi.localization.getResource("formmodeler-component-editor-numberformat-coma-number");
            var point = bizagi.localization.getResource("formmodeler-component-editor-numberformat-point-number");

            var numberFormats = [{icon: "date",label: percentage, value: "percentage" },
                                 {icon: "date",label: thousands, value: "thousands" } ];

            var thousandsFormats = [{icon: "date",label: coma, value: "coma" },
                                {icon: "date",label: point, value: "point" } ];
            /*
             UI Controls
             bizagi.editor.component.editor.uiControls.booleanSwitch
             */
            self.mySwitchDefaultFormat = new self.uiControls.booleanSwitch(
                {
                    uiEditor: self,
                    uiInitValue: data.initValueDateFormat,
                    uiContainer: $('.numberformat-modal-default-format', elPopUpContent),
                    uiValues: (data.dateFormatUIValues) ? data.dateFormatUIValues.uiValues : data.dateFormatUIValues,
                    uiInline: self.inline,
                    onChange: function (elValue, event) {
                        self.responseChangeDefaultFormat(elValue, elPopUpContent);
                    }
                });

            self.numberFormatComboBox = new self.uiControls.comboBox(
                {
                    uiEditor: self,
                    uiContainer: $(".ui-control-editor.number-format", elPopUpContent),
                    uiInitValue: data.value,
                    uiValues: numberFormats,
                    uiInline: self.inline,
                    onChange: function (elValue, event) {
                        self.responseNumberFormatComboChange(elValue, elPopUpContent);
                    }
                });

            self.thousandsFormatComboBox = new self.uiControls.comboBox(
                {
                    uiEditor: self,
                    uiContainer: $(".ui-control-editor.thousands-format", elPopUpContent),
                    uiValues: thousandsFormats,
                    uiInline: self.inline,
                    onChange: function (elValue, event) {
                        self.responseThousandFormatComboChange(elValue, elPopUpContent);
                    }
                });
        },

        responseChangeDefaultFormat: function (elValue, elPopUpContent) {
            if (elValue) {
                $('.numberformat-options-default-format-false', elPopUpContent).hide();
            } else {
                $('.numberformat-options-default-format-false', elPopUpContent).show();
            }
        },

        responseNumberFormatComboChange: function (elValue, elPopUpContent) {
            var self = this, samplevalue;
            var modalThousands = $('.numberformat-modal-thousands', elPopUpContent);

            if (elValue == "thousands") {
                modalThousands.show();
                samplevalue = "formmodeler-component-editor-numberformat-sample-" + self.thousandsFormatComboBox.getValue();
                self.sampleInput.text(bizagi.localization.getResource(samplevalue));
            } else {
                modalThousands.hide();
                samplevalue = "formmodeler-component-editor-numberformat-sample-" + elValue;
                self.sampleInput.text(bizagi.localization.getResource(samplevalue));
            }
        },

        responseThousandFormatComboChange: function (elValue, elPopUpContent) {
            var self = this, samplevalue;

            samplevalue = "formmodeler-component-editor-numberformat-sample-" + elValue;
            self.sampleInput.text(bizagi.localization.getResource(samplevalue));
        }
    }
);