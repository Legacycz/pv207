﻿/*
*   Name: BizAgi Editor Component Properties  Commands Group by
*   Author: Alexander Mejia
*   Comments:
*   -   This script will define basic stuff for command
*/

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.groupby", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid())
            self.indexedProperty.notShow = true;

    },

    /*
    * The groupby property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }

});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.showcolumn", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid())
            self.indexedProperty.notShow = true;

    },

    /*
    * The showcolumn property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }

});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.displaytype", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid())
            self.indexedProperty.notShow = true;

    },

    /*
    * The groupby property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }

});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.isadministrable", {},
{

    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var entityType = self.element.triggerGlobalHandler("getContextEntityType");        
        if (entityType != "parameter")
            self.indexedProperty.notShow = true;
               
    }
});


bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.selectProcess", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var entityType = self.element.triggerGlobalHandler("getContextEntityType");

        if (entityType != "application")
            self.indexedProperty.notShow = true;
        else {           
            self.indexedProperty.allowClick = self.element.triggerGlobalHandler("getControllerInfo", { type: "isNewForm" });
        }

    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.allowdecimals", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            var dataType = node && node.getDataType();

            function allowDecimals(dataType) {
                if (dataType == 'OracleNumber') { return false;}
                if (dataType == 'BigInt') { return false; }
                if (dataType == 'Int') { return false; }
                if (dataType == 'SmallInt') { return false; }
                if (dataType == 'TinyInt') { return false; }

                return true;
            }


            if (!allowDecimals(dataType)) {
                self.indexedProperty.notShow = true;
                self.properties.allowdecimals = false;
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.defaultinclude", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        if (self.parentIsGrid() || !self.properties.selectable )
            self.indexedProperty.notShow = true;

    },

    /*
    * The groupby property only aplies to groupedgrid control
    * if the control parent isn't a groupedgrid, this property is hidden   
    */
    parentIsGrid: function () {
        var self = this;

        var element = self.element;
        var parent = element.parent;

        return parent.type == "grid";
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.numberrange", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            if (node && node.getRenderType() == "oracleNumber") {
                self.properties.allowdecimals = false;
                self.indexedProperty.notShow = true;                
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.defaultvalue", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            if (node && node.getRenderType() == "oracleNumber") {
                self.indexedProperty.notShow = true;
            }
        }
    }
});


bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.maxlines", {},
{
    execute: function () {
        var self = this;
        if (self.element.properties.isextended == false || self.element.properties.isextended == "false")
            self.indexedProperty.show = false;
        else
            self.indexedProperty.show = true;
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.formatvalue", {},
{
    execute: function () {
        var self = this;
        if (self.element.properties.displaytype == "label") {
            self.indexedProperty.show = false;
        } else {
            self.indexedProperty.show = true;
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.format", {},
{
    execute: function () {
        var self = this;
        if (self.element.properties.displaytype == "value") {
            self.indexedProperty.show = false;
        } else {
            self.indexedProperty.show = true;
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.xpath", {},
{
    execute: function () {
        var self = this;
        if (self.element.properties.typedinamic){
            if (self.element.properties.typedinamic === "databind" && self.properties.allowdinamiclabel) {
                self.indexedProperty.show = true;
            } else {
                self.indexedProperty.show = false;
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.data.filter", {},
{
    execute: function () {
        var self = this;
        if (self.element.properties.typedinamic){
            if (self.element.properties.typedinamic === "databind" && self.properties.allowdinamiclabel) {
                self.indexedProperty.show = true;
            } else {
                self.indexedProperty.show = false;
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.data.displayattrib", {},
{
    execute: function () {
        var self = this;
        if (self.element.properties.typedinamic){
            if (self.element.properties.typedinamic === "databind" && self.properties.allowdinamiclabel) {
                self.indexedProperty.show = true;
            } else {
                self.indexedProperty.show = false;
            }
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.datarule", {},
{
    execute: function () {
        var self = this;

        if (self.element.properties.typedinamic === "ruleexpression" && self.properties.allowdinamiclabel) {
            self.indexedProperty.show = true;
        } else {
            self.indexedProperty.show = false;
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.typedinamic", {},
{
    //bindingtype
    execute: function () {
        var self = this;
        if (self.properties.allowdinamiclabel) {
            self.indexedProperty.show = true;
        } else {
            self.indexedProperty.show = false;
            self.element.requiredPropertiesOverrides({"xpath":false,"data.displayattrib":false,"data.filter":false,"datarule":false});
        }

        if (self.element.properties.typedinamic === "databind" && self.properties.allowdinamiclabel) {
            self.element.requiredPropertiesOverrides({"xpath":true,"data.displayattrib":true,"data.filter":true,"datarule":false});
            self.element.setProperty("datarule",undefined);
        } else if(self.element.properties.typedinamic === "ruleexpression" && self.properties.allowdinamiclabel){
            self.element.requiredPropertiesOverrides({"xpath":false,"data.displayattrib":false,"data.filter":false,"datarule":true});
            self.element.setProperty("xpath", undefined);
            self.element.setProperty("data.displayattrib",undefined);
            self.element.setProperty("data.filter","");
        }
        self.element.validRequiredProperties();
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.displayName", {},
{
    execute: function () {
        var self = this;

        if (self.properties.allowdinamiclabel) {
            self.indexedProperty.show = false;
        } else {
            self.indexedProperty.show = true;
        }
    }
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.required", {},
    {
        execute: function () {
            var self = this;
            if (self.properties.display === "check" && self.properties.isexclusive) {
                self.indexedProperty.show = false;
            } else {
                self.indexedProperty.show = true;
            }
        }
    });

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.isexclusive", {},
    {
        execute: function () {
            var self = this;
            var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
            if (self.properties.display == "option" && isGridContext) {
                self.indexedProperty.show = false;
                self.properties.isexclusive = false;
            } else {
                self.indexedProperty.show = true;
            }
            if (self.properties.display === "check" && self.properties.isexclusive) {
                var def = self.element.triggerGlobalHandler("getControllerInfo", {
                    type: "searchDependencies",
                    guids: [self.args.element.guid],
                    message: bizagi.localization.getResource("formmodeler-command-searchdependencies-error-column-check")
                });
                $.when(def).done(function (response) {
                    if (!response.result) {
                        self.properties.isexclusive = false;
                    }
                });
            }
        }
    });

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.totalize", {},
    {
        execute: function () {
            var self = this;
            if (self.properties.display === "check" && self.properties.isexclusive) {
                self.indexedProperty.show = false;
            } else {
                self.indexedProperty.show = true;
            }
        }
    });

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.submitonchange", {},
    {
        execute: function () {
            var self = this;
            if (self.properties.display === "check" && self.properties.isexclusive) {
                self.indexedProperty.show = false;
            } else {
                self.indexedProperty.show = true;
            }
        }
    });

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.thousands", {},
{
    /*
    *   Executes the command
    */
    execute: function () {
        var self = this;

        var isGridContext = self.element.triggerGlobalHandler("getControllerInfo", { type: "isGridContext" });
        var xpathNavigatorModel = (isGridContext) ? self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModelGrid" }) : self.element.triggerGlobalHandler("getControllerInfo", { type: "getXpathNavigatorModel" });

        if (self.properties.xpath) {
            var xpath = bizagi.editor.utilities.resolveComplexXpath(self.properties.xpath);
            node = xpathNavigatorModel.getNodeByXpath(xpath);
            if (node.getDataType() == "TinyInt") {
                //TODO: ajustar segun reglas de tiny integer
                 self.indexedProperty.notShow = true;
                self.properties.allowdecimals = false;
            }
        }
	}
});

bizagi.editor.component.properties.commands.base.extend("bizagi.editor.component.properties.commands.newrecords", {},
{
    /*
     *   Executes the command
     */
    execute: function () {
        var self = this;

        if (self.properties.newrecords) {
            if (bizagi.util.parseBoolean(self.properties.newrecords.allownew))   {
                self.element.requiredPropertiesOverrides({ "newrecords.allownewform": true });
            } else if (!bizagi.util.parseBoolean(self.properties.newrecords.allownew)){
                self.element.requiredPropertiesOverrides({ "newrecords.allownewform": false });
            } else {
            }
        }
    }
});
