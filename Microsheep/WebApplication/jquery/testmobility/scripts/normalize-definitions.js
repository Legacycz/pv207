/**
 * Created by RicardoPD on 11/26/2014.
 */

//var vGlobals = require('../config/globalConfig');

require("shelljs/global");

var device = "desktop";
var modules = ["rendering", "workportal"];
var port = 8001;
var normalizeFile = "config/bizagi.module.definition.js";

process.argv.forEach(function(val, index, array) {
    if(val.indexOf("device=") !== -1) {
        device = val.split("=")[1];
    }
    if(val.indexOf("modules=") !== -1) {
        modules = val.split("=")[1].split(",");
    }
    if(val.indexOf("port=") !== -1) {
        port = val.split("=")[1];
    }
    if(val.indexOf("output=") !== -1) {
        normalizeFile = val.split("=")[1];
    }
});

console.log("Device: ", device);
console.log("Modules: ", modules);
console.log("Port: ", port);


var bizagiModules = {};

var getFiles = function(device, callback) {
    var fs = require("fs");
    var pathToBase = "";
    var configFile = "bizagi.module.definition.json.txt";

    /**
     * Default files to include
     * @type {string[]}
     */
    var result = [
       // {"pattern": "http://localhost:" + port + "/" + vGlobals.bizagiConfig.branch + "/jquery/steal.js", "included": true, "served": true},
        {"pattern": "http://localhost:" + port + "/jquery/steal.js", "included": true, "served": true},
        {"pattern": pathToBase + "bizagi.loader.js", "included": true, "served": true}
    ];

    device = device || "desktop";
    var fileMapping = {};

    console.log("loading dependencies");



    return fs.readFile("../" + configFile, "utf-8", function (err, data) {
        if (err) {
            console.log(err);
        }
        var dataConfig = JSON.parse(data);
        var webparts = [];

        // map files
        dataConfig.files.js.forEach(function (value) {
            fileMapping[value.name] = value.location;
        });

        var getPaths = function (data) {
            var paths = [];

            data.forEach(function (value) {
                if (typeof fileMapping[value] != "undefined") {
                    // Replace jquery folder
                    var file = fileMapping[value].replace("jquery/", "");
                    var pattern = { pattern: pathToBase + file, included: true, served: true};

                    paths.push(pattern);
                }
            });
            return paths;
        };

        var getWebpartsPaths = function () {
            var paths = [];
            dataConfig.webparts.forEach(function (value) {
                if(webparts.indexOf(value.name) !== -1 ){
                    var location =  value.location.replace("jquery/", "");
                    var strFile=  fs.readFileSync("../" + location + "/webpart.definition.json.txt", "utf8");
                    var objFile = JSON.parse(strFile.trim());

                    if (objFile.devices){
                        if(typeof objFile.devices[device] !== 'undefined'){
                            objFile = objFile.devices[device];
                        }
                        else{
                            console.log("No existe la definicion del device " + device + " Para el webpart " + webpart.name);
                        }
                    }
                    objFile.js.forEach(function (value) {
                        var pattern = { pattern: pathToBase + location + value.src, included: true, served: true};
                        console.log(pattern);
                        paths.push(pattern);
                    });

                }

            });
            return paths;
        };

        var getDeviceData = function (data, device) {
            var result = [];
            data.forEach(function (value) {
                if (value.name === device) {
                    result = value.components.js;
                    if (typeof value.components.webparts !== "undefined" ) {
                        value.components.webparts.forEach(function (val) {
                            if (webparts.indexOf(val) === -1) {
                                webparts.push(val);
                            }
                        });
                    }
                }
            });
            return result;
        };

        var getModuleData = function (data, module) {
            var result = [];
            data.forEach(function (value) {
                if (value.name == module) {
                    result = value.devices;
                }
            });
            return result;
        };

        if(modules) {
            var addPath = function(value) {
                result.push(value);
            };
            for(var module in modules) {
                var moduleData = getModuleData(dataConfig.modules, modules[module]);
                var deviceData = getDeviceData(moduleData, device);
                getPaths(deviceData).forEach(addPath);
            }

            getWebpartsPaths(webparts).forEach(function(value) {
                result.push(value);
            });
        }

        callback(result);
    });
};

var getHeaderContent = function(globalName) {
    return "var " + globalName + "=";
};

var getExportModuleContent = function(globalName) {
    return "if(typeof module != 'undefined' && module.exports){module.exports = " + globalName + ";}";
};

var writeNormalizeFile = function(data) {
    var fs = require("fs");
    chmod("755", normalizeFile);

    fs.writeFile(normalizeFile, data, function (err) {
        if (err) {
            console.log(err);
        } else {
            console.log("The file "+normalizeFile+" has been saved!");
        }
    });
};


bizagiModules.desktop = getFiles(device, function(data) {
    var moduleName = "bizagiModules";
    var text = getHeaderContent(moduleName);
    text += JSON.stringify(data) + ";\n";
    text += getExportModuleContent(moduleName);
    writeNormalizeFile(text);
});