﻿/**
 * Created by RicardoPD on 6/6/2014.
 */
var util = require('util');
var ejs = require('ejs');
var file = require("fs");


var CURRENT_TYPE = 'workportal'; // default

process.argv.forEach(function(val, index, array) {
    if(val.indexOf("environment=") !== -1) {
        CURRENT_TYPE = val.split("=")[1];
    }
});

var path = './scripts/reporter/e2e.tmpl.ejs'
    , str = file.readFileSync(path, 'utf8');

var e2eHtml = '<link href="../../../resources/libs/bootstrap.min.css" rel="stylesheet" type="text/css">';
e2eHtml += '<link href="../../../resources/libs/e2e.report.css" rel="stylesheet" type="text/css">';
e2eHtml += '<link href="../../../resources/libs/ide.css" rel="stylesheet" type="text/css">';
e2eHtml += '<script src="../../../resources/libs/jquery.min.js" type="text/javascript"></script>';
e2eHtml += '<script src="../../../resources/libs/bootstrap.min.js" type="text/javascript"></script>';
e2eHtml += '<script src="../../../resources/libs/jquery.collapsible.min.js" type="text/javascript"></script>';

e2eHtml += '<div class="inner-content"><h1>E2E Test Results Tablet</h1>';
e2eHtml += '<div class="buttons"><button id="btnCollapse">Collapse all</button><button id="btnExtend">Extend all</button></div>';

var tests = [];

var testHead = '';
var testSteps = '';
var bFlag;

function setHeader(testRun){
    e2eHtml += '<div class="big-container-' + testRun.browserOptions.browserName + '">';
    e2eHtml += '<table cellspacing="0" cellpadding="0"><tr class="overview"><td colspan="4">Browser: ' + testRun.browserOptions.browserName + '</tr>';
    e2eHtml += '<tr class="overview"><td colspan="4">Timestamp: ' + Date().toString() + '</tr>';
    e2eHtml += '<tr><td colspan="4"></tr></table><div class="panel-group" id="container_' + testRun.browserOptions.browserName + '">';
}

function groupByCategory(data){
    var groups = {};

    for(var i = 0; i < data.length; i++) {
        var item = data[i];

        if(!groups[item.category]) {
            groups[item.category] = {result: [], success: true, total: 0, pass: 0};
        }

        groups[item.category].result.push({
            surname: item.name.split('.')[1],
            name: item.name,
            success: item.success,
            time: item.time,
            steps: item.steps
        });

        groups[item.category].total ++;
        groups[item.category].pass= groups[item.category].pass + (item.success ? 1 : 0);
        groups[item.category].success = (groups[item.category].success && item.success);
    }

    var result = [];

    for(var x in groups) {
        if(groups.hasOwnProperty(x)) {
            var obj = {};
            obj[x] = groups[x];
            result.push(obj);
        }
    }
    return result;

}

function groupByGroup(data){
    var groups = {};

    for(var i = 0; i < data.result.length; i++) {
        var item = data.result[i];

        if(!groups[item.surname]) {
            groups[item.surname] = {result : [], success: true, total: 0, pass: 0};
        }

        groups[item.surname].result.push({
            name: item.name,
            success: item.success,
            time: item.time,
            steps: item.steps
        });

        groups[item.surname].total ++;
        groups[item.surname].pass = groups[item.surname].pass + (item.success ? 1 : 0);
        groups[item.surname].success = (groups[item.surname].success && item.success);
    }


    var result = [];

    for(var x in groups) {
        if(groups.hasOwnProperty(x)) {
            var obj = {};
            obj[x] = groups[x];
            result.push(obj);
        }
    }
    data.result = result;
    return data;
}

function renderHtml(browser){
    if (tests.length > 0) {
        tests = groupByCategory(tests);

        for(var i = 0, len = tests.length; i< len; i++){
            var test = tests[i];
            for(var x in test) {
                if(test.hasOwnProperty(x)) {
                    test[x] = groupByGroup(test[x]);
                }
            }

        }

        e2eHtml += ejs.render(str, {"browser" : browser ,"tests": tests});
    }
}

/** An example interpreter listener factory with all listener functions implemented. */
exports.getInterpreterListener = function (testRun) {
    var browser = "";
    var category = "";
    var steps = [];

    var timeTest;
    var timeStep;

    return {
        'startTests': function () {
            console.log('Listener: test run starting!');
            bFlag = testRun.browserOptions.browserName;
            setHeader(testRun);
        },
        'startTestRun': function (testRun, info) {
            console.log("Listener: test run starting!");
            timeTest = (new Date()).getTime();
            testHead = '';
            testSteps = '';
            /*  console.log("Listener: success: " + info.success);
            console.log("Listener: error: " + util.inspect(info.error));*/
        },
        'endTestRun': function (testRun, info) {
            browser = testRun.browserOptions.browserName;
            category = testRun.name.split('.')[0];

            console.log("Listener: test run ending!");
            if (bFlag !== browser) {
                renderHtml(bFlag);

                e2eHtml += '</div></div>';

                setHeader(testRun);

                bFlag = browser;
                tests = [];
            }

            var data = { category: category, name: testRun.name, success: info.success, time: ((new Date()).getTime() - timeTest), steps: steps };
            tests.push(data);
            steps = [];
        },
        'startStep': function (testRun, step) {
            console.log("Listener: step starting!");
            timeStep = (new Date()).getTime();

        },
        'endStep': function (testRun, step, info) {
            steps.push({ jsonStep: JSON.stringify(step), success: info.success, time: ((new Date()).getTime() - timeStep) });
            info.error ? console.log(info.error) : "";
        },

        'endTests': function (result) {
            console.log("Fin de pruebas E2E -" + CURRENT_TYPE);

            renderHtml(bFlag);

            e2eHtml += '</div></div>';



            var date = new Date((new Date()).setHours(0, 0, 0, 0));
            var formatFolder = (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear();
            var path = './logs/mobility-report/jsonresult/tablet/' + formatFolder;
            var resultFormatFolder = formatFolder.replace(new RegExp('-', 'g'), '');
            console.log(path);
            //note its not recursive
            file.mkdir(path, 0777, function (err) {
                if (err) {
                    if (err.code == 'EEXIST') {
                        console.log("Ya existe el folder:" + path);
                        file.writeFileSync(path + "/e2e-tablet-"+ CURRENT_TYPE + ".js", "var mobilityE2E_" + resultFormatFolder + " = " + JSON.stringify(result) + ";");
                    } // ignore the error if the folder already exists
                    else console.log(err); // something else went wrong
                } else {
                    console.log("Se creo correctamente el Folder:" + path);
                    file.writeFileSync(path + "/e2e-tablet-"+ CURRENT_TYPE + ".js", "var mobilityE2E_" + resultFormatFolder + " = " + JSON.stringify(result) + ";");
                } // successfully created folder

            });

            var pathJsonDefinition = './logs/mobility-report/jsonresult/tablet/historicaldef.json';
            file.readFile(pathJsonDefinition, 'utf8', function (err, data) {
                if (err) {
                   // console.log('REVISE EL ARCHIVO ./logs/jsonresult/tablet/historicaldef.json --Error: ' + err);
                    console.log('Se crea el archivo vacío --Error: ' + err);

                    file.writeFileSync(pathJsonDefinition, "var historic =  ['" + formatFolder + "'];");

                    return;
                }
                data = eval(data); //JSON.parse(data);
                //al realizar el eval se crea historic
                //no existe en el json
                if (historic.indexOf(formatFolder) == -1) {
                    historic.push(formatFolder);
                    file.writeFileSync(pathJsonDefinition, "var historic =  " + JSON.stringify(historic) + ";"); //JSON.stringify(data) );
                }
                console.log("Se actualizó el archivo de históricos");
            });

            file.writeFileSync("./logs/e2e/tablet/e2e-tablet-"+CURRENT_TYPE+".html",
                e2eHtml);
        }
    };
};