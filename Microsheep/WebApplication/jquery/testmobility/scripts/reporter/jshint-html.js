/**
 * Created by RicharU on 19/01/2015.
 */
"use strict";

module.exports = {

    /**
     * Make a new JSHINT report.
     *
     * @method reporter
     *
     * @param  {Array} files List of files *.js.
     */
    reporter: function(files) {

        var fs = require("fs");

        var BASE_PATH = mobilityHelper.getCurrentDirectory(global.base_path);
        var CURRENT_DEVICE = global.device ? global.device : 'smartphone';

        var REPORT_DIRECTORY_HINT = mobilityHelper.validateDirectory(BASE_PATH + 'logs/mobility-report/jsonresult/' + CURRENT_DEVICE) + '/';
        var STATIC_CODE_DIRECTORY_HINT = mobilityHelper.validateDirectory(BASE_PATH + 'logs/static-code/' + CURRENT_DEVICE) + '/';

        var date = new Date((new Date()).setHours(0, 0, 0, 0));
        var formatFolder = (date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear();

        var resultFormatFolder = formatFolder.replace(new RegExp('-', 'g'), '');
        var pathToSaveFile = REPORT_DIRECTORY_HINT + formatFolder + '/';

        var lenFiles = files.length;
        var html = '';

        global.results = global.results || {};

        if (lenFiles === 0) {
            global.results.hint = {
                "result": "success"
            };
            console.log("DONE");
        } else {

            global.results.hint = {
                "result": "error"
            };
            global.results.hint.errors = [];

            console.log(["\nERROR LIST (", lenFiles, ")\n\n"].join("").red);

            html += '<style>html,h1{font-size:30px;color:#bbb;margin-top:0px;}body{color:#000;font-size:14px;font-family:Segoe UI,Arial,sans-serif;margin:0;padding:0;}body{padding:10px 40px;}table{width:100%;margin-bottom:20px;}tr.header{background:#ddd;font-weight:bold;border-bottom:none;}td{font-size:14px;padding:7px;border-top:none;border-left:1px black solid;border-bottom:1px black solid;border-right:none;}tr.pass td{color:#003b07;background:#B5D592;}tr.skip td{color:#7d3a00;background:#ffd24a;}tr.fail td{color:#5e0e00;background:#ff9c8a;}tr:first-child td{border-top:1px black solid;}td:last-child{border-right:1px black solid;}tr.overview{font-weight:bold;color:#777;}tr.overview td{padding-bottom:0px;border-bottom:none;}tr.system-out td{color:#777;}hr{height:2px;margin:30px 0;background:#000;border:none;}td.alignCenter{text-align: center}</style>';
            html += '<h1>JS Quality Report (' + lenFiles + ' warning)</h1>';
            html += '<p>Last update :  ' + new Date() + '</p>';
            html += '<table border="0" cellspacing="0" cellpadding="0">';
            html += '<tr class="header">';
            html += '<td class="alignCenter">' + '#' + '</td>';
            html += '<td class="alignCenter">' + 'Description' + '</td>';
            html += '<td class="alignCenter">' + 'File' + '</td>';
            html += '<td class="alignCenter">' + 'Line' + '</td>';
            html += '<td class="alignCenter">' + 'Character' + '</td>';
            html += '</tr>';

            for (var i = 0; i < lenFiles; i++) {

                var error = files[i].error;

                var currentError = {
                    "file": files[i].file,
                    "message": error.reason,
                    "line": error.line,
                    "character": error.character,
                    "code": error.code
                };

                global.results.hint.errors.push(currentError);

                var output = ["[", (i + 1), "] ", "[", currentError.code, "]", "[File:", currentError.file, "] [Line:", currentError.line, "] [Column:", currentError.character, "]"];

                html += '<tr class="skip">';
                html += '<td class="alignCenter">' + (i + 1) + '</td>';
                html += '<td>' + '[' + currentError.code + ']' + error.reason + '</td>';
                html += '<td>' + currentError.file + '</td>';
                html += '<td class="alignCenter">' + currentError.line + '</td>';
                html += '<td class="alignCenter">' + currentError.character + '</td>';
                html += '</tr>';

                console.log("\n" + output.join('').red);
                console.log(currentError.message.blue);
            }

            html += '<tr class="skip"><td colspan="5">&nbsp;</td></tr>';
            html += '</table>';
        }

        console.log("\nStarting...".green);

        console.log("\n\n--------------------------------------------------------------------------------".green);
        console.log("                                JSHINT REPORT ".green);
        console.log("--------------------------------------------------------------------------------".green);

        console.log(pathToSaveFile + " [" + resultFormatFolder + "]");

        fs.mkdir(pathToSaveFile, 777, function(err) {

            if (err) {
                if (err.code == 'EEXIST') {
                    // Ignore the error if the folder already exists
                    console.log(("\nThe folder already exists: " + pathToSaveFile).red);
                    mobilityHelper.saveContent((pathToSaveFile + "jshint.js"), ("var jshint_" + resultFormatFolder + " = " + JSON.stringify(files) + ";"));

                } else {
                    // something else went wrong
                    console.log(('\n>> ' + err).red);
                }
            } else {
                // successfully created folder
                console.log(("\nSuccessfully created folder: " + pathToSaveFile).green);
                mobilityHelper.saveContent((pathToSaveFile + "jshint.js"), ("var jshint_" + resultFormatFolder + " = " + JSON.stringify(files) + ";"));
            }
        });

        console.log("--------------------------------------------------------------------------------".green);

        // Save Report
        mobilityHelper.saveContent((STATIC_CODE_DIRECTORY_HINT + 'hint.html'), html);
    }

};

/**
 * Helper Functions for JSHINT report.
 *
 * @autor RicharU <Richar.Urbano@Bizagi.com>
 *
 */

var mobilityHelper = {

    _fs: require('fs'),

    /* Get current path by global variable */
    getCurrentDirectory: function(path) {

        var currentDirectory = process.cwd();
        //var saveFile = currentDirectory + '/scripts/reporter/jshint-directory.js';

        // Get current directory
        if (typeof(path) !== 'undefined' && path != '' && path != null) {
            currentDirectory = path.replace('?', ':');
        }

        if (currentDirectory[currentDirectory.length - 1] != '/') {
            currentDirectory = currentDirectory.concat('/');
        }

        // Save reports directory
        //mobilityHelper.saveContent(saveFile, ("var jshint_directory = '" + currentDirectory + "';"));

        return currentDirectory;
    },

    /* Create root directory */
    validateDirectory: function(directory) {
        var fs = mobilityHelper._fs || require('fs');

        var listDirectories = directory.replace(/\/$/, '').split('/');

        for (var i = 1, directoryNumber = listDirectories.length; i <= directoryNumber; i++) {
            var segment = listDirectories.slice(0, i).join('/');
            !fs.existsSync(segment) ? fs.mkdirSync(segment) : null;
        }

        return directory;
    },

    /* Save data to file */
    saveContent: function(path, content) {

        var fs = mobilityHelper._fs || require('fs');

        fs.writeFileSync(mobilityHelper._assignWritablePermissions(path), content);
    },

    /* Assign writable permissions (0777)*/
    _assignWritablePermissions: function(file) {

        var fs = mobilityHelper._fs || require('fs');

        if (fs.existsSync(file)) {
            fs.chmod(file, 511);
        }

        return file;
    }
};