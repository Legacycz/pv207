/**
 * Created by FernandoL on 21/11/2014.
 */
(function($) {
    jQuery.fn.extend({
        scriptLoader: function(scripts) {
            var chain = $.Deferred();
            chain.resolve();
            scripts.forEach(function(script, index, array) {
                chain = chain.then(function() {
                    return $.getScript(script, function(data, status) {
                        console.info('Loaded: ' + status + ' - ' + script);
                    });
                });
            });
            chain.then(function() {
                console.info('scriptLoader completed');
            });
            return chain.promise();
        }
    });

    $.urlParam = function(name) {
        var results = new RegExp('[\?&amp;]' + name + '=([^&amp;#]*)').exec(window.location.href);
        return (results != null) ? results[1] : ''; //results[1] || 0;
    }

})(jQuery);

$(window).load(function() {
    var device = $(".js-select-device option:selected").val();
    changeSelect();
    localStorage.setItem('device', device);
});

function getDevice() {
    device = localStorage.getItem('device');
};

function changeSelect() {
    $(".js-select-device option").removeAttr("selected");
    $.each($(".js-select-device option"), function(key, value) {
        if (value.value == device) {
            $(this).prop("selected", true);
        }
    });
    device = localStorage.getItem('device');
};

$(".js-select-device").on('change', function() {
    var selection = $(this).val();
    console.log(selection);
    activateDevice(selection);
});

function activateDevice(newDevice) {
    localStorage.setItem('device', newDevice);
    location.reload();
};

function initReports() {
    var lastDate = '',
        previousDate = '',
        scriptToLoad = [];
    var file = "../testmobility/logs/mobility-report/jsonresult/historicaldef.json";

    // Get current device
    getDevice();

    // Select current device
    $.each($(".js-select-device a"), function(key, value) {
        var selection = $(this).data('device');
        if (selection == device) {
            $('.js-button').text(selection);
            $(this).attr("selected", "selected");
        }
    });

    if (device == "") {
        device = "smartphone";
    }

    // Get current history
    $.getJSON(file, function(result) {

        console.log("Historial: " + JSON.stringify(result));
        var listData = result.historic || {};
        historic = listData[device];

        for (var iterator in historic) {
            var dateToLoad = lastDate = historic[iterator];

            if (iterator != 0) {
                previousDate = historic[iterator - 1]
            }

            addDateToCombosOptions(dateToLoad);
            //Elements To Load
            var FilesToLoad = [
                'coverage_rendering.js',
                'coverage_workportal.js',
                'e2e-rendering.js',
                'e2e-workportal.js',
                'jshint.js',
                'csslint.js',
                'unitRendering.js',
                'unitWorkportal.js'
            ];

            var pathToLoadFiles = '../testmobility/logs/mobility-report/jsonresult/' + device + '/' + dateToLoad + "/";

            for (it in FilesToLoad) {
                scriptToLoad.push(pathToLoadFiles + FilesToLoad[it]);
            }
        }

        //ready load all history js
        $(document).scriptLoader(scriptToLoad).done(function() {

            //si vienen con parametros es necesario ubicarlos en lasts date y previous date
            //example for parameters http://localhost:63342/jquery/test/logs/report-smartphone.html?from=8-12-2014&to=8-14-2014#
            if ($.urlParam('to') != '') {
                lastDate = $.urlParam('to').replace(new RegExp('-', 'g'), '');
            }
            if ($.urlParam('from') != '') {
                previousDate = $.urlParam('from').replace(new RegExp('-', 'g'), '');
            }

            addHandlers(lastDate, previousDate);
            loadDateInfo();
            loadGraphics();

            setTimeout(function() {
                $('#startLoading').hide();
            }, 1000);

        }).fail(function() {
            alert("Faltan archivos por ser generados");
            $('#startLoading').hide();
        });

    });

};

function loadGraphics() {

    var dates = [];
    var rowsDataCoverage = [];
    var rowsDataCoverageRendering = [];
    var rowsDataunit = [];
    var rowsDataunitRendering = [];
    var rowsDatae2e = [];
    var rowsDatahint = [];

    for (var iterator in historic) {
        var dateToLoad = historic[iterator];
        var loadData = dateToLoad.replace(new RegExp('-', 'g'), '');
        dates.push(dateToLoad);


        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var coverage = eval("coverageworkportal_" + loadData);
        rowsDataCoverage.push([dateToLoad, coverage.root.metrics.statements.pct, coverage.root.metrics.branches.pct, coverage.root.metrics.functions.pct]);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var coverageRendering = eval("coveragerendering_" + loadData);
        rowsDataCoverageRendering.push([dateToLoad, coverageRendering.root.metrics.statements.pct, coverageRendering.root.metrics.branches.pct, coverageRendering.root.metrics.functions.pct]);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var unit = eval("unitWorkportal_" + loadData);
        rowsDataunit.push([dateToLoad, unit.total, (unit.total) - (unit.success) /*, unit.netTime */ ]);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var unitRendering = eval("unitRendering_" + loadData);
        rowsDataunitRendering.push([dateToLoad, unitRendering.total, (unitRendering.total) - (unitRendering.success) /*, unit.netTime */ ]);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var e2e = eval("mobilityE2E_" + loadData);
        rowsDatae2e.push([dateToLoad, e2e.total, (e2e.total) - (e2e.success) /*, unit.netTime */ ]);
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var warningsJs = eval("jshint_" + loadData);
        var warningCSS = eval("csslint_" + loadData);
        rowsDatahint.push([dateToLoad, warningsJs.length, warningCSS.length]);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var optionsCoverage = {
        title: 'Covertura Workportal Historic Performance',
        hAxis: {
            title: 'Dates',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };

    var headerCoverage = ['Date', 'statements', 'branches', 'functions'];

    rowsDataCoverage.unshift(headerCoverage);

    var dataCoverage = google.visualization.arrayToDataTable(rowsDataCoverage);

    var chartCoverage = new google.visualization.AreaChart(document.getElementById('chart_div_coverage'));

    chartCoverage.draw(dataCoverage, optionsCoverage);


    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var optionsCoverageRendering = {
        title: 'Covertura Rendering Historic Performance',
        hAxis: {
            title: 'Dates',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };

    var headerCoverageRendering = ['Date', 'statements', 'branches', 'functions'];
    rowsDataCoverageRendering.unshift(headerCoverageRendering);

    var dataCoverageRendering = google.visualization.arrayToDataTable(rowsDataCoverageRendering);
    var chartCoverageRendering = new google.visualization.AreaChart(document.getElementById('chart_div_coverage_rendering'));
    chartCoverageRendering.draw(dataCoverageRendering, optionsCoverageRendering);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var optionsUnit = {
        title: 'Unit Historic Performance',
        hAxis: {
            title: 'Dates',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };

    var headerUnit = ['Date', 'Total', 'Error' /*,'Time'*/ ];
    rowsDataunit.unshift(headerUnit);

    var dataUnit = google.visualization.arrayToDataTable(rowsDataunit);

    var chartUnit = new google.visualization.AreaChart(document.getElementById('chart_div_unit'));
    chartUnit.draw(dataUnit, optionsUnit);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    var optionsUnitRendering = {
        title: 'Unit Historic Performance',
        hAxis: {
            title: 'Dates',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };

    var headerUnitRendering = ['Date', 'Total', 'Error' /*,'Time'*/ ];
    rowsDataunitRendering.unshift(headerUnitRendering);

    var dataUnitRendering = google.visualization.arrayToDataTable(rowsDataunitRendering);
    var chartUnitRendering = new google.visualization.AreaChart(document.getElementById('chart_div_unit_rendering'));
    chartUnitRendering.draw(dataUnitRendering, optionsUnitRendering);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var optionse2e = {
        title: 'E2E Historic Performance',
        hAxis: {
            title: 'Dates',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };

    var headere2e = ['Date', 'Total', 'Error' /*,'Time'*/ ];
    rowsDatae2e.unshift(headere2e);

    var datae2e = google.visualization.arrayToDataTable(rowsDatae2e);

    var charte2e = new google.visualization.AreaChart(document.getElementById('chart_div_e2e'));
    charte2e.draw(datae2e, optionse2e);

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    var optionsHint = {
        title: 'Hint Historic Performance(Come Down)',
        hAxis: {
            title: 'Dates',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };
    var headerHint = ['Date', '# Warnings on JS', '# Warnings on Css'];

    rowsDatahint.unshift(headerHint);
    var datahint = google.visualization.arrayToDataTable(rowsDatahint);

    var chartHint = new google.visualization.AreaChart(document.getElementById('chart_div_hint'));


    google.visualization.events.addListener(chartHint, 'ready', function() {

        $("#chart_div_coverage").appendTo(".coverage-workportal-chart");
        $("#chart_div_coverage_rendering").appendTo(".coverage-rendering-chart");
        $("#chart_div_unit").appendTo(".unit-chart");
        $("#chart_div_unit_rendering").appendTo(".unit-chart-rendering");
        $("#chart_div_e2e").appendTo(".e2e-chart");
        $("#chart_div_hint").appendTo(".hint-chart");

        $('#startLoading').hide();
    });

    chartHint.draw(datahint, optionsHint);
}


function addDateToCombosOptions(dateToAddInCombo) {

    var valueOnCombo = dateToAddInCombo.replace(new RegExp('-', 'g'), '');
    var option = '<option value="' + valueOnCombo + '">' + dateToAddInCombo + '</option>'; //javascript bugs on use this line :--->new Option(dateToAddInCombo, valueOnCombo);

    $('#baseDate').append(option);
    $('#sinceDate').append(option);

}



function addHandlers(lastDate, previousDate) {

    $('#baseDate').val(lastDate.replace(new RegExp('-', 'g'), ''));
    $('#sinceDate').val(previousDate.replace(new RegExp('-', 'g'), ''));

    $('#baseDate').change(function() {
        loadDateInfo();
    });
    $('#sinceDate').change(function() {
        loadDateInfo();
    });

}


function loadDateInfo() {

    //select the combos
    console.info("Seleccionar los ultimos valores")
    var baseDate = $('#baseDate').val();

    var coverage = eval("coverageworkportal_" + baseDate);
    var coverageRendering = eval("coveragerendering_" + baseDate);
    var unitSmartphone = eval("unitWorkportal_" + baseDate);
    var unitSmartphoneRendering = eval("unitRendering_" + baseDate);
    var mobilityE2E = eval("mobilityE2E_" + baseDate);
    var jshint = eval("jshint_" + baseDate);
    var csslint = eval("csslint_" + baseDate);

    var sinceDate = $('#sinceDate').val();

    if (sinceDate) {
        var coverageSince = eval("coverageworkportal_" + sinceDate);
        var coverageRenderingSince = eval("coveragerendering_" + sinceDate);
        var unitSmartphoneSince = eval("unitWorkportal_" + sinceDate);
        var unitSmartphoneRenderingSince = eval("unitRendering_" + sinceDate);
        var mobilityE2ESince = eval("mobilityE2E_" + sinceDate);
        var jshintSince = eval("jshint_" + sinceDate);
        var csslintSince = eval("csslint_" + sinceDate);
    }

    $("#coverage #statements-pct").html(coverage.root.metrics.statements.pct);
    $("#coverage #statements-covered").html(coverage.root.metrics.statements.covered);
    $("#coverage #statements-total").html(coverage.root.metrics.statements.total);

    $("#coverage #branches-pct").html(coverage.root.metrics.branches.pct);
    $("#coverage #branches-covered").html(coverage.root.metrics.branches.covered);
    $("#coverage #branches-total").html(coverage.root.metrics.branches.total);

    $("#coverage #functions-pct").html(coverage.root.metrics.functions.pct);
    $("#coverage #functions-covered").html(coverage.root.metrics.functions.covered);
    $("#coverage #functions-total").html(coverage.root.metrics.functions.total);

    //Coverage rendering
    $("#coverage #rendering-statements-pct").html(coverageRendering.root.metrics.statements.pct);
    $("#coverage #rendering-statements-covered").html(coverageRendering.root.metrics.statements.covered);
    $("#coverage #rendering-statements-total").html(coverageRendering.root.metrics.statements.total);

    $("#coverage #rendering-branches-pct").html(coverageRendering.root.metrics.branches.pct);
    $("#coverage #rendering-branches-covered").html(coverageRendering.root.metrics.branches.covered);
    $("#coverage #rendering-branches-total").html(coverageRendering.root.metrics.branches.total);

    $("#coverage #rendering-functions-pct").html(coverageRendering.root.metrics.functions.pct);
    $("#coverage #rendering-functions-covered").html(coverageRendering.root.metrics.functions.covered);
    $("#coverage #rendering-functions-total").html(coverageRendering.root.metrics.functions.total);

    //unit
    $("#unit #total-tests").html(unitSmartphone.total);
    $("#unit #total-errors").html((unitSmartphone.total) - (unitSmartphone.success));
    $("#unit #total-failures").html(unitSmartphone.failed);
    $("#unit #total-skipped").html(unitSmartphone.skipped);
    $("#unit #total-runtime").html(unitSmartphone.netTime);

    //unit rendering
    $("#unit #rendering-total-tests").html(unitSmartphoneRendering.total);
    $("#unit #rendering-total-errors").html((unitSmartphoneRendering.total) - (unitSmartphone.success));
    $("#unit #rendering-total-failures").html(unitSmartphoneRendering.failed);
    $("#unit #rendering-total-skipped").html(unitSmartphoneRendering.skipped);
    $("#unit #rendering-total-runtime").html(unitSmartphoneRendering.netTime);

    //e2e
    $("#e2e #total-tests").html(mobilityE2E.total)
    $("#e2e #total-errors").html((mobilityE2E.total) - (mobilityE2E.success))
    $("#e2e #total-failures").html(mobilityE2E.failed)
    $("#e2e #total-skipped").html(mobilityE2E.skipped)

    //hint
    $("#hint #warning").html(jshint.length)
    $("#hint #cssWarning").html(csslint.length)

    if (sinceDate) {
        statsCoverage(coverage, coverageSince);
        statsCoverageRendering(coverageRendering, coverageRenderingSince);
        statsUnitTest(unitSmartphone, unitSmartphoneSince);
        statsUnitTestRendering(unitSmartphoneRendering, unitSmartphoneRenderingSince);
        statsE2E(mobilityE2E, mobilityE2ESince);
        statsHint(jshint, jshintSince, csslint, csslintSince);
    }
}

function statsCoverage(coverage, coverageSince) {

    var statements = calculatePercent(coverage.root.metrics.statements.pct, coverageSince.root.metrics.statements.pct);
    var branches = calculatePercent(coverage.root.metrics.branches.pct, coverageSince.root.metrics.branches.pct);
    var functions = calculatePercent(coverage.root.metrics.functions.pct, coverageSince.root.metrics.functions.pct);

    changeStatusRow(statements, '.statements-pct-st', '.statements-pct-class');
    changeStatusRow(statements, '.branches-pct-st', '.branches-pct-class');
    changeStatusRow(statements, '.functions-pct-st', '.functions-pct-class');

}

function statsCoverageRendering(coverageRendering, coverageRenderingSince) {

    var statements = calculatePercent(coverageRendering.root.metrics.statements.pct, coverageRenderingSince.root.metrics.statements.pct);
    var branches = calculatePercent(coverageRendering.root.metrics.branches.pct, coverageRenderingSince.root.metrics.branches.pct);
    var functions = calculatePercent(coverageRendering.root.metrics.functions.pct, coverageRenderingSince.root.metrics.functions.pct);

    changeStatusRow(statements, '.rendering-statements-pct-st', '.rendering-statements-pct-class');
    changeStatusRow(statements, '.rendering-branches-pct-st', '.rendering-branches-pct-class');
    changeStatusRow(statements, '.rendering-functions-pct-st', '.rendering-functions-pct-class');
}

function statsUnitTest(unitSmartphone, unitSmartphoneSince) {
    var totalTest = calculatePercent(unitSmartphone.total, unitSmartphoneSince.total);
    changeStatusRow(totalTest, '.unit-total-test-st', '.unit-pct-class');
}

function statsUnitTestRendering(unitSmartphonerendering, unitSmartphoneRenderingSince) {
    var totalTest = calculatePercent(unitSmartphonerendering.total, unitSmartphoneRenderingSince.total);
    changeStatusRow(totalTest, '.rendering-unit-total-test-st', '.rendering-unit-pct-class');
}

function statsE2E(mobilityE2E, mobilityE2ESince) {
    var totalTest = calculatePercent(mobilityE2E.total, mobilityE2ESince.total);
    changeStatusRow(totalTest, '.e2e-total-test-st', '.e2e-pct-class');
}

function statsHint(jshint, jshintSince, csslint, csslintSince) {
    var totaljs = calculatePercent(jshint.length, jshintSince.length);
    var totalcss = calculatePercent(csslint.length, csslintSince.length);

    changeStatusRow(totaljs, '.jshint-st', '.jshint-pct-class');
    changeStatusRow(totalcss, '.csslint-st', '.csshint-pct-class');
}

function calculatePercent(number1, number2) {
    return (number1 - number2).toFixed(2);
}

function changeStatusRow(result, elementNumber, groupClass) {
    var arrow = $(groupClass).find(".image_arrow");
    arrow.removeClass();
    arrow.addClass('image_arrow');
    if (result < 0) {
        //negative
        arrow.addClass('down');
    }

    if (result == 0) {
        //neutral
        arrow.addClass('standings');
    }

    if (result > 0) {
        arrow.addClass('up');
    }

    $(elementNumber).html(result);
}

function changeStateMenu(thisEl) {
    $(thisEl).parent().parent().find(".menu_content").removeClass("selected");
    $(thisEl).parent().addClass("selected");
}

//Events
$("#btnHome").on('click', function() {
    window.location = "#";

    $("#details").hide();
    $("#dashboard").show();

    changeStateMenu(this);

});

$("#btnCoverage").on('click', function() {
    //window.location = "#coverage";
    getDevice();

    $("#pnlReport").attr("src", "resources/coverage/" + device + "/index.html?" + Number(new Date()))
    $("#details").show();
    $("#dashboard").hide();

    changeStateMenu(this);
});

$("#btnUnit").on('click', function() {
    // window.location = "#unit";
    getDevice();

    $("#pnlReport").attr("src", "resources/unit/" + device + "/index.html?" + Number(new Date()))
    $("#details").show();
    $("#dashboard").hide();

    changeStateMenu(this);
});

$("#btnE2e").on('click', function() {
    // window.location = "#e2e";
    getDevice();

    $("#pnlReport").attr("src", "resources/e2e/" + device + "/index.html?" + Number(new Date()))
    $("#details").show();
    $("#dashboard").hide();

    changeStateMenu(this);
});

$("#btnHint").on('click', function() {
    //window.location = "#hint";
    getDevice();

    $("#pnlReport").attr("src", "logs/static-code/" + device + "/hint.html?" + Number(new Date()))
    $("#details").show();
    $("#dashboard").hide();

    changeStateMenu(this);
});

$("#btnCss").on('click', function() {
    //window.location = "#css";
    getDevice();

    $("#pnlReport").attr("src", "logs/static-code/" + device + "/csslint.html?" + Number(new Date()))
    $("#details").show();
    $("#dashboard").hide();

    changeStateMenu(this);
});

$("#btnTodoE2E").on('click', function() {
    //window.location = "#todo";
    getDevice();

    $("#pnlReport").attr("src", "http://jira.bizagi.com:8080/si/jira.issueviews:issue-html/DRAGON-19981/DRAGON-19981.html" + Number(new Date()))
    $("#details").show();
    $("#dashboard").hide();

    changeStateMenu(this);
});


var hash = window.location.hash.replace("#", "");

switch (hash) {
    case "coverage":
        $("#btnCoverage").click();
        break;
    case "unit":
        $("#btnUnit").click();
        break;
    case "e2e":
        $("#btnE2e").click();
        break;
    case "hint":
        $("#btnHint").click();
        break;
    case "css":
        $("#btnCss").click();
        break;
    case "todo":
        $("#btnTodoE2E").click();
        break;
    default:
        $("#btnHome").click();
        break;
}