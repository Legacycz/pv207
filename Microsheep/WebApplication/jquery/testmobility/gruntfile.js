/**
 * Created by oscaro on 12/23/2014.
 */

module.exports = function(grunt) {
    grunt.config.init({

        nodestatic: {
            server: {
                options: {
                    port: 8001,
                    base: './../../',
                    verbose: true,
                    keepalive: true
                }
            }
        },
        "bgShell": {
            normalizeDefinitionSmartphone: {
                cmd: 'node scripts/normalize-definitions.js device=smartphone_ios modules=workportalflat',
                bg: false
            },
            normalizeDefinitionTablet: {
                cmd: 'node scripts/normalize-definitions.js device=tablet_ios modules=workportalflat',
                bg: false
            },
            runWebServerBg: {
                cmd: 'node scripts/web-server.js -p 8001 -chroot ../../',
                bg: true
            },

            runE2EWorkportalSmartphoneIos: {
                cmd: "se-interpreter ./config/se-interpreter/se-interpreter-workportal-smartphoneios.conf.json --parallel=1 --environment=workportal --device=smartphone --listener=" + __dirname + "\\scripts\\reporter\\e2e-listener.js",
                bg: false
            },

            runE2ERenderSmartphoneIos: {
                cmd: "se-interpreter ./config/se-interpreter/se-interpreter-render-smartphoneios.conf.json --parallel=1 --environment=rendering --device=smartphone --listener=" + __dirname + "\\scripts\\reporter\\e2e-listener.js",
                bg: false
            },

            "runE2EWorkportalTabletIos": {
                cmd: "se-interpreter ./config/se-interpreter/se-interpreter-workportal-tabletios.conf.json --parallel=1 --environment=workportal --device=tablet --listener=" + __dirname + "\\scripts\\reporter\\e2e-listener.js",
                bg: false
            },
            "runE2ERenderTabletIos": {
                cmd: "se-interpreter ./config/se-interpreter/se-interpreter-render-tabletios.conf.json --parallel=1 --environment=rendering --device=tablet --listener=" + __dirname + "\\scripts\\reporter\\e2e-listener.js",
                bg: false
            },

            "runSeleniumHub": {
                cmd: "java -jar scripts/selenium/selenium-server-standalone-2.43.1.jar -role hub -userExtensions scripts/user-extensions.js",
                bg: true
            },
            "runSeleniumNode": {
                cmd: "java -jar scripts/selenium/selenium-server-standalone-2.43.1.jar -role node -hub http://localhost:4444/grid/register -log logs/selenium/selenium.log",
                bg: true
            },


            "sleep10": {
                cmd: 'node -e "setTimeout(new Function(), 10000)"',
                bg: false
            }
        },
        "karma": {
            unitSmartphoneRendering: {
                configFile: 'config/karma/smartphone.rendering.conf.js'
            },
            unitSmartphoneWorkportal: {
                configFile: 'config/karma/smartphone.workportal.conf.js'
            },
            unitTabletRendering: {
                configFile: 'config/karma/tablet.rendering.conf.js'
            },
            unitTabletWorkportal: {
                configFile: 'config/karma/tablet.workportal.conf.js'
            }
        },
        "start-selenium-server": {
            startSeleniumHub: {
                options: {
                    downloadUrl: ['http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar'],
                    downloadLocation: 'scripts/selenium',
                    serverOptions: {
                        role: "hub"
                    }
                }
            },
            startSeleniumHubMac: {
                options: {
                    downloadUrl: ['http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar'],
                    downloadLocation: 'scripts/selenium',
                    serverOptions: {
                        role: "hub",
                        port: "4445"
                    }
                }
            },
            startSeleniumNode: {
                options: {
                    downloadUrl: [
                        'http://chromedriver.storage.googleapis.com/2.8/chromedriver_win32.zip',
                        'http://selenium.googlecode.com/files/IEDriverServer_x64_2.39.0.zip',
                        'http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar'
                    ],
                    downloadLocation: 'scripts/selenium',
                    serverOptions: {
                        role: "node",
                        hub: "http://localhost:4444/grid/register",
                        "browserSessionReuse": "",
                        "Dwebdriver.ie.driver=scripts/selenium/IEDriverServer.exe": "", // Java argument
                        "Dwebdriver.chrome.driver=scripts/selenium/chromedriver.exe": "" // Java argument
                    }
                }
            },
            startSeleniumNodeMac: {
                options: {
                    downloadUrl: [
                        'http://chromedriver.storage.googleapis.com/2.8/chromedriver_mac32.zip',
                        'http://selenium.googlecode.com/files/IEDriverServer_x64_2.39.0.zip',
                        'http://selenium-release.storage.googleapis.com/2.43/selenium-server-standalone-2.43.1.jar'
                    ],
                    downloadLocation: 'scripts/selenium',
                    serverOptions: {
                        role: "node",
                        hub: "http://localhost:4445/grid/register",
                        "browserSessionReuse": "",
                        "Dwebdriver.ie.driver=scripts/selenium/IEDriverServer.exe": "", // Java argument
                        "Dwebdriver.chrome.driver=scripts/selenium/chromedriver": "" // Java argument
                    }
                }
            }
        },

        csslint: {
            strict: {
                options: {
                    import: false,
                    'adjoining-classes': false,
                    'box-sizing': false,
                    'box-model': false,
                    formatters: [
                        {id: 'csslint-xml', dest: 'logs/static-code/smartphone/csslint'}
                    ]
                },
                src: ['../workportalflat/*.css',
                    '../workportalflat/**/*.css']
            },
            mobilitySmartphone: {
                options: {
                    import: false,
                    'adjoining-classes': false,
                    'box-sizing': false,
                    'box-model': false,
                    formatters: [
                        {id: 'csslint-xml', dest: 'logs/static-code/smartphone/csslint'}
                    ]
                },
                src: [
                    '../workportalflat/smartphone/**/**/*.css',
                    '../workportalflat/webparts/common/**/*.css',
                    '../workportalflat/webparts/smartphone/**/*.css'
                ]
            },
            mobilityTablet: {
                options: {
                    import: false,
                    'adjoining-classes': false,
                    'box-sizing': false,
                    'box-model': false,
                    formatters: [
                        {id: 'csslint-xml', dest: 'logs/static-code/tablet/csslint'}
                    ]
                },
                src: [
                    '../workportalflat/tablet/**/**/*.css',
                    '../workportalflat/webparts/common/**/*.css',
                    '../workportalflat/webparts/tablet/**/*.css'
                ]
            }

        },

        jshint: {
            default: {
                options: {
                    jshintrc: ".jshintrc",
                    force: true,
                    ignores: [
                        '../automatictesting/*',
                        '../production/**',
                        '../test/**',
                        '../common/jquery.ui/*',
                        '../*.min.js',
                        '../**/*.min.js',
                        '../jquery.*.js',
                        '../**/jquery.*.js',
                        '../**/jquery_*.js',
                        '../reporting/plugins/**/*.js',
                        '../workportal/test/**/*.js',
                        '../rendering/test/**/*.js'
                    ],
                    reporter: './scripts/reporter/jshint.js'
                },
                files: {
                    src: ['../*.js', '../**/*.js']
                }
            },
            jenkins: {
                options: {
                    jshintrc: ".jshintrc",
                    force: true,
                    ignores: [
                        '../automatictesting/*',
                        '../production/**',
                        '../test/**',
                        '../common/jquery.ui/*',
                        '../*.min.js',
                        '../**/*.min.js',
                        '../jquery.*.js',
                        '../**/jquery.*.js',
                        '../**/jquery_*.js',
                        '../reporting/plugins/**/*.js',
                        '../workportal/test/**/*.js',
                        '../rendering/test/**/*.js',
                        '../renderingflat/**/*.js'
                    ],
                    reporter: require('jshint-jenkins-checkstyle-reporter'),
                    reporterOutput: './logs/report-jshint-checkstyle.xml'
                },
                files: {
                    src: ['../*.js', '../**/*.js']
                }
            },
            mobilitySmartphone: {
                options: {
                    jshintrc: true,
                    force: true,
                    ignores: [],
                    reporter: './scripts/reporter/jshint-html.js'
                },
                files: {
                    src: [
                        '../workportalflat/common/**/**/*.js',
                        '../workportalflat/webparts/smartphone/**/*.js'
                    ]
                }
            },
            mobilityTablet: {
                options: {
                    jshintrc: true,
                    force: true,
                    ignores: [],
                    reporter: './scripts/reporter/jshint-html.js'
                },
                files: {
                    src: [
                        '../workportalflat/common/**/**/*.js',
                        '../workportalflat/webparts/tablet/**/*.js'
                    ]
                }
            }
        }

    });

    grunt.loadNpmTasks('grunt-bg-shell');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-nodestatic');
    grunt.loadNpmTasks('grunt-selenium-server');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-csslint');

    grunt.registerTask('set_global', 'Set a global variable.', function (name, value) {
        global[name] = value;
    });

    grunt.registerTask('set_config', 'Set a config property.', function (name, val) {
        grunt.config.set(name, val);
    });

    //1. configure server
    //2.start server
    //3.Run unit test
    //3.1.smartphone
    //3.2 tablet
    //4.Run E2E test
    //4.1 E2E test Smartphone
    //4.2 E2E test Tablet
    //5.Run css hint
    //6.Run js hint
    //7.Run performance test

    // Default task.

    //1. configure server
    //change the port setting
    grunt.registerTask("configureServer", []);
    //2.start server
    //other options for this: npminstall grunt-concurrent
    grunt.registerTask("run", ["nodestatic:server"]);
    //3.Run unit test
    //3.1.smartphone
    grunt.registerTask('unitSmartphoneWorkportal', ['bgShell:normalizeDefinitionSmartphone', 'karma:unitSmartphoneWorkportal']);
    grunt.registerTask('unitSmartphoneRendering', ['bgShell:normalizeDefinitionSmartphone', 'karma:unitSmartphoneRendering']);
    grunt.registerTask('unitSmartphone', ['unitSmartphoneWorkportal', 'unitSmartphoneRendering']); //corre por aparte las de rendering y workportal
    //3.2 tablet
    grunt.registerTask('unitTabletWorkportal', ['bgShell:normalizeDefinitionTablet', 'karma:unitTabletWorkportal']);
    grunt.registerTask('unitTabletRendering', ['bgShell:normalizeDefinitionTablet', 'karma:unitTabletRendering']);
    grunt.registerTask('unitTablet', ['unitTabletWorkportal', 'unitTabletRendering']);


    grunt.registerTask('initSeleniumServer', ['start-selenium-server:startSeleniumHub', 'bgShell:sleep10', 'start-selenium-server:startSeleniumNode', 'bgShell:sleep10']);
    //grunt.registerTask('initSeleniumServer', ['bgShell:runSeleniumHub', 'bgShell:sleep15', 'bgShell:runSeleniumNode', 'bgShell:sleep10']);

    //4.1 E2E test Smartphone
    grunt.registerTask('e2e-smartphoneWorkportal', ['initSeleniumServer', 'bgShell:runE2EWorkportalSmartphoneIos']);
    grunt.registerTask('e2e-smartphoneRendering', ['initSeleniumServer', 'bgShell:runE2ERenderSmartphoneIos']);
    grunt.registerTask('e2e-smartphone', ['e2e-smartphoneWorkportal', 'e2e-smartphoneRendering']);

    //4.2 E2E test Tablet
    grunt.registerTask('e2e-tabletWorkportal', ['initSeleniumServer', 'bgShell:runE2EWorkportalTabletIos']);
    grunt.registerTask('e2e-tabletRendering', ['initSeleniumServer', 'bgShell:runE2ERenderTabletIos']);
    grunt.registerTask('e2e-tablet', ['e2e-tabletWorkportal', 'e2e-tabletRendering']);

    //5.Run css lint
    // set the grunt force option
    //grunt.option("force", true);
    grunt.registerTask('csslint-smartphone', ['set_global:device:smartphone', 'csslint:mobilitySmartphone']);
    grunt.registerTask('csslint-tablet', ['set_global:device:tablet', 'csslint:mobilityTablet']);

    //6.Run js hint
    // grunt set_global:base_path:D?\Reportes mobility-hint-smartphone [ In base_path for directory, change ":" for "?" ]
    grunt.registerTask('jshint-smartphone', ['set_global:device:smartphone', 'jshint:mobilitySmartphone']);
    grunt.registerTask('jshint-tablet', ['set_global:device:tablet', 'set_global:local_path:false', 'jshint:mobilityTablet']);

    //7.Run performance test
    grunt.registerTask('run-test', ['unitSmartphone', 'unitTablet', 'e2e-smartphone', 'e2e-tablet', 'csslint-smartphone', 'csslint-tablet', 'jshint-smartphone', 'jshint-tablet']);

};
