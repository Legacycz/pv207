﻿/**
 * Created by RicardoPD on 4/23/2014.
 */
// Karma configuration
// Generated on Wed Apr 23 2014 09:55:25 GMT-0500 (SA Pacific Standard Time)

var day = new Date();
var today = day.getFullYear() + "-" + (day.getMonth() + 1) + "-" + day.getDate();
var modules = require("../globalConfig");

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../../../',

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['junit', 'progress', 'coverage', 'html'],

        // web server port
        port: 9876,

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: (function () {
            var renderingFiles = [];
            var parsedDefinition = require('./../bizagi.module.definition.js');

            //Adds this item to the beginning of the array
            renderingFiles.push(
                { pattern: "workportalflat/testsuite/smartphone/unit/unit.config.js", included: true, served: true });

            // Merge definition
            for(var i in parsedDefinition){
                renderingFiles.push(parsedDefinition[i]);
            }

            renderingFiles.push(
                { pattern: "bizagi.configuration.js", included: true, served: true },
                { pattern: "renderingflat/css/**/*.*", included: false, served: true },
                { pattern: "renderingflat/tmpl/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/*.*", included: false, served: true },
                { pattern: "renderingflat/js/base/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/common/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/desktop/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/plugins/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/services/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/smartphone/**/*.*", included: false, served: true },
                { pattern: "renderingflat/js/tablet/**/*.*", included: false, served: true },
                { pattern: "common/**/*.*", included: false, served: true },
                { pattern: "workportalflat/testsuite/smartphone/unit/unit.bizagi.rendering.init.js", included: true, served: true }
            );

            renderingFiles.push({ pattern: "renderingflat/js/test/smartphone/unit/render/*", included: true, served: true });

            return renderingFiles;
        })(),


        // list of files to exclude
        exclude:[

        ],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        urlRoot: '/',

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,


        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,


        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['PhantomJS'],

        preprocessors: {
            './renderingflat/**/*.js': 'coverage'
        },

        coverageReporter: {
            dir: "./testmobility/logs/coverage/",
            reporters: [
                {
                    type: 'html',
                    subdir: 'smartphone/rendering/report'
                },
                {
                    type: 'json-summary',
                    subdir: 'smartphone/rendering/report'
                }
            ]
        },

        htmlReporter: {
            outputFile: './testmobility/logs/unit/smartphone/rendering/unitRendering.html',
            title: 'Unit Test Report For Rendering',
            device: 'smartphone'
        },


        junitReporter: {
            outputFile: './testmobility/logs/unit/smartphone/rendering/test-results.xml',
            suite: 'junit'
        },

        plugins: [
            'karma-junit-reporter',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-requirejs',
            'karma-coverage',
            'karma-htmlfile-reporter'
        ],

        proxies: {
            "/jquery/": "http://localhost:8001/" + modules.bizagiConfig.branch + "/jquery/"
        },

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true
    });
};
