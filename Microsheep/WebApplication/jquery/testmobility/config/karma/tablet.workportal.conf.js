﻿/**
* Created by oscaro TabletFlat
*/
// Karma configuration
// Generated on Wed Apr 23 2014 09:55:25 GMT-0500 (SA Pacific Standard Time)

var day = new Date();
var today = day.getFullYear() + "-" + (day.getMonth() + 1) + "-" + day.getDate();

module.exports = function (config) {
    config.set({

        // base path that will be used to resolve all patterns (eg. files, exclude)
        basePath: '../../../',

        // test results reporter to use
        // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
        reporters: ['junit', 'progress', 'coverage', 'html'],

        // web server port
        port: 9876,

        // frameworks to use
        // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: (function () {
            var workportalFiles = [];
            var parsedDefinition = require('../bizagi.module.definition.js');

            //Adds this item to the beginning of the array
            workportalFiles.push({
                pattern: "workportalflat/testsuite/tablet/unit/unit.config.js",
                included: true,
                served: true
            });

            // Merge definition
            for (var i in parsedDefinition) {
                workportalFiles.push(parsedDefinition[i]);
            }

            workportalFiles.push({pattern: "bizagi.configuration.js", included: true, served: true},
                {
                    pattern: "workportalflat/testsuite/tablet/unit/unit.bizagi.workportal.init.js",
                    included: true,
                    served: true
                }
            );

            //workportalFiles.push({pattern: "workportalflat/webparts/tablet/**/test/unit/**/*", included: true, served: true});
            workportalFiles.push({pattern: "workportalflat/common/services/test/unit/*", included: true, served: true});

            return workportalFiles;
        })(),

        // list of files to exclude
        exclude: [],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        urlRoot: '/',

        // level of logging
        // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // start these browsers
        // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
        browsers: ['Chrome'],//se cambió a chrome dado que phantomJS es viejo y tiene problemas con funciones como bind

        preprocessors: {
            './workportalflat/**/bizagi.*.js': 'coverage'
        },

        coverageReporter: {
            reporters: [
                {
                    type: 'lcov',
                    dir: './testmobility/logs/coverage/tablet/workportal/',
                    subdir: 'report',
                    options: {name: 'workportal', device: 'tablet'}
                }
            ]
        },

        htmlReporter: {
            outputFile: './testmobility/logs/unit/tablet/workportal/unitWorkportal.html',
            title: 'Unit Test Report For Workportal',
            device: 'tablet'
        },

        junitReporter: {
            outputFile: './testmobility/logs/unit/tablet/workportal/test-results.xml',
            suite: 'junit'
        },

        plugins: [
            'karma-junit-reporter',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-jasmine',
            'karma-chrome-launcher',
            'karma-requirejs',
            'karma-coverage',
            'karma-htmlfile-reporter'
        ],

        proxies: {
            //"/jquery/": "http://localhost:8001/" + modules.bizagiConfig.branch + "/jquery/"
            "/jquery/": "http://localhost:8001/jquery/"
        },

        // Continuous Integration mode
        // if true, Karma captures browsers, runs the tests and exits
        singleRun: true
    });
};
