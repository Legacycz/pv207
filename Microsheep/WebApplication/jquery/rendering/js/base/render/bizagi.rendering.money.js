﻿/*
*   Name: BizAgi Render Money Class
*   Author: Diego Parra
*   Comments:
*   -   This script will define basic stuff for money numbers
*/

bizagi.rendering.number.extend("bizagi.rendering.money", {}, {

    /*
    *   Update or init the element data
    */
    initializeData: function (data) {
        var self = this;
        // Call base
        this._super(data);

        // Fill default properties
        var properties = this.properties;
        var format = this.getResource("numericFormat");

        // Configures symbol
        properties.showsymbol = typeof (properties.showsymbol) !== "undefined" ? properties.showsymbol : true;

        if (properties.showsymbol) {
            properties.symbol = properties.currencySymbol ? properties.currencySymbol :
    		                (typeof (BIZAGI_DEFAULT_CURRENCY_INFO) !== "undefined" ? BIZAGI_DEFAULT_CURRENCY_INFO.symbol : format.symbol);
        }
        else {
            properties.symbol = "";
        }
        properties.groupDigits = true;
    },
        
    /*
    *   Returns the display value of the render
    */
    getDisplayValue: function () {
        var self = this,
                properties = self.properties,
                control = self.getControl(),
                value = self.getValue() || $("input", control).val() || 0;

        // Workaround to get the display value by the format currency plugin, because it requires a control
        var label = $('<label/>').html(value);
        label.formatCurrency(
                {
                    symbol: properties.symbol,
                    positiveFormat: properties.positiveFormat,
                    negativeFormat: properties.negativeFormat,
                    decimalSymbol: properties.decimalSymbol,
                    digitGroupSymbol: properties.digitGroupSymbol,
                    groupDigits: properties.groupDigits,
                    roundToDecimalPlace: properties.numDecimals,
                    colorize: properties.colorize
                });

        return label.text();
    }

});
