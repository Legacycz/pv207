﻿/*
*   Name: BizAgi Render Combo Column Decorator Class
*   Author: Diego Parra
*   Comments:
*   -   This script will define basic stuff for Combo Columns
*/

bizagi.rendering.columns.column.extend("bizagi.rendering.columns.search", {}, {

	/*
	*   Constructor
	*/
	init: function (params) {
		// Call base
		this._super(params);

		// Set base column styles
		var properties = this.properties;
	},

	/*
	*   Returns the in-memory processed element 
	*   so the caller could append it to any place
	*/
	render: function (surrogateKey, value, tableCell) {
		var self = this;
		var decorated = self.getDecorated(surrogateKey);
		var properties = self.properties;

		// If the control needs to recalculate data, reset it on the decorated render
		if (properties.recalculate) {
			decorated.resetData();
		}

		// Change the xpath context
		var xpathContext = properties.xpathContext.length > 0 ? properties.xpathContext + "." + self.grid.properties.xpath + "[id=" + surrogateKey + "]" : self.grid.properties.xpath + "[id=" + surrogateKey + "]";
        decorated.properties.xpathContext = xpathContext;

		// Render the control
		var result = this._super(surrogateKey, value, tableCell);

		return result;
	},

	/*
    *   Gets the internal value
    */
    getValue: function (surrogateKey) {
        var self = this;
    	var decorated = self.getDecorated(surrogateKey);
        var value = decorated.getValue();
        if (value && typeof(value) == "object" ) return value.id;
        	
    	return value;
    }

});
