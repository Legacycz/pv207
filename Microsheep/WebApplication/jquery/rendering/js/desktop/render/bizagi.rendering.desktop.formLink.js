/*
 *   Name: BizAgi Desktop Render Form Link Extension
 *   Author: Diego Parra
 *   Comments:
 *   -   This script will redefine the link render class to adjust to desktop devices
 */

// Extends itself
bizagi.rendering.formLink.extend("bizagi.rendering.formLink", {}, {
    /*
    *   Template method to implement in each device to customize the render's behaviour to add handlers
    */
    configureHandlers: function () {
        var self = this,
            control = self.getControl(),
            link = $(".ui-bizagi-render-link", control),
            properties = self.properties;

        // Call base
        self._super();

        // Define dialog
        self.dialog = null;

        // Bind click event
        link.click(function () {
            // Open the link inside a dialog
            (properties.hasentryrule) ? self.openLinkRule("entryrule") : self.openLink();
        });
    },

    openLinkRule: function (property) {
        var self = this;
        var def = new $.Deferred();

        // save form data
        var form = self.getFormContainer();
        self.getPropertyValue(property, {}).done(function (dataRuleResp) {
            if (dataRuleResp.messages != null) {
                var message = dataRuleResp.messages[0];
                var form = self.getFormContainer();
                form.validationController.showErrorMessage(message);
            } else {
                if (property == "entryrule") self.openLink();
            }
            def.resolve();
        });

        return def.promise();
    },

    getPropertyValue: function (propertyName, params) {
        var self = this;
        var properties = self.properties;
        var defer = new $.Deferred();
        self.dataService.multiaction().getPropertyData({
            url: properties.dataUrl,
            xpath: properties.xpath,
            idRender: properties.id,
            xpathContext: properties.xpathContext,
            idPageCache: properties.idPageCache,
            property: propertyName,
            extra: params,
            dataType: params ? params.dataType : null
        }).done(function (data) {
            if (data.type == "error") {
                // Workflow engine error, we gotta show it with error
                self.getFormContainer().addErrorMessage(data.message);
                defer.reject(data);
            } else {
                defer.resolve(data);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            defer.reject(jqXHR, textStatus, errorThrown);
        });

        return defer.promise();
    },

    /*
    *   Method to render non editable values
    */
    postRenderReadOnly: function () {
        var self = this,
            mode = self.getMode();

        if (mode == "execution") {
            self.configureHandlers();
        }
    },
    /*
    *   Opens the link display form inside a dialog
    */
    openLink: function () {
        var self = this, data, options;
        var properties = self.properties;
        if (typeof (properties.canBeSaved) == "undefined") properties.canBeSaved = true;
        var form = self.getFormContainer();

        $.when(self.getFormContainer().properties.editable ? form.submitData() : undefined).done(function () {
                // Send edit request
                $.when(self.submitEditRequest()).done(function () {
                    // Show dialog with new form after that
                    self.dialog = new bizagi.rendering.dialog.form(self.dataService, self.renderFactory, {
                        showSaveButton: properties.canBeSaved,
                        maximized: properties.maximized,
                        title: properties.displayName,
                        onSave: function (data) {
                            var def = new $.Deferred();

                            self.closeExitRule().done(function () {
                                $.when(self.submitSaveRequest(data)).done(function (result) {
                                    def.resolve(result);
                                });
                            });

                            return def.promise();
                        },
                        onCancel: function () {
                            return self.submitRollbackRequest();
                        },
                        close: function () {
                            //self.closeExitRule();
                        }
                    });

                    self.renderFormLinkDialog();
                });
            });    
        

        
    },

    closeExitRule: function () {
        var self = this;
        var properties = self.properties;
        var def = new $.Deferred();

        if (properties.hasexitrule) {
            $.when(self.openLinkRule("exitrule")).done(function (result) {
                def.resolve(result);
            });
        } else {
            def.resolve();
        }

        return def.promise();
    },

    /*
    *   Render Dialog
    */
    renderFormLinkDialog: function () {

        var self = this;
        var properties = self.properties;

        self.dialog.render({
            idRender: properties.id,
            xpathContext: self.getFormLinkXpathContext(),
            idPageCache: properties.idPageCache,
            recordXPath: self.getFormLinkXpath(),
            requestedForm: "linkform",
            editable: properties.editable,
            url: properties.editPage
        }).fail(function () {
            // Sends a rollback request to delete checkpoints
            self.submitRollbackRequest();
        });
    },

    getFormLinkXpathContext: function () {
        var self = this;
        var xpathContext = self.properties.xpathContext;
        var parentXpathContext = self.getXpathContext();
        if (parentXpathContext && xpathContext) {
            if (xpathContext.indexOf(parentXpathContext) === 0) {
                return xpathContext;
            } else {
                return parentXpathContext + "." + xpathContext;
            }
        } else {
            return xpathContext;
        }
    }
});
