﻿/*
*   Name: BizAgi Desktop Form Extension
*   Author: Diego Parra
*   Comments:
*   -   This script will redefine the container class to adjust to desktop devices
*   -   Will apply a desktop form template
*/

// Auto extend
bizagi.rendering.form.extend("bizagi.rendering.form", {

    /* 
    *   Template method to implement in each device to customize each container after processed
    */
    postRenderContainer: function (container) {
        var self = this;
        var buttons = self.getButtons();

        // Call base
        self._super(container);

        // Style buttons
        buttons.button();

        //Set button length
        var lengthButtons = (buttons) ? buttons.length : 0;

        if (lengthButtons && self.params.type === "form" && self.params.isEntityForm !== true) {

            $(document).data('auto-save', 'auto-save');

            //bind event auto-save
            self.autoSaveFunction = function (e, deferredSave) {
                self.autoSaveEvents(deferredSave);
            };

            //Keeps the reference of the function required for handling autoSave
            if (!self.saveFormDelegate)
                self.saveFormDelegate = $.proxy(self.autoSaveFunction, self);
            else
                $(document).unbind("save-form", self.saveFormDelegate);

            $(document).bind("save-form", self.saveFormDelegate);

            //bind event beforeunload
            self.beforeUnloadFunction = function (e) {

                var newData = {};
                self.collectRenderValues(newData);
                //if there are changes in the form show a message
                if (!$.isEmptyObject(newData) && $(document).data('auto-save')) {
                    return bizagi.localization.getResource("confirmation-savebox-message2");
                }

                return;
            };
            $(window).unbind('beforeunload').bind('beforeunload', self.beforeUnloadFunction);
        } else {
            $(document).bind("save-form", function (e, deferredSave) {
                // the event must always be present QAF-416
                deferredSave.resolve();
            });
        }

        self.buildProcessPath();

    },
    /*
    * Auto Save Events
    */
    autoSaveEvents: function (deferredSave, saveBox) {

        var self = this
        var data = {};
        var changes = [];
        var form = self.getFormContainer();
        form.hasChanged(changes);

        /* 
        *saving changes to validate or not the contents into a table
        */
        if (changes.length > 0) {

            $.when(bizagi.showSaveBox(bizagi.localization.getResource("confirmation-savebox-message1"), "Bizagi", "warning")).done(function () {
                self.collectRenderValues(data);
                $.when(self.saveForm(data)).done(function () {
                    deferredSave.resolve();
                });
            }).fail(function (params) {
                if (params == "cancel") {
                    deferredSave.reject();
                } else {
                    deferredSave.resolve();

                    //unbinding the save-form function in case if is still bound
                    if (self.saveFormDelegate)
                        $(document).unbind("save-form", self.saveFormDelegate);
                }
            });
        } else {
            //unbinding the save-form function in case if is still bound
            if (self.saveFormDelegate)
                $(document).unbind("save-form", self.saveFormDelegate);

            deferredSave.resolve();
        }
    },

    /*
    *   Template method to get the buttons objects 
    */
    getButtons: function () {
        var self = this;
        var container = self.container;

        return $(".ui-bizagi-button-container .ui-bizagi-button", container);
    },

    /*
    *   Template method to implement in each device to customize the container's behaviour to show layout
    */
    configureLayoutView: function () {
        // Do nothing
    },

    /*
    *   Dispose the class so we can detect when a class is invalid already
    */
    dispose: function () {
        var self = this;

        // Remove  the document handlers for auto-save
        $(document).unbind("save-form", self.autoSaveFunction);
        $(window).unbind('beforeunload', self.beforeUnloadFunction);

        //delete self.autoSaveFunction;
        //delete self.beforeUnloadFunction;

        // Call base
        self._super();
    },
    
     /*
    *   Submits all the form to the server and returns a deferred to check when the process finishes
    */
    saveForm: function (data) {
        var self = this;

        // Collect data
        data = self.collectRenderValuesForSubmit(data);

        // Check if there are deferreds to wait
        var deferredsToWait = null;
        if (data.deferreds) {
            deferredsToWait = $.when.apply($, data.deferreds);
            delete data.deferreds;
        }

        // Wait for deferreds
        return $.when(deferredsToWait).pipe(function () {
            self.startLoading();

            // Submit the form
            return self.dataService.submitData({
                action: "SAVE",
                data: data,
                xpathContext: self.properties.xpathContext,
                idPageCache: self.properties.idPageCache,
                isOfflineForm: (typeof bizagi.context.isOfflineForm !== "undefined" ? bizagi.context.isOfflineForm : false)
            }).pipe(function () {
                self.endLoading();
            });
        });
    },

    /*
    * Gets changes of current navigation form
    */
    getChanges: function () {
        var self = this;
        var data = {};

        if (typeof (self.collectRenderValues) != "undefined") {
            self.collectRenderValues(data);
        }

        return data;
    },
    /*
     *build path process
     */
    buildProcessPath: function (container) {
        var self = this;
        var properties = self.properties;
        var mode = self.getMode();
        var queryForm = self.getFormType() == "queryForm";
        var summaryForm = self.params.summaryForm || false;
        var globalForm = self.properties.displayAsReadOnly || false;
        if (mode == "execution" && !queryForm && !summaryForm && !globalForm && self.properties.processPath && self.properties.processPath.length > 0) {
            var template = self.renderFactory.getTemplate("form-process-path");
            var html = $.fasttmpl(template, {processPath: properties.processPath});
            self.pathProcess = html;
        } else {
            self.pathProcess = "";
        }
    }
});
