/**
* Created by RicardoPD on 3/4/2015.
*/

(function($) {
    $.fn.actionSheet = function(options) {
        options = options || {};
        var self = this;
        self.guid = (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
        self.template = '<ul id="actionsheet-#=data.id#" class="bz-actionsheet">' +
            '#for(var i = 0; i < data.actions.length; i++){#' +
            '<li class="bz-render-as-option #if(i === data.actions.length-1){#bz-render-last-child#}#"><a ' +
            '#if(data.actions[i].guid !== "back"){#data-bz-ordinal="#= data.actions[i].guid #"' +
            '#}else{#data-bz-ordinal="-1"#' +
            '}#>#= data.actions[i].displayName #' +
            '</a></li>' +
            '#}#' +
            '</ul>';

        var defaults = {
            setDataToShow: function() {
            },
            actionClicked: function() {
            },
            actions: []
        };

        var settings = $.extend(defaults, options);

        // Private functions

        function S4() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        }

        var methods = {
            "init": function() {
                methods.bindElements();
            },
            "bindElements": function() {
                self.on("click", methods.showActionSheet);
            },
            "showActionSheet": function() {
                $.when(settings.setDataToShow()).done(function(actions) {
                    actions = typeof actions !== 'undefined' ? actions : settings.actions;
                    if (actions.length > 0) {
                        $.each($('.bz-actionsheet', 'body'), function(i, e) {
                            $(e).data("kendoMobileActionSheet").destroy();
                        });

                        var actionSheetTmpl = kendo.template(self.template, { useWithBlock: false });
                        $('body').append(actionSheetTmpl({ id: self.guid, actions: actions }));
                        var actionsheet = $('#actionsheet-' + self.guid).kendoMobileActionSheet({
                            type: "phone",
                            'destroy': function() {
                                this.element.remove();
                            }
                        });
                        $("a[data-bz-ordinal]", actionsheet).kendoMobileButton({
                            click: function(e) {
                                var action = actions.find(function(el) { return el.guid == e.button.data("bz-ordinal") });
                                settings.actionClicked(action);
                            }
                        });

                        $(actionsheet).data("kendoMobileActionSheet").open();
                    }
                });
            }
        };

        methods.init();

        return methods;
    };
})(jQuery);
