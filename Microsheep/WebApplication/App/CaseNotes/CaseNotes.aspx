<%@ Page language="c#" Codebehind="CaseNotes.aspx.cs" AutoEventWireup="false" Inherits="BizAgiBPM.App.CaseNotes.CaseNotes" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>CaseNotes</title> 
		<!--#include file="../../include/BizAgiMeta.inc"-->
		<link rel="stylesheet" href="../../css/estilos.css" type="text/css">
		<LINK href="../../css/calendar.css" type="text/css" rel="stylesheet">
		<%WriteHead();%>
		<script language="JavaScript" src="../../js/implementation.js"></script>
		
		<script language="JavaScript" src="../../js/WorkPortal/BAWindows/prototype.js"></script>
		<script language="JavaScript" src="../../js/WorkPortal/BAWindows/window.js"></script>
		<script language="JavaScript" src="../../js/WorkPortal/BAWindows/BAWindow.js"></script>
		<link href="../../css/WorkPortal/BAWindow.css" type="text/css" rel="stylesheet">
		
	</HEAD>
	<body onclick="BAonclick(event)" onload="BAonloadNoWizard()">
		<div id="CaseNotesMainWindow" style="visibility:visible;display:block;">
			<DIV class="text" id="popupcalendar"></DIV>
			<table width="100%" border="0" cellspacing="2" cellpadding="2">
				<tr>
					<td align="center">
						<%((BizAgi.Defs.IMagicForm)GetMagicFormInstance()).ProcessForm();%>
					</td>
				</tr>
			</table>
		</div>
	</body>
</HTML>
