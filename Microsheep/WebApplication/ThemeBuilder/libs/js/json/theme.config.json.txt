{
    "theme": {
        "title": "Theme Builder",
        "version": "10.12.2",
        "date": "",
        "brand": {
            "url": "http://www.bizagi.com",
            "logo": "libs/css/img/logo-white.png",
            "alt": "Bizagi"
        },
        "support": [
            "Chrome",
            "Firefox",
            "IE9+"
        ],
        "less": "libs/css/base.theme.less",
        "templates": [
            {   
                "name": "custom.theme.layout",
                "src": "libs/tmpl/layout.theme.generator.tmpl.html"
            },
            {
                "name": "custom.theme.layout.tabs",
                "src": "libs/tmpl/previewTabs.theme.generator.tmpl.html"
            },
            {
                "name": "custom.theme.category.list",
                "src": "libs/tmpl/categoryControls.theme.generator.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.select",
                "src": "libs/tmpl/select.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.new",
                "src": "libs/tmpl/new.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.switch",
                "src": "libs/tmpl/switch.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.radio",
                "src": "libs/tmpl/radio.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.preview.workportal",
                "src": "libs/tmpl/preview.workportal.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.preview.webparts",
                "src": "libs/tmpl/preview.webparts.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.themes",
                "src": "libs/tmpl/prebuild.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.save.panel",
                "src": "libs/tmpl/save.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.new.panel",
                "src": "libs/tmpl/new.dialog.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.update.panel",
                "src": "libs/tmpl/update.theme.ui.control.tmpl.html"
            },
            {
                "name": "custom.theme.ui.controls.color.base",
                "src": "libs/tmpl/color.base.theme.ui.control.tmpl.html"
            },
            {
                "name": "access.denied.form",
                "src": "libs/tmpl/access.denied.form.tmpl.html"
            }
        ],
        "css": {
            "securefonts": [
                {
                    "label": "Arial",
                    "value": "Arial, 'Helvetica Neue', Helvetica, sans-serif"
                },
                {
                    "label": "Franklin Gothic",
                    "value": "'Franklin Gothic Medium', 'Franklin Gothic', 'ITC Franklin Gothic', Arial, sans-serif"
                },
                {
                    "label": "Geneva",
                    "value": "Geneva, Tahoma, Verdana, sans-serif"
                },
                {
                    "label": "Helvetica Neue",
                    "value": "'Helvetica Neue', Helvetica, Arial, sans-serif"
                },
                {
                    "label": "Optima",
                    "value": "Optima, Segoe, 'Segoe UI', Candara, Calibri, Arial, sans-serif"
                },
                {
                    "label": "Segoe UI",
                    "value": "'Segoe UI', Frutiger, 'Frutiger Linotype', 'Dejavu Sans', 'Helvetica Neue', Arial, sans-serif"
                },
                {
                    "label": "Tahoma",
                    "value": "Tahoma, Verdana, Segoe, sans-serif"
                },
                {
                    "label": "Trebuchet MS",
                    "value": "'Trebuchet MS', 'Lucida Grande', 'Lucida Sans Unicode', 'Lucida Sans', Tahoma, sans-serif"
                },
                {
                    "label": "Verdana",
                    "value": "Verdana, Geneva, sans-serif"
                }
            ]
        },
        "previewPanels": [
            {
                "id": "tabsWorkportal",
                "name": "WorkPortal",
                "url": "../default.aspx",
                "css": "biz-ui-theme-panel-workportal"
            }
        ],
        "localization": {
            "en": "resources/bizagi.resources.json.txt",
            "en_us": "resources/bizagi.resources.json.txt",
            "es": "resources/bizagi.resources.es.json.txt",
            "es_es": "resources/bizagi.resources.es-es.json.txt",
            "de": "resources/bizagi.resources.de.json.txt",
            "fr": "resources/bizagi.resources.fr.json.txt",
            "it": "resources/bizagi.resources.it.json.txt",
            "ja": "resources/bizagi.resources.ja.json.txt",
            "nl": "resources/bizagi.resources.nl.json.txt",
            "pt-br": "resources/bizagi.resources.pt-br.json.txt",
            "pt": "resources/bizagi.resources.pt.json.txt",
            "ru": "resources/bizagi.resources.ru.json.txt",
            "zh": "resources/bizagi.resources.zh.json.txt",
			"cs-cz": "resources/bizagi.resources.cs-cz.json.txt",
			"cs": "resources/bizagi.resources.cs-cz.json.txt"
        }
    }
}